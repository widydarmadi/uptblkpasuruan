@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
              <p class="card-text">Selamat datang di halaman Administrator Website UPT Balai Latihan Kerja Pasuruan - Provinsi Jawa Timur</p>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
@endsection


@section('js')

@endsection