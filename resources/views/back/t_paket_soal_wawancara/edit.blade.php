@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <input type="hidden" value="{{$old->id_t_paket_soal_wawancara}}" id="id_t_paket_soal_wawancara" name="id_t_paket_soal_wawancara">

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-2">
                                <label class="col-form-label" for="nm_paket_soal_wawancara">Nama Modul / Paket</label>
                                </div>
                                <div class="col-sm-10">
                                <input type="text" value="{{$old->nm_paket_soal_wawancara}}" id="nm_paket_soal_wawancara" placeholder="contoh : APBD" class="form-control" name="nm_paket_soal_wawancara">
                                </div>
                            </div>
                        </div>



                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-2">
                                <label class="col-form-label" for="aktif">Active</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif" name="aktif">
                                        <option {{($old->aktif=='1') ? 'selected' : ''}} value="1">Yes</option>
                                        <option {{($old->aktif!='1') ? 'selected' : ''}} value="0">No</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-2">
                                <label class="col-form-label" for="m_soal">Pilih Soal</label>
                                </div>
                                <div class="col-sm-10">
                                    <select class="form-control" id="m_soal" name="m_soal">
                                        <option value="">-- pilih soal --</option>
                                        @foreach($m_soal as $item)
                                        <option  value="{{$item->id_m_soal_wawancara}}">{!!$item->soal_wawancara!!}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-9 offset-sm-2">
                            <button id="submitform" type="button" class="btn btn-success me-1 waves-effect waves-float waves-light mr-2"><span>Ubah Data</span></button>
                            <button id="addsoal" type="button" class="btn btn-info me-1 waves-effect waves-float waves-light mr-2"><span>Tambahkan Soal ke dalam Modul</span></button>
                            <a href="{{route('admin.t_paket_soal_wawancara.index')}}" class="btn btn-secondary waves-effect">Kembali</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        
        
        
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            
            <div class="card-body">
                <div class="row">
                    <div class="col">
                        <table class="table table-sm table-striped table-hover">
                            <thead>
                                <tr>
                                    <th>No.</th>
                                    <th>Soal</th>
                                    <th>Hapus</th>
                                </tr>
                            </thead>

                            <tbody>
                                @php
                                $i = 1;    
                                @endphp
                                @forelse ($paket_soal as $item)
                                    
                                <tr>
                                    <td>{{$i++}}</td>
                                    <td>{!!$item->soal->soal_wawancara!!}</td>
                                    <td><a href="javascript:void(0)" 
                                        data-id_t_paket_soal_wawancara="{{$item->id_t_paket_soal_wawancara}}"
                                        data-id_t_paket_soal_wawancara_det="{{$item->id_t_paket_soal_wawancara_det}}" 
                                        class="btn btn-sm btn-danger deletesoal">hapus</a></td>
                                </tr>

                                @empty
                                <tr>
                                    <td colspan="3" class="text-center">Belum ada soal yang ditambahkan ke modul ini</td>
                                </tr>
                                @endforelse

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
          </div>
        </div>



        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#submitform").click(function(){
            $(".text-danger").remove();
            event.preventDefault();
            var data = new FormData($('#form')[0]);
            $("#submitform").attr('disabled', true);
            $("#submitform span").text(loading_text);

            $.ajax({
                url:"{{ route("admin.t_paket_soal_wawancara.update") }}",
                method:"POST",
                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                data: data,
                processData: false,
                contentType: false,
                success:function(data)
                {
                    if($.isEmptyObject(data.error)){

                        if(data.status == true){
                            $("#submitform").removeAttr('disabled');
                            $("#submitform span").text('Submit');
                            $("form").each(function() { this.reset() });
                            swal.fire({
                                title: "Success",
                                text: data.message,
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }

                    }else{
                        displayWarningSwal();
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $.each(data.error, function(key, value) {
                            var element = $("#" + key);
                            element.closest("div.form-control")
                            .removeClass("text-danger")
                            .addClass(value.length > 0 ? "text-danger" : "")
                            .find("#error_" + key).remove();
                            element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                        });
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        });
        
        
        
        $("#addsoal").click(function(){
            $(".text-danger").remove();
            event.preventDefault();
            var data = new FormData($('#form')[0]);
            $("#addsoal").attr('disabled', true);
            $("#addsoal span").text(loading_text);

            $.ajax({
                url:"{{ route("admin.t_paket_soal_wawancara.addsoal") }}",
                method:"POST",
                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                data: data,
                processData: false,
                contentType: false,
                success:function(data)
                {
                    if($.isEmptyObject(data.error)){

                        if(data.status == true){
                            $("#addsoal").removeAttr('disabled');
                            $("#addsoal span").text('Tambahkan Soal ke dalam Modul');
                            $("form").each(function() { this.reset() });
                            swal.fire({
                                title: "Success",
                                text: data.message,
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }

                    }else{
                        displayWarningSwal();
                        $("#addsoal").removeAttr('disabled');
                        $("#addsoal span").text('Tambahkan Soal ke dalam Modul');
                        $.each(data.error, function(key, value) {
                            var element = $("#" + key);
                            element.closest("div.form-control")
                            .removeClass("text-danger")
                            .addClass(value.length > 0 ? "text-danger" : "")
                            .find("#error_" + key).remove();
                            element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                        });
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        });
    });



$('.deletesoal').click(function(){
    var id_t_paket_soal_wawancara = $(this).data("id_t_paket_soal_wawancara");
    var id_t_paket_soal_wawancara_det = $(this).data("id_t_paket_soal_wawancara_det");
    swal.fire({
        title: "Konfirmasi Penghapusan",
        text: 'Apakah Anda yakin menghapus soal di dalam modul ini ?',
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.t_paket_soal_wawancara.delete_soal') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id_t_paket_soal_wawancara_det:id_t_paket_soal_wawancara_det,
                    id_t_paket_soal_wawancara:id_t_paket_soal_wawancara
                },
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

    })
});

</script>
@endsection
