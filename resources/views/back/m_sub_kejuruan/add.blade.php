@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
              
            </div>
            
            <div class="card-body">
                
                <form method="post" id="form">

                    <div class="row">

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_kejuruan">Nama Sub Kejuruan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="nm_m_kejuruan" placeholder="contoh : MENJAHIT" class="form-control" name="nm_m_kejuruan">
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kategori_kejuruan">Kategori Kejuruan</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" id="id_m_kategori_kejuruan" name="id_m_kategori_kejuruan">
                                        <option value="">Pilih Kategori Kejuruan</option>
                                        @foreach ($kategori_kejuruan as $item)
                                        <option value="{{$item->id_m_kategori_kejuruan}}">{{$item->nm_m_kategori_kejuruan}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif">Active</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif" name="aktif">
                                        <option value="1">Yes</option>
                                        <option value="0">No</option>
                                      </select>
                                </div>
                            </div>
                        </div>


                   

                        <div class="col-sm-9 offset-sm-3">
                        <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_sub_kejuruan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_sub_kejuruan.save") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
    });

    
  //Date picker
  $('#picker').datetimepicker({
        format: 'L'
  });

</script>
@endsection
