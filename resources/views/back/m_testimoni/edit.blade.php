@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">


                        <input type="hidden" value="{{$old->id_m_testimoni}}" name="id_m_testimoni" />


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_pengirim">Nama Alumni</label>
                                </div>
                                <div class="col-sm-9">
                                <input id="nm_pengirim" value="{{$old->nm_pengirim}}" class="form-control" name="nm_pengirim" />
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="isi_m_testimoni">Isi Testimoni</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_isi_m_testimoni">{!!$old->isi_m_testimoni!!}</div>
                                    <textarea style="display: none" id="isi_m_testimoni" class="form-control" name="isi_m_testimoni" rows="4">{{$old->isi_m_testimoni}}</textarea>
                                </div>
                            </div>
                        </div>

                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jk">Jenis Kelamin</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="jk" name="jk">
                                        <option value="L" {{($old->jk == 'L') ? 'selected' : ''}}>LAKI-LAKI</option>
                                        <option value="P" {{($old->jk == 'P') ? 'selected' : ''}}>PEREMPUAN</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                      

                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif_m_testimoni">Tampilkan</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif_m_testimoni" name="aktif_m_testimoni">
                                        <option {{($old->aktif_m_testimoni=='1') ? 'selected' : ''}} value="1">Yes</option>
                                        <option {{($old->aktif_m_testimoni!='1') ? 'selected' : ''}} value="0">No</option>
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-sm-9 offset-sm-3">
                        <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_testimoni.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>

var editor = CKEDITOR.replace( 'div_isi_m_testimoni' );
editor.on( 'change', function( evt ) {
    $('#isi_m_testimoni').html(evt.editor.getData());
});

$(document).ready( function () {
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        

        $.ajax({
            url:"{{ route("admin.m_testimoni.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});

</script>
@endsection
