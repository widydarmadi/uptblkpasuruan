<!DOCTYPE html>
<!--
This is a starter template page. Use this page to start your new project from
scratch. This page gets rid of all links and provides the needed markup only.
-->
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{$page_title ?? '[Admin] UPT BLK Pasuruan'}}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />


  <link rel="stylesheet" href="{{asset('assets/back')}}/select2/select2.min.css">
  <link rel="stylesheet" href="{{asset('assets/back')}}/select2/select2-bootstrap4.min.css">

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/fonts/sourcesans.css">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/adminlte.min.css">
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/custom.css">
  <!-- DataTables -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/buttons.bootstrap4.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/daterangepicker.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/tempusdominus-bootstrap-4.min.css">
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
    

      <!-- Messages Dropdown Menu -->
      <li class="nav-item dropdown">
        <a class="nav-link" data-toggle="dropdown" href="#">
          <i class="far fa-user"></i> {{session()->get('logged_in.nm_user')}}
        </a>
        <div class="dropdown-menu dropdown-menu-right">
          {{-- <div class="dropdown-divider"></div> --}}
          <a class="dropdown-item" href="{{route('admin.edit_profile')}}">Edit Profil</a>
          <a href="{{route('admin.logout')}}" class="dropdown-item">Keluar</a>
        </div>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin.main')}}" class="brand-link ml-3">
      {{-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> --}}
      <span class="brand-text font-weight-bold">Website UPT BLK</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
  
      

 
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <li class="nav-item">
            <a href="javascript:void(0)" class="nav-link">
              <i class="nav-icon fas fa-home"></i>
              <p>
                Halaman Utama
              </p>
            </a>
          </li>
          @php
          $hak_akses = \App\Models\M_hak_akses::whereHas('menu', function(\Illuminate\Database\Eloquent\Builder $query){
            $query->whereNull('id_parent');
          })->where('id_m_user_group', session('logged_in.id_m_user_group'))
          ->where('m_menu.aktif', '1')
          ->join('m_menu', 'm_menu.id_m_menu','=','m_hak_akses.id_m_menu')->orderBy('m_menu.order_m_menu')->get();
          @endphp

          @foreach ($hak_akses as $item)
          <li class="nav-item @if($parent_menu_active == $item->menu->nm_menu) menu-open @endif">
            <a href="{{($item->menu->route) ? route($item->menu->route) : '#'}}" class="nav-link @if($child_menu_active == $item->menu->nm_menu) active @endif">
              <i class="nav-icon fas fa-{{$item->menu->icon}}"></i>
              <p>
                {{$item->menu->nm_menu}}
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            @php
            $parent = $item->id_m_menu;
            $sub = \DB::table('m_hak_akses')->join('m_menu','m_menu.id_m_menu','=','m_hak_akses.id_m_menu')
            ->where('m_menu.id_parent',$parent)->orderBy('m_menu.order_m_menu')
            ->where('m_menu.aktif', '1')
            ->where('m_hak_akses.id_m_user_group', session('logged_in.id_m_user_group'))->get();
            @endphp

            @if($sub->count() > 0)
            <ul class="nav nav-treeview">
            @endif

            @foreach($sub as $s)
              <li class="nav-item">
                <a 
                data-bs-toggle="tooltip" 
                data-bs-placement="right" 
                title data-bs-original-title="{{$s->nm_menu}}"
                href="{{\Route::has($s->route) ? route($s->route,['menu'=>$s->id_m_menu, 'process'=>$s->id_m_process]) : '#'}}" class="nav-link @if($child_menu_active == $s->nm_menu) active @endif">
                  <i class="far fa-circle nav-icon"></i>
                  <p>{{$s->nm_menu}}</p>
                </a>
              </li>
            @endforeach
            @if($sub->count() > 0)
            </ul>
            @endif
          </li>
          @endforeach
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper pt-4">
    {{-- <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">{{$page_title}}</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="{{route('admin.main')}}">Halaman Utama</a></li>
              <li class="breadcrumb-item active">{{$page_title}}</li>
            </ol>
          </div>
        </div>
      </div>
    </div> --}}
    <!-- /.content-header -->

    <!-- Main content -->
    @yield('content')
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="float-right d-none d-sm-inline">
      {{-- Anything you want --}}
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2021 {{env('APP_NAME')}}
  </footer>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->

<!-- jQuery -->
<script src="{{asset('assets/back')}}/js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/back')}}/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/back')}}/js/adminlte.min.js"></script>
<script src="{{asset('assets/back')}}/js/sweetalert2@10.js"></script>

<!-- DataTables  & Plugins -->
<script src="{{asset('assets/back')}}/js/jquery.dataTables.min.js"></script>
<script src="{{asset('assets/back')}}/js/dataTables.bootstrap4.min.js"></script>
<script src="{{asset('assets/back')}}/js/dataTables.responsive.min.js"></script>
<script src="{{asset('assets/back')}}/js/responsive.bootstrap4.min.js"></script>
<script src="{{asset('assets/back')}}/js/dataTables.buttons.min.js"></script>
<script src="{{asset('assets/back')}}/js/buttons.bootstrap4.min.js"></script>
<script src="{{asset('assets/back/js')}}/sweetalert2@10.js"></script>

<!-- date-range-picker -->
<script src="{{asset('assets/back/js')}}/daterangepicker.js"></script>
<script src="{{asset('assets/back/js')}}/moment.min.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('assets/back/js')}}/tempusdominus-bootstrap-4.min.js"></script>
<script src="{{asset('assets/back/js')}}/ckeditor/ckeditor.js"></script>

<script src="{{asset('assets/back')}}/select2/select2.full.min.js"></script>

<script>
  const loading_text = 'Harap tunggu...';
  const data_saved = 'Data Berhasil Disimpan';
  const data_deleted = 'Data Berhasil Dihapus';
  const confirm_delete_text = 'Apakah Anda yakin ingin menghapus data ini ?';

  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  })

  function displayErrorSwal(msg){
    if(msg == null){
      swal.fire("Oops !", "Server sedang bermasalah. Harap menghubungi administrator kami", "error");
    }else{
      swal.fire("Oops !", msg, "error");
    }
    $("#submitform").removeAttr('disabled');
    $("#submitform span").text('Submit');
  }


  function displayWarningSwal(msg){
    if(msg == null){
        swal.fire("Oops !", "Harap memeriksa kembali inputan Anda", "warning");
    }else{
        swal.fire("Oops !", msg, "warning");
    }
  }


</script>
@yield('js')
</body>
</html>
