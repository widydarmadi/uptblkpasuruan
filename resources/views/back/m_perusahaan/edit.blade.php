@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">

                <form method="post" id="form">

                    <div class="row">
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_perusahaan">Nama Perusahaan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="hidden" value="{{$old->id_m_perusahaan}}" id="id_m_perusahaan" class="form-control" name="id_m_perusahaan">
                                <input type="text" value="{{$old->nm_m_perusahaan}}" id="nm_m_perusahaan" class="form-control" name="nm_m_perusahaan">
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="alamat_m_perusahaan">Alamat Perusahaan</label>
                                </div>
                                <div class="col-sm-9">
                                <textarea id="alamat_m_perusahaan" class="form-control" name="alamat_m_perusahaan">{{$old->alamat_m_perusahaan}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="telp_m_perusahaan">Telp Perusahaan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="telp_m_perusahaan" value="{{$old->telp_m_perusahaan}}" class="form-control" name="telp_m_perusahaan">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="hrd_m_perusahaan">HRD Perusahaan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="hrd_m_perusahaan" value="{{$old->hrd_m_perusahaan}}" class="form-control" name="hrd_m_perusahaan">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="deskripsi_m_perusahaan">Deskripsi</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_deskripsi_m_perusahaan">{!!$old->deskripsi_m_perusahaan!!}</div>
                                    <textarea style="display: none" id="deskripsi_m_perusahaan" class="form-control" name="deskripsi_m_perusahaan" rows="4">{{$old->deskripsi_m_perusahaan}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="is_mitra">Bertindak sebagai Mitra ?</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="is_mitra" name="is_mitra">
                                        <option value="1" {{($old->is_mitra == '1' ? 'selected' : '')}}>YA</option>
                                        <option value="0" {{($old->is_mitra != '1' ? 'selected' : '')}}>TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif_m_perusahaan">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif_m_perusahaan" name="aktif_m_perusahaan">
                                        <option value="1" {{($old->aktif_m_perusahaan == '1' ? 'selected' : '')}}>YA</option>
                                        <option value="0" {{($old->aktif_m_perusahaan != '1' ? 'selected' : '')}}>TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="logo">Logo Perusahaan <br><span style="color: red">(format : jpg, png)</span></label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="logo" name="logo" class="form-control" />
                                </div>
                            </div>
                        </div>
                        


                        @if ($old->logo)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for=""></label>
                                </div>
                                <div class="col-sm-3">
                                    <img width="100%" src="{{asset('storage'.'/'.$old->logo)}}" />
                                </div>
                            </div>
                        </div>

                        @endif

                        
                        <div class="col-sm-9 offset-sm-3">
                            <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_perusahaan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
var editor = CKEDITOR.replace( 'div_deskripsi_m_perusahaan' );
editor.on( 'change', function( evt ) {
    $('#deskripsi_m_perusahaan').html(evt.editor.getData());
});


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_perusahaan.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});


$('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})
</script>
@endsection
