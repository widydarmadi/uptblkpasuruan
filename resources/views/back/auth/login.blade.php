<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>{{env('APP_NAME')}}</title>
  <meta name="csrf-token" content="{{ csrf_token() }}" />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/all.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/icheck-bootstrap.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('assets/back')}}/css/adminlte.min.css">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <!-- /.login-logo -->
  <div class="card card-outline card-primary">
    <div class="card-header text-center">
      <a href="javscript:void(0)" class="h1"><b>Web UPT BLK</b></a>
    </div>
    <div class="card-body">
      <p class="login-box-msg">Silahkan Masukkan<br>Username dan Password</p>

      <form method="post" id="login_form">
        <div class="row mb-3">
          <div class="col">
            <input type="text" id="username" name="username" class="form-control" placeholder="Username">
          </div>
        </div>
        <div class="row mb-3">
          <div class="col">
            <input type="password" id="password" name="password" class="form-control" placeholder="Password">
          </div>
        </div>
        <div class="row">
          <!-- /.col -->
          <div class="col-md-12">
            <button type="submit" id="submitform" class="btn btn-primary btn-block"><span>Sign In</span></button>
          </div>
          <!-- /.col -->
        </div>
      </form>

       

   
    </div>
    <!-- /.card-body -->
    
  </div>
  <!-- /.card -->
  <div class="text-center mt-2">
    2021 &copy; UPT Balai Latihan Kerja Pasuruan
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('assets/back')}}/js/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="{{asset('assets/back')}}/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="{{asset('assets/back')}}/js/adminlte.min.js"></script>

<script src="{{asset('assets/back')}}/js/sweetalert2@10.js"></script>

<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
$(document).ready(function(){
    $("#login_form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#login_form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text('Mohon tunggu...');

        $.ajax({
            url:"{{route('authenticate')}}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            datatype:'json',
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Log In');
                        $("form").each(function() { this.reset() });
                        location.href = data.redirect;

                    }else{
                        swal.fire("Oops", data.messages, "warning");
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Log In');
                    }

                }else{
                    // swal.fire("Terjadi kesalahan input!", "cek kembali inputan anda", "warning");
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Log In');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(){
                swal.fire("Telah terjadi kesalahan pada sistem", "Mohon refresh halaman browser Anda", "error");
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Log In');
            }
        });
    });
});
</script>
</body>
</html>
