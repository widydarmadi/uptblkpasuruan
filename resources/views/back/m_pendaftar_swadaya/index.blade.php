@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}} <a href="{{route('admin.m_pendaftar_swadaya.add')}}" class="btn btn-success btn-sm float-right d-flex">Tambah Data</a></h5>
          </div>
          <div class="card-body">

            <div class="row">
              <div class="col-md-6">
                <label class="form-label">Kategori Kejuruan</label>
                <select name="id_m_kategori_kejuruan" id="id_m_kategori_kejuruan" class="form-control select2">
                  <option value="">Pilih Kategori Kejuruan</option>
                  @forelse ($list_kategori_kejuruan as $item)
                      <option {{($id_m_kategori_kejuruan == $item->id_m_kategori_kejuruan) ? 'selected' : null}} value="{{$item->id_m_kategori_kejuruan}}">{{$item->nm_m_kategori_kejuruan}}</option>
                  @empty
                  @endforelse
                </select>
              </div>
              <div class="col-md-6">
                <label class="form-label">Sub Kejuruan</label>
                <select name="id_m_kejuruan" id="id_m_kejuruan" class="form-control select2">
                  <option value="">Pilih Sub Kejuruan</option>
                  @if(count($list_kejuruan) > 0)
                    @foreach ($list_kejuruan as $item)
                    <option {{($id_m_kejuruan == $item['id_m_kejuruan']) ? 'selected' : ''}} value="{{$item['id_m_kejuruan']}}">{{$item['nm_m_kejuruan']}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="col-md-6 mb-4 mt-4">
                <label class="form-label">Program Pelatihan</label>
                <select name="id_m_program_pelatihan" id="id_m_program_pelatihan" class="form-control select2">
                  <option value="">Pilih Program Pelatihan</option>
                  @if(count($list_program_pelatihan) > 0)
                    @foreach ($list_program_pelatihan as $item)
                    <option {{($id_m_program_pelatihan == $item['id_m_program_pelatihan']) ? 'selected' : ''}} value="{{$item['id_m_program_pelatihan']}}">{{$item['nm_m_program_pelatihan']}}</option>
                    @endforeach
                  @endif
                </select>
              </div>

              <div class="col-md-6 mb-4 mt-4">
                <label class="form-label">Jadwal / Gelombang</label>
                <select name="id_m_jadwal" id="id_m_jadwal" class="form-control select2">
                  <option value="">Pilih Jadwal / Gelombang</option>
                  @if(count($list_jadwal) > 0)
                    @foreach ($list_jadwal as $item)
                    <option {{(request()->get('id_m_jadwal') == $item['id_m_jadwal']) ? 'selected' : ''}} value="{{$item['id_m_jadwal']}}">{{$item['nm_m_jadwal']}}</option>
                    @endforeach
                  @endif
                </select>
              </div>
              <div class="col-md-4 mb-4">
                <button class="btn d-block w-100 btn-info" id="filter_report">FILTER</button>
              </div>
              <div class="col-md-4 mb-4">
                <button class="btn d-block w-100 btn-info" id="print_excel">EKSPOR EXCEL</button>
              </div>
              <div class="col-md-4 mb-4">
                <button class="btn d-block w-100 bg-purple" onclick="location.href='{{route('admin.m_pendaftar_swadaya.index')}}'"><span class="text-white">RESET FORM</span></button>
              </div>
            </div>

            <table id="datatable" class="table-striped table-sm table-hover table table-bordered dt-responsive nowrap">
              <thead>
                <tr>
                  <th>No</th>
                  <th>No Register</th>
                  <th>Tgl Permohonan</th>
                  <th>Pemohon</th>
                  <th>WA</th>
                  <th>Jadwal</th>
                  <th>Jumlah Peserta</th>
                  <th>Status</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>

          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>




<!-- Modal Siswa -->
<div
  class="modal fade"
  id="modal_peserta"
  tabindex="-1"
  aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true"
>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Tambah Data Peserta Pelatihan Pihak Ketiga</h5>
        <button type="button" class="close" onclick="location.href='{{route('admin.m_pendaftar_swadaya.index')}}'" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>

<!-- Modal Siswa -->
<div
  class="modal fade"
  id="modal_lihat_peserta"
  tabindex="-1"
  aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true"
>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Detail Peserta Pelatihan Pihak Ketiga</h5>
        <button type="button" class="close" onclick="location.href='{{route('admin.m_pendaftar_swadaya.index')}}'" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>


@endsection


@section('js')
<script>
$.ajaxSetup({
  headers: {
      'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});

var table;
var id_m_kategori_kejuruan = {{$id_m_kategori_kejuruan ?? 'null'}};
var id_m_kejuruan = {{$id_m_kejuruan ?? 'null' }};
var id_m_program_pelatihan = {{$id_m_program_pelatihan  ?? 'null'}};
var id_m_jadwal = {{request()->get('id_m_jadwal') ?? 'null'}};

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.m_pendaftar_swadaya.datatable') }}',
            method: 'post',
            data: function(q){
                q.id_m_kategori_kejuruan = id_m_kategori_kejuruan,
                q.id_m_kejuruan = id_m_kejuruan,
                q.id_m_program_pelatihan = id_m_program_pelatihan,
                q.id_m_jadwal = id_m_jadwal
            }
        },
    });
});


$('#filter_report').click(function(){
    id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
    id_m_kejuruan = $('#id_m_kejuruan').val();
    id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
    id_m_jadwal = $('#id_m_jadwal').val();
    table.ajax.reload();
})



$('#datatable').on('click', '.delete', function(){
    var id_m_pendaftar_mtu = $(this).data("id_m_pendaftar_mtu");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "YA",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_pendaftar_mtu.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_m_pendaftar_mtu:id_m_pendaftar_mtu},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});





$('#datatable').on('click', '.peserta', function(){
  var no_register = $(this).data('no_register');
  $('#modal_peserta').modal('show');
  $.ajax({
      url:"{{ route("admin.m_pendaftar_swadaya.peserta") }}",
      method:"get",
      data: {
          no_register:no_register
      },
      dataType: 'html',
      success:function(data)
      {
          $('#modal_peserta .modal-body').html(data);
      },
      error: function(data){
        displayErrorSwal();
      }
  });
})


$('#datatable').on('click', '.lihatpeserta', function(){
  var no_register = $(this).data('no_register');
  $('#modal_lihat_peserta').modal('show');
  $.ajax({
      url:"{{ route("admin.m_pendaftar_swadaya.lihat_peserta") }}",
      method:"get",
      data: {
          no_register:no_register
      },
      dataType: 'html',
      success:function(data)
      {
          $('#modal_lihat_peserta .modal-body').html(data);
      },
      error: function(data){
        displayErrorSwal();
      }
  });
})


$('#modal_peserta').on('submit', '#form_peserta', function(){
  $(".text-danger").remove();
  event.preventDefault();
  var data = new FormData($('#form_peserta')[0]);

  $.ajax({
      url:"{{ route("admin.m_pendaftar_swadaya.peserta_post") }}",
      method:"POST",
      headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
      data: data,
      processData: false,
      contentType: false,
      success:function(data)
      {
        if($.isEmptyObject(data.error)){

          if(data.status == true){
              $("#submitform").removeAttr('disabled');
              $("#submitform span").text('Submit');
              $("form").each(function() { this.reset() });
              swal.fire({
                  title: "Success",
                  text: data.message,
                  icon: "success"
              }).then(function() {
                $( '#form_peserta' ).each(function(){
                    $('#id_m_provinsi').val(null).trigger('change');
                    $('#id_m_kota').val(null).trigger('change');
                    this.reset();
                });
              });
          }else{
              displayErrorSwal(data.message);
          }

          }else{
              displayWarningSwal();
              $("#submitform").removeAttr('disabled');
              $("#submitform span").text('Submit');
              $.each(data.error, function(key, value) {
                  var element = $("#" + key);
                  element.closest("div.form-control")
                  .removeClass("text-danger")
                  .addClass(value.length > 0 ? "text-danger" : "")
                  .find("#error_" + key).remove();
                  element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
              });
          }
      },
      error: function(data){
          displayErrorSwal();
          $("#submitform").removeAttr('disabled');
          $("#submitform span").text('Upload');
      }
  });
})




$('#id_m_kategori_kejuruan').change(function(){
  var id = $(this).val();
  $('#id_m_kejuruan').html('');
  $('#id_m_program_pelatihan').html('');
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.m_pendaftar_swadaya.load_kejuruan_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_kejuruan').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})



$('#id_m_kejuruan').change(function(){
  var id = $(this).val();
  $('#id_m_program_pelatihan').html('');
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.m_pendaftar_swadaya.load_program_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_program_pelatihan').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})


$('#id_m_program_pelatihan').change(function(){
  var id = $(this).val();
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.m_pendaftar_swadaya.load_jadwal_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_jadwal').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})


$('#print_excel').click(function(){
  id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
  id_m_kejuruan = $('#id_m_kejuruan').val();
  id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
  id_m_jadwal = $('#id_m_jadwal').val();
  window.open('{{route('admin.m_pendaftar_swadaya.export_xls')}}?id_m_kategori_kejuruan=' + id_m_kategori_kejuruan + '&id_m_kejuruan=' + id_m_kejuruan + '&id_m_program_pelatihan=' + id_m_program_pelatihan + '&id_m_jadwal=' + id_m_jadwal, '_blank');
})


</script>
@endsection
