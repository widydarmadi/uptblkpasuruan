<form method="post" id="form_peserta">

    <div class="row">
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                <label class="col-form-label" for="no_register">Register Pelatihan Pihak Ketiga</label>
                </div>
                <div class="col-sm-9">
                <input type="text" value="{{$no_register}}" readonly id="no_register" class="form-control" name="no_register">
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                <label class="col-form-label" for="nm_t_siswa">Nama Lengkap</label>
                </div>
                <div class="col-sm-9">
                <input type="text"  id="nm_t_siswa" class="form-control" name="nm_t_siswa">
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                <label class="col-form-label" for="nik_t_siswa">NIK</label>
                </div>
                <div class="col-sm-9">
                <input type="text"  id="nik_t_siswa" class="form-control" name="nik_t_siswa">
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                <label class="col-form-label" for="id_m_provinsi">Provinsi</label>
                </div>
                <div class="col-sm-6">
                    @php
                        $list_provinsi = \App\Models\M_provinsi::orderBy('nm_m_provinsi')->get();
                    @endphp
                    <select name="id_m_provinsi" id="id_m_provinsi" class="form-control select2">
                        <option value="" class="value">-- pilih provinsi --</option>
                        @foreach ($list_provinsi as $item)
                            <option value="{{$item->id_m_provinsi}}" class="value">{{$item->nm_m_provinsi}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                <label class="col-form-label" for="id_m_kota">Pilih Kabupaten / Kota</label>
                </div>
                <div class="col-sm-6">
                    <select name="id_m_kota" id="id_m_kota" class="form-control select2">
                        <option value="" class="value">-- pilih provinsi terlebih dahulu --</option>
                    </select>
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                <label class="col-form-label" for="alamat_t_siswa">Alamat Rumah</label>
                </div>
                <div class="col-sm-9">
                    <input id="alamat_t_siswa" name="alamat_t_siswa" type="text" class="form-control">
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="jk_t_siswa">Jenis Kelamin</label>
                </div>
                <div class="col-sm-4">
                    <select id="jk_t_siswa" name="jk_t_siswa" class="select form-control">
                        <option value="">pilih salah satu</option>
                        <option value="LAKI-LAKI">LAKI-LAKI</option>
                        <option value="PEREMPUAN">PEREMPUAN</option>
                    </select>
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="id_m_pendidikan">Pendidikan Terakhir</label>
                </div>
                <div class="col-sm-4">
                    @php
                        $list_pendidikan = \App\Models\M_pendidikan::where('aktif_m_pendidikan','1')->get();
                    @endphp
                    
                    <select name="id_m_pendidikan" id="id_m_pendidikan" class="form-control" required>
                    @foreach ($list_pendidikan as $item)
                        <option value="{{$item->id_m_pendidikan}}">{{$item->nm_m_pendidikan}}</option>
                    @endforeach
                    </select>
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="nm_jurusan">Silahkan tulis Jurusan bagi yang lulusan SMA, SMK, D1, D3, dan S1</label>
                </div>
                <div class="col-sm-9">
                    <input id="nm_jurusan" name="nm_jurusan" type="text" class="form-control">
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="wa_t_siswa">No. HP (yang terdaftar No.WA Aktif)</label>
                </div>
                <div class="col-sm-4">
                    <input id="wa_t_siswa" name="wa_t_siswa" type="text" class="form-control">
                </div>
            </div>
        </div>
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="tempat_lahir_t_siswa">Tempat, Tanggal Lahir</label>
                </div>
                <div class="col-sm-4">
                    <input id="tempat_lahir_t_siswa" name="tempat_lahir_t_siswa" type="text" placeholder="Tempat Lahir" class="form-control">
                </div>
                <div class="col-sm-4">
                    <div class="input-group date" id="tgl_lahir_t_siswa" data-target-input="nearest">
                        <input placeholder="Tanggal Lahir" type="text" name="tgl_lahir_t_siswa" class="form-control datetimepicker-input" data-target="#tgl_lahir_t_siswa"/>
                        <div class="input-group-append" data-target="#tgl_lahir_t_siswa" data-toggle="datetimepicker">
                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="email_t_siswa">Email yang aktif</label>
                </div>
                <div class="col-sm-6">
                    <input id="email_t_siswa" name="email_t_siswa" type="text" class="form-control">
                </div>
            </div>
        </div>

        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="is_disabilitas">Apakah Penyandang Disabilitas ?</label>
                </div>
                <div class="col-sm-3">
                    <select name="is_disabilitas" id="is_disabilitas" class="form-control">
                        <option value="">-- pilih satu --</option>
                        <option value="TIDAK">TIDAK</option>
                        <option value="YA">YA</option>
                      </select>
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="id_m_disabilitas">Jika penyandang disabilitas, diisi jenis disabilitasnya</label>
                </div>
                <div class="col-sm-3">
                    <select name="id_m_disabilitas" id="id_m_disabilitas" class="form-control">
                        <option value="">-- pilih jenis disabilitas --</option>
                    </select>
                </div>
            </div>
        </div>
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="quest_pernah_blk">Pernah mengikuti kursus BLK ?</label>
                </div>
                <div class="col-sm-3">
                    <select name="quest_pernah_blk" id="quest_pernah_blk" class="form-control" required>
                        <option value="BELUM">BELUM PERNAH</option>
                        <option value="PERNAH">PERNAH</option>
                    </select>
                </div>
            </div>
        </div>
       
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="quest_masih_bekerja">Masih Bekerja ?</label>
                </div>
                <div class="col-sm-3">
                    <select name="quest_masih_bekerja" id="quest_masih_bekerja" class="form-control" required>
                        <option value="TIDAK">TIDAK</option>
                        <option value="YA">YA</option>
                      </select>
                </div>
            </div>
        </div>
        
        
        
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-3">
                    <label class="col-form-label" for="quest_minat_setelah_kursus">Minat Sesudah Menyelesaikan Pelatihan</label>
                </div>
                <div class="col-sm-3">
                    <select name="quest_minat_setelah_kursus" id="quest_minat_setelah_kursus" class="form-control" required>
                        <option value="BEKERJA">BEKERJA</option>
                        <option value="WIRASWASTA">WIRASWASTA</option>
                    </select>
                </div>
            </div>
        </div>

        <div class="col-sm-9 offset-sm-3">
            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
        </div>
    </div>
</form>



<script>
$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
    theme: 'bootstrap4'
    })

    //Date picker
    $('.date').each(function(){
        $(this).datetimepicker({
            format: 'Y-MM-DD'
        });
    })

})


$('#is_disabilitas').change(function(){
    var is_disabilitas = $('#is_disabilitas').val();
    $("#id_m_disabilitas").html('');
    if(is_disabilitas == 'YA'){
        $.ajax({
            type: 'post',
            url: '{{route('get_jenis_disabilitas')}}',
            data:{
                is_disabilitas:is_disabilitas
            },
            success: function (data) {
                $("#id_m_disabilitas").html(data.html);
            }
        });
    }
})


$('#id_m_provinsi').change(function(){
    var id_m_provinsi = $('#id_m_provinsi').val();
    $.ajax({
        type: 'post',
        url: '{{route('get_combo_kota')}}',
        data:{
        id_m_provinsi:id_m_provinsi
        },
        success: function (data) {
            $("#id_m_kota").html(data.html);
        }
    });
})


</script>