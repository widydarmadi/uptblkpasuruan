@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">

                        <input type="hidden" value="{{$old->id_m_program_pelatihan}}" id="id_m_program_pelatihan" name="id_m_program_pelatihan">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kategori_kejuruan">Kategori Kejuruan</label>
                                </div>
                                <div class="col-sm-5">
                                    <select class="form-control select2" id="id_m_kategori_kejuruan" name="id_m_kategori_kejuruan">
                                        <option value="">-- pilih kategori kejuruan --</option>
                                        @foreach ($kategori_kejuruan as $item)
                                        <option {{($item->id_m_kategori_kejuruan == $selected_kategori_kejuruan) ? 'selected' : ''}} value="{{$item->id_m_kategori_kejuruan}}">{{$item->nm_m_kategori_kejuruan}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kejuruan">Sub Kejuruan</label>
                                </div>
                                <div class="col-sm-5">
                                    <select class="form-control select2" id="id_m_kejuruan" name="id_m_kejuruan">
                                        <option value="">Pilih Sub Kejuruan</option>
                                        @if($list_sub_kejuruan)
                                        @foreach ($list_sub_kejuruan as $item)
                                        <option {{($item->id_m_kejuruan == $old->id_m_kejuruan) ? 'selected' : ''}} value="{{$item->id_m_kejuruan}}">{{$item->nm_m_kejuruan}}</option>
                                        @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_program_pelatihan">Nama Program</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->nm_m_program_pelatihan}}" id="nm_m_program_pelatihan" placeholder="contoh : Program ABC" class="form-control" name="nm_m_program_pelatihan">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="deskripsi">Deskripsi Singkat</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea id="deskripsi" placeholder="" class="form-control" name="deskripsi">{{$old->deskripsi}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jampel">Jumlah Jam Pelajaran</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->jampel}}" id="jampel" placeholder="contoh : 80 jam" class="form-control" name="jampel">
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_sumber_dana">Sumber Dana</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="id_m_sumber_dana" name="id_m_sumber_dana">
                                        <option value="">-- pilih sumber dana --</option>
                                        @foreach ($sumber_dana as $item)
                                        <option {{($item->id_m_sumber_dana == $old->id_m_sumber_dana) ? 'selected' : ''}} value="{{$item->id_m_sumber_dana}}">{{$item->nm_m_sumber_dana}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>


    
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_tipe_kejuruan">Tipe Pelatihan</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="id_m_tipe_pelatihan" name="id_m_tipe_pelatihan">
                                        <option value="">-- pilih tipe pelatihan --</option>
                                        @foreach ($tipe_pelatihan as $item)
                                        <option {{($item->id_m_tipe_pelatihan == $old->id_m_tipe_pelatihan) ? 'selected' : ''}} value="{{$item->id_m_tipe_pelatihan}}">{{$item->nm_m_tipe_pelatihan}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif" name="aktif">
                                        <option {{($old->aktif=='1') ? 'selected' : ''}} value="1">YA</option>
                                        <option {{($old->aktif!='1') ? 'selected' : ''}} value="0">TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="foto">Foto Sampul</label>
                                </div>
                                <div class="col-sm-3">
                                <input type="file" id="foto" class="form-control" name="foto">
                                </div>
                            </div>
                        </div>

                        @if ($old->foto)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="foto"></label>
                                </div>
                                <div class="col-sm-3">
                                    <img src="{{asset('storage'.'/'.$old->foto)}}" width="100%" />
                                    <br>
                                    <br>
                                    <a data-id_m_program_pelatihan="{{$old->id_m_program_pelatihan}}" class="resetfoto btn btn-sm btn-warning" href="javascript:void(0)">hapus foto sampul</a>
                                </div>
                            </div>
                        </div>

                        @endif
                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="file_pdf">Narasi Berupa File PDF</label>
                                </div>
                                <div class="col-sm-3">
                                <input type="file" id="file_pdf" class="form-control" name="file_pdf">
                                </div>
                            </div>
                        </div>

                        @if ($old->file_pdf)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="file_pdf"></label>
                                </div>
                                <div class="col-sm-3">
                                    <a target="_blank" class="btn btn-sm btn-info" href="{{asset('storage'.'/'.$old->file_pdf)}}">lihat file</a>
                                    <br>
                                    <br>
                                    <a data-id_m_program_pelatihan="{{$old->id_m_program_pelatihan}}" class="resetpdf btn btn-sm btn-warning" href="javascript:void(0)">hapus file pdf</a>
                                </div>
                            </div>
                        </div>

                        @endif



                        



                        <div class="col-sm-9 offset-sm-3">
                        <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_program_pelatihan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_program_pelatihan.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });


    $(".resetfoto").click(function(){
        var id_m_program_pelatihan = $(this).data("id_m_program_pelatihan");
        swal.fire({
            title: "Confirmation",
            text: 'Apakah Anda yakin ingin menghapus file gambar ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            cancelButtonText: "Cancel",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){
                $.ajax({
                    url:"{{ route('admin.m_program_pelatihan.reset') }}",
                    method:"post",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        id_m_program_pelatihan:id_m_program_pelatihan,
                        edit:{{request()->get('id_m_program_pelatihan')}}
                    },
                    success:function(data)
                    {
                        if(data.status == true){
                            swal.fire({
                                title: "Sukses!",
                                text: 'Gambar latar telah dikembalikan ke mode default',
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }

        })
    })
    
    
    $(".resetpdf").click(function(){
        var id_m_program_pelatihan = $(this).data("id_m_program_pelatihan");
        swal.fire({
            title: "Confirmation",
            text: 'Apakah Anda yakin ingin menghapus file pdf ini ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            cancelButtonText: "Cancel",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){
                $.ajax({
                    url:"{{ route('admin.m_program_pelatihan.reset_pdf') }}",
                    method:"post",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        id_m_program_pelatihan:id_m_program_pelatihan,
                        edit:{{request()->get('id_m_program_pelatihan')}}
                    },
                    success:function(data)
                    {
                        if(data.status == true){
                            swal.fire({
                                title: "Sukses!",
                                text: 'Gambar latar telah dikembalikan ke mode default',
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }

        })
    })

            
    $('#id_m_kategori_kejuruan').change(function(){
        var id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
        $.ajax({
            type: 'post',
            url: '{{route('get_combo_kejuruan')}}',
            data:{
            id_m_kategori_kejuruan:id_m_kategori_kejuruan
            },
            success: function (data) {
                $("#id_m_kejuruan").html(data.html);
            }
        });
    })

        
    });

</script>
@endsection
