<hr />
<p>Anda dapat menambahkan file gambar dengan mengklik Pilih File / Gambar, kemudian klik "Upload File"</p>
<form method="post" id="form_file">
    <div class="row">
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-4">
                    <label class="col-form-label" for="upload_m_file">Pilih File / Gambar</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" id="id_m_folder" value="{{$id_m_folder}}" class="form-control" name="id_m_folder">
                        <input type="file" id="upload_m_file" class="form-control" name="upload_m_file">
                    </div>
                    <div class="col-sm-4">
                        <div class="float-right">
                            <button type="button" id="submit_file" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Upload File</span></button>
                        </div>
                    </div>
            </div>
        </div>

    </div>
</form>


<div class="row mt-3">
    @forelse ($folder as $item)
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img width="100%" style="height: 180px;"  class="w-100" src="{{asset('storage'.'/'.$item->upload_m_file)}}" />
                </div>
                <div class="col-md-12 mt-2">
                    <input type="text" style="display: none;" value="{{asset('storage'.'/'.$item->upload_m_file)}}" id="clipboard_{{$item->id_m_file}}">
                    <a href="javascript:void(0)" onclick="copys({{$item->id_m_file}})" class="btn btn-sm btn-secondary copy"><i class="fas fa-link"></i> Salin URL</a>
                    <a href="javascript:void(0)" data-id_m_folder="{{$item->id_m_folder}}" data-id_m_file="{{$item->id_m_file}}" class="btn btn-sm btn-danger delete_file"><i class="fas fa-trash"></i> Hapus Media</a>

                </div>
            </div>
        </div>
    @empty
        <div class="col-md-12">
            <div class="alert alert-info">Belum ada file yang diunggah di dalam folder ini.</div>
        </div>
    @endforelse
</div>



<script>
    
function copys(ids) {
  // Get the text field
  var copyText = document.getElementById("clipboard_"+ids);

  // Select the text field
  copyText.select();
  copyText.setSelectionRange(0, 99999); // For mobile devices

   // Copy the text inside the text field
  navigator.clipboard.writeText(copyText.value);
    swal.fire({
        title: "Copied !",
        text: "URL telah berhasil disalin",
        icon: "success"
    }).then(function() {

    });
}


</script>