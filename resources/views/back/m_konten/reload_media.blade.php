<div class="row mt-3">
    @forelse ($folder as $item)
        <div class="col-md-4">
            <div class="row">
                <div class="col-md-12">
                    <img src="{{$item->upload_m_file}}" class="w-100" />
                </div>
                <div class="col-md-12 mt-2">
                    <a href="javascript:void(0)" data-upload_m_file="{{$item->upload_m_file}}" class="btn btn-sm btn-secondary"><i class="fas fa-link"> Salin URL</a>
                </div>
                <div class="col-md-12">
                    <a href="javascript:void(0)" data-id_m_file="{{$item->id_m_file}}" class="btn btn-sm btn-danger"><i class="fas fa-trash"> Hapus Media</a>
                </div>
            </div>
        </div>
    @empty
        <div class="col-md-12">
            <div class="alert alert-info">Belum ada file yang diunggah di dalam folder ini.</div>
        </div>
    @endforelse
</div>
