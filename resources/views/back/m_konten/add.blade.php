@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            Untuk menyisipkan beberapa foto dalam sebuah konten, silahkan klik <strong>Tambah Folder Media</strong> atau <strong>menambah file di dalam folder yang sudah ada</strong> dengan mengklik tombol <strong>salin URL</strong> pada tabel <strong>"Kelola Media"</strong> pada bagian bawah halaman ini, 
                            kemudian klik icon insert image <img src="{{asset('assets/back/images/img.png')}}" /> pada text editor <strong>"Narasi Lengkap"</strong>.
                            Kemudian <strong>Paste / tempelkan</strong> URL yang telah disalin pada kolom isian URL seperti gambar berikut : <br><img height="40" src="{{asset('assets/back/images/url.png')}}" />
                            <hr />
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="judul_m_konten">Judul Konten / Posting</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="judul_m_konten" class="form-control" name="judul_m_konten">
                                </div>
                            </div>
                        </div>

                        

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="narasi_m_konten">Narasi Lengkap</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_narasi_m_konten"></div>
                                    <textarea style="display: none" id="narasi_m_konten" class="form-control" name="narasi_m_konten" rows="4"></textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif_m_konten">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif_m_konten" name="aktif_m_konten">
                                        <option value="1">YA</option>
                                        <option value="0">TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo">Pilih Foto primer (opsional)</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="photo" name="photo" class="form-control" />
                                </div>
                            </div>
                        </div>
                        
                        {{--                         
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo2">Pilih Foto sekunder (opsional)</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="photo2" name="photo2" class="form-control" />
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="bg_m_konten">Foto Untuk Background Header (opsional)

                                </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="bg_m_konten" name="bg_m_konten" class="form-control" />
                                </div>
                                <div class="col-sm-6">
                                    <p class="text-info">Resolusi ideal adalah : 1900 x 500 pixel (landscape)</p>
                                </div>
                            </div>
                        </div>
 --}}

                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_konten.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">Kelola Media (Sisipkan beberapa gambar pada konten)
              <a href="javascript:void(0)" id="addfolder" class="btn btn-success btn-sm float-right d-flex">Tambah Folder Media</a>
              </h5>
            </div>
            <div class="card-body">
                <table id="datatable" class="table table-hover table-bordered table-sm">
                    <thead>
                        <tr>
                            <th>No.</th>
                            <th>Nama Folder Media</th>
                            <th>Tanggal Buat</th>
                            <th>Opsi</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>



<!-- Modal Hak Akses Per Module -->
<div
  class="modal fade"
  id="modal_create_folder"
  tabindex="-1"
  aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true"
>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Kelola Folder Media</h5>
        <button type="button" class="close" onclick="location.href='{{route('admin.m_konten.add')}}'" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>


@endsection

@section('js')
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var editor = CKEDITOR.replace( 'div_narasi_m_konten' );
editor.on( 'change', function( evt ) {
    $('#narasi_m_konten').html(evt.editor.getData());
});

var table;

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.m_konten.datatable_media') }}',
            method: 'post'
        },
    });
});


$('#addfolder').click(function(){
  $('#modal_create_folder').modal('show');
  
  $.ajax({
      url:"{{ route("admin.m_konten.add_media") }}",
      method:"post",
      dataType: 'html',
      success:function(data)
      {
          $('#modal_create_folder .modal-body').html(data);
      },
      error: function(data){
        displayErrorSwal();
      }
  });
})


$('#datatable').on('click', '.edit', function(){
  $('#modal_create_folder').modal('show');
  var id_m_folder = $(this).data("id_m_folder");
  $.ajax({
      url:"{{ route("admin.m_konten.edit_media") }}",
      method:"post",
      data:{
        id_m_folder : id_m_folder
      },
      dataType: 'html',
      success:function(data)
      {
          $('#modal_create_folder .modal-body').html(data);
      },
      error: function(data){
        displayErrorSwal();
      }
  });
})

    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_konten.save") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});




$('#datatable').on('click', '.delete_folder', function(){
    var id_m_folder = $(this).data("id_m_folder");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_konten.delete_folder') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_m_folder:id_m_folder},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});

</script>
@endsection
