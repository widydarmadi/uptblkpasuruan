<form method="post" id="form_media">
    <div class="row">
        <div class="col-12">
            <div class="mb-3 row">
                <div class="col-sm-5">
                <label class="col-form-label" for="nm_m_folder">Nama Folder</label>
                </div>
                <div class="col-sm-7">
                <input type="text" id="nm_m_folder" readonly class="form-control" value="{{$old->nm_m_folder}}" name="nm_m_folder">
                </div>
            </div>
        </div>

{{--         
        <div class="col-sm-12">
            <button type="submit" id="submit_folder" class="btn btn-info me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
        </div> --}}
    </div>
</form>


<div id="load_folder">
    <hr />

    <p>Anda dapat menambahkan file gambar dengan mengklik Pilih File / Gambar, kemudian klik "Upload File"</p>

    <form method="post" id="form_file">
        <div class="row">
            <div class="col-12">
                <div class="mb-3 row">
                    <div class="col-sm-4">
                    <label class="col-form-label" for="upload_m_file">Pilih File / Gambar</label>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" id="id_m_folder" value="{{$old->id_m_folder}}" class="form-control" name="id_m_folder">
                        <input type="file" id="upload_m_file" class="form-control" name="upload_m_file">
                    </div>
                    <div class="col-sm-4">
                        <div class="float-right">
                            <button type="button" id="submit_file" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Upload File</span></button>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </form>


    <div class="row mt-3">
        @forelse ($old->file as $item)
            <div class="col-md-4">
                <div class="row">
                    <div class="col-md-12">
                        <img width="100%" style="height: 180px;" class="w-100" src="{{asset('storage'.'/'.$item->upload_m_file)}}" />
                    </div>
                    <div class="col-md-12 mt-2">
                        <input type="text" style="display: none;" value="{{asset('storage'.'/'.$item->upload_m_file)}}" id="clipboard_{{$item->id_m_file}}">
                        <a href="javascript:void(0)" onclick="copys({{$item->id_m_file}})" class="btn btn-sm btn-secondary copy"><i class="fas fa-link"></i> Salin URL</a>
                        <a href="javascript:void(0)" data-id_m_file="{{$item->id_m_file}}" data-id_m_folder="{{$old->id_m_folder}}" class="btn btn-sm btn-danger delete_file"><i class="fas fa-trash"></i> Hapus Media</a>

                    </div>
                </div>
            </div>
        @empty
            <div class="col-md-12">
                <div class="alert alert-info">Belum ada file yang diunggah di dalam folder ini.</div>
            </div>
        @endforelse
    </div>

</div>


<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

function copys(ids) {
  // Get the text field
  var copyText = document.getElementById("clipboard_"+ids);

  // Select the text field
  copyText.select();
  copyText.setSelectionRange(0, 99999); // For mobile devices

   // Copy the text inside the text field
  navigator.clipboard.writeText(copyText.value);
    swal.fire({
        title: "Copied !",
        text: "URL telah berhasil disalin",
        icon: "success"
    }).then(function() {

    });
}

$(document).ready( function () {
    $("#form_media").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form_media')[0]);
        $("#submit_folder").attr('disabled', true);
        $("#submit_folder span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_konten.save_media") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submit_folder").removeAttr('disabled');
                        $("#submit_folder span").text('Submit');
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            // location.href = data.redirect;
                            $('#load_folder').html('');
                            $('#submit_folder').css('display', 'none');
                            $('#nm_m_folder').attr('readonly', true);
                            $.ajax({
                                url:"{{ route("admin.m_konten.load_folder_media") }}",
                                method:"POST",
                                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                                data: {
                                    id_m_folder : data.id_m_folder
                                },
                                success:function(data)
                                {
                                    $('#load_folder').html(data)
                                },
                                error: function(data){
                                    displayErrorSwal(data.message);
                                }
                            });
                            
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submit_folder").removeAttr('disabled');
                    $("#submit_folder span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });





    $('#load_folder').on('click', '#submit_file' , function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form_file')[0]);
        $("#submit_file").attr('disabled', true);
        $("#submit_file span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_konten.save_file") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submit_file").removeAttr('disabled');
                        $("#submit_file span").text('Upload File');
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            $.ajax({
                                url:"{{ route("admin.m_konten.load_folder_media") }}",
                                method:"POST",
                                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                                data: {
                                    id_m_folder : data.id_m_folder
                                },
                                success:function(data)
                                {
                                    $('#load_folder').html(data)
                                },
                                error: function(data){
                                    displayErrorSwal(data.message);
                                }
                            });
                            
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submit_file").removeAttr('disabled');
                    $("#submit_file span").text('Upload File');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });


});





$('#load_folder').on('click', '.delete_file' , function(){
    var id_m_file = $(this).data("id_m_file");
    var id_m_folder = $(this).data("id_m_folder");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OKA",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_konten.delete_file') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id_m_file:id_m_file,
                    id_m_folder:id_m_folder
                },
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            $.ajax({
                                url:"{{ route("admin.m_konten.load_folder_media") }}",
                                method:"POST",
                                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                                data: {
                                    id_m_folder : data.id_m_folder
                                },
                                success:function(data)
                                {
                                    $('#load_folder').html(data)
                                },
                                error: function(data){
                                    displayErrorSwal(data.message);
                                }
                            });
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});



$('.delete_file').click(function(){
    var id_m_file = $(this).data("id_m_file");
    var id_m_folder = $(this).data("id_m_folder");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OKB",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_konten.delete_file') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id_m_file:id_m_file,
                    id_m_folder:id_m_folder
                },
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            $.ajax({
                                url:"{{ route("admin.m_konten.load_folder_media") }}",
                                method:"POST",
                                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                                data: {
                                    id_m_folder : data.id_m_folder
                                },
                                success:function(data)
                                {
                                    $('#load_folder').html(data)
                                },
                                error: function(data){
                                    displayErrorSwal(data.message);
                                }
                            });
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});
</script>
