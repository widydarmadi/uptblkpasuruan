@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="judul_primary_m_welcome">Teks Baris 1</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->judul_primary_m_welcome}}" id="judul_primary_m_welcome"  class="form-control" name="judul_primary_m_welcome">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="judul_secondary_m_welcome">Teks Baris 2</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->judul_secondary_m_welcome}}" id="judul_secondary_m_welcome"  class="form-control" name="judul_secondary_m_welcome">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="video_m_welcome">Link Video Youtube</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->video_m_welcome}}" placeholder="contoh : https://www.youtube.com/watch?v=Wul43r5snCM&ab_channel=UPTBALAILATIHANKERJAPASURUAN" id="video_m_welcome"  class="form-control" name="video_m_welcome">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="teks_m_welcome">Narasi Sambutan</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_teks_m_welcome">{!!$old->teks_m_welcome!!}</div>
                                    <textarea style="display: none" id="teks_m_welcome" class="form-control" name="teks_m_welcome" rows="4">{{$old->teks_m_welcome}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-9 offset-sm-3">
                            <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>

var editor = CKEDITOR.replace( 'div_teks_m_welcome' );
editor.on( 'change', function( evt ) {
    $('#teks_m_welcome').html(evt.editor.getData());
});


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_welcome.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
    });

</script>
@endsection
