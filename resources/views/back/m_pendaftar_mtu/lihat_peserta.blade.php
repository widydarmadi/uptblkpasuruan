<input id="no_register" name="no_register" value="{{$no_register}}" type="hidden" />

<div class="row">
    <div class="col-md 4">
        <div class="alert bg-info">
            <p>No Register : {{$no_register}}</p>
            <p>Judul Pelatihan : {{$nama_latihan}}</p>
        </div>
    </div>
</div>

<table id="datatablez" class="table table-sm table-striped table-hover table-bordered">
<thead>
    <tr>
        <th>No.</th>
        <th>NIK</th>
        <th>Nama Peserta</th>
        <th>Email</th>
        <th>HP</th>
        <th>Alamat</th>
        <th>Pendidikan</th>
        <th>TTL</th>
        <th>Aksi</th>
    </tr>
</thead>
<tbody>
  
</tbody>
</table>


<script>
var table;
var no_register = $('#no_register').val();
$(document).ready( function () {
    table = $('#datatablez').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.m_pendaftar_mtu.datatable_siswa') }}',
            method: 'post',
            data: function(q){
                q.no_register = no_register
            }
        },
    });
});



$('#datatablez').on('click', '.deletesiswa', function(){
    var id_t_siswa_noninst = $(this).data("id_t_siswa_noninst");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "YA",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_pendaftar_mtu.delete_peserta') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_t_siswa_noninst:id_t_siswa_noninst},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});
</script>

