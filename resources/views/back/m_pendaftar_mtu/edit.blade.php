@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <input type="hidden" name="id_m_pendaftar_mtu" id="id_m_pendaftar_mtu" value="{{$old->id_m_pendaftar_mtu}}" />

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_pemohon">Nama Pemohon</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->nm_pemohon}}"  id="nm_pemohon" class="form-control" name="nm_pemohon">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jabatan_pemohon">Jabatan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->jabatan_pemohon}}"  id="jabatan_pemohon" class="form-control" name="jabatan_pemohon">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="wa_pemohon">No HP / WhatsApp yang dapat dihubungi</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text"  value="{{$old->wa_pemohon}}" id="wa_pemohon" class="form-control" name="wa_pemohon">
                                </div>
                            </div>
                        </div>
                  
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="email_pemohon">Email Pemohon</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" value="{{$old->email_pemohon}}"  id="email_pemohon" class="form-control" name="email_pemohon">
                                </div>
                            </div>
                        </div>
                        
                  
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_jenis_lokasi_mtu">Jenis Lokasi MTU</label>
                                </div>
                                <div class="col-sm-5">
                                    <select name="id_m_jenis_lokasi_mtu" class="form-control select2" id="id_m_jenis_lokasi_mtu">
                                        <option value="">-- pilih --</option>
                                        @foreach ($jenis_lokasi as $item)
                                            <option {{($item->id_m_jenis_lokasi_mtu == $old->id_m_jenis_lokasi_mtu) ? 'selected' : null}} value="{{$item->id_m_jenis_lokasi_mtu}}">{{$item->nm_jenis_lokasi_mtu}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_lokasi_mtu">Nama Lokasi MTU</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->nm_lokasi_mtu}}"  id="nm_lokasi_mtu" class="form-control" name="nm_lokasi_mtu">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="telp_lokasi_mtu">Telp Lokasi MTU</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" value="{{$old->telp_lokasi_mtu}}"  id="telp_lokasi_mtu" class="form-control" name="telp_lokasi_mtu">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="email_mtu">Email MTU</label>
                                </div>
                                <div class="col-sm-5">
                                <input type="text" value="{{$old->email_mtu}}"  id="email_mtu" class="form-control" name="email_mtu">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="alamat_lokasi_mtu">Alamat Lokasi MTU</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->alamat_lokasi_mtu}}" id="alamat_lokasi_mtu" class="form-control" name="alamat_lokasi_mtu">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_provinsi">Provinsi</label>
                                </div>
                                <div class="col-sm-6">
                                    @php
                                        $list_provinsi = \App\Models\M_provinsi::orderBy('nm_m_provinsi')->get();
                                    @endphp
                                    <select name="id_m_provinsi" id="id_m_provinsi" class="form-control select2">
                                        <option value="" class="value">-- pilih provinsi --</option>
                                        @foreach ($list_provinsi as $item)
                                            <option {{($id_m_provinsi == $item->id_m_provinsi) ? 'selected' : null}} value="{{$item->id_m_provinsi}}" class="value">{{$item->nm_m_provinsi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kota">Pilih Kabupaten / Kota</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="id_m_kota" id="id_m_kota" class="form-control select2">
                                        <option value="" class="value">-- pilih provinsi terlebih dahulu --</option>
                                        @foreach ($list_kab as $item)
                                            <option {{($old->id_m_kota == $item->id_m_kota) ? 'selected' : null}} value="{{$item->id_m_kota}}" class="value">{{$item->nm_m_kota}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kecamatan">Pilih Kecamatan</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="id_m_kecamatan" id="id_m_kecamatan" class="form-control select2">
                                        <option value="" class="value">-- pilih kab/kota terlebih dahulu --</option>
                                        @foreach ($list_kec as $item)
                                            <option {{($old->id_m_kecamatan == $item->id_m_kecamatan) ? 'selected' : null}} value="{{$item->id_m_kecamatan}}" class="value">{{$item->nm_m_kecamatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kelurahan">Pilih Kelurahan</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="id_m_kelurahan" id="id_m_kelurahan" class="form-control select2">
                                        <option value="" class="value">-- pilih kecamatan terlebih dahulu --</option>
                                        @foreach ($list_kel as $item)
                                            <option {{($old->id_m_kelurahan == $item->id_m_kelurahan) ? 'selected' : null}} value="{{$item->id_m_kelurahan}}" class="value">{{$item->nm_m_kelurahan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="potensi_wilayah">Potensi Wilayah</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea name="potensi_wilayah" id="potensi_wilayah" class="form-control">{{$old->potensi_wilayah}}</textarea>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="id_m_jadwal">Pilih pelatihan yang akan diikuti</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="id_m_jadwal" id="id_m_jadwal" class="form-control select2">
                                        <option value="" >-- pilih pelatihan --</option>

                                        @if($list_gelombang)
                                          @php
                                            app()->setLocale('id');
                                          @endphp
                                          @forelse ($list_gelombang as $item)
                                            @php
                                                $mulai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_mulai)))->isoFormat('D MMMM YYYY');
                                                $selesai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_selesai)))->isoFormat('D MMMM YYYY');
                                            @endphp
                                            <option {{($item->id_m_jadwal == $old->id_m_jadwal) ? 'selected' : ''}} value="{{$item->id_m_jadwal}}">{{$item->nm_m_jadwal.' | '.$mulai.' s/d '.$selesai}}</option>
                                          @empty
                                            <option value="0">BELUM ADA JADWAL PELATIHAN DIBUKA</option>
                                          @endforelse
                                        @endif
                                        
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="progress">Set Status Pengajuan</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="progress" id="progress" class="form-control">
                                        <option value="">-- pilih status --</option>
                                        <option {{($old->progress == 'DITINDAKLANJUTI') ? 'selected' : null}} value="DITINDAKLANJUTI">DITINDAKLANJUTI</option>
                                        <option {{($old->progress == 'DISETUJUI') ? 'selected' : null}} value="DISETUJUI">DISETUJUI</option>
                                        <option {{($old->progress == 'DITOLAK') ? 'selected' : null}} value="DITOLAK">DITOLAK</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="is_tampil">Tampilkan di pengumuman website ?</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="is_tampil" id="is_tampil" class="form-control">
                                        <option {{($old->is_tampil != 'YA') ? 'selected' : null}} value="TIDAK">TIDAK</option>
                                        <option {{($old->is_tampil == 'YA') ? 'selected' : null}} value="YA">YA</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="file_proposal">File Proposal (PDF)</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="file" name="file_proposal" id="file_proposal" class="form-control" />
                                </div>
                                <div class="col-sm-2">
                                    <a href="{{asset('storage'.'/'.$old->file_proposal)}}" target="_blank" class="btn btn-sm btn-warning">lihat file</a>
                                </div>
                            </div>
                        </div>
                        

                        <input placeholder="grup_id_cbt" type="hidden" value="{{(request()->filled('grup_id')) ? request()->get('grup_id') : null}}" id="cbt_grup_id" name="cbt_grup_id" />
                        <input placeholder="grup_id_cbt" type="hidden" value="MTU" id="cat" name="cat" />

                

                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_pendaftar_mtu.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
  //Date picker
  $('.date').each(function(){
      $(this).datetimepicker({
         format: 'Y-MM-DD'
      });
  })


  $('#is_disabilitas').change(function(){
    var is_disabilitas = $('#is_disabilitas').val();
    $("#id_m_disabilitas").html('');
    if(is_disabilitas == 'YA'){
        $.ajax({
            type: 'post',
            url: '{{route('get_jenis_disabilitas')}}',
            data:{
                is_disabilitas:is_disabilitas
            },
            success: function (data) {
                $("#id_m_disabilitas").html(data.html);
            }
        });
    }
})



$('#id_m_jadwal').change(function(){
    var id_m_jadwal = $(this).val();
    $.ajax({
        type: 'post',
        url: '{{route('get_cbt_grup_id')}}',
        data:{
        id_m_jadwal:id_m_jadwal
        },
        success: function (data) {
        if(data.valuez){
            $("#cbt_grup_id").val(data.valuez);
        }else{
            $("#cbt_grup_id").val('');
        }
        }
    });
})


$(document).ready( function () {
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_pendaftar_mtu.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});


$('#id_m_provinsi').change(function(){
    var id_m_provinsi = $('#id_m_provinsi').val();
    $('#id_m_kota').val('');
    $('#id_m_kecamatan').html('');
    $('#id_m_kelurahan').html('');
    $.ajax({
        type: 'post',
        url: '{{route('get_combo_kota')}}',
        data:{
        id_m_provinsi:id_m_provinsi
        },
        success: function (data) {
            $("#id_m_kota").html(data.html);
        }
    });
})

$('#id_m_kota').change(function(){
    var id_m_kota = $('#id_m_kota').val();
    $('#id_m_kecamatan').html('');
    $('#id_m_kelurahan').html('');
    $.ajax({
        type: 'post',
        url: '{{route('get_combo_kecamatan')}}',
        data:{
            id_m_kota:id_m_kota
        },
        success: function (data) {
            $("#id_m_kecamatan").html(data.html);
        }
    });
})

$('#id_m_kecamatan').change(function(){
    var id_m_kecamatan = $('#id_m_kecamatan').val();
    $('#id_m_kelurahan').html('');
    $.ajax({
        type: 'post',
        url: '{{route('get_combo_kelurahan')}}',
        data:{
            id_m_kecamatan:id_m_kecamatan
        },
        success: function (data) {
            $("#id_m_kelurahan").html(data.html);
        }
    });
})

</script>
@endsection
