@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">

                <form method="post" id="form">

                    <div class="row">
                        
                       
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="no_register">No Register</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly id="no_register" value="{{$old->no_register}}" class="form-control" name="no_register">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_pendaftar">Nama</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="hidden" id="id_m_pendaftar" value="{{$old->id_m_pendaftar}}" class="form-control" name="id_m_pendaftar">
                                <input type="text" readonly id="nm_m_pendaftar" value="{{$old->nm_m_pendaftar}}" class="form-control" name="nm_m_pendaftar">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="tgl_lahir_m_pendaftar">Tanggal Lahir</label>
                                </div>
                                <div class="col-sm-9">
                                    @php
                                        app()->setLocale('id');
                                    @endphp
                                <input type="text" readonly id="tgl_lahir_m_pendaftar" value="{{($old) ? \Carbon\Carbon::parse($old->tgl_lahir_m_pendaftar)->isoFormat('D MMMM YYYY') : ''}}" class="form-control" name="tgl_lahir_m_pendaftar">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="email_m_pendaftar">Email</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly id="email_m_pendaftar" value="{{$old->email_m_pendaftar}}" class="form-control" name="email_m_pendaftar">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_jadwal">Pelatihan yang diikuti</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly id="nm_m_jadwal" value="{{$old->jadwal->nm_m_jadwal}}" class="form-control" name="nm_m_jadwal">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="username_cbt">Username CBT</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly id="username_cbt" value="{{$username_cbt}}" class="form-control" name="username_cbt">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="password_cbt">Password CBT</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly id="password_cbt" value="{{$password_cbt}}" class="form-control" name="password_cbt">
                                </div>
                            </div>
                        </div>


                        
                        <div class="col-sm-9 offset-sm-3">
                            <a href="{{route('admin.m_pendaftar.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')

@endsection
