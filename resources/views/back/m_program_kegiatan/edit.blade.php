@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">

                        <input type="hidden" value="{{$old->id_m_program_kegiatan}}" name="id_m_program_kegiatan" id="id_m_program_kegiatan" />

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_program_kegiatan">Nama Program</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->nm_m_program_kegiatan}}" id="nm_m_program_kegiatan" placeholder="contoh : Program ABC" class="form-control" name="nm_m_program_kegiatan">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_sumber_dana">Sumber Dana</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="id_m_sumber_dana" name="id_m_sumber_dana">
                                        <option value="">-- pilih sumber dana --</option>
                                        @foreach ($sumber_dana as $item)
                                        <option {{($old->id_m_sumber_dana == $item->id_m_sumber_dana) ? 'selected' : ''}} value="{{$item->id_m_sumber_dana}}">{{$item->nm_m_sumber_dana}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="route">Tanggal Mulai</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="tgl_mulai" data-target-input="nearest">
                                        <input type="text" value="{{$old->tgl_mulai}}" name="tgl_mulai" class="form-control datetimepicker-input" data-target="#tgl_mulai"/>
                                        <div class="input-group-append" data-target="#tgl_mulai" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="route">Tanggal Selesai</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="tgl_selesai" data-target-input="nearest">
                                        <input type="text" value="{{$old->tgl_selesai}}" name="tgl_selesai" class="form-control datetimepicker-input" data-target="#tgl_selesai"/>
                                        <div class="input-group-append" data-target="#tgl_selesai" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="col-sm-9 offset-sm-3">
                        <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_program_kegiatan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_program_kegiatan.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});



 //Date picker
 $('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})
</script>
@endsection
