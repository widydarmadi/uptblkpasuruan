@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}} <a href="{{route('admin.m_program_kegiatan.add')}}" class="btn btn-success btn-sm float-right d-flex">Tambah Data</a></h5>
            
          </div>
          
          <div class="card-body">
              <table id="datatable" class="table-striped table-sm table-hover table table-bordered dt-responsive">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Nama Program</th>
                    <th>Tgl Mulai</th>
                    <th>Tgl Selesai</th>
                    <th>Sumber Dana</th>
                    <th>Updated at</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection


@section('js')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var table;

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.m_program_kegiatan.datatable') }}',
            method: 'post'
        },
    });
});


$('#datatable').on('click', '.delete', function(){
    var id_m_program_kegiatan = $(this).data("id_m_program_kegiatan");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_program_kegiatan.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_m_program_kegiatan:id_m_program_kegiatan},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});
</script>
@endsection
