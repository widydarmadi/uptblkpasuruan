@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_pegawai">Nama Pegawai</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text"  id="nm_m_pegawai" class="form-control" name="nm_m_pegawai">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jabatan_m_pegawai">Jabatan Pegawai</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text"  id="jabatan_m_pegawai" placeholder="contoh : Instruktur TIK" class="form-control" name="jabatan_m_pegawai">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="order_tampil">Urut Tampil (Web)</label>
                                </div>
                                <div class="col-sm-2">
                                <input type="number"  id="order_tampil" class="form-control" name="order_tampil">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="pangkat_m_pegawai">Pangkat Pegawai</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text"  id="pangkat_m_pegawai" placeholder="contoh : Penata Muda" class="form-control" name="pangkat_m_pegawai">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="pangkat_m_pegawai">Kategori Pegawai</label>
                                </div>
                                <div class="col-sm-3">
                                    <select name="kategori" class="form-control" id="kategori">
                                        <option value="">-- pilih kategori --</option>
                                        <option value="PEGAWAI UPT BLK PASURUAN">PEGAWAI UPT BLK PASURUAN</option>
                                        <option value="INSTRUKTUR PNS">INSTRUKTUR PNS</option>
                                        <option value="INSTRUKTUR PTT">INSTRUKTUR PTT</option>
                                        <option value="ASISTEN INSTRUKTUR">ASISTEN INSTRUKTUR</option>
                                         <option value="STRUKTURAL">STRUKTURAL</option>
                                        <option value="PELAKSANA">PELAKSANA</option>
                                        <option value="INSTRUKTUR PPPK">INSTRUKTUR PPPK</option>
                                        <option value="PTT">PTT</option>
                                        <option value="OUTSOURCING">OUTSOURCING</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="unit_kerja_m_pegawai">Unit Kerja</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text"  id="unit_kerja_m_pegawai" value="UPT Balai Latihan Kerja Pasuruan" class="form-control" name="unit_kerja_m_pegawai">
                                </div>
                            </div>
                        </div>
                    

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="golongan_m_pegawai">Golongan Pegawai</label>
                                </div>
                                <div class="col-sm-3">
                                <input type="text"  id="golongan_m_pegawai" placeholder="contoh : III / a" class="form-control" name="golongan_m_pegawai">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="detail_jabatan_m_pegawai">Uraian Jabatan</label>
                                </div>
                                <div class="col-sm-9">
                                <textarea id="detail_jabatan_m_pegawai" class="form-control" name="detail_jabatan_m_pegawai" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="kompetensi_m_pegawai">Kompetensi</label>
                                </div>
                                <div class="col-sm-9">
                                <textarea id="kompetensi_m_pegawai" class="form-control" name="kompetensi_m_pegawai" rows="3"></textarea>
                                </div>
                            </div>
                        </div>
                    

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="aktif_m_pegawai">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif_m_pegawai" name="aktif_m_pegawai">
                                        <option value="1">YA</option>
                                        <option value="0">TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        

                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_pegawai.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>

    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_pegawai.save") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});


</script>
@endsection
