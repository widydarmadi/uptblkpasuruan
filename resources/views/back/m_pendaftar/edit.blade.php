@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_pendaftar">Nama Lengkap</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="hidden" value="{{$old->id_m_pendaftar}}" id="id_m_pendaftar" class="form-control" name="id_m_pendaftar">
                                <input type="text" value="{{$old->nm_m_pendaftar}}" id="nm_m_pendaftar" class="form-control" name="nm_m_pendaftar">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nik_m_pendaftar">NIK</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="nik_m_pendaftar" value="{{$old->nik_m_pendaftar}}" class="form-control" name="nik_m_pendaftar">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_provinsi">Provinsi</label>
                                </div>
                                <div class="col-sm-6">
                                    @php
                                        $list_provinsi = \App\Models\M_provinsi::orderBy('nm_m_provinsi')->get();
                                    @endphp
                                    <select name="id_m_provinsi" id="id_m_provinsi" class="form-control select2">
                                        <option value="" class="value">-- pilih provinsi --</option>
                                        @foreach ($list_provinsi as $item)
                                            <option {{($id_m_provinsi == $item->id_m_provinsi) ? 'selected' : null}} value="{{$item->id_m_provinsi}}" class="value">{{$item->nm_m_provinsi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_kota">Pilih Kabupaten / Kota</label>
                                </div>
                                <div class="col-sm-6">
                                    <select name="id_m_kota" id="id_m_kota" class="form-control select2">
                                        <option value="" class="value">-- pilih provinsi terlebih dahulu --</option>
                                        @foreach ($list_kab as $item)
                                            <option {{($old->id_m_kota == $item->id_m_kota) ? 'selected' : null}} value="{{$item->id_m_kota}}" class="value">{{$item->nm_m_kota}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="alamat_m_pendaftar">Alamat Rumah</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="alamat_m_pendaftar" value="{{$old->alamat_m_pendaftar}}" name="alamat_m_pendaftar" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="jk_m_pendaftar">Jenis Kelamin</label>
                                </div>
                                <div class="col-sm-4">
                                    <select id="jk_m_pendaftar" name="jk_m_pendaftar" class="select form-control">
                                        <option value="">pilih salah satu</option>
                                        <option {{($old->jk_m_pendaftar == 'LAKI-LAKI') ? 'selected' : null}} value="LAKI-LAKI">LAKI-LAKI</option>
                                        <option {{($old->jk_m_pendaftar == 'PEREMPUAN') ? 'selected' : null}} value="PEREMPUAN">PEREMPUAN</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="id_m_pendidikan">Pendidikan Terakhir</label>
                                </div>
                                <div class="col-sm-4">
                                    @php
                                        $list_pendidikan = \App\Models\M_pendidikan::where('aktif_m_pendidikan','1')->get();
                                    @endphp
                                    
                                    <select name="id_m_pendidikan" id="id_m_pendidikan" class="form-control" required>
                                    @foreach ($list_pendidikan as $item)
                                        <option {{($old->id_m_pendidikan == $item->id_m_pendidikan) ? 'selected' : null}} value="{{$item->id_m_pendidikan}}">{{$item->nm_m_pendidikan}}</option>
                                    @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="nm_jurusan">Silahkan tulis Jurusan bagi yang lulusan SMA, SMK, D1, D3, dan S1</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="nm_jurusan" value="{{$old->nm_jurusan}}" name="nm_jurusan" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="tempat_lahir_m_pendaftar">Tempat, Tanggal Lahir</label>
                                </div>
                                <div class="col-sm-4">
                                    <input id="tempat_lahir_m_pendaftar" value="{{$old->tempat_lahir_m_pendaftar}}" name="tempat_lahir_m_pendaftar" type="text" placeholder="Tempat Lahir" class="form-control">
                                </div>
                                <div class="col-sm-4">
                                    <div class="input-group date" id="tgl_lahir_m_pendaftar" data-target-input="nearest">
                                        <input value="{{$old->tgl_lahir_m_pendaftar}}" placeholder="Tanggal Lahir" type="text" name="tgl_lahir_m_pendaftar" class="form-control datetimepicker-input" data-target="#tgl_lahir_m_pendaftar"/>
                                        <div class="input-group-append" data-target="#tgl_lahir_m_pendaftar" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="wa_m_pendaftar">No. HP (yang terdaftar No.WA Aktif)</label>
                                </div>
                                <div class="col-sm-4">
                                    <input id="wa_m_pendaftar" value="{{$old->wa_m_pendaftar}}" name="wa_m_pendaftar" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="email_m_pendaftar">Email yang aktif</label>
                                </div>
                                <div class="col-sm-6">
                                    <input id="email_m_pendaftar" value="{{$old->email_m_pendaftar}}" name="email_m_pendaftar" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="quest_pernah_blk">Pernah mengikuti kursus BLK ?</label>
                                </div>
                                <div class="col-sm-3">
                                    <select name="quest_pernah_blk" id="quest_pernah_blk" class="form-control" required>
                                        <option {{($old->quest_pernah_blk == 'BELUM') ? 'selected' : null}} value="BELUM">BELUM PERNAH</option>
                                        <option {{($old->quest_pernah_blk == 'PERNAH') ? 'selected' : null}} value="PERNAH">PERNAH</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="quest_masih_bekerja">Masih Bekerja ?</label>
                                </div>
                                <div class="col-sm-3">
                                    <select name="quest_masih_bekerja" id="quest_masih_bekerja" class="form-control" required>
                                        <option {{($old->quest_masih_bekerja == 'TIDAK') ? 'selected' : null}} value="TIDAK">TIDAK</option>
                                        <option {{($old->quest_masih_bekerja == 'YA') ? 'selected' : null}} value="YA">YA</option>
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="is_disabilitas">Apakah Penyandang Disabilitas ?</label>
                                </div>
                                <div class="col-sm-3">
                                    <select name="is_disabilitas" id="is_disabilitas" class="form-control">
                                        <option value="">-- pilih satu --</option>
                                        <option {{($old->is_disabilitas != 'YA') ? 'selected' : null}} value="TIDAK">TIDAK</option>
                                        <option {{($old->is_disabilitas == 'YA') ? 'selected' : null}} value="YA">YA</option>
                                      </select>
                                </div>
                            </div>
                        </div>

                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="id_m_disabilitas">Jika penyandang disabilitas, diisi jenis disabilitasnya</label>
                                </div>
                                <div class="col-sm-3">
                                    <select name="id_m_disabilitas" id="id_m_disabilitas" class="form-control">
                                        <option value="">-- pilih jenis disabilitas --</option>
                                        @if ($old->is_disabilitas == 'YA')
                                        @foreach ($list_disabilitas as $item)
                                        <option {{($old->id_m_disabilitas == $item->id_disabilitas) ? 'selected' : null}} value="{{$item->id_disabilitas}}">{{$item->nama_disabilitas}}</option>
                                        @endforeach    
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="tgl_daftar">Tanggal Daftar</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="tgl_daftar" data-target-input="nearest">
                                        <input type="text" name="tgl_daftar" value="{{\Carbon\Carbon::parse($old->created_at)->format('Y-m-d')}}" class="form-control datetimepicker-input" data-target="#tgl_daftar"/>
                                        <div class="input-group-append" data-target="#tgl_daftar" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="id_m_jadwal">Pilih pelatihan yang akan diikuti</label>
                                </div>
                                <div class="col-sm-9">
                                    <select name="id_m_jadwal" id="id_m_jadwal" class="form-control select2">
                                        <option value="" >-- pilih pelatihan --</option>

                                        @if($list_gelombang)
                                          @php
                                            app()->setLocale('id');
                                          @endphp
                                          @forelse ($list_gelombang as $item)
                                            @php
                                                $mulai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_mulai)))->isoFormat('D MMMM YYYY');
                                                $selesai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_selesai)))->isoFormat('D MMMM YYYY');
                                            @endphp
                                            <option {{($old->id_m_jadwal == $item->id_m_jadwal) ? 'selected' : null}} value="{{$item->id_m_jadwal}}">{{$item->nm_m_jadwal.' | '.$mulai.' s/d '.$selesai}}</option>
                                          @empty
                                            <option value="0">BELUM ADA JADWAL PELATIHAN DIBUKA</option>
                                          @endforelse
                                        @endif
                                        
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="quest_minat_setelah_kursus">Minat Sesudah Menyelesaikan Pelatihan</label>
                                </div>
                                <div class="col-sm-3">
                                    <select name="quest_minat_setelah_kursus" id="quest_minat_setelah_kursus" class="form-control" required>
                                        <option value="BEKERJA">BEKERJA</option>
                                        <option value="WIRASWASTA">WIRASWASTA</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <input placeholder="grup_id_cbt" type="hidden" value="{{$old->cbt_grup_id}}" id="cbt_grup_id" name="cbt_grup_id" />
                        <input placeholder="grup_id_cbt" type="hidden" value="PBK" id="cat" name="cat" />

                

                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_pendaftar.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
//Date picker
$('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})

$('#is_disabilitas').change(function(){
    var is_disabilitas = $('#is_disabilitas').val();
    $("#id_m_disabilitas").html('');
    if(is_disabilitas == 'YA'){
        $.ajax({
            type: 'post',
            url: '{{route('get_jenis_disabilitas')}}',
            data:{
                is_disabilitas:is_disabilitas
            },
            success: function (data) {
                $("#id_m_disabilitas").html(data.html);
            }
        });
    }
})


$('#id_m_provinsi').change(function(){
    var id_m_provinsi = $('#id_m_provinsi').val();
    $.ajax({
        type: 'post',
        url: '{{route('get_combo_kota')}}',
        data:{
        id_m_provinsi:id_m_provinsi
        },
        success: function (data) {
            $("#id_m_kota").html(data.html);
        }
    });
})

$('#id_m_jadwal').change(function(){
    var id_m_jadwal = $(this).val();
    $.ajax({
        type: 'post',
        url: '{{route('get_cbt_grup_id')}}',
        data:{
        id_m_jadwal:id_m_jadwal
        },
        success: function (data) {
        if(data.valuez){
            $("#cbt_grup_id").val(data.valuez);
        }else{
            $("#cbt_grup_id").val('');
        }
        }
    });
})


$(document).ready( function () {
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_pendaftar.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});


</script>
@endsection
