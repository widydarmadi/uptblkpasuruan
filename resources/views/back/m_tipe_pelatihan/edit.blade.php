@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">

                        <input type="hidden" value="{{$old->id_m_tipe_pelatihan}}" id="id_m_tipe_pelatihan" name="id_m_tipe_pelatihan">
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_tipe_pelatihan">Nama Tipe Pelatihan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="nm_m_tipe_pelatihan" value="{{$old->nm_m_tipe_pelatihan}}" placeholder="contoh : Swadaya" class="form-control" name="nm_m_tipe_pelatihan">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="deskripsi_m_tipe_pelatihan">Deskripsi Singkat</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_deskripsi_m_tipe_pelatihan">{!!$old->deskripsi_m_tipe_pelatihan!!}</div>
                                    <textarea style="display: none" id="deskripsi_m_tipe_pelatihan" class="form-control" name="deskripsi_m_tipe_pelatihan" rows="4">{{$old->deskripsi_m_tipe_pelatihan}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif_m_tipe_pelatihan">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif_m_tipe_pelatihan" name="aktif_m_tipe_pelatihan">
                                        <option {{($old->aktif_m_tipe_pelatihan=='1') ? 'selected' : ''}} value="1">YA</option>
                                        <option {{($old->aktif_m_tipe_pelatihan != '1') ? 'selected' : ''}} value="0">TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="gambar_m_tipe_pelatihan">Gambar / Icon</label>
                                </div>
                                <div class="col-sm-3">
                                <input type="file" id="gambar_m_tipe_pelatihan" class="form-control" name="gambar_m_tipe_pelatihan">
                                </div>
                            </div>
                        </div>

                        @if ($old->gambar_m_tipe_pelatihan)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="gambar_m_tipe_pelatihan"></label>
                                </div>
                                <div class="col-sm-3">
                                    <img src="{{asset('storage'.'/'.$old->gambar_m_tipe_pelatihan)}}" width="100%" />
                                    <br>
                                    <br>
                                    <a data-id_m_tipe_pelatihan="{{$old->id_m_tipe_pelatihan}}" class="resetfoto btn btn-sm btn-warning" href="javascript:void(0)">hapus foto</a>
                                </div>
                            </div>
                        </div>

                        @endif
                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="pdf_m_tipe_pelatihan">Narasi Berupa File PDF</label>
                                </div>
                                <div class="col-sm-3">
                                <input type="file" id="pdf_m_tipe_pelatihan" class="form-control" name="pdf_m_tipe_pelatihan">
                                </div>
                            </div>
                        </div>

                        @if ($old->pdf_m_tipe_pelatihan)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="pdf_m_tipe_pelatihan"></label>
                                </div>
                                <div class="col-sm-3">
                                    <a target="_blank" class="btn btn-sm btn-info" href="{{asset('storage'.'/'.$old->pdf_m_tipe_pelatihan)}}">lihat file</a>
                                    <br>
                                    <br>
                                    <a data-id_m_tipe_pelatihan="{{$old->id_m_tipe_pelatihan}}" class="resetpdf btn btn-sm btn-warning" href="javascript:void(0)">hapus file pdf</a>
                                </div>
                            </div>
                        </div>

                        @endif



                        <div class="col-sm-9 offset-sm-3">
                        <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_tipe_pelatihan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>

var editor = CKEDITOR.replace( 'div_deskripsi_m_tipe_pelatihan' );
editor.on( 'change', function( evt ) {
    $('#deskripsi_m_tipe_pelatihan').html(evt.editor.getData());
});

    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_tipe_pelatihan.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });


    $(".resetfoto").click(function(){
        var id_m_tipe_pelatihan = $(this).data("id_m_tipe_pelatihan");
        swal.fire({
            title: "Confirmation",
            text: 'Apakah Anda yakin ingin menghapus file gambar ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            cancelButtonText: "Cancel",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){
                $.ajax({
                    url:"{{ route('admin.m_tipe_pelatihan.reset') }}",
                    method:"post",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        id_m_tipe_pelatihan:id_m_tipe_pelatihan,
                        edit:{{request()->get('id_m_tipe_pelatihan')}}
                    },
                    success:function(data)
                    {
                        if(data.status == true){
                            swal.fire({
                                title: "Sukses!",
                                text: 'Gambar latar telah dikembalikan ke mode default',
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }

        })
    })
    
    
    $(".resetpdf").click(function(){
        var id_m_tipe_pelatihan = $(this).data("id_m_tipe_pelatihan");
        swal.fire({
            title: "Confirmation",
            text: 'Apakah Anda yakin ingin menghapus file pdf ini ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            cancelButtonText: "Cancel",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){
                $.ajax({
                    url:"{{ route('admin.m_tipe_pelatihan.reset_pdf') }}",
                    method:"post",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{
                        id_m_tipe_pelatihan:id_m_tipe_pelatihan,
                        edit:{{request()->get('id_m_tipe_pelatihan')}}
                    },
                    success:function(data)
                    {
                        if(data.status == true){
                            swal.fire({
                                title: "Sukses!",
                                text: 'Gambar latar telah dikembalikan ke mode default',
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }

        })
    })

            
    $('#id_m_kategori_kejuruan').change(function(){
        var id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
        $.ajax({
            type: 'post',
            url: '{{route('get_combo_kejuruan')}}',
            data:{
            id_m_kategori_kejuruan:id_m_kategori_kejuruan
            },
            success: function (data) {
                $("#id_m_kejuruan").html(data.html);
            }
        });
    })

        
    });

</script>
@endsection
