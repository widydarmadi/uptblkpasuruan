@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_posisi_m_lowongan">Posisi yang dibutuhkan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="nm_posisi_m_lowongan" class="form-control" name="nm_posisi_m_lowongan">
                                </div>
                            </div>
                        </div>

                 
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_perusahaan">Pilih Perusahaan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="id_m_perusahaan" name="id_m_perusahaan">
                                        <option value="">-- pilih perusahaan --</option>
                                        @foreach ($perusahaan as $item)
                                        <option value="{{$item->id_m_perusahaan}}">{{$item->nm_m_perusahaan}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo">Pilih Foto Pendukung (ekstensi gambar)</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="photo" name="photo" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="route">Batas Awal Lowongan</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="date_start" data-target-input="nearest">
                                        <input type="text" name="date_start" class="form-control datetimepicker-input" data-target="#date_start"/>
                                        <div class="input-group-append" data-target="#date_start" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="route">Batas Akhir Lowongan</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="date_end" data-target-input="nearest">
                                        <input type="text" name="date_end" class="form-control datetimepicker-input" data-target="#date_end"/>
                                        <div class="input-group-append" data-target="#date_end" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="deskripsi_m_lowongan">Deskripsi Lowongan</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_deskripsi_m_lowongan"></div>
                                    <textarea style="display: none" id="deskripsi_m_lowongan" class="form-control" name="deskripsi_m_lowongan" rows="4"></textarea>
                                </div>
                            </div>
                        </div>
                        
                      

                        <div class="col-sm-9 offset-sm-3">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_info_lowongan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>

var editor = CKEDITOR.replace( 'div_deskripsi_m_lowongan' );
editor.on( 'change', function( evt ) {
    $('#deskripsi_m_lowongan').html(evt.editor.getData());
});


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_info_lowongan.save") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});

 //Date picker
 $('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})

</script>
@endsection
