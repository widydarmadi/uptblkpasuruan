@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">

                <form method="post" id="form">

                    <div class="row">
                        <input type="hidden" id="id_m_bg" value="{{$old->id_m_bg}}" class="form-control" name="id_m_bg">
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_bg">Nama Halaman</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly value="{{$old->nm_m_bg}}" id="nm_m_bg" class="form-control" name="nm_m_bg">
                                </div>
                            </div>
                        </div>

                 
             
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo">Foto Untuk Background Header
                                </label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="photo" name="photo" class="form-control" />
                                </div>
                                <div class="col-sm-6">
                                    <p class="text-info">Resolusi ideal adalah : 1900 x 500 pixel (landscape)</p>
                                </div>
                            </div>
                        </div>

                        @if ($old->photo)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo"></label>
                                </div>
                                <div class="col-sm-3">
                                    <img src="{{asset('storage'.'/'.$old->photo)}}" width="100%" />
                                    <br>
                                    <br>
                                    <a data-id_m_bg="{{$old->id_m_bg}}" class="resetfoto btn btn-sm btn-warning" href="javascript:void(0)">Kembalikan ke default</a>
                                </div>
                            </div>
                        </div>

                        @endif

                        
                        <div class="col-sm-9 offset-sm-3">
                            <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.m_bg.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
    $(".resetfoto").click(function(){
        var id_m_bg = $(this).data("id_m_bg");
        swal.fire({
            title: "Confirmation",
            text: 'Apakah Anda yakin ingin mengembalikan background halaman ini menjadi default ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "OK",
            cancelButtonText: "Cancel",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){
                $.ajax({
                    url:"{{ route('admin.m_bg.reset') }}",
                    method:"post",
                    headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                    data:{id_m_bg:id_m_bg},
                    success:function(data)
                    {
                        if(data.status == true){
                            swal.fire({
                                title: "Sukses!",
                                text: 'Background telah dikembalikan ke mode default',
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }

        })
    })


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_bg.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});



 //Date picker
 $('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})
</script>
@endsection
