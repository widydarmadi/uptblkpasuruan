@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="alamat_kantor">Alamat Kantor</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_alamat_kantor">{!!$old->alamat_kantor!!}</div>
                                    <textarea style="display: none" id="alamat_kantor" class="form-control" name="alamat_kantor" rows="4">{{$old->alamat_kantor}}</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jam_kerja">Alamat Kantor</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_jam_kerja">{!!$old->jam_kerja!!}</div>
                                    <textarea style="display: none" id="jam_kerja" class="form-control" name="jam_kerja" rows="4">{{$old->jam_kerja}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="ig">Nama Akun Instagram</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_ig">{!!$old->ig!!}</div>
                                    <textarea style="display: none" id="ig" class="form-control" name="ig" rows="2">{{$old->ig}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="youtube">Nama Kanal Youtube</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->youtube}}" placeholder="contoh : UPT BALAI LATIHAN KERJA PASURUAN" id="youtube"  class="form-control" name="youtube">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="telp">Telepon / Whatsapp</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->telp}}" id="telp"  class="form-control" name="telp">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="email">Alamat Email</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" value="{{$old->email}}" id="email"  class="form-control" name="email">
                                </div>
                            </div>
                        </div>
                        

                        <div class="col-sm-9 offset-sm-3">
                            <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>

var editor = CKEDITOR.replace( 'div_alamat_kantor' );
editor.on( 'change', function( evt ) {
    $('#alamat_kantor').html(evt.editor.getData());
});
var editor2 = CKEDITOR.replace( 'div_ig' );
editor2.on( 'change', function( evt ) {
    $('#ig').html(evt.editor.getData());
});
var editor3 = CKEDITOR.replace( 'div_jam_kerja' );
editor3.on( 'change', function( evt ) {
    $('#jam_kerja').html(evt.editor.getData());
});


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_teks_footer.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
    });

</script>
@endsection
