@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">

                <form method="post" id="form">

                    <div class="row">
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="judul_m_slider">Judul Slider (Teks Kecil)</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="hidden" id="id_m_slider" value="{{$old->id_m_slider}}" class="form-control" name="id_m_slider">
                                <input type="text" id="judul_m_slider" value="{{$old->judul_m_slider}}" class="form-control" name="judul_m_slider">
                                </div>
                            </div>
                        </div>

                        

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="deskripsi_m_slider">Deskripsi Slider (Teks Besar)</label>
                                </div>
                                <div class="col-sm-9">
                                    <textarea class="form-control" id="deskripsi_m_slider" name="deskripsi_m_slider"> {{$old->deskripsi_m_slider}}</textarea>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="aktif_m_slider">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="aktif_m_slider" name="aktif_m_slider">
                                        <option value="1" {{($old->aktif_m_slider == '1') ? 'selected' : ''}}>YA</option>
                                        <option value="0" {{($old->aktif_m_slider != '1') ? 'selected' : ''}}>TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo">Pilih Background (Full Screen)</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="photo" name="photo" class="form-control" />
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo"></label>
                                </div>
                                <div class="col-sm-3">
                                    <img src="{{asset('storage'.'/'.$old->photo)}}" width="100%" />
                                </div>
                            </div>
                        </div>
                        
                        {{-- <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="foto_kanan">Pilih Foto Kanan (Pendukung)</label>
                                </div>
                                <div class="col-sm-3">
                                    <input type="file" id="foto_kanan" name="foto_kanan" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="photo"></label>
                                </div>
                                <div class="col-sm-3">
                                    <img src="{{asset('storage'.'/'.$old->foto_kanan)}}" width="100%" />
                                </div>
                            </div>
                        </div> --}}


                        <div class="col-sm-9 offset-sm-2">
                        <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_slider.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_slider.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
    });

</script>
@endsection
