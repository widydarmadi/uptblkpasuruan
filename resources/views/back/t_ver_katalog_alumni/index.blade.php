@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}}</h5>
          </div>
          <div class="card-body">

            <table id="datatable" class="table table-sm table-bordered table-hover table-sm table-striped" width="100%">
              <thead>
                  <tr>
                      <th>No</th>
                      <th>Pemilik Usaha</th>
                      <th>Nama Produk / Jasa</th>
                      <th>Tipe</th>
                      <th>Kategori</th>
                      <th>Bidang Usaha</th>
                      <th>Alamat</th>
                      <th>Tampil Katalog</th>
                      <th>#</th>
                  </tr>
              </thead>

              <tbody>
              
              </tbody>
          </table>

          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection


@section('js')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var table;

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.t_ver_katalog_alumni.datatable') }}',
            method: 'post'
        },
    });
});


$('#datatable').on('click', '.delete', function(){
    var id_t_usaha = $(this).data("id_t_usaha");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.t_ver_katalog_alumni.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_t_usaha:id_t_usaha},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});



</script>
@endsection
