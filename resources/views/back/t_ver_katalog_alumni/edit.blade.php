@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">

                <form method="post" id="form">

                    <div class="row">
                        <input type="hidden" id="id_t_usaha" value="{{$old->id_t_usaha}}" class="form-control" name="id_t_usaha">
                        

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="nm_t_usaha" class="control-label">Nama Produk Barang / Jasa</label> 
                                </div>
                                <div class="col-sm-9">
                                    <input id="nm_t_usaha" value="{{$old->nm_t_usaha}}" name="nm_t_usaha" type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="is_tampil" class="control-label">Tampilkan ke Katalog Alumni</label> 
                                </div>
                                <div class="col-sm-3">
                                    <select id="is_tampil" name="is_tampil" class="form-control">
                                        <option value="">-- pilih opsi --</option>
                                        <option value="YA" {{($old->is_tampil == 'YA') ? 'selected' : null}}>YA</option>
                                        <option value="TIDAK" {{($old->is_tampil != 'YA') ? 'selected' : null}}>TIDAK</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="kategori_t_usaha" class="control-label">Kategori Produk</label> 
                                </div>
                                <div class="col-sm-3">
                                    <select id="kategori_t_usaha" name="kategori_t_usaha" class="form-control">
                                        <option value="">-- pilih kategori produk --</option>
                                        <option value="BARANG" {{($old->kategori_t_usaha == 'BARANG') ? 'selected' : null}}>BARANG</option>
                                        <option value="JASA" {{($old->kategori_t_usaha == 'JASA') ? 'selected' : null}}>JASA</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="tipe_t_usaha" class="control-label">Tipe Operasional</label> 
                                </div>
                                <div class="col-sm-3">
                                    <select id="tipe_t_usaha" name="tipe_t_usaha" class="form-control">
                                        <option value="">-- pilih tipe operasional --</option>
                                        <option value="OFFLINE" {{($old->tipe_t_usaha == 'OFFLINE') ? 'selected' : null}}>OFFLINE</option>
                                        <option value="ONLINE" {{($old->tipe_t_usaha == 'ONLINE') ? 'selected' : null}}>ONLINE</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="id_m_bidang_usaha" class="control-label">Bidang Usaha</label> 
                                </div>
                                <div class="col-sm-5">
                                    <select id="id_m_bidang_usaha" name="id_m_bidang_usaha" class="form-control">
                                        <option value="">-- pilih bidang usaha --</option>
                                        @foreach ($bidang_usaha as $item)
                                        <option {{($old->id_m_bidang_usaha == $item->id_m_bidang_usaha) ? 'selected' : null}} value="{{$item->id_m_bidang_usaha}}">{{$item->nm_m_bidang_usaha}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="alamat_t_usaha" class="control-label">Alamat Usaha</label> 
                                </div>
                                <div class="col-sm-9">
                                    <input id="alamat_t_usaha" value="{{$old->alamat_t_usaha}}" name="alamat_t_usaha" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="telp_t_usaha" class="control-label">Nomor Telepon (HP) Usaha</label> 
                                </div>
                                <div class="col-sm-5">
                                    <input id="telp_t_usaha" value="{{$old->telp_t_usaha}}" name="telp_t_usaha" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="email_t_usaha" class="control-label">Email</label> 
                                </div>
                                <div class="col-sm-5">
                                    <input id="email_t_usaha" value="{{$old->email_t_usaha}}" name="email_t_usaha" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="ig_t_usaha" class="control-label">Akun Instagram</label> 
                                </div>
                                <div class="col-sm-5">
                                    <input id="ig_t_usaha" value="{{$old->ig_t_usaha}}" name="ig_t_usaha" type="text" placeholder="contoh : @usahaviral" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="id_m_provinsi" class="control-label">Provinsi</label> 
                                </div>
                                <div class="col-sm-5">
                                    <select id="id_m_provinsi" name="id_m_provinsi" class="form-control">
                                        <option value="">-- pilih provinsi --</option>
                                        @foreach ($list_prov as $item)
                                        <option {{($old->id_m_provinsi == $item->id_m_provinsi) ? 'selected' : null}} value="{{$item->id_m_provinsi}}">{{$item->nm_m_provinsi}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="id_m_kota" class="control-label">Kabupaten / Kota</label> 
                                </div>
                                <div class="col-sm-5">
                                    <select id="id_m_kota" name="id_m_kota" class="form-control">
                                        @foreach ($list_kab as $item)
                                            <option {{($old->id_m_kota == $item->id_m_kota) ? 'selected' : null}} value="{{$item->id_m_kota}}" class="value">{{$item->nm_m_kota}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="id_m_kecamatan" class="control-label">Kecamatan</label> 
                                </div>
                                <div class="col-sm-5">
                                    <select id="id_m_kecamatan" name="id_m_kecamatan" class="form-control">
                                        @foreach ($list_kec as $item)
                                            <option {{($old->id_m_kecamatan == $item->id_m_kecamatan) ? 'selected' : null}} value="{{$item->id_m_kecamatan}}" class="value">{{$item->nm_m_kecamatan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label for="foto_t_usaha" class="control-label">Unggah Foto Promosi Usaha / Flyer / Brosur</label> 
                                </div>
                                <div class="col-sm-5">
                                    <input type="file" id="foto_t_usaha" name="foto_t_usaha" class="form-control" />
                                </div>
                            </div>
                        </div>

                        @if ($old->foto_t_usaha)
                            
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="foto_t_usaha"></label>
                                </div>
                                <div class="col-sm-3">
                                    <img src="{{asset('storage'.'/'.$old->foto_t_usaha)}}" width="100%" />
                                    <br>
                                    <br>
                                    <a data-id_t_usaha="{{$old->id_t_usaha}}" class="resetfoto btn btn-sm btn-warning" href="javascript:void(0)">hapus foto</a>
                                </div>
                            </div>
                        </div>

                        @endif

                   
                        
                        <div class="col-sm-9 offset-sm-3">
                            <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Simpan Perubahan</span></button>
                            <a href="{{route('admin.t_ver_katalog_alumni.index')}}" class="btn btn-secondary waves-effect">Kembali</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
    $('#id_m_provinsi').change(function(){
        $('#id_m_kota').val('');
        $('#id_m_kecamatan').val('');
        var id_m_provinsi = $(this).val();
        $.ajax({
            url:"{{ route("get_combo_kota") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_provinsi:id_m_provinsi
            },
            success:function(data)
            {
                $('#id_m_kota').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })
    
    $('#id_m_kota').change(function(){
        var id_m_kota = $(this).val();
        $('#id_m_kecamatan').val('');
        $.ajax({
            url:"{{ route("get_combo_kecamatan") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_kota:id_m_kota
            },
            success:function(data)
            {
                $('#id_m_kecamatan').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })


$(".resetfoto").click(function(){
    var id_t_usaha = $(this).data("id_t_usaha");
    swal.fire({
        title: "Confirmation",
        text: 'Apakah Anda yakin ingin menghapus file gambar brosur / flyer ?',
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.t_ver_katalog_alumni.reset_foto') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id_t_usaha:id_t_usaha,
                    edit:{{request()->get('id_t_usaha')}}
                },
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Sukses!",
                            text: 'Gambar brosur telah dihapus',
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

    })
})


$("#form").submit(function(){
    $(".text-danger").remove();
    event.preventDefault();
    var data = new FormData($('#form')[0]);
    $("#submitform").attr('disabled', true);
    $("#submitform span").text(loading_text);

    $.ajax({
        url:"{{ route("admin.t_ver_katalog_alumni.update") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: data,
        processData: false,
        contentType: false,
        success:function(data)
        {
            if($.isEmptyObject(data.error)){
                if(data.status == true){
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Simpan');
                    $("form").each(function() { this.reset() });
                    swal.fire({
                        title: "Success",
                        text: data.message,
                        icon: "success"
                    }).then(function() {
                        location.href = data.redirect;
                    });
                }else{
                    displayWarningSwal(data.message);
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Simpan');
                }

            }else{
                displayWarningSwal();
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Simpan');
                $.each(data.error, function(key, value) {
                    var element = $("#" + key);
                    element.closest("div.form-control")
                    .removeClass("text-danger")
                    .addClass(value.length > 0 ? "text-danger" : "")
                    .find("#error_" + key).remove();
                    element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                });
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});
</script>
@endsection
