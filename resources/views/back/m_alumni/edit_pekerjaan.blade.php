@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_alumni">Nama Lengkap</label>
                                </div>
                                <div class="col-sm-9">
                                    <input type="hidden" value="{{$old->id_m_alumni}}" id="id_m_alumni" class="form-control" name="id_m_alumni">
                                    <input type="text" readonly value="{{$old->nm_m_alumni}}" id="nm_m_alumni" class="form-control" name="nm_m_alumni">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nik_m_alumni">NIK</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" readonly value="{{$old->nik_m_alumni}}"  id="nik_m_alumni" class="form-control" name="nik_m_alumni">
                                </div>
                            </div>
                        </div>
                        
                        
                        
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="alamat_m_alumni">Alamat Rumah</label>
                                </div>
                                <div class="col-sm-9">
                                    <input id="alamat_m_alumni" readonly value="{{$old->alamat_m_alumni}}" name="alamat_m_alumni" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                    <label class="col-form-label" for="jk_m_alumni">Jenis Kelamin</label>
                                </div>
                                <div class="col-sm-4">
                                    <input id="jk_m_alumni" readonly value="{{$old->jk_m_alumni}}" name="jk_m_alumni" type="text" class="form-control">
                                </div>
                            </div>
                        </div>

                        <div class="col-sm-9">
                            <a href="{{route('admin.m_alumni.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
                <hr>
                <br />
                <a href="#" class="btn btn-success" id="add_modal">Tambah Data Pekerjaan</a> 
                <br />
                <br />
                <table id="datatable" class="table table-bordered table-hover table-sm table-striped" width="100%">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Instansi</th>
                            <th>Bidang Usaha</th>
                            <th>Mulai Kerja</th>
                            <th>Jabatan</th>
                            <th>No Telp Instansi</th>
                            <th>Hapus ?</th>
                        </tr>
                    </thead>

                    <tbody>
                    
                    </tbody>
                </table>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>




<!-- Modal Hak Akses Per Module -->
<div
  class="modal fade"
  id="modal_pekerjaan"
  tabindex="-1"
  aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true"
>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Data Pekerjaan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>

@endsection

@section('js')
<script>

$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});

var table;
var id_m_alumni = $('#id_m_alumni').val();

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 10,
        ajax: {
            url: '{{ route('admin.m_alumni.datatable_pekerjaan') }}',
            method: 'post',
            data: function(q){
                q.id_m_alumni = id_m_alumni
            }
        },
    });
});

$('#add_modal').click(function(){
    $.ajax({
        url:"{{ route("admin.m_alumni.add_pekerjaan") }}",
        method:"post",
        data:{
            id_m_alumni: {{$old->id_m_alumni}}
        },
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        success:function(data)
        {
            $('.modal-body').html(data);
            $('#modal_pekerjaan').modal('show');
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
})



$(document).ready( function () {
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_alumni.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});


$('#datatable').on('click', '.delete', function(){
    var id_t_pekerjaan = $(this).data("id_t_pekerjaan");
    var id_m_alumni = $(this).data("id_m_alumni");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "YA",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_alumni.delete_pekerjaan') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id_m_alumni:id_m_alumni,
                    id_t_pekerjaan:id_t_pekerjaan
                },
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});


//Date picker
$('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})

</script>
@endsection
