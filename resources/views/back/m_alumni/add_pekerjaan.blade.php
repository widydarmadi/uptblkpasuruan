<form method="POST" id="form2">
    <div class="form-group row">
        
        <input type="hidden" name="id_m_alumni" id="id_m_alumni" value="{{$id_m_alumni}}" />

        <div class="col-md-12" style="margin-bottom: 15px;">
          <label for="nm_t_pekerjaan" class="control-label">Masukkan Nama Instansi / Lembaga Nama Perusahaan</label> 
          <input id="nm_t_pekerjaan" name="nm_t_pekerjaan" type="text" class="form-control">
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="id_m_bidang_usaha" class="control-label">Bidang Usaha</label> 
            <select id="id_m_bidang_usaha" name="id_m_bidang_usaha" class="form-control">
                <option value="">-- pilih bidang usaha --</option>
                @foreach ($bidang_usaha as $item)
                <option value="{{$item->id_m_bidang_usaha}}">{{$item->nm_m_bidang_usaha}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="jabatan_t_pekerjaan" class="control-label">Jabatan</label> 
            <input id="jabatan_t_pekerjaan" name="jabatan_t_pekerjaan" type="text" class="form-control">
        </div>
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="tgl_mulai_bekerja" class="control-label">Tanggal Mulai Bekerja</label> 
            <div class="input-group date" id="tgl_mulai_bekerja" data-target-input="nearest">
                <input type="text" readonly name="tgl_mulai_bekerja" class="form-control datetimepicker-input" data-target="#tgl_mulai_bekerja"/>
                <div class="input-group-append" data-target="#tgl_mulai_bekerja" data-toggle="datetimepicker">
                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                </div>
            </div>
        </div>
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="telp_t_pekerjaan" class="control-label">Nomor Telepon Kantor</label> 
            <input id="telp_t_pekerjaan" name="telp_t_pekerjaan" type="text" class="form-control">
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="alamat_t_pekerjaan" class="control-label">Alamat Kantor</label> 
            <input id="alamat_t_pekerjaan" name="alamat_t_pekerjaan" type="text" class="form-control">
        </div>
        
        
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="id_m_provinsi" class="control-label">Provinsi</label> 
            <select id="id_m_provinsi" name="id_m_provinsi" class="form-control">
                <option value="">-- pilih provinsi --</option>
                @foreach ($id_m_provinsi as $item)
                <option value="{{$item->id_m_provinsi}}">{{$item->nm_m_provinsi}}</option>
                @endforeach
            </select>
        </div>
        
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="id_m_kota" class="control-label">Kabupaten / Kota</label> 
            <select id="id_m_kota" name="id_m_kota" class="form-control">
                <option value="">-- pilih kabupaten / kota --</option>
            </select>
        </div>
        
        
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="id_m_kecamatan" class="control-label">Kecamatan</label> 
            <select id="id_m_kecamatan" name="id_m_kecamatan" class="form-control">
                <option value="">-- pilih kecamatan --</option>
            </select>
        </div>


        <div class="col-md-12">
          <br />
            <button type="submit" name="submitform" id="submitform" class="btn btn-success">Simpan</button>

        </div>
      </div>
</form>


<script>
    $('#id_m_provinsi').change(function(){
        $('#id_m_kota').val('');
        $('#id_m_kecamatan').val('');
        var id_m_provinsi = $(this).val();
        $.ajax({
            url:"{{ route("get_combo_kota") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_provinsi:id_m_provinsi
            },
            success:function(data)
            {
                $('#id_m_kota').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })
    
    $('#id_m_kota').change(function(){
        var id_m_kota = $(this).val();
        $('#id_m_kecamatan').val('');
        $.ajax({
            url:"{{ route("get_combo_kecamatan") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_kota:id_m_kota
            },
            success:function(data)
            {
                $('#id_m_kecamatan').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })


$("#form2").submit(function(){
    $(".text-danger").remove();
    event.preventDefault();
    var data = new FormData($('#form2')[0]);
    $("#submitform").attr('disabled', true);
    $("#submitform span").text(loading_text);

    $.ajax({
        url:"{{ route("admin.m_alumni.add_pekerjaan_post") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: data,
        processData: false,
        contentType: false,
        success:function(data)
        {
            if($.isEmptyObject(data.error)){
                if(data.status == true){
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Simpan');
                    $("form").each(function() { this.reset() });
                    swal.fire({
                        title: "Success",
                        text: data.message,
                        icon: "success"
                    }).then(function() {
                        location.href = data.redirect;
                    });
                }else{
                    displayWarningSwal(data.message);
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Simpan');
                }

            }else{
                displayWarningSwal();
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Simpan');
                $.each(data.error, function(key, value) {
                    var element = $("#" + key);
                    element.closest("div.form-control")
                    .removeClass("text-danger")
                    .addClass(value.length > 0 ? "text-danger" : "")
                    .find("#error_" + key).remove();
                    element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                });
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});

 //Date picker
 $('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})

</script>