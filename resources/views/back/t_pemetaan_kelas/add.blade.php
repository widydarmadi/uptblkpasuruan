@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_program_pelatihan">Program Pelatihan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="id_m_program_pelatihan" name="id_m_program_pelatihan">
                                        <option value="">-- pilih program pelatihan --</option>
                                        @foreach ($id_m_program_pelatihan as $item)
                                            <option value="{{$item->id_m_program_pelatihan}}">{{$item->nm_m_program_pelatihan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_jadwal">Jadwal / Gelombang Pelatihan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="id_m_jadwal" name="id_m_jadwal">
                                        <option value="">-- pilih jadwal pelatihan --</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nama_kelas">Nama Kelas</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="nama_kelas" placeholder="contoh format : NAMA_PELATIHAN - GELOMBANG - KODE_KELAS" class="form-control" name="nama_kelas">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="status">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="status" name="status">
                                        <option value="1">YA</option>
                                        <option value="0">TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>

                        <hr />
                        

                        <div class="col-sm-12">
                            <div id="list_siswa">
                                
                            </div>
                        </div>


                        <div class="col-sm-9">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.t_pemetaan_kelas.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
$('#id_m_program_pelatihan').change(function(){
    var id_m_program_pelatihan = $(this).val();
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas.load_jadwal") }}",
        method:"POST",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: {
            id_m_program_pelatihan:id_m_program_pelatihan
        },
        success:function(data)
        {
            if(data.status == true){
                $('#id_m_jadwal').html(data.html);
            }else{
                displayErrorSwal(data.message);
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});

$('#id_m_jadwal').change(function(){
    var id_m_jadwal = $(this).val();
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas.list_siswa") }}",
        method:"POST",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: {
            id_m_jadwal:id_m_jadwal,
            pagetype:'add'
        },
        success:function(data)
        {
            $('#list_siswa').html(data);
            
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
    
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas.load_nama_jadwal") }}",
        method:"POST",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: {
            id_m_jadwal:id_m_jadwal,
        },
        success:function(data)
        {
            $('#nama_kelas').val('Kelas ' + data);
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});

  
var tmp = [];
$('#list_siswa').on('change', '#all', function(){
    var checked = $('.pilih_siswa').val();
    $('.pilih_siswa').prop('checked', this.checked);
        
    if ($(this).is(':checked')) {
        tmp.push(checked);
    }else{
        tmp.splice($.inArray(checked, tmp),1);
    }
    console.log(tmp);
});

$('#list_siswa').on('change', '.pilih_siswa', function(){
    var checked = $(this).val();
        
    if ($(this).is(':checked')) {
        tmp.push(checked);
    }else{
        tmp.splice($.inArray(checked, tmp),1);
    }
    console.log(tmp);
});


$(document).ready( function () {
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        data.append("tmp", tmp);
        data.append('tmp', JSON.stringify(tmp));

        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);


        swal.fire({
            title: "Pesan Konfirmasi",
            text: 'Apakah Anda yakin untuk menambahkan kelas baru ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){

                $.ajax({
                    url:"{{ route("admin.t_pemetaan_kelas.save") }}",
                    method:"POST",
                    headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                    data: data,
                    processData: false,
                    contentType: false,
                    success:function(data)
                    {
                        if($.isEmptyObject(data.error)){

                            if(data.status == true){
                                $("#submitform").removeAttr('disabled');
                                $("#submitform span").text('Submit');
                                $("form").each(function() { this.reset() });
                                swal.fire({
                                    title: "Success",
                                    text: data.message,
                                    icon: "success"
                                }).then(function() {
                                    location.href = data.redirect;
                                });
                            }else{
                                displayErrorSwal(data.message);
                            }

                        }else{
                            displayWarningSwal();
                            $("#submitform").removeAttr('disabled');
                            $("#submitform span").text('Submit');
                            $.each(data.error, function(key, value) {
                                var element = $("#" + key);
                                element.closest("div.form-control")
                                .removeClass("text-danger")
                                .addClass(value.length > 0 ? "text-danger" : "")
                                .find("#error_" + key).remove();
                                element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                            });
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }else{
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Submit');
            }
        })
    });
});




</script>
@endsection
