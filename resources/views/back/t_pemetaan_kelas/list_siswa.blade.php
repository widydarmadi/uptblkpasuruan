<div class="col-12">
    <div class="mb-3 row">
        <div class="col-sm-12">
            <div class="alert bg-info">
                <ul>
                    <li>Silahkan memilih nama-nama peserta yang akan menempati kelas pelatihan tersebut</li>
                    <li>Jumlah peserta dalam satu kelas yang dipilih maksimal 16 (enam belas) peserta</li>
                </ul>
            </div>
            
            <table style="width: 100%" class="table table-striped table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" style="margin-left: 0px; position:relative;" class="form-check-input" name="all" id="all" /> </th>
                        <th>Reg</th>
                        <th>NIK</th>
                        <th>NAMA</th>
                        <th>EMAIL</th>
                        <th>NO HP / WA</th>
                        <th>NILAI TES</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($list_siswa as $item)
                    <tr>
                        <td><input type="checkbox" {{($item->seleksi->peserta) ? 'checked' : ''}} value="{{$item->id_m_pendaftar}}" style="margin-left: 0px; position:relative;" class="form-check-input pilih_siswa" name="pilih_siswa[]" /></td>
                        <td>{{$item->no_register}}</td>
                        <td>{{$item->nik_m_pendaftar}}</td>
                        <td>{{$item->nm_m_pendaftar}}</td>
                        <td>{{$item->email_m_pendaftar}}</td>
                        <td>{{$item->wa_m_pendaftar}}</td>
                        <td>{{number_format($item->seleksi->total_nilai_keseluruhan, 2, ',', '.')}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center">Tidak ada data peserta</td>
                    </tr>
                    @endforelse
                    
                </tbody>
            </table>
        </div>
    </div>
</div>