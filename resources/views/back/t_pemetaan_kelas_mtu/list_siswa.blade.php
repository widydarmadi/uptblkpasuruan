<div class="col-12">
    <div class="mb-3 row">
        <div class="col-sm-12">
            <div class="alert bg-info">
                <ul>
                    <li>Silahkan memilih nama-nama peserta yang akan menempati kelas pelatihan tersebut</li>
                    <li>Jumlah peserta dalam satu kelas yang dipilih maksimal 16 (enam belas) peserta</li>
                </ul>
            </div>
            <table style="width: 100%" class="table table-striped table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" style="margin-left: 0px; position:relative;" class="form-check-input" name="all" id="all" /> </th>
                        <th>NIK</th>
                        <th>NAMA</th>
                        <th>ALAMAT</th>
                        <th>JENIS KELAMIN</th>
                        <th>TINGKAT PENDIDIKAN</th>
                        <th>JURUSAN</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($list_siswa as $item)
                    <tr>
                        <td><input type="checkbox" {{($item->peserta_noninst) ? 'checked' : ''}} value="{{$item->id_t_siswa_noninst}}" style="margin-left: 0px; position:relative;" class="form-check-input pilih_siswa" name="pilih_siswa[]" /></td>
                        <td>{{$item->nik_t_siswa}}</td>
                        <td>{{$item->nm_t_siswa}}</td>
                        <td>{{$item->alamat_t_siswa}}</td>
                        <td>{{$item->jk_t_siswa}}</td>
                        <td>{{$item->pendidikan->nm_m_pendidikan}}</td>
                        <td>{{$item->nm_jurusan}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="7" align="center">Tidak ada data peserta yang belum mendapatkan kelas</td>
                    </tr>
                    @endforelse
                    
                </tbody>
            </table>
        </div>
    </div>
</div>