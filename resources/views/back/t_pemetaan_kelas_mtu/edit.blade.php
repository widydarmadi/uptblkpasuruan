@extends('back.template.index')

@section('content')


<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_program_pelatihan">Program Pelatihan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="id_m_program_pelatihan" name="id_m_program_pelatihan">
                                        @foreach ($id_m_program_pelatihan as $item)
                                            <option {{($selected_program == $item->id_m_program_pelatihan) ? 'selected' : ''}} value="{{$item->id_m_program_pelatihan}}">{{$item->nm_m_program_pelatihan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_jadwal">Jadwal / Gelombang Pelatihan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="id_m_jadwal" name="id_m_jadwal">
                                        <option value="{{$old->jadwal->id_m_jadwal}}" selected>{{$old->jadwal->nm_m_jadwal}}</option>
                                        {{-- @foreach ($id_m_jadwal as $item)
                                            <option {{($selected_jadwal == $item->id_m_jadwal) ? 'selected' : ''}} value="{{$item->id_m_jadwal}}">{{$item->nm_m_jadwal}}</option>
                                        @endforeach --}}
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_pemohon">Pemohon Pelatihan MTU</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="nm_pemohon" name="nm_pemohon">
                                        <option value="{{$old->pendaftar_mtu->no_register}}" selected>{{$old->pendaftar_mtu->nm_pemohon}}</option>

                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nama_kelas">Nama Kelas</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="hidden" id="id_t_kelas_pelatihan" value="{{$old->id_t_kelas_pelatihan}}" name="id_t_kelas_pelatihan">
                                <input type="text" id="nama_kelas" value="{{$old->nama_kelas}}" placeholder="contoh format : NAMA_PELATIHAN - GELOMBANG - KODE_KELAS" class="form-control" name="nama_kelas">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="status">Status Aktif</label>
                                </div>
                                <div class="col-sm-3">
                                    <select class="form-control" id="status" name="status">
                                        <option {{($old->status == '1') ? 'selected' : ''}} value="1">YA</option>
                                        <option {{($old->status != '1') ? 'selected' : ''}} value="0">TIDAK</option>
                                      </select>
                                </div>
                            </div>
                        </div>

                        <hr />
                        

                        <div class="col-sm-12">
                            <div id="list_siswa">
                                
                            </div>
                        </div>


                        <div class="col-sm-9">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                            <a href="{{route('admin.t_pemetaan_kelas_mtu.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>
            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
var tmp = [];

function list_siswa(){
    var no_register = $('#nm_pemohon').val();
    console.log(no_register);
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas_mtu.list_siswa") }}",
        method:"POST",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: {
            no_register:no_register
        },
        success:function(data)
        {
            $('#list_siswa').html(data);
            var checked = $('.pilih_siswa').val();
            if ($('.pilih_siswa').is(':checked')) {
                tmp.push(checked);
            }else{
                tmp.splice($.inArray(checked, tmp),1);
            }
            console.log(tmp);
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
}

list_siswa();

$('#list_siswa').on('change', '#all', function(){
    var checked = $('.pilih_siswa').val();
    $('.pilih_siswa').prop('checked', this.checked);
        
    if ($(this).is(':checked')) {
        tmp.push(checked);
    }else{
        tmp.splice($.inArray(checked, tmp),1);
    }
    console.log(tmp);
});

$('#list_siswa').on('change', '.pilih_siswa', function(){
    var checked = $(this).val();
        
    if ($(this).is(':checked')) {
        tmp.push(checked);
    }else{
        tmp.splice($.inArray(checked, tmp),1);
    }
    console.log(tmp);
});

    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);
        swal.fire({
            title: "Pesan Konfirmasi",
            text: 'Apakah Anda yakin bahwa data yang diinputkan sudah sesuai ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){

                $.ajax({
                    url:"{{ route("admin.t_pemetaan_kelas_mtu.update") }}",
                    method:"POST",
                    headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                    data: data,
                    processData: false,
                    contentType: false,
                    success:function(data)
                    {
                        if($.isEmptyObject(data.error)){

                            if(data.status == true){
                                $("#submitform").removeAttr('disabled');
                                $("#submitform span").text('Submit');
                                $("form").each(function() { this.reset() });
                                swal.fire({
                                    title: "Success",
                                    text: data.message,
                                    icon: "success"
                                }).then(function() {
                                    location.href = data.redirect;
                                });
                            }else{
                                displayErrorSwal(data.message);
                            }

                        }else{
                            displayWarningSwal();
                            $("#submitform").removeAttr('disabled');
                            $("#submitform span").text('Submit');
                            $.each(data.error, function(key, value) {
                                var element = $("#" + key);
                                element.closest("div.form-control")
                                .removeClass("text-danger")
                                .addClass(value.length > 0 ? "text-danger" : "")
                                .find("#error_" + key).remove();
                                element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                            });
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }else{
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Submit');
            }
        })
    });
});




$('#id_m_jadwal').change(function(){
    var id_m_jadwal = $(this).val();
    
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas_mtu.load_nama_jadwal") }}",
        method:"POST",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: {
            id_m_jadwal:id_m_jadwal,
        },
        success:function(data)
        {
            $('#nama_kelas').val('Kelas ' + data);
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});

</script>
@endsection
