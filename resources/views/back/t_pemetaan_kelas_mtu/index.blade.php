@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}} <a href="{{route('admin.t_pemetaan_kelas_mtu.add')}}" class="btn btn-success btn-sm float-right d-flex">Tambah Kelas Pelatihan</a></h5>
          </div>
          <div class="card-body">

            <div class="row">
              <div class="col-md-6">
                <label class="form-label">Kategori Kejuruan</label>
                <select name="id_m_kategori_kejuruan" id="id_m_kategori_kejuruan" class="form-control select2">
                  <option value="">Pilih Kategori Kejuruan</option>
                  @forelse ($list_kategori_kejuruan as $item)
                      <option value="{{$item->id_m_kategori_kejuruan}}">{{$item->nm_m_kategori_kejuruan}}</option>
                  @empty
                  @endforelse
                </select>
              </div>
              <div class="col-md-6">
                <label class="form-label">Sub Kejuruan</label>
                <select name="id_m_kejuruan" id="id_m_kejuruan" class="form-control select2">
                  <option value="">Pilih Sub Kejuruan</option>
                </select>
              </div>
              <div class="col-md-6 mb-4 mt-4">
                <label class="form-label">Program Pelatihan</label>
                <select name="id_m_program_pelatihan" id="id_m_program_pelatihan" class="form-control select2">
                  <option value="">Pilih Program Pelatihan</option>
                </select>
              </div>
              <div class="col-md-6 mb-4 mt-4">
                <label class="form-label">Jadwal / Gelombang</label>
                <select name="id_m_jadwal" id="id_m_jadwal" class="form-control select2">
                  <option value="">Pilih Jadwal / Gelombang</option>
                </select>
              </div>
              <div class="col-md-6 mb-4">
                <button class="btn d-block w-100 btn-info" id="filter_report">FILTER</button>
              </div>
            </div>

          
            <table id="datatable" class="table-striped table-sm table-hover table table-bordered dt-responsive">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Kelas</th>
                  <th>Jadwal</th>
                  <th>Status</th>
                  <th>Jml Peserta</th>
                  <th>Tgl buat</th>
                  <th>Aksi</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>

          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection


@section('js')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var table;
var id_m_kategori_kejuruan;
var id_m_kejuruan;
var id_m_program_pelatihan;
var id_m_jadwal;


$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.t_pemetaan_kelas_mtu.datatable') }}',
            method: 'post',
            data: function(q){
                q.id_m_kategori_kejuruan = id_m_kategori_kejuruan,
                q.id_m_kejuruan = id_m_kejuruan,
                q.id_m_program_pelatihan = id_m_program_pelatihan,
                q.id_m_jadwal = id_m_jadwal
            }
        },
    });
});

$('#filter_report').click(function(){
    id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
    id_m_kejuruan = $('#id_m_kejuruan').val();
    id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
    id_m_jadwal = $('#id_m_jadwal').val();
    table.ajax.reload();
})


$('#datatable').on('click', '.delete', function(){
    var id_t_kelas_pelatihan = $(this).data("id_t_kelas_pelatihan");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.t_pemetaan_kelas_mtu.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_t_kelas_pelatihan:id_t_kelas_pelatihan},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});





$('#id_m_kategori_kejuruan').change(function(){
  var id = $(this).val();
  $('#id_m_kejuruan').html('');
  $('#id_m_program_pelatihan').html('');
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas_mtu.load_kejuruan_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_kejuruan').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})



$('#id_m_kejuruan').change(function(){
  var id = $(this).val();
  $('#id_m_program_pelatihan').html('');
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas_mtu.load_program_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_program_pelatihan').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})


$('#id_m_program_pelatihan').change(function(){
  var id = $(this).val();
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.t_pemetaan_kelas_mtu.load_jadwal_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_jadwal').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})

</script>
@endsection
