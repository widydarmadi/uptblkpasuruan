<style>
    h4{
        font-family: Arial, Helvetica, sans-serif;
    }
    table.grid{
        border-collapse: collapse;
        outline: none;
    }

    table.grid tr td,table.grid tr th{
        border: 1px solid #000000;
        padding: 3px;
        font-family: Arial, Helvetica, sans-serif;
    }
</style>

<div align="center">
    <h4>LAPORAN PENGADUAN MASYARAKAT<BR>PADA WEBSITE UPT BALAI LATIHAN KERJA PASURUAN<br>
    @if(request()->filled('tgl_awal') and request()->filled('tgl_akhir'))
    @php
        app()->setLocale('id');
    @endphp
    Tanggal : {{\Carbon\Carbon::parse(request()->get('tgl_awal'))->isoFormat('D MMMM YYYY')}} s/d {{\Carbon\Carbon::parse(request()->get('tgl_akhir'))->isoFormat('D MMMM YYYY')}}
    @endif
    </h4>
</div>
<BR>
<table class="grid" cellpadding="0" cellspacing="0" border="1" width="100%">
    <thead>
        <tr>
            <th>No.</th>
            <th>Email</th>
            <th>Permasalahan</th>
            <th>Lokasi</th>
            <th>Detail Laporan</th>
            <th>Tanggal</th>
        </tr>
    </thead>

    <tbody>
        @php
            $i=1;
        @endphp
        @forelse ($table as $item)
            <tbody>
                <tr>
                    <td align="center">{{$i++}}</td>
                    <td align="center">{{$item->email_m_pengaduan}}</td>
                    <td>{{$item->permasalahan->nm_m_permasalahan}}</td>
                    <td>{{$item->lokasi_m_pengaduan}}</td>
                    <td>{{$item->isi_m_pengaduan}}</td>
                    <td align="center">{{\Carbon\Carbon::parse($item->created_at)->format('d-m-Y H:i:s')}}</td>
                </tr>
            </tbody>
        @empty
            
        @endforelse
    </tbody>
</table>