@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">

                <form method="post" id="form">

                    <div class="row">
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="email_m_pengaduan">Email</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="hidden" id="id_m_pengaduan" value="{{$old->id_m_pengaduan}}" class="form-control" name="id_m_pengaduan">
                                <input type="text" readonly id="email_m_pengaduan" value="{{$old->email_m_pengaduan}}" class="form-control" name="email_m_pengaduan">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="is_bersedia">Bersedia ungkapkan identitas</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="is_bersedia" readonly value="{{($old->is_bersedia == '1') ? 'YA' : 'TIDAK' }}" class="form-control" name="is_bersedia">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="status_pelapor">Status Pelapor</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="status_pelapor" readonly value="{{($old->status_pelapor) ? $old->status_pelapor : '-' }}" class="form-control" name="status_pelapor">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="is_terlapor">Apakah terlapor pernah dilaporkan sebelumnya</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="is_terlapor" readonly value="{{($old->is_terlapor == '1') ? 'YA' : 'TIDAK' }}" class="form-control" name="is_terlapor">
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="email_korespondensi_m_pengaduan">Email Korespondensi</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="email_korespondensi_m_pengaduan" readonly value="{{($old->email_korespondensi_m_pengaduan) ? $old->email_korespondensi_m_pengaduan : '-' }}" class="form-control" name="email_korespondensi_m_pengaduan">
                                </div>
                            </div>
                        </div>
                       
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_permasalahan">Jenis Permasalahan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="id_m_permasalahan" readonly value="{{($old->id_m_permasalahan) ? \App\Models\M_permasalahan::select('nm_m_permasalahan')->where('id_m_permasalahan',$old->id_m_permasalahan)->first()->nm_m_permasalahan : '-' }}" class="form-control" name="id_m_permasalahan">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jml_kerugian">Jumlah Kerugian (Estimasi)</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="jml_kerugian" readonly value="{{($old->jml_kerugian) ? $old->jml_kerugian : '-' }}" class="form-control" name="jml_kerugian">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="terlibat_m_pengaduan">Yang terlibat</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="terlibat_m_pengaduan" readonly value="{{($old->terlibat_m_pengaduan) ? $old->terlibat_m_pengaduan : '-' }}" class="form-control" name="terlibat_m_pengaduan">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="lokasi_m_pengaduan">Lokasi Kejadian</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="lokasi_m_pengaduan" readonly value="{{($old->lokasi_m_pengaduan) ? $old->lokasi_m_pengaduan : '-' }}" class="form-control" name="lokasi_m_pengaduan">
                                </div>
                            </div>
                        </div>
                        
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="waktu_m_pengaduan">Waktu Kejadian</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="waktu_m_pengaduan" readonly value="{{($old->waktu_m_pengaduan) ? \Carbon\Carbon::parse($old->waktu_m_pengaduan)->format('d-m-Y') : '-' }}" class="form-control" name="waktu_m_pengaduan">
                                </div>
                            </div>
                        </div>
                      
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="ket_m_pengaduan">Keterangan Tambahan</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="ket_m_pengaduan" readonly value="{{($old->ket_m_pengaduan) ? $old->ket_m_pengaduan : '-' }}" class="form-control" name="ket_m_pengaduan">
                                </div>
                            </div>
                        </div>
                        
                        

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="isi_m_pengaduan">Isi Pengaduan</label>
                                </div>
                                <div class="col-sm-9">
                                    <div id="div_isi_m_pengaduan">{!!$old->isi_m_pengaduan!!}</div>
                                    <textarea style="display: none" id="isi_m_pengaduan" class="form-control" name="isi_m_pengaduan" rows="4">{{$old->isi_m_pengaduan}}</textarea>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="saran_m_pengaduan">Kritik dan Saran</label>
                                </div>
                                <div class="col-sm-9">
                                <input type="text" id="saran_m_pengaduan" readonly value="{{($old->saran_m_pengaduan) ? $old->saran_m_pengaduan : '-' }}" class="form-control" name="saran_m_pengaduan">
                                </div>
                            </div>
                        </div>
                       
                        
                        <div class="col-sm-9 offset-sm-3">
                            {{-- <button id="submitform" type="submit" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button> --}}
                            <a href="{{route('admin.m_pengaduan.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
        </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
var editor = CKEDITOR.replace( 'div_isi_m_pengaduan' );
editor.on( 'change', function( evt ) {
    $('#isi_m_pengaduan').html(evt.editor.getData());
});


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_pengaduan.update") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
    });

</script>
@endsection
