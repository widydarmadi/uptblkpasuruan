@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}}</h5>
          </div>
          <div class="card-body">

            <div class="row">
              <div class="col-md-2">
                <label class="form-label">Periode Awal</label>
                
                <div class="input-group date" id="tanggal" data-target-input="nearest">
                    <input type="text" readonly="" id="date" name="tanggal" class="form-control datetimepicker-input" data-target="#tanggal"/>
                    <div class="input-group-append" data-target="#tanggal" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

              </div>
              <div class="col-md-2">
                <label class="form-label">Periode Akhir</label>
                
                <div class="input-group date" id="tanggal2" data-target-input="nearest">
                    <input type="text" readonly="" id="date2" name="tanggal2" class="form-control datetimepicker-input" data-target="#tanggal2"/>
                    <div class="input-group-append" data-target="#tanggal2" data-toggle="datetimepicker">
                        <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                    </div>
                </div>

              </div>
              <div class="col-md-4">
                <label class="form-label">Topik Permasalahan</label>
                <select name="id_m_permasalahan" id="id_m_permasalahan" class="form-control select2">
                  <option value="">Pilih Topik Permasalahan</option>
                  @foreach ($id_m_permasalahan as $item)
                      <option value="{{$item->id_m_permasalahan}}">{{$item->nm_m_permasalahan}}</option>
                  @endforeach
                </select>
              </div>
              <div class="col-md-2 mb-4 mt-3 pt-3">
                <button class="btn d-block w-100 btn-info" id="filter_report">FILTER</button>
              </div>
              <div class="col-md-2 mb-4 mt-3 pt-3">
                <button class="btn d-block w-100 btn-info" id="print_report">CETAK</button>
              </div>
            </div>

            <table id="datatable" class="table-striped table-sm table-hover table table-bordered dt-responsive nowrap">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Email</th>
                  <th>Status Terlapor</th>
                  <th>Permasalahan</th>
                  <th>Lokasi Kejadian</th>
                  <th>Tanggal Masuk</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>

          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>

<!-- Modal Print -->
<div
  class="modal fade"
  id="modal_print"
  tabindex="-1"
  aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true"
>
  <div class="modal-dialog modal-xl">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Cetak Laporan Pengaduan</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>




@endsection


@section('js')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var table;
var tgl_awal;
var tgl_akhir;
var id_m_permasalahan;

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.m_pengaduan.datatable') }}',
            method: 'post',
            data: function(q){
              q.tgl_awal = tgl_awal,
              q.tgl_akhir = tgl_akhir,
              q.id_m_permasalahan = id_m_permasalahan
            }
        },
    });
});


$('#filter_report').click(function(){
    tgl_awal = $('#date').val();
    tgl_akhir = $('#date2').val();
    id_m_permasalahan = $('#id_m_permasalahan').val();
    table.ajax.reload();
})

$('#print_report').click(function(){
    tgl_awal = $('#date').val();
    tgl_akhir = $('#date2').val();
    id_m_permasalahan = $('#id_m_permasalahan').val();
    $('#modal_print').modal('show');

    $.ajax({
        url:"{{ route("admin.m_pengaduan.iframe_print") }}",
        method:"post",
        dataType: 'html',
        data: {
          tgl_awal: tgl_awal,
          tgl_akhir: tgl_akhir,
          id_m_permasalahan: id_m_permasalahan
        },
        success:function(data)
        {
            $('#modal_print .modal-body').html(data);
        },
        error: function(data){
          displayErrorSwal();
        }
    });
})

$('#datatable').on('click', '.delete', function(){
    var id_m_pengaduan = $(this).data("id_m_pengaduan");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_pengaduan.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_m_pengaduan:id_m_pengaduan},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});

  //Date picker
  $('.date').each(function(){
      $(this).datetimepicker({
         format: 'Y-MM-DD'
      });
  })


</script>
@endsection
