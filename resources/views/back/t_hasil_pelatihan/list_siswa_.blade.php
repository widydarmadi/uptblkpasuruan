<div class="col-12">
    <div class="mb-3 row">
        <div class="col-sm-12">
            <div class="alert bg-info">
                Centang pada kolom di bawah untuk peserta-peserta yang dinyatakan <strong>KOMPETEN</strong>. Apabila tidak di centang, maka peserta tersebut dianggap <strong>BELUM KOMPETEN</strong>
            </div>
            <table style="width: 100%" class="table table-striped table-sm table-bordered table-hover">
                <thead>
                    <tr>
                        <th><input type="checkbox" style="margin-left: 0px; position:relative;" class="form-check-input" name="all" id="all" /> </th>
                        <th>Reg</th>
                        <th>NIK</th>
                        <th>NAMA</th>
                        <th>JENIS KELAMIN</th>
                        <th>EMAIL</th>
                        <th>KATEGORI PELATIHAN</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($list_siswa as $item)
                    <tr>
                        <td><input type="checkbox" 
                            {{($item->seleksi and $item->seleksi->peserta) ? 
                            ($item->seleksi->peserta->status == 'KOMPETEN' ? 'checked' : null) : ''}} 
                            value="{{$item->id_m_pendaftar}}" style="margin-left: 0px; position:relative;" class="form-check-input pilih_siswa" name="pilih_siswa[]" /></td>
                        <td>{{$item->no_register}}</td>
                        <td>{{$item->nik_m_pendaftar}}</td>
                        <td>{{$item->nm_m_pendaftar}}</td>
                        <td>{{$item->jk_m_pendaftar}}</td>
                        <td>{{$item->email_m_pendaftar}}</td>
                        <td>{{$item->kategori_m_pendaftar}}</td>
                    </tr>
                    @empty
                    <tr>
                        <td colspan="5" align="center">Tidak ada data peserta</td>
                    </tr>
                    @endforelse
                    
                </tbody>
            </table>
        </div>
    </div>
</div>