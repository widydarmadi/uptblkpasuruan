@extends('back.template.index')

@section('content')

<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}} </h5>
            </div>
            <div class="card-body">

                    <div class="row">


                        <input type="hidden" value="{{$old->id_m_album}}" name="id_m_album" />


                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-4">
                                <label class="col-form-label" for="judul_m_album">Judul Album</label>
                                </div>
                                <div class="col-sm-8">
                                <textarea id="judul_m_album" readonly class="form-control" name="judul_m_album" rows="4">{{$old->judul_m_album}}</textarea>
                                </div>
                            </div>
                        </div>
                     
                      
                     
                        <div class="col-sm-8 offset-sm-4">
                        <a href="{{route('admin.m_galeri.index')}}" class="btn btn-secondary waves-effect">Kembali</a>
                        </div>
                    </div>
            </div>
          </div>





          <div class="card card-green card-outline">
            <div class="card-header">
              <h5 class="m-0">Unggah Foto ke dalam Album ini</h5>
            </div>
            <div class="card-body">
                <form method="post" id="form">

                    <div class="row">

                        <input type="hidden" value="{{$old->id_m_album}}" name="id_m_album" />

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-4">
                                <label class="col-form-label" for="judul_m_album">Pilih Foto (berekstensi JPG, JPEG, atau PNG)</label>
                                </div>
                                <div class="col-sm-8">
                                    <input type="file" id="photo" name="photo" class="form-control" />
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-4">
                                <label class="col-form-label" for="caption">Caption / Keterangan</label>
                                </div>
                                <div class="col-sm-8">
                                <textarea id="caption"  class="form-control" name="caption" rows="4">{{$old->caption}}</textarea>
                                </div>
                            </div>
                        </div>
                     
                      
                     
                        <div class="col-sm-8 offset-sm-4">
                            <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Upload Foto</span></button>
                        </div>
                    </div>
                </form>


                <div class="row mt-3">
                    @foreach ($album as $item)
                        <div class="col-md-2 text-center">
                            <img class="rounded" src="{{asset('storage'.'/'.$item->photo)}}" width="100%" />
                            <br>
                            <a class="btn mt-1 btn-sm btn-danger delete" href="javascript:void(0)" data-id_m_album="{{$item->id_m_album}}" data-id_t_album="{{$item->id_t_album}}" >hapus foto ?</a>
                        </div>
                    @endforeach
                </div>

            </div>
          </div>



        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>


    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        

        $.ajax({
            url:"{{ route("admin.m_galeri.upload_post") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
});






$('.delete').click(function(){
    var id_t_album = $(this).data("id_t_album");
    var id_m_album = $(this).data("id_m_album");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_galeri.hapus_foto') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{
                    id_t_album:id_t_album,
                    id_m_album:id_m_album
                },
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            location.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});

</script>
@endsection
