@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}} <a href="{{route('admin.m_jadwal.add')}}" class="btn btn-success btn-sm float-right d-flex">Tambah Data</a></h5>
            
          </div>
          
          <div class="card-body">
              <table id="datatable" class="table-striped table-sm table-hover table table-bordered dt-responsive nowrap">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>Program</th>
                    <th>Gelombang</th>
                    <th>Tahun</th>
                    {{-- <th>Mulai</th>
                    <th>Akhir</th> --}}
                    <th>Kuota</th>
                    <th>Status</th>
                    <th>Pengumuman di Website</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>
@endsection


@section('js')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var table;

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('admin.m_jadwal.datatable') }}',
            method: 'post'
        },
    });
});


$('#datatable').on('click', '.delete', function(){
    var id_m_jadwal = $(this).data("id_m_jadwal");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_jadwal.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_m_jadwal:id_m_jadwal},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Deleted!",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});



$('#datatable').on('click', '.publish', function(){
    var id_m_jadwal = $(this).data("id_m_jadwal");
    swal.fire({
        title: "Konfirmasi",
        text: 'Apakah Anda yakin untuk mengaktifkan / menonaktifkan pengumuman hasil seleksi untuk jadwal ini ke dalam website UPT BLK Pasuruan?',
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "YA",
        cancelButtonText: "TIDAK",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('admin.m_jadwal.publish') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_m_jadwal:id_m_jadwal},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Berhasil !",
                            text: 'Data Pengumuman Kelulusan Telah Diubah',
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }

        })
});
</script>
@endsection
