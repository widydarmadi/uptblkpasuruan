@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
              
            </div>
            
            <div class="card-body">
                
                <form method="post" id="form">

                    <div class="row">

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_program_pelatihan">Program Pelatihan</label>
                                </div>
                                <div class="col-sm-9">
                                    <select class="form-control select2" id="id_m_program_pelatihan" name="id_m_program_pelatihan">
                                        <option value="">Pilih Program Pelatihan</option>
                                        
                                        @foreach ($prog as $item)
                                            <option value="{{$item->id_m_program_pelatihan}}">{{$item->nm_m_program_pelatihan . ' - ' . $item->tipe_pelatihan->nm_m_tipe_pelatihan}}</option>
                                        @endforeach
                                    
                                    </select>
                                </div>
                            </div>
                        </div>

                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="id_m_gelombang">Gelombang</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" id="id_m_gelombang" name="id_m_gelombang">
                                        <option value="">Pilih Gelombang</option>
                                        @foreach ($gelombang as $item)
                                        <option value="{{$item->id_m_gelombang}}">{{$item->nm_m_gelombang}}</option>
                                        @endforeach
                                      </select>
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="tahun">Tahun Pelaksanaan</label>
                                </div>
                                <div class="col-sm-2">
                                <input type="text" id="tahun" placeholder="contoh : {{date('Y')}}" class="form-control" name="tahun">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="nm_m_jadwal">Nama Pelatihan</label>
                                </div>
                                <div class="col-sm-5">
                                    <input type="text" id="nm_m_jadwal" placeholder="Nama Pelatihan" class="form-control" style="background: #E3FDFB" name="nm_m_jadwal">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="text_gelombang" placeholder="Gelombang" class="form-control" style="background: #E3FDFB" name="text_gelombang">
                                </div>
                                <div class="col-sm-2">
                                    <input type="text" id="text_tahun" placeholder="Tahun" class="form-control" style="background: #E3FDFB" name="text_tahun">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="route">Tanggal Mulai</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="tgl_mulai" data-target-input="nearest">
                                        <input type="text" name="tgl_mulai" class="form-control datetimepicker-input" data-target="#tgl_mulai"/>
                                        <div class="input-group-append" data-target="#tgl_mulai" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="route">Tanggal Selesai</label>
                                </div>
                                <div class="col-sm-3">
                                    <div class="input-group date" id="tgl_selesai" data-target-input="nearest">
                                        <input type="text" name="tgl_selesai" class="form-control datetimepicker-input" data-target="#tgl_selesai"/>
                                        <div class="input-group-append" data-target="#tgl_selesai" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="jml_paket">Jumlah Paket</label>
                                </div>
                                <div class="col-sm-2">
                                <input type="text" id="jml_paket" placeholder="contoh : 2" class="form-control" name="jml_paket">
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="kuota">Jumlah Kuota</label>
                                </div>
                                <div class="col-sm-2">
                                <input type="text" id="kuota" placeholder="contoh : 200" class="form-control" name="kuota">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="hari">Jumlah Hari</label>
                                </div>
                                <div class="col-sm-2">
                                <input type="text" id="hari" placeholder="contoh : 33" class="form-control" name="hari">
                                </div>
                            </div>
                        </div>

                        <div class="col-12">
                            <div class="mb-3 row">
                                <div class="col-sm-3">
                                <label class="col-form-label" for="status">Status Tampil</label>
                                </div>
                                <div class="col-sm-4">
                                    <select class="form-control" id="status" name="status">
                                        <option value="">Pilih Status Tampil</option>
                                        <option value="OPEN">Tampil (pengguna diperbolehkan mendaftar)</option>
                                        <option value="CLOSE">Tidak Tampil</option>
                                      </select>
                                </div>
                            </div>
                        </div>
                        
                   

                        <div class="col-sm-9 offset-sm-3">
                        <button type="submit" id="submitform" class="btn btn-success me-1 waves-effect waves-float waves-light"><span>Submit</span></button>
                        <a href="{{route('admin.m_jadwal.index')}}" class="btn btn-secondary waves-effect">Back</a>
                        </div>
                    </div>
                </form>

            </div>
          </div>
        </div>
        <!-- /.col-md-6 -->
      </div>
      <!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>

@endsection

@section('js')
<script>
    var text_label;
    $(document).ready( function () {
        $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("admin.m_jadwal.save") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){

                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }

                }else{
                    displayWarningSwal();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Submit');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    });
    });

    
  //Date picker
  $('.date').each(function(){
      $(this).datetimepicker({
         format: 'Y-MM-DD'
      });
  })


  $('#id_m_program_pelatihan').change(function(){
        $('#nm_m_jadwal').val('');
        var values = $(this).val();
        $.ajax({
            url:"{{ route("admin.m_jadwal.set_text_program") }}",
            method:"POST",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: {
                id_m_program_pelatihan: values
            },
            success:function(data)
            {
                text_label = data.html;
                $('#nm_m_jadwal').val(text_label);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });

        $('#nm_m_jadwal').val($(this).text());
  })
  
  
  $('#id_m_gelombang').change(function(){
    var text_gelombang = $( "#id_m_gelombang option:selected" ).text();
    $('#text_gelombang').val(text_gelombang);
  })

  $('#tahun').keyup(function(){
    var text_tahun = $("#tahun").val();
    $('#text_tahun').val(text_tahun);
  })

</script>
@endsection
