<table class="table table-sm table-striped table-hover">
    <thead>
        <tr>
            <th>Nama Modul Soal</th>
            <th>Jumlah Soal</th>
            <th>Pilih</th>
        </tr>
    </thead>

    <tbody>
        @foreach ($menu as $item)
        <tr>
            <td>{{$item->nm_paket_soal_wawancara}}</td>
            <td>{{$item->soal_count}}</td>
            <td>
                <a 
                data-id_m_pendaftar="{{$id_m_pendaftar}}"
                data-paket_soal="{{$item->id_t_paket_soal_wawancara}}"
                class="btn btn-sm btn-success pilihsoal" href="#">Pilih Modul</a>
                {{-- <a class="btn btn-sm btn-success pilihsoal" href="{{route('admin.t_wawancara.edit',['id_m_pendaftar' => $id_m_pendaftar, 'paket_soal' => $item->id_t_paket_soal_wawancara])}}">Pilih Modul</a> --}}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


<script>
$('.pilihsoal').click(function(){
    var id_m_pendaftar = $(this).data('id_m_pendaftar');
    var paket_soal = $(this).data('paket_soal');
    swal.fire({
        title: "Pesan Konfirmasi",
        text: 'Apakah Anda siap untuk memulai sesi wawancara ?',
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {
        if(e.value){
            location.href = '{{route('admin.t_wawancara.edit')}}?id_m_pendaftar='+id_m_pendaftar+'&paket_soal='+paket_soal;
        }
    })
})

</script>