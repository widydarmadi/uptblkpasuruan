@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">


                        <div class="row">

                            <input type="hidden" id="id_m_pendaftar" value="{{$old->id_m_pendaftar}}" class="form-control" name="id_m_pendaftar">
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="no_register">No. Register</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="no_registerxxx" value="{{$old->no_register}}" class="form-control" name="no_registerxxx">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="nik_m_pendaftar">NIK Calon Peserta</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="nik_m_pendaftar" value="{{$old->nik_m_pendaftar}}" class="form-control" name="nik_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="nm_m_pendaftar">Nama Calon Peserta</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="nm_m_pendaftar" value="{{$old->nm_m_pendaftar}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="wa_m_pendaftar">No. HP / WA</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="wa_m_pendaftar" value="{{$old->wa_m_pendaftar}}" class="form-control" name="wa_m_pendaftar">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="alamat_m_pendaftar">Alamat</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="alamat_m_pendaftar" value="{{$old->alamat_m_pendaftar}}" class="form-control" name="email_m_pendaftar">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="email_m_pendaftar">Email</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="email_m_pendaftar" value="{{$old->email_m_pendaftar}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="id_m_pendidikan">Pendidikan</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="id_m_pendidikan" value="{{($old->pendidikan) ? $old->pendidikan->nm_m_pendidikan : null}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="nm_jurusan">Jurusan</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="nm_jurusan" value="{{$old->nm_jurusan}}" class="form-control" name="nm_jurusan">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="is_disabilitas">Penyandang Disabilitas ?</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="is_disabilitas" value="{{($old->is_disabilitas == null) ? 'TIDAK' : $old->is_disabilitas}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="disabilitas">Jenis Disabilitas</label>
                                    </div>
                                    <div class="col-sm-9">
                                    @php
                                        if($old->is_disabilitas != 'YA'){
                                            $txt_dis = null;
                                        }else{
                                            $jenis_disabilitas = \App\Models\M_disabilitas::where('id_disabilitas', $old->id_m_disabilitas)->first();
                                            $txt_dis = $jenis_disabilitas->nama_disabilitas;
                                        }
                                    @endphp
                                    <input type="text" readonly id="disabilitas" value="{{$txt_dis}}" class="form-control" name="disabilitas">
                                    </div>
                                </div>
                            </div>
                         
{{--                             
                            
                            <div class="col-sm-9 offset-sm-3">
                                <a href="{{route('admin.t_wawancara.index')}}" class="btn btn-secondary waves-effect">Back</a>
                            </div> --}}
                        </div>

                </div>
            </div>
        </div>
        <!-- /.col-md-6 -->
        
        
        
        
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">Soal Pertanyaan Wawancara User</h5>
            </div>
            <div class="card-body">

                    <form method="post" id="form_wawancara">
                        <input type="hidden" id="cbt_grup_id" value="{{$old->cbt_grup_id}}" class="form-control" name="cbt_grup_id">
                        <input type="hidden" id="paket_soal" value="{{request()->get('paket_soal')}}" class="form-control" name="paket_soal">
                        <input type="hidden" id="id_m_pendaftar" value="{{$old->id_m_pendaftar}}" class="form-control" name="id_m_pendaftar">
                        <input type="hidden" readonly id="email_m_pendaftar" value="{{$old->email_m_pendaftar}}" class="form-control" name="email_m_pendaftar">

                        <div class="row">

                            @if($list_soal)

                            @foreach($list_soal as $item)
                            <input type="hidden" readonly value="{{$item->id_m_soal_wawancara}}" class="form-control" name="id_m_soal[]">

                                <div class="col-12">
                                    <div class="mb-3 row" style="border-bottom: 1px solid #dddddd">
                                        <div class="col-sm-9">
                                            <label class="col-form-label text-primary" for="nm_jurusan">{!!$item->soal->soal_wawancara!!}</label>
                                        </div>
                                        <div class="col-sm-2">
                                            <select required name="score[]" class="form-control" id="score">
                                                <option value="">Skor Wawancara</option>
                                                <option value="10">10</option>
                                                <option value="20">20</option>
                                                <option value="30">30</option>
                                                <option value="40">40</option>
                                                <option value="50">50</option>
                                                <option value="60">60</option>
                                                <option value="70">70</option>
                                                <option value="80">80</option>
                                                <option value="90">90</option>
                                                <option value="100">100</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>

                            @endforeach

                            @endif



                            <div class="col-12 mb-3" style="border-bottom: 1px solid #dddddd">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label text-primary" for="note">Catatan Wawancara</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <textarea name="note" id="note" class="form-control" rows="5"></textarea>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-9 offset-sm-0">
                                <button type="submit" id="submitform" class="btn btn-lg btn-success me-1 waves-effect waves-float waves-light"><span>Selesai Wawancara</span></button>
                                &nbsp;&nbsp;&nbsp;<a href="{{route('admin.t_wawancara.index')}}" class="btn btn-lg btn-secondary waves-effect">Kembali</a>
                            </div>
                            
                        </div>
                    </form>

                </div>
            </div>
        </div>
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#form_wawancara").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form_wawancara')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);
        swal.fire({
            title: "Pesan Konfirmasi",
            text: 'Apakah Anda yakin bahwa data yang diinputkan sudah sesuai ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){

                $.ajax({
                    url:"{{ route("admin.t_wawancara.update") }}",
                    method:"POST",
                    headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                    data: data,
                    processData: false,
                    contentType: false,
                    success:function(data)
                    {
                        if($.isEmptyObject(data.error)){

                            if(data.status == true){
                                $("#submitform").removeAttr('disabled');
                                $("#submitform span").text('Selesai Wawancara');
                                $("form").each(function() { this.reset() });
                                swal.fire({
                                    title: "Success",
                                    text: data.message,
                                    icon: "success"
                                }).then(function() {
                                    location.href = data.redirect;
                                });
                            }else{
                                displayErrorSwal(data.message);
                            }

                        }else{
                            displayWarningSwal();
                            $("#submitform").removeAttr('disabled');
                            $("#submitform span").text('Selesai Wawancara');
                            $.each(data.error, function(key, value) {
                                var element = $("#" + key);
                                element.closest("div.form-control")
                                .removeClass("text-danger")
                                .addClass(value.length > 0 ? "text-danger" : "")
                                .find("#error_" + key).remove();
                                element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                            });
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }else{
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Selesai Wawancara');
            }
        })
    });
});


$('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})
</script>
@endsection
