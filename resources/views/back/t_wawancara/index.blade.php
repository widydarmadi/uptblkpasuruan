@extends('back.template.index')

@section('content')

<div class="content">
  <div class="container-fluid">
    <div class="row">
      <!-- /.col-md-12 -->
      <div class="col-lg-12">
        <div class="card card-yellow card-outline">
          <div class="card-header">
            <h5 class="m-0">{{$page_title}}</h5>
          </div>
          <div class="card-body">

            <div class="alert bg-info">
              <p>
                Informasi status perankingan dan kelulusan yang ditampilkan pada tabel berikut ini bersifat dinamis, mengacu pada pergerakan nilai antar calon peserta pada masing-masing jadwal pelatihan yang sedang dibuka.
                 <br>Kelulusan diambil sejumlah kuota pada masing-masing jadwal pelatihan dengan nilai peserta yang paling tinggi.
              </p>
              <a href="javascript:void(0)" id="updatedata" class="btn btn-light"><span class="text-dark">Perbarui Peringkat</span></a>
            </div>

            <div class="row">
              <div class="col-md-6">
                <label class="form-label">Kategori Kejuruan</label>
                <select name="id_m_kategori_kejuruan" id="id_m_kategori_kejuruan" class="form-control select2">
                  <option value="">Pilih Kategori Kejuruan</option>
                  @forelse ($list_kategori_kejuruan as $item)
                      <option value="{{$item->id_m_kategori_kejuruan}}">{{$item->nm_m_kategori_kejuruan}}</option>
                  @empty
                  @endforelse
                </select>
              </div>
              <div class="col-md-6">
                <label class="form-label">Sub Kejuruan</label>
                <select name="id_m_kejuruan" id="id_m_kejuruan" class="form-control select2">
                  <option value="">Pilih Sub Kejuruan</option>
                </select>
              </div>
              <div class="col-md-6 mb-4 mt-4">
                <label class="form-label">Program Pelatihan</label>
                <select name="id_m_program_pelatihan" id="id_m_program_pelatihan" class="form-control select2">
                  <option value="">Pilih Program Pelatihan</option>
                </select>
              </div>
              <div class="col-md-6 mb-4 mt-4">
                <label class="form-label">Jadwal / Gelombang</label>
                <select name="id_m_jadwal" id="id_m_jadwal" class="form-control select2">
                  <option value="">Pilih Jadwal / Gelombang</option>
                </select>
              </div>
              <div class="col-md-6 mb-4">
                <button class="btn d-block w-100 btn-info" id="filter_report">FILTER</button>
              </div>
              <div class="col-md-6 mb-4">
                <button class="btn d-block w-100 btn-info" id="print_excel">EKSPOR EXCEL</button>
              </div>
            </div>

            <table id="datatable" class="table-striped table-sm table-hover table table-bordered ">
              <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>NIK</th>
                  <th>Program</th>
                  <th>No.Register</th>
                  <th>CBT</th>
                  <th>Wawancara</th>
                  <th>Skor</th>
                  <th>Keterangan</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>

              </tbody>
            </table>

          </div>
        </div>
      </div>
      <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
  </div><!-- /.container-fluid -->
</div>


<!-- Modal Hak Akses Per Module -->
<div
  class="modal fade"
  id="modal_pilih_soal"
  tabindex="-1"
  aria-labelledby="exampleModalCenterTitle"
  aria-hidden="true"
>
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalCenterTitle">Pilih Modul Soal</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">×</span>
        </button>
      </div>
      <div class="modal-body">
        
      </div>
    </div>
  </div>
</div>


@endsection


@section('js')
<script>
$.ajaxSetup({
headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
}
});

var table;
var id_m_kategori_kejuruan;
var id_m_kejuruan;
var id_m_program_pelatihan;
var id_m_jadwal;

$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 16,
        ajax: {
            url: '{{ route('admin.t_wawancara.datatable') }}',
            method: 'post',
            data: function(q){
                q.id_m_kategori_kejuruan = id_m_kategori_kejuruan,
                q.id_m_kejuruan = id_m_kejuruan,
                q.id_m_program_pelatihan = id_m_program_pelatihan,
                q.id_m_jadwal = id_m_jadwal
            }
        },
    });
});

$('#filter_report').click(function(){
    id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
    id_m_kejuruan = $('#id_m_kejuruan').val();
    id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
    id_m_jadwal = $('#id_m_jadwal').val();
    table.ajax.reload();
})


$('#updatedata').click(function(){
    swal.fire({
        title: "Pesan Konfirmasi",
        text: 'Apakah Anda yakin untuk mengupdate data ranking ?',
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){

            $.ajax({
                url:"{{ route("admin.t_wawancara.perbarui_peringkat") }}",
                method:"POST",
                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                success:function(data)
                {
                    if($.isEmptyObject(data.error)){

                        if(data.status == true){
                            $("#submitform").removeAttr('disabled');
                            $("#submitform span").text('Submit');
                            $("form").each(function() { this.reset() });
                            swal.fire({
                                title: "Success",
                                text: data.message,
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }

                    }else{
                        displayWarningSwal();
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Submit');
                        $.each(data.error, function(key, value) {
                            var element = $("#" + key);
                            element.closest("div.form-control")
                            .removeClass("text-danger")
                            .addClass(value.length > 0 ? "text-danger" : "")
                            .find("#error_" + key).remove();
                            element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                        });
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }
    })
})




$('#datatable').on('click', '.start_wawancara', function(){
  $('#modal_pilih_soal').modal('show');
  var id_m_pendaftar = $(this).data('id_m_pendaftar');
  $.ajax({
      url:"{{ route("admin.t_wawancara.set_soal") }}",
      method:"post",
      dataType: 'html',
      data: {
        id_m_pendaftar: id_m_pendaftar
      },
      success:function(data)
      {
          $('#modal_pilih_soal .modal-body').html(data);
      },
      error: function(data){
        displayErrorSwal();
      }
  });
})


$('#datatable').on('click', '.undurdiri', function(){
  var id_m_pendaftar = $(this).data('id_m_pendaftar');
  swal.fire({
        title: "Pesan Konfirmasi",
        text: 'Apakah peserta yang bersangkutan yakin untuk mengundurkan diri dari rangkaian seleksi ?',
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "Ya",
        cancelButtonText: "Tidak",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){

            $.ajax({
                url:"{{ route("admin.t_wawancara.set_undur_diri") }}",
                method:"POST",
                headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                data:{
                  id_m_pendaftar:id_m_pendaftar
                },
                success:function(data)
                {
                    if($.isEmptyObject(data.error)){
                        if(data.status == true){
                            swal.fire({
                                title: "Success",
                                text: data.message,
                                icon: "success"
                            }).then(function() {
                                location.href = data.redirect;
                            });
                        }else{
                            displayErrorSwal(data.message);
                        }

                    }else{
                        displayWarningSwal();
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }
    })
})


$('#id_m_kategori_kejuruan').change(function(){
  var id = $(this).val();
  $('#id_m_kejuruan').html('');
  $('#id_m_program_pelatihan').html('');
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.t_wawancara.load_kejuruan_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_kejuruan').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})



$('#id_m_kejuruan').change(function(){
  var id = $(this).val();
  $('#id_m_program_pelatihan').html('');
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.t_wawancara.load_program_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_program_pelatihan').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})


$('#id_m_program_pelatihan').change(function(){
  var id = $(this).val();
  $('#id_m_jadwal').html('');
  if(id != null){
    $.ajax({
        url:"{{ route("admin.t_wawancara.load_jadwal_available") }}",
        method:"post",
        dataType: 'json',
        data: {
          id: id
        },
        success:function(data)
        {
          if(data.status == true){
            $('#id_m_jadwal').html(data.html);
          }
        },
        error: function(data){
          displayErrorSwal();
        }
    });
  }
})



$('#print_excel').click(function(){
  id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
  id_m_kejuruan = $('#id_m_kejuruan').val();
  id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
  id_m_jadwal = $('#id_m_jadwal').val();
  window.open('{{route('admin.t_wawancara.export_xls')}}?id_m_kategori_kejuruan=' + id_m_kategori_kejuruan + '&id_m_kejuruan=' + id_m_kejuruan + '&id_m_program_pelatihan=' + id_m_program_pelatihan + '&id_m_jadwal=' + id_m_jadwal, '_blank');
})
</script>
@endsection
