@extends('back.template.index')

@section('content')
<div class="content">
    <div class="container-fluid">
      <div class="row">
        <!-- /.col-md-12 -->
        <div class="col-lg-12">
          <div class="card card-yellow card-outline">
            <div class="card-header">
              <h5 class="m-0">{{$page_title}}</h5>
            </div>
            <div class="card-body">


                        <div class="row">

                            <input type="hidden" id="id_m_pendaftar" value="{{$old->id_m_pendaftar}}" class="form-control" name="id_m_pendaftar">
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="no_register">No. Register</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="no_registerxxx" value="{{$old->no_register}}" class="form-control" name="no_registerxxx">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="nik_m_pendaftar">NIK Calon Peserta</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="nik_m_pendaftar" value="{{$old->nik_m_pendaftar}}" class="form-control" name="nik_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="nm_m_pendaftar">Nama Calon Peserta</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="nm_m_pendaftar" value="{{$old->nm_m_pendaftar}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="wa_m_pendaftar">No. HP / WA</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="wa_m_pendaftar" value="{{$old->wa_m_pendaftar}}" class="form-control" name="wa_m_pendaftar">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="alamat_m_pendaftar">Alamat</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="alamat_m_pendaftar" value="{{$old->alamat_m_pendaftar}}" class="form-control" name="email_m_pendaftar">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="email_m_pendaftar">Email</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="email_m_pendaftar" value="{{$old->email_m_pendaftar}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="id_m_pendidikan">Pendidikan</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="id_m_pendidikan" value="{{($old->pendidikan) ? $old->pendidikan->nm_m_pendidikan : null}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="nm_jurusan">Jurusan</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="nm_jurusan" value="{{$old->nm_jurusan}}" class="form-control" name="nm_jurusan">
                                    </div>
                                </div>
                            </div>

                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="is_disabilitas">Penyandang Disabilitas ?</label>
                                    </div>
                                    <div class="col-sm-9">
                                    <input type="text" readonly id="is_disabilitas" value="{{($old->is_disabilitas == 'YA') ? 'YA' : 'TIDAK'}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                            
                            <div class="col-12">
                                <div class="mb-3 row">
                                    <div class="col-sm-3">
                                    <label class="col-form-label" for="disabilitas">Jenis Disabilitas</label>
                                    </div>
                                    <div class="col-sm-9">
                                    @php
                                        if($old->is_disabilitas != 'YA'){
                                            $txt_dis = null;
                                        }else{
                                            $jenis_disabilitas = \App\Models\M_disabilitas::where('id_disabilitas', $old->id_m_disabilitas)->first();
                                            $txt_dis = $jenis_disabilitas->nama_disabilitas;
                                        }
                                    @endphp
                                    <input type="text" readonly id="disabilitas" value="{{$txt_dis}}" class="form-control" name="nm_m_pendaftar">
                                    </div>
                                </div>
                            </div>
                         
                            
                            
                            <div class="col-sm-9 offset-sm-3">
                                <a href="{{route('admin.t_wawancara.index')}}" class="btn btn-secondary waves-effect">Kembali</a>
                            </div>
                        </div>

                </div>
            </div>
        </div>
        <!-- /.col-md-6 -->

        <div class="col-lg-12">
            <div class="card card-yellow card-outline">
              <div class="card-header">
                <h5 class="m-0">Tabel Penilaian</h5>
              </div>
              <div class="card-body">
                <div class="row">
                    <div class="col-6">
                        <table class="table table-sm table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Soal CBT</th>
                                    <th>Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($cbt_user and $cbt_user->cbt_tes_user and $cbt_user->cbt_tes_user->cbt_tes_soal)
                                @foreach($cbt_user->cbt_tes_user->cbt_tes_soal as $item)
                                <tr>
                                    <td>{!! $item->cbt_soal->soal_detail !!}</td>
                                    <td>{{$item->tessoal_nilai}}</td>
                                </tr>
                                @endforeach
                                @endif
                                <tr>
                                    <td><strong>NILAI CBT</strong></td>
                                    <td><strong>{{($old->seleksi) ? $old->seleksi->total_nilai_cbt : null}}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                        
                        
                    </div>
                    <div class="col-6">
                        <table class="table table-sm table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Soal wawancara</th>
                                    <th>Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if($old->penilaian_wawancara)
                                    @foreach($old->penilaian_wawancara as $item)
                                    <tr>
                                        <td>{!!$item->soal_wawancara->soal_wawancara!!}</td>
                                        <td>{{$item->nilai}}</td>
                                    </tr>
                                    @endforeach
                                @endif
                                <tr>
                                    <td><strong>RATA - RATA NILAI</strong></td>
                                    <td><strong>{{$old->seleksi->total_nilai_wawancara}}</strong></td>
                                </tr>
                            </tbody>
                        </table>

                        <p></p>
                        <p></p>

                        <table class="table table-dark table-sm table-striped table-hover table-bordered">
                            <thead>
                                <tr>
                                    <th>Rekapitulasi Pembobotan Nilai</th>
                                    <th>Nilai</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td><strong>NILAI CBT</strong></td>
                                    <td><strong>{{$old->seleksi->total_nilai_cbt}}</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>NILAI WAWANCARA</strong></td>
                                    <td><strong>{{$old->seleksi->total_nilai_wawancara}}</strong></td>
                                </tr>
                                <tr>
                                    <td><strong>NILAI KESELURUHAN <span class="text-warning">(Wawancara 60% + CBT 40%)</span></strong><i></i></td>
                                    <td><strong>{{$old->seleksi->total_nilai_keseluruhan}}</strong></td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center"><strong>{{$old->seleksi->status}}</strong></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
              </div>
            </div>
        </div>
        
        <!-- /.col-md-6 -->
    </div>
    <!-- /.row -->
    </div><!-- /.container-fluid -->
</div>

@endsection

@section('js')
<script>
    $(document).ready( function () {
        $("#form_wawancara").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form_wawancara')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);
        swal.fire({
            title: "Pesan Konfirmasi",
            text: 'Apakah Anda yakin bahwa data yang diinputkan sudah sesuai ?',
            icon: "warning",
            showCancelButton: !0,
            confirmButtonText: "Ya",
            cancelButtonText: "Tidak",
            reverseButtons: !0
        }).then(function (e) {

            if(e.value){

                $.ajax({
                    url:"{{ route("admin.t_wawancara.update") }}",
                    method:"POST",
                    headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
                    data: data,
                    processData: false,
                    contentType: false,
                    success:function(data)
                    {
                        if($.isEmptyObject(data.error)){

                            if(data.status == true){
                                $("#submitform").removeAttr('disabled');
                                $("#submitform span").text('Submit');
                                $("form").each(function() { this.reset() });
                                swal.fire({
                                    title: "Success",
                                    text: data.message,
                                    icon: "success"
                                }).then(function() {
                                    location.href = data.redirect;
                                });
                            }else{
                                displayErrorSwal(data.message);
                            }

                        }else{
                            displayWarningSwal();
                            $("#submitform").removeAttr('disabled');
                            $("#submitform span").text('Submit');
                            $.each(data.error, function(key, value) {
                                var element = $("#" + key);
                                element.closest("div.form-control")
                                .removeClass("text-danger")
                                .addClass(value.length > 0 ? "text-danger" : "")
                                .find("#error_" + key).remove();
                                element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                            });
                        }
                    },
                    error: function(data){
                        displayErrorSwal(data.message);
                    }
                });
            }
        })
    });
});


$('.date').each(function(){
    $(this).datetimepicker({
        format: 'Y-MM-DD'
    });
})
</script>
@endsection
