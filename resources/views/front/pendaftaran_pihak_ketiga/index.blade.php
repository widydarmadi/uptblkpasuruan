@extends('front.template.index_detail')

@section('content')

<style>
  .swal2-popup {
    font-size: 1.4rem!important;
  }
</style>


@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'pendaftaran')->first();
    // dd($bg);
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Pendaftaran Calon Peserta dengan Pihak Ketiga</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Pendaftaran Calon Peserta dengan Pihak Ketiga</li>
            </ul>
        </div>
    </div>
</section>

<section class="vc_row section-wrapper vc_custom_1461778734831 vc_row-has-fill">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element vc_custom_1459910992473">
                            <div class="wpb_wrapper">
                                <form method="post" id="form">
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label class="control-label" for="nm_pemohon">Nama Pemohon (Atas Nama)</label> 
                                        <input id="nm_pemohon" name="nm_pemohon" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="jabatan_pemohon" class="control-label">Jabatan Pemohon</label> 
                                        <input id="jabatan_pemohon" name="jabatan_pemohon" type="text" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label class="control-label" for="wa_pemohon">Nomor HP / Whatsapp</label> 
                                        <input id="wa_pemohon" name="wa_pemohon" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="email_pemohon" class="control-label">Email Pemohon</label> 
                                        <input id="email_pemohon" name="email_pemohon" type="text" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label class="control-label" for="id_m_jenis_lokasi_mtu">Jenis Pihak Ketiga</label> 
                                        <select id="id_m_jenis_lokasi_mtu" name="id_m_jenis_lokasi_mtu" class="form-control">
                                          <option value="">-- pilih salah satu --</option>
                                          @foreach ($jenis_lokasi_mtu as $item)
                                          <option value="{{$item->id_m_jenis_lokasi_mtu}}">{{$item->nm_jenis_lokasi_mtu}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <div class="col-md-6">
                                        <label for="nm_lokasi_mtu" class="control-label">Nama Instansi</label> 
                                        <input id="nm_lokasi_mtu" name="nm_lokasi_mtu" type="text" class="form-control">
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label class="control-label" for="telp_lokasi_mtu">Telepon / HP Instansi</label> 
                                        <input id="telp_lokasi_mtu" name="telp_lokasi_mtu" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="email_mtu" class="control-label">Email Instansi Penyelenggara</label> 
                                        <input id="email_mtu" name="email_mtu" type="text" class="form-control">
                                      </div>
                                    </div>
                                    
                                    
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="id_m_provinsi" class="control-label">Provinsi Domisili Instansi</label> 
                                        @php
                                            $list_provinsi = \App\Models\M_provinsi::orderBy('nm_m_provinsi')->get();
                                        @endphp
                                        <select name="id_m_provinsi" id="id_m_provinsi" class="form-control">
                                          <option value="" class="value">-- pilih provinsi --</option>
                                          @foreach ($list_provinsi as $item)
                                              <option value="{{$item->id_m_provinsi}}" class="value">{{$item->nm_m_provinsi}}</option>
                                          @endforeach
                                        </select>
                                      </div>

                                      <div class="col-md-6">
                                        <label for="id_m_kota" class="control-label">Pilih Kabupaten / Kota</label> 
                                        
                                        <select name="id_m_kota" id="id_m_kota" class="form-control">
                                          <option value="" class="value">-- pilih provinsi terlebih dahulu --</option>
                                        </select>
                                      </div>
                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="id_m_kecamatan" class="control-label">Kecamatan</label> 
                                        
                                        <select name="id_m_kecamatan" id="id_m_kecamatan" class="form-control">
                                          <option value="" class="value">-- pilih kecamatan --</option>
                                        </select>
                                      </div>

                                      <div class="col-md-6">
                                        <label for="id_m_kelurahan" class="control-label">Kelurahan</label> 
                                        
                                        <select name="id_m_kelurahan" id="id_m_kelurahan" class="form-control">
                                          <option value="" class="value">-- pilih kelurahan --</option>
                                        </select>
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <div class="col-md-12">
                                        <label class="control-label" for="alamat_lokasi_mtu">Alamat Instansi</label> 
                                        <input id="alamat_lokasi_mtu" name="alamat_lokasi_mtu" type="text" class="form-control">
                                      </div>
                                    </div>

                                    {{-- <div class="form-group row">
                                      <div class="col-md-12">
                                        <label class="control-label" for="potensi_wilayah">Potensi Wilayah</label> 
                                        <input id="potensi_wilayah" name="potensi_wilayah" type="text" class="form-control">
                                      </div>
                                    </div>
                                     --}}
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="id_m_jadwal" class="control-label">Pilih pelatihan yang akan diikuti</label> 
                                        
                                        <select name="id_m_jadwal" id="id_m_jadwal" class="form-control">
                                          <option value="" >-- pilih pelatihan --</option>

                                          @if($list_gelombang)
                                            @php
                                              app()->setLocale('id');
                                            @endphp
                                            @forelse ($list_gelombang as $item)
                                              @php
                                                  $mulai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_mulai)))->isoFormat('D MMMM YYYY');
                                                  $selesai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_selesai)))->isoFormat('D MMMM YYYY');
                                              @endphp
                                              <option {{($item->id_m_jadwal == $id_m_jadwal) ? 'selected' : ''}} value="{{$item->id_m_jadwal}}">{{$item->nm_m_jadwal.' | '.$mulai.' s/d '.$selesai}}</option>
                                            @empty
                                              <option value="0">BELUM ADA JADWAL PELATIHAN DIBUKA</option>
                                            @endforelse
                                          @endif
                                          
                                        </select>
                                      </div>


                                      <div class="col-sm-6">
                                        <label class="control-label" for="file_proposal">File Proposal Pengajuan Pelatihan (PDF)</label> 
                                            <input type="file" name="file_proposal" id="file_proposal" class="form-control" />
                                      </div>

                                    </div>


                                    <div class="form-group row">
                                      
                                      <div class="col-md-5">
                                        <label for="isi_m_pengaduan" class="control-label">Masukkan ulang kode acak yang tertera di bawah ini</label> 
                                        <br>
                                        <div class="captcha">
                                        <span>{!! captcha_img() !!}</span>
                                          <button onclick="reload_captcha();" type="button" class="btn btn-info" style="padding: 7px 15px;" class="reload" id="reload">
                                              &#x21bb;
                                          </button>
                                        </div>
                                      </div>

                                      

                                    </div> 
                                    
                                    
                                    <div class="form-group row">
                                      <div class="col-md-3">
                                          <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                                      </div>
                                    </div>



                                    <div class="alert bg-warning">
                                      <span class="text-red"><strong>PERHATIAN :</strong></span>
                                      <p>Harap membaca dan memahami prosedur terkait Pelatihan dengan Pihak Ketiga dengan meng-klik <a href="{{url('konten/pendaftaran-institusional-pbk')}}">tautan ini</a> sebelum melanjutkan pendaftaran.</p>
                                    </div>

                                    <input type="hidden" value="{{(request()->filled('grup_id')) ? request()->get('grup_id') : null}}" id="cbt_grup_id" name="cbt_grup_id" />



                                    <div class="form-group">
                                      <button type="submit" id="submitform" class="btn btn-success"><span>Kirim Formulir</span></button>
                                    </div>
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<p></p>
<p></p>
<p></p>
<p></p>

@endsection



@section('js')
<script>
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        var catValue = 'SWA';
        data.append('cat', catValue);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("pendaftaran_pihak_ketiga_post") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){
                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Kirim Formulir');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                        reload_captcha();
                    }

                }else{
                    displayWarningSwal();
                    reload_captcha();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Kirim Formulir');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
                reload_captcha();
            }
        });
    });

    $('#is_disabilitas').change(function(){
      var is_disabilitas = $('#is_disabilitas').val();
      $("#id_m_disabilitas").html('');
      if(is_disabilitas == 'YA'){
          $.ajax({
              type: 'post',
              url: '{{route('get_jenis_disabilitas')}}',
              data:{
                  is_disabilitas:is_disabilitas
              },
              success: function (data) {
                  $("#id_m_disabilitas").html(data.html);
              }
          });
      }
    })


    
    $('#id_m_kategori_kejuruan').change(function(){
      var id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
      $("#id_m_program_pelatihan").html('');
      $("#id_m_jadwal").html('');
      $("#cbt_grup_id").val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_combo_kejuruan')}}',
          data:{
            id_m_kategori_kejuruan:id_m_kategori_kejuruan
          },
          success: function (data) {
              $("#id_m_kejuruan").html(data.html);
          }
      });
    })


    $('#id_m_kejuruan').change(function(){
      var id_m_kejuruan = $('#id_m_kejuruan').val();
      $("#id_m_program_pelatihan").html('');
      $("#id_m_jadwal").html('');
      $("#cbt_grup_id").val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_pelatihan_dibuka')}}',
          data:{
            id_m_kejuruan:id_m_kejuruan
          },
          success: function (data) {
              $("#id_m_program_pelatihan").html(data.html);
          }
      });
    })
    
    
    $('#id_m_program_pelatihan').change(function(){
      var id_m_program_pelatihan = $(this).val();
      $("#id_m_jadwal").html('');
      $("#cbt_grup_id").val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_gelombang_dibuka')}}',
          data:{
            id_m_program_pelatihan:id_m_program_pelatihan
          },
          success: function (data) {
              $("#id_m_jadwal").html(data.html);
          }
      });
    })
    
    
    $('#id_m_jadwal').change(function(){
      var id_m_jadwal = $(this).val();
      $.ajax({
          type: 'post',
          url: '{{route('get_cbt_grup_id')}}',
          data:{
            id_m_jadwal:id_m_jadwal
          },
          success: function (data) {
            if(data.valuez){
              $("#cbt_grup_id").val(data.valuez);
            }else{
              $("#cbt_grup_id").val('');
            }
          }
      });
    })




    $('#id_m_provinsi').change(function(){
      var id_m_provinsi = $('#id_m_provinsi').val();
      $('#id_m_kota').val('');
      $('#id_m_kecamatan').val('');
      $('#id_m_kelurahan').val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_combo_kota')}}',
          data:{
            id_m_provinsi:id_m_provinsi
          },
          success: function (data) {
              $("#id_m_kota").html(data.html);
          }
      });
    })

    $('#id_m_kota').change(function(){
      var id_m_kota = $('#id_m_kota').val();
      $('#id_m_kecamatan').val('');
      $('#id_m_kelurahan').val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_combo_kecamatan')}}',
          data:{
            id_m_kota:id_m_kota
          },
          success: function (data) {
              $("#id_m_kecamatan").html(data.html);
          }
      });
    })
    
    $('#id_m_kecamatan').change(function(){
      var id_m_kecamatan = $('#id_m_kecamatan').val();
      $('#id_m_kelurahan').val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_combo_kelurahan')}}',
          data:{
            id_m_kecamatan:id_m_kecamatan
          },
          success: function (data) {
              $("#id_m_kelurahan").html(data.html);
          }
      });
    })
</script>
@endsection