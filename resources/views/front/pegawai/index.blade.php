@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'daftar-kejuruan')->first();
    // dd($bg);
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Profil Pegawai</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Profil Pegawai</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                @php
                                    $group = \App\Models\M_pegawai::select('kategori')
                                            ->where('aktif_m_pegawai', '1')
                                            ->orderBy('urut_kategori')
                                            ->groupBy('kategori','urut_kategori')->distinct('kategori')->get();
                                @endphp
                                
                                @foreach ($group as $kategori)
                                <h2>{{$kategori->kategori}}</h2>
                                    
                                    @php
                                        $list_pegawai = \App\Models\M_pegawai::where('aktif_m_pegawai', '1')->where('kategori', $kategori->kategori)->orderBy('order_tampil')->get();
                                        $i = 1;
                                    @endphp
                                    @foreach ($list_pegawai as $item)
                                    <table class="table table-sm table-striped table-hover table-bordered mb-3">
                                        <tr>
                                            <td width="35%">Nama</td>
                                            <td width="65%"><strong>{{$item->nm_m_pegawai}}</strong> 
                                            <!--({{$i++}})-->
                                            </td>
                                        </tr>
                                        @if($item->kategori == 'PEGAWAI UPT BLK PASURUAN' or $item->kategori == 'INSTRUKTUR PNS')
                                        <tr>
                                            <td>Pangkat / Golongan</td>
                                            <td>{{$item->pangkat_m_pegawai . ' ( ' . $item->golongan_m_pegawai . ' )'}}</td>
                                        </tr>
                                        @endif
                                        <tr>
                                            <td>Jabatan</td>
                                            <td>{{$item->jabatan_m_pegawai}}</td>
                                        </tr>
                                        <tr>
                                            <td>Unit Kerja</td>
                                            <td>{{$item->unit_kerja_m_pegawai}}</td>
                                        </tr>
                                    </table>
                                    <br/>
                                    @endforeach
                                
                                @endforeach
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>
$('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {

    if ($(window).innerWidth() < winWidth) {
        $('.carousel-inner>.item>img').css({
            'min-width': winWidth, 'width': winWidth
        });
    }
    else {
        winWidth = $(window).innerWidth();
        $('.carousel-inner>.item>img').css({
            'min-width': '', 'width': ''
        });
    }
});
</script>
@endsection