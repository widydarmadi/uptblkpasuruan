@extends('front.template.index')

@section('content')

<section class="page-title text-center" style="background-image: url({{asset('assets/front/images/bgpattern-purple.jpg')}});" role="banner">
    <div></div>
    <div class="container">
        <h1 style="">{{$konten->judul_m_konten}}</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">{{$konten->judul_m_konten}}</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                {!!$konten->narasi_m_konten!!}
                                <p></p>
                                <p></p>
                                @if ($konten->photo)
                                    <img src="{{asset('storage'.'/'.$konten->photo)}}" width="80%" />
                                @endif
                                
                                <br/>
                                <br/>
                                
                                @if ($konten->photo2)
                                    <img src="{{asset('storage'.'/'.$konten->photo2)}}" width="80%" />
                                @endif
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>
$('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {

    if ($(window).innerWidth() < winWidth) {
        $('.carousel-inner>.item>img').css({
            'min-width': winWidth, 'width': winWidth
        });
    }
    else {
        winWidth = $(window).innerWidth();
        $('.carousel-inner>.item>img').css({
            'min-width': '', 'width': ''
        });
    }
});
</script>
@endsection