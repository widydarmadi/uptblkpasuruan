@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'pendaftaran')->first();
    // dd($bg);
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Pendaftaran Calon Peserta UPT BLK Pasuruan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Pendaftaran Calon Peserta UPT BLK Pasuruan</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                <p>
                                    UPT Balai Latihan Kerja Pasuruan merupakan salah satu lembaga pelatihan milik 
                                    Pemerintah Provinsi Jawa Timur yang berfokus pada pengembangan kompetensi calon tenaga kerja melalui program-program pelatihan yang disusun berdasarkan kebutuhan dunia industri. Sehingga, setelah mengikuti pelatihan, para alumni diharapkan mampu bersaing secara global.
                                </p>
                               
                               <div class="row">
                                   @foreach ($kategori_kejuruan as $item)
                                   <div class="col-md-4">
                                       <div class="kejuruan-list">
                                            <H4 class="text-center theme-color">{{$item->nm_m_kategori_kejuruan}}</H4>
                                            @if(file_exists(public_path('storage').'/'.$item->photo) && isset($item->photo))
                                            <img style="height: 215px;" src="{{asset('storage'.'/'.$item->photo)}}" width="100%" />
                                            @else
                                            <img src="{{asset('assets/front/images/noimage.png')}}" width="100%" />
                                            @endif

                                            <p class="text-center"><strong>PROGRAM KEAHLIAN</strong></p>
                                            @php
                                                $list = \App\Models\M_kejuruan::where('id_m_kategori_kejuruan', $item->id_m_kategori_kejuruan)->orderBy('nm_m_kejuruan')->get();
                                            @endphp
                                            <ul class="list-group">
                                            @foreach ($list as $item)
                                                <li class="list-group-item text-center">{{$item->nm_m_kejuruan}}</li>
                                            @endforeach
                                            </ul>
                                        </div>
                                   </div>
                                   @endforeach
                               </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')

@endsection