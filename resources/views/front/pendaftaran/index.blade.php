@extends('front.template.index_detail')

@section('content')

<style>
  .swal2-popup {
    font-size: 1.4rem!important;
  }
</style>


@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'pendaftaran')->first();
    // dd($bg);
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Pendaftaran Calon Peserta Institusional</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Pendaftaran Calon Peserta Institusional</li>
            </ul>
        </div>
    </div>
</section>

<section class="vc_row section-wrapper vc_custom_1461778734831 vc_row-has-fill">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element vc_custom_1459910992473">
                            <div class="wpb_wrapper">
                                <form method="post" id="form">
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label class="control-label" for="nm_m_pendaftar">Nama Lengkap</label> 
                                        <input id="nm_m_pendaftar" name="nm_m_pendaftar" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="nik_m_pendaftar" class="control-label">NIK</label> 
                                        <input id="nik_m_pendaftar" maxlength="16" name="nik_m_pendaftar" type="text" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      
                                      <div class="col-md-6">
                                        <label for="id_m_provinsi" class="control-label">Provinsi Domisili</label> 
                                        @php
                                            $list_provinsi = \App\Models\M_provinsi::orderBy('nm_m_provinsi')->get();
                                        @endphp
                                        <select name="id_m_provinsi" id="id_m_provinsi" class="form-control">
                                          <option value="" class="value">-- pilih provinsi --</option>
                                          @foreach ($list_provinsi as $item)
                                              <option value="{{$item->id_m_provinsi}}" class="value">{{$item->nm_m_provinsi}}</option>
                                          @endforeach
                                        </select>
                                      </div>

                                      <div class="col-md-6">
                                        <label for="id_m_kota" class="control-label">Pilih Kabupaten / Kota</label> 
                                        
                                        <select name="id_m_kota" id="id_m_kota" class="form-control">
                                          <option value="" class="value">-- pilih provinsi terlebih dahulu --</option>
                                        </select>
                                      </div>


                                    </div>
                                    
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="alamat_m_pendaftar" class="control-label">Alamat Rumah</label> 
                                        <input id="alamat_m_pendaftar" name="alamat_m_pendaftar" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="jk_m_pendaftar" class="control-label">Jenis Kelamin</label> 
                                        <select id="jk_m_pendaftar" name="jk_m_pendaftar" class="select form-control">
                                          <option value="">pilih salah satu</option>
                                          <option value="LAKI-LAKI">LAKI-LAKI</option>
                                          <option value="PEREMPUAN">PEREMPUAN</option>
                                        </select>
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      
                                      <div class="col-md-6">
                                        <label for="id_m_pendidikan" class="control-label">Pendidikan Terakhir</label> 
                                        @php
                                            $list_pendidikan = \App\Models\M_pendidikan::where('aktif_m_pendidikan','1')->get();
                                        @endphp
                                        
                                        <select name="id_m_pendidikan" id="id_m_pendidikan" class="form-control" required>
                                        @foreach ($list_pendidikan as $item)
                                              <option value="{{$item->id_m_pendidikan}}">{{$item->nm_m_pendidikan}}</option>
                                          @endforeach
                                        </select>
                                      </div>

                                      <div class="col-md-6">
                                        <label for="nm_jurusan" class="control-label">Silahkan tulis Jurusan bagi yang lulusan SMA, SMK, D1, D3, dan S1</label> 
                                        <input id="nm_jurusan" name="nm_jurusan" type="text" class="form-control">
                                      </div>


                                    </div>


                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="tempat_lahir_m_pendaftar" class="control-label">Tempat Lahir</label> 
                                        <input id="tempat_lahir_m_pendaftar" name="tempat_lahir_m_pendaftar" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="tgl_lahir_m_pendaftar" class="control-label">Tanggal Lahir</label> 
                                        <input id="tgl_lahir_m_pendaftar" name="tgl_lahir_m_pendaftar" type="date" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="wa_m_pendaftar" class="control-label">No. HP (yang terdaftar No.WA Aktif)</label> 
                                        <input id="wa_m_pendaftar" name="wa_m_pendaftar" type="text" class="form-control">
                                      </div>
                                      <div class="col-md-6">
                                        <label for="email_m_pendaftar" class="control-label">Email yang aktif</label> 
                                        <input id="email_m_pendaftar" name="email_m_pendaftar" type="text" class="form-control">
                                      </div>
                                    </div>
                                    
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="quest_pernah_blk" class="control-label">Pernah mengikuti kursus BLK ?</label> 
                                        <select name="quest_pernah_blk" id="quest_pernah_blk" class="form-control" required>
                                          <option value="BELUM">BELUM PERNAH</option>
                                          <option value="PERNAH">PERNAH</option>
                                        </select>
                                      </div>
                                      <div class="col-md-6">
                                        <label for="quest_masih_bekerja" class="control-label">Masih Bekerja ?</label> 
                                        <select name="quest_masih_bekerja" id="quest_masih_bekerja" class="form-control" required>
                                          <option value="TIDAK">TIDAK</option>
                                          <option value="YA">YA</option>
                                        </select>
                                      </div>
                                    </div>
                                    
                                    
                                    {{-- <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="id_m_kategori_kejuruan" class="control-label">Pilih Kategori Kejuruan</label> 
                                        @php
                                            $kat = \App\Models\M_kategori_kejuruan::where('aktif','1')->orderBy('nm_m_kategori_kejuruan')->get();
                                        @endphp
                                        <select name="id_m_kategori_kejuruan" id="id_m_kategori_kejuruan" class="form-control" required>
                                          <option value="" >-- Pilih Kategori Kejuruan --</option>
                                          @foreach ($kat as $item)
                                              <option {{($id_m_kategori_kejuruan == $item->id_m_kategori_kejuruan) ? 'selected' : ''}} value="{{$item->id_m_kategori_kejuruan}}">{{$item->nm_m_kategori_kejuruan}}</option>
                                          @endforeach
                                        </select>
                                      </div>

                                      <div class="col-md-6">
                                        <label for="id_m_kejuruan" class="control-label">Pilih Sub Kejuruan</label> 
                                        
                                        <select name="id_m_kejuruan" id="id_m_kejuruan" class="form-control">
                                          <option value="" >-- pilih kategori kejuruan terlebih dahulu --</option>
                                          @if($list_kejuruan)
                                          @foreach ($list_kejuruan as $item)
                                            <option {{($item->id_m_kejuruan == $id_m_kejuruan) ? 'selected' : ''}} value="{{$item->id_m_kejuruan}}">{{$item->nm_m_kejuruan}}</option>
                                          @endforeach
                                          @endif
                                        </select>
                                      </div>


                                    </div> --}}

                                    
                                    <div class="form-group row">

                                      {{-- <div class="col-md-6">
                                        <label for="id_m_program_pelatihan" class="control-label">Pilih Program Pelatihan</label> 
                                        
                                        <select name="id_m_program_pelatihan" id="id_m_program_pelatihan" class="form-control select2">
                                          <option value="" >-- pilih program pelatihan --</option>
                                          @if($list_program_pelatihan)
                                            @forelse ($list_program_pelatihan as $item)
                                              <option {{($item->id_m_program_pelatihan == $id_m_program_pelatihan) ? 'selected' : ''}} value="{{$item->id_m_program_pelatihan}}">{{$item->nm_m_program_pelatihan}}</option>
                                            @empty
                                              <option value="0">BELUM ADA PROGRAM UNTUK KEJURUAN INI</option>
                                            @endforelse
                                          @endif
                                        </select>
                                      </div> --}}


                                      <div class="col-md-6">
                                        <label for="id_m_jadwal" class="control-label">Pilih pelatihan yang akan diikuti</label> 
                                        
                                        <select name="id_m_jadwal" id="id_m_jadwal" class="form-control">
                                          <option value="" >-- pilih pelatihan --</option>

                                          @if($list_gelombang)
                                            @php
                                              app()->setLocale('id');
                                            @endphp
                                            @forelse ($list_gelombang as $item)
                                              @php
                                                  $mulai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_mulai)))->isoFormat('D MMMM YYYY');
                                                  $selesai = \Carbon\Carbon::parse(date('d-m-Y', strtotime($item->tgl_selesai)))->isoFormat('D MMMM YYYY');
                                              @endphp
                                              <option {{($item->id_m_jadwal == $id_m_jadwal) ? 'selected' : ''}} value="{{$item->id_m_jadwal}}">{{$item->nm_m_jadwal.' | '.$mulai.' s/d '.$selesai}}</option>
                                            @empty
                                              <option value="0">BELUM ADA JADWAL PELATIHAN DIBUKA</option>
                                            @endforelse
                                          @endif
                                          
                                        </select>
                                      </div>

                                      <div class="col-md-6">
                                        <label for="quest_minat_setelah_kursus" class="control-label">Minat Sesudah Menyelesaikan Pelatihan</label> 
                                        <select name="quest_minat_setelah_kursus" id="quest_minat_setelah_kursus" class="form-control" required>
                                          <option value="BEKERJA">BEKERJA</option>
                                          <option value="WIRASWASTA">WIRASWASTA</option>
                                        </select>
                                      </div>

                                    </div>

                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="is_disabilitas" class="control-label">Apakah Termasuk Penyandang Disabilitas ?</label> 

                                        <select name="is_disabilitas" id="is_disabilitas" class="form-control">
                                          <option value="">-- pilih satu --</option>
                                          <option value="TIDAK">TIDAK</option>
                                          <option value="YA">YA</option>
                                        </select>
                                      </div>
                                      <div class="col-md-6">
                                        <label for="id_m_disabilitas" class="control-label">Jika penyandang disabilitas, silahkan mengisi jenis disabilitasnya</label> 

                                        <select name="id_m_disabilitas" id="id_m_disabilitas" class="form-control">
                                            <option value="">-- pilih jenis disabilitas --</option>
                                        </select>
                                      </div>
                                    </div>
                                    
                            


                                    <div class="form-group row">
                                      
                                      <div class="col-md-5">
                                        <label for="isi_m_pengaduan" class="control-label">Masukkan ulang kode acak yang tertera di bawah ini</label> 
                                        <br>
                                        <div class="captcha">
                                        <span>{!! captcha_img() !!}</span>
                                          <button onclick="reload_captcha();" type="button" class="btn btn-info" style="padding: 7px 15px;" class="reload" id="reload">
                                              &#x21bb;
                                          </button>
                                        </div>
                                      </div>

                                      

                                    </div> 
                                    
                                    
                                    <div class="form-group row">
                                      <div class="col-md-3">
                                          <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                                      </div>
                                    </div>



                                    <div class="alert bg-warning">
                                      <span class="text-red"><strong>PERHATIAN :</strong></span>
                                      <p>Harap membaca dan memahami prosedur terkait Pelatihan Institusional dengan meng-klik <a href="{{url('konten/pendaftaran-institusional-pbk')}}">tautan ini</a> sebelum melanjutkan pendaftaran.</p>
                                    </div>

                                    <input type="hidden" value="{{(request()->filled('grup_id')) ? request()->get('grup_id') : null}}" id="cbt_grup_id" name="cbt_grup_id" />



                                    <div class="form-group">
                                      <button type="submit" id="submitform" class="btn btn-success"><span>Kirim Formulir</span></button>
                                    </div>
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<p></p>
<p></p>
<p></p>
<p></p>

@endsection



@section('js')
<script>
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        var catValue = '{{request()->get('cat')}}';
        data.append('cat', catValue);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("pendaftaran_post") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){
                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Kirim Formulir');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                        reload_captcha();
                    }

                }else{
                    displayWarningSwal();
                    reload_captcha();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Kirim Formulir');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
                reload_captcha();
            }
        });
    });

    $('#is_disabilitas').change(function(){
      var is_disabilitas = $('#is_disabilitas').val();
      $("#id_m_disabilitas").html('');
      if(is_disabilitas == 'YA'){
          $.ajax({
              type: 'post',
              url: '{{route('get_jenis_disabilitas')}}',
              data:{
                  is_disabilitas:is_disabilitas
              },
              success: function (data) {
                  $("#id_m_disabilitas").html(data.html);
              }
          });
      }
    })


    $('#id_m_provinsi').change(function(){
      var id_m_provinsi = $('#id_m_provinsi').val();
      $.ajax({
          type: 'post',
          url: '{{route('get_combo_kota')}}',
          data:{
            id_m_provinsi:id_m_provinsi
          },
          success: function (data) {
              $("#id_m_kota").html(data.html);
          }
      });
    })
    
    
    $('#id_m_kategori_kejuruan').change(function(){
      var id_m_kategori_kejuruan = $('#id_m_kategori_kejuruan').val();
      $("#id_m_program_pelatihan").html('');
      $("#id_m_jadwal").html('');
      $("#cbt_grup_id").val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_combo_kejuruan')}}',
          data:{
            id_m_kategori_kejuruan:id_m_kategori_kejuruan
          },
          success: function (data) {
              $("#id_m_kejuruan").html(data.html);
          }
      });
    })


    $('#id_m_kejuruan').change(function(){
      var id_m_kejuruan = $('#id_m_kejuruan').val();
      $("#id_m_program_pelatihan").html('');
      $("#id_m_jadwal").html('');
      $("#cbt_grup_id").val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_pelatihan_dibuka')}}',
          data:{
            id_m_kejuruan:id_m_kejuruan
          },
          success: function (data) {
              $("#id_m_program_pelatihan").html(data.html);
          }
      });
    })
    
    
    $('#id_m_program_pelatihan').change(function(){
      var id_m_program_pelatihan = $(this).val();
      $("#id_m_jadwal").html('');
      $("#cbt_grup_id").val('');
      $.ajax({
          type: 'post',
          url: '{{route('get_gelombang_dibuka')}}',
          data:{
            id_m_program_pelatihan:id_m_program_pelatihan
          },
          success: function (data) {
              $("#id_m_jadwal").html(data.html);
          }
      });
    })
    
    
    $('#id_m_jadwal').change(function(){
      var id_m_jadwal = $(this).val();
      $.ajax({
          type: 'post',
          url: '{{route('get_cbt_grup_id')}}',
          data:{
            id_m_jadwal:id_m_jadwal
          },
          success: function (data) {
            if(data.valuez){
              $("#cbt_grup_id").val(data.valuez);
            }else{
              $("#cbt_grup_id").val('');
            }
          }
      });
    })
</script>
@endsection