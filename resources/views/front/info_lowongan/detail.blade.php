@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'lowongan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">{{$lowongan->nm_posisi_m_lowongan}}</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li><a href="{{url('info-lowongan')}}">Lowongan</a></li>
                <li class="active">{{$lowongan->nm_posisi_m_lowongan}}</li>
            </ul>
        </div>
    </div>
</section>

<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                


                                @php
                                    app()->setLocale('id');
                                @endphp
                                <div class="col-md-12" style="margin-bottom: 30px;">

                                    
                                    <br>
                                    <h3 class="text-danger">{{\Str::ucfirst($lowongan->nm_posisi_m_lowongan)}}</h3>
                                    <h5>{{\Str::ucfirst($lowongan->perusahaan->nm_m_perusahaan)}}</h5>
                                    <p>Lowongan Dibuka : 
                                        {{\Carbon\Carbon::parse($lowongan->date_start)->isoFormat('DD MMMM YYYY')}}
                                        s/d
                                        {{\Carbon\Carbon::parse($lowongan->date_end)->isoFormat('DD MMMM YYYY')}}
                                    </p>

                                    {!!$lowongan->deskripsi_m_lowongan!!}

                                    @if(isset($lowongan->photo))
                                    <img src="{{asset('storage'.'/'.$lowongan->photo)}}" alt="{{$lowongan->nm_m_perusahaan}}" width="80%">
                                    @else
                                    <img src="{{asset('assets/front/images/altimage_lowongan.jpg')}}" alt="{{$lowongan->nm_m_perusahaan}}" width="80%">
                                    @endif
                                    <br>
                                </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection
