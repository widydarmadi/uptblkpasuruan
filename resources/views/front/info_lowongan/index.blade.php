@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'lowongan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Lowongan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Lowongan</li>
            </ul>
        </div>
    </div>
</section>



<div class="blog-wrapper blog-grid content-wrapper">
    <div class="container">
        <div class="row">
            <div id="main" class="posts-content masonry-wrap clearfix masonry" role="main" style="position: relative; height: 1470.12px;">
                
                @foreach ($lowongan as $item)
                <div class="col-md-4 col-sm-6 col-xs-12 masonry-column masonry-brick" style="position: absolute; left: 0px; top: 0px;">
                    <article id="post-3414" class="post-wrapper post-3414 post type-post status-publish format-standard has-post-thumbnail hentry category-standard">
                        <header class="featured-wrapper">
                            {{-- <div class="post-author">
                                <img
                                    alt=""
                                    src="http://1.gravatar.com/avatar/7094fab31759a3e2fef636938f9956a7?s=60&amp;d=mm&amp;r=g"
                                    srcset="http://1.gravatar.com/avatar/7094fab31759a3e2fef636938f9956a7?s=120&amp;d=mm&amp;r=g 2x"
                                    class="avatar avatar-60 photo"
                                    height="60"
                                    width="60"
                                />
                            </div> --}}
                            <div class="post-thumbnail">
                                <img
                                    width="750"
                                    style="height:300px;"
                                    @if(isset($item->photo))
                                    src="{{asset('storage'.'/'.$item->photo)}}" 
                                    @else
                                    src="{{asset('assets/front/images/altimage_lowongan.jpg')}}" 
                                    @endif
                                    {{-- src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-9-750x350.jpg" --}}
                                    class="img-responsive wp-post-image"
                                    alt="Gear Up your Website For Mobile Devices"
                                />
                            </div>
                            <ul class="post-meta">
                                <li>
                                    <span class="author vcard"> <i class="fa fa-user"></i><a class="url fn n" href="#">admin</a> </span>
                                </li>
                            </ul>
                        </header>
                        <div class="blog-content" style="height: 200px;">
                            <div class="entry-header">
                                <h4 class="entry-title"><a href="{{url('info-lowongan/detail/'.$item->id_m_lowongan)}}" rel="bookmark">{{$item->nm_posisi_m_lowongan}}</a></h4>
                            </div>
                            <div class="entry-content">
                                <strong>{{\Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM YYYY')}}</strong>
                                <p>
                                    <h5>{{\Str::ucfirst($item->perusahaan->nm_m_perusahaan)}}</h5>
                                    <p>Lowongan Dibuka : 
                                        {{\Carbon\Carbon::parse($item->date_start)->isoFormat('DD MMMM YYYY')}}
                                        s/d
                                        {{\Carbon\Carbon::parse($item->date_end)->isoFormat('DD MMMM YYYY')}}
                                    </p>
                                    <a href="{{url('info-lowongan/detail/'.$item->id_m_lowongan)}}" class="more-link"><span class="readmore">Selengkapnya</span></a>
                                </p>
                            </div>
                        </div>
                        <footer class="entry-footer clearfix">
                            <ul class="entry-meta">
                                <li>&nbsp;</li>
                            </ul>
                        </footer>
                    </article>
                </div>
                @endforeach
                
                

                <div class="col-md-12 text-center masonry-brick" style="position: absolute; left: 0px; top: 1370px;">
                    {{$lowongan->links()}}
                </div>
            </div>
        </div>
    </div>
</div>


@endsection

@section('js')
<script>
$('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {

    if ($(window).innerWidth() < winWidth) {
        $('.carousel-inner>.item>img').css({
            'min-width': winWidth, 'width': winWidth
        });
    }
    else {
        winWidth = $(window).innerWidth();
        $('.carousel-inner>.item>img').css({
            'min-width': '', 'width': ''
        });
    }
});
</script>
@endsection