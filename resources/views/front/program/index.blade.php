@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'program-kegiatan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Program Kegiatan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Program Kegiatan</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                @php
                                    app()->setLocale('id');
                                @endphp
                                @foreach ($program_kegiatan as $item)
                                    <div class="col-md-12" style="margin-bottom: 30px;">
                                        <h3 class="text-danger">{{\Str::ucfirst($item->nm_m_program_kegiatan)}}</h3>
                                        <p>Diselenggarakan Pada : 
                                            {{\Carbon\Carbon::parse($item->tgl_mulai)->isoFormat('DD MMMM YYYY')}}
                                            s/d
                                            {{\Carbon\Carbon::parse($item->tgl_selesai)->isoFormat('DD MMMM YYYY')}}
                                        </p>
                                        <p>Sumber Pendanaan : {{$item->sumber_dana->nm_m_sumber_dana}}</p>
                                        <hr>
                                    </div>
                                @endforeach

                                {{$program_kegiatan->links()}}
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>
$('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {

    if ($(window).innerWidth() < winWidth) {
        $('.carousel-inner>.item>img').css({
            'min-width': winWidth, 'width': winWidth
        });
    }
    else {
        winWidth = $(window).innerWidth();
        $('.carousel-inner>.item>img').css({
            'min-width': '', 'width': ''
        });
    }
});
</script>
@endsection