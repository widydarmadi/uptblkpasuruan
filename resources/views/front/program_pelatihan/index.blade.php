@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'program-pelatihan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Program Pelatihan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Program Pelatihan</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">

                        <h2 class="theme-color">Ikuti pelatihan dan tingkatkan keterampilan mu sekarang !</h2>
                        <br />
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">

                                <div class="row" style="padding-bottom: 20px;">
                                    
                                    <div class="col-md-4">
                                        <select name="tipe" id="tipe" class="form-control">
                                            <option value="">-- SEMUA TIPE PELATIHAN --</option>
                                            @foreach ($tipe as $item)
                                                <option {{(request()->get('tipe') == $item->id_m_tipe_pelatihan) ? 'selected' : null}} value="{{$item->id_m_tipe_pelatihan}}">{{$item->nm_m_tipe_pelatihan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-4">
                                        <select name="kejuruan" id="kejuruan" class="form-control">
                                            <option value="">-- SEMUA KEJURUAN --</option>
                                            @foreach ($kejuruan as $item)
                                                <option {{(request()->get('kejuruan') == $item->id_m_kejuruan) ? 'selected' : null}} value="{{$item->id_m_kejuruan}}">{{$item->nm_m_kejuruan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <button id="filter" name="filter" class="btn btn-success" style="padding: 9px;">FILTER</button>
                                        <button id="reset" onclick="location.href='{{route('program_pelatihan_home')}}'" name="reset" class="btn btn-primary" style="padding: 9px;">RESET</button>
                                    </div>
                                </div>

                               @php
                                   $x = 1;
                               @endphp
                               <div class="row" style="padding-bottom: 20px;">

                               @forelse ($program_pelatihan as $item)
                                <div class="col-md-4" style="">
                                    <div class="boxpelatihan aa" style="margin: 10px 0px; height: 530px;">
                                        @if ($item->foto)
                                            <img src="{{asset('storage'.'/'.$item->foto)}}" width="100%" height="200" />
                                        @else
                                            <img src="{{asset('front/images/noimage.png')}}" width="100%" height="200"  />
                                        @endif
                                        <div style="padding: 15px;">
                                                <h3>{{$item->nm_m_program_pelatihan}}</h3>
                                                {{$item->jampel}}<br>
                                                <i>Kategori Pelatihan : {{$item->tipe_pelatihan->nm_m_tipe_pelatihan}}</i>
                                                <p>{{substr($item->deskripsi, 0, 100)}} ...</p>
                                            <ul>
                                                <li style="list-style: none; margin-left:-40px;">
                                                    <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                                    <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                                    <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                                    <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                                    <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 4.9
                                                </li>
                                            </ul>
                                        </div>
                                        <a class="btn btn-danger w-100" style="margin: 0px 15px;" href="{{route('program_pelatihan_detil', ['id' => $item->id_m_program_pelatihan])}}" tabindex="-1"> Selengkapnya </a>
                                        
                                    </div>
                               </div>



                               @empty
                               <p>Mohon maaf, saat ini belum ada program pelatihan yang dibuka.</p>
                               @endforelse

                               
                            </div>
                        </div>
                    </div>
                    {{$program_pelatihan->appends($_GET)->links()}}
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>

$('#filter').click(function(){    
    var tipe = $('#tipe').val();
    var kejuruan = $('#kejuruan').val();
    location.href = '{{route('program_pelatihan_home')}}?kejuruan=' + kejuruan + '&tipe=' + tipe;
})
</script>
@endsection