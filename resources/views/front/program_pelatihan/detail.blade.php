@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'program-pelatihan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Detail Pelatihan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Detail Pelatihan</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">

                        @php
                            if($old->id_m_tipe_pelatihan == 1){
                                $link_daftar = 'pendaftaran';
                            }else{
                                $link_daftar = 'pendaftaran_mtu';
                            }
                        @endphp

                        <h2>{{$old->nm_m_program_pelatihan}}</h2>
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                <div class="col-md-3">
                                    Kejuruan :<br>
                                    <strong>{{$old->kejuruan->nm_m_kejuruan}}</strong>
                                </div>
                                <div class="col-md-2">
                                    Durasi :<br>
                                    <strong>{{$old->jampel}}</strong>
                                </div>
                                <div class="col-md-2">
                                    Sumber Anggaran :<br>
                                    <strong>{{$old->sumber_dana->nm_m_sumber_dana}}</strong>
                                </div>
                                <div class="col-md-2">
                                    Rating :<br>
                                    <ul>
                                        <li style="list-style: none; margin-left:-40px;">
                                            <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                            <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                            <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                            <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 
                                            <i style="display: inline-block!important; font-size: 17px; color: orange; display:flex" class="fa fa-star"></i> 4.9
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-md-3">
                                    Tipe Pelatihan :<br>
                                    <strong>{{$old->tipe_pelatihan->nm_m_tipe_pelatihan}}</strong>
                                </div>

                                <div class="col-md-8">
    
                                <br />
                                    @if($old->foto)
                                    <img width="100%" src="{{asset('storage'.'/'.$old->foto)}}" />
                                    @else
                                    <img width="100%" src="{{asset('assets/front/images/avatar-pelatihan.jpg')}}" />
                                    @endif  
                                    <br />
                                    <br />
                                    <h2 class="theme-color">Deskripsi</h2>

                                    <p>{{$old->deskripsi}}<br></p>
                                    <a href="{{route('program_pelatihan_home')}}" style="padding: 10px 20px;" class="btn btn-primary">Kembali</a>
                                    <br>
                                </div>
                                <div class="col-md-4">
                                <br />
                                <h3>Unduh Program</h3>
                                @if($old->file_pdf)
                                <p>Anda dapat mengunduh file dari program pelatihan ini dengan cara klik tombol dibawah ini.</p>
                                <a href="{{asset('storage'.'/'.$old->file_pdf)}}" target="_blank" style="padding: 10px 20px;" class="btn btn-primary">Unduh Program</a>
                                @else
                                <p>Dokumen pelatihan belum tersedia.</p>
                                @endif  
                                </div>

                                {{-- @if($old->jadwal)

                                @php
                                    app()->setLocale('id');
                                @endphp
                                @forelse($old->jadwal as $jadwal)

                                    <div class="col-md-4 ">
                                        <div class="boxpelatihan" style="padding: 15px;">

                                            <h3>{{$jadwal->nm_m_jadwal}}</h3>
                                            <p>{{$jadwal->tahun}}</p>
                                            <p>
                                                {{($jadwal->tgl_mulai) ? \Carbon\Carbon::parse($jadwal->tgl_mulai)->isoFormat('D MMMM YYYY') : '-'}} 
                                                s/d 
                                                {{($jadwal->tgl_selesai) ? \Carbon\Carbon::parse($jadwal->tgl_selesai)->isoFormat('D MMMM YYYY') : '-'}} 
                                            </p>
                                            
                                            @if($old->id_m_tipe_pelatihan == 1)

                                            <a class="btn btn-{{($jadwal->status == 'OPEN') ? 'danger' : 'secondary disabled'}} w-100 " 
                                                @if($jadwal->status == 'CLOSE')
                                                href="#"
                                                @else
                                                href="{{route($link_daftar, ['id_m_jadwal' => $jadwal->id_m_jadwal, 'grup_id' => $jadwal->grup_id, 'cat' => 'PBK'])}}"
                                                @endif
                                                tabindex="-1"> 
                                                {{($jadwal->status == 'OPEN') ? 'Ikuti pelatihan ini' : 'Pendaftaran sudah ditutup'}}
                                            </a>

                                            @else 
                                            <a class="btn btn-danger w-100 " 
                                                href="{{route($link_daftar, ['id_m_jadwal' => $jadwal->id_m_jadwal, 'grup_id' => $jadwal->grup_id, 'cat' => 'PBK'])}}"
                                                tabindex="-1"> Ikuti pelatihan ini
                                            </a>
                                            @endif
                                        </div>
                                </div>

                                @empty

                                @endforelse

                                @endif --}}
                               
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')

@endsection