<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link type="text/css" media="all" href="{{asset('assets/front/css')}}/autoptimize_edf689da60de82ae2d4e0ff84c0e5516.css" rel="stylesheet" />
        <link type="text/css" media="only screen and (max-width: 768px)" href="{{asset('assets/front/css')}}/autoptimize_dcb2de333eec7ab4ae31385ed8d6a393.css" rel="stylesheet" />
        <link type="text/css" media="screen" href="{{asset('assets/front/css')}}/autoptimize_281a9c0efc121555d2dbded049025ed5.css" rel="stylesheet" />
        <title>UPT Balai Latihan Kerja - Pasuruan</title>
        <script src="{{asset('assets/front/js')}}/jquery-1.9.1.min.js" ></script>
        <link href="https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@300;700&display=swap" rel="stylesheet">
        <link href="{{asset('assets/front/css/custom.css')}}" rel="stylesheet">
    </head>
    <body
        id="home"
        class="home page-template page-template-page-templates page-template-template-home page-template-page-templatestemplate-home-php page page-id-14 wp-embed-responsive woocommerce-js header-transparent has-header-search footer-multipage has-site-logo wpb-js-composer js-comp-ver-5.7 vc_responsive"
        data-spy="scroll"
        data-target=".navbar"
        data-offset="100"
    >
    <div id="preloader" style="background-color: rgb(255, 255, 255); display: none;">
        <div id="status" style="display: none;"><div class="status-mes" style="background-image: url(http://trendytheme.net/demo2/wp/69/multipage/wp-content/themes/sixtyninestudio/images/preloader.gif);"></div></div>
    </div>
    <div id="undefined-sticky-wrapper" class="sticky-wrapper is-sticky" style="height: 90px;">
        <div class="header-wrapper" style="position: fixed; top: 0px;">
            <nav class="navbar navbar-default">
                <div class="container">
                    
                    {{-- <div class="search-box-wrap pull-right hidden-sm hidden-xs">
                        <div class="search-icon"></div>
                        <form role="search" method="get" class="search-form header-search-form" action="http://trendytheme.net/demo2/wp/69/multipage/" style="display: none;">
                            <label class="screen-reader-text" for="header-search-field">Search for:</label> <input type="search" id="header-search-field" class="form-control" placeholder="Search" value="" name="s" title="Search for:" />
                            <button type="submit"><i class="fa fa-search"></i></button> <input type="hidden" value="post" name="post_type[]" />
                        </form>
                    </div> --}}
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mobile-toggle"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                        <div class="navbar-brand">
                            <h1>
                                <a href="http://trendytheme.net/demo2/wp/69/multipage/" title="Sixty Nine Studio">
                                    <img class="site-logo hidden-xs" src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/LOGO.png" alt="Sixty Nine Studio" />
                                    <img class="mobile-logo visible-xs" src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/LOGO.png" alt="Sixty Nine Studio" />
                                </a>
                            </h1>
                        </div>
                    </div>
                    <div class="main-menu-wrapper hidden-xs clearfix">
                        <div class="main-menu">
                            <ul id="menu-primary-menu" class="menu nav navbar-nav navbar-right">
                                @php
                                $load_parent_menu = \App\Models\M_menu_front::where('aktif', '1')->whereNull('id_parent')->get();
                                @endphp

                                @foreach ($load_parent_menu as $item)
                                <li
                                    class="menu-item" id="menu-item-{{$item->id_m_menu}}"
                                >
                                    @php
                                    $load_child_menu = \App\Models\M_menu_front::where('id_parent', $item->id_m_menu)->where('aktif', '1')->get();

                                    @endphp
                                    <a title="{{$item->nm_menu}}" href="{{($item->url != '') ? $item->url : 'javascript:void(0)'}}">{{$item->nm_menu}}
                                        @if(count($load_child_menu) > 0)
                                            <span class="fa fa-angle-down"></span>
                                        @endif
                                    </a>
                                    @if(count($load_child_menu) > 0)
                                    <div class="dropdown-wrapper menu-item-depth-0">
                                        <ul role="menu" class="dropdown-menu">
                                            @foreach ($load_child_menu as $item)
                                                <li id="menu-item-{{$item->id_m_menu}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                                    <a href="{{($item->url != '') ? $item->url : 'javascript:void(0)'}}">{{$item->nm_menu}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </li>
                                @endforeach
                                
{{--                                 
                                
                                <li id="menu-item-3369" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3369 dropdown has-menu-child">
                                    <a title="Blog" href="#">Blog <span class="fa fa-angle-down"></span></a>
                                    <div class="dropdown-wrapper menu-item-depth-0">
                                        <ul role="menu" class="dropdown-menu">
                                            <li id="menu-item-3370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                                <a title="Blog Standard" href="http://trendytheme.net/demo2/wp/69/multipage/blog/">Blog Standard</a>
                                            </li>
                                            <li id="menu-item-3471" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3471 dropdown has-menu-child dropdown-inverse">
                                                <a title="Blog Grid View" href="#">Blog Grid View <span class="fa fa-angle-right"></span></a>
                                                <div class="dropdown-wrapper menu-item-depth-1">
                                                    <ul role="menu" class="dropdown-menu">
                                                        <li id="menu-item-3474" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3474 has-menu-child">
                                                            <a title="Blog Grid" href="http://trendytheme.net/demo2/wp/69/multipage/blog-grid/">Blog Grid</a>
                                                        </li>
                                                        <li id="menu-item-3472" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3472 has-menu-child">
                                                            <a title="Grid Right Sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/grid-right-sidebar/">Grid Right Sidebar</a>
                                                        </li>
                                                        <li id="menu-item-3473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3473 has-menu-child">
                                                            <a title="Grid Left Sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/grid-left-sidebar/">Grid Left Sidebar</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </li>
                                <li id="menu-item-3453" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3453 has-menu-child">
                                    <a title="Contact" href="http://trendytheme.net/demo2/wp/69/multipage/contact-us/">Hubungi</a>
                                </li> --}}
                            </ul>
                        </div>
                    </div>



                    <div class="visible-xs">
                        <div class="mobile-menu collapse navbar-collapse mobile-toggle">
                            <ul id="menu-primary-menu-1" class="menu nav navbar-nav">
                                <li
                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item current-menu-ancestor current-menu-parent current_page_parent current_page_ancestor menu-item-has-children menu-item-32 active active active has-menu-child"
                                >
                                    <a title="Home" href="http://trendytheme.net/demo2/wp/69/multipage/">Home</a>
                                    <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-32" aria-expanded="false"><i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i></button>
                                    <ul role="menu" class="collapse dropdown-menu-32">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-14 current_page_item menu-item-3678 active has-menu-child">
                                            <a title="Home Default" href="http://trendytheme.net/demo2/wp/69/multipage/">Home Default</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2861 has-menu-child">
                                            <a title="Home Corporate" href="http://trendytheme.net/demo2/wp/69/multipage/home-corporate/">Home Corporate</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2862 has-menu-child">
                                            <a title="Home Creative" href="http://trendytheme.net/demo2/wp/69/multipage/home-creative/">Home Creative</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3902 has-menu-child">
                                            <a title="FullScreen App Landing" href="http://trendytheme.net/demo2/wp/69/multipage/fullscreen-app-landing/">FullScreen App Landing</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3901 has-menu-child">
                                            <a title="App Landing Page" href="http://trendytheme.net/demo2/wp/69/multipage/app-landing-page/">App Landing Page</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3903 has-menu-child">
                                            <a title="Home Blog" href="http://trendytheme.net/demo2/wp/69/multipage/home-blog/">Home Blog</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3080 has-menu-child">
                                            <a title="Home Portfolio" href="http://trendytheme.net/demo2/wp/69/multipage/home-portfolio/">Home Portfolio</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3079 has-menu-child">
                                            <a title="Home Portfolio Wide" href="http://trendytheme.net/demo2/wp/69/multipage/home-portfolio-wide/">Home Portfolio Wide</a>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3770 has-menu-child">
                                            <a title="Home Parallax" href="http://trendytheme.net/demo2/wp/69/multipage/home-parallax/">Home Parallax</a>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4111 dropdown has-menu-child">
                                            <a title="One Page" href="#">One Page</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-4111" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-4111">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4112 has-menu-child">
                                                    <a title="OnePage Default" target="_blank" href="http://104.131.177.38/demo2/wp/69/onepage/">OnePage Default</a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4216 has-menu-child">
                                                    <a title="App Landing" target="_blank" href="http://104.131.177.38/demo2/wp/69/onepage/app-landing/">App Landing</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3004 has-menu-child">
                                    <a title="Pages" href="#">Pages</a>
                                    <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3004" aria-expanded="false">
                                        <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul role="menu" class="collapse dropdown-menu-3004">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3012 dropdown has-menu-child">
                                            <a title="About" href="#">About</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3012" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3012">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3002 has-menu-child">
                                                    <a title="About 69Studio" href="http://trendytheme.net/demo2/wp/69/multipage/about-69studio/">About 69Studio</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3003 has-menu-child">
                                                    <a title="About Us Two" href="http://trendytheme.net/demo2/wp/69/multipage/about-us-two/">About Us Two</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4168 has-menu-child">
                                                    <a title="About Three" href="http://trendytheme.net/demo2/wp/69/multipage/about-three/">About Three</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3009 has-menu-child"><a title="Services" href="http://trendytheme.net/demo2/wp/69/multipage/services/">Services</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4169 dropdown has-menu-child">
                                            <a title="Case Study" href="#">Case Study</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-4169" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-4169">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3011 has-menu-child">
                                                    <a title="Case study One" href="http://trendytheme.net/demo2/wp/69/multipage/case-study-one/">Case study One</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3010 has-menu-child">
                                                    <a title="Case Study two" href="http://trendytheme.net/demo2/wp/69/multipage/case-study-two/">Case Study two</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3599 dropdown has-menu-child">
                                            <a title="Page Header Style" href="#">Page Header Style</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3599" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3599">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3592 has-menu-child">
                                                    <a title="Page Title Parallax BG" href="http://trendytheme.net/demo2/wp/69/multipage/page-title-parallax-bg/">Page Title Parallax BG</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3594 has-menu-child">
                                                    <a title="Page Title Right" href="http://trendytheme.net/demo2/wp/69/multipage/page-title-right/">Page Title Right</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3595 has-menu-child">
                                                    <a title="Page Title Left" href="http://trendytheme.net/demo2/wp/69/multipage/page-title-left/">Page Title Left</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3593 has-menu-child">
                                                    <a title="Page Title Center" href="http://trendytheme.net/demo2/wp/69/multipage/page-title-center/">Page Title Center</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3589 has-menu-child">
                                                    <a title="Page Title Right Content" href="http://trendytheme.net/demo2/wp/69/multipage/page-title-right-content/">Page Title Right Content</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3591 has-menu-child">
                                                    <a title="Page Title Left Content" href="http://trendytheme.net/demo2/wp/69/multipage/page-title-left-content/">Page Title Left Content</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3605 dropdown has-menu-child">
                                            <a title="Page Content Style" href="#">Page Content Style</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3605" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3605">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3515 has-menu-child">
                                                    <a title="Page Left Sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/page-left-sidebar/">Page Left Sidebar</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3514 has-menu-child">
                                                    <a title="Page Right Sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/page-right-sidebar/">Page Right Sidebar</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3513 has-menu-child">
                                                    <a title="Page with no sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/page-with-no-sidebar/">Page with no sidebar</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3450 has-menu-child"><a title="FAQ" href="http://trendytheme.net/demo2/wp/69/multipage/faq/">FAQ</a></li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-3606 has-menu-child"><a title="404" href="http://trendytheme.net/demo2/wp/69/multipage/404-not-found">404</a></li>
                                    </ul>
                                </li>
                                <li class="new menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3752 has-menu-child">
                                    <a title="Features" href="#">Features</a>
                                    <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3752" aria-expanded="false">
                                        <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul role="menu" class="collapse dropdown-menu-3752">
                                        <li class="new menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4291 dropdown has-menu-child">
                                            <a title="Shop" href="#">Shop</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-4291" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-4291">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4290 has-menu-child">
                                                    <a title="Products" href="http://trendytheme.net/demo2/wp/69/multipage/shop/">Products</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-product menu-item-4292 has-menu-child">
                                                    <a title="Single Product" href="http://trendytheme.net/demo2/wp/69/multipage/product/green-headphone/">Single Product</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4287 has-menu-child">
                                                    <a title="My account" href="http://trendytheme.net/demo2/wp/69/multipage/my-account/">My account</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4288 has-menu-child">
                                                    <a title="Checkout" href="http://trendytheme.net/demo2/wp/69/multipage/checkout/">Checkout</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4289 has-menu-child"><a title="Cart" href="http://trendytheme.net/demo2/wp/69/multipage/cart/">Cart</a></li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3753 dropdown has-menu-child">
                                            <a title="Sliders" href="#">Sliders</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3753" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3753">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3756 has-menu-child">
                                                    <a title="Corporate Slider" href="http://trendytheme.net/demo2/wp/69/multipage/corporate-slider/">Corporate Slider</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4030 has-menu-child">
                                                    <a title="Creative Slider" href="http://trendytheme.net/demo2/wp/69/multipage/creative-slider/">Creative Slider</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3755 has-menu-child">
                                                    <a title="Content Slider One" href="http://trendytheme.net/demo2/wp/69/multipage/content-slider-one/">Content Slider One</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4034 has-menu-child">
                                                    <a title="Content Slider Two" href="http://trendytheme.net/demo2/wp/69/multipage/content-slider-two/">Content Slider Two</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3767 has-menu-child">
                                                    <a title="Bootstrap Slider" href="http://trendytheme.net/demo2/wp/69/multipage/bootstrap-slider/">Bootstrap Slider</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3927 has-menu-child">
                                                    <a title="News Slider" href="http://trendytheme.net/demo2/wp/69/multipage/news-slider/">News Slider</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3768 dropdown has-menu-child">
                                            <a title="Hero Units" href="#">Hero Units</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3768" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3768">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3801 has-menu-child">
                                                    <a title="Text Rotator BG" href="http://trendytheme.net/demo2/wp/69/multipage/text-rotator-bg/">Text Rotator BG</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3798 has-menu-child">
                                                    <a title="Particle BG" href="http://trendytheme.net/demo2/wp/69/multipage/particle-bg/">Particle BG</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3796 has-menu-child">
                                                    <a title="Parallax BG" href="http://trendytheme.net/demo2/wp/69/multipage/parallax-bg/">Parallax BG</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3797 has-menu-child">
                                                    <a title="Video BG" href="http://trendytheme.net/demo2/wp/69/multipage/video-bg/">Video BG</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3754 dropdown has-menu-child">
                                            <a title="Menu Style" href="#">Menu Style</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3754" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3754">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3784 has-menu-child">
                                                    <a title="Default Menu" href="http://trendytheme.net/demo2/wp/69/multipage/default-menu/">Default Menu</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3783 has-menu-child">
                                                    <a title="Transparent Menu" href="http://trendytheme.net/demo2/wp/69/multipage/transparent-menu/">Transparent Menu</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-4244 dropdown has-menu-child">
                                            <a title="Header topbar style" href="#">Header topbar style</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-4244" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-4244">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4246 has-menu-child">
                                                    <a title="Topbar Default" href="http://trendytheme.net/demo2/wp/69/multipage/topbar-default/">Topbar Default</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4245 has-menu-child">
                                                    <a title="Topbar transparent" href="http://trendytheme.net/demo2/wp/69/multipage/topbar-transparent/">Topbar transparent</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="new menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2199 has-mega-menu has-menu-child">
                                    <a title="Portfolio" href="#">Portfolio</a>
                                    <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2199" aria-expanded="false">
                                        <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul role="menu" class="collapse dropdown-menu-2199">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2463 dropdown has-mega-menu-child">
                                            <a title="Boxed" href="#">Boxed</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2463" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2463">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2498 has-menu-child">
                                                    <a title="Boxed Two Column" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-two-column/">Boxed Two Column</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2499 has-menu-child">
                                                    <a title="Boxed Three Column" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-three-column/">Boxed Three Column</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2464 has-menu-child">
                                                    <a title="Boxed Four Column" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio-boxed-four/">Boxed Four Column</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2484 has-menu-child">
                                                    <a title="Boxed Five Column" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-five-column/">Boxed Five Column</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2483 has-menu-child">
                                                    <a title="Boxed Six Column" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-six-column/">Boxed Six Column</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2500 dropdown has-mega-menu-child">
                                            <a title="Boxed Gutter" href="#">Boxed Gutter</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2500" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2500">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2516 has-menu-child">
                                                    <a title="Boxed Gutter Two" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-gutter-two/">Boxed Gutter Two</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2515 has-menu-child">
                                                    <a title="Boxed Gutter Three" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-gutter-three/">Boxed Gutter Three</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2514 has-menu-child">
                                                    <a title="Boxed Gutter Four" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-gutter-four/">Boxed Gutter Four</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2513 has-menu-child">
                                                    <a title="Boxed Gutter Five" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-gutter-five/">Boxed Gutter Five</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2512 has-menu-child">
                                                    <a title="Boxed Gutter Six" href="http://trendytheme.net/demo2/wp/69/multipage/boxed-gutter-six/">Boxed Gutter Six</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2536 dropdown has-mega-menu-child">
                                            <a title="Wide" href="#">Wide</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2536" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2536">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2542 has-menu-child">
                                                    <a title="Wide Two Col" href="http://trendytheme.net/demo2/wp/69/multipage/wide-two-column/">Wide Two Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2541 has-menu-child">
                                                    <a title="Wide Three Col" href="http://trendytheme.net/demo2/wp/69/multipage/wide-three-column/">Wide Three Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2540 has-menu-child">
                                                    <a title="Wide Four Col" href="http://trendytheme.net/demo2/wp/69/multipage/wide-four-column/">Wide Four Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2539 has-menu-child">
                                                    <a title="Wide Five Col" href="http://trendytheme.net/demo2/wp/69/multipage/wide-five-column/">Wide Five Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2537 has-menu-child">
                                                    <a title="Wide Six Col" href="http://trendytheme.net/demo2/wp/69/multipage/wide-six-column/">Wide Six Col</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2560 dropdown has-mega-menu-child">
                                            <a title="Wide Gutter" href="#">Wide Gutter</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2560" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2560">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2565 has-menu-child">
                                                    <a title="Wide Gutter Two" href="http://trendytheme.net/demo2/wp/69/multipage/wide-gutter-two/">Wide Gutter Two</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2564 has-menu-child">
                                                    <a title="Wide Gutter Three" href="http://trendytheme.net/demo2/wp/69/multipage/wide-gutter-three/">Wide Gutter Three</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2563 has-menu-child">
                                                    <a title="Wide Gutter Four" href="http://trendytheme.net/demo2/wp/69/multipage/wide-gutter-four/">Wide Gutter Four</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2562 has-menu-child">
                                                    <a title="Wide Gutter Five" href="http://trendytheme.net/demo2/wp/69/multipage/wide-gutter-five/">Wide Gutter Five</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2561 has-menu-child">
                                                    <a title="Wide Gutter Six" href="http://trendytheme.net/demo2/wp/69/multipage/wide-gutter-six/">Wide Gutter Six</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2583 dropdown has-mega-menu-child">
                                            <a title="Portfolio Title" href="#">Portfolio Title</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2583" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2583">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2587 has-menu-child">
                                                    <a title="Title Two Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-two-column/">Title Two Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2586 has-menu-child">
                                                    <a title="Title Three Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-three-column/">Title Three Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2588 has-menu-child">
                                                    <a title="Title Four Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-four-column/">Title Four Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2585 has-menu-child">
                                                    <a title="Title Five Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-five-column/">Title Five Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2584 has-menu-child">
                                                    <a title="Title Six Column" href="http://trendytheme.net/demo2/wp/69/multipage/title-six-column/">Title Six Column</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3093 dropdown has-mega-menu-child">
                                            <a title="Portfolio Wide Title" href="#">Portfolio Wide Title</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3093" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3093">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3098 has-menu-child">
                                                    <a title="Title Wide 2 Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-wide-2-col/">Title Wide 2 Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3097 has-menu-child">
                                                    <a title="Title Wide 3 Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-wide-3-col/">Title Wide 3 Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3096 has-menu-child">
                                                    <a title="Title Wide 4 Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-wide-4-col/">Title Wide 4 Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3095 has-menu-child">
                                                    <a title="Title Wide 5 Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-wide-5-col/">Title Wide 5 Col</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3094 has-menu-child">
                                                    <a title="Title Wide 6 Col" href="http://trendytheme.net/demo2/wp/69/multipage/title-wide-6-col/">Title Wide 6 Col</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2599 dropdown has-mega-menu-child">
                                            <a title="Portfolio Gutter Title" href="#">Portfolio Gutter Title</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2599" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2599">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2603 has-menu-child">
                                                    <a title="Title Gutter Two" href="http://trendytheme.net/demo2/wp/69/multipage/title-gutter-two/">Title Gutter Two</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2602 has-menu-child">
                                                    <a title="Title Gutter Three" href="http://trendytheme.net/demo2/wp/69/multipage/title-gutter-three/">Title Gutter Three</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2601 has-menu-child">
                                                    <a title="Title Gutter Four" href="http://trendytheme.net/demo2/wp/69/multipage/title-gutter-four/">Title Gutter Four</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2600 has-menu-child">
                                                    <a title="Title Gutter Five" href="http://trendytheme.net/demo2/wp/69/multipage/title-gutter-five/">Title Gutter Five</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2629 dropdown has-mega-menu-child">
                                            <a title="Portfolio Details" href="#">Portfolio Details</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2629" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-2629">
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2630 has-menu-child">
                                                    <a title="Portfolio Single" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/creative-design/">Portfolio Single</a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2632 has-menu-child">
                                                    <a title="Portfolio Sidbar" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/web-design/">Portfolio Sidbar</a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-2631 has-menu-child">
                                                    <a title="Portfolio Gallery" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/business-cards/">Portfolio Gallery</a>
                                                </li>
                                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-4199 has-menu-child">
                                                    <a title="Portfolio Sidebar Gallery" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/brand-design/">Portfolio Sidebar Gallery</a>
                                                </li>
                                                <li class="new menu-item menu-item-type-post_type menu-item-object-tt-portfolio menu-item-4298 has-menu-child">
                                                    <a title="Portfolio Video &amp; Extra images" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/interface-design/">Portfolio Video &amp; Extra images</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-2846 has-mega-menu has-menu-child">
                                    <a title="Shortcodes" href="#">Shortcodes</a>
                                    <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-2846" aria-expanded="false">
                                        <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul role="menu" class="collapse dropdown-menu-2846">
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3099 dropdown has-mega-menu-child">
                                            <a title="Shortcode List One" href="#">Shortcode List One</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3099" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3099">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2852 has-menu-child">
                                                    <a title="Accordion and Toggles" href="http://trendytheme.net/demo2/wp/69/multipage/accordion-and-toggles/">Accordion and Toggles</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2851 has-menu-child">
                                                    <a title="Alert Messages" href="http://trendytheme.net/demo2/wp/69/multipage/alert-messages/">Alert Messages</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2850 has-menu-child">
                                                    <a title="Hero Blocks" href="http://trendytheme.net/demo2/wp/69/multipage/hero-blocks/">Hero Blocks</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2849 has-menu-child">
                                                    <a title="Progress Bars" href="http://trendytheme.net/demo2/wp/69/multipage/progress-bars/">Progress Bars</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3109 has-menu-child">
                                                    <a title="Fun Facts" href="http://trendytheme.net/demo2/wp/69/multipage/fun-facts/">Fun Facts</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3100 dropdown has-mega-menu-child">
                                            <a title="Shortcode List Two" href="#">Shortcode List Two</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3100" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3100">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2847 has-menu-child">
                                                    <a title="Portfolio Hover Style" href="http://trendytheme.net/demo2/wp/69/multipage/portfolio-hover-style/">Portfolio Hover Style</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3361 has-menu-child">
                                                    <a title="Shortcode Clients" href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-clients/">Shortcode Clients</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3304 has-menu-child">
                                                    <a title="Call To Action Box" href="http://trendytheme.net/demo2/wp/69/multipage/call-to-action-box/">Call To Action Box</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3110 has-menu-child">
                                                    <a title="Tab style" href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-tab/">Tab style</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3331 has-menu-child">
                                                    <a title="Testimonial Style" href="http://trendytheme.net/demo2/wp/69/multipage/testimonial-style/">Testimonial Style</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3101 dropdown has-mega-menu-child">
                                            <a title="Shortcode List Three" href="#">Shortcode List Three</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3101" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3101">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3251 has-menu-child">
                                                    <a title="Shortcode Buttons" href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-buttons/">Shortcode Buttons</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3359 has-menu-child">
                                                    <a title="Featured Boxes" href="http://trendytheme.net/demo2/wp/69/multipage/featured-boxes/">Featured Boxes</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3926 has-menu-child">
                                                    <a title="Flicker Gallery" href="http://trendytheme.net/demo2/wp/69/multipage/flicker-gallery/">Flicker Gallery</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3250 has-menu-child">
                                                    <a title="Shortcode Gallery(Slider)" href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-galleryslider/">Shortcode Gallery(Slider)</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2848 has-menu-child">
                                                    <a title="Typography" href="http://trendytheme.net/demo2/wp/69/multipage/typography/">Typography</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3303 dropdown has-mega-menu-child">
                                            <a title="Shortcode List Four" href="#">Shortcode List Four</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3303" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3303">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3611 has-menu-child">
                                                    <a title="Featured Animated Boxes" href="http://trendytheme.net/demo2/wp/69/multipage/featured-animated-boxes/">Featured Animated Boxes</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3249 has-menu-child">
                                                    <a title="Shortcode Photo Gallery" href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-photo-gallery/">Shortcode Photo Gallery</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4125 has-menu-child">
                                                    <a title="Grid and Column" href="http://trendytheme.net/demo2/wp/69/multipage/grid-and-column/">Grid and Column</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-4134 has-menu-child">
                                                    <a title="Media Embeds" href="http://trendytheme.net/demo2/wp/69/multipage/media-embeds/">Media Embeds</a>
                                                </li>
                                            </ul>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3369 has-menu-child">
                                    <a title="Blog" href="#">Blog</a>
                                    <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3369" aria-expanded="false">
                                        <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul role="menu" class="collapse dropdown-menu-3369">
                                        <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child">
                                            <a title="Blog Standard" href="http://trendytheme.net/demo2/wp/69/multipage/blog/">Blog Standard</a>
                                        </li>
                                        <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-3471 dropdown has-menu-child">
                                            <a title="Blog Grid View" href="#">Blog Grid View</a>
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-3471" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                            <ul role="menu" class="collapse dropdown-menu-3471">
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3474 has-menu-child">
                                                    <a title="Blog Grid" href="http://trendytheme.net/demo2/wp/69/multipage/blog-grid/">Blog Grid</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3472 has-menu-child">
                                                    <a title="Grid Right Sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/grid-right-sidebar/">Grid Right Sidebar</a>
                                                </li>
                                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3473 has-menu-child">
                                                    <a title="Grid Left Sidebar" href="http://trendytheme.net/demo2/wp/69/multipage/grid-left-sidebar/">Grid Left Sidebar</a>
                                                </li>
                                            </ul>
                                        </li>
                                        <li class="menu-item menu-item-type-post_type menu-item-object-post menu-item-3482 has-menu-child">
                                            <a title="Single Article" href="http://trendytheme.net/demo2/wp/69/multipage/gear-up-your-website-for-mobile-devices/">Single Article</a>
                                        </li>
                                    </ul>
                                </li>
                                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3453 has-menu-child"><a title="Contact" href="http://trendytheme.net/demo2/wp/69/multipage/contact-us/">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    <section class="vc_row section-wrapper">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_revslider_element wpb_content_element">
                                <div class="forcefullwidth_wrapper_tp_banner" id="rev_slider_31_1_forcefullwidth" style="position: relative; width: 100%; height: auto; margin-top: 0px; margin-bottom: 0px;">
                                    <div
                                        id="rev_slider_31_1_wrapper"
                                        class="rev_slider_wrapper fullscreen-container"
                                        style="background-color: rgb(255, 255, 255); padding: 0px; height: 199px; margin-top: 0px; margin-bottom: 0px; position: absolute; max-height: none; overflow: visible; width: 1423px; left: -142px;"
                                    >
                                        <div
                                            id="rev_slider_31_1"
                                            class="rev_slider fullscreenbanner revslider-initialised tp-simpleresponsive"
                                            style="max-height: none; margin-top: 0px; margin-bottom: 0px; height: 100%;"
                                            data-version="5.2.6"
                                            data-slideactive="rs-69"
                                        >
                                            <ul class="tp-revslider-mainul" style="visibility: visible; display: block; overflow: hidden; width: 100%; height: 100%; max-height: none;">
                                                <li
                                                    data-index="rs-80"
                                                    data-transition="fade"
                                                    data-slotamount="7"
                                                    data-hideafterloop="0"
                                                    data-hideslideonmobile="off"
                                                    data-easein="default"
                                                    data-easeout="default"
                                                    data-masterspeed="1000"
                                                    data-thumb="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg1-100x50.jpg"
                                                    data-rotate="0"
                                                    data-saveperformance="off"
                                                    data-title="Slide"
                                                    data-param1=""
                                                    data-param2=""
                                                    data-param3=""
                                                    data-param4=""
                                                    data-param5=""
                                                    data-param6=""
                                                    data-param7=""
                                                    data-param8=""
                                                    data-param9=""
                                                    data-param10=""
                                                    data-description=""
                                                    class="tp-revslider-slidesli"
                                                    style="width: 100%; height: 100%; overflow: hidden; z-index: 18; visibility: hidden; opacity: 0; background-color: rgba(255, 255, 255, 0);"
                                                >
                                                    <div class="slotholder" style="position: absolute; top: 0px; left: 0px; z-index: 0; width: 100%; height: 100%; visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                                        <!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg1.jpg" alt="" title="webproduct_darkbg1.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="" width="1920" height="1080">-->
                                                        <div
                                                            class="tp-bgimg defaultimg"
                                                            style="
                                                                background-color: rgba(0, 0, 0, 0);
                                                                background-repeat: no-repeat;
                                                                background-image: url('http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg1.jpg');
                                                                background-size: cover;
                                                                background-position: center center;
                                                                width: 100%;
                                                                height: 100%;
                                                                opacity: 1;
                                                                visibility: inherit;
                                                                z-index: 20;
                                                            "
                                                            src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg1.jpg"
                                                        ></div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden; left: 387px; top: 166px; z-index: 5;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <span
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-80-layer-1"
                                                                    data-x="10"
                                                                    data-y="center"
                                                                    data-voffset="-92"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="x:left;s:300;e:easeOutExpo;"
                                                                    data-transform_out="x:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1000"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 5;
                                                                        white-space: nowrap;
                                                                        font-size: 32px;
                                                                        line-height: 32px;
                                                                        font-weight: 300;
                                                                        color: rgb(255, 255, 255);
                                                                        visibility: hidden;
                                                                        transition: none 0s ease 0s;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 0.0001;
                                                                        transform: translate3d(-26.6129px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    WELCOME TO
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden; left: 387px; top: 205px; z-index: 6;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-80-layer-11"
                                                                    data-x="10"
                                                                    data-y="center"
                                                                    data-voffset=""
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:-50px;opacity:0;s:300;e:easeOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 6;
                                                                        white-space: nowrap;
                                                                        font-size: 53px;
                                                                        line-height: 53px;
                                                                        font-weight: 800;
                                                                        color: rgb(255, 255, 255);
                                                                        text-transform: uppercase;
                                                                        visibility: hidden;
                                                                        transition: none 0s ease 0s;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 0.0001;
                                                                        transform: translate3d(0px, 26.6129px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    69Studio
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden; left: 390px; top: 277px; z-index: 7;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-80-layer-3"
                                                                    data-x="16"
                                                                    data-y="center"
                                                                    data-voffset="107"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:-50px;opacity:0;s:300;e:easeOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2200"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 7;
                                                                        white-space: nowrap;
                                                                        font-size: 13px;
                                                                        line-height: 21px;
                                                                        font-weight: 400;
                                                                        color: rgb(255, 255, 255);
                                                                        text-transform: uppercase;
                                                                        visibility: hidden;
                                                                        transition: none 0s ease 0s;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 0.0001;
                                                                        transform: translate3d(0px, 26.6129px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    ELEGANT . UNIQUE . BEAUTIFUL
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden; left: 391px; top: 327px; z-index: 8;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption rev-btn rs-hover-ready"
                                                                    id="slide-80-layer-4"
                                                                    data-x="18"
                                                                    data-y="center"
                                                                    data-voffset="226"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:400;e:Linear.easeNone;"
                                                                    data-style_hover="c:rgba(255, 42, 64, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 0);"
                                                                    data-transform_in="x:left;s:300;e:easeOutExpo;"
                                                                    data-transform_out="x:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    data-responsive="off"
                                                                    style="
                                                                        z-index: 8;
                                                                        white-space: nowrap;
                                                                        font-size: 14px;
                                                                        line-height: 17px;
                                                                        font-weight: 400;
                                                                        color: rgb(255, 255, 255);
                                                                        background-color: rgba(0, 0, 0, 0);
                                                                        padding: 15px 35px;
                                                                        border-color: rgb(255, 255, 255);
                                                                        border-style: solid;
                                                                        border-width: 1px;
                                                                        border-radius: 30px;
                                                                        outline: currentcolor none medium;
                                                                        box-shadow: none;
                                                                        box-sizing: border-box;
                                                                        cursor: pointer;
                                                                        visibility: hidden;
                                                                        transition: none 0s ease 0s;
                                                                        font-style: normal;
                                                                        text-decoration: rgb(255, 255, 255);
                                                                        margin: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 0.0001;
                                                                        transform: translate3d(-26.6129px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    LEARN MORE
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden; left: 635px; top: 131px; z-index: 9;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-80-layer-14"
                                                                    data-x="right"
                                                                    data-hoffset="28"
                                                                    data-y="center"
                                                                    data-voffset="61"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="x:50px;opacity:0;s:1500;e:easeOutExpo;"
                                                                    data-transform_out="x:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1700"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 9;
                                                                        visibility: hidden;
                                                                        transition: none 0s ease 0s;
                                                                        line-height: 0px;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        font-weight: 400;
                                                                        font-size: 7px;
                                                                        white-space: nowrap;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 0.0001;
                                                                        transform: translate3d(26.6129px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/desktop_slider-2.png"
                                                                        alt=""
                                                                        data-ww="736px"
                                                                        data-hh="496px"
                                                                        data-no-retina=""
                                                                        style="
                                                                            width: 391.742px;
                                                                            height: 264px;
                                                                            transition: none 0s ease 0s;
                                                                            line-height: 0px;
                                                                            border-width: 0px;
                                                                            margin: 0px;
                                                                            padding: 0px;
                                                                            letter-spacing: 0px;
                                                                            font-weight: 400;
                                                                            font-size: 7px;
                                                                        "
                                                                        width="736"
                                                                        height="496"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li
                                                    data-index="rs-69"
                                                    data-transition="fade"
                                                    data-slotamount="7"
                                                    data-hideafterloop="0"
                                                    data-hideslideonmobile="off"
                                                    data-easein="default"
                                                    data-easeout="default"
                                                    data-masterspeed="1000"
                                                    data-thumb="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2-100x50.jpg"
                                                    data-rotate="0"
                                                    data-saveperformance="off"
                                                    data-title="Slide"
                                                    data-param1=""
                                                    data-param2=""
                                                    data-param3=""
                                                    data-param4=""
                                                    data-param5=""
                                                    data-param6=""
                                                    data-param7=""
                                                    data-param8=""
                                                    data-param9=""
                                                    data-param10=""
                                                    data-description=""
                                                    class="tp-revslider-slidesli active-revslide"
                                                    style="width: 100%; height: 100%; overflow: hidden; z-index: 20; visibility: inherit; opacity: 1; background-color: rgba(255, 255, 255, 0);"
                                                >
                                                    <div class="slotholder" style="position: absolute; top: 0px; left: 0px; z-index: 0; width: 100%; height: 100%; visibility: inherit; opacity: 1; transform: matrix(1, 0, 0, 1, 0, 0);">
                                                        <!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg" alt="" title="webproduct_darkbg2.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="" width="1920" height="1080">-->
                                                        <div
                                                            class="tp-bgimg defaultimg"
                                                            style="
                                                                background-color: rgba(0, 0, 0, 0);
                                                                background-repeat: no-repeat;
                                                                background-image: url('http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg');
                                                                background-size: cover;
                                                                background-position: center center;
                                                                width: 100%;
                                                                height: 100%;
                                                                opacity: 1;
                                                                visibility: inherit;
                                                                z-index: 20;
                                                            "
                                                            src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg"
                                                        ></div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 685px; top: 42px; z-index: 5;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-9"
                                                                    data-x="right"
                                                                    data-hoffset="-264"
                                                                    data-y="center"
                                                                    data-voffset="52"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:1500;e:Power4.easeIn;"
                                                                    data-start="2500"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 5;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        line-height: 0px;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        font-weight: 400;
                                                                        font-size: 3px;
                                                                        white-space: nowrap;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/macbookpro.png"
                                                                        alt=""
                                                                        data-ww="1000px"
                                                                        data-hh="600px"
                                                                        data-no-retina=""
                                                                        style="
                                                                            width: 229.263px;
                                                                            height: 137.558px;
                                                                            transition: none 0s ease 0s;
                                                                            line-height: 0px;
                                                                            border-width: 0px;
                                                                            margin: 0px;
                                                                            padding: 0px;
                                                                            letter-spacing: 0px;
                                                                            font-weight: 400;
                                                                            font-size: 3px;
                                                                        "
                                                                        width="1000"
                                                                        height="600"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 725px; top: 61px; z-index: 6;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-10"
                                                                    data-x="677"
                                                                    data-y="268"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:1500;e:Power2.easeIn;"
                                                                    data-start="3350"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 6;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        line-height: 0px;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        font-weight: 400;
                                                                        font-size: 3px;
                                                                        white-space: nowrap;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/express_macbook_content1.png"
                                                                        alt=""
                                                                        data-ww="653px"
                                                                        data-hh="408px"
                                                                        data-no-retina=""
                                                                        style="
                                                                            width: 149.709px;
                                                                            height: 93.5392px;
                                                                            transition: none 0s ease 0s;
                                                                            line-height: 0px;
                                                                            border-width: 0px;
                                                                            margin: 0px;
                                                                            padding: 0px;
                                                                            letter-spacing: 0px;
                                                                            font-weight: 400;
                                                                            font-size: 3px;
                                                                        "
                                                                        width="653"
                                                                        height="408"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 698px; top: 80px; z-index: 7;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-7"
                                                                    data-x="561"
                                                                    data-y="350"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="x:right;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:1500;e:Power2.easeIn;"
                                                                    data-start="3000"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 7;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        line-height: 0px;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        font-weight: 400;
                                                                        font-size: 3px;
                                                                        white-space: nowrap;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/ihpone_dark.png"
                                                                        alt=""
                                                                        data-ww="260px"
                                                                        data-hh="450px"
                                                                        data-no-retina=""
                                                                        style="
                                                                            width: 59.6083px;
                                                                            height: 103.168px;
                                                                            transition: none 0s ease 0s;
                                                                            line-height: 0px;
                                                                            border-width: 0px;
                                                                            margin: 0px;
                                                                            padding: 0px;
                                                                            letter-spacing: 0px;
                                                                            font-weight: 400;
                                                                            font-size: 3px;
                                                                        "
                                                                        width="260"
                                                                        height="450"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 708px; top: 99px; z-index: 8;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-8"
                                                                    data-x="606"
                                                                    data-y="433"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:1.1;sY:1.1;skX:0;skY:0;opacity:0;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:1500;e:Power4.easeIn;"
                                                                    data-start="3950"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 8;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        line-height: 0px;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        font-weight: 400;
                                                                        font-size: 3px;
                                                                        white-space: nowrap;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/express_iphone_content1.png"
                                                                        alt=""
                                                                        data-ww="170px"
                                                                        data-hh="286px"
                                                                        data-no-retina=""
                                                                        style="
                                                                            width: 38.9747px;
                                                                            height: 65.5691px;
                                                                            transition: none 0s ease 0s;
                                                                            line-height: 0px;
                                                                            border-width: 0px;
                                                                            margin: 0px;
                                                                            padding: 0px;
                                                                            letter-spacing: 0px;
                                                                            font-weight: 400;
                                                                            font-size: 3px;
                                                                        "
                                                                        width="170"
                                                                        height="286"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 572px; top: 71px; z-index: 9;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <span
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-1"
                                                                    data-x="10"
                                                                    data-y="center"
                                                                    data-voffset="-92"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="x:left;s:300;e:easeOutExpo;"
                                                                    data-transform_out="x:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1000"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 9;
                                                                        white-space: nowrap;
                                                                        font-size: 14px;
                                                                        line-height: 14px;
                                                                        font-weight: 300;
                                                                        color: rgb(255, 255, 255);
                                                                        text-transform: uppercase;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    Responsive
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 572px; top: 91px; z-index: 10;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-11"
                                                                    data-x="10"
                                                                    data-y="center"
                                                                    data-voffset=""
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:-50px;opacity:0;s:300;e:easeInOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 10;
                                                                        white-space: nowrap;
                                                                        font-size: 18px;
                                                                        line-height: 18px;
                                                                        font-weight: 800;
                                                                        color: rgb(255, 255, 255);
                                                                        text-transform: uppercase;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    Grid System
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 573px; top: 120px; z-index: 11;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-69-layer-3"
                                                                    data-x="16"
                                                                    data-y="center"
                                                                    data-voffset="107"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:-50px;opacity:0;s:300;e:easeOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2200"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="
                                                                        z-index: 11;
                                                                        white-space: nowrap;
                                                                        font-size: 6px;
                                                                        line-height: 9px;
                                                                        font-weight: 400;
                                                                        color: rgb(255, 255, 255);
                                                                        text-transform: uppercase;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        border-width: 0px;
                                                                        margin: 0px;
                                                                        padding: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    Elegent . Modern . Clean
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: visible; left: 573px; top: 127px; z-index: 12;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute; overflow: visible; height: auto; width: auto;">
                                                                <div
                                                                    class="tp-caption rev-btn rs-hover-ready"
                                                                    id="slide-69-layer-4"
                                                                    data-x="18"
                                                                    data-y="center"
                                                                    data-voffset="226"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:400;e:Linear.easeNone;"
                                                                    data-style_hover="c:rgba(255, 42, 64, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 0);"
                                                                    data-transform_in="x:left;s:300;e:easeOutExpo;"
                                                                    data-transform_out="x:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    data-responsive="off"
                                                                    style="
                                                                        z-index: 12;
                                                                        white-space: nowrap;
                                                                        font-size: 14px;
                                                                        line-height: 17px;
                                                                        font-weight: 400;
                                                                        color: rgb(255, 255, 255);
                                                                        background-color: rgba(0, 0, 0, 0);
                                                                        padding: 15px 35px;
                                                                        border-color: rgb(255, 255, 255);
                                                                        border-style: solid;
                                                                        border-width: 1px;
                                                                        border-radius: 30px;
                                                                        outline: currentcolor none medium;
                                                                        box-shadow: none;
                                                                        box-sizing: border-box;
                                                                        cursor: pointer;
                                                                        visibility: inherit;
                                                                        transition: none 0s ease 0s;
                                                                        font-style: normal;
                                                                        text-decoration: rgb(255, 255, 255);
                                                                        margin: 0px;
                                                                        letter-spacing: 0px;
                                                                        min-height: 0px;
                                                                        min-width: 0px;
                                                                        max-height: none;
                                                                        max-width: none;
                                                                        opacity: 1;
                                                                        transform: translate3d(0px, 0px, 0px);
                                                                        transform-origin: 50% 50% 0px;
                                                                    "
                                                                >
                                                                    LEARN MORE
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li
                                                    data-index="rs-71"
                                                    data-transition="fade"
                                                    data-slotamount="7"
                                                    data-hideafterloop="0"
                                                    data-hideslideonmobile="off"
                                                    data-easein="default"
                                                    data-easeout="default"
                                                    data-masterspeed="1000"
                                                    data-thumb="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2-100x50.jpg"
                                                    data-rotate="0"
                                                    data-saveperformance="off"
                                                    data-title="Slide"
                                                    data-param1=""
                                                    data-param2=""
                                                    data-param3=""
                                                    data-param4=""
                                                    data-param5=""
                                                    data-param6=""
                                                    data-param7=""
                                                    data-param8=""
                                                    data-param9=""
                                                    data-param10=""
                                                    data-description=""
                                                    class="tp-revslider-slidesli"
                                                    style="width: 100%; height: 100%; overflow: hidden;"
                                                >
                                                    <div class="slotholder" style="position: absolute; top: 0px; left: 0px; z-index: 0; width: 100%; height: 100%;">
                                                        <!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg" alt="" title="webproduct_darkbg2.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="" width="1920" height="1080">-->
                                                        <div
                                                            class="tp-bgimg defaultimg"
                                                            style="
                                                                background-color: rgba(0, 0, 0, 0);
                                                                background-repeat: no-repeat;
                                                                background-image: url('http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg');
                                                                background-size: cover;
                                                                background-position: center center;
                                                                width: 100%;
                                                                height: 100%;
                                                                opacity: 0;
                                                            "
                                                            src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg"
                                                        ></div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-71-layer-13"
                                                                    data-x="right"
                                                                    data-hoffset="-32"
                                                                    data-y="center"
                                                                    data-voffset="-267"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;rZ:-30;"
                                                                    data-transform_in="y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:2000;e:Power2.easeIn;"
                                                                    data-start="2500"
                                                                    data-responsive_offset="on"
                                                                    data-end="6000"
                                                                    style="z-index: 5; visibility: hidden;"
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/examplelayer1.png"
                                                                        alt=""
                                                                        data-ww="600px"
                                                                        data-hh="338px"
                                                                        data-no-retina=""
                                                                        width="800"
                                                                        height="450"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-71-layer-14"
                                                                    data-x="779"
                                                                    data-y="276"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;rZ:-10;"
                                                                    data-transform_in="y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:2000;e:Power2.easeIn;"
                                                                    data-start="2750"
                                                                    data-responsive_offset="on"
                                                                    data-end="6000"
                                                                    style="z-index: 6; visibility: hidden;"
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/examplelayer2.png"
                                                                        alt=""
                                                                        data-ww="600px"
                                                                        data-hh="338px"
                                                                        data-no-retina=""
                                                                        width="800"
                                                                        height="450"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-71-layer-15"
                                                                    data-x="786"
                                                                    data-y="559"
                                                                    data-width="['none','none','none','none']"
                                                                    data-height="['none','none','none','none']"
                                                                    data-transform_idle="o:1;rZ:10;"
                                                                    data-transform_in="y:bottom;rX:-20deg;rY:-20deg;rZ:0deg;s:1500;e:Power3.easeOut;"
                                                                    data-transform_out="opacity:0;s:2000;e:Power2.easeIn;"
                                                                    data-start="3000"
                                                                    data-responsive_offset="on"
                                                                    data-end="6000"
                                                                    style="z-index: 7; visibility: hidden;"
                                                                >
                                                                    <img
                                                                        src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/8.jpg"
                                                                        alt=""
                                                                        data-ww="600px"
                                                                        data-hh="338px"
                                                                        data-no-retina=""
                                                                        width="800"
                                                                        height="450"
                                                                    />
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <span
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-71-layer-1"
                                                                    data-x="10"
                                                                    data-y="center"
                                                                    data-voffset="-92"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="x:left;s:300;e:easeOutExpo;"
                                                                    data-transform_out="x:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1000"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="z-index: 8; white-space: nowrap; font-size: 60px; line-height: 60px; font-weight: 300; color: rgb(255, 255, 255); visibility: hidden;"
                                                                >
                                                                    TRUELY
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-71-layer-11"
                                                                    data-x="10"
                                                                    data-y="center"
                                                                    data-voffset=""
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:-50px;opacity:0;s:300;e:easeOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="z-index: 9; white-space: nowrap; font-size: 80px; line-height: 80px; font-weight: 800; color: rgb(255, 255, 255); text-transform: uppercase; visibility: hidden;"
                                                                >
                                                                    MULTIPURPOSE
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-71-layer-3"
                                                                    data-x="16"
                                                                    data-y="center"
                                                                    data-voffset="107"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:-50px;opacity:0;s:300;e:easeOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2200"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="z-index: 10; white-space: nowrap; font-size: 25px; line-height: 40px; font-weight: 400; color: rgb(255, 255, 255); text-transform: uppercase; visibility: hidden;"
                                                                >
                                                                    CORPORATE . BUSINESS . PORTFOLIO
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption rev-btn"
                                                                    id="slide-71-layer-4"
                                                                    data-x="18"
                                                                    data-y="center"
                                                                    data-voffset="226"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:400;e:Linear.easeNone;"
                                                                    data-style_hover="c:rgba(255, 42, 64, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 0);"
                                                                    data-transform_in="x:left;s:300;e:easeOutExpo;"
                                                                    data-transform_out="x:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    data-responsive="off"
                                                                    style="
                                                                        z-index: 11;
                                                                        white-space: nowrap;
                                                                        font-size: 14px;
                                                                        line-height: 17px;
                                                                        font-weight: 400;
                                                                        color: rgb(255, 255, 255);
                                                                        background-color: rgba(0, 0, 0, 0);
                                                                        padding: 15px 35px;
                                                                        border-color: rgb(255, 255, 255);
                                                                        border-style: solid;
                                                                        border-width: 1px;
                                                                        border-radius: 30px;
                                                                        outline: currentcolor none medium;
                                                                        box-shadow: none;
                                                                        box-sizing: border-box;
                                                                        cursor: pointer;
                                                                        visibility: hidden;
                                                                    "
                                                                >
                                                                    LEARN MORE
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                                <li
                                                    data-index="rs-68"
                                                    data-transition="fade"
                                                    data-slotamount="7"
                                                    data-hideafterloop="0"
                                                    data-hideslideonmobile="off"
                                                    data-easein="default"
                                                    data-easeout="default"
                                                    data-masterspeed="1000"
                                                    data-thumb="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2-100x50.jpg"
                                                    data-rotate="0"
                                                    data-saveperformance="off"
                                                    data-title="Slide"
                                                    data-param1=""
                                                    data-param2=""
                                                    data-param3=""
                                                    data-param4=""
                                                    data-param5=""
                                                    data-param6=""
                                                    data-param7=""
                                                    data-param8=""
                                                    data-param9=""
                                                    data-param10=""
                                                    data-description=""
                                                    class="tp-revslider-slidesli"
                                                    style="width: 100%; height: 100%; overflow: hidden;"
                                                >
                                                    <div class="slotholder" style="position: absolute; top: 0px; left: 0px; z-index: 0; width: 100%; height: 100%;">
                                                        <!--Runtime Modification - Img tag is Still Available for SEO Goals in Source - <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg" alt="" title="webproduct_darkbg2.jpg" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg defaultimg" data-no-retina="" width="1920" height="1080">-->
                                                        <div
                                                            class="tp-bgimg defaultimg"
                                                            style="
                                                                background-color: rgba(0, 0, 0, 0);
                                                                background-repeat: no-repeat;
                                                                background-image: url('http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg');
                                                                background-size: cover;
                                                                background-position: center center;
                                                                width: 100%;
                                                                height: 100%;
                                                                opacity: 0;
                                                            "
                                                            src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2.jpg"
                                                        ></div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <span
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-68-layer-1"
                                                                    data-x="center"
                                                                    data-hoffset=""
                                                                    data-y="center"
                                                                    data-voffset="-90"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:bottom;s:1000;e:easeOutExpo;"
                                                                    data-transform_out="y:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1000"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="z-index: 5; white-space: nowrap; font-size: 50px; line-height: 60px; font-weight: 300; color: rgb(255, 255, 255); visibility: hidden;"
                                                                >
                                                                    69STUDIO INCLUDES
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-68-layer-11"
                                                                    data-x="center"
                                                                    data-hoffset=""
                                                                    data-y="center"
                                                                    data-voffset=""
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:bottom;s:1000;e:easeOutExpo;"
                                                                    data-transform_out="y:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="1500"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="z-index: 6; white-space: nowrap; font-size: 110px; line-height: 80px; font-weight: 800; color: rgb(255, 255, 255); text-transform: uppercase; visibility: hidden;"
                                                                >
                                                                    EVERYTHING
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption tp-resizeme"
                                                                    id="slide-68-layer-3"
                                                                    data-x="center"
                                                                    data-hoffset=""
                                                                    data-y="center"
                                                                    data-voffset="110"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_in="y:bottom;s:1000;e:easeOutExpo;"
                                                                    data-transform_out="y:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2000"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    style="z-index: 7; white-space: nowrap; font-size: 25px; line-height: 40px; font-weight: 400; color: rgb(255, 255, 255); text-transform: uppercase; visibility: hidden;"
                                                                >
                                                                    you need to build incredible page layouts
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="tp-parallax-wrap" style="position: absolute; visibility: hidden;">
                                                        <div class="tp-loop-wrap" style="position: absolute;">
                                                            <div class="tp-mask-wrap" style="position: absolute;">
                                                                <div
                                                                    class="tp-caption rev-btn"
                                                                    id="slide-68-layer-4"
                                                                    data-x="center"
                                                                    data-hoffset=""
                                                                    data-y="center"
                                                                    data-voffset="245"
                                                                    data-width="['auto']"
                                                                    data-height="['auto']"
                                                                    data-transform_idle="o:1;"
                                                                    data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:400;e:Linear.easeNone;"
                                                                    data-style_hover="c:rgba(255, 42, 64, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(0, 0, 0, 0);"
                                                                    data-transform_in="y:bottom;s:1000;e:easeOutExpo;"
                                                                    data-transform_out="y:50px;opacity:0;s:1000;e:easeOutExpo;"
                                                                    data-start="2200"
                                                                    data-splitin="none"
                                                                    data-splitout="none"
                                                                    data-responsive_offset="on"
                                                                    data-responsive="off"
                                                                    style="
                                                                        z-index: 8;
                                                                        white-space: nowrap;
                                                                        font-size: 14px;
                                                                        line-height: 17px;
                                                                        font-weight: 400;
                                                                        color: rgb(255, 255, 255);
                                                                        background-color: rgba(0, 0, 0, 0);
                                                                        padding: 15px 35px;
                                                                        border-color: rgb(255, 255, 255);
                                                                        border-style: solid;
                                                                        border-width: 1px;
                                                                        border-radius: 30px;
                                                                        outline: currentcolor none medium;
                                                                        box-shadow: none;
                                                                        box-sizing: border-box;
                                                                        cursor: pointer;
                                                                        visibility: hidden;
                                                                    "
                                                                >
                                                                    LEARN MORE
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </li>
                                            </ul>
                                            <script>
                                                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                                var htmlDivCss = "";
                                                if (htmlDiv) {
                                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                                } else {
                                                    var htmlDiv = document.createElement("div");
                                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                                }
                                            </script>
                                            <div class="tp-bannertimer tp-bottom" style="visibility: hidden; width: 14.6615%; transform: translate3d(0px, 0px, 0px);"></div>
                                            <div class="tp-loader spinner2" style="display: none;">
                                                <div class="dot1"></div>
                                                <div class="dot2"></div>
                                                <div class="bounce1"></div>
                                                <div class="bounce2"></div>
                                                <div class="bounce3"></div>
                                            </div>
                                            <div class="tp-leftarrow tparrows hades" style="top: 50%; transform: matrix(1, 0, 0, 1, 0, -50); left: 0px; visibility: hidden; opacity: 0;">
                                                <div class="tp-arr-allwrapper">
                                                    <div
                                                        class="tp-arr-imgholder"
                                                        style="background-image: url('http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg1-100x50.jpg');"
                                                    ></div>
                                                </div>
                                            </div>
                                            <div class="tp-rightarrow tparrows hades" style="top: 50%; transform: matrix(1, 0, 0, 1, -100, -50); left: 100%; visibility: hidden; opacity: 0;">
                                                <div class="tp-arr-allwrapper">
                                                    <div
                                                        class="tp-arr-imgholder"
                                                        style="background-image: url('http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/revslider/web-product-dark/webproduct_darkbg2-100x50.jpg');"
                                                    ></div>
                                                </div>
                                            </div>
                                        </div>
                                        <script>
                                            var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                            var htmlDivCss = "";
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement("div");
                                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                        <script type="text/javascript">
                                            /******************************************
				-	PREPARE PLACEHOLDER FOR SLIDER	-
			******************************************/

                                            var setREVStartSize = function () {
                                                try {
                                                    var e = new Object(),
                                                        i = jQuery(window).width(),
                                                        t = 9999,
                                                        r = 0,
                                                        n = 0,
                                                        l = 0,
                                                        f = 0,
                                                        s = 0,
                                                        h = 0;
                                                    e.c = jQuery("#rev_slider_31_1");
                                                    e.gridwidth = [1240];
                                                    e.gridheight = [868];

                                                    e.sliderLayout = "fullscreen";
                                                    e.fullScreenAutoWidth = "off";
                                                    e.fullScreenAlignForce = "off";
                                                    e.fullScreenOffsetContainer = "";
                                                    e.fullScreenOffset = "";
                                                    if (
                                                        (e.responsiveLevels &&
                                                            (jQuery.each(e.responsiveLevels, function (e, f) {
                                                                f > i && ((t = r = f), (l = e)), i > f && f > r && ((r = f), (n = e));
                                                            }),
                                                            t > r && (l = n)),
                                                        (f = e.gridheight[l] || e.gridheight[0] || e.gridheight),
                                                        (s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth),
                                                        (h = i / s),
                                                        (h = h > 1 ? 1 : h),
                                                        (f = Math.round(h * f)),
                                                        "fullscreen" == e.sliderLayout)
                                                    ) {
                                                        var u = (e.c.width(), jQuery(window).height());
                                                        if (void 0 != e.fullScreenOffsetContainer) {
                                                            var c = e.fullScreenOffsetContainer.split(",");
                                                            if (c)
                                                                jQuery.each(c, function (e, i) {
                                                                    u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u;
                                                                }),
                                                                    e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0
                                                                        ? (u -= (jQuery(window).height() * parseInt(e.fullScreenOffset, 0)) / 100)
                                                                        : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0));
                                                        }
                                                        f = u;
                                                    } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                                    e.c.closest(".rev_slider_wrapper").css({ height: f });
                                                } catch (d) {
                                                    console.log("Failure at Presize of Slider:" + d);
                                                }
                                            };

                                            setREVStartSize();

                                            var tpj = jQuery;
                                            // tpj.noConflict();

                                            var revapi31;
                                            tpj(document).ready(function () {
                                                if (tpj("#rev_slider_31_1").revolution == undefined) {
                                                    revslider_showDoubleJqueryError("#rev_slider_31_1");
                                                } else {
                                                    revapi31 = tpj("#rev_slider_31_1")
                                                        .show()
                                                        .revolution({
                                                            sliderType: "standard",
                                                            jsFileLocation: "//trendytheme.net/demo2/wp/69/multipage/wp-content/plugins/revslider/public/assets/js/",
                                                            sliderLayout: "fullscreen",
                                                            dottedOverlay: "none",
                                                            delay: 6500,
                                                            navigation: {
                                                                keyboardNavigation: "off",
                                                                keyboard_direction: "horizontal",
                                                                mouseScrollNavigation: "off",
                                                                mouseScrollReverse: "default",
                                                                onHoverStop: "off",
                                                                arrows: {
                                                                    style: "hades",
                                                                    enable: true,
                                                                    hide_onmobile: false,
                                                                    hide_onleave: true,
                                                                    hide_delay: 200,
                                                                    hide_delay_mobile: 1200,
                                                                    tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
                                                                    left: {
                                                                        h_align: "left",
                                                                        v_align: "center",
                                                                        h_offset: 0,
                                                                        v_offset: 0,
                                                                    },
                                                                    right: {
                                                                        h_align: "right",
                                                                        v_align: "center",
                                                                        h_offset: 0,
                                                                        v_offset: 0,
                                                                    },
                                                                },
                                                            },
                                                            visibilityLevels: [1240, 1024, 778, 480],
                                                            gridwidth: 1240,
                                                            gridheight: 868,
                                                            lazyType: "none",
                                                            shadow: 0,
                                                            spinner: "spinner2",
                                                            stopLoop: "off",
                                                            stopAfterLoops: -1,
                                                            stopAtSlide: -1,
                                                            shuffle: "off",
                                                            autoHeight: "off",
                                                            fullScreenAutoWidth: "off",
                                                            fullScreenAlignForce: "off",
                                                            fullScreenOffsetContainer: "",
                                                            fullScreenOffset: "",
                                                            disableProgressBar: "on",
                                                            hideThumbsOnMobile: "off",
                                                            hideSliderAtLimit: 0,
                                                            hideCaptionAtLimit: 0,
                                                            hideAllCaptionAtLilmit: 0,
                                                            debugMode: false,
                                                            fallbacks: {
                                                                simplifyAll: "off",
                                                                nextSlideOnWindowFocus: "off",
                                                                disableFocusListener: false,
                                                            },
                                                        });
                                                }
                                            }); /*ready*/
                                        </script>
                                        <script>
                                            var htmlDivCss = " #rev_slider_31_1_wrapper .tp-loader.spinner2{ background-color: #ff2a40 !important; } ";
                                            var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement("div");
                                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                        <script>
                                            var htmlDivCss = unescape(
                                                ".hades.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.25%29%3B%0A%09width%3A100px%3B%0A%09height%3A100px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hades.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A30px%3B%0A%09color%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20100px%3B%0A%09text-align%3A%20center%3B%0A%20%20transition%3A%20background%200.3s%2C%20color%200.3s%3B%0A%7D%0A.hades.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hades.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A.hades.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20color%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%7D%0A.hades%20.tp-arr-allwrapper%20%7B%0A%20%20position%3Aabsolute%3B%0A%20%20left%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20background%3A%23888%3B%20%0A%20%20width%3A100px%3Bheight%3A100px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-ms-filter%3A%20%22progid%3Adximagetransform.microsoft.alpha%28opacity%3D0%29%22%3B%0A%20%20filter%3A%20alpha%28opacity%3D0%29%3B%0A%20%20-moz-opacity%3A%200.0%3B%0A%20%20-khtml-opacity%3A%200.0%3B%0A%20%20opacity%3A%200.0%3B%0A%20%20-webkit-transform%3A%20rotatey%28-90deg%29%3B%0A%20%20transform%3A%20rotatey%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%200%25%2050%25%3B%0A%20%20transform-origin%3A%200%25%2050%25%3B%0A%7D%0A.hades.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20left%3Aauto%3B%0A%20%20%20right%3A100%25%3B%0A%20%20%20-webkit-transform-origin%3A%20100%25%2050%25%3B%0A%20%20transform-origin%3A%20100%25%2050%25%3B%0A%20%20%20-webkit-transform%3A%20rotatey%2890deg%29%3B%0A%20%20transform%3A%20rotatey%2890deg%29%3B%0A%7D%0A%0A.hades%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20-ms-filter%3A%20%22progid%3Adximagetransform.microsoft.alpha%28opacity%3D100%29%22%3B%0A%20%20filter%3A%20alpha%28opacity%3D100%29%3B%0A%20%20-moz-opacity%3A%201%3B%0A%20%20-khtml-opacity%3A%201%3B%0A%20%20opacity%3A%201%3B%20%20%0A%20%20%20%20-webkit-transform%3A%20rotatey%280deg%29%3B%0A%20%20transform%3A%20rotatey%280deg%29%3B%0A%0A%20%7D%0A%20%20%20%20%0A.hades%20.tp-arr-iwrapper%20%7B%0A%7D%0A.hades%20.tp-arr-imgholder%20%7B%0A%20%20background-size%3Acover%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3Bleft%3A0px%3B%0A%20%20width%3A100%25%3Bheight%3A100%25%3B%0A%7D%0A.hades%20.tp-arr-titleholder%20%7B%0A%7D%0A.hades%20.tp-arr-subtitleholder%20%7B%0A%7D%0A%0A"
                                            );
                                            var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                            if (htmlDiv) {
                                                htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                            } else {
                                                var htmlDiv = document.createElement("div");
                                                htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                                document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                            }
                                        </script>
                                    </div>
                                    <div class="tp-fullwidth-forcer" style="width: 100%; height: 199px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_tta-container" data-vc-action="collapse">
                                <div
                                    class="vc_general vc_tta vc_tta-tabs vc_tta-color-grey vc_tta-style-tab-default vc_tta-shape-square vc_tta-o-shape-group vc_tta-o-no-fill vc_tta-tabs-position-top vc_tta-controls-align-left tabs-grid-column-4"
                                >
                                    <div class="vc_tta-tabs-container">
                                        <ul class="vc_tta-tabs-list">
                                            <li class="vc_tta-tab vc_active" data-vc-tab="">
                                                <a href="#1459897694524-c5f782cf-8dc8" data-vc-tabs="" data-vc-container=".vc_tta"><i class="vc_tta-icon vc_li vc_li-display"></i><span class="vc_tta-title-text">Web Design</span></a>
                                            </li>
                                            <li class="vc_tta-tab" data-vc-tab="">
                                                <a href="#1459910122597-b78a8106-4b35" data-vc-tabs="" data-vc-container=".vc_tta"><i class="vc_tta-icon vc_li vc_li-phone"></i><span class="vc_tta-title-text">Apps Design</span></a>
                                            </li>
                                            <li class="vc_tta-tab" data-vc-tab="">
                                                <a href="#1459910266124-60f8ed77-79cc" data-vc-tabs="" data-vc-container=".vc_tta"><i class="vc_tta-icon vc_li vc_li-params"></i><span class="vc_tta-title-text">Game Design</span></a>
                                            </li>
                                            <li class="vc_tta-tab" data-vc-tab="">
                                                <a href="#1459910497721-5b00aab2-f065" data-vc-tabs="" data-vc-container=".vc_tta"><i class="vc_tta-icon vc_li vc_li-bulb"></i><span class="vc_tta-title-text">Creative Ideas</span></a>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="vc_tta-panels-container">
                                        <div class="vc_tta-panels">
                                            <div class="vc_tta-panel vc_active" id="1459897694524-c5f782cf-8dc8" data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4 class="vc_tta-panel-title">
                                                        <a href="#1459897694524-c5f782cf-8dc8" data-vc-accordion="" data-vc-container=".vc_tta-container">
                                                            <i class="vc_tta-icon vc_li vc_li-display"></i><span class="vc_tta-title-text">Web Design</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner vc_custom_1459914409875">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1460525740010">
                                                                        <div class="wpb_wrapper">
                                                                            <h2><strong>Creative Web Design</strong></h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1459910664602">
                                                                        <div class="wpb_wrapper">
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                                                                minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                                                                vulputate velit esse molestie consequat,
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_btn3-container vc_btn3-inline">
                                                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-classic vc_btn3-icon-left vc_btn3-color-theme_primary_color" href="#" title="">
                                                                            <i class="vc_btn3-icon fa fa-long-arrow-right"></i> Learn More
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460526163587">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                                <img
                                                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/mac.png"
                                                                                    class="vc_single_image-img attachment-full"
                                                                                    alt=""
                                                                                    srcset="
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/mac.png         409w,
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/mac-300x185.png 300w
                                                                                    "
                                                                                    sizes="(max-width: 409px) 100vw, 409px"
                                                                                    width="409"
                                                                                    height="252"
                                                                                />
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1459910122597-b78a8106-4b35" data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4 class="vc_tta-panel-title">
                                                        <a href="#1459910122597-b78a8106-4b35" data-vc-accordion="" data-vc-container=".vc_tta-container">
                                                            <i class="vc_tta-icon vc_li vc_li-phone"></i><span class="vc_tta-title-text">Apps Design</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner vc_custom_1459899291214">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1460525807921">
                                                                        <div class="wpb_wrapper">
                                                                            <h2><strong>Creative Apps Design</strong></h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1459910635058">
                                                                        <div class="wpb_wrapper">
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                                                                minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                                                                vulputate velit esse molestie consequat,
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_btn3-container vc_btn3-inline">
                                                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-classic vc_btn3-icon-left vc_btn3-color-theme_primary_color" href="#" title="">
                                                                            <i class="vc_btn3-icon fa fa-long-arrow-right"></i> Learn More
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460526361211">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                                <img
                                                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/app.png"
                                                                                    class="vc_single_image-img attachment-full"
                                                                                    alt=""
                                                                                    srcset="
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/app.png         409w,
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/app-300x185.png 300w
                                                                                    "
                                                                                    sizes="(max-width: 409px) 100vw, 409px"
                                                                                    width="409"
                                                                                    height="252"
                                                                                />
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1459910266124-60f8ed77-79cc" data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4 class="vc_tta-panel-title">
                                                        <a href="#1459910266124-60f8ed77-79cc" data-vc-accordion="" data-vc-container=".vc_tta-container">
                                                            <i class="vc_tta-icon vc_li vc_li-params"></i><span class="vc_tta-title-text">Game Design</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner vc_custom_1459899291214">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1460525818014">
                                                                        <div class="wpb_wrapper">
                                                                            <h2><strong>Creative Game Design</strong></h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1459910606857">
                                                                        <div class="wpb_wrapper">
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                                                                minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                                                                vulputate velit esse molestie consequat,
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_btn3-container vc_btn3-inline">
                                                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-classic vc_btn3-icon-left vc_btn3-color-theme_primary_color" href="#" title="">
                                                                            <i class="vc_btn3-icon fa fa-long-arrow-right"></i> Learn More
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460526376739">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                                <img
                                                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/game.png"
                                                                                    class="vc_single_image-img attachment-full"
                                                                                    alt=""
                                                                                    srcset="
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/game.png         409w,
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/game-300x185.png 300w
                                                                                    "
                                                                                    sizes="(max-width: 409px) 100vw, 409px"
                                                                                    width="409"
                                                                                    height="252"
                                                                                />
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="vc_tta-panel" id="1459910497721-5b00aab2-f065" data-vc-content=".vc_tta-panel-body">
                                                <div class="vc_tta-panel-heading">
                                                    <h4 class="vc_tta-panel-title">
                                                        <a href="#1459910497721-5b00aab2-f065" data-vc-accordion="" data-vc-container=".vc_tta-container">
                                                            <i class="vc_tta-icon vc_li vc_li-bulb"></i><span class="vc_tta-title-text">Creative Ideas</span>
                                                        </a>
                                                    </h4>
                                                </div>
                                                <div class="vc_tta-panel-body">
                                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner vc_custom_1459899291214">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1460530325639">
                                                                        <div class="wpb_wrapper">
                                                                            <h2><strong>Creative&nbsp;Ideas</strong></h2>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wpb_text_column wpb_content_element vc_custom_1459910568658">
                                                                        <div class="wpb_wrapper">
                                                                            <p>
                                                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat. Ut wisi enim ad
                                                                                minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in
                                                                                vulputate velit esse molestie consequat,
                                                                            </p>
                                                                        </div>
                                                                    </div>
                                                                    <div class="vc_btn3-container vc_btn3-inline">
                                                                        <a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-classic vc_btn3-icon-left vc_btn3-color-theme_primary_color" href="#" title="">
                                                                            <i class="vc_btn3-icon fa fa-long-arrow-right"></i> Learn More
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="wpb_column vc_column_container vc_col-sm-6">
                                                            <div class="vc_column-inner">
                                                                <div class="wpb_wrapper">
                                                                    <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460526493497">
                                                                        <figure class="wpb_wrapper vc_figure">
                                                                            <div class="vc_single_image-wrapper vc_box_border_grey">
                                                                                <img
                                                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/idea.png"
                                                                                    class="vc_single_image-img attachment-full"
                                                                                    alt=""
                                                                                    srcset="
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/idea.png         409w,
                                                                                        http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/idea-300x185.png 300w
                                                                                    "
                                                                                    sizes="(max-width: 409px) 100vw, 409px"
                                                                                    width="409"
                                                                                    height="252"
                                                                                />
                                                                            </div>
                                                                        </figure>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper vc_custom_1461778734831 vc_row-has-fill">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-offset-3 vc_col-md-8">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_text_column wpb_content_element vc_custom_1461536408274">
                                <div class="wpb_wrapper">
                                    <h2><span class="theme-color">69</span>STUDIO IS A CREATIVE WORDPRESS THEME</h2>
                                </div>
                            </div>
                            <div class="wpb_text_column wpb_content_element vc_custom_1459910992473">
                                <div class="wpb_wrapper">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                        aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit e
                                    </p>
                                </div>
                            </div>
                            <div class="vc_btn3-container vc_btn3-inline"><a class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-rounded vc_btn3-style-classic vc_btn3-color-theme_primary_color" href="#" title="">Buy Now</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper vc_custom_1460526862572">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="sction-title-wrapper text-center vc_custom_1460527031403">
                                <h2 style="" class="section-title theme-color">What We Do?</h2>
                                <div class="sub-title" style="">
                                    <p>
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur<br />
                                        magni dolores eos qui ratione voluptatem sequi nesciunt.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper vc_custom_1459912407416">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="row icon-block-wrapper block-style-border hover-theme-color three-column">
                                <div class="icon-block-grid col-sm-6 col-md-4" style="">
                                    <div class="icon-block icon-default icon-position-center">
                                        <div class="tt-icon flat-icon"><i class="flaticon-certificate3 theme-color" style=""></i></div>
                                        <div class="tt-content">
                                            <h3 class="" style="">Branding &amp; Identity</h3>
                                            <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="icon-block-grid col-sm-6 col-md-4" style="">
                                    <div class="icon-block icon-default icon-position-center">
                                        <div class="tt-icon flat-icon"><i class="flaticon-smartphone97 theme-color" style=""></i></div>
                                        <div class="tt-content">
                                            <h3 class="" style="">Web Design &amp; Development</h3>
                                            <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="icon-block-grid col-sm-6 col-md-4" style="">
                                    <div class="icon-block icon-default icon-position-center">
                                        <div class="tt-icon flat-icon"><i class="flaticon-cart theme-color" style=""></i></div>
                                        <div class="tt-content">
                                            <h3 class="" style="">Ecommerce Design</h3>
                                            <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="icon-block-grid col-sm-6 col-md-4" style="">
                                    <div class="icon-block icon-default icon-position-center">
                                        <div class="tt-icon flat-icon"><i class="flaticon-paint104 theme-color" style=""></i></div>
                                        <div class="tt-content">
                                            <h3 class="" style="">UI/UX Design</h3>
                                            <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="icon-block-grid col-sm-6 col-md-4" style="">
                                    <div class="icon-block icon-default icon-position-center">
                                        <div class="tt-icon flat-icon"><i class="flaticon-light92 theme-color" style=""></i></div>
                                        <div class="tt-content">
                                            <h3 class="" style="">Inovative Idea</h3>
                                            <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="icon-block-grid col-sm-6 col-md-4" style="">
                                    <div class="icon-block icon-default icon-position-center">
                                        <div class="tt-icon flat-icon"><i class="flaticon-document169 theme-color" style=""></i></div>
                                        <div class="tt-content">
                                            <h3 class="" style="">PSD To HTML</h3>
                                            <p>Quisque velit nisi, pretium ut lacinia in, elementum id enim. Nulla quis lorem ut libero malesuada feugiat</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-7">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <h2 style="font-size: 30px; line-height: 45px; text-align: left; font-family: Roboto Slab; font-weight: 700; font-style: normal;" class="vc_custom_heading vc_custom_1459914637413">RESPONSIVE</h2>
                            <div style="font-size: 45px; line-height: 36px; text-align: left; font-family: Roboto Slab; font-weight: 700; font-style: normal;" class="vc_custom_heading theme-color vc_custom_1460905731442">
                                ON ALL DEVICES
                            </div>
                            <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                                <div class="wpb_wrapper">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                        aliquip ex ea commodo consequat incididunt ut labore et dolore magna aliqua.
                                    </p>
                                </div>
                            </div>
                            <div class="wpb_text_column wpb_content_element vc_custom_1459914012589">
                                <div class="wpb_wrapper">
                                    <p>
                                        Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
                                        aliquip ex ea commodo consequat incididunt ut labore et dolore magna aliqua.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-sm-offset-1">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460542917437">
                                <figure class="wpb_wrapper vc_figure">
                                    <div class="vc_single_image-wrapper vc_box_border_grey">
                                        <img
                                            src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/responsive-tab.png"
                                            class="vc_single_image-img attachment-full"
                                            alt=""
                                            srcset="
                                                http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/responsive-tab.png         327w,
                                                http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/responsive-tab-181x300.png 181w
                                            "
                                            sizes="(max-width: 327px) 100vw, 327px"
                                            width="327"
                                            height="543"
                                        />
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper vc_custom_1461775198634 vc_row-has-fill">
        <div class="container">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-2 vc_col-xs-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="process-box text-center flat-icon" style="">
                                <i class="flaticon-cup7" style=""></i>
                                <h3 style="">Meeting</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-2 vc_col-xs-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="process-box text-center flat-icon" style="">
                                <i class="flaticon-light110" style=""></i>
                                <h3 style="">Planning</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-2 vc_col-xs-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="process-box text-center flat-icon" style="">
                                <i class="flaticon-paint104" style=""></i>
                                <h3 style="">Designing</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-2 vc_col-xs-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="process-box text-center flat-icon" style="">
                                <i class="flaticon-document169" style=""></i>
                                <h3 style="">Developing</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-2 vc_col-xs-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="process-box text-center flat-icon" style="">
                                <i class="flaticon-tool45" style=""></i>
                                <h3 style="">Testing</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-4 vc_col-md-2 vc_col-xs-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="process-box text-center flat-icon" style="">
                                <i class="flaticon-wrapped4" style=""></i>
                                <h3 style="">Delivary</h3>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper vc_custom_1468391659580">
        <div class="container-fullwidth">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="sction-title-wrapper text-center vc_custom_1460527860550">
                                <h2 style="" class="section-title theme-color">Our Awesome Works</h2>
                                <div class="sub-title" style="">
                                    <p>
                                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur<br />
                                        magni dolores eos qui ratione voluptatem sequi nesciunt.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper vc_custom_1468391724027">
        <div class="container-fullwidth">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="portfolio-container">
                                <div class="tt-filter-wrap filter-rounded text-center default-color">
                                    <ul class="tt-filter list-inline">
                                        <li><button class="active" data-group="all">All</button></li>
                                        <li><button data-group="app-design">App Design</button></li>
                                        <li><button data-group="graphic-design">Graphic Design</button></li>
                                        <li><button data-group="interface">Interface</button></li>
                                        <li><button data-group="web">Web</button></li>
                                    </ul>
                                </div>
                                <div class="row">
                                    <div class="tt-grid shuffle" style="position: relative; height: 426px; transition: height 250ms ease-out 0s;">
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["app-design"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img
                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-9-800x600.jpg"
                                                    class="img-responsive wp-post-image"
                                                    alt="Creative Design"
                                                    width="800"
                                                    height="600"
                                                />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/creative-design/">Creative Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/creative-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["web"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(285px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-14-800x600.jpg" class="img-responsive wp-post-image" alt="Web Design" width="800" height="600" />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/web-design/">Web Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/web-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["graphic-design"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(570px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img
                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-10-800x600.jpg"
                                                    class="img-responsive wp-post-image"
                                                    alt="Business Cards"
                                                    width="800"
                                                    height="600"
                                                />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/business-cards/">Business Cards</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/business-cards/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["web"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(855px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-2-800x600.jpg" class="img-responsive wp-post-image" alt="Brand Design" width="800" height="600" />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/brand-design/">Brand Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/brand-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["interface"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(1140px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img
                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-8-800x600.jpg"
                                                    class="img-responsive wp-post-image"
                                                    alt="Image Manipulation"
                                                    width="800"
                                                    height="600"
                                                />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/image-manipulation/">Image Manipulation</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/image-manipulation/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["app-design"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(0px, 213px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-4-800x600.jpg" class="img-responsive wp-post-image" alt="Form Design" width="800" height="600" />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/form-design/">Form Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/form-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["interface"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(285px, 213px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-3-800x600.jpg" class="img-responsive wp-post-image" alt="Game Design" width="800" height="600" />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/game-design/">Game Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/game-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["graphic-design"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(570px, 213px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img
                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-7-800x600.jpg"
                                                    class="img-responsive wp-post-image"
                                                    alt="Character Design"
                                                    width="800"
                                                    height="600"
                                                />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/character-design/">Character Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/character-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["web"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(855px, 213px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img
                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-13-800x600.jpg"
                                                    class="img-responsive wp-post-image"
                                                    alt="Dashboard Design"
                                                    width="800"
                                                    height="600"
                                                />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/dashboard-design/">Dashboard Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/dashboard-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div
                                            class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                            data-groups='["interface"]'
                                            style="position: absolute; top: 0px; left: 0px; transform: translate3d(1140px, 213px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                        >
                                            <div class="portfolio hover-one">
                                                <div class="tt-overlay-theme-color"></div>
                                                <img
                                                    src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/02/portfolio-6-800x600.jpg"
                                                    class="img-responsive wp-post-image"
                                                    alt="Interface Design"
                                                    width="800"
                                                    height="600"
                                                />
                                                <div class="portfolio-info">
                                                    <h3 class="project-title"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/interface-design/">Interface Design</a></h3>
                                                    <div class="project-meta"><a href="http://trendytheme.net/demo2/wp/69/multipage/portfolio/interface-design/" class="btn btn-outline">View full Project</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vc_row section-wrapper">
        <div class="container-fullwidth">
            <div class="row">
                <div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="case-study-wrapper theme-bg" style="">
                                <div class="case-study-left">
                                    <h2 style="color: #ffffff;">Best theme ever! Discover 69Studio!</h2>
                                    <div class="case-study-content" style="color: #ffffff;"><p>Yes, strangely enough we’ve had a couple of people ask how they can contact us. So if you’re interested in discussing project ……….</p></div>
                                    <a href="#" class="btn btn-outline btn-lg">Discover More</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-6">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="case-study-wrapper dark-bg" style="">
                                <div class="case-study-right">
                                    <h2 style="color: #ffffff;">Clean and powerful multipurpose theme!</h2>
                                    <div class="case-study-content" style="color: #ffffff;"><p>Yes, strangely enough we’ve had a couple of people ask how they can contact us. So if you’re interested in discussing project ……….</p></div>
                                    <a href="#" class="btn btn-outline btn-lg">Purchase Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    <footer class="footer-section footer-multi-wrapper">
        <div class="container">
            <div class="row">
                <div class="tt-sidebar-wrapper footer-sidebar clearfix text-left" role="complementary">
                    <div id="black-studio-tinymce-2" class="col-md-3 col-sm-6 widget widget_black_studio_tinymce">
                        <h3 class="widget-title">About</h3>
                        <div class="textwidget">
                            <p>
                                Quickly enhance worldwide strategic theme areas with accurate outsourcing synergistically create value-added.<br />
                                <span class="dashed-border"></span>
                            </p>
                            <ul>
                                <li><i class="fa fa-map-marker"></i>123 Fake Street- London 12358</li>
                                <li><i class="fa fa-phone"></i>666 777 888, 111 222 333</li>
                                <li><i class="fa fa-envelope"></i>username@host.com</li>
                            </ul>
                        </div>
                    </div>
                    <div id="nav_menu-2" class="col-md-3 col-sm-6 widget widget_nav_menu">
                        <h3 class="widget-title">Quick Links</h3>
                        <div class="menu-quick-links-container">
                            <ul id="menu-quick-links" class="menu">
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="http://trendytheme.net/demo2/wp/69/multipage/about-69studio/">About</a></li>
                                <li id="menu-item-2695" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2695"><a href="http://trendytheme.net/demo2/wp/69/multipage/blog/">Blog</a></li>
                                <li id="menu-item-3687" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3687"><a href="http://trendytheme.net/demo2/wp/69/multipage/blog-grid/">Blog Grid</a></li>
                                <li id="menu-item-3690" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3690"><a href="http://trendytheme.net/demo2/wp/69/multipage/fun-facts/">Fun Facts</a></li>
                                <li id="menu-item-3691" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3691"><a href="http://trendytheme.net/demo2/wp/69/multipage/hero-blocks/">Hero Blocks</a></li>
                                <li id="menu-item-3692" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3692"><a href="http://trendytheme.net/demo2/wp/69/multipage/services/">Services</a></li>
                                <li id="menu-item-3694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3694"><a href="http://trendytheme.net/demo2/wp/69/multipage/team-member/">Members</a></li>
                                <li id="menu-item-3696" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3696"><a href="http://trendytheme.net/demo2/wp/69/multipage/fun-facts/">Fun Facts</a></li>
                                <li id="menu-item-3697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3697"><a href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-clients/">Clients</a></li>
                                <li id="menu-item-3698" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3698"><a href="http://trendytheme.net/demo2/wp/69/multipage/shortcode-photo-gallery/">Gallery</a></li>
                                <li id="menu-item-3688" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3688"><a href="http://trendytheme.net/demo2/wp/69/multipage/contact-us/">Contact Us</a></li>
                                <li id="menu-item-3689" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3689"><a href="http://trendytheme.net/demo2/wp/69/multipage/faq/">FAQ</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="copyright">Copyright © 2021 Pemerintah Provinsi Jawa Timur - UPT Balai Latihan Kerja, Pasuruan</div>
                    </div>
                    <div class="col-sm-5">
                        <div class="social-links-wrap text-right">
                            <div class="social-icon">
                                <ul class="list-inline">
                                    <li>
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fa fa-google-plus"></i></a>
                                    </li>
                                    <li>
                                        <a href="#" target="_blank"><i class="fa fa-pinterest"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </footer>
    
    
    <script type="text/javascript">
        window.jQuery || document.write('<script src="{{asset("assets/front/js/jquery-1.4.min.js")}}"><\/script>');
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, "woocommerce-js");
        document.body.className = c;
    </script>
    <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
    </script>
    <link rel="stylesheet" id="vc_google_fonts_roboto_slab100300regular700-css" href="https://fonts.googleapis.com/css?family=Roboto+Slab%3A100%2C300%2Cregular%2C700" type="text/css" media="all" />
    <script type="text/javascript">
        var wpcf7 = { apiSettings: { root: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/wp-json\/contact-form-7\/v1", namespace: "contact-form-7\/v1" } };
    </script>
    <script type="text/javascript">
        var woocommerce_params = { ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%" };
    </script>
    <script type="text/javascript">
        var wc_cart_fragments_params = {
            ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php",
            wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%",
            cart_hash_key: "wc_cart_hash_b930bcc34884a3f2cd0a7e5efdbd1f43",
            fragment_name: "wc_fragments_b930bcc34884a3f2cd0a7e5efdbd1f43",
            request_timeout: "5000",
        };
    </script>
    <script type="text/javascript">
        var sixtyninestudioJSObject = { sixtyninestudio_sticky_menu: "1" };
    </script>
    <script type="text/javascript" defer="" src="{{asset('assets/front/js')}}/autoptimize_af513b33625b472e83375336554cbcc1.js"></script>
    <div id="toTop" style="display: block;"><i class="fa fa-angle-up"></i></div>
</body>

</html>
