<html
    lang="en-US"
    class="wf-opensans-n3-active wf-opensans-n4-active wf-opensans-n6-active wf-opensans-n7-active wf-opensans-n8-active wf-opensans-i3-active wf-opensans-i4-active wf-opensans-i6-active wf-opensans-i7-active wf-opensans-i8-active wf-robotoslab-n1-active wf-robotoslab-n3-active wf-robotoslab-n4-active wf-robotoslab-n7-active wf-active js_active vc_desktop vc_transform vc_transform skrollr skrollr-desktop"
>
<head>
    <meta charset="UTF-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{asset('assets/front')}}/images/favicon.ico" />
    <link type="text/css" media="all" href="{{asset('assets/front/css')}}/autoptimize_2c934750174b77ec2a6e92709722e2b5.css" rel="stylesheet" />
    <link type="text/css" media="only screen and (max-width: 768px)" href="{{asset('assets/front/css')}}/autoptimize_dcb2de333eec7ab4ae31385ed8d6a393.css" rel="stylesheet" />
    <link type="text/css" media="screen" href="{{asset('assets/front/css')}}/autoptimize_281a9c0efc121555d2dbded049025ed5.css" rel="stylesheet" />
    <link type="text/css" media="screen" href="{{asset('assets/front/css')}}/autoptimize_fff2a198e0d30396873fd5ea586683d0.css" rel="stylesheet" />
    <title>UPT Balai Latihan Kerja - Pasuruan</title>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <script src="{{asset('assets/front/js')}}/jquery-1.9.1.min.js" ></script>

    <script type="text/javascript" src="https://trendytheme.net/demo2/wp/69/multipage/wp-includes/js/jquery/jquery.js"></script>
    <script type="text/javascript">
        var zilla_likes = { ajaxurl: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php" };
    </script>
    <script type="text/javascript">
        var wc_add_to_cart_params = {
            ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php",
            wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%",
            i18n_view_cart: "View cart",
            cart_url: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/cart\/",
            is_cart: "",
            cart_redirect_after_add: "no",
        };
    </script>
    <noscript>
        <style>
            .woocommerce-product-gallery {
                opacity: 1 !important;
            }
        </style>
    </noscript>
    <link rel="icon" href="{{asset('assets/front/images/favicon.ico')}}" sizes="32x32" />
    <noscript>
        <style type="text/css">
            .wpb_animate_when_almost_visible {
                opacity: 1;
            }
        </style>
    </noscript>


    {{-- CUSTOM ADDITIONAL BOJES --}}
    {{-- <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet"> --}}
    <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
    <link href="{{asset('assets/front/css/custom.css')}}" rel="stylesheet">
    {{-- CUSTOM ADDITIONAL BOJES --}}

    <link rel="stylesheet" href="{{asset('assets/front/plugins/datatable')}}/dataTables.bootstrap.min.css" />
    <script src="{{asset('assets/front/plugins/datatable')}}/jquery-3.5.1.js"></script>
    <script src="{{asset('assets/front/plugins/datatable')}}/jquery.dataTables.min.js"></script>
    <script src="{{asset('assets/front/plugins/datatable')}}/dataTables.bootstrap.min.js"></script>

    <link rel="stylesheet" href="{{asset('assets/front/plugins/datepicker/bootstrap-datepicker.min.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/front/plugins/datepicker/font-awesome.min.css')}}" />
    <script src="{{asset('assets/front/plugins/datepicker/bootstrap-datepicker.min.js')}}"></script>
    {{-- <script src="{{asset('assets/front/plugins/datepicker/jquery.2.1.3.min.js')}}"></script> --}}
    </head>
    <body
        id="home"
        class="page-template page-template-page-templates page-template-template-vc page-template-page-templatestemplate-vc-php page page-id-8 wp-embed-responsive woocommerce-js header-default has-header-search footer-multipage has-site-logo wpb-js-composer js-comp-ver-5.7 vc_responsive"
        data-spy="scroll"
        data-target=".navbar"
        data-offset="100"
    >
        
        <div id="undefined-sticky-wrapper" class="sticky-wrapper" style="height: 92px;">
            <div class="header-wrapper" style="">
                <x-frontend.navigation />
            </div>
        </div>
        
        
        @yield('content')


        <x-frontend.footer />



        <script type="text/javascript">
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, "woocommerce-js");
            document.body.className = c;
        </script>


        <script type="text/javascript">
            window.jQuery || document.write('<script src="{{asset("assets/front/js/jquery-1.4.min.js")}}"><\/script>');

            const loading_text = 'Harap tunggu...';
            const data_saved = 'Data Berhasil Disimpan';
            const data_deleted = 'Data Berhasil Dihapus';
            const confirm_delete_text = 'Apakah Anda yakin ingin menghapus data ini ?';


            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });

            function displayErrorSwal(msg){
                if(msg == null){
                swal.fire("Oops !", "Server sedang bermasalah. Harap menghubungi administrator kami", "error");
                }else{
                swal.fire("Oops !", msg, "error");
                }
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Submit');
            }


            function displayWarningSwal(msg){
                if(msg == null){
                    swal.fire("Oops !", "Harap memeriksa kembali inputan Anda", "warning");
                }else{
                    swal.fire("Oops !", msg, "warning");
                }
            }

            function reload_captcha(){
                $('#captcha').val('');
                $.ajax({
                    type: 'GET',
                    url: '{{route('reload_captcha')}}',
                    success: function (data) {
                        $(".captcha span").html(data.captcha);
                    }
                });
            }
        </script>


        <script type="text/javascript">
            function revslider_showDoubleJqueryError(sliderID) {
                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                jQuery(sliderID).show().html(errorMessage);
            }
        </script>
        <script type="text/javascript">
            var wpcf7 = { apiSettings: { root: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/wp-json\/contact-form-7\/v1", namespace: "contact-form-7\/v1" } };
        </script>
        <script type="text/javascript">
            var woocommerce_params = { ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%" };
        </script>
        <script type="text/javascript">
            var sixtyninestudioJSObject = { sixtyninestudio_sticky_menu: "1" };
        </script>
        <script type="text/javascript" defer="" src="{{asset('assets/front/js')}}/autoptimize_af513b33625b472e83375336554cbcc1.js"></script>
        <script type="text/javascript" defer src="{{asset('assets/front/js')}}/autoptimize_5bfdab92b9d18cd8d7e84b4db0e96edf.js"></script>
        <script src="{{asset('assets/back/js')}}/sweetalert2@10.js"></script>
        
        <!--<script type="text/javascript" defer="" src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/cache/autoptimize/js/autoptimize_cd62285b02c877f68c256b9d41962a69.js"></script>-->
        <div id="toTop" style="display: none;"><i class="fa fa-angle-up"></i></div>

        @yield('js')

        <script>
            $(document).ready(function () {
                $('#datatable').DataTable();

                var table16 = $('#datatable16').DataTable({
                    processing: true,
                    serverside: false,
                    pageLength: 16
                });
            });
        </script>
    </body>
</html>
