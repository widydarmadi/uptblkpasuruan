<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <!-- favicon icon -->
        <link rel="shortcut icon" href="{{asset('assets/front')}}/images/favicon.ico" />
        <link type="text/css" media="all" href="{{asset('assets/front/css')}}/autoptimize_edf689da60de82ae2d4e0ff84c0e5516.css" rel="stylesheet" />
        <link type="text/css" media="only screen and (max-width: 768px)" href="{{asset('assets/front/css')}}/autoptimize_dcb2de333eec7ab4ae31385ed8d6a393.css" rel="stylesheet" />
        <link type="text/css" media="screen" href="{{asset('assets/front/css')}}/autoptimize_281a9c0efc121555d2dbded049025ed5.css" rel="stylesheet" />
        <title>UPT Balai Latihan Kerja - Pasuruan</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <script src="{{asset('assets/front/js')}}/jquery-1.9.1.min.js" ></script>
        {{-- <link href="https://fonts.googleapis.com/css2?family=Hind+Siliguri:wght@300;700&display=swap" rel="stylesheet"> --}}
        <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet">
        <link href="{{asset('assets/front/css/custom.css')}}" rel="stylesheet">
    </head>
    <body
        id="home"
        class="home page-template page-template-page-templates page-template-template-home page-template-page-templatestemplate-home-php page page-id-14 wp-embed-responsive woocommerce-js header-transparent has-header-search footer-multipage has-site-logo wpb-js-composer js-comp-ver-5.7 vc_responsive"
        data-spy="scroll"
        data-target=".navbar"
        data-offset="100"
    >
    <div id="preloader" style="background-color: rgb(255, 255, 255); display: none;">
        <div id="status" style="display: none;"><div class="status-mes" style="background-image: url(http://trendytheme.net/demo2/wp/69/multipage/wp-content/themes/sixtyninestudio/images/preloader.gif);"></div></div>
    </div>
    <div id="undefined-sticky-wrapper" class="sticky-wrapper is-sticky" style="height: 90px;">
        <div class="header-wrapper" style="position: fixed; top: 0px;">
            <div style="background: #ffffff;">
                <div class="container" >
                    <div class="row">
                        <div class="col-12">
                            <h1>
                                <a href="{{url('/')}}" title="Halaman Utama">
                                    <img style="height: 50px; margin-top:10px;" class="site-logo hidden-xs" src="{{asset('assets/front/images/logo-top.png')}}" alt="UPT BLK Pasuruan" />
                                    <img style="height: 60px; margin-top:10px;" class="mobile-logo visible-xs" src="{{asset('assets/front/images/logo-top.png')}}" alt="UPT BLK Pasuruan" />
                                </a>
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
            
            <nav class="navbar navbar-default">
                <div class="container">
                    
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mobile-toggle"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
                        <div class="navbar-brand">
                            {{-- <h1>
                                <a href="http://trendytheme.net/demo2/wp/69/multipage/" title="Sixty Nine Studio">
                                    <img class="site-logo hidden-xs" src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/LOGO.png" alt="Sixty Nine Studio" />
                                    <img class="mobile-logo visible-xs" src="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/LOGO.png" alt="Sixty Nine Studio" />
                                </a>
                            </h1> --}}
                            
                        </div>
                    </div>
                    <div class="main-menu-wrapper hidden-xs clearfix">
                        <div class="main-menu">
                            <ul id="menu-primary-menu" class="menu nav navbar-nav">
                                @php
                                $load_parent_menu = \App\Models\M_menu_front::where('aktif', '1')->whereNull('id_parent')->get();
                                @endphp

                                @foreach ($load_parent_menu as $item)
                                <li
                                    class="menu-item" id="menu-item-{{$item->id_m_menu}}"
                                >
                                    @php
                                    $load_child_menu = \App\Models\M_menu_front::where('id_parent', $item->id_m_menu)->where('aktif', '1')->get();

                                    @endphp
                                    <a title="{{$item->nm_menu}}" href="{{($item->url != '') ? url($item->url) : 'javascript:void(0)'}}">{{$item->nm_menu}}
                                        @if(count($load_child_menu) > 0)
                                            <span class="fa fa-angle-down"></span>
                                        @endif
                                    </a>
                                    @if(count($load_child_menu) > 0)
                                    <div class="dropdown-wrapper menu-item-depth-0">
                                        <ul role="menu" class="dropdown-menu">
                                            @foreach ($load_child_menu as $item)
                                                <li id="menu-item-{{$item->id_m_menu}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                                    <a href="{{($item->url != '') ? url($item->url) : 'javascript:void(0)'}}">{{$item->nm_menu}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>



                    <div class="visible-xs">
                        <div class="mobile-menu collapse navbar-collapse mobile-toggle">
                            <ul id="menu-primary-menu-1" class="menu nav navbar-nav">
                                @php
                                $load_parent_menu = \App\Models\M_menu_front::where('aktif', '1')->whereNull('id_parent')->get();
                                @endphp

                                @foreach ($load_parent_menu as $item)
                                <li
                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-{{$item->id_m_menu}} has-menu-child" id="menu-item-{{$item->id_m_menu}}"
                                >
                                    @php
                                    $load_child_menu = \App\Models\M_menu_front::where('id_parent', $item->id_m_menu)->where('aktif', '1')->get();
                                    @endphp
                                    <a title="{{$item->nm_menu}}" href="{{($item->url != '') ? $item->url : 'javascript:void(0)'}}">{{$item->nm_menu}}
                                        @if(count($load_child_menu) > 0)
                                            <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-{{$item->id_m_menu}}" aria-expanded="false">
                                                <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                            </button>
                                        @endif
                                    </a>
                                    @if(count($load_child_menu) > 0)
                                        <ul role="menu" class="collapse dropdown-menu-{{$item->id_m_menu}}">
                                            @foreach ($load_child_menu as $item)
                                                <li id="menu-item-{{$item->id_m_menu}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                                    <a href="{{($item->url != '') ? $item->url : 'javascript:void(0)'}}">{{$item->nm_menu}}</a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    @endif
                                </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </nav>
        </div>
    </div>
    
    
    @yield('content')
    
    
    
    <footer class="footer-section footer-multi-wrapper">
        <div class="container">
            <div class="row">
                <div class="tt-sidebar-wrapper footer-sidebar clearfix text-left" role="complementary">
                    <div id="black-studio-tinymce-2" class="col-md-4 col-sm-6 widget widget_black_studio_tinymce">
                        <h3 class="widget-title">UPT Balai Latihan Kerja Pasuruan</h3>
                        <div class="textwidget">
                            <p>
                                Jl. Pahlawan Sunaryo No.96S, Pateban, Kb. Waris, 
                                <br>
                                Kec. Pandaan, Pasuruan, 
                                <br>
                                Jawa Timur 67156<br />
                                <span class="dashed-border"></span>
                                Senin s/d Kamis : 07.00 s/d 15.30 WIB
                                <br>Jumat : 07.00 s/d 14.30 WIB
                            </p>
                            <ul>
                                <li><i class="fa fa-instagram"></i>@uptblkpasuruan</li>
                                <li><i class="fa fa-phone"></i>085806785550 (whatsapp only)</li>
                            </ul>
                        </div>
                    </div>
                    <div id="nav_menu-2" class="col-md-4 col-sm-6 widget widget_nav_menu">
                        <h3 class="widget-title">Quick Links</h3>
                        <div class="menu-quick-links-container">
                            <ul id="menu-quick-links" class="menu">
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('pengumuman')}}">Pengumuman</a></li>
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('galeri')}}">Galeri</a></li>
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('agenda')}}">Agenda</a></li>
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('program-kegiatan')}}">Program</a></li>
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('info-lowongan')}}">Lowongan</a></li>
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('konten/hubungi-kami')}}">Hubungi Kami</a></li>
                                <li id="menu-item-2694" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2694"><a href="{{url('daftar')}}">Daftar</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-7">
                        <div class="copyright">Copyright © 2021 Pemerintah Provinsi Jawa Timur - UPT Balai Latihan Kerja, Pasuruan</div>
                    </div>
                    
                </div>
            </div>
        </div>
    </footer>
    
    
    <script type="text/javascript">
        window.jQuery || document.write('<script src="{{asset("assets/front/js/jquery-1.4.min.js")}}"><\/script>');
        // var c = document.body.className;
        // c = c.replace(/woocommerce-no-js/, "woocommerce-js");
        // document.body.className = c;

        const loading_text = 'Harap tunggu...';
        const data_saved = 'Data Berhasil Disimpan';
        const data_deleted = 'Data Berhasil Dihapus';
        const confirm_delete_text = 'Apakah Anda yakin ingin menghapus data ini ?';


        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        function displayErrorSwal(msg){
            if(msg == null){
            swal.fire("Oops !", "Server sedang bermasalah. Harap menghubungi administrator kami", "error");
            }else{
            swal.fire("Oops !", msg, "error");
            }
            $("#submitform").removeAttr('disabled');
            $("#submitform span").text('Submit');
        }


        function displayWarningSwal(msg){
            if(msg == null){
                swal.fire("Oops !", "Harap memeriksa kembali inputan Anda", "warning");
            }else{
                swal.fire("Oops !", msg, "warning");
            }
        }

        function reload_captcha(){
            $('#captcha').val('');
            $.ajax({
                type: 'GET',
                url: 'reload-captcha',
                success: function (data) {
                    $(".captcha span").html(data.captcha);
                }
            });
        }
    </script>
    <script type="text/javascript">
        function revslider_showDoubleJqueryError(sliderID) {
            var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
            errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
            errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
            errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
            errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
            jQuery(sliderID).show().html(errorMessage);
        }
    </script>
    <link rel="stylesheet" id="vc_google_fonts_roboto_slab100300regular700-css" href="https://fonts.googleapis.com/css?family=Roboto+Slab%3A100%2C300%2Cregular%2C700" type="text/css" media="all" />
    <script type="text/javascript">
        var wpcf7 = { apiSettings: { root: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/wp-json\/contact-form-7\/v1", namespace: "contact-form-7\/v1" } };
    </script>
    <script type="text/javascript">
        var woocommerce_params = { ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%" };
    </script>
    <script type="text/javascript">
        var wc_cart_fragments_params = {
            ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php",
            wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%",
            cart_hash_key: "wc_cart_hash_b930bcc34884a3f2cd0a7e5efdbd1f43",
            fragment_name: "wc_fragments_b930bcc34884a3f2cd0a7e5efdbd1f43",
            request_timeout: "5000",
        };
    </script>
    <script type="text/javascript">
        var sixtyninestudioJSObject = { sixtyninestudio_sticky_menu: "1" };
    </script>
    <script type="text/javascript" defer="" src="{{asset('assets/front/js')}}/autoptimize_af513b33625b472e83375336554cbcc1.js"></script>
    <script src="{{asset('assets/back/js')}}/sweetalert2@10.js"></script>
    <div id="toTop" style="display: block;"><i class="fa fa-angle-up"></i></div>

    @yield('js')
</body>

</html>
