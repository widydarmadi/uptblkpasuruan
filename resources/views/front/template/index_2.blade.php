<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="{{asset('assets/front')}}/images/favicon.ico" />
        <link type="text/css" media="all" href="{{asset('assets/front/css')}}/autoptimize_2c934750174b77ec2a6e92709722e2b5.css" rel="stylesheet" />
        <link type="text/css" media="only screen and (max-width: 768px)" href="{{asset('assets/front/css')}}/autoptimize_dcb2de333eec7ab4ae31385ed8d6a393.css" rel="stylesheet" />
        <link type="text/css" media="screen" href="{{asset('assets/front/css')}}/autoptimize_281a9c0efc121555d2dbded049025ed5.css" rel="stylesheet" />
        <title>UPT Balai Latihan Kerja - Pasuruan</title>
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <!--<script src="{{asset('assets/front/js')}}/jquery-1.9.1.min.js" ></script>-->
        <script src="https://code.jquery.com/jquery-2.0.0.js" integrity="sha256-iW43nTNM8LFseNmWKhV5FHFW1KcjVQMvzg3l9nPU4oc=" crossorigin="anonymous"></script>
  <!--      <script-->
  <!--src="https://code.jquery.com/jquery-3.7.1.js"-->
  <!--integrity="sha256-eKhayi8LEQwp4NKxN+CfCh+3qOVUtJn3QNZ0TciWLP4="-->
  <!--crossorigin="anonymous"></script>-->

        <script type="text/javascript">
            var zilla_likes = { ajaxurl: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php" };
        </script>
        <script type="text/javascript">
            var wc_add_to_cart_params = {
                ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php",
                wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%",
                i18n_view_cart: "View cart",
                cart_url: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/cart\/",
                is_cart: "",
                cart_redirect_after_add: "no",
            };
        </script>
        <noscript>
            <style>
                .woocommerce-product-gallery {
                    opacity: 1 !important;
                }
            </style>
        </noscript>
        <link rel="icon" href="{{asset('assets/front/images/favicon.ico')}}" sizes="32x32" />
        <noscript>
            <style type="text/css">
                .wpb_animate_when_almost_visible {
                    opacity: .5;
                }
            </style>
        </noscript>


        {{-- CUSTOM ADDITIONAL BOJES --}}
        {{-- <link href="https://fonts.googleapis.com/css2?family=Quicksand:wght@400;700&display=swap" rel="stylesheet"> --}}
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans:ital,wght@0,300;0,400;0,500;0,600;0,700;0,800;1,300;1,400;1,500;1,600;1,700;1,800" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css2?family=Roboto+Slab&display=swap" rel="stylesheet">
        <link href="{{asset('assets/front/css/custom.css')}}" rel="stylesheet">
        {{-- CUSTOM ADDITIONAL BOJES --}}
    </head>
    <body
        id="home"
        class="page-template page-template-page-templates page-template-template-home page-template-page-templatestemplate-home-php page page-id-3742 wp-embed-responsive woocommerce-no-js header-transparent has-header-search footer-multipage has-site-logo wpb-js-composer js-comp-ver-5.7 vc_responsive"
        data-spy="scroll"
        data-target=".navbar"
        data-offset="100"
    >
        {{-- <div id="preloader" style="background-color: #ffffff;">
            <div id="status"><div class="status-mes" style="background-image: url(http://trendytheme.net/demo2/wp/69/multipage/wp-content/themes/sixtyninestudio/images/preloader.gif);"></div></div>
        </div> --}}
        <div class="header-wrapper">
            <x-frontend.navigation />
        </div>

        @yield('content')

        <x-frontend.footer />
       
        <script type="text/javascript">
            var c = document.body.className;
            c = c.replace(/woocommerce-no-js/, "woocommerce-js");
            document.body.className = c;
        </script>


        <script type="text/javascript">
            window.jQuery || document.write('<script src="{{asset("assets/front/js/jquery-1.4.min.js")}}"><\/script>');

            const loading_text = 'Harap tunggu...';
            const data_saved = 'Data Berhasil Disimpan';
            const data_deleted = 'Data Berhasil Dihapus';
            const confirm_delete_text = 'Apakah Anda yakin ingin menghapus data ini ?';


            // $.ajaxSetup({
            //     headers: {
            //         'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            //     }
            // });

            function displayErrorSwal(msg){
                if(msg == null){
                swal.fire("Oops !", "Server sedang bermasalah. Harap menghubungi administrator kami", "error");
                }else{
                swal.fire("Oops !", msg, "error");
                }
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Submit');
            }


            function displayWarningSwal(msg){
                if(msg == null){
                    swal.fire("Oops !", "Harap memeriksa kembali inputan Anda", "warning");
                }else{
                    swal.fire("Oops !", msg, "warning");
                }
            }

            function reload_captcha(){
                $('#captcha').val('');
                $.ajax({
                    type: 'GET',
                    url: 'reload-captcha',
                    success: function (data) {
                        $(".captcha span").html(data.captcha);
                    }
                });
            }
        </script>


        <script type="text/javascript">
            function revslider_showDoubleJqueryError(sliderID) {
                var errorMessage = "Revolution Slider Error: You have some jquery.js library include that comes after the revolution files js include.";
                errorMessage += "<br> This includes make eliminates the revolution slider libraries, and make it not work.";
                errorMessage += "<br><br> To fix it you can:<br>&nbsp;&nbsp;&nbsp; 1. In the Slider Settings -> Troubleshooting set option:  <strong><b>Put JS Includes To Body</b></strong> option to true.";
                errorMessage += "<br>&nbsp;&nbsp;&nbsp; 2. Find the double jquery.js include and remove it.";
                errorMessage = "<span style='font-size:16px;color:#BC0C06;'>" + errorMessage + "</span>";
                jQuery(sliderID).show().html(errorMessage);
            }
        </script>
        <script type="text/javascript">
            var wpcf7 = { apiSettings: { root: "http:\/\/trendytheme.net\/demo2\/wp\/69\/multipage\/wp-json\/contact-form-7\/v1", namespace: "contact-form-7\/v1" } };
        </script>
        <script type="text/javascript">
            var woocommerce_params = { ajax_url: "\/demo2\/wp\/69\/multipage\/wp-admin\/admin-ajax.php", wc_ajax_url: "\/demo2\/wp\/69\/multipage\/?wc-ajax=%%endpoint%%" };
        </script>
        <script type="text/javascript">
            var sixtyninestudioJSObject = { sixtyninestudio_sticky_menu: "1" };
        </script>
        <script type="text/javascript" defer="" src="{{asset('assets/front/js')}}/autoptimize_af513b33625b472e83375336554cbcc1.js"></script>
        <!--<script type="text/javascript" defer src="{{asset('assets/front/js')}}/autoptimize_5bfdab92b9d18cd8d7e84b4db0e96edf.js"></script>-->
        <script type="text/javascript" defer src="{{asset('assets/front/js')}}/autoptimize_1.js"></script>
        <!--<script defer src="https://trendytheme.net/demo2/wp/69/multipage/wp-content/cache/autoptimize/js/autoptimize_60374ed180a28ca0f081084a2dbd0e0c.js"></script>-->
        <script src="{{asset('assets/back/js')}}/sweetalert2@10.js"></script>

        @yield('js')
    </body>
</html>
