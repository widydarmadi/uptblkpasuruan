Selamat, <strong>{{ $details['nama'] }}</strong>  !<br/>
Anda sudah berhasil melakukan pendaftaran sebagai alumni di UPT BLK Pasuruan.<br/>
<br/>
Untuk mengakses halaman alumni, silahkan memasukkan email dan password sebagai berikut :<br/>
USERNAME / EMAIL : {{ $details['email'] }}<br/>
PASSWORD : {{ $details['password'] }}
<br/><br/>
Untuk mengaktifkan akun ini, silahkan klik tautan di bawah ini.<br />
<a href="{{route('aktivasi_alumni', ['token' => \Crypt::encrypt($details['nik'])])}}">Aktivasi Akun Saya</a><br /><br />
Dimohon untuk tidak memberitahukan identitas apapun termasuk : Nomor Induk Kependudukan (NIK), username, maupun password kepada siapapun.
<br/>
Terima Kasih.