{{-- <br/>                
Selamat Anda sudah berhasil melakukan pendaftaran di UPT BLK Pasuruan, Tahapan selanjutnya adalah mengikuti Tes Seleksi yang akan diinformasikan selanjutnya pada halaman website UPT BLK Pasuruan atau di akun media sosial UPT BLK Pasuruan.
<br>
Untuk no registrasi tes seleksi anda adalah sebagai berikut :<br>
Simpan username dan password dibawah ini untuk kebutuhan tes seleksi.<br>
USERNAME : {{ $details['username'] }}<br>
PASSWORD : 123456<br>

Ketentuan saat tes seleksi  :<br>
1. Silahkan Hadir Tepat waktu<br>
2. Memakai Pakaian Bebas Rapi dan Bersepatu<br>
3. Membawa Handphone masing-masing dan pastikan sudah terhubung dengan internet / paket data<br>
       
<br/>        
Terimakasih. <br/> --}}


Selamat, <strong>{{ $details['nama'] }}</strong>  !<br/>
Anda sudah berhasil melakukan pendaftaran di UPT BLK Pasuruan, <br/>
Tahapan selanjutnya adalah mengikuti Tes Seleksi, jadwal akan diinformasikan selanjutnya pada halaman website UPT BLK Pasuruan atau di akun media sosial UPT BLK Pasuruan.<br/>
<br/>
Simpan username dan password dibawah ini untuk kebutuhan tes seleksi.<br/>
NAMA : {{ $details['nama'] }}<br/>
TANGGAL LAHIR : {{ $details['tgl_lahir'] }}<br/>
PELATIHAN : {{ $details['pelatihan'] }}<br/>
USERNAME : {{ $details['username'] }}<br/>
PASSWORD : 123456
<br/><br/>
Ketentuan saat tes seleksi :<br/>
1. Silahkan Hadir Tepat waktu.<br/>
2. Memakai Pakaian Bebas Rapi dan Bersepatu.<br/>
3. Membawa Handphone dan pastikan sudah terhubung dengan internet / paket data.
