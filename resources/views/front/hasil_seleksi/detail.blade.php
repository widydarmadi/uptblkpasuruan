@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'lowongan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Hasil Seleksi Calon Peserta</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Hasil Seleksi Calon Peserta</li>
            </ul>
        </div>
    </div>
</section>

<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div id="main" class="posts-content" role="main">
                            
                            @php
                                app()->setLocale('id');
                            @endphp
                            
                            <article id="post-3397" class="post-wrapper post-3397 post type-post status-publish format-standard hentry category-post-formats tag-standard">
                                <header class="featured-wrapper"></header>
                                <div class="blog-content">
                                    <div class="entry-header">
                                        <h2 class="entry-title">{{$item->nm_m_jadwal}}</h2>
                                        <ul class="entry-meta clearfix">
                                            <li><i class="fa fa-calendar"></i>{{\Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM YYYY')}}</li>
                                            
                                            <li>
                                                <span class="posted-in"> <i class="fa fa-folder-open"></i><a href="javascript:void(0)" rel="category tag">Pengumuman Seleksi</a> </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="entry-content">
                                        <p align="justify">
                                            Berikut kami sampaikan daftar nama calon peserta yang lulus tahap seleksi kompetensi dasar dan tes wawancara 
                                            pada program pelatihan <strong>"{{$item->nm_m_jadwal}}"</strong>
                                        </p>
                                        <p>Selanjutnya, diharapkan agar selalu mengikuti informasi terbaru terkait pelatihan melalui website UPT BLK Pasuruan atau media sosial <a target="_blank" href="https://instagram.com/uptblkpasuruan">instagram</a> kami. Terima Kasih</p>

                                        <table id="datatable16" class="table table-sm table-hover table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>No.</th>
                                                    <th>Nama Calon Peserta</th>
                                                    <th>No Register</th>
                                                    <th>Kab / Kota Domisili</th>
                                                    <th>Alamat</th>
                                                    <th>Jenis Kelamin</th>
                                                    {{-- <th>CBT</th>
                                                    <th>Wawancara</th> --}}
                                                    <th>Total Nilai</th>
                                                    <th>Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @php
                                                    $i = 1;      
                                                @endphp


                                                @foreach ($seleksi as $item)
                                                <tr>
                                                    <td>{{$i++}}</td>
                                                    <td>{{$item->pendaftar->nm_m_pendaftar}}</td>
                                                    <td>{{$item->pendaftar->no_register}}</td>
                                                    <td>{{$item->pendaftar->kota->nm_m_kota}}</td>
                                                    <td>{{$item->pendaftar->alamat_m_pendaftar}}</td>
                                                    <td>{{$item->pendaftar->jk_m_pendaftar}}</td>
                                                    {{-- <td>{{$item->total_nilai_cbt}}</td>
                                                    <td>{{$item->total_nilai_wawancara}}</td> --}}
                                                    <td>{{$item->total_nilai_keseluruhan}}</td>
                                                    <td>{{$item->status}}</td>
                                                </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                
                            </article>

                            <a href="{{route('hasil_seleksi')}}" class="btn btn-danger">Kembali</a>

                        </div>
                        
                        
                    </div>
                </div>
            </div>
            
            

          
            
        </div>
    </div>
</section>

@endsection

