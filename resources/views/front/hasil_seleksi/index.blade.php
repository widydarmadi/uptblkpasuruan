@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'lowongan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Hasil Seleksi Calon Peserta</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Hasil Seleksi Calon Peserta</li>
            </ul>
        </div>
    </div>
</section>

<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div id="main" class="posts-content" role="main">

                            <label for="">Pencarian Berdasarkan Program Pelatihan</label>
                            <div class="row" style="padding-bottom: 20px;">
                                <div class="col-md-6">
                                    <select name="id_m_program_pelatihan" id="id_m_program_pelatihan" class="form-control">
                                        <option value="">SEMUA PROGRAM PELATIHAN</option>
                                        @foreach ($prog as $item)
                                            <option {{(request()->get('program_pelatihan') == $item->id_m_program_pelatihan) ? 'selected' : null}} value="{{$item->id_m_program_pelatihan}}">{{$item->nm_m_program_pelatihan}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                {{-- <div class="col-md-6">
                                    <select name="id_m_jadwal" id="id_m_jadwal" class="form-control">
                                        <option value="">SEMUA JADWAL</option>
                                        
                                    </select>
                                </div> --}}
                                <div class="col-md-2">
                                    <button id="filter" name="filter" class="btn btn-success" style="padding: 9px;">FILTER</button>
                                </div>
                            </div>


                            
                            @php
                                app()->setLocale('id');
                            @endphp
                            @foreach ($data as $item)
                                
                            <article id="post-3397" class="post-wrapper post-3397 post type-post status-publish format-standard hentry category-post-formats tag-standard">
                                <header class="featured-wrapper"></header>
                                <div class="blog-content">
                                    <div class="entry-header">
                                        <h2 class="entry-title"><a href="{{route('hasil_seleksi_detail', ['id' => $item->id_m_jadwal])}}" rel="bookmark">{{$item->nm_m_jadwal}}</a></h2>
                                        <ul class="entry-meta clearfix">
                                            <li><i class="fa fa-calendar"></i><a href="{{route('hasil_seleksi_detail', ['id' => $item->id_m_jadwal])}}" rel="bookmark">{{\Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM YYYY')}}</a></li>
                                            
                                            <li>
                                                <span class="posted-in"> <i class="fa fa-folder-open"></i><a href="javascript:void(0)" rel="category tag">Pengumuman Seleksi</a> </span>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="entry-content">
                                        <p>
                                            Berikut kami sampaikan daftar nama calon peserta yang lulus tahap seleksi kompetensi dasar dan tes wawancara 
                                            pada program pelatihan <strong>"{{$item->nm_m_jadwal}}"</strong>
                                            <a href="{{route('hasil_seleksi_detail', ['id' => $item->id_m_jadwal])}}" class="more-link"><span class="readmore">SELENGKAPNYA</span></a>
                                        </p>
                                    </div>
                                </div>
                                <footer class="entry-footer clearfix">
                                    <ul class="entry-meta">
                                        <li>
                                            <span class="post-comments"> &nbsp; </span>
                                        </li>
                                    </ul>
                                </footer>
                            </article>

                            @endforeach

                            {{$data->links()}}


                        </div>
                        
                        
                    </div>
                </div>
            </div>
            
            

            
        </div>
    </div>
</section>

@endsection

@section('js')
    <script>
                
        $('#id_m_program_pelatihan').change(function(){
            var id = $(this).val();
            $('#id_m_jadwal').html('');
            if(id != null){
                $.ajax({
                    url:"{{ route("load_jadwal_available") }}",
                    method:"post",
                    dataType: 'json',
                    data: {
                        id: id
                    },
                    success:function(data)
                    {
                    if(data.status == true){
                        $('#id_m_jadwal').html(data.html);
                    }
                    },
                    error: function(data){
                    displayErrorSwal();
                    }
                });
            }
        })
        
    $('#filter').click(function(){    
        var id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
        var id_m_jadwal = $('#id_m_jadwal').val();
        location.href = '{{route('hasil_seleksi')}}?program_pelatihan=' + id_m_program_pelatihan;
        // location.href = '{{route('hasil_seleksi')}}?program_pelatihan=' + id_m_program_pelatihan + '&jadwal=' + id_m_jadwal;
    })
    </script>
@endsection