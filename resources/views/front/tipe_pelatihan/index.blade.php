@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'tipe-pelatihan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Tipe Pelatihan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Tipe Pelatihan</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                               @php
                                   $x = 1;
                               @endphp
                                
                               @foreach ($tipe_pelatihan as $item)

                               @php
                                   if($x % 2 ==0){
                                    $odd_1 = 4;
                                    $odd_2 = 8;
                                   }else{
                                    $odd_1 = 8;
                                    $odd_2 = 4;
                                   }
                               @endphp
                                <div class="row">
                                    <div class="col-md-{{$odd_1}}">

                                        @if($odd_1 == 4)

                                            @if($item->gambar_m_tipe_pelatihan)
                                            <img src="{{url('storage').'/'.$item->gambar_m_tipe_pelatihan}}" style="width:100%" />
                                            @else
                                            <img src="{{url('assets/front/images/noimage.png')}}" style="width:100%" />
                                            @endif

                                        @else

                                            <h1 class="theme-color">{{$item->nm_m_tipe_pelatihan}}</h1>
                                            <p>{!!$item->deskripsi_m_tipe_pelatihan!!}</p>     
                                            <br/>
                                            @if($item->pdf_m_tipe_pelatihan)
                                            <a class="btn btn-primary" href="{{url('storage').'/'.$item->pdf_m_tipe_pelatihan}}" target="_blank">Lihat Brosur</a>
                                            @endif
                                            <br/>

                                        @endif


                                    </div>
                                    <div class="col-md-{{$odd_2}}">
                                        @if($odd_2 == 4)

                                            @if($item->gambar_m_tipe_pelatihan)
                                            <img src="{{url('storage').'/'.$item->gambar_m_tipe_pelatihan}}" style="width:100%" />
                                            @else
                                            <img src="{{url('assets/front/images/noimage.png')}}" style="width:100%" />
                                            @endif

                                        @else

                                            <h1 class="theme-color">{{$item->nm_m_tipe_pelatihan}}</h1>
                                            <p>{!!$item->deskripsi_m_tipe_pelatihan!!}</p>     
                                            <br/>
                                            @if($item->pdf_m_tipe_pelatihan)
                                            <a class="btn btn-primary" href="{{asset('storage').'/'.$item->pdf_m_tipe_pelatihan}}" target="_blank">Lihat Brosur</a>
                                            @endif
                                            <br/>

                                        @endif
                                    </div>
                                </div>

                                @php
                                    $x++;
                                @endphp
                                <br/>
                                <br/>
                                <br/>
                               @endforeach

                               
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')

@endsection