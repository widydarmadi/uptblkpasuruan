@extends('front.template.index_detail')

@section('content')
<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'galeri')->first();
@endphp

<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                    <div class="row">
                                        <div class="col-md-12">

                                            <div align="center" class="text-center">
                                                <h1 class="theme-color">{{$album->judul_m_album}}</h1>
                                                
                                                <div class="sub-title" style="">
                                                    Album dibuat pada : 
                                                    
                                                    {{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$album->created_at)->isoFormat('D MMMM YYYY')}}
                                                    <h4>{!!$album->deskripsi_m_album!!}</h4>
                                                </div>
                                                
                                            </div>

                                            
                                        </div>
                                    </div>
                                    <div class="row"  id="profile-grid">
                                        
                                        <div class="tz-gallery">
                                            
                                            <div class="row">
                                                @foreach ($galeri as $item)
                                    
                                                <div class="col-sm-12 col-md-4">
                                                    <a class="lightbox" href="{{asset('storage'.'/'.$item->photo)}}">
                                                        <img style="height: 235px;" src="{{asset('storage'.'/'.$item->photo)}}" alt="{{$item->caption}}">
                                                    </a>
                                                </div>

                                                @endforeach
                                                
                                            </div>
                                    
                                        </div>

                                        

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div align="center">
                                                    <a href="{{route('galeri')}}" class="btn btn-danger">Kembali</a>
                                                </div>

                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                                <br>
                                            </div>
                                        </div>
                                    </div>
                                
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>



@endsection

@section('js')
<script src="https://cdnjs.cloudflare.com/ajax/libs/baguettebox.js/1.8.1/baguetteBox.min.js"></script>
<script>
    baguetteBox.run('.tz-gallery');
</script>
@endsection