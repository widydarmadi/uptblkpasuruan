@extends('front.template.index_detail')

@section('content')

@php
// dd($galeri);
    $bg = \App\Models\M_bg::where('slug_m_bg', 'galeri')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Galeri Foto</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Galeri Foto</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                


                                @php
                                    app()->setLocale('id');
                                @endphp

                                @forelse ($galeri as $item)

                                    <div class="col-md-4" style="margin-bottom: 30px;">

                                        <div class="box-lowongan">
                                            <a href="{{url('galeri/detail/'.$item->id_m_album)}}">
                                            @if(isset($item->cover->photo))
                                            <img style="height: 250px;" src="{{asset('storage'.'/'.$item->cover->photo)}}" alt="{{$item->cover->caption}}" width="100%">
                                            @else
                                            <img style="height: 250px;" src="{{asset('assets/front/images/noimage.png')}}" alt="{{$item->cover->caption}}" width="100%">
                                            @endif
                                            </a>
                                            <br>
                                            <br>
                                            <a href="{{url('galeri/detail/'.$item->id_m_album)}}">
                                                <h3 class="text-danger">{{\Str::ucfirst($item->judul_m_album)}}</h3>
                                            </a>
                                            <p>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s',$item->created_at)->isoFormat('D MMMM Y')}}</p>

                                        </div>
                                    </div>

                                @empty
                                <div class="col-md-12">
                                    <div class="alert-danger">
                                        <div class="alert-body text-center">
                                            <br>
                                            <p>Belum ada album foto yang ditambahkan</p>
                                            <br>
                                        </div>
                                    </div>
                                    <br>
                                    <br>
                                </div>
                                @endforelse
                                


                                




                            </div>
                        </div>
                        
                        
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col">
                    {{-- {{$galeri->links()}} --}}
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection