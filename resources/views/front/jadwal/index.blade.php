@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'jadwal-pelatihan')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Jadwal Pelatihan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Jadwal Pelatihan</li>
            </ul>
        </div>
    </div>
</section>

<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">

                                <h2>Jadwal Pelatihan Yang Dibuka</h2>

                                {{-- <h2 class="theme-color">Tipe Pelatihan : Institusioinal</h2> --}}
                                <div class="row" style="padding-bottom: 20px;">
                                    <div class="col-md-3">
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="">-- SEMUA TAHUN --</option>
                                            @foreach ($tahun as $item)
                                                <option {{(request()->get('tahun') == $item->tahun) ? 'selected' : null}} value="{{$item->tahun}}">{{$item->tahun}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="anggaran" id="anggaran" class="form-control">
                                            <option value="">-- SEMUA SUMBER ANGGARAN --</option>
                                            @foreach ($anggaran as $item)
                                                <option {{(request()->get('anggaran') == $item->id_m_sumber_dana) ? 'selected' : null}} value="{{$item->id_m_sumber_dana}}">{{$item->nm_m_sumber_dana}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-3">
                                        <select name="tipe" id="tipe" class="form-control">
                                            <option value="">-- SEMUA TIPE PELATIHAN --</option>
                                            @foreach ($tipe as $item)
                                                <option {{(request()->get('tipe') == $item->id_m_tipe_pelatihan) ? 'selected' : null}} value="{{$item->id_m_tipe_pelatihan}}">{{$item->nm_m_tipe_pelatihan}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <button id="filter" name="filter" class="btn btn-success" style="padding: 9px;">FILTER</button>
                                        <button id="reset" onclick="location.href='{{route('jadwal')}}'" name="reset" class="btn btn-primary" style="padding: 9px;">RESET</button>
                                    </div>
                                </div>
                               
                               @php
                                   $x = 1;
                               @endphp

                               <table class="table table-striped table-bordered table-hover">
                                   <thead>
                                       <tr>
                                           <th>No.</th>
                                           <th>Nama Pelatihan</th>
                                           <th>Tipe</th>
                                           {{-- <th>Gelombang</th> --}}
                                           <th>Paket</th>
                                           <th>Jumlah</th>
                                           <th>Hari</th>
                                           <th>Tgl Mulai</th>
                                           <th>Status</th>
                                           {{-- <th>Tgl Selesai</th> --}}
                                           <th>Daftar</th>
                                       </tr>
                                   </thead>

                                   <tbody>
                                       @php
                                           app()->setLocale('id');
                                       @endphp
                                        @foreach ($jadwal as $item)
                                       <tr>
                                           <td>{{$x++}}</td>
                                           <td>{{$item->nm_m_jadwal}}</td>
                                           <td>{{($item->program_pelatihan and $item->program_pelatihan->tipe_pelatihan) ? $item->program_pelatihan->tipe_pelatihan->nm_m_tipe_pelatihan : null}}</td>
                                           {{-- <td>{{$item->program_pelatihan->nm_m_program_pelatihan}}</td> --}}
                                           {{-- <td>{{$item->gelombang->nm_m_gelombang}}</td> --}}
                                           <td>{{$item->jml_paket}}</td>
                                           <td>{{$item->kuota}}</td>
                                           <td>{{$item->hari}}</td>
                                           <td>{{\Carbon\Carbon::parse($item->tgl_mulai)->isoFormat('dddd, D MMMM YYYY')}}</td>
                                           <td>
                                            @if($item->status == 'OPEN')
                                            <span class="badge alert-success">{{$item->status}}</span>
                                            @elseif($item->status == 'CLOSE')
                                            <span class="badge alert-danger">{{$item->status}}</span>
                                            @endif
                                           </td>
                                           {{-- <td>{{\Carbon\Carbon::parse($item->tgl_selesai)->isoFormat('dddd, D MMMM YYYY')}}</td> --}}
                                           <td>
                                            @if($item->program_pelatihan)
                                                @if($item->program_pelatihan->id_m_tipe_pelatihan == 1)
                                                <a style="padding: 3px 8px;" class="btn btn-success btn-sm {{($item->status == 'OPEN') ? '' : 'disabled'}}" href="{{route('pendaftaran', ['cat' => 'PBK', 'id_m_jadwal' => $item->id_m_jadwal, 'grup_id' => $item->grup_id])}}">Daftar</a>
                                                @elseif($item->program_pelatihan->id_m_tipe_pelatihan == 2)
                                                <a style="padding: 3px 8px;" class="btn btn-success btn-sm" href="{{route('pendaftaran_mtu', ['cat' => 'MTU', 'id_m_jadwal' => $item->id_m_jadwal])}}">Daftar</a>
                                                @elseif($item->program_pelatihan->id_m_tipe_pelatihan == 3)
                                                <a style="padding: 3px 8px;" class="btn btn-success btn-sm" href="{{route('pendaftaran_pihak_ketiga', ['cat' => 'SWA', 'id_m_jadwal' => $item->id_m_jadwal])}}">Daftar</a>
                                                @endif

                                            @endif
                                           </td>
                                       </tr>
                                        @endforeach

                                   </tbody>
                               </table>
                               
                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>

$('#filter').click(function(){    
    var tahun = $('#tahun').val();
    var anggaran = $('#anggaran').val();
    var tipe = $('#tipe').val();
    location.href = '{{route('jadwal')}}?tahun=' + tahun + '&anggaran=' + anggaran + '&tipe=' + tipe;
    // location.href = '{{route('hasil_seleksi')}}?program_pelatihan=' + id_m_program_pelatihan + '&jadwal=' + id_m_jadwal;
})
</script>
@endsection