@extends('front.template.index_detail')

@section('content')

@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'katalog-alumni')->first();
    // dd($bg);
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Katalog Alumni</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Katalog Alumni</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper vc_custom_1461531052706" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                {{-- <div class="vc_column-inner"> --}}
                    <div class="wpb_wrapper">
                        <div class="sction-title-wrapper text-left vc_custom_1461534674457">
                            {{-- <h2 style="" class="section-title theme-color">Katalog Alumni</h2> --}}
                            
                        </div>
                        <div class="portfolio-container">
                            <div class="row">
                                <div class="col-md-4">
                                    <select name="kategori" class="form-control" id="kategori">
                                        <option value="">Semua Kategori</option>
                                        @foreach ($kategori as $item)
                                        <option value="{{$item->id_m_kategori_katalog_alumni}}" {{($item->id_m_kategori_katalog_alumni == request()->get('kategori')) ? 'selected' : ''}}>{{$item->nm_m_kategori_katalog_alumni}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <input type="text" value="{{request()->get('search')}}" name="search" id="search" class="form-control" value="" />
                                </div>
                                <div class="col-md-3">
                                    <button class="btn btn-primary" style="padding: 9px 20px;" id="filter">Cari</button>
                                    <button class="btn btn-danger" style="padding: 9px 20px;" onclick="location.href='{{route('katalog_alumni')}}'">Atur Ulang</button>
                                </div>

                            </div>
                            <div class="tt-filter-wrap filter-transparent text-left default-color">
                                <ul class="tt-filter list-inline">
                                    {{-- <li><button class="active" data-group="all">Semua</button></li> --}}
                                    @php
                                        $list_alumni = \App\Models\M_kategori_katalog_alumni::where('aktif_m_kategori_katalog_alumni', '1')->orderByDesc('created_at')->get();
                                    @endphp
                                    @foreach ($list_alumni as $item)
                                        {{-- <li><button data-group="{{$item->id_m_kategori_katalog_alumni}}">{{$item->nm_m_kategori_katalog_alumni}}</button></li> --}}
                                    @endforeach
                                </ul>
                            </div>
                            <div class="row">
                                <div class="tt-grid shuffle" style="position: relative; height: 879px; transition: height 250ms ease-out 0s;">
                                    @php
                                        $kategori = request()->get('kategori');
                                        $search = request()->get('search');
                                       
                                        $list_alumni = \App\Models\Join_katalog::when(request()->filled('kategori'),function($q) use ($kategori){
                                                            $q->where('id_m_kategori_katalog_alumni', $kategori);
                                                        })
                                                        ->when(isset($search), function($q) use ($search){
                                                            $q->where('nm_m_katalog_alumni', 'like', '%'.$search.'%');
                                                        })->orderByDesc('id')->paginate(9);
                                    @endphp
                                    @foreach ($list_alumni as $item)
                                    <div
                                        class="tt-item portfolio-item col-md-4 col-sm-6 col-xs-12 shuffle-item filtered"
                                        data-groups='["{{$item->id_m_kategori_katalog_alumni}}"]'
                                        style="position: absolute; top: 0px; left: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                    >
                                        <div class="portfolio hover-two">
                                            <div class="tt-overlay-theme-color"></div>
                                            <img
                                                src="{{asset('storage'.'/'.$item->photo)}}"
                                                class="img-responsive wp-post-image"
                                                width="800"
                                                height="600"
                                            />
                                            <div class="portfolio-info">
                                                <h3 class="project-title"><a target="_blank" href="https://instagram.com/{{str_replace('@','',$item->instagram)}}">{{$item->nm_m_katalog_alumni}}</a></h3>
                                                <div class="project-meta"><a target="_blank" href="https://instagram.com/{{str_replace('@','',$item->instagram)}}" class="btn btn-outline">{{$item->instagram}}</a></div>
                                            </div>
                                            <ul class="portfolio-external-link">
                                                <li>
                                                    <a class="tt-lightbox" href="{{asset('storage'.'/'.$item->photo)}}"><i class="fa fa-search"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>

                            </div>
                            {{$list_alumni->appends($_GET)->links()}}
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
</section>
@endsection


@section('js')
<script>

$('#filter').click(function(){    
    var kategori = $('#kategori').val();
    var search = $('#search').val();
    location.href = '{{route('katalog_alumni')}}?kategori=' + kategori + '&search=' + search;
})
</script>
@endsection