@extends('front.template.index')

@section('content')

<!--SLIDER-->

{{-- <div id="myCarousel" class="carousel slide">
    @php
        $list_slider = \App\Models\M_slider::where('aktif_m_slider', '1')->orderByDesc('id_m_slider')->get();
        $i_slider = 0;
        $j_slider = 0;
    @endphp
    <ol class="carousel-indicators">
        @foreach ($list_slider as $item)
        <li data-target="#myCarousel" data-slide-to="{{$i_slider}}" class="{{($i_slider == 0) ? 'active' : ''}}" contenteditable="false"></li>
        @php
            $i_slider++;
        @endphp
        @endforeach
    </ol>
    <div class="carousel-inner">
        
        @foreach ($list_slider as $item)
        <div class="item {{($j_slider == 0) ? 'active' : ''}}">
            <img src="{{asset('storage'.'/'.$item->photo)}}" alt="" class="">
          
        </div>
        @php
            $j_slider++;
        @endphp
        @endforeach
        
        
    </div>    

    <a class="left carousel-control" href="#myCarousel" data-slide="prev">
        <span class="glyphicon glyphicon-chevron-left"></span>
    </a>

    <a class="right carousel-control" href="#myCarousel" data-slide="next">
        <span class="glyphicon glyphicon-chevron-right"></span>
    </a>


</div> --}}

<!--SLIDER-->


<section>
    <div id="tt-home-carousel" class="carousel slide carousel-fade creative-carousel trendy-slider control-one"
        data-ride="carousel" data-interval="5000">

        <div class="carousel-inner">
            @php
                $list_slider = \App\Models\M_slider::where('aktif_m_slider', '1')->orderByDesc('id_m_slider')->get();
                $i_slider = 0;
                $j_slider = 0;
            @endphp
            @foreach ($list_slider as $item)
            <div class="item {{($j_slider == 0) ? 'active' : ''}}">
                <img src="{{asset('storage'.'/'.$item->photo)}}" alt="First slide" class="img-responsive">
                <div class="carousel-caption creative-caption">
                    <div class="row">
                        <div class="col-lg-8">
                            <h1 class="animated fadeInDown delay-1"><span>We are simply creative</span></h1>
                            <p class="animated fadeInDown delay-3">A simple clean unique html template for build brand
                            </p>
                            <a class="btn learnmore-btn animated fadeInUp delay-4" href="#">View Works</a>
                        </div>
                        <div class="col-lg-4 visible-lg">
                            <div class="animated fadeInRight delay-3">
                                <img src="{{asset('storage'.'/'.$item->photo)}}">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @php
                $j_slider++;
            @endphp
            @endforeach
            
        </div>

        <a class="left carousel-control" href="#tt-home-carousel" role="button" data-slide="prev">
            <span class="fa fa-angle-left"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#tt-home-carousel" role="button" data-slide="next">
            <span class="fa fa-angle-right"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</section>



<section class="vc_row section-wrapper">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_containers vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <h2 style="color: #E60067" class="vc_custom_heading vc_custom_1459914637413">Selamat Datang di</h2>
                        <div style="font-size: 45px; line-height: 36px; text-align: left; font-weight: 700; font-style: normal;" class="vc_custom_heading theme-color vc_custom_1460905731442">
                            UPT Balai Latihan Kerja Pasuruan
                        </div>
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                <p></p>
                                <p></p>
                                <p>
                                    <strong>UPT Balai Latihan Kerja Pasuruan merupakan salah satu lembaga pelatihan milik pemerintah yang berfokus pada pengembangan kompetensi calon tenaga kerja melalui program-program pelatihan yang disusun berdasarkan kebutuhan dunia industri.
Sehingga, setelah mengikuti pelatihan, para alumni diharapkan mampu bersaing secara global.</strong>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_containers vc_col-sm-5 vc_col-sm-offset-1">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460542917437">
                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                    <img
                                        src="{{asset('assets/front/images/meeting.jpg')}}"
                                        class="vc_single_image-img attachment-full"
                                        sizes="(max-width: 327px) 100vw, 327px"
                                    />
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="vc_row section-wrapper vc_custom_1461775198634 vc_row-has-fill">
    <div class="container">
        <div class="row">
            @php
                $list_kejuruan = \App\Models\M_kejuruan::where('aktif_m_kejuruan', '1')->get();
            @endphp

            {{-- @foreach ($list_kejuruan as $item)
            <div class="wpb_column vc_column_container vc_col-sm-4">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="process-box text-center flat-icon" style="">
                            <i class="flaticon-cup7" style=""></i>
                            <h3 style="">{{$item->nm_m_kejuruan}}</h3>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach --}}
            
            
        </div>
    </div>
</section>
<section class="vc_row section-wrapper vc_custom_1468391659580">
    <div class="container-fullwidth">
        <div class="row">
            <div class="wpb_column vc_column_containers vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="sction-title-wrapper text-center vc_custom_1460527860550">
                            <h2 style="" class="section-title theme-color">Katalog Alumni</h2>
                            {{-- <div class="sub-title" style="">
                                <p>
                                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur<br />
                                    magni dolores eos qui ratione voluptatem sequi nesciunt.
                                </p>
                            </div> --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="vc_row section-wrapper vc_custom_1468391724027">
    <div class="container-fullwidth">
        <div class="row">
            <div class="wpb_column vc_column_containers vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="portfolio-container">
                            <div class="tt-filter-wrap filter-rounded text-center default-color">
                                <ul class="tt-filter list-inline">
                                    <li><button class="active" data-group="all">Semua</button></li>
                                    @php
                                        $list_alumni = \App\Models\M_kategori_katalog_alumni::take(15)->orderByDesc('created_at')->get();
                                    @endphp
                                    @foreach ($list_alumni as $item)
                                        <li><button data-group="{{$item->id_m_kategori_katalog_alumni}}">{{$item->nm_m_kategori_katalog_alumni}}</button></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="row">
                                <div class="tt-grid shuffle" style="position: relative; height: 426px; transition: height 250ms ease-out 0s;">
                                    @php
                                        $list_alumni = \App\Models\M_katalog_alumni::where('aktif_m_katalog_alumni', '1')->orderByDesc('created_at')->with('m_kategori_katalog_alumni')->take(15)->get();
                                    @endphp
                                    @foreach ($list_alumni as $item)
                                    <div
                                        class="tt-item portfolio-item col-md-5 no-padding col-sm-6 col-xs-12 shuffle-item filtered"
                                        data-groups='["{{$item->id_m_kategori_katalog_alumni}}"]'
                                        style="position: absolute; top: 0px; left: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                    >
                                        <div class="portfolio hover-one">
                                            <div class="tt-overlay-theme-color"></div>
                                            <img
                                                src="{{asset('storage'.'/'.$item->photo)}}"
                                                class="img-responsive wp-post-image"
                                                alt="{{$item->nm_m_kategori_alumni}}"
                                                width="800"
                                                height="600"
                                            />
                                            <div class="portfolio-info">
                                                <h3 class="project-title"><a href="https://instagram.com/{{str_replace('@','',$item->instagram)}}">{{$item->nm_m_katalog_alumni}}</a></h3>
                                                <div class="project-meta"><a href="https://instagram.com/{{str_replace('@','',$item->instagram)}}" class="btn btn-outline">{{$item->instagram}}</a></div>
                                            </div>
                                        </div>
                                    </div>
                                    @endforeach

                                    
                                </div>
                                <p></p>
                                <div class="text-center">
                                    <a href="#" class="btn btn-outline-success btn-lg">Lihat Lebih Banyak</a>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="vc_row section-wrapper">
    <div class="container-fullwidth">
        <div class="row">
            <div class="wpb_column vc_column_containers vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="case-study-wrapper theme-bg" style="">
                            <div class="case-study-left">
                                <h2>Kejuruan Pelatihan</h2>
                                <div class="case-study-content"><p>UPT Balai Latihan Kerja Pasuruan merupakan salah satu lembaga pelatihan milik pemerintah yang berfokus pada pengembangan kompetensi calon tenaga kerja melalui program-program pelatihan yang disusun berdasarkan kebutuhan dunia industri. 
                                <br>Sehingga, setelah mengikuti pelatihan, para alumni diharapkan mampu bersaing secara global.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_containers vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="case-study-wrapper dark-bg" style="">
                            <div class="case-study-right">
                                <h2>Mitra BLK</h2>
                                <div class="case-study-content"><p>UPT Balai Latihan Kerja Pasuruan berkomitmen untuk berkontribusi dalam penyerapan calon tenaga kerja yang telah mengikuti pelatihan. <br>Hal ini dilakukan dengan cara menjalin kerja sama dengan beberapa perusahaan di wilayah Kabupaten Pasuruan dan sekitarnya.</p></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper">
    <div class="container-fullwidth">
        <div class="row">
            <div class="wpb_column vc_column_containers vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="case-study-wrapper" style="">
                            <div class="case-study-left">
                                <h2 class="text-center">Mitra</h2>


                                @php
                                    $list_mitra = \App\Models\M_mitra::where('aktif_m_mitra', '1')->take(12)->get();
                                @endphp
                                <div class="testimonial-carousel-wrapper">
                                    <div class="client-slider-v4 clearfix">
                                        <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                            
                                        </div>
                                        
                                        <ol class="flex-control-nav flex-control-thumbs">
                                            @foreach ($list_mitra as $item)
                                            <li><img title="{{$item->nm_m_mitra}}" src="{{asset('storage/'.$item->logo)}}" class="" draggable="false" /></li>
                                            @endforeach
                                        </ol>
                                        <ul class="flex-direction-nav">
                                            <li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li>
                                            <li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
                                        </ul>
                                    </div>
                                </div>

                                    
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>





<section class="vc_row section-wrapper vc_custom_1461778734831 vc_row-has-fill" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element vc_custom_1459910992473">
                          <p></p>
                          <p></p>
                          <p></p>
                          <h2 class="text-center text-info">Form Pengaduan</h2>
                          <div class="alert alert-info">
                              <p><strong>Kami membutuhkan masukan dan saran Anda demi kemajuan pelayanan UPT Balai Latihan Kerja Pasuruan melalui formulir dibawah ini.</strong></p>
                              <p><strong>Kerahasiaan data pengirim akan kami jaga.</strong></p>
                          </div>
                            <div class="wpb_wrapper">
                                <form method="post" id="form">
                                    <div class="form-group">
                                      <label class="control-label" for="nm_m_pengaduan">Nama Lengkap</label> 
                                      <input id="nm_m_pengaduan" name="nm_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="email_m_pengaduan" class="control-label">Email yang aktif</label> 
                                      <input id="email_m_pengaduan" name="email_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                      <label for="subyek_m_pengaduan" class="control-label">Subyek (Topik Pengaduan)</label> 
                                      <input id="subyek_m_pengaduan" name="subyek_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                      <label for="isi_m_pengaduan" class="control-label">Isi Pengaduan</label> 
                                      <textarea style="height: 150px!important;" rows="4" class="form-control" name="isi_m_pengaduan" id="isi_m_pengaduan"></textarea>
                                    </div>
                                    <div class="form-group">
                                      <label for="isi_m_pengaduan" class="control-label">Masukkan ulang kode acak yang tertera di bawah ini</label> 
                                      <br>
                                      <div class="captcha">
                                      <span>{!! captcha_img() !!}</span>
                                        <button onclick="reload_captcha();" type="button" class="btn btn-info" style="padding: 7px 15px;" class="reload" id="reload">
                                            &#x21bb;
                                        </button>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <button type="submit" id="submitform" class="btn btn-success"><span>Kirim Pengaduan</span></button>
                                    </div>
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection

@section('js')
<script>
$('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {
    if ($(window).innerWidth() < winWidth) {
        $('.carousel-inner>.item>img').css({
            'min-width': winWidth, 'width': winWidth
        });
    }
    else {
        winWidth = $(window).innerWidth();
        $('.carousel-inner>.item>img').css({
            'min-width': '', 'width': ''
        });
    }
});

</script>

<script>
    $("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("pengaduan_post") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){
                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Kirim Formulir');
                        $("form").each(function() { this.reset() });
                        swal.fire({
                            title: "Success",
                            text: data.message,
                            icon: "success"
                        }).then(function() {
                            location.href = data.redirect;
                        });
                    }else{
                        displayErrorSwal(data.message);
                        reload_captcha();
                    }

                }else{
                    displayWarningSwal();
                    reload_captcha();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Kirim Formulir');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
                reload_captcha();
            }
        });
    });


    
</script>
@endsection