@extends('front.template.index')

@section('content')
<section class="vc_row section-wrapper">
    <x-frontend.slider />
</section>

<x-frontend.pendaftaran />


@foreach ($m_welcome as $item)
    
<section class="vc_row section-wrapper" style="margin-top: 50px">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_containers vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper" style="padding: 15px;">
                        <h2 class="vc_custom_heading vc_custom_1459914637413">{{$item->judul_primary_m_welcome}}</h2>
                        <h1 class="theme-color">
                            {{$item->judul_secondary_m_welcome}}
                        </h1>
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper text-justify">
                                <p></p>
                                <p></p>
                                <p>
                                   {!!$item->teks_m_welcome!!}
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_containers vc_col-sm-5 vc_col-sm-offset-1">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_center vc_custom_1460542917437">
                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                    @if ($item->video_m_welcome)

                                    @php
                                    $url = $item->video_m_welcome;
                                    parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                                    $video_id = $my_array_of_vars['v'];
                                    @endphp
                                    <iframe width="560" height="315" src="https://www.youtube.com/embed/{{$video_id}}" frameborder="0" gesture="media" allow="encrypted-media" allowfullscreen></iframe>
                                    @else
                                    <img
                                        src="{{asset('assets/front/images/meeting.jpg')}}"
                                        class="vc_single_image-img attachment-full"
                                        sizes="(max-width: 327px) 100vw, 327px"
                                    />
                                        
                                    @endif
                                </div>
                            </figure>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endforeach

<x-frontend.reformasi-birokrasi />
<p></p>
<p></p>
<br>
<x-frontend.katalog-alumni />
<x-frontend.pendaftaran />
<p></p>
<br>
<x-frontend.kejuruan-list />

<x-frontend.testimonial />

<x-frontend.mitra />

<x-frontend.manual />

<x-frontend.faq />

<x-frontend.pengaduan />



@endsection

@section('js')

<script>
// var winWidth = $(window).innerWidth();
// $(window).resize(function () {
//     if ($(window).innerWidth() < winWidth) {
//         $('.carousel-inner>.item>img').css({
//             'min-width': winWidth, 'width': winWidth
//         });
//     }
//     else {
//         winWidth = $(window).innerWidth();
//         $('.carousel-inner>.item>img').css({
//             'min-width': '', 'width': ''
//         });
//     }
// });


$("#form").submit(function(){
    $(".text-danger").remove();
    event.preventDefault();
    var data = new FormData($('#form')[0]);
    $("#submitform").attr('disabled', true);
    $("#submitform span").text(loading_text);

    $.ajax({
        url:"{{ route("pengaduan_post") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: data,
        processData: false,
        contentType: false,
        success:function(data)
        {
            if($.isEmptyObject(data.error)){
                if(data.status == true){
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Kirim Formulir');
                    $("form").each(function() { this.reset() });
                    swal.fire({
                        title: "Success",
                        text: data.message,
                        icon: "success"
                    }).then(function() {
                        location.href = data.redirect;
                    });
                }else{
                    displayErrorSwal(data.message);
                    reload_captcha();
                }

            }else{
                displayWarningSwal();
                reload_captcha();
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Kirim Formulir');
                $.each(data.error, function(key, value) {
                    var element = $("#" + key);
                    element.closest("div.form-control")
                    .removeClass("text-danger")
                    .addClass(value.length > 0 ? "text-danger" : "")
                    .find("#error_" + key).remove();
                    element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                });
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
            reload_captcha();
        }
    });
});


    
</script>
@endsection