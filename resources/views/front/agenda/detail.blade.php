@extends('front.template.index_detail')

@section('content')
<style>
.post-meta {
    position: absolute;
    bottom: 5px;
    width: 100%;
    padding: 0 0 0 10px;
    margin: 0;
    list-style: none;
    z-index: 100;
    color: #fff;
}
</style>
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'agenda')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Agenda</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Agenda</li>
            </ul>
        </div>
    </div>
</section>







<div class="blog-wrapper blog-grid content-wrapper">
    <div class="container">
        <div class="row">
            <div id="main" class="posts-content" role="main" style="">
                <div class="col-md-12 " >
                    <h3 class="theme-color">{{$agenda->judul_m_agenda}}</h3>
                    <strong>Tanggal Pelaksanaan : {{\Carbon\Carbon::parse($agenda->tgl_mulai)->isoFormat('D MMMM YYYY')}}
                        @if($agenda->tgl_mulai != $agenda->tgl_selesai)
                        s/d {{\Carbon\Carbon::parse($agenda->tgl_selesai)->isoFormat('D MMMM YYYY')}}
                        @endif
                        </strong><br />
                    <img
                        width="70%"
                        src="{{isset($agenda->photo) ? asset('storage'.'/'.$agenda->photo) : asset('assets/front/images/noimage.png')}}"
                    />
                    {!!$agenda->isi_m_agenda!!}

                    <a href="{{route('agenda')}}" class="btn btn-danger">Kembali</a>
                    
                </div>
                <br />
                    <br />
                    <br />
            </div>
        </div>
    </div>
</div>



@endsection

@section('js')
<script>

</script>
@endsection