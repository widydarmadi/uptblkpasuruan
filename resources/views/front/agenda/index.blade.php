@extends('front.template.index_detail')

@section('content')
<style>
.post-meta {
    position: absolute;
    bottom: 5px;
    width: 100%;
    padding: 0 0 0 10px;
    margin: 0;
    list-style: none;
    z-index: 100;
    color: #fff;
}
</style>
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'agenda')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Agenda</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Agenda</li>
            </ul>
        </div>
    </div>
</section>







<div class="blog-wrapper blog-grid content-wrapper">
    <div class="container">
        <div class="row">
            <div id="main" class="posts-content masonry-wrap clearfix masonry" role="main" style="position: relative; height: 1470.12px;">
                
                @foreach ($agenda as $item)
                <div class="col-md-4 col-sm-6 col-xs-12 masonry-column masonry-brick" style="position: absolute; left: 0px; top: 0px;">
                    <article id="post-3414" class="post-wrapper post-3414 post type-post status-publish format-standard has-post-thumbnail hentry category-standard">
                        <header class="featured-wrapper">
                            <div class="post-thumbnail">
                                <img
                                    width="100%"
                                    style="height: 270px"
                                    src="{{isset($item->photo) ? asset('storage'.'/'.$item->photo) : asset('assets/front/images/noimage.png')}}"
                                    class="img-responsive wp-post-image"
                                />
                            </div>
                            <ul class="post-meta">
                                <li>
                                    <span class="author vcard"> <i class="fa fa-user"></i><a class="url fn n" href="#">admin</a> </span>
                                </li>
                            </ul>
                        </header>
                        <div class="blog-content" style="height: 250px;">
                            <div class="entry-header">
                                <h4 class="entry-title"><a href="{{route('agenda_detail', ['slug' => $item->slug_m_agenda])}}" rel="bookmark">{{substr($item->judul_m_agenda, 0, 100)}}</a></h4>
                            </div>
                            <div class="entry-content">
                                <p>
                                    {{--<strong>{{\Carbon\Carbon::parse($item->tgl_mulai)->isoFormat('D MMMM YYYY')}} 
                                    @if($item->tgl_mulai != $item->tgl_selesai)
                                    s/d {{\Carbon\Carbon::parse($item->tgl_selesai)->isoFormat('D MMMM YYYY')}}
                                    @endif    
                                    </strong>--}}
                                    {!!substr(strip_tags($item->isi_m_agenda), 0, 120)!!} ...
                                    <a href="{{route('agenda_detail', ['slug' => $item->slug_m_agenda])}}" class="more-link"><span class="readmore">Selengkapnya</span></a>
                                </p>
                            </div>
                        </div>
                        <footer class="entry-footer clearfix">
                            <ul class="entry-meta">
                                <li>&nbsp;</li>
                            </ul>
                        </footer>
                    </article>
                </div>
                @endforeach
                
                

                <div class="col-md-12 text-center masonry-brick" style="position: absolute; left: 0px; top: 1370px;">
                    {{$agenda->links()}}
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('js')
<script>
$('#myCarousel').carousel();
var winWidth = $(window).innerWidth();
$(window).resize(function () {

    if ($(window).innerWidth() < winWidth) {
        $('.carousel-inner>.item>img').css({
            'min-width': winWidth, 'width': winWidth
        });
    }
    else {
        winWidth = $(window).innerWidth();
        $('.carousel-inner>.item>img').css({
            'min-width': '', 'width': ''
        });
    }
});
</script>
@endsection