@extends('front.template.index_detail')

@section('content')
<style>
.post-meta {
    position: absolute;
    bottom: 5px;
    width: 100%;
    padding: 0 0 0 10px;
    margin: 0;
    list-style: none;
    z-index: 100;
    color: #fff;
}
</style>
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'pengumuman')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Pengumuman</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Pengumuman</li>
            </ul>
        </div>
    </div>
</section>









<div class="blog-wrapper blog-grid content-wrapper" style="min-height: 1000px;">
    <div class="container">
        <div class="row">
            <div id="main" class="posts-content" >

                <label for="">Pencarian Berdasarkan Program Pelatihan</label>
                <div class="row" style="padding-bottom: 20px;">
                    <div class="col-md-6">
                        <select name="id_m_program_pelatihan" id="id_m_program_pelatihan" class="form-control">
                            <option value="">SEMUA PROGRAM PELATIHAN</option>
                            @foreach ($prog as $item)
                                <option {{(request()->get('program_pelatihan') == $item->id_m_program_pelatihan) ? 'selected' : null}} value="{{$item->id_m_program_pelatihan}}">{{$item->nm_m_program_pelatihan}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-md-2">
                        <button id="filter" name="filter" class="btn btn-success" style="padding: 9px;">FILTER</button>
                    </div>
                </div>


                @php
                    app()->setLocale('id');
                @endphp
                @foreach ($list as $item)
                <article id="post-3397" class="post-wrapper post-3397 post type-post status-publish format-standard hentry category-post-formats tag-standard">
                    <div class="blog-content">
                        <div class="entry-header">
                            <h2 class="entry-title"><a href="{{route('pengumuman_detail', ['id' => $item->id_m_pendaftar_mtu])}}">{{$item->jadwal->nm_m_jadwal}}</a></h2>
                            <ul class="entry-meta clearfix">
                                <li><i class="fa fa-calendar"></i><a href="{{route('hasil_seleksi_detail', ['id' => $item->id_m_pendaftar_mtu])}}">{{\Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM YYYY')}}</a></li>
                            </ul>
                        </div>
                        <div class="entry-content">
                            <p>
                                Berikut kami sampaikan daftar calon peserta MTU / Swadana yang telah melalui tahap verifikasi administrasi untuk pelatihan MTU / Swadana dengan materi : <strong>"{{$item->jadwal->nm_m_jadwal}}"</strong>
                                <a href="{{route('pengumuman_detail', ['id' => $item->id_m_pendaftar_mtu])}}" class="more-link"><span class="readmore">SELENGKAPNYA</span></a>
                            </p>
                        </div>
                    </div>
                    <footer class="entry-footer clearfix">
                        <ul class="entry-meta">
                            <li>
                                <span class="post-comments"> &nbsp; </span>
                            </li>
                        </ul>
                    </footer>
                </article>

                @endforeach
                
                

                <div class="col-md-12 text-center masonry-brick" style="position: absolute; left: 0px; top: 1370px;">
                    {{$list->appends($_GET)->links()}}
                </div>
            </div>
        </div>
    </div>
</div>



@endsection

@section('js')
    <script>
    $('#filter').click(function(){    
        var id_m_program_pelatihan = $('#id_m_program_pelatihan').val();
        location.href = '{{route('pengumuman_index')}}?program_pelatihan=' + id_m_program_pelatihan;
    })
    </script>
@endsection