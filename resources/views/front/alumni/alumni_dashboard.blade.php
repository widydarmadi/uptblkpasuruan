@extends('front.template.index_detail')

@section('content')

<style>
    .swal2-popup {
      font-size: 1.4rem!important;
    }
    .box_red{
        background-color: #F9F7F7!important;
        padding: 15px;margin: 10px 1px; 
        border-radius: 5px;
        border:1px solid #F4F0F0;
        box-shadow: 0px 2px 5px 0px rgb(0 0 0 / 10%);
        margin-bottom: 15px!important;
        cursor: pointer;
        transition: all .3s ease;
    }
    .box_red:hover{
        background: #ffffff!important;
    }
    .box_red a{
        color: black;
        font-size: 16px;
    }
  </style>
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'faq')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Halaman Utama - Alumni</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Halaman Utama - Alumni</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                    


                                <div class="panel-group" id="accordionGroupOpen" role="tablist" aria-multiselectable="true">
                                    <div class="row">
                                        <div class="col-md-3" 
                                            style="margin: 0 auto;">
                                            <div class="box_red" onclick="location.href='{{route('alumni_page.edit_profil')}}'">
                                                <a href="javascript:void(0)"><img src="{{asset('assets/front/images/resume.png')}}" height="40" />&nbsp;&nbsp; Ubah Profil Alumni</a>
                                            </div>
                                        </div>
                                        {{--<div class="col-md-3" 
                                            style="margin: 0 auto;">
                                            <div class="box_red" onclick="location.href='{{route('alumni_page.edit_profil')}}'">
                                                <a href="#"><img src="{{asset('assets/front/images/loading.png')}}" height="40" />&nbsp;&nbsp; Reset Password</a>
                                            </div>
                                        </div>--}}
                                        {{-- <div class="col-md-3" 
                                            style="margin: 0 auto;">
                                            <div class="box_red" onclick="location.href='{{route('alumni_page.edit_profil')}}'">
                                                <a href="#"><img src="{{asset('assets/front/images/certificate.png')}}" height="40" />&nbsp;&nbsp; Download Sertifikat</a>
                                            </div>
                                        </div> --}}
                                        <div class="col-md-3" 
                                            style="margin: 0 auto;">
                                            <div class="box_red" onclick="location.href='{{route('alumni_page.pekerjaan.index')}}'">
                                                <a href="{{route('alumni_page.pekerjaan.index')}}"><img src="{{asset('assets/front/images/job-loss.png')}}" height="40" />&nbsp;&nbsp; Data Pekerjaan</a>
                                            </div>
                                        </div>
                                        <div class="col-md-3" 
                                            style="margin: 0 auto;">
                                            <div class="box_red" onclick="location.href='{{route('alumni_page.usaha.index')}}'">
                                                <a href="{{route('alumni_page.usaha.index')}}"><img src="{{asset('assets/front/images/my-business.png')}}" height="40" />&nbsp;&nbsp; Data Usaha</a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>
$("#form").submit(function(){
    $(".text-danger").remove();
    event.preventDefault();
    var data = new FormData($('#form')[0]);
    var catValue = '{{request()->get('cat')}}';
    data.append('cat', catValue);
    $("#submitform").attr('disabled', true);
    $("#submitform span").text(loading_text);

    $.ajax({
        url:"{{ route("alumni_daftar_post") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: data,
        processData: false,
        contentType: false,
        success:function(data)
        {
            if($.isEmptyObject(data.error)){
                if(data.status == true){
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Daftar Sekarang');
                    $("form").each(function() { this.reset() });
                    swal.fire({
                        title: "Success",
                        text: data.message,
                        icon: "success"
                    }).then(function() {
                        location.href = data.redirect;
                    });
                }else{
                    displayWarningSwal(data.message);
                    reload_captcha();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Daftar Sekarang');
                }

            }else{
                displayWarningSwal();
                reload_captcha();
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Daftar Sekarang');
                $.each(data.error, function(key, value) {
                    var element = $("#" + key);
                    element.closest("div.form-control")
                    .removeClass("text-danger")
                    .addClass(value.length > 0 ? "text-danger" : "")
                    .find("#error_" + key).remove();
                    element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                });
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});


$('#ceknik').click(function(){
    var nik_m_alumni = $('#nik_m_alumni').val();
    $("#ceknik").attr('disabled', true);
    $("#ceknik span").text(loading_text);
    $.ajax({
        url:"{{ route("alumni_get_nik") }}",
        method:"post",
        datatype: 'json',
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: {
            nik_m_alumni: nik_m_alumni
        },
        success:function(data)
        {
            if(data.status == true){
                $("#ceknik").removeAttr('disabled');
                $("#ceknik span").text('Cek NIK');
                $("#nm_m_alumni").val(data.text);
            }else{
                displayWarningSwal(data.message);
                $("#nm_m_alumni").val('');
                $("#ceknik").removeAttr('disabled');
                $("#ceknik span").text('Cek NIK');
            }

        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
})
</script>
@endsection