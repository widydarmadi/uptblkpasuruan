<form method="POST" id="form">
    <div class="form-group row">
        
        <input type="hidden" name="id_m_alumni" id="id_m_alumni" value="{{session()->get('alumni_logged_in.id_m_alumni')}}" />
        <div class="col-md-12" style="margin-bottom: 15px;">
          <label for="nm_t_pekerjaan" class="control-label">Masukkan Nama Instansi / Lembaga Nama Perusahaan</label> 
          <input id="nm_t_pekerjaan" name="nm_t_pekerjaan" type="text" class="form-control">
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="id_m_bidang_usaha" class="control-label">Bidang Usaha</label> 
            <select id="id_m_bidang_usaha" name="id_m_bidang_usaha" class="form-control">
                <option value="">-- pilih bidang usaha --</option>
                @foreach ($bidang_usaha as $item)
                <option value="{{$item->id_m_bidang_usaha}}">{{$item->nm_m_bidang_usaha}}</option>
                @endforeach
            </select>
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="jabatan_t_pekerjaan" class="control-label">Jabatan</label> 
            <input id="jabatan_t_pekerjaan" name="jabatan_t_pekerjaan" type="text" class="form-control">
        </div>
        <div class="col-md-4" style="margin-bottom: 15px;">
            <label for="tgl_mulai_bekerja" class="control-label">Tanggal Mulai Bekerja</label> 
            <input id="tgl_mulai_bekerja" name="tgl_mulai_bekerja" type="date" class="form-control">
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="alamat_t_pekerjaan" class="control-label">Alamat Kantor</label> 
            <input id="alamat_t_pekerjaan" name="alamat_t_pekerjaan" type="text" class="form-control">
        </div>
        <div class="col-md-12" style="margin-bottom: 15px;">
            <label for="telp_t_pekerjaan" class="control-label">Nomor Telepon Kantor</label> 
            <input id="telp_t_pekerjaan" name="telp_t_pekerjaan" type="text" class="form-control">
        </div>
        
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="id_m_provinsi" class="control-label">Provinsi</label> 
            <select id="id_m_provinsi" name="id_m_provinsi" class="form-control">
                <option value="">-- pilih provinsi --</option>
                @foreach ($id_m_provinsi as $item)
                <option value="{{$item->id_m_provinsi}}">{{$item->nm_m_provinsi}}</option>
                @endforeach
            </select>
        </div>
        
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="id_m_kota" class="control-label">Kabupaten / Kota</label> 
            <select id="id_m_kota" name="id_m_kota" class="form-control">
                <option value="">-- pilih kabupaten / kota --</option>
            </select>
        </div>
        
        
        <div class="col-md-6" style="margin-bottom: 15px;">
            <label for="id_m_kecamatan" class="control-label">Kecamatan</label> 
            <select id="id_m_kecamatan" name="id_m_kecamatan" class="form-control">
                <option value="">-- pilih kecamatan --</option>
            </select>
        </div>
        
{{-- 
        <div class="col-md-12">
            <label for="" class="control-label">Masukkan captcha di bawah ini</label> 
            <br>
            <div class="captcha">
            <span>{!! captcha_img() !!}</span>
            <button onclick="reload_captcha();" type="button" class="btn btn-info" style="padding: 7px 15px;" class="reload" id="reload">
                &#x21bb;
            </button>
            </div>
        </div>
        <div class="col-md-7">
            <br />
            <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
        </div> --}}

        <div class="col-md-12">
          <br />
            <button type="submit" name="submitform" id="submitform" class="btn btn-danger">Simpan</button>

        </div>
      </div>
</form>


<script>
    $('#id_m_provinsi').change(function(){
        $('#id_m_kota').val('');
        $('#id_m_kecamatan').val('');
        var id_m_provinsi = $(this).val();
        $.ajax({
            url:"{{ route("get_combo_kota") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_provinsi:id_m_provinsi
            },
            success:function(data)
            {
                $('#id_m_kota').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })
    
    $('#id_m_kota').change(function(){
        var id_m_kota = $(this).val();
        $('#id_m_kecamatan').val('');
        $.ajax({
            url:"{{ route("get_combo_kecamatan") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_kota:id_m_kota
            },
            success:function(data)
            {
                $('#id_m_kecamatan').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })


$("#form").submit(function(){
    $(".text-danger").remove();
    event.preventDefault();
    var data = new FormData($('#form')[0]);
    $("#submitform").attr('disabled', true);
    $("#submitform span").text(loading_text);

    $.ajax({
        url:"{{ route("alumni_page.pekerjaan.save") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: data,
        processData: false,
        contentType: false,
        success:function(data)
        {
            if($.isEmptyObject(data.error)){
                if(data.status == true){
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Simpan');
                    $("form").each(function() { this.reset() });
                    swal.fire({
                        title: "Success",
                        text: data.message,
                        icon: "success"
                    }).then(function() {
                        location.href = data.redirect;
                    });
                }else{
                    displayWarningSwal(data.message);
                    reload_captcha();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Simpan');
                }

            }else{
                displayWarningSwal();
                reload_captcha();
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Simpan');
                $.each(data.error, function(key, value) {
                    var element = $("#" + key);
                    element.closest("div.form-control")
                    .removeClass("text-danger")
                    .addClass(value.length > 0 ? "text-danger" : "")
                    .find("#error_" + key).remove();
                    element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                });
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});
</script>