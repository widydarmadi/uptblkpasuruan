@extends('front.template.index_detail')

@section('content')

<style>
    .swal2-popup {
      font-size: 1.4rem!important;
    }
    .box_red{
        background-color: #ff2a40!important;
        padding: 15px;margin: 10px 1px; 
        border-radius: 10px;
        border:1px solid #ddd
    }
  </style>



@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'faq')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">{{$page_title}}</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">{{$page_title}}</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                    
                                <a href="#" class="btn btn-danger" id="add_modal">Tambah Data Pekerjaan</a> 
                                <a href="{{route('alumni_page.dashboard')}}" class="btn btn-primary">Kembali</a>

                                <br />
                                <br />
                                <table id="datatable" class="table table-bordered table-hover table-sm table-striped" width="100%">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Instansi</th>
                                            <th>Bidang Usaha</th>
                                            <th>Mulai Kerja</th>
                                            <th>Jabatan</th>
                                            <th>No Telp Instansi</th>
                                            <th>Hapus ?</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                    
                                    </tbody>
                                </table>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>


<!-- Modal -->
<div class="modal fade" id="modal_pekerjaan">
    <div class="modal-dialog modal-lg modal-dialog-centered">
      <div class="modal-content">
      <!-- header-->
        <div class="modal-header">
          <button class="close" data-dismiss="modal"><span>&times;</span></button>
          <h4 class="modal-title" id="">Data Pekerjaan</h4>
        </div>
        <!--body-->
        <div class="modal-body">
          
        </div>
        {{-- <div class="modal-footer">
          <button class="btn btn-danger" data-dismiss="modal">Tutup</button>
        </div> --}}
      </div>
    </div>
  </div>

@endsection

@section('js')

<script>
var table;
$('#add_modal').click(function(){
    $.ajax({
        url:"{{ route("alumni_page.pekerjaan.add") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        success:function(data)
        {
            $('.modal-body').html(data);
            $('#modal_pekerjaan').modal('show');
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
})


$(document).ready( function () {
    table = $('#datatable').DataTable({
        processing: true,
        serverside: true,
        pageLength: 20,
        ajax: {
            url: '{{ route('alumni_page.pekerjaan.datatable') }}',
            method: 'post'
        },
    });
});




$('#datatable').on('click', '.delete', function(){
    var id_t_pekerjaan = $(this).data("id_t_pekerjaan");
    swal.fire({
        title: "Confirmation",
        text: confirm_delete_text,
        icon: "warning",
        showCancelButton: !0,
        confirmButtonText: "OK",
        cancelButtonText: "Cancel",
        reverseButtons: !0
    }).then(function (e) {

        if(e.value){
            $.ajax({
                url:"{{ route('alumni_page.pekerjaan.delete') }}",
                method:"post",
                headers: { 'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content') },
                data:{id_t_pekerjaan:id_t_pekerjaan},
                success:function(data)
                {
                    if(data.status == true){
                        swal.fire({
                            title: "Berhasil menghapus !",
                            text: data_deleted,
                            icon: "success"
                        }).then(function() {
                            table.ajax.reload();
                        });
                    }else{
                        displayErrorSwal(data.message);
                    }
                },
                error: function(data){
                    displayErrorSwal(data.message);
                }
            });
        }else{

        }

        })
});

</script>
@endsection