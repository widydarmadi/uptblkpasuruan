@extends('front.template.index_detail')

@section('content')
<style>
    .swal2-popup {
      font-size: 1.4rem!important;
    }
  </style>
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'faq')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Portal Alumni</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Portal Alumni</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                    


                                <div class="panel-group" id="accordionGroupOpen" role="tablist" aria-multiselectable="true">
                                    <div class="row">
                                        <div class="col-md-6" style="margin: 0 auto; padding: 15px; border:1px solid #ddd">
                                            <div class="alert bg-warning">
                                                <h3>Login Sebagai Alumni Peserta</h3>
                                                Anda akan memasuki portal halaman Alumni peserta pelatihan UPT BLK Pasuruan Prov. Jawa Timur.
                                                Pastikan bahwa Anda pernah mengikuti pelatihan di UPT BLK Pasuruan.
                                            </div>
                                            @if($aktif and $aktif == true)
                                            <div class="alert bg-success">
                                                Akun Anda telah terverifikasi.
                                            </div>
                                            @endif
                                            <form method="POST" id="form">
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                      <label for="email_m_alumni" class="control-label">Masukkan Email</label> 
                                                      <input id="email_m_alumni" name="email_m_alumni" type="text" class="form-control">
                                                    </div>
                                                    <div class="col-md-12">
                                                      <label for="password" class="control-label">Masukkan Password</label> 
                                                      <input type="password" name="password" id="password" class="form-control" />
                                                    </div>
                                                    
                                                    <div class="col-md-12">
                                                      <br />
                                                        <button type="submit" name="submitform" id="submitform" class="btn btn-danger">Masuk</button>
                                                        <a href="{{route('alumni_daftar')}}" class="btn btn-white">Belum punya akun ? Daftar Sekarang</a>
                                                    </div>
                                                  </div>
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')
<script>
$("#form").submit(function(){
        $(".text-danger").remove();
        event.preventDefault();
        var data = new FormData($('#form')[0]);
        var catValue = '{{request()->get('cat')}}';
        data.append('cat', catValue);
        $("#submitform").attr('disabled', true);
        $("#submitform span").text(loading_text);

        $.ajax({
            url:"{{ route("alumni_login_post") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data: data,
            processData: false,
            contentType: false,
            success:function(data)
            {
                if($.isEmptyObject(data.error)){
                    if(data.status == true){
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Masuk');
                        $("form").each(function() { this.reset() });
                        location.href = data.redirect;
                    }else{
                        displayWarningSwal(data.message);
                        $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Masuk');
                    }

                }else{
                    displayWarningSwal();
                    
                    $("#submitform").removeAttr('disabled');
                        $("#submitform span").text('Masuk');
                    $.each(data.error, function(key, value) {
                        var element = $("#" + key);
                        element.closest("div.form-control")
                        .removeClass("text-danger")
                        .addClass(value.length > 0 ? "text-danger" : "")
                        .find("#error_" + key).remove();
                        element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                    });
                }
            },
            error: function(data){
                displayErrorSwal(data.message);
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Masuk');
            }
        });
    });
</script>
@endsection