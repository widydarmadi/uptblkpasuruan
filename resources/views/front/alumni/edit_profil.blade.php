@extends('front.template.index_detail')

@section('content')

<style>
    .swal2-popup {
      font-size: 1.4rem!important;
    }
  </style>
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'faq')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Portal Alumni</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}">Home</a></li>
                <li class="active">Portal Alumni</li>
            </ul>
        </div>
    </div>
</section>


<section class="vc_row section-wrapper" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                    


                                <div class="panel-group" id="accordionGroupOpen" role="tablist" aria-multiselectable="true">
                                    <div class="row">
                                        <div class="col-md-12" style="margin: 0 auto; padding: 15px; border:1px solid #ddd">
                                            <div class="alert bg-warning">
                                                <h3>Profil Saya</h3>
                                                Anda dapat melakukan perubahan data melalui formulir di bawah ini.
                                            </div>
                                            <form method="POST" id="form">
                                                <div class="form-group row">
                                                    
                                                    <input id="id_m_alumni" readonly value="{{$sess->id_m_alumni}}" name="id_m_alumni" type="hidden" class="form-control">
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                      <label for="nik_m_alumni" class="control-label">NIK</label> 
                                                      <input id="nik_m_alumni" readonly value="{{session()->get('alumni_logged_in.nik_m_alumni')}}" name="nik_m_alumni" type="text" class="form-control">
                                                          
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                      <label for="nm_m_alumni" class="control-label">Nama Alumni</label> 
                                                      <input id="nm_m_alumni" value="{{session()->get('alumni_logged_in.nm_m_alumni')}}" name="nm_m_alumni" readonly type="text" class="form-control">
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="email_m_alumni" class="control-label">Masukkan Email</label> 
                                                        <input id="email_m_alumni" name="email_m_alumni" value="{{$sess->email_m_alumni}}" type="text" class="form-control">
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="hp_m_alumni" class="control-label">Nomor Handphone / Whatsapp</label> 
                                                        <input id="hp_m_alumni" name="hp_m_alumni" value="{{$sess->hp_m_alumni}}" type="text" class="form-control">
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="password" class="control-label">Masukkan Password Baru</label> 
                                                        <input placeholder="kosongi jika password tidak berubah" type="password" name="password" id="password" class="form-control" />
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="repassword" class="control-label">Masukkan Ulang Password Baru</label> 
                                                        <input placeholder="kosongi jika password tidak berubah" type="password" name="repassword" id="repassword" class="form-control" />
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                      <label for="is_bekerja_m_alumni" class="control-label">Status Bekerja</label> 
                                                      <select name="is_bekerja_m_alumni" id="is_bekerja_m_alumni" class="form-control">
                                                        <option value="">- pilih status pekerjaan -</option>
                                                        <option {{$sess->is_bekerja_m_alumni == 'BELUM BEKERJA' ? 'selected' : null }} value="BELUM BEKERJA">BELUM BEKERJA</option>
                                                        <option {{$sess->is_bekerja_m_alumni == 'BEKERJA' ? 'selected' : null }} value="BEKERJA">BEKERJA</option>
                                                        <option {{$sess->is_bekerja_m_alumni == 'WIRAUSAHA' ? 'selected' : null }} value="WIRAUSAHA">WIRAUSAHA</option>
                                                      </select>
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                      <label for="jk_m_alumni" class="control-label">Gender / Jenis Kelamin</label> 
                                                      <select name="jk_m_alumni" id="jk_m_alumni" class="form-control">
                                                        <option value="">- pilih jenis kelamin -</option>
                                                        <option {{$sess->jk_m_alumni == 'LAKI-LAKI' ? 'selected' : null}} value="LAKI-LAKI">LAKI-LAKI</option>
                                                        <option {{$sess->jk_m_alumni == 'PEREMPUAN' ? 'selected' : null}} value="PEREMPUAN">PEREMPUAN</option>
                                                      </select>
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                      <label for="id_m_pendidikan" class="control-label">Pendidikan Terakhir</label> 
                                                      <select name="id_m_pendidikan" id="id_m_pendidikan" class="form-control">
                                                        <option value="">- pilih jenjang pendidikan -</option>
                                                        @foreach ($pendidikan as $item)
                                                        <option {{$sess->id_m_pendidikan == $item->id_m_pendidikan ? 'selected' : null}} value="{{$item->id_m_pendidikan}}">{{$item->nm_m_pendidikan}}</option>
                                                        @endforeach
                                                      </select>
                                                    </div>

                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="kejuruan_m_alumni" class="control-label">Nama Kejuruan Sekolah / Kuliah</label> 
                                                        <input id="kejuruan_m_alumni" value="{{$sess->kejuruan_m_alumni}}" name="kejuruan_m_alumni" type="text" class="form-control">
                                                    </div>

                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="asal_sekolah_m_alumni" class="control-label">Asal Sekolah</label> 
                                                        <input id="asal_sekolah_m_alumni" value="{{$sess->asal_sekolah_m_alumni}}" name="asal_sekolah_m_alumni" type="text" class="form-control">
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="alamat_m_alumni" class="control-label">Alamat Domisili</label> 
                                                        <input id="alamat_m_alumni" value="{{$sess->alamat_m_alumni}}" name="alamat_m_alumni" type="text" class="form-control">
                                                    </div>
                                                    @php
                                                        $get_prov = \App\Models\M_kota::select('id_m_provinsi')->where('id_m_kota', $sess->id_m_kota)->first();
                                                        if($get_prov){
                                                            $selected = $get_prov->id_m_provinsi;
                                                        }else{
                                                            $selected = null;
                                                        }
                                                    @endphp
                                                    <div class="col-md-6" style="margin-bottom: 15px;">
                                                        <label for="id_m_provinsi" class="control-label">Provinsi</label> 
                                                        <select id="id_m_provinsi" name="id_m_provinsi" class="form-control">
                                                            <option value="">-- pilih provinsi --</option>
                                                            
                                                            @foreach ($id_m_provinsi as $item)
                                                            <option {{$selected == $item->id_m_provinsi ? 'selected' : null}} value="{{$item->id_m_provinsi}}">{{$item->nm_m_provinsi}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    
                                                    <div class="col-md-6" style="margin-bottom: 15px;">
                                                        <label for="id_m_kota" class="control-label">Kabupaten / Kota</label> 
                                                        <select id="id_m_kota" name="id_m_kota" class="form-control">
                                                            {{--<option value="{{$sess->id_m_kota}}">{{$sess->kota->nm_m_kota}}</option>--}}
                                                            @foreach ($list_kab as $item)
                                                                <option {{($sess->id_m_kota == $item->id_m_kota) ? 'selected' : null}} value="{{$item->id_m_kota}}" class="value">{{$item->nm_m_kota}}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="tempat_lahir_m_alumni" class="control-label">Tempat Lahir</label> 
                                                        <input id="tempat_lahir_m_alumni" value="{{$sess->tempat_lahir_m_alumni}}" name="tempat_lahir_m_alumni" type="text" class="form-control">
                                                    </div>
                                                    <div style="margin-bottom:13px;" class="col-md-6">
                                                        <label for="tgl_lahir_m_alumni" class="control-label">Tanggal Lahir</label> 
                                                        <input id="tgl_lahir_m_alumni" value="{{($sess->tgl_lahir_m_alumni) ? \Carbon\Carbon::parse($sess->tgl_lahir_m_alumni)->format('Y-m-d') : null}}" name="tgl_lahir_m_alumni" type="date" class="form-control">
                                                        
                                                    </div>
                                                    
                                                    {{-- {{dump($sess)}} --}}
                  
                                                    <div style="margin-bottom:13px;" class="col-md-12">
                                                      <br />
                                                        <button type="submit" name="submitform" id="submitform" class="btn btn-primary">Ubah Profil</button>
                                                        <a href="{{route('alumni_page.dashboard')}}" class="btn btn-danger">Kembali</a>

                                                    </div>
                                                  </div>
                                            </form>
                                        </div>
                                    </div>
                                    
                                </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

@endsection

@section('js')

<script>
 $('.input-group.date').datepicker({format: "dd-mm-yyyy"}); 

$("#form").submit(function(){
    $(".text-danger").remove();
    event.preventDefault();
    var data = new FormData($('#form')[0]);
    $("#submitform").attr('disabled', true);
    $("#submitform span").text(loading_text);

    $.ajax({
        url:"{{ route("alumni_page.edit_profil_post") }}",
        method:"post",
        headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
        data: data,
        processData: false,
        contentType: false,
        success:function(data)
        {
            if($.isEmptyObject(data.error)){
                if(data.status == true){
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Daftar Sekarang');
                    $("form").each(function() { this.reset() });
                    swal.fire({
                        title: "Success",
                        text: data.message,
                        icon: "success"
                    }).then(function() {
                        location.href = data.redirect;
                    });
                }else{
                    displayWarningSwal(data.message);
                    reload_captcha();
                    $("#submitform").removeAttr('disabled');
                    $("#submitform span").text('Daftar Sekarang');
                }

            }else{
                displayWarningSwal();
                reload_captcha();
                $("#submitform").removeAttr('disabled');
                $("#submitform span").text('Daftar Sekarang');
                $.each(data.error, function(key, value) {
                    var element = $("#" + key);
                    element.closest("div.form-control")
                    .removeClass("text-danger")
                    .addClass(value.length > 0 ? "text-danger" : "")
                    .find("#error_" + key).remove();
                    element.after("<div id=error_"+ key + " class=text-danger>" + value + "</div>");
                });
            }
        },
        error: function(data){
            displayErrorSwal(data.message);
        }
    });
});
$('#id_m_provinsi').change(function(){
        $('#id_m_kota').val('');
        $('#id_m_kecamatan').val('');
        var id_m_provinsi = $(this).val();
        $.ajax({
            url:"{{ route("get_combo_kota") }}",
            method:"post",
            headers: { "X-CSRF-TOKEN": $("meta[name=csrf-token]").attr("content") },
            data:{
                id_m_provinsi:id_m_provinsi
            },
            success:function(data)
            {
                $('#id_m_kota').html(data.html);
            },
            error: function(data){
                displayErrorSwal(data.message);
            }
        });
    })
</script>
@endsection