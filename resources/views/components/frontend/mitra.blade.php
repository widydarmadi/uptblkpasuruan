<section class="vc_row section-wrapper vc_custom_1461531052706" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="case-study-wrapper" style="">
                            <div class="case-study-left">
                                <h2 class="text-center">Mitra</h2>


                                @php
                                    $list_mitra = \App\Models\M_perusahaan::where('is_mitra', '1')->where('aktif_m_perusahaan','1')->get();
                                @endphp
                                <div class="testimonial-carousel-wrapper">
                                    <div class="client-slider-v4 clearfix">
                                        <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                            
                                        </div>
                                        
                                        <ol class="flex-control-nav flex-control-thumbs">
                                            @foreach ($list_mitra as $item)
                                            <li>
                                                @if(isset($item->logo))
                                                    <img  title="{{$item->nm_m_perusahaan}}" src="{{asset('storage'.'/'.$item->logo)}}" width="100%" />
                                                @else
                                                    <img  title="{{$item->nm_m_perusahaan}}" src="{{asset('assets/front/images/noimage.png')}}" width="100%" />
                                                @endif
                                            </li>
                                            @endforeach
                                        </ol>
                                        <ul class="flex-direction-nav">
                                            <li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li>
                                            <li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
                                        </ul>
                                    </div>
                                </div>

                                    
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>