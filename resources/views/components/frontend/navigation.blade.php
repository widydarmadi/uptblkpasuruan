<nav class="navbar navbar-default">
    <div class="container">
        
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".mobile-toggle"><span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span></button>
            <div class="navbar-brand">
                <div class="container" >
                    <div class="row">
                        <div class="col-12">
                            <h1 style="float: left">
                                <a href="{{url('/')}}" title="Halaman Utama">
                                    @php
                                        $getlogo = \App\Models\M_logo::where('aktif_m_logo', '1')->first();
                                        //dump($get_logo);
                                    @endphp

                                    @if (isset($getlogo->photo))
                                    <img style="height: 50px;" class="site-logo hidden-xs" src="{{asset('storage'.'/'.$getlogo->photo)}}" alt="UPT BLK Pasuruan" />
                                    <img style="height: 60px;" class="mobile-logo visible-xs" src="{{asset('storage'.'/'.$getlogo->photo)}}" alt="UPT BLK Pasuruan" />
                                    @else
                                    <img style="height: 50px;" class="site-logo hidden-xs" src="{{asset('assets/front/images/logo-top.png')}}" alt="UPT BLK Pasuruan" />
                                    <img style="height: 60px;" class="mobile-logo visible-xs" src="{{asset('assets/front/images/logo-top.png')}}" alt="UPT BLK Pasuruan" />
                                    @endif
                                </a>
                            </h1>

                            <div class="main-menu-wrapper hidden-xs clearfix">
                                <div class="main-menu">
                                    <ul id="menu-primary-menu" class="menu nav navbar-nav">
                                        @php
                                        $load_parent_menu = \App\Models\M_menu_front::where('aktif', '1')->whereNull('id_parent')->get();
                                        @endphp

                                        @foreach ($load_parent_menu as $item)
                                        
                                        @if($item->nm_menu == 'Login' and session()->get('alumni_logged_in.id_m_alumni') != null)
                                        <li
                                            class="menu-item" id="menu-item-{{$item->id_m_menu}}"
                                        >
                                            <a class="bg-danger" href="{{route('alumni_page.logout')}}" style="padding: 5px 5px; color:red;">
                                                <strong>Logout</strong>
                                            </a>
                                        </li>
                                        @else
                                        <li
                                            class="menu-item" id="menu-item-{{$item->id_m_menu}}"
                                        >
                                            @php
                                            $load_child_menu = \App\Models\M_menu_front::where('id_parent', $item->id_m_menu)->where('aktif', '1')->get();
                    
                                            @endphp
                                            <a title="{{$item->nm_menu}}" href="{{($item->url != '') ? url($item->url) : 'javascript:void(0)'}}">{{$item->nm_menu}}
                                                @if(count($load_child_menu) > 0)
                                                    <span class="fa fa-angle-down"></span>
                                                @endif
                                            </a>
                                            @if(count($load_child_menu) > 0)
                                            <div class="dropdown-wrapper menu-item-depth-0">
                                                <ul role="menu" class="dropdown-menu">
                                                    @foreach ($load_child_menu as $item)
                                                        <li id="menu-item-{{$item->id_m_menu}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                                            <a href="{{($item->url != '') ? url($item->url) : 'javascript:void(0)'}}">{{$item->nm_menu}}</a>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                            @endif
                                        </li>
                                        @endif
                                        
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
        
        
        {{-- <div class="main-menu-wrapper hidden-xs clearfix">
            <div class="main-menu">
                <ul id="menu-primary-menu" class="menu nav navbar-nav">
                    @php
                    $load_parent_menu = \App\Models\M_menu_front::where('aktif', '1')->whereNull('id_parent')->get();
                    @endphp

                    @foreach ($load_parent_menu as $item)
                    <li
                        class="menu-item" id="menu-item-{{$item->id_m_menu}}"
                    >
                        @php
                        $load_child_menu = \App\Models\M_menu_front::where('id_parent', $item->id_m_menu)->where('aktif', '1')->get();

                        @endphp
                        <a title="{{$item->nm_menu}}" href="{{($item->url != '') ? url($item->url) : 'javascript:void(0)'}}">{{$item->nm_menu}}
                            @if(count($load_child_menu) > 0)
                                <span class="fa fa-angle-down"></span>
                            @endif
                        </a>
                        @if(count($load_child_menu) > 0)
                        <div class="dropdown-wrapper menu-item-depth-0">
                            <ul role="menu" class="dropdown-menu">
                                @foreach ($load_child_menu as $item)
                                    <li id="menu-item-{{$item->id_m_menu}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                        <a href="{{($item->url != '') ? url($item->url) : 'javascript:void(0)'}}">{{$item->nm_menu}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div> --}}



        <div class="visible-xs">
            <div class="mobile-menu collapse navbar-collapse mobile-toggle">
                <ul id="menu-primary-menu-1" class="menu nav navbar-nav">
                    @php
                    $load_parent_menu = \App\Models\M_menu_front::where('aktif', '1')->whereNull('id_parent')->get();
                    @endphp

                    @foreach ($load_parent_menu as $item)
                    <li
                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-{{$item->id_m_menu}} has-menu-child" id="menu-item-{{$item->id_m_menu}}"
                    >
                        @php
                        $load_child_menu = \App\Models\M_menu_front::where('id_parent', $item->id_m_menu)->where('aktif', '1')->get();
                        @endphp
                        <a title="{{$item->nm_menu}}" href="{{($item->url != '') ? $item->url : 'javascript:void(0)'}}">{{$item->nm_menu}}
                            @if(count($load_child_menu) > 0)
                                <button class="dropdown-menu-trigger" type="button" data-toggle="collapse" data-target=".dropdown-menu-{{$item->id_m_menu}}" aria-expanded="false">
                                    <i class="fa fa-angle-right"></i><i class="fa fa-angle-down"></i>
                                </button>
                            @endif
                        </a>
                        @if(count($load_child_menu) > 0)
                            <ul role="menu" class="collapse dropdown-menu-{{$item->id_m_menu}}">
                                @foreach ($load_child_menu as $item)
                                    <li id="menu-item-{{$item->id_m_menu}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-3370 has-menu-child dropdown-inverse">
                                        <a href="{{($item->url != '') ? $item->url : 'javascript:void(0)'}}">{{$item->nm_menu}}</a>
                                    </li>
                                @endforeach
                            </ul>
                        @endif
                    </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</nav>