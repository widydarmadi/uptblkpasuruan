<section class="vc_row section-wrapper vc_custom_1459912407416" style="margin-bottom: 30px">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h2 class="text-center">Daftar Kejuruan</h2>
                <br />
                @php
                    $x = 1;
                @endphp
                {{-- @php
                    $coll = collect($kategori_kejuruan)->SortByDesc('kejuruan_count');
                @endphp --}}
                @foreach ($kategori_kejuruan as $item)
                @if($x % 3 == 0)
                <div class="row">
                @endif
                    <div class="col-md-4">
                        <div class="kejuruan-list" style="height: 550px;">
                                <H4 class="text-center theme-color">{{$item->nm_m_kategori_kejuruan}}</H4>
                                @if(isset($item->photo))
                                <img style="height: 215px;" src="{{asset('storage'.'/'.$item->photo)}}" width="100%" />
                                @else
                                <img src="{{asset('assets/front/images/noimage.png')}}" width="100%" />
                                @endif

                                {!!$item->desc_m_kategori_kejuruan!!}

                                {{-- <p class="text-center"><strong>SUB KEJURUAN</strong></p>
                                @php
                                    $list = \App\Models\M_kejuruan::where('id_m_kategori_kejuruan', $item->id_m_kategori_kejuruan)->orderBy('nm_m_kejuruan')->get();
                                @endphp
                                <ul class="list-group">
                                @foreach ($list as $item)
                                    <li class="list-group-item text-center">{{$item->nm_m_kejuruan}}</li>
                                @endforeach
                                </ul> --}}

                                <a style="display: block;" class="btn btn-danger w-100 text-dark" href="{{route('program_pelatihan_home', ['kategori_id' => $item->id_m_kategori_kejuruan])}}">Cek Program Pelatihan</a>

                            </div>
                    </div>
                @if($x % 3 == 0)
                </div>
                @endif
                @php
                    $x++;
                @endphp
                @endforeach
            </div>
       </div>
    </div>
</section>
