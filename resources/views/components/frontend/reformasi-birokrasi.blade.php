<section class="vc_row section-wrapper vc_custom_1461404414456">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">

                        {{-- <div class="row">
                            <div class="col-md-6">
                                <h1 class="theme-color">Zona Integritas</h1>
                                <p>Zona Integritas adalah mewujudkan Wilayah Bebas Korupsi melalui Reformasi Birokrasi</p>

                            </div>
                            <div class="col-md-6">
                                <h1 class="theme-color">SIDESI</h1>
                                <p>SIDESI merupakan inovasi pelayanan publik di UPT BLK Pasuruan</p>

                            </div>
                        </div> --}}


                        <div class="wpb_revslider_element wpb_content_element">
                            <link href="http://fonts.googleapis.com/css?family=Roboto+Slab%3A400%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                            <link href="http://fonts.googleapis.com/css?family=Roboto%3A900%2C700" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                            <link href="http://fonts.googleapis.com/css?family=Roboto+Slab%3A400" rel="stylesheet" property="stylesheet" type="text/css" media="all" />
                            <div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" style="margin: 0px auto; background-color: #ffffff; padding: 0px; margin-top: 0px; margin-bottom: 0px;">
                                <div id="rev_slider_6_1" class="rev_slider fullwidthabanner" style="display: none;" data-version="5.2.6">
                                    <ul>

                                        @foreach ($rb as $item)
                                            
                                        <li
                                            data-index="rs-{{$item->id_m_reformasi_birokrasi}}"
                                            data-transition="parallaxvertical"
                                            data-slotamount="default"
                                            data-hideafterloop="0"
                                            data-hideslideonmobile="off"
                                            data-easein="default"
                                            data-easeout="default"
                                            data-masterspeed="default"
                                            data-thumb="{{asset('assets/front/images/avatar-rb.jpg')}}"
                                            data-rotate="0"
                                            data-fstransition="fade"
                                            data-fsmasterspeed="1500"
                                            data-fsslotamount="7"
                                            data-saveperformance="off"
                                            data-title="{{$item->judul_m_reformasi_birokrasi}}"
                                            data-param1="{{\Carbon\Carbon::parse($item->updated_at)->isoFormat('DD MMMM YYYY')}}"
                                            {{-- data-param2="{!!$item->isi_m_reformasi_birokrasi!!}" --}}
                                            data-param3=""
                                            data-param4=""
                                            data-param5=""
                                            data-param6=""
                                            data-param7=""
                                            data-param8=""
                                            data-param9=""
                                            data-param10=""
                                            data-description=""
                                        >
                                            <img
                                                src="{{(isset($item->photo)) ? asset('storage'.'/'.$item->photo) : asset('assets/front/images/avatar-rb.jpg')}}"
                                                alt=""
                                                title="portfolio-2"
                                                width="1140"
                                                height="640"
                                                data-bgposition="center center"
                                                data-bgfit="cover"
                                                data-bgrepeat="no-repeat"
                                                data-bgparallax="10"
                                                class="rev-slidebg"
                                                data-no-retina
                                            />
                                            <div
                                                class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
                                                id="slide-{{$item->id_m_reformasi_birokrasi}}-layer-3"
                                                data-x="['center','center','center','center']"
                                                data-hoffset="['0','0','0','0']"
                                                data-y="['middle','middle','middle','middle']"
                                                data-voffset="['0','0','0','0']"
                                                data-width="full"
                                                data-height="full"
                                                data-whitespace="normal"
                                                data-transform_idle="o:1;"
                                                data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                                data-transform_out="opacity:0;s:1000;e:Power3.easeInOut;"
                                                data-start="1000"
                                                data-basealign="slide"
                                                data-responsive_offset="on"
                                                style="z-index: 5; text-transform: left; background-color: rgba(0, 0, 0, 0.35); border-color: rgba(0, 0, 0, 1);"
                                            ></div>
                                            <div
                                                class="tp-caption Newspaper-Title tp-resizeme"
                                                id="slide-{{$item->id_m_reformasi_birokrasi}}-layer-1"
                                                data-x="['left','left','left','left']"
                                                data-hoffset="['50','50','50','30']"
                                                data-y="['top','top','top','top']"
                                                data-voffset="['165','135','105','130']"
                                                data-fontsize="['50','50','50','30']"
                                                data-lineheight="['55','55','55','35']"
                                                data-width="['600','600','600','420']"
                                                data-height="none"
                                                data-whitespace="normal"
                                                data-transform_idle="o:1;"
                                                data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                                data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                                data-mask_in="x:0px;y:0px;"
                                                data-mask_out="x:0;y:0;"
                                                data-start="1000"
                                                data-splitin="none"
                                                data-splitout="none"
                                                data-responsive_offset="on"
                                                style="z-index: 6; min-width: 600px; max-width: 600px; white-space: normal; color: rgba(255, 255, 255, 1); text-transform: left;"
                                            >
                                                {{$item->judul_m_reformasi_birokrasi}}
                                                <h4 style="color: #ffffff">{!!$item->isi_m_reformasi_birokrasi!!}</h4>
                                            </div>
                                            <div
                                                class="tp-caption Newspaper-Subtitle tp-resizeme"
                                                id="slide-{{$item->id_m_reformasi_birokrasi}}-layer-2"
                                                data-x="['left','left','left','left']"
                                                data-hoffset="['50','50','50','30']"
                                                data-y="['top','top','top','top']"
                                                data-voffset="['140','110','80','100']"
                                                data-width="none"
                                                data-height="none"
                                                data-whitespace="nowrap"
                                                data-transform_idle="o:1;"
                                                data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                                data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                                data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                                data-start="1000"
                                                data-splitin="none"
                                                data-splitout="none"
                                                data-responsive_offset="on"
                                                style="z-index: 7; white-space: nowrap; text-transform: uppercase;"
                                            >
                                                {{\Carbon\Carbon::parse($item->updated_at)->isoFormat('DD MMMM YYYY')}}
                                            </div>
                                            <a
                                                class="tp-caption Newspaper-Button rev-btn"
                                                href="{{url('reformasi-birokrasi')}}"
                                                target="_self"
                                                id="slide-{{$item->id_m_reformasi_birokrasi}}-layer-5"
                                                data-x="['left','left','left','left']"
                                                data-hoffset="['53','53','53','30']"
                                                data-y="['top','top','top','top']"
                                                data-voffset="['361','331','301','245']"
                                                data-width="none"
                                                data-height="none"
                                                data-whitespace="nowrap"
                                                data-transform_idle="o:1;"
                                                data-transform_hover="o:1;rX:0;rY:0;rZ:0;z:0;s:300;e:Power1.easeInOut;"
                                                data-style_hover="c:rgba(0, 0, 0, 1.00);bg:rgba(255, 255, 255, 1.00);bc:rgba(255, 255, 255, 1.00);"
                                                data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                                                data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                                                data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                                                data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                                                data-start="1000"
                                                data-splitin="none"
                                                data-splitout="none"
                                                data-actions=""
                                                data-responsive_offset="on"
                                                data-responsive="off"
                                                style="
                                                    z-index: 8;
                                                    white-space: nowrap;
                                                    text-transform: left;
                                                    outline: none;
                                                    box-shadow: none;
                                                    box-sizing: border-box;
                                                    -moz-box-sizing: border-box;
                                                    -webkit-box-sizing: border-box;
                                                    cursor: pointer;
                                                "
                                            >
                                                SELENGKAPNYA
                                            </a>
                                        </li>
                                        @endforeach
                                        
                                    </ul>
                                    <script>
                                        var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                        var htmlDivCss = "";
                                        if (htmlDiv) {
                                            htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                        } else {
                                            var htmlDiv = document.createElement("div");
                                            htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                            document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                        }
                                    </script>
                                    <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                                </div>
                                <script>
                                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                    var htmlDivCss =
                                        '.tp-caption.Newspaper-Button,.Newspaper-Button{color:rgba(255,255,255,1.00);font-size:13px;line-height:17px;font-weight:700;font-style:normal;font-family:Roboto;padding:12px 35px 12px 35px;text-decoration:none;background-color:rgba(255,255,255,0);border-color:rgba(255,255,255,0.25);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;text-align:left;letter-spacing:2px}.tp-caption.Newspaper-Button:hover,.Newspaper-Button:hover{color:rgba(0,0,0,1.00);text-decoration:none;background-color:rgba(255,255,255,1.00);border-color:rgba(255,255,255,1.00);border-style:solid;border-width:1px;border-radius:0px 0px 0px 0px;cursor:pointer}.tp-caption.Newspaper-Subtitle,.Newspaper-Subtitle{color:rgba(168,216,238,1.00);font-size:15px;line-height:20px;font-weight:900;font-style:normal;font-family:Roboto;padding:0 0 0 0px;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;text-align:left}.tp-caption.Newspaper-Title,.Newspaper-Title{color:rgba(255,255,255,1.00);font-size:30px;line-height:55px;font-weight:400;font-style:normal;font-family:"Roboto Slab";padding:0 0 10px 0;text-decoration:none;background-color:transparent;border-color:transparent;border-style:none;border-width:0px;border-radius:0 0 0 0px;text-align:left}';
                                    if (htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    } else {
                                        var htmlDiv = document.createElement("div");
                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                                <script type="text/javascript">
                                    /******************************************
        -	PREPARE PLACEHOLDER FOR SLIDER	-
    ******************************************/

                                    var setREVStartSize = function () {
                                        try {
                                            var e = new Object(),
                                                i = jQuery(window).width(),
                                                t = 9999,
                                                r = 0,
                                                n = 0,
                                                l = 0,
                                                f = 0,
                                                s = 0,
                                                h = 0;
                                            e.c = jQuery("#rev_slider_6_1");
                                            e.responsiveLevels = [1240, 1024, 778, 480];
                                            e.gridwidth = [1240, 1024, 778, 480];
                                            e.gridheight = [500, 450, 400, 350];

                                            e.sliderLayout = "fullwidth";
                                            if (
                                                (e.responsiveLevels &&
                                                    (jQuery.each(e.responsiveLevels, function (e, f) {
                                                        f > i && ((t = r = f), (l = e)), i > f && f > r && ((r = f), (n = e));
                                                    }),
                                                    t > r && (l = n)),
                                                (f = e.gridheight[l] || e.gridheight[0] || e.gridheight),
                                                (s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth),
                                                (h = i / s),
                                                (h = h > 1 ? 1 : h),
                                                (f = Math.round(h * f)),
                                                "fullscreen" == e.sliderLayout)
                                            ) {
                                                var u = (e.c.width(), jQuery(window).height());
                                                if (void 0 != e.fullScreenOffsetContainer) {
                                                    var c = e.fullScreenOffsetContainer.split(",");
                                                    if (c)
                                                        jQuery.each(c, function (e, i) {
                                                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u;
                                                        }),
                                                            e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0
                                                                ? (u -= (jQuery(window).height() * parseInt(e.fullScreenOffset, 0)) / 100)
                                                                : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0));
                                                }
                                                f = u;
                                            } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                            e.c.closest(".rev_slider_wrapper").css({ height: f });
                                        } catch (d) {
                                            console.log("Failure at Presize of Slider:" + d);
                                        }
                                    };

                                    setREVStartSize();

                                    var tpj = jQuery;

                                    var revapi6;
                                    tpj(document).ready(function () {
                                        if (tpj("#rev_slider_6_1").revolution == undefined) {
                                            revslider_showDoubleJqueryError("#rev_slider_6_1");
                                        } else {
                                            revapi6 = tpj("#rev_slider_6_1")
                                                .show()
                                                .revolution({
                                                    sliderType: "standard",
                                                    jsFileLocation: "{{asset('assets/front/plugins/news-slider/js')}}/",
                                                    sliderLayout: "fullwidth",
                                                    dottedOverlay: "none",
                                                    delay: 9000,
                                                    navigation: {
                                                        keyboardNavigation: "on",
                                                        keyboard_direction: "horizontal",
                                                        mouseScrollNavigation: "off",
                                                        mouseScrollReverse: "default",
                                                        onHoverStop: "off",
                                                        touch: {
                                                            touchenabled: "on",
                                                            swipe_threshold: 75,
                                                            swipe_min_touches: 1,
                                                            swipe_direction: "horizontal",
                                                            drag_block_vertical: false,
                                                        },
                                                        arrows: {
                                                            style: "gyges",
                                                            enable: true,
                                                            hide_onmobile: false,
                                                            hide_over: 778,
                                                            hide_onleave: false,
                                                            tmp: "",
                                                            left: {
                                                                h_align: "right",
                                                                v_align: "bottom",
                                                                h_offset: 40,
                                                                v_offset: 0,
                                                            },
                                                            right: {
                                                                h_align: "right",
                                                                v_align: "bottom",
                                                                h_offset: 0,
                                                                v_offset: 0,
                                                            },
                                                        },
                                                        tabs: {
                                                            style: "hebe",
                                                            enable: true,
                                                            width: 250,
                                                            height: 100,
                                                            min_width: 250,
                                                            wrapper_padding: 0,
                                                            wrapper_color: "transparent",
                                                            wrapper_opacity: "0",
                                                            tmp: '<div class="tp-tab-title">@php echo '{{';@endphpparam1@php echo '}}';@endphp</div><div class="tp-tab-desc">@php echo '{{';@endphptitle@php echo '}}';@endphp</div>',
                                                            visibleAmount: 3,
                                                            hide_onmobile: true,
                                                            hide_under: 778,
                                                            hide_onleave: false,
                                                            hide_delay: 200,
                                                            direction: "vertical",
                                                            span: false,
                                                            position: "inner",
                                                            space: 10,
                                                            h_align: "right",
                                                            v_align: "center",
                                                            h_offset: 30,
                                                            v_offset: 0,
                                                        },
                                                    },
                                                    responsiveLevels: [1240, 1024, 778, 480],
                                                    visibilityLevels: [1240, 1024, 778, 480],
                                                    gridwidth: [1240, 1024, 778, 480],
                                                    gridheight: [500, 450, 400, 350],
                                                    lazyType: "none",
                                                    parallax: {
                                                        type: "scroll",
                                                        origo: "enterpoint",
                                                        speed: 400,
                                                        levels: [5, 10, 15, 20, 25, 30, 35, 40, 45, 50, 47, 48, 49, 50, 51, 55],
                                                        type: "scroll",
                                                    },
                                                    shadow: 0,
                                                    spinner: "off",
                                                    stopLoop: "on",
                                                    stopAfterLoops: 0,
                                                    stopAtSlide: 1,
                                                    shuffle: "off",
                                                    autoHeight: "off",
                                                    disableProgressBar: "on",
                                                    hideThumbsOnMobile: "off",
                                                    hideSliderAtLimit: 0,
                                                    hideCaptionAtLimit: 0,
                                                    hideAllCaptionAtLilmit: 0,
                                                    debugMode: false,
                                                    fallbacks: {
                                                        simplifyAll: "off",
                                                        nextSlideOnWindowFocus: "off",
                                                        disableFocusListener: false,
                                                    },
                                                });
                                        }
                                    }); /*ready*/
                                </script>
                                <script>
                                    var htmlDivCss = unescape(
                                        "%0A.hebe%20.tp-tab-title%20%7B%0A%20%20%20%20color%3Argb%28168%2C%20216%2C%20238%29%3B%0A%20%20%20%20font-size%3A13px%3B%0A%20%20%20%20font-weight%3A700%3B%0A%20%20%20%20text-transform%3Auppercase%3B%0A%20%20%20%20font-family%3A%22Roboto%20Slab%22%0A%20%20%20%20margin-bottom%3A5px%3B%0A%7D%0A%0A.hebe%20.tp-tab-desc%20%7B%0A%09font-size%3A18px%3B%0A%20%20%20%20font-weight%3A400%3B%0A%20%20%20%20color%3Argb%28255%2C%20255%2C%20255%29%3B%0A%20%20%20%20line-height%3A25px%3B%0A%09font-family%3A%22Roboto%20Slab%22%3B%0A%7D%0A%0A"
                                    );
                                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                    if (htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    } else {
                                        var htmlDiv = document.createElement("div");
                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                            </div>
                        </div>




                        


                    </div>
                </div>
            </div>
        </div>
    </div>
</section>