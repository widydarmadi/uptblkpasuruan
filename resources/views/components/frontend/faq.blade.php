<section class="vc_row section-wrapper bg-silver" style="margin-bottom: 30px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column  vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="sction-title-wrapper text-left vc_custom_1461534674457">
                            <h2 style="" class="section-title theme-color">FAQ</h2>
                        </div>
                        
                        <div class="wpb_text_column wpb_content_element vc_custom_1459914002700">
                            <div class="wpb_wrapper">
                                    


                                <div class="panel-group" id="accordionGroupOpen" role="tablist" aria-multiselectable="true">
                                    
                                    @php
                                    $x = 0;
                                    @endphp
                                    
                                    @foreach ($faq as $item)
                                    
                                    <div class="panel panel-default">
                                      <div class="panel-heading" role="tab" id="headingOne">
                                        <h4 class="panel-title">
                                          <a role="button" data-toggle="collapse" data-parent="#accordionGroupOpen" href="#faq_{{$item->id_m_faq}}" aria-expanded="false" aria-controls="collapseOpenOne">
                                            {{$item->tanya_m_faq}}
                                          </a>
                                        </h4>
                                      </div>
                                      <div id="faq_{{$item->id_m_faq}}" class="panel-collapse collapse {{($x == 0) ? 'in' : ''}}" role="tabpanel" aria-labelledby="headingOne">
                                        <div class="panel-body" >
                                            {!!$item->jawab_m_faq!!}
                                        </div>
                                      </div>
                                    </div>

                                    @php
                                    $x++;
                                    @endphp

                                    @endforeach

                                    
                                  </div>


                            </div>
                        </div>
                        
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</section>

