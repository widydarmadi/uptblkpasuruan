<section class="vc_row section-wrapper vc_custom_1460529049797 vc_row-has-fill" style="background:#286090!important;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_col-sm-12">
                <div class="vc_column-inner">
                    
                    
                    <div class="wpb_wrapper" style="padding: 15px;">
                        <div class="tt-cta-wrapper button-right">
                            <div class="tt-cta-content">
                                <h2 style="color: #ffffff; font-size: 25px;" class="section-title custom-color">Unduh Petunjuk Penggunaan Aplikasi SIDESI</h2>
                                <div class="sub-title" style="font-size: 18px;">
                                    {{--<p><span style="color: #ffffff;">Dapatkan paket pelatihan kerja sesuai dengan keahlian Anda.</span></p>--}}
                                    <p style="color:#fff;">Silahkan mengunduh / download dokumentasi teknis mengenai petunjuk penggunaan Sistem Informasi Sadar Pelatihan Terintegrasi (SIDESI)</p>
                                </div>
                            </div>
                            <div class="tt-cta-button" style=""><a class="btn btn-outline btn-md" href="{{url('assets/front/docs/MANUAL BOOK.pdf')}}" target="_blank">Klik Disini</a></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
