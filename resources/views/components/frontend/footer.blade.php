<footer class="footer-section footer-multi-wrapper">
    <div class="container">
        <div class="row">
            <div class="tt-sidebar-wrapper footer-sidebar clearfix text-left" role="complementary">
                <div id="black-studio-tinymce-2" class="col-md-4 col-sm-12 widget widget_black_studio_tinymce">
                    <h3 class="widget-title">UPT Balai Latihan Kerja Pasuruan</h3>
                    <div class="textwidget">
                        <p>
                            {!!$footer->alamat_kantor!!}
                            <span class="dashed-border"></span>
                            {!!$footer->jam_kerja!!}
                        </p>
                        <ul>
                            <li><i class="fa fa-instagram"></i>{!!$footer->ig!!}</li>
                            <li><i class="fa fa-phone"></i>{{$footer->telp}}</li>
                            <li><i class="fa fa-youtube"></i>{{$footer->youtube}}</li>
                            <li><i class="fa fa-envelope"></i>{{$footer->email}}</li>
                        </ul>
                    </div>
                </div>
                <div id="nav_menu-2" class="col-md-3 col-sm-12 widget widget_nav_menu">
                    <h3 class="widget-title">Quick Links</h3>
                    <div class="menu-quick-links-container">
                        <ul id="menu-quick-links" class="menu">
                            @php
                                $link = \App\Models\M_link_cepat::where('aktif', '1')->orderBy('id_m_link_cepat')->get();
                            @endphp

                            @foreach ($link as $item)
                                <li id="menu-item-{{$item->id_m_link_cepat}}" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-{{$item->id_m_link_cepat}}"><a target="_blank" href="{{$item->url_m_link_cepat}}">{{$item->nm_m_link_cepat}}</a></li>
                            @endforeach
                        </ul>
                        
                    </div>
                </div>
                <div id="nav_menu-2" class="col-md-4 col-sm-12 widget widget_nav_menu">
                    <h3 class="widget-title">MAP BLK Pasuruan</h3>
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe width="100%" height="300" id="gmap_canvas" src="https://maps.google.com/maps?q=upt%20blk%20pasuruan&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-sm-7">
                    <div class="copyright">Copyright © 2021 Pemerintah Provinsi Jawa Timur - UPT Balai Latihan Kerja, Pasuruan</div>
                </div>
                
            </div>
        </div>
    </div>
</footer>