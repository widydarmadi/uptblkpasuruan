{{-- <section class="vc_row section-wrapper vc_custom_1461534387292 vc_row-has-fill" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="sction-title-wrapper text-left vc_custom_1461533115567">
                            <h2 style="" class="section-title theme-color">Testimoni</h2>
                            <div class="sub-title" style="">
                                <p class="font-weight-bold">
                                   <strong>Apa pendapat dari para stakeholder mengenai alumni dari UPT BLK Pasuruan ?</strong>
                                </p>
                            </div>
                        </div>
                        <div class="testimonial-carousel-wrapper">
                            <div class="client-slider-v3 clearfix">
                                <div class="flex-viewport" style="overflow: hidden; position: relative;">
                                    <ul class="slides" style="width: 1000%; transition-duration: 0.6s; transform: translate3d(-1710px, 0px, 0px);">
                                        @foreach ($list as $item)
                                         
                                        <li data-thumb="{{($item->jk == 'P') ? asset('assets/front/images/avatar_female.png') : asset('assets/front/images/avatar_male.png')}} " class="clone" aria-hidden="true" style="width: 570px; float: left; display: block;">
                                            <blockquote>
                                                <div class="quote-content" style="">
                                                    <p>
                                                        {{$item->isi_m_testimoni}}
                                                    </p>
                                                </div>
                                                <footer class="clearfix">
                                                    <div class="client-info"><strong><span class="client-name" style="">{{$item->nm_pengirim}}</span></strong></div>
                                                </footer>
                                            </blockquote>
                                        </li>

                                        @endforeach

                                        
                                    </ul>
                                </div>
                                
                                <ul class="flex-direction-nav">
                                    <li class="flex-nav-prev"><a class="flex-prev" href="#">Previous</a></li>
                                    <li class="flex-nav-next"><a class="flex-next" href="#">Next</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section> --}}




<section class="vc_row section-wrapper vc_testi vc_row-has-fill">
    <div class="tt-overlay" style="background-color: rgba(0, 0, 0, 0.5);"></div>
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12" style="background: transparent!important;">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="testimonial-carousel-wrapper">
                            <div class="client-slider-v2 carousel slide testimonial-carousel-66500" data-ride="carousel">
                                <div class="carousel-inner text-center">
                                    @php
                                        $x = 0;
                                    @endphp
                                    @foreach ($list as $item)
                                    <div class="item @if($x == 0)active @endif">
                                        <blockquote>
                                            <div class="row">
                                                <div class="col-sm-8 col-sm-offset-2 testimonial-content" style="">
                                                    <div class="quote-content" style="">
                                                        <p>
                                                            {!!$item->isi_m_testimoni!!}
                                                        </p>
                                                    </div>
                                                    <span class="author"> <span style="color: #ffffff;"> {{$item->nm_pengirim}} </span> </span>
                                                </div>
                                            </div>
                                        </blockquote>
                                    </div>
                                    @php
                                        $x++;
                                    @endphp

                                    @endforeach
                                </div>
                                <br>
                                <br>
                                <a data-slide="prev" href=".testimonial-carousel-66500" class="left carousel-control"><i class="fa fa-chevron-left"></i></a>
                                <a data-slide="next" href=".testimonial-carousel-66500" class="right carousel-control"><i class="fa fa-chevron-right"></i></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>