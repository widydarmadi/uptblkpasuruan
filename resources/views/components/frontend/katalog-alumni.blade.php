<section class="vc_row section-wrapper vc_custom_1461531052706" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12">
                {{-- <div class="vc_column-inner"> --}}
                    <div class="wpb_wrapper">
                        <div class="sction-title-wrapper text-left vc_custom_1461534674457">
                            <h2 style="" class="section-title theme-color">Katalog Alumni</h2>
                            {{-- <div class="sub-title" style="">
                                <p>
                                    Messenger bag gentrify pitchfork tattooed craft beer iphone skateboard locavore carles etsy viny Photo booth beard raw denim letterpress vegan messenger bag stumptown. American apparel have a terry
                                    richardson vinyl chambray.
                                </p>
                            </div> --}}
                        </div>
                        <div class="portfolio-container">
                            <div class="tt-filter-wrap filter-transparent text-left default-color">
                                <ul class="tt-filter list-inline">
                                    <li><button class="active" data-group="all">Semua</button></li>
                                    @php
                                        $list_alumni = \App\Models\M_kategori_katalog_alumni::where('aktif_m_kategori_katalog_alumni', '1')->take(15)->orderByDesc('created_at')->get();
                                    @endphp
                                    @foreach ($list_alumni as $item)
                                        <li><button data-group="{{$item->id_m_kategori_katalog_alumni}}">{{$item->nm_m_kategori_katalog_alumni}}</button></li>
                                    @endforeach
                                </ul>
                            </div>
                            <div class="row">
                                <div class="tt-grid shuffle" style="position: relative; height: 879px; transition: height 250ms ease-out 0s;">
                                    @php
                                        $list_alumni = \App\Models\M_katalog_alumni::where('aktif_m_katalog_alumni', '1')->orderByDesc('created_at')->with('m_kategori_katalog_alumni')->take(15)->get();
                                    @endphp
                                    @foreach ($list_alumni as $item)
                                    <div
                                        class="tt-item portfolio-item col-md-4 col-sm-6 col-xs-12 shuffle-item filtered"
                                        data-groups='["{{$item->id_m_kategori_katalog_alumni}}"]'
                                        style="position: absolute; top: 0px; left: 0px; transform: translate3d(0px, 0px, 0px) scale3d(1, 1, 1); opacity: 1; transition: transform 250ms ease-out 0s, opacity 250ms ease-out 0s;"
                                    >
                                        <div class="portfolio hover-two">
                                            <div class="tt-overlay-theme-color"></div>
                                            <img
                                                src="{{asset('storage'.'/'.$item->photo)}}"
                                                class="img-responsive wp-post-image"
                                                alt="{{$item->nm_m_kategori_alumni}}"
                                                width="800"
                                                height="600"
                                            />
                                            <div class="portfolio-info">
                                                <h3 class="project-title"><a target="_blank" href="https://instagram.com/{{str_replace('@','',$item->instagram)}}">{{$item->nm_m_katalog_alumni}}</a></h3>
                                                <div class="project-meta"><a target="_blank" href="https://instagram.com/{{str_replace('@','',$item->instagram)}}" class="btn btn-outline">{{$item->instagram}}</a></div>
                                            </div>
                                            <ul class="portfolio-external-link">
                                                <li>
                                                    <a class="tt-lightbox" href="{{asset('storage'.'/'.$item->photo)}}"><i class="fa fa-search"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    @endforeach
                                    
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-12">
                                    <div class="text-center">
                                        <a href="{{route('katalog_alumni')}}" class="btn btn-primary">Selengkapnya</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {{-- </div> --}}
            </div>
        </div>
    </div>
</section>
