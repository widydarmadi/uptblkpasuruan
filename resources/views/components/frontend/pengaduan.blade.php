<section class="vc_row section-wrapper vc_custom_1461778734831 vc_row-has-fill" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_text_column wpb_content_element vc_custom_1459910992473">
                          <p></p>
                          <p></p>
                          <p></p>
                          <h2 class="text-center">Form Pengaduan</h2>
                          <div class="alert alert-info">
                              <p><strong>Pengisian Formulir pengaduan ini bertujuan sebagai upaya peningkatan kualitas pelayanan kami. 
                                Silahkan laporkan sesuai dengan kondisi yang sebenar-benarnya. Privasi data pelapor akan sangat terjaga. Terimakasih</strong></p>
                          </div>
                            <div class="wpb_wrapper">
                                <form method="post" id="form">
                                    {{-- <div class="form-group">
                                      <label class="control-label" for="nm_m_pengaduan">Nama Lengkap</label> 
                                      <input id="nm_m_pengaduan" name="nm_m_pengaduan" type="text" class="form-control">
                                    </div> --}}
                                    
                                    <div class="form-group">
                                      <label for="email_m_pengaduan" class="control-label">Email yang aktif</label> 
                                      <input id="email_m_pengaduan" name="email_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="email_m_pengaduan" class="control-label">Apakah anda bersedia untuk mengungkapkan identitas ? <span style="color:red">*</span> </label> 
                                      <div class="form-check">
                                        <label>
                                          <input type="radio" name="is_bersedia" value="1" > <span class="label-text">Ya</span>
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <label>
                                          <input type="radio" name="is_bersedia" required value="0"> <span class="label-text">Tidak</span>
                                        </label>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                      <label for="status_pelapor" class="control-label">Status Anda Sebagai Pelapor ? <span style="color:red">*</span> </label> 
                                      <select name="status_pelapor" class="form-control" id="status_pelapor">
                                        <option value="">PILIH</option>
                                        <option value="MASYARAKAT">MASYARAKAT</option>
                                        <option value="PESERTA PELATIHAN">Peserta Pelatihan</option>
                                        <option value="PEGAWAI UPT BLK PASURUAN">Pegawai UPT BLK Pasuruan</option>
                                      </select>
                                    </div>
                                    <div class="form-group">
                                      <label for="is_terlapor" class="control-label">Apakah terlapor pernah dilaporkan sebelumnya ? <span style="color:red">*</span> </label> 
                                      <div class="form-check">
                                        <label>
                                          <input type="radio" name="is_terlapor" value="1" > <span class="label-text">Ya</span>
                                        </label>
                                      </div>
                                      <div class="form-check">
                                        <label>
                                          <input type="radio" name="is_terlapor" required value="0"> <span class="label-text">Tidak</span>
                                        </label>
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="email_korespondensi_m_pengaduan" class="control-label">Masukkan email yang akan digunakan untuk korespondensi dengan kami</label> 
                                        <input id="email_korespondensi_m_pengaduan" name="email_korespondensi_m_pengaduan" type="text" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="id_m_permasalahan" class="control-label">Permasalahan yang diadukan ? <span style="color:red">*</span> </label> 
                                        <select name="id_m_permasalahan" class="form-control" id="id_m_permasalahan">
                                          <option value="">PILIH</option>
                                          @php
                                              $permasalahan = \App\Models\M_permasalahan::get();
                                          @endphp
                                          @foreach ($permasalahan as $item)
                                          <option value="{{$item->id_m_permasalahan}}">{{$item->nm_m_permasalahan}}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                    </div>
                                      
                                    <div class="form-group">
                                      <label for="jml_kerugian_m_pengaduan" class="control-label">Jumlah Kerugian <span style="color:red">*</span></label> 
                                      <input id="jml_kerugian_m_pengaduan" name="jml_kerugian_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="terlibat_m_pengaduan" class="control-label">Pihak yang Terlibat <span style="color:red">*</span></label> 
                                      <input id="terlibat_m_pengaduan" name="terlibat_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="lokasi_m_pengaduan" class="control-label">Lokasi Kejadian <span style="color:red">*</span></label> 
                                      <input id="lokasi_m_pengaduan" name="lokasi_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    <div class="form-group row">
                                      <div class="col-md-4">
                                        <label for="waktu_m_pengaduan" class="control-label">Waktu Kejadian <span style="color:red">*</span></label> 
                                        <input id="waktu_m_pengaduan" name="waktu_m_pengaduan" type="date" class="form-control">
                                      </div>
                                    </div>
                                    
                                    <div class="form-group">
                                      <label for="isi_m_pengaduan" class="control-label">Kronologis Kejadian <span style="color:red">*</span></label> 
                                      <textarea style="height: 150px!important;" rows="4" class="form-control" name="isi_m_pengaduan" id="isi_m_pengaduan"></textarea>
                                    </div>
                                    <div class="form-group">
                                      <label for="ket_m_pengaduan" class="control-label">Keterangan Tambahan</label> 
                                      <input id="ket_m_pengaduan" name="ket_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                      <label for="saran_m_pengaduan" class="control-label">Kritik dan Saran</label> 
                                      <input id="saran_m_pengaduan" name="saran_m_pengaduan" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                      <label for="isi_m_pengaduan" class="control-label">Masukkan ulang kode acak yang tertera di bawah ini</label> 
                                      <br>
                                      <div class="captcha">
                                      <span>{!! captcha_img() !!}</span>
                                        <button onclick="reload_captcha();" type="button" class="btn btn-info" style="padding: 7px 15px;" class="reload" id="reload">
                                            &#x21bb;
                                        </button>
                                      </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-md-5">
                                                <input id="captcha" type="text" class="form-control" placeholder="Enter Captcha" name="captcha">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                      <button type="submit" id="submitform" class="btn btn-success"><span>Kirim Pengaduan</span></button>
                                    </div>
                                  </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>