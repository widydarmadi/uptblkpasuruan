<section class="vc_row section-wrapper vc_custom_1460529049797 vc_row-has-fill">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_col-sm-12">
                <div class="vc_column-inner">
                    
                    
                    <div class="wpb_wrapper" style="padding: 15px;">
                        <div class="tt-cta-wrapper vc_custom_1462678141621 button-right">
                            <div class="tt-cta-content">
                                <h2 style="color: #ffffff; font-size: 25px;" class="section-title custom-color">Daftarkan Diri Anda Sekarang Juga</h2>
                                <div class="sub-title" style="font-size: 18px;">
                                    <p><span style="color: #ffffff;">Dapatkan paket pelatihan kerja sesuai dengan keahlian Anda.</span></p>
                                </div>
                            </div>
                            <div class="tt-cta-button" style=""><a class="btn btn-outline btn-md" href="{{url('pendaftaran?cat=PBK')}}" target="">Klik Disini</a></div>
                        </div>
                    </div>


                </div>
            </div>
        </div>
    </div>
</section>
