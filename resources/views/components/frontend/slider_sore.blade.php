<div class="container">
    <div class="row">
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner" style="padding-top: 0px;">
                <div class="wpb_wrapper">
                    <div class="wpb_revslider_element wpb_content_element">
                        <div id="rev_slider_30_1_wrapper" class="rev_slider_wrapper fullscreen-container" style="background-color: #ffffff; padding: 0px;">
                            <div id="rev_slider_30_1" class="rev_slider fullscreenbanner" style="display: none;" data-version="5.2.6">
                                <ul>
                                    @php
                                        $i = 0;
                                        $j = 12;
                                    @endphp
                                    @foreach ($list as $item)

                                    {{-- @if ($i == 0)
                                        @php
                                        $rs = 64;
                                        @endphp
                                    @elseif ($i == 1)
                                        @php
                                        $rs = 60;
                                        @endphp
                                    @endif --}}
                                        
                                    <li
                                        data-index="rs-{{$item->id_m_slider}}"
                                        data-transition="fade"
                                        data-slotamount="7"
                                        data-hideafterloop="0"
                                        data-hideslideonmobile="off"
                                        data-easein="default"
                                        data-easeout="default"
                                        data-masterspeed="1000"
                                        {{-- data-thumb="http://trendytheme.net/demo2/wp/69/multipage/wp-content/uploads/2016/04/office-100x50.jpg" --}}
                                        data-rotate="0"
                                        data-saveperformance="off"
                                        data-title="Slide"
                                        data-param1=""
                                        data-param2=""
                                        data-param3=""
                                        data-param4=""
                                        data-param5=""
                                        data-param6=""
                                        data-param7=""
                                        data-param8=""
                                        data-param9=""
                                        data-param10=""
                                        data-description=""
                                    >
                                        <img
                                            src="{{asset('storage'.'/'.$item->photo)}}"
                                            alt=""
                                            title="office"
                                            width="2100"
                                            height="1400"
                                            data-bgposition="center center"
                                            data-kenburns="on"
                                            data-duration="10000"
                                            data-ease="Linear.easeNone"
                                            data-scalestart="150"
                                            data-scaleend="100"
                                            data-rotatestart="0"
                                            data-rotateend="0"
                                            data-offsetstart="0 0"
                                            data-offsetend="0 0"
                                            class="rev-slidebg"
                                            data-no-retina
                                        />
                                        <div
                                            class="tp-caption tp-shape tp-shapewrapper tp-resizeme"
                                            id="slide-{{$item->id_m_slider}}-layer-{{$j++}}"
                                            data-x="center"
                                            data-hoffset=""
                                            data-y="center"
                                            data-voffset=""
                                            data-width="['full','full','full','full']"
                                            data-height="['full','full','full','full']"
                                            data-transform_idle="o:1;"
                                            data-transform_in="opacity:0;s:1500;e:Power3.easeInOut;"
                                            data-transform_out="opacity:0;s:1000;e:Power4.easeIn;"
                                            data-start="0"
                                            data-basealign="slide"
                                            data-responsive_offset="on"
                                            style="z-index: 5; background-color: rgba(0, 0, 0, 0.6); border-color: rgba(0, 0, 0, 1);"
                                        >
                                        </div>
                                        <span
                                            class="tp-caption tp-resizeme"
                                            id="slide-{{$item->id_m_slider}}-layer-{{$j++}}"
                                            data-x="center"
                                            data-hoffset=""
                                            data-y="center"
                                            data-voffset="-90"
                                            data-width="['auto']"
                                            data-height="['auto']"
                                            data-transform_idle="o:1;"
                                            data-transform_in="y:bottom;s:1000;e:easeOutExpo;"
                                            data-transform_out="y:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                            data-start="1000"
                                            data-splitin="none"
                                            data-splitout="none"
                                            data-responsive_offset="on"
                                            style="z-index: 6; white-space: nowrap; font-size: 40px; line-height: 60px; font-weight: 300; color: rgba(255, 255, 255, 1); text-transform: uppercase;"
                                        >
                                            {{$item->judul_m_slider}}
                                        </span>

                                        <div
                                            class="tp-caption tp-resizeme"
                                            id="slide-{{$item->id_m_slider}}-layer-{{$j++}}"
                                            data-x="center"
                                            data-hoffset=""
                                            data-y="center"
                                            data-voffset=""
                                            data-width="['auto']"
                                            data-height="['auto']"
                                            data-transform_idle="o:1;"
                                            data-transform_in="y:bottom;s:1000;e:easeOutExpo;"
                                            data-transform_out="y:-50px;opacity:0;s:1000;e:easeOutExpo;"
                                            data-start="1200"
                                            data-splitin="none"
                                            data-splitout="none"
                                            data-responsive_offset="on"
                                            style="z-index: 7; white-space: nowrap; font-size: 50px; line-height: 80px; font-weight: 800; color:#ffffff; text-transform: uppercase;"
                                        >
                                        {!!$item->deskripsi_m_slider!!}
                                        </div>
                                       
                                        
                                    </li>

                                    @php
                                        $i++;
                                    @endphp

                                    @endforeach
                                    
                                </ul>
                                <script>
                                    var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                    var htmlDivCss = "";
                                    if (htmlDiv) {
                                        htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                    } else {
                                        var htmlDiv = document.createElement("div");
                                        htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                        document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                    }
                                </script>
                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>
                            </div>
                            <script>
                                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                var htmlDivCss = "";
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script type="text/javascript">
                                /******************************************
    -	PREPARE PLACEHOLDER FOR SLIDER	-
******************************************/

                                var setREVStartSize = function () {
                                    try {
                                        var e = new Object(),
                                            i = jQuery(window).width(),
                                            t = 9999,
                                            r = 0,
                                            n = 0,
                                            l = 0,
                                            f = 0,
                                            s = 0,
                                            h = 0;
                                        e.c = jQuery("#rev_slider_30_1");
                                        e.gridwidth = [1240];
                                        e.gridheight = [868];

                                        e.sliderLayout = "fullscreen";
                                        e.fullScreenAutoWidth = "off";
                                        e.fullScreenAlignForce = "off";
                                        e.fullScreenOffsetContainer = "";
                                        e.fullScreenOffset = "";
                                        if (
                                            (e.responsiveLevels &&
                                                (jQuery.each(e.responsiveLevels, function (e, f) {
                                                    f > i && ((t = r = f), (l = e)), i > f && f > r && ((r = f), (n = e));
                                                }),
                                                t > r && (l = n)),
                                            (f = e.gridheight[l] || e.gridheight[0] || e.gridheight),
                                            (s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth),
                                            (h = i / s),
                                            (h = h > 1 ? 1 : h),
                                            (f = Math.round(h * f)),
                                            "fullscreen" == e.sliderLayout)
                                        ) {
                                            var u = (e.c.width(), jQuery(window).height());
                                            if (void 0 != e.fullScreenOffsetContainer) {
                                                var c = e.fullScreenOffsetContainer.split(",");
                                                if (c)
                                                    jQuery.each(c, function (e, i) {
                                                        u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u;
                                                    }),
                                                        e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0
                                                            ? (u -= (jQuery(window).height() * parseInt(e.fullScreenOffset, 0)) / 100)
                                                            : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0));
                                            }
                                            f = u;
                                        } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                                        e.c.closest(".rev_slider_wrapper").css({ height: f });
                                    } catch (d) {
                                        console.log("Failure at Presize of Slider:" + d);
                                    }
                                };

                                setREVStartSize();

                                var tpj = jQuery;

                                var revapi30;
                                tpj(document).ready(function () {
                                    if (tpj("#rev_slider_30_1").revolution == undefined) {
                                        revslider_showDoubleJqueryError("#rev_slider_30_1");
                                    } else {
                                        revapi30 = tpj("#rev_slider_30_1")
                                            .show()
                                            .revolution({
                                                sliderType: "standard",
                                                jsFileLocation: "//trendytheme.net/demo2/wp/69/multipage/wp-content/plugins/revslider/public/assets/js/",
                                                sliderLayout: "fullscreen",
                                                dottedOverlay: "none",
                                                delay: 7000,
                                                navigation: {
                                                    keyboardNavigation: "on",
                                                    keyboard_direction: "horizontal",
                                                    mouseScrollNavigation: "off",
                                                    mouseScrollReverse: "default",
                                                    onHoverStop: "off",
                                                    arrows: {
                                                        style: "hades",
                                                        enable: true,
                                                        hide_onmobile: false,
                                                        hide_onleave: true,
                                                        hide_delay: 200,
                                                        hide_delay_mobile: 1200,
                                                        tmp: '<div class="tp-arr-allwrapper">	<div class="tp-arr-imgholder"></div></div>',
                                                        left: {
                                                            h_align: "left",
                                                            v_align: "center",
                                                            h_offset: 0,
                                                            v_offset: 0,
                                                        },
                                                        right: {
                                                            h_align: "right",
                                                            v_align: "center",
                                                            h_offset: 0,
                                                            v_offset: 0,
                                                        },
                                                    },
                                                },
                                                visibilityLevels: [1240, 1024, 778, 480],
                                                gridwidth: 1240,
                                                gridheight: 868,
                                                lazyType: "none",
                                                shadow: 0,
                                                spinner: "spinner2",
                                                stopLoop: "off",
                                                stopAfterLoops: -1,
                                                stopAtSlide: -1,
                                                shuffle: "off",
                                                autoHeight: "off",
                                                fullScreenAutoWidth: "off",
                                                fullScreenAlignForce: "off",
                                                fullScreenOffsetContainer: "",
                                                fullScreenOffset: "",
                                                disableProgressBar: "on",
                                                hideThumbsOnMobile: "off",
                                                hideSliderAtLimit: 0,
                                                hideCaptionAtLimit: 0,
                                                hideAllCaptionAtLilmit: 0,
                                                debugMode: false,
                                                fallbacks: {
                                                    simplifyAll: "off",
                                                    nextSlideOnWindowFocus: "off",
                                                    disableFocusListener: false,
                                                },
                                            });
                                    }
                                }); /*ready*/
                            </script>
                            <script>
                                var htmlDivCss = " #rev_slider_30_1_wrapper .tp-loader.spinner2{ background-color: #ff2a40 !important; } ";
                                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                            <script>
                                var htmlDivCss = unescape(
                                    ".hades.tparrows%20%7B%0A%09cursor%3Apointer%3B%0A%09background%3Argba%280%2C0%2C0%2C0.25%29%3B%0A%09width%3A100px%3B%0A%09height%3A100px%3B%0A%09position%3Aabsolute%3B%0A%09display%3Ablock%3B%0A%09z-index%3A100%3B%0A%7D%0A%0A.hades.tparrows%3Abefore%20%7B%0A%09font-family%3A%20%22revicons%22%3B%0A%09font-size%3A30px%3B%0A%09color%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%09display%3Ablock%3B%0A%09line-height%3A%20100px%3B%0A%09text-align%3A%20center%3B%0A%20%20transition%3A%20background%200.3s%2C%20color%200.3s%3B%0A%7D%0A.hades.tparrows.tp-leftarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce824%22%3B%0A%7D%0A.hades.tparrows.tp-rightarrow%3Abefore%20%7B%0A%09content%3A%20%22%5Ce825%22%3B%0A%7D%0A%0A.hades.tparrows%3Ahover%3Abefore%20%7B%0A%20%20%20color%3Argba%280%2C0%2C0%2C0.5%29%3B%0A%20%20%20background%3Argba%28255%2C%20255%2C%20255%2C%201%29%3B%0A%20%7D%0A.hades%20.tp-arr-allwrapper%20%7B%0A%20%20position%3Aabsolute%3B%0A%20%20left%3A100%25%3B%0A%20%20top%3A0px%3B%0A%20%20background%3A%23888%3B%20%0A%20%20width%3A100px%3Bheight%3A100px%3B%0A%20%20-webkit-transition%3A%20all%200.3s%3B%0A%20%20transition%3A%20all%200.3s%3B%0A%20%20-ms-filter%3A%20%22progid%3Adximagetransform.microsoft.alpha%28opacity%3D0%29%22%3B%0A%20%20filter%3A%20alpha%28opacity%3D0%29%3B%0A%20%20-moz-opacity%3A%200.0%3B%0A%20%20-khtml-opacity%3A%200.0%3B%0A%20%20opacity%3A%200.0%3B%0A%20%20-webkit-transform%3A%20rotatey%28-90deg%29%3B%0A%20%20transform%3A%20rotatey%28-90deg%29%3B%0A%20%20-webkit-transform-origin%3A%200%25%2050%25%3B%0A%20%20transform-origin%3A%200%25%2050%25%3B%0A%7D%0A.hades.tp-rightarrow%20.tp-arr-allwrapper%20%7B%0A%20%20%20left%3Aauto%3B%0A%20%20%20right%3A100%25%3B%0A%20%20%20-webkit-transform-origin%3A%20100%25%2050%25%3B%0A%20%20transform-origin%3A%20100%25%2050%25%3B%0A%20%20%20-webkit-transform%3A%20rotatey%2890deg%29%3B%0A%20%20transform%3A%20rotatey%2890deg%29%3B%0A%7D%0A%0A.hades%3Ahover%20.tp-arr-allwrapper%20%7B%0A%20%20%20-ms-filter%3A%20%22progid%3Adximagetransform.microsoft.alpha%28opacity%3D100%29%22%3B%0A%20%20filter%3A%20alpha%28opacity%3D100%29%3B%0A%20%20-moz-opacity%3A%201%3B%0A%20%20-khtml-opacity%3A%201%3B%0A%20%20opacity%3A%201%3B%20%20%0A%20%20%20%20-webkit-transform%3A%20rotatey%280deg%29%3B%0A%20%20transform%3A%20rotatey%280deg%29%3B%0A%0A%20%7D%0A%20%20%20%20%0A.hades%20.tp-arr-iwrapper%20%7B%0A%7D%0A.hades%20.tp-arr-imgholder%20%7B%0A%20%20background-size%3Acover%3B%0A%20%20position%3Aabsolute%3B%0A%20%20top%3A0px%3Bleft%3A0px%3B%0A%20%20width%3A100%25%3Bheight%3A100%25%3B%0A%7D%0A.hades%20.tp-arr-titleholder%20%7B%0A%7D%0A.hades%20.tp-arr-subtitleholder%20%7B%0A%7D%0A%0A"
                                );
                                var htmlDiv = document.getElementById("rs-plugin-settings-inline-css");
                                if (htmlDiv) {
                                    htmlDiv.innerHTML = htmlDiv.innerHTML + htmlDivCss;
                                } else {
                                    var htmlDiv = document.createElement("div");
                                    htmlDiv.innerHTML = "<style>" + htmlDivCss + "</style>";
                                    document.getElementsByTagName("head")[0].appendChild(htmlDiv.childNodes[0]);
                                }
                            </script>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>