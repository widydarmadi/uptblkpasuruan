<table>
    <thead>
    <tr>
        <th>No</th>
        <th width="20">NIK</th>
        <th width="30">No Register</th>
        <th width="50">Nama</th>
        <th width="50">Alamat</th>
        <th width="20">Kota / Kab</th>
        <th width="20">Pendidikan</th>
        <th width="20">Jurusan</th>
        <th width="20">HP / Whatsapp</th>
        <th width="30">Email</th>
        <th width="20">Tempat Lahir</th>
        <th width="17">Tanggal Lahir</th>
        <th width="17">Jenis Kelamin</th>
        <th width="20">Pernah Ikut BLK ?</th>
        <th width="20">Pernah Bekerja</th>
        <th width="20">Minat Stlh Lulus</th>
        <th width="15">Kategori Pelatihan</th>
        <th width="30">Nama Pelatihan</th>
        <th width="20">Penyandang Disabilitas ?</th>
        <th width="30">Jenis Disabilitas</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i = 1;
    @endphp
    @foreach($data as $data)

        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ "'".$data->nik_m_pendaftar }}</td>
            <td>{{ $data->no_register }}</td>
            <td>{{ $data->nm_m_pendaftar }}</td>
            <td>{{ $data->alamat_m_pendaftar }}</td>
            <td>{{ $data->kota->nm_m_kota }}</td>
            <td>{{ $data->pendidikan->nm_m_pendidikan }}</td>
            <td>{{ $data->nm_jurusan }}</td>
            <td>{{ $data->wa_m_pendaftar }}</td>
            <td>{{ $data->email_m_pendaftar }}</td>
            <td>{{ \Str::upper($data->tempat_lahir_m_pendaftar) }}</td>
            <td>{{ \Carbon\Carbon::parse($data->tgl_lahir_m_pendaftar)->format('d-m-Y') }}</td>
            <td>{{ $data->jk_m_pendaftar }}</td>
            <td>{{ $data->quest_pernah_blk }}</td>
            <td>{{ $data->quest_masih_bekerja }}</td>
            <td>{{ $data->quest_minat_setelah_kursus }}</td>
            <td>{{ $data->kategori_m_pendaftar }}</td>
            <td>{{ $data->jadwal->nm_m_jadwal }}</td>
            <td>{{ ($data->is_disabilitas == 'YA') ? 'YA' : 'TIDAK' }}</td>
            <td>{{ ($data->disabilitas) ? $data->disabilitas->nama_disabilitas : '-' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>