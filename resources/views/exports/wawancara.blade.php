<table>
    <thead>
    <tr>
        <th>No</th>
        <th width="20">NIK</th>
        <th width="30">No Register</th>
        <th width="50">Nama</th>
        <th width="50">Alamat</th>
        <th width="20">Kota / Kab</th>
        <th width="20">Pendidikan</th>
        <th width="20">Jurusan</th>
        <th width="20">HP / Whatsapp</th>
        <th width="30">Email</th>
        <th width="20">Tempat Lahir</th>
        <th width="17">Tanggal Lahir</th>
        <th width="17">Jenis Kelamin</th>
        <th width="20">Pernah Ikut BLK ?</th>
        <th width="20">Pernah Bekerja</th>
        <th width="20">Minat Stlh Lulus</th>
        <th width="15">Kategori Pelatihan</th>
        <th width="30">Nama Pelatihan</th>
        <th width="20">Penyandang Disabilitas ?</th>
        <th width="30">Jenis Disabilitas</th>
        <th width="15">User CBT</th>
        <th width="15">Pass CBT</th>
        <th width="12">Nilai CBT</th>
        <th width="12">Nilai Wawancara</th>
        <th width="15">Nilai Total</th>
        <th width="15">Status</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i = 1;
    @endphp
    @foreach($wawancara as $wawancara)

    @php
        $expl = explode('-',$wawancara->no_register);
        $tahun_daftar = $expl[1];
        $tanggal_daftar = $expl[3];
        $tipe_daftar = $expl[4];
        $urut_daftar = $expl[5];

        $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
        $cbt_user = \App\Models\Cbt_user::select('user_id','user_name', 'user_password')
                    ->where('user_name', $username)
                    ->with('cbt_tes_user', function($q){
                        $q->with('cbt_tes_soal:tessoal_tesuser_id,tessoal_tesuser_id,tessoal_nilai');
                    })->first();
        if($cbt_user and $cbt_user->cbt_tes_user and $cbt_user->cbt_tes_user->cbt_tes_soal){
            $cbt_score = $cbt_user->cbt_tes_user->cbt_tes_soal->sum('tessoal_nilai');
        }else{
            $cbt_score = '<span class="text-danger">belum ada</span>';
        }

                    $cek_seleksi = \App\Models\T_seleksi::select('id_t_seleksi', 'status')
                                    ->where('id_m_pendaftar', $wawancara->id_m_pendaftar);
    @endphp
        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ "'".$wawancara->nik_m_pendaftar }}</td>
            <td>{{ $wawancara->no_register }}</td>
            <td>{{ $wawancara->nm_m_pendaftar }}</td>
            <td>{{ $wawancara->alamat_m_pendaftar }}</td>
            <td>{{ $wawancara->kota->nm_m_kota }}</td>
            <td>{{ $wawancara->pendidikan->nm_m_pendidikan }}</td>
            <td>{{ $wawancara->nm_jurusan }}</td>
            <td>{{ $wawancara->wa_m_pendaftar }}</td>
            <td>{{ $wawancara->email_m_pendaftar }}</td>
            <td>{{ \Str::upper($wawancara->tempat_lahir_m_pendaftar) }}</td>
            <td>{{ \Carbon\Carbon::parse($wawancara->tgl_lahir_m_pendaftar)->format('d-m-Y') }}</td>
            <td>{{ $wawancara->jk_m_pendaftar }}</td>
            <td>{{ $wawancara->quest_pernah_blk }}</td>
            <td>{{ $wawancara->quest_masih_bekerja }}</td>
            <td>{{ $wawancara->quest_minat_setelah_kursus }}</td>
            <td>{{ $wawancara->kategori_m_pendaftar }}</td>
            <td>{{ $wawancara->jadwal->nm_m_jadwal }}</td>
            <td>{{ ($wawancara->is_disabilitas == 'YA') ? 'YA' : 'TIDAK' }}</td>
            <td>{{ ($wawancara->disabilitas) ? $wawancara->disabilitas->nama_disabilitas : '-' }}</td>
            <td>{{ ($cbt_user) ? $cbt_user->user_name : '-' }}</td>
            <td>{{ ($cbt_user) ? $cbt_user->user_password : '-' }}</td>
            <td>{{ ($cbt_score) ? $cbt_score : 'belum ada' }}</td>
            <td>{{ ($cek_seleksi->first()) ? $wawancara->seleksi->total_nilai_wawancara : 'belum ada' }}</td>
            <td>{{ ($cek_seleksi->first()) ? $wawancara->seleksi->total_nilai_keseluruhan : 'belum ada' }}</td>
            <td>{{ ($cek_seleksi->first()) ? $cek_seleksi->first()->status : '' }}</td>
        </tr>
    @endforeach
    </tbody>
</table>