<table>
    <thead>
    <tr>
        <th>No</th>
        <th width="30">No Register</th>
        <th width="40">Nama Pemohon</th>
        <th width="20">Jabatan Pemohon</th>
        <th width="20">HP / Whatsapp</th>
        <th width="30">Email</th>
        <th width="30">Nama Lokasi MTU</th>
        <th width="15">Telp Lokasi MTU</th>
        <th width="15">Email MTU</th>
        <th width="40">Alamat Lokasi MTU</th>
        <th width="15">Kelurahan</th>
        <th width="15">Kecamatan</th>
        <th width="15">Kota / Kab</th>
        <th width="15">Provinsi</th>
        <th width="50">Potensi Wilayah</th>
        <th width="50">Jadwal yang diikuti</th>
        <th width="13">Status Progres</th>
        <th width="16">Kategori Pendaftar</th>
    </tr>
    </thead>
    <tbody>
    @php
        $i = 1;
    @endphp
    @foreach($data as $data)

        <tr>
            <td>{{ $i++ }}</td>
            <td>{{ $data->no_register }}</td>
            <td>{{ $data->nm_pemohon }}</td>
            <td>{{ $data->jabatan_pemohon }}</td>
            <td>{{ $data->wa_pemohon }}</td>
            <td>{{ $data->email_pemohon }}</td>
            <td>{{ $data->nm_lokasi_mtu }}</td>
            <td>{{ $data->telp_lokasi_mtu }}</td>
            <td>{{ $data->email_mtu }}</td>
            <td>{{ $data->alamat_lokasi_mtu }}</td>
            <td>{{ $data->kelurahan->nm_m_kelurahan }}</td>
            <td>{{ $data->kecamatan->nm_m_kecamatan }}</td>
            <td>{{ $data->kota->nm_m_kota }}</td>
            <td>{{ $data->provinsi->nm_m_provinsi }}</td>
            <td>{{ $data->potensi_wilayah }}</td>
            <td>{{ $data->jadwal->nm_m_jadwal }}</td>
            <td>{{ $data->progress }}</td>
            <td>{{ $data->kategori_m_pendaftar }}</td>
        </tr>
    @endforeach
    </tbody>
</table>