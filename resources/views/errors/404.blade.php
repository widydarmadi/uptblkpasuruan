@extends('front.template.index_detail')

@section('content')
<link rel="stylesheet" href="https://rawgit.com/LeshikJanz/libraries/master/Bootstrap/baguetteBox.min.css">
@php
    $bg = \App\Models\M_bg::where('slug_m_bg', 'galeri')->first();
@endphp
<section class="page-title text-center" 
@if(isset($bg->photo) and $bg->aktif_m_bg == '1') style="background-image: url({{asset('storage'.'/'.$bg->photo)}});" 
@else style="background-image: url({{asset('assets/front/images/1647919128_bg-blk.jpg')}});"
@endif
role="banner">
<div class="title-overlay-color"></div>
    <div class="container">
        <h1 style="">Halaman tidak ditemukan</h1>
        <div class="tt-breadcrumb" style="">
            <ul class="breadcrumb">
                <li><a href="#">Halaman tidak ditemukan</a></li>
            </ul>
        </div>
    </div>
</section>


  <section class="vc_row section-wrapper vc_custom_1461778734831 vc_row-has-fill" style="margin-bottom: 100px;">
    <div class="container">
        <div class="row">
            <div class="wpb_column vc_column_container vc_col-sm-12 vc_col-md-12">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <p>Mohon maaf, halaman yang Anda cari tidak ditemukan atau masih belum tersedia ! Kembali ke <a href="{{url('/')}}">halaman utama</a></p>
                        <br>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>
  
@endsection