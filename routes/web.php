<?php

use App\Http\Controllers\Back\Auth;
use App\Http\Controllers\Front\Captcha;
use App\Http\Controllers\Front\Combo;
use Illuminate\Support\Facades\Route;

//pass admin : 162534
//////*********************************************************FRONT END ROUTES****************************************************** */
//////*************************************************************************************************************************** */

Route::get('/', [\App\Http\Controllers\Front\Home::class, 'index'])->name('index');
Route::get('/konten/{slug}', [\App\Http\Controllers\Front\Konten::class, 'konten'])->name('konten');
Route::get('/profil-pegawai', [\App\Http\Controllers\Front\Profil_pegawai::class, 'profil_pegawai'])->name('profil_pegawai');
Route::get('/hasil-seleksi', [\App\Http\Controllers\Front\Hasil_seleksi::class, 'index'])->name('hasil_seleksi');
Route::get('/hasil-seleksi/{id}', [\App\Http\Controllers\Front\Hasil_seleksi::class, 'detail'])->name('hasil_seleksi_detail');
// Route::get('/profil-pegawai/{slug}', [\App\Http\Controllers\Front\Profil_pegawai::class, 'profil_pegawai'])->name('profil_pegawai');
Route::get('/penempatan', [\App\Http\Controllers\Front\Penempatan::class, 'penempatan'])->name('penempatan');
Route::get('/faq', [\App\Http\Controllers\Front\Faq::class, 'faq'])->name('faq');
Route::get('/pendaftaran', [\App\Http\Controllers\Front\Pendaftaran::class, 'index'])->name('pendaftaran');
Route::post('/pendaftaran', [\App\Http\Controllers\Front\Pendaftaran::class, 'index_post'])->name('pendaftaran_post');
Route::get('/pendaftaran-mtu', [\App\Http\Controllers\Front\Pendaftaran_mtu::class, 'index'])->name('pendaftaran_mtu');
Route::post('/pendaftaran-mtu', [\App\Http\Controllers\Front\Pendaftaran_mtu::class, 'index_post'])->name('pendaftaran_mtu_post');
Route::get('/pendaftaran-pihak-ketiga', [\App\Http\Controllers\Front\Pendaftaran_pihak_ketiga::class, 'index'])->name('pendaftaran_pihak_ketiga');
Route::post('/pendaftaran-pihak-ketiga', [\App\Http\Controllers\Front\Pendaftaran_pihak_ketiga::class, 'index_post'])->name('pendaftaran_pihak_ketiga_post');
Route::get('/katalog-alumni', [\App\Http\Controllers\Front\Katalog::class, 'index'])->name('katalog_alumni');
Route::post('/api/login_as_alumni', [\App\Http\Controllers\Front\Api::class, 'login_as_alumni'])->name('login_as_alumni');
// Route::get('/katalog-alumni/produk', [\App\Http\Controllers\Front\Katalog::class, 'katalog_produk'])->name('katalog_produk');
// Route::get('/katalog-alumni/produk/{slug}', [\App\Http\Controllers\Front\Katalog::class, 'katalog_produk_detail'])->name('katalog_produk_detail');
// Route::get('/katalog-alumni/jasa', [\App\Http\Controllers\Front\Katalog::class, 'katalog_jasa'])->name('katalog_jasa');
// Route::get('/katalog-alumni/jasa/{slug}', [\App\Http\Controllers\Front\Katalog::class, 'katalog_jasa_detail'])->name('katalog_jasa_detail');

Route::get('/publikasi/pengumuman', [\App\Http\Controllers\Front\Pengumuman::class, 'index'])->name('pengumuman_index');
Route::get('/publikasi/pengumuman/{id}', [\App\Http\Controllers\Front\Pengumuman::class, 'pengumuman_detail'])->name('pengumuman_detail');
// Route::get('/publikasi/pengumuman/{slug}', [\App\Http\Controllers\Front\Publikasi::class, 'pengumuman_detail'])->name('pengumuman_detail');
Route::get('/program-kegiatan', [\App\Http\Controllers\Front\Program_kegiatan::class, 'program_kegiatan'])->name('program_kegiatan');
Route::get('/program-kegiatan/{slug}', [\App\Http\Controllers\Front\Program_kegiatan::class, 'program_kegiatan_detail'])->name('program_kegiatan_detail');
Route::get('/info-lowongan', [\App\Http\Controllers\Front\Info_lowongan::class, 'info_lowongan'])->name('info_lowongan');
Route::get('/info-lowongan/detail/{id}', [\App\Http\Controllers\Front\Info_lowongan::class, 'detail'])->name('info_lowongan_detail');
Route::get('/galeri', [\App\Http\Controllers\Front\Galeri::class, 'index'])->name('galeri');
Route::get('/galeri/detail/{id}', [\App\Http\Controllers\Front\Galeri::class, 'detail'])->name('galeri_detail');
Route::get('/agenda', [\App\Http\Controllers\Front\Agenda::class, 'index'])->name('agenda');
Route::get('/agenda/{slug}', [\App\Http\Controllers\Front\Agenda::class, 'detail'])->name('agenda_detail');
// Route::get('/agenda/{id}', [\App\Http\Controllers\Front\Publikasi::class, 'agenda_detail'])->name('agenda_detail');
Route::get('/pengaduan', [\App\Http\Controllers\Front\Pengaduan::class, 'pengaduan'])->name('pengaduan');
Route::post('/pengaduan_post', [\App\Http\Controllers\Front\Pengaduan::class, 'pengaduan_post'])->name('pengaduan_post');
Route::get('/reload-captcha', [Captcha::class, 'reloadCaptcha'])->name('reload_captcha');
Route::post('/get_combo_kota', [Combo::class, 'get_combo_kota'])->name('get_combo_kota');
Route::post('/get_combo_kecamatan', [Combo::class, 'get_combo_kecamatan'])->name('get_combo_kecamatan');
Route::post('/get_combo_kelurahan', [Combo::class, 'get_combo_kelurahan'])->name('get_combo_kelurahan');
Route::post('/get_combo_kejuruan', [Combo::class, 'get_combo_kejuruan'])->name('get_combo_kejuruan');
Route::post('/get_jenis_disabilitas', [Combo::class, 'get_jenis_disabilitas'])->name('get_jenis_disabilitas');
Route::post('/get_pelatihan_dibuka', [Combo::class, 'get_pelatihan_dibuka'])->name('get_pelatihan_dibuka');
Route::post('/get_gelombang_dibuka', [Combo::class, 'get_gelombang_dibuka'])->name('get_gelombang_dibuka');
Route::post('/get_cbt_grup_id', [Combo::class, 'get_cbt_grup_id'])->name('get_cbt_grup_id');

Route::get('/kejuruan', [\App\Http\Controllers\Front\Kejuruan::class, 'index'])->name('kejuruan');
Route::get('/tipe-pelatihan', [\App\Http\Controllers\Front\Tipe_pelatihan::class, 'index'])->name('tipe_pelatihan');
Route::get('/program-pelatihan', [\App\Http\Controllers\Front\Program_pelatihan::class, 'home'])->name('program_pelatihan_home');
Route::get('/program-pelatihan/{id}', [\App\Http\Controllers\Front\Program_pelatihan::class, 'index'])->name('program_pelatihan');
Route::get('/program-pelatihan-detil/{id}', [\App\Http\Controllers\Front\Program_pelatihan::class, 'detail'])->name('program_pelatihan_detil');
Route::get('/jadwal', [\App\Http\Controllers\Front\Jadwal::class, 'index'])->name('jadwal');
Route::get('/send_email_pendaftaran', [\App\Http\Controllers\Front\Email::class, 'send_email_pendaftaran'])->name('send_email_pendaftaran');

Route::get('/alumni/login', [\App\Http\Controllers\Front\Alumni::class, 'alumni_login'])->name('alumni_login');
Route::post('/alumni/login', [\App\Http\Controllers\Front\Alumni::class, 'alumni_login_post'])->name('alumni_login_post');
Route::get('/alumni/daftar', [\App\Http\Controllers\Front\Alumni::class, 'alumni_daftar'])->name('alumni_daftar');
Route::post('/alumni/daftar', [\App\Http\Controllers\Front\Alumni::class, 'alumni_daftar_post'])->name('alumni_daftar_post');
Route::post('/alumni/get_nik', [\App\Http\Controllers\Front\Alumni::class, 'alumni_get_nik'])->name('alumni_get_nik');
Route::get('/aktivasi_alumni', [\App\Http\Controllers\Front\Alumni::class, 'aktivasi_alumni'])->name('aktivasi_alumni');
Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'load_jadwal_available'])->name('load_jadwal_available');



Route::group([
    'prefix' => 'alumni_page',
    'as' => 'alumni_page.',
    'middleware' => ['ceklogin_alumni'],
], function (){
    
    Route::get('/', [\App\Http\Controllers\Front\Alumni::class, 'alumni_dashboard'])->name('dashboard');
    Route::get('/edit-profil', [\App\Http\Controllers\Front\Alumni::class, 'edit_profil'])->name('edit_profil');
    Route::post('/edit-profil', [\App\Http\Controllers\Front\Alumni::class, 'edit_profil_post'])->name('edit_profil_post');
    Route::get('/dashboard', [\App\Http\Controllers\Front\Alumni::class, 'alumni_dashboard'])->name('dashboard');
    Route::get('/logout', [\App\Http\Controllers\Front\Alumni::class, 'logout'])->name('logout');
    Route::get('/reset_password', [\App\Http\Controllers\Front\Alumni::class, 'edit_profil'])->name('edit_profil');
    Route::post('/reset_password', [\App\Http\Controllers\Front\Alumni::class, 'edit_profil_post'])->name('edit_profil_post');
    
    Route::group([
        'prefix' => 'pekerjaan',
        'as' => 'pekerjaan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Front\Alumni_pekerjaan::class, 'index'])->name('index');
        Route::post('/datatable', [\App\Http\Controllers\Front\Alumni_pekerjaan::class, 'datatable'])->name('datatable');
        Route::post('/add', [\App\Http\Controllers\Front\Alumni_pekerjaan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Front\Alumni_pekerjaan::class, 'save'])->name('save');
        Route::post('/delete', [\App\Http\Controllers\Front\Alumni_pekerjaan::class, 'delete'])->name('delete');
    });
    
    Route::group([
        'prefix' => 'usaha',
        'as' => 'usaha.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Front\Alumni_usaha::class, 'index'])->name('index');
        Route::post('/datatable', [\App\Http\Controllers\Front\Alumni_usaha::class, 'datatable'])->name('datatable');
        Route::post('/add', [\App\Http\Controllers\Front\Alumni_usaha::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Front\Alumni_usaha::class, 'save'])->name('save');
        Route::post('/delete', [\App\Http\Controllers\Front\Alumni_usaha::class, 'delete'])->name('delete');
    });

});


//////*********************************************************BACK END ROUTES****************************************************** */
//////*************************************************************************************************************************** */




// Route::get('/daftar', [\App\Http\Controllers\Front\Home::class, 'daftar'])->name('daftar');
// Route::post('/daftar_post', [\App\Http\Controllers\Front\Home::class, 'daftar_post'])->name('daftar_post');
Route::get('/admin/login', [Auth::class, 'login'])->name('login');
Route::post('/admin/authenticate', [Auth::class, 'authenticate'])->name('authenticate');
Route::group([
    'prefix' => 'admin',
    'as' => 'admin.',
    'middleware' => ['ceklogin_bo'],
], function (){

    Route::get('/', [\App\Http\Controllers\Back\Main::class, 'index'])->name('index');
    Route::get('/main', [\App\Http\Controllers\Back\Main::class, 'index'])->name('main');
    Route::get('/edit_profile', [\App\Http\Controllers\Back\Main::class, 'edit_profile'])->name('edit_profile');
    Route::post('/edit_profile', [\App\Http\Controllers\Back\Main::class, 'edit_profile_post'])->name('edit_profile_post');
    Route::get('/logout', [Auth::class, 'logout'])->name('logout');

    /**
     * master m_alumni
     */

    Route::group([
        'prefix' => 'm_alumni',
        'as' => 'm_alumni.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_alumni::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_alumni::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_alumni::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_alumni::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_alumni::class, 'update'])->name('update');
        Route::get('/edit_pekerjaan', [\App\Http\Controllers\Back\Master_alumni::class, 'edit_pekerjaan'])->name('edit_pekerjaan');
        Route::post('/add_pekerjaan', [\App\Http\Controllers\Back\Master_alumni::class, 'add_pekerjaan'])->name('add_pekerjaan');
        Route::post('/add_pekerjaan_post', [\App\Http\Controllers\Back\Master_alumni::class, 'add_pekerjaan_post'])->name('add_pekerjaan_post');
        Route::post('/delete_pekerjaan', [\App\Http\Controllers\Back\Master_alumni::class, 'delete_pekerjaan'])->name('delete_pekerjaan');
        Route::get('/view', [\App\Http\Controllers\Back\Master_alumni::class, 'view'])->name('view');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_alumni::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_alumni::class, 'datatable'])->name('datatable');
        Route::post('/datatable_pekerjaan', [\App\Http\Controllers\Back\Master_alumni::class, 'datatable_pekerjaan'])->name('datatable_pekerjaan');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Master_alumni::class, 'export_xls'])->name('export_xls');
    });
    
    /**
     * t_ver_katalog_alumni
     */

    Route::group([
        'prefix' => 't_ver_katalog_alumni',
        'as' => 't_ver_katalog_alumni.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'index'])->name('index');
        Route::get('/view', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'view'])->name('view');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'datatable'])->name('datatable');
        Route::post('/reset_foto', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'reset_foto'])->name('reset_foto');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Transaksi_ver_katalog_alumni::class, 'export_xls'])->name('export_xls');
    });

    /**
     * master m_pendaftar
     */

    Route::group([
        'prefix' => 'm_pendaftar',
        'as' => 'm_pendaftar.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_pendaftar::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_pendaftar::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_pendaftar::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_pendaftar::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_pendaftar::class, 'update'])->name('update');
        Route::get('/view', [\App\Http\Controllers\Back\Master_pendaftar::class, 'view'])->name('view');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_pendaftar::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_pendaftar::class, 'datatable'])->name('datatable');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Master_pendaftar::class, 'export_xls'])->name('export_xls');
    });

    /**
     * master m_pendaftar_mtu
     */

    Route::group([
        'prefix' => 'm_pendaftar_mtu',
        'as' => 'm_pendaftar_mtu.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'update'])->name('update');
        Route::get('/peserta', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'peserta'])->name('peserta');
        Route::get('/lihat_peserta', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'lihat_peserta'])->name('lihat_peserta');
        Route::post('/peserta', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'peserta_post'])->name('peserta_post');
        Route::post('/datatable_siswa', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'datatable_siswa'])->name('datatable_siswa');
        Route::post('/delete_peserta', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'delete_peserta'])->name('delete_peserta');
        Route::get('/view', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'view'])->name('view');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'datatable'])->name('datatable');
        Route::post('/load_program_available', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'load_program_available'])->name('load_program_available');
        Route::post('/load_kejuruan_available', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'load_kejuruan_available'])->name('load_kejuruan_available');
        Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'load_jadwal_available'])->name('load_jadwal_available');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Master_pendaftar_mtu::class, 'export_xls'])->name('export_xls');
    });
    
    
    /**
     * master m_pendaftar_pihak_ketiga
     */

    Route::group([
        'prefix' => 'm_pendaftar_swadaya',
        'as' => 'm_pendaftar_swadaya.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'update'])->name('update');
        Route::get('/peserta', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'peserta'])->name('peserta');
        Route::get('/lihat_peserta', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'lihat_peserta'])->name('lihat_peserta');
        Route::post('/peserta', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'peserta_post'])->name('peserta_post');
        Route::post('/datatable_siswa', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'datatable_siswa'])->name('datatable_siswa');
        Route::post('/delete_peserta', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'delete_peserta'])->name('delete_peserta');
        Route::get('/view', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'view'])->name('view');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'datatable'])->name('datatable');
        Route::post('/load_program_available', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'load_program_available'])->name('load_program_available');
        Route::post('/load_kejuruan_available', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'load_kejuruan_available'])->name('load_kejuruan_available');
        Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'load_jadwal_available'])->name('load_jadwal_available');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Master_pendaftar_swadaya::class, 'export_xls'])->name('export_xls');
    });

    /**
     * master m_user_bo
     */

    Route::group([
        'prefix' => 'm_user_bo',
        'as' => 'm_user_bo.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_user_bo::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_user_bo::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_user_bo::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_user_bo::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_user_bo::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_user_bo::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_user_bo::class, 'datatable'])->name('datatable');
    });



    /**
     * master user group
     */
    Route::group([
        'prefix' => 'm_user_group',
        'as' => 'm_user_group.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_user_group::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_user_group::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_user_group::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_user_group::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_user_group::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_user_group::class, 'delete'])->name('delete');
        Route::get('/hakakses', [\App\Http\Controllers\Back\Master_user_group::class, 'hakakses'])->name('hakakses');
        // Route::post('/hakakses', [\App\Http\Controllers\Back\Master_user_group::class, 'hakakses_update'])->name('hakakses_update');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_user_group::class, 'datatable'])->name('datatable');
        Route::get('/manage', [\App\Http\Controllers\Back\Master_user_group::class, 'manage'])->name('manage');
        Route::post('/manage', [\App\Http\Controllers\Back\Master_user_group::class, 'manage_post'])->name('manage_post');
    });


    /**
     * master Back menu
     */
    Route::group([
        'prefix' => 'm_menu',
        'as' => 'm_menu.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_menu::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_menu::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_menu::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_menu::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_menu::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_menu::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_menu::class, 'datatable'])->name('datatable');
    });





    /**
     * master welcome
     */
    Route::group([
        'prefix' => 'm_welcome',
        'as' => 'm_welcome.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_welcome::class, 'index'])->name('index');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_welcome::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_welcome::class, 'update'])->name('update');
    });


    /**
     * master galeri
     */
    Route::group([
        'prefix' => 'm_galeri',
        'as' => 'm_galeri.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_galeri::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_galeri::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_galeri::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_galeri::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_galeri::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_galeri::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_galeri::class, 'datatable'])->name('datatable');
        Route::get('/upload', [\App\Http\Controllers\Back\Master_galeri::class, 'upload'])->name('upload');
        Route::post('/upload', [\App\Http\Controllers\Back\Master_galeri::class, 'upload_post'])->name('upload_post');
        Route::post('/hapus_foto', [\App\Http\Controllers\Back\Master_galeri::class, 'hapus_foto'])->name('hapus_foto');
    });
    
    /**
     * master soal_wawancara
     */
    Route::group([
        'prefix' => 'm_soal_wawancara',
        'as' => 'm_soal_wawancara.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_soal_wawancara::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master agenda
     */
    Route::group([
        'prefix' => 'm_agenda',
        'as' => 'm_agenda.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_agenda::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_agenda::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_agenda::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_agenda::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_agenda::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_agenda::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_agenda::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master footer
     */
    Route::group([
        'prefix' => 'm_teks_footer',
        'as' => 'm_teks_footer.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_teks_footer::class, 'edit'])->name('index');
        Route::post('/update', [\App\Http\Controllers\Back\Master_teks_footer::class, 'update'])->name('update');
    });
    
    
    /**
     * master link
     */
    Route::group([
        'prefix' => 'm_link_cepat',
        'as' => 'm_link_cepat.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_link_cepat::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_link_cepat::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_link_cepat::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_link_cepat::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_link_cepat::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_link_cepat::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_link_cepat::class, 'datatable'])->name('datatable');
    });
    
    
    /**
     * master pegawai
     */
    Route::group([
        'prefix' => 'm_pegawai',
        'as' => 'm_pegawai.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_pegawai::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_pegawai::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_pegawai::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_pegawai::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_pegawai::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_pegawai::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_pegawai::class, 'datatable'])->name('datatable');
    });

    
    /**
     * master konten
     */
    Route::group([
        'prefix' => 'm_konten',
        'as' => 'm_konten.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_konten::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_konten::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_konten::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_konten::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_konten::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_konten::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_konten::class, 'datatable'])->name('datatable');
        
        Route::post('/datatable_media', [\App\Http\Controllers\Back\Master_konten::class, 'datatable_media'])->name('datatable_media');
        Route::post('/load_folder_media', [\App\Http\Controllers\Back\Master_konten::class, 'load_folder_media'])->name('load_folder_media');
        Route::post('/add_media', [\App\Http\Controllers\Back\Master_konten::class, 'add_media'])->name('add_media');
        Route::post('/edit_media', [\App\Http\Controllers\Back\Master_konten::class, 'edit_media'])->name('edit_media');
        Route::post('/save_media', [\App\Http\Controllers\Back\Master_konten::class, 'save_media'])->name('save_media');
        Route::post('/delete_folder', [\App\Http\Controllers\Back\Master_konten::class, 'delete_folder'])->name('delete_folder');
        Route::post('/delete_file', [\App\Http\Controllers\Back\Master_konten::class, 'delete_file'])->name('delete_file');
        Route::post('/delete_media', [\App\Http\Controllers\Back\Master_konten::class, 'delete_media'])->name('delete_media');
        Route::post('/save_file', [\App\Http\Controllers\Back\Master_konten::class, 'save_file'])->name('save_file');
        Route::post('/reload_media', [\App\Http\Controllers\Back\Master_konten::class, 'reload_media'])->name('reload_media');
    });


    
    /**
     * master m_slider
     */

    Route::group([
        'prefix' => 'm_slider',
        'as' => 'm_slider.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_slider::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_slider::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_slider::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_slider::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_slider::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_slider::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_slider::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master m_menu_front
     */

    Route::group([
        'prefix' => 'm_menu_front',
        'as' => 'm_menu_front.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_menu_front::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_menu_front::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_menu_front::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_menu_front::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_menu_front::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_menu_front::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_menu_front::class, 'datatable'])->name('datatable');
    });



    /**
     * master faq
     */
    Route::group([
        'prefix' => 'm_faq',
        'as' => 'm_faq.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_faq::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_faq::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_faq::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_faq::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_faq::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_faq::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_faq::class, 'datatable'])->name('datatable');
    });

    /**
     * master jadwal
     */
    Route::group([
        'prefix' => 'm_jadwal',
        'as' => 'm_jadwal.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_jadwal::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_jadwal::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_jadwal::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_jadwal::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_jadwal::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_jadwal::class, 'delete'])->name('delete');
        Route::post('/publish', [\App\Http\Controllers\Back\Master_jadwal::class, 'publish'])->name('publish');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_jadwal::class, 'datatable'])->name('datatable');
        Route::post('/set_text_program', [\App\Http\Controllers\Back\Master_jadwal::class, 'set_text_program'])->name('set_text_program');
    });

    /**
     * master kategori_kejuruan
     */
    Route::group([
        'prefix' => 'm_kategori_kejuruan',
        'as' => 'm_kategori_kejuruan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_kategori_kejuruan::class, 'datatable'])->name('datatable');
    });

    /**
     * master sub_kejuruan
     */
    Route::group([
        'prefix' => 'm_sub_kejuruan',
        'as' => 'm_sub_kejuruan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_sub_kejuruan::class, 'datatable'])->name('datatable');
    });

    /**
     * master tipe_pelatihan
     */
    Route::group([
        'prefix' => 'm_tipe_pelatihan',
        'as' => 'm_tipe_pelatihan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_tipe_pelatihan::class, 'datatable'])->name('datatable');
        Route::post('/reset', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'reset'])->name('reset');
        Route::post('/reset_pdf', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'reset_pdf'])->name('reset_pdf');
    });

    /**
     * master program_pelatihan
     */
    Route::group([
        'prefix' => 'm_program_pelatihan',
        'as' => 'm_program_pelatihan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'datatable'])->name('datatable');
        Route::post('/reset', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'reset'])->name('reset');
        Route::post('/reset_pdf', [\App\Http\Controllers\Back\Master_program_pelatihan::class, 'reset_pdf'])->name('reset_pdf');
    });

    /**
     * master sumber_dana
     */
    Route::group([
        'prefix' => 'm_sumber_dana',
        'as' => 'm_sumber_dana.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_sumber_dana::class, 'datatable'])->name('datatable');
    });
    
    
    /**
     * master gelombang
     */
    Route::group([
        'prefix' => 'm_gelombang',
        'as' => 'm_gelombang.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_gelombang::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_gelombang::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_gelombang::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_gelombang::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_gelombang::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_gelombang::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_gelombang::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master program_kegiatan
     */
    Route::group([
        'prefix' => 'm_program_kegiatan',
        'as' => 'm_program_kegiatan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_program_kegiatan::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master pengaduan
     */
    Route::group([
        'prefix' => 'm_pengaduan',
        'as' => 'm_pengaduan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_pengaduan::class, 'index'])->name('index');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_pengaduan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_pengaduan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_pengaduan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_pengaduan::class, 'datatable'])->name('datatable');
        Route::post('/iframe_print', [\App\Http\Controllers\Back\Master_pengaduan::class, 'iframe_print'])->name('iframe_print');
        Route::get('/iframe_pdf', [\App\Http\Controllers\Back\Master_pengaduan::class, 'iframe_pdf'])->name('iframe_pdf');
    });


    /**
     * master testimoni
     */
    Route::group([
        'prefix' => 'm_testimoni',
        'as' => 'm_testimoni.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_testimoni::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_testimoni::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_testimoni::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_testimoni::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_testimoni::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_testimoni::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_testimoni::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master perusahaan
     */
    Route::group([
        'prefix' => 'm_perusahaan',
        'as' => 'm_perusahaan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_perusahaan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_perusahaan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_perusahaan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_perusahaan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_perusahaan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_perusahaan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_perusahaan::class, 'datatable'])->name('datatable');
    });
    
    /**
     * master info_lowongan
     */
    Route::group([
        'prefix' => 'm_info_lowongan',
        'as' => 'm_info_lowongan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_info_lowongan::class, 'datatable'])->name('datatable');
    });



    /**
     * master kategori_katalog_alumni
     */
    Route::group([
        'prefix' => 'm_kategori_katalog_alumni',
        'as' => 'm_kategori_katalog_alumni.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_kategori_katalog_alumni::class, 'datatable'])->name('datatable');
    });


    /**
     * master katalog_alumni
     */
    Route::group([
        'prefix' => 'm_katalog_alumni',
        'as' => 'm_katalog_alumni.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_katalog_alumni::class, 'datatable'])->name('datatable');
    });


    /**
     * master bg
     */
    Route::group([
        'prefix' => 'm_bg',
        'as' => 'm_bg.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_bg::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_bg::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_bg::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_bg::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_bg::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_bg::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_bg::class, 'datatable'])->name('datatable');
        Route::post('/reset', [\App\Http\Controllers\Back\Master_bg::class, 'reset'])->name('reset');
    });


    /**
     * master logo
     */
    Route::group([
        'prefix' => 'm_logo',
        'as' => 'm_logo.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_logo::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_logo::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_logo::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_logo::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_logo::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_logo::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_logo::class, 'datatable'])->name('datatable');
    });

    /**
     * master reformasi_birokrasi
     */
    Route::group([
        'prefix' => 'm_reformasi_birokrasi',
        'as' => 'm_reformasi_birokrasi.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Master_reformasi_birokrasi::class, 'datatable'])->name('datatable');
    });

    /** 
     * TRANSAKSI ==============================================================================================================
     */



    /**
     * master reformasi_birokrasi
     */
    Route::group([
        'prefix' => 't_wawancara',
        'as' => 't_wawancara.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'edit'])->name('edit');
        Route::get('/edit_nilai', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'edit_nilai'])->name('edit_nilai');
        Route::get('/history', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'history'])->name('history');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'update'])->name('update');
        Route::post('/update_nilai', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'update_nilai'])->name('update_nilai');
        Route::post('/set_soal', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'set_soal'])->name('set_soal');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'datatable'])->name('datatable');
        Route::post('/perbarui_peringkat', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'perbarui_peringkat'])->name('perbarui_peringkat');
        Route::post('/set_undur_diri', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'set_undur_diri'])->name('set_undur_diri');
        Route::post('/load_program_available', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'load_program_available'])->name('load_program_available');
        Route::post('/load_kejuruan_available', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'load_kejuruan_available'])->name('load_kejuruan_available');
        Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'load_jadwal_available'])->name('load_jadwal_available');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Transaksi_wawancara::class, 'export_xls'])->name('export_xls');
    });
    
    
    
    /**
     * master t_wawancara_v2
     */
    Route::group([
        'prefix' => 't_wawancara_v2',
        'as' => 't_wawancara_v2.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'edit'])->name('edit');
        Route::get('/edit_nilai', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'edit_nilai'])->name('edit_nilai');
        Route::get('/history', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'history'])->name('history');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'update'])->name('update');
        Route::post('/update_nilai', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'update_nilai'])->name('update_nilai');
        Route::post('/set_soal', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'set_soal'])->name('set_soal');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'datatable'])->name('datatable');
        Route::post('/perbarui_peringkat', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'perbarui_peringkat'])->name('perbarui_peringkat');
        Route::post('/set_undur_diri', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'set_undur_diri'])->name('set_undur_diri');
        Route::post('/load_program_available', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'load_program_available'])->name('load_program_available');
        Route::post('/load_kejuruan_available', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'load_kejuruan_available'])->name('load_kejuruan_available');
        Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'load_jadwal_available'])->name('load_jadwal_available');
        Route::get('/export_xls', [\App\Http\Controllers\Back\Transaksi_wawancara_v2::class, 'export_xls'])->name('export_xls');
    });


    /**
     * master t_paket_soal_wawancara
     */
    Route::group([
        'prefix' => 't_paket_soal_wawancara',
        'as' => 't_paket_soal_wawancara.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'delete'])->name('delete');
        Route::post('/addsoal', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'addsoal'])->name('addsoal');
        Route::post('/delete_soal', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'delete_soal'])->name('delete_soal');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_paket_soal_wawancara::class, 'datatable'])->name('datatable');
    });

    /**
     * master t_pemetaan_kelas
     */
    Route::group([
        'prefix' => 't_pemetaan_kelas',
        'as' => 't_pemetaan_kelas.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'datatable'])->name('datatable');
        Route::post('/list_siswa', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'list_siswa'])->name('list_siswa');
        Route::post('/load_jadwal', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'load_jadwal'])->name('load_jadwal');
        Route::post('/load_nama_jadwal', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas::class, 'load_nama_jadwal'])->name('load_nama_jadwal');
    });

    /**
     * master t_hasil_pelatihan
     */
    Route::group([
        'prefix' => 't_hasil_pelatihan',
        'as' => 't_hasil_pelatihan.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan::class, 'index'])->name('index');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan::class, 'update'])->name('update');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan::class, 'datatable'])->name('datatable');
        Route::post('/list_siswa', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan::class, 'list_siswa'])->name('list_siswa');
        Route::post('/load_jadwal', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan::class, 'load_jadwal'])->name('load_jadwal');
    });
    
    
    /**
     * master t_pemetaan_kelas_mtu
     */
    Route::group([
        'prefix' => 't_pemetaan_kelas_mtu',
        'as' => 't_pemetaan_kelas_mtu.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'index'])->name('index');
        Route::get('/add', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'add'])->name('add');
        Route::post('/save', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'save'])->name('save');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'update'])->name('update');
        Route::post('/delete', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'delete'])->name('delete');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'datatable'])->name('datatable');
        Route::post('/list_siswa', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'list_siswa'])->name('list_siswa');
        Route::post('/nm_pemohon', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'nm_pemohon'])->name('nm_pemohon');
        Route::post('/load_jadwal', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'load_jadwal'])->name('load_jadwal');
        Route::post('/load_nama_jadwal', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'load_nama_jadwal'])->name('load_nama_jadwal');
        Route::post('/load_program_available', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'load_program_available'])->name('load_program_available');
        Route::post('/load_kejuruan_available', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'load_kejuruan_available'])->name('load_kejuruan_available');
        Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Transaksi_pemetaan_kelas_mtu::class, 'load_jadwal_available'])->name('load_jadwal_available');
    });

    /**
     * master t_hasil_pelatihan_mtu
     */
    Route::group([
        'prefix' => 't_hasil_pelatihan_mtu',
        'as' => 't_hasil_pelatihan_mtu.',
    ], function (){
        Route::get('/', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'index'])->name('index');
        Route::get('/edit', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'edit'])->name('edit');
        Route::post('/update', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'update'])->name('update');
        Route::post('/datatable', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'datatable'])->name('datatable');
        Route::post('/list_siswa', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'list_siswa'])->name('list_siswa');
        Route::post('/load_jadwal', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'load_jadwal'])->name('load_jadwal');
        Route::post('/load_program_available', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'load_program_available'])->name('load_program_available');
        Route::post('/load_kejuruan_available', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'load_kejuruan_available'])->name('load_kejuruan_available');
        Route::post('/load_jadwal_available', [\App\Http\Controllers\Back\Transaksi_hasil_pelatihan_mtu::class, 'load_jadwal_available'])->name('load_jadwal_available');
    });




});