/*!
* jQuery Migrate - v1.4.1 - 2016-05-19
* Copyright jQuery Foundation and other contributors
*/
(function(jQuery, window, undefined) {
    jQuery.migrateVersion = "1.4.1";
    var warnedAbout = {};
    jQuery.migrateWarnings = [];
    if (window.console && window.console.log) {
        window.console.log("JQMIGRATE: Migrate is installed" + (jQuery.migrateMute ? "" : " with logging active") + ", version " + jQuery.migrateVersion);
    }
    if (jQuery.migrateTrace === undefined) {
        jQuery.migrateTrace = true;
    }
    jQuery.migrateReset = function() {
        warnedAbout = {};
        jQuery.migrateWarnings.length = 0;
    }
    ;
    function migrateWarn(msg) {
        var console = window.console;
        if (!warnedAbout[msg]) {
            warnedAbout[msg] = true;
            jQuery.migrateWarnings.push(msg);
            if (console && console.warn && !jQuery.migrateMute) {
                console.warn("JQMIGRATE: " + msg);
                if (jQuery.migrateTrace && console.trace) {
                    console.trace();
                }
            }
        }
    }
    function migrateWarnProp(obj, prop, value, msg) {
        if (Object.defineProperty) {
            try {
                Object.defineProperty(obj, prop, {
                    configurable: true,
                    enumerable: true,
                    get: function() {
                        migrateWarn(msg);
                        return value;
                    },
                    set: function(newValue) {
                        migrateWarn(msg);
                        value = newValue;
                    }
                });
                return;
            } catch (err) {}
        }
        jQuery._definePropertyBroken = true;
        obj[prop] = value;
    }
    if (document.compatMode === "BackCompat") {
        migrateWarn("jQuery is not compatible with Quirks Mode");
    }
    var attrFn = jQuery("<input/>", {
        size: 1
    }).attr("size") && jQuery.attrFn
      , oldAttr = jQuery.attr
      , valueAttrGet = jQuery.attrHooks.value && jQuery.attrHooks.value.get || function() {
        return null;
    }
      , valueAttrSet = jQuery.attrHooks.value && jQuery.attrHooks.value.set || function() {
        return undefined;
    }
      , rnoType = /^(?:input|button)$/i
      , rnoAttrNodeType = /^[238]$/
      , rboolean = /^(?:autofocus|autoplay|async|checked|controls|defer|disabled|hidden|loop|multiple|open|readonly|required|scoped|selected)$/i
      , ruseDefault = /^(?:checked|selected)$/i;
    migrateWarnProp(jQuery, "attrFn", attrFn || {}, "jQuery.attrFn is deprecated");
    jQuery.attr = function(elem, name, value, pass) {
        var lowerName = name.toLowerCase()
          , nType = elem && elem.nodeType;
        if (pass) {
            if (oldAttr.length < 4) {
                migrateWarn("jQuery.fn.attr( props, pass ) is deprecated");
            }
            if (elem && !rnoAttrNodeType.test(nType) && (attrFn ? name in attrFn : jQuery.isFunction(jQuery.fn[name]))) {
                return jQuery(elem)[name](value);
            }
        }
        if (name === "type" && value !== undefined && rnoType.test(elem.nodeName) && elem.parentNode) {
            migrateWarn("Can't change the 'type' of an input or button in IE 6/7/8");
        }
        if (!jQuery.attrHooks[lowerName] && rboolean.test(lowerName)) {
            jQuery.attrHooks[lowerName] = {
                get: function(elem, name) {
                    var attrNode, property = jQuery.prop(elem, name);
                    return property === true || typeof property !== "boolean" && (attrNode = elem.getAttributeNode(name)) && attrNode.nodeValue !== false ? name.toLowerCase() : undefined;
                },
                set: function(elem, value, name) {
                    var propName;
                    if (value === false) {
                        jQuery.removeAttr(elem, name);
                    } else {
                        propName = jQuery.propFix[name] || name;
                        if (propName in elem) {
                            elem[propName] = true;
                        }
                        elem.setAttribute(name, name.toLowerCase());
                    }
                    return name;
                }
            };
            if (ruseDefault.test(lowerName)) {
                migrateWarn("jQuery.fn.attr('" + lowerName + "') might use property instead of attribute");
            }
        }
        return oldAttr.call(jQuery, elem, name, value);
    }
    ;
    jQuery.attrHooks.value = {
        get: function(elem, name) {
            var nodeName = (elem.nodeName || "").toLowerCase();
            if (nodeName === "button") {
                return valueAttrGet.apply(this, arguments);
            }
            if (nodeName !== "input" && nodeName !== "option") {
                migrateWarn("jQuery.fn.attr('value') no longer gets properties");
            }
            return name in elem ? elem.value : null;
        },
        set: function(elem, value) {
            var nodeName = (elem.nodeName || "").toLowerCase();
            if (nodeName === "button") {
                return valueAttrSet.apply(this, arguments);
            }
            if (nodeName !== "input" && nodeName !== "option") {
                migrateWarn("jQuery.fn.attr('value', val) no longer sets properties");
            }
            elem.value = value;
        }
    };
    var matched, browser, oldInit = jQuery.fn.init, oldFind = jQuery.find, oldParseJSON = jQuery.parseJSON, rspaceAngle = /^\s*</, rattrHashTest = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/, rattrHashGlob = /\[(\s*[-\w]+\s*)([~|^$*]?=)\s*([-\w#]*?#[-\w#]*)\s*\]/g, rquickExpr = /^([^<]*)(<[\w\W]+>)([^>]*)$/;
    jQuery.fn.init = function(selector, context, rootjQuery) {
        var match, ret;
        if (selector && typeof selector === "string") {
            if (!jQuery.isPlainObject(context) && (match = rquickExpr.exec(jQuery.trim(selector))) && match[0]) {
                if (!rspaceAngle.test(selector)) {
                    migrateWarn("$(html) HTML strings must start with '<' character");
                }
                if (match[3]) {
                    migrateWarn("$(html) HTML text after last tag is ignored");
                }
                if (match[0].charAt(0) === "#") {
                    migrateWarn("HTML string cannot start with a '#' character");
                    jQuery.error("JQMIGRATE: Invalid selector string (XSS)");
                }
                if (context && context.context && context.context.nodeType) {
                    context = context.context;
                }
                if (jQuery.parseHTML) {
                    return oldInit.call(this, jQuery.parseHTML(match[2], context && context.ownerDocument || context || document, true), context, rootjQuery);
                }
            }
        }
        ret = oldInit.apply(this, arguments);
        if (selector && selector.selector !== undefined) {
            ret.selector = selector.selector;
            ret.context = selector.context;
        } else {
            ret.selector = typeof selector === "string" ? selector : "";
            if (selector) {
                ret.context = selector.nodeType ? selector : context || document;
            }
        }
        return ret;
    }
    ;
    jQuery.fn.init.prototype = jQuery.fn;
    jQuery.find = function(selector) {
        var args = Array.prototype.slice.call(arguments);
        if (typeof selector === "string" && rattrHashTest.test(selector)) {
            try {
                document.querySelector(selector);
            } catch (err1) {
                selector = selector.replace(rattrHashGlob, function(_, attr, op, value) {
                    return "[" + attr + op + "\"" + value + "\"]";
                });
                try {
                    document.querySelector(selector);
                    migrateWarn("Attribute selector with '#' must be quoted: " + args[0]);
                    args[0] = selector;
                } catch (err2) {
                    migrateWarn("Attribute selector with '#' was not fixed: " + args[0]);
                }
            }
        }
        return oldFind.apply(this, args);
    }
    ;
    var findProp;
    for (findProp in oldFind) {
        if (Object.prototype.hasOwnProperty.call(oldFind, findProp)) {
            jQuery.find[findProp] = oldFind[findProp];
        }
    }
    jQuery.parseJSON = function(json) {
        if (!json) {
            migrateWarn("jQuery.parseJSON requires a valid JSON string");
            return null;
        }
        return oldParseJSON.apply(this, arguments);
    }
    ;
    jQuery.uaMatch = function(ua) {
        ua = ua.toLowerCase();
        var match = /(chrome)[ \/]([\w.]+)/.exec(ua) || /(webkit)[ \/]([\w.]+)/.exec(ua) || /(opera)(?:.*version|)[ \/]([\w.]+)/.exec(ua) || /(msie) ([\w.]+)/.exec(ua) || ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec(ua) || [];
        return {
            browser: match[1] || "",
            version: match[2] || "0"
        };
    }
    ;
    if (!jQuery.browser) {
        matched = jQuery.uaMatch(navigator.userAgent);
        browser = {};
        if (matched.browser) {
            browser[matched.browser] = true;
            browser.version = matched.version;
        }
        if (browser.chrome) {
            browser.webkit = true;
        } else if (browser.webkit) {
            browser.safari = true;
        }
        jQuery.browser = browser;
    }
    migrateWarnProp(jQuery, "browser", jQuery.browser, "jQuery.browser is deprecated");
    jQuery.boxModel = jQuery.support.boxModel = (document.compatMode === "CSS1Compat");
    migrateWarnProp(jQuery, "boxModel", jQuery.boxModel, "jQuery.boxModel is deprecated");
    migrateWarnProp(jQuery.support, "boxModel", jQuery.support.boxModel, "jQuery.support.boxModel is deprecated");
    jQuery.sub = function() {
        function jQuerySub(selector, context) {
            return new jQuerySub.fn.init(selector,context);
        }
        jQuery.extend(true, jQuerySub, this);
        jQuerySub.superclass = this;
        jQuerySub.fn = jQuerySub.prototype = this();
        jQuerySub.fn.constructor = jQuerySub;
        jQuerySub.sub = this.sub;
        jQuerySub.fn.init = function init(selector, context) {
            var instance = jQuery.fn.init.call(this, selector, context, rootjQuerySub);
            return instance instanceof jQuerySub ? instance : jQuerySub(instance);
        }
        ;
        jQuerySub.fn.init.prototype = jQuerySub.fn;
        var rootjQuerySub = jQuerySub(document);
        migrateWarn("jQuery.sub() is deprecated");
        return jQuerySub;
    }
    ;
    jQuery.fn.size = function() {
        migrateWarn("jQuery.fn.size() is deprecated; use the .length property");
        return this.length;
    }
    ;
    var internalSwapCall = false;
    if (jQuery.swap) {
        jQuery.each(["height", "width", "reliableMarginRight"], function(_, name) {
            var oldHook = jQuery.cssHooks[name] && jQuery.cssHooks[name].get;
            if (oldHook) {
                jQuery.cssHooks[name].get = function() {
                    var ret;
                    internalSwapCall = true;
                    ret = oldHook.apply(this, arguments);
                    internalSwapCall = false;
                    return ret;
                }
                ;
            }
        });
    }
    jQuery.swap = function(elem, options, callback, args) {
        var ret, name, old = {};
        if (!internalSwapCall) {
            migrateWarn("jQuery.swap() is undocumented and deprecated");
        }
        for (name in options) {
            old[name] = elem.style[name];
            elem.style[name] = options[name];
        }
        ret = callback.apply(elem, args || []);
        for (name in options) {
            elem.style[name] = old[name];
        }
        return ret;
    }
    ;
    jQuery.ajaxSetup({
        converters: {
            "text json": jQuery.parseJSON
        }
    });
    var oldFnData = jQuery.fn.data;
    jQuery.fn.data = function(name) {
        var ret, evt, elem = this[0];
        if (elem && name === "events" && arguments.length === 1) {
            ret = jQuery.data(elem, name);
            evt = jQuery._data(elem, name);
            if ((ret === undefined || ret === evt) && evt !== undefined) {
                migrateWarn("Use of jQuery.fn.data('events') is deprecated");
                return evt;
            }
        }
        return oldFnData.apply(this, arguments);
    }
    ;
    var rscriptType = /\/(java|ecma)script/i;
    if (!jQuery.clean) {
        jQuery.clean = function(elems, context, fragment, scripts) {
            context = context || document;
            context = !context.nodeType && context[0] || context;
            context = context.ownerDocument || context;
            migrateWarn("jQuery.clean() is deprecated");
            var i, elem, handleScript, jsTags, ret = [];
            jQuery.merge(ret, jQuery.buildFragment(elems, context).childNodes);
            if (fragment) {
                handleScript = function(elem) {
                    if (!elem.type || rscriptType.test(elem.type)) {
                        return scripts ? scripts.push(elem.parentNode ? elem.parentNode.removeChild(elem) : elem) : fragment.appendChild(elem);
                    }
                }
                ;
                for (i = 0; (elem = ret[i]) != null; i++) {
                    if (!(jQuery.nodeName(elem, "script") && handleScript(elem))) {
                        fragment.appendChild(elem);
                        if (typeof elem.getElementsByTagName !== "undefined") {
                            jsTags = jQuery.grep(jQuery.merge([], elem.getElementsByTagName("script")), handleScript);
                            ret.splice.apply(ret, [i + 1, 0].concat(jsTags));
                            i += jsTags.length;
                        }
                    }
                }
            }
            return ret;
        }
        ;
    }
    var eventAdd = jQuery.event.add
      , eventRemove = jQuery.event.remove
      , eventTrigger = jQuery.event.trigger
      , oldToggle = jQuery.fn.toggle
      , oldLive = jQuery.fn.live
      , oldDie = jQuery.fn.die
      , oldLoad = jQuery.fn.load
      , ajaxEvents = "ajaxStart|ajaxStop|ajaxSend|ajaxComplete|ajaxError|ajaxSuccess"
      , rajaxEvent = new RegExp("\\b(?:" + ajaxEvents + ")\\b")
      , rhoverHack = /(?:^|\s)hover(\.\S+|)\b/
      , hoverHack = function(events) {
        if (typeof (events) !== "string" || jQuery.event.special.hover) {
            return events;
        }
        if (rhoverHack.test(events)) {
            migrateWarn("'hover' pseudo-event is deprecated, use 'mouseenter mouseleave'");
        }
        return events && events.replace(rhoverHack, "mouseenter$1 mouseleave$1");
    };
    if (jQuery.event.props && jQuery.event.props[0] !== "attrChange") {
        jQuery.event.props.unshift("attrChange", "attrName", "relatedNode", "srcElement");
    }
    if (jQuery.event.dispatch) {
        migrateWarnProp(jQuery.event, "handle", jQuery.event.dispatch, "jQuery.event.handle is undocumented and deprecated");
    }
    jQuery.event.add = function(elem, types, handler, data, selector) {
        if (elem !== document && rajaxEvent.test(types)) {
            migrateWarn("AJAX events should be attached to document: " + types);
        }
        eventAdd.call(this, elem, hoverHack(types || ""), handler, data, selector);
    }
    ;
    jQuery.event.remove = function(elem, types, handler, selector, mappedTypes) {
        eventRemove.call(this, elem, hoverHack(types) || "", handler, selector, mappedTypes);
    }
    ;
    jQuery.each(["load", "unload", "error"], function(_, name) {
        jQuery.fn[name] = function() {
            var args = Array.prototype.slice.call(arguments, 0);
            if (name === "load" && typeof args[0] === "string") {
                return oldLoad.apply(this, args);
            }
            migrateWarn("jQuery.fn." + name + "() is deprecated");
            args.splice(0, 0, name);
            if (arguments.length) {
                return this.bind.apply(this, args);
            }
            this.triggerHandler.apply(this, args);
            return this;
        }
        ;
    });
    jQuery.fn.toggle = function(fn, fn2) {
        if (!jQuery.isFunction(fn) || !jQuery.isFunction(fn2)) {
            return oldToggle.apply(this, arguments);
        }
        migrateWarn("jQuery.fn.toggle(handler, handler...) is deprecated");
        var args = arguments
          , guid = fn.guid || jQuery.guid++
          , i = 0
          , toggler = function(event) {
            var lastToggle = (jQuery._data(this, "lastToggle" + fn.guid) || 0) % i;
            jQuery._data(this, "lastToggle" + fn.guid, lastToggle + 1);
            event.preventDefault();
            return args[lastToggle].apply(this, arguments) || false;
        };
        toggler.guid = guid;
        while (i < args.length) {
            args[i++].guid = guid;
        }
        return this.click(toggler);
    }
    ;
    jQuery.fn.live = function(types, data, fn) {
        migrateWarn("jQuery.fn.live() is deprecated");
        if (oldLive) {
            return oldLive.apply(this, arguments);
        }
        jQuery(this.context).on(types, this.selector, data, fn);
        return this;
    }
    ;
    jQuery.fn.die = function(types, fn) {
        migrateWarn("jQuery.fn.die() is deprecated");
        if (oldDie) {
            return oldDie.apply(this, arguments);
        }
        jQuery(this.context).off(types, this.selector || "**", fn);
        return this;
    }
    ;
    jQuery.event.trigger = function(event, data, elem, onlyHandlers) {
        if (!elem && !rajaxEvent.test(event)) {
            migrateWarn("Global events are undocumented and deprecated");
        }
        return eventTrigger.call(this, event, data, elem || document, onlyHandlers);
    }
    ;
    jQuery.each(ajaxEvents.split("|"), function(_, name) {
        jQuery.event.special[name] = {
            setup: function() {
                var elem = this;
                if (elem !== document) {
                    jQuery.event.add(document, name + "." + jQuery.guid, function() {
                        jQuery.event.trigger(name, Array.prototype.slice.call(arguments, 1), elem, true);
                    });
                    jQuery._data(this, name, jQuery.guid++);
                }
                return false;
            },
            teardown: function() {
                if (this !== document) {
                    jQuery.event.remove(document, name + "." + jQuery._data(this, name));
                }
                return false;
            }
        };
    });
    jQuery.event.special.ready = {
        setup: function() {
            if (this === document) {
                migrateWarn("'ready' event is deprecated");
            }
        }
    };
    var oldSelf = jQuery.fn.andSelf || jQuery.fn.addBack
      , oldFnFind = jQuery.fn.find;
    jQuery.fn.andSelf = function() {
        migrateWarn("jQuery.fn.andSelf() replaced by jQuery.fn.addBack()");
        return oldSelf.apply(this, arguments);
    }
    ;
    jQuery.fn.find = function(selector) {
        var ret = oldFnFind.apply(this, arguments);
        ret.context = this.context;
        ret.selector = this.selector ? this.selector + " " + selector : selector;
        return ret;
    }
    ;
    if (jQuery.Callbacks) {
        var oldDeferred = jQuery.Deferred
          , tuples = [["resolve", "done", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), "resolved"], ["reject", "fail", jQuery.Callbacks("once memory"), jQuery.Callbacks("once memory"), "rejected"], ["notify", "progress", jQuery.Callbacks("memory"), jQuery.Callbacks("memory")]];
        jQuery.Deferred = function(func) {
            var deferred = oldDeferred()
              , promise = deferred.promise();
            deferred.pipe = promise.pipe = function() {
                var fns = arguments;
                migrateWarn("deferred.pipe() is deprecated");
                return jQuery.Deferred(function(newDefer) {
                    jQuery.each(tuples, function(i, tuple) {
                        var fn = jQuery.isFunction(fns[i]) && fns[i];
                        deferred[tuple[1]](function() {
                            var returned = fn && fn.apply(this, arguments);
                            if (returned && jQuery.isFunction(returned.promise)) {
                                returned.promise().done(newDefer.resolve).fail(newDefer.reject).progress(newDefer.notify);
                            } else {
                                newDefer[tuple[0] + "With"](this === promise ? newDefer.promise() : this, fn ? [returned] : arguments);
                            }
                        });
                    });
                    fns = null;
                }).promise();
            }
            ;
            deferred.isResolved = function() {
                migrateWarn("deferred.isResolved is deprecated");
                return deferred.state() === "resolved";
            }
            ;
            deferred.isRejected = function() {
                migrateWarn("deferred.isRejected is deprecated");
                return deferred.state() === "rejected";
            }
            ;
            if (func) {
                func.call(deferred, deferred);
            }
            return deferred;
        }
        ;
    }
}
)(jQuery, window);
(function(a) {
    if (typeof define === "function" && define.amd && define.amd.jQuery) {
        define(["jquery"], a)
    } else {
        a(jQuery)
    }
}(function(f) {
    var y = "1.6.9"
      , p = "left"
      , o = "right"
      , e = "up"
      , x = "down"
      , c = "in"
      , A = "out"
      , m = "none"
      , s = "auto"
      , l = "swipe"
      , t = "pinch"
      , B = "tap"
      , j = "doubletap"
      , b = "longtap"
      , z = "hold"
      , E = "horizontal"
      , u = "vertical"
      , i = "all"
      , r = 10
      , g = "start"
      , k = "move"
      , h = "end"
      , q = "cancel"
      , a = "ontouchstart"in window
      , v = window.navigator.msPointerEnabled && !window.navigator.pointerEnabled
      , d = window.navigator.pointerEnabled || window.navigator.msPointerEnabled
      , C = "TouchSwipe";
    var n = {
        fingers: 1,
        threshold: 75,
        cancelThreshold: null,
        pinchThreshold: 20,
        maxTimeThreshold: null,
        fingerReleaseThreshold: 250,
        longTapThreshold: 500,
        doubleTapThreshold: 200,
        swipe: null,
        swipeLeft: null,
        swipeRight: null,
        swipeUp: null,
        swipeDown: null,
        swipeStatus: null,
        pinchIn: null,
        pinchOut: null,
        pinchStatus: null,
        click: null,
        tap: null,
        doubleTap: null,
        longTap: null,
        hold: null,
        triggerOnTouchEnd: true,
        triggerOnTouchLeave: false,
        allowPageScroll: "auto",
        fallbackToMouseEvents: true,
        excludedElements: "label, button, input, select, textarea, a, .noSwipe",
        preventDefaultEvents: true
    };
    f.fn.swipetp = function(H) {
        var G = f(this)
          , F = G.data(C);
        if (F && typeof H === "string") {
            if (F[H]) {
                return F[H].apply(this, Array.prototype.slice.call(arguments, 1))
            } else {
                f.error("Method " + H + " does not exist on jQuery.swipetp")
            }
        } else {
            if (!F && (typeof H === "object" || !H)) {
                return w.apply(this, arguments)
            }
        }
        return G
    }
    ;
    f.fn.swipetp.version = y;
    f.fn.swipetp.defaults = n;
    f.fn.swipetp.phases = {
        PHASE_START: g,
        PHASE_MOVE: k,
        PHASE_END: h,
        PHASE_CANCEL: q
    };
    f.fn.swipetp.directions = {
        LEFT: p,
        RIGHT: o,
        UP: e,
        DOWN: x,
        IN: c,
        OUT: A
    };
    f.fn.swipetp.pageScroll = {
        NONE: m,
        HORIZONTAL: E,
        VERTICAL: u,
        AUTO: s
    };
    f.fn.swipetp.fingers = {
        ONE: 1,
        TWO: 2,
        THREE: 3,
        ALL: i
    };
    function w(F) {
        if (F && (F.allowPageScroll === undefined && (F.swipe !== undefined || F.swipeStatus !== undefined))) {
            F.allowPageScroll = m
        }
        if (F.click !== undefined && F.tap === undefined) {
            F.tap = F.click
        }
        if (!F) {
            F = {}
        }
        F = f.extend({}, f.fn.swipetp.defaults, F);
        return this.each(function() {
            var H = f(this);
            var G = H.data(C);
            if (!G) {
                G = new D(this,F);
                H.data(C, G)
            }
        })
    }
    function D(a5, aw) {
        var aA = (a || d || !aw.fallbackToMouseEvents)
          , K = aA ? (d ? (v ? "MSPointerDown" : "pointerdown") : "touchstart") : "mousedown"
          , az = aA ? (d ? (v ? "MSPointerMove" : "pointermove") : "touchmove") : "mousemove"
          , V = aA ? (d ? (v ? "MSPointerUp" : "pointerup") : "touchend") : "mouseup"
          , T = aA ? null : "mouseleave"
          , aE = (d ? (v ? "MSPointerCancel" : "pointercancel") : "touchcancel");
        var ah = 0
          , aQ = null
          , ac = 0
          , a2 = 0
          , a0 = 0
          , H = 1
          , ar = 0
          , aK = 0
          , N = null;
        var aS = f(a5);
        var aa = "start";
        var X = 0;
        var aR = null;
        var U = 0
          , a3 = 0
          , a6 = 0
          , ae = 0
          , O = 0;
        var aX = null
          , ag = null;
        try {
            aS.bind(K, aO);
            aS.bind(aE, ba)
        } catch (al) {
            f.error("events not supported " + K + "," + aE + " on jQuery.swipetp")
        }
        this.enable = function() {
            aS.bind(K, aO);
            aS.bind(aE, ba);
            return aS
        }
        ;
        this.disable = function() {
            aL();
            return aS
        }
        ;
        this.destroy = function() {
            aL();
            aS.data(C, null);
            aS = null
        }
        ;
        this.option = function(bd, bc) {
            if (aw[bd] !== undefined) {
                if (bc === undefined) {
                    return aw[bd]
                } else {
                    aw[bd] = bc
                }
            } else {
                f.error("Option " + bd + " does not exist on jQuery.swipetp.options")
            }
            return null
        }
        ;
        function aO(be) {
            if (aC()) {
                return
            }
            if (f(be.target).closest(aw.excludedElements, aS).length > 0) {
                return
            }
            var bf = be.originalEvent ? be.originalEvent : be;
            var bd, bg = bf.touches, bc = bg ? bg[0] : bf;
            aa = g;
            if (bg) {
                X = bg.length
            } else {
                be.preventDefault()
            }
            ah = 0;
            aQ = null;
            aK = null;
            ac = 0;
            a2 = 0;
            a0 = 0;
            H = 1;
            ar = 0;
            aR = ak();
            N = ab();
            S();
            if (!bg || (X === aw.fingers || aw.fingers === i) || aY()) {
                aj(0, bc);
                U = au();
                if (X == 2) {
                    aj(1, bg[1]);
                    a2 = a0 = av(aR[0].start, aR[1].start)
                }
                if (aw.swipeStatus || aw.pinchStatus) {
                    bd = P(bf, aa)
                }
            } else {
                bd = false
            }
            if (bd === false) {
                aa = q;
                P(bf, aa);
                return bd
            } else {
                if (aw.hold) {
                    ag = setTimeout(f.proxy(function() {
                        aS.trigger("hold", [bf.target]);
                        if (aw.hold) {
                            bd = aw.hold.call(aS, bf, bf.target)
                        }
                    }, this), aw.longTapThreshold)
                }
                ap(true)
            }
            return null
        }
        function a4(bf) {
            var bi = bf.originalEvent ? bf.originalEvent : bf;
            if (aa === h || aa === q || an()) {
                return
            }
            var be, bj = bi.touches, bd = bj ? bj[0] : bi;
            var bg = aI(bd);
            a3 = au();
            if (bj) {
                X = bj.length
            }
            if (aw.hold) {
                clearTimeout(ag)
            }
            aa = k;
            if (X == 2) {
                if (a2 == 0) {
                    aj(1, bj[1]);
                    a2 = a0 = av(aR[0].start, aR[1].start)
                } else {
                    aI(bj[1]);
                    a0 = av(aR[0].end, aR[1].end);
                    aK = at(aR[0].end, aR[1].end)
                }
                H = a8(a2, a0);
                ar = Math.abs(a2 - a0)
            }
            if ((X === aw.fingers || aw.fingers === i) || !bj || aY()) {
                aQ = aM(bg.start, bg.end);
                am(bf, aQ);
                ah = aT(bg.start, bg.end);
                ac = aN();
                aJ(aQ, ah);
                if (aw.swipeStatus || aw.pinchStatus) {
                    be = P(bi, aa)
                }
                if (!aw.triggerOnTouchEnd || aw.triggerOnTouchLeave) {
                    var bc = true;
                    if (aw.triggerOnTouchLeave) {
                        var bh = aZ(this);
                        bc = F(bg.end, bh)
                    }
                    if (!aw.triggerOnTouchEnd && bc) {
                        aa = aD(k)
                    } else {
                        if (aw.triggerOnTouchLeave && !bc) {
                            aa = aD(h)
                        }
                    }
                    if (aa == q || aa == h) {
                        P(bi, aa)
                    }
                }
            } else {
                aa = q;
                P(bi, aa)
            }
            if (be === false) {
                aa = q;
                P(bi, aa)
            }
        }
        function M(bc) {
            var bd = bc.originalEvent ? bc.originalEvent : bc
              , be = bd.touches;
            if (be) {
                if (be.length) {
                    G();
                    return true
                }
            }
            if (an()) {
                X = ae
            }
            a3 = au();
            ac = aN();
            if (bb() || !ao()) {
                aa = q;
                P(bd, aa)
            } else {
                if (aw.triggerOnTouchEnd || (aw.triggerOnTouchEnd == false && aa === k)) {
                    bc.preventDefault();
                    aa = h;
                    P(bd, aa)
                } else {
                    if (!aw.triggerOnTouchEnd && a7()) {
                        aa = h;
                        aG(bd, aa, B)
                    } else {
                        if (aa === k) {
                            aa = q;
                            P(bd, aa)
                        }
                    }
                }
            }
            ap(false);
            return null
        }
        function ba() {
            X = 0;
            a3 = 0;
            U = 0;
            a2 = 0;
            a0 = 0;
            H = 1;
            S();
            ap(false)
        }
        function L(bc) {
            var bd = bc.originalEvent ? bc.originalEvent : bc;
            if (aw.triggerOnTouchLeave) {
                aa = aD(h);
                P(bd, aa)
            }
        }
        function aL() {
            aS.unbind(K, aO);
            aS.unbind(aE, ba);
            aS.unbind(az, a4);
            aS.unbind(V, M);
            if (T) {
                aS.unbind(T, L)
            }
            ap(false)
        }
        function aD(bg) {
            var bf = bg;
            var be = aB();
            var bd = ao();
            var bc = bb();
            if (!be || bc) {
                bf = q
            } else {
                if (bd && bg == k && (!aw.triggerOnTouchEnd || aw.triggerOnTouchLeave)) {
                    bf = h
                } else {
                    if (!bd && bg == h && aw.triggerOnTouchLeave) {
                        bf = q
                    }
                }
            }
            return bf
        }
        function P(be, bc) {
            var bd, bf = be.touches;
            if ((J() || W()) || (Q() || aY())) {
                if (J() || W()) {
                    bd = aG(be, bc, l)
                }
                if ((Q() || aY()) && bd !== false) {
                    bd = aG(be, bc, t)
                }
            } else {
                if (aH() && bd !== false) {
                    bd = aG(be, bc, j)
                } else {
                    if (aq() && bd !== false) {
                        bd = aG(be, bc, b)
                    } else {
                        if (ai() && bd !== false) {
                            bd = aG(be, bc, B)
                        }
                    }
                }
            }
            if (bc === q) {
                ba(be)
            }
            if (bc === h) {
                if (bf) {
                    if (!bf.length) {
                        ba(be)
                    }
                } else {
                    ba(be)
                }
            }
            return bd
        }
        function aG(bf, bc, be) {
            var bd;
            if (be == l) {
                aS.trigger("swipeStatus", [bc, aQ || null, ah || 0, ac || 0, X, aR]);
                if (aw.swipeStatus) {
                    bd = aw.swipeStatus.call(aS, bf, bc, aQ || null, ah || 0, ac || 0, X, aR);
                    if (bd === false) {
                        return false
                    }
                }
                if (bc == h && aW()) {
                    aS.trigger("swipe", [aQ, ah, ac, X, aR]);
                    if (aw.swipe) {
                        bd = aw.swipe.call(aS, bf, aQ, ah, ac, X, aR);
                        if (bd === false) {
                            return false
                        }
                    }
                    switch (aQ) {
                    case p:
                        aS.trigger("swipeLeft", [aQ, ah, ac, X, aR]);
                        if (aw.swipeLeft) {
                            bd = aw.swipeLeft.call(aS, bf, aQ, ah, ac, X, aR)
                        }
                        break;
                    case o:
                        aS.trigger("swipeRight", [aQ, ah, ac, X, aR]);
                        if (aw.swipeRight) {
                            bd = aw.swipeRight.call(aS, bf, aQ, ah, ac, X, aR)
                        }
                        break;
                    case e:
                        aS.trigger("swipeUp", [aQ, ah, ac, X, aR]);
                        if (aw.swipeUp) {
                            bd = aw.swipeUp.call(aS, bf, aQ, ah, ac, X, aR)
                        }
                        break;
                    case x:
                        aS.trigger("swipeDown", [aQ, ah, ac, X, aR]);
                        if (aw.swipeDown) {
                            bd = aw.swipeDown.call(aS, bf, aQ, ah, ac, X, aR)
                        }
                        break
                    }
                }
            }
            if (be == t) {
                aS.trigger("pinchStatus", [bc, aK || null, ar || 0, ac || 0, X, H, aR]);
                if (aw.pinchStatus) {
                    bd = aw.pinchStatus.call(aS, bf, bc, aK || null, ar || 0, ac || 0, X, H, aR);
                    if (bd === false) {
                        return false
                    }
                }
                if (bc == h && a9()) {
                    switch (aK) {
                    case c:
                        aS.trigger("pinchIn", [aK || null, ar || 0, ac || 0, X, H, aR]);
                        if (aw.pinchIn) {
                            bd = aw.pinchIn.call(aS, bf, aK || null, ar || 0, ac || 0, X, H, aR)
                        }
                        break;
                    case A:
                        aS.trigger("pinchOut", [aK || null, ar || 0, ac || 0, X, H, aR]);
                        if (aw.pinchOut) {
                            bd = aw.pinchOut.call(aS, bf, aK || null, ar || 0, ac || 0, X, H, aR)
                        }
                        break
                    }
                }
            }
            if (be == B) {
                if (bc === q || bc === h) {
                    clearTimeout(aX);
                    clearTimeout(ag);
                    if (Z() && !I()) {
                        O = au();
                        aX = setTimeout(f.proxy(function() {
                            O = null;
                            aS.trigger("tap", [bf.target]);
                            if (aw.tap) {
                                bd = aw.tap.call(aS, bf, bf.target)
                            }
                        }, this), aw.doubleTapThreshold)
                    } else {
                        O = null;
                        aS.trigger("tap", [bf.target]);
                        if (aw.tap) {
                            bd = aw.tap.call(aS, bf, bf.target)
                        }
                    }
                }
            } else {
                if (be == j) {
                    if (bc === q || bc === h) {
                        clearTimeout(aX);
                        O = null;
                        aS.trigger("doubletap", [bf.target]);
                        if (aw.doubleTap) {
                            bd = aw.doubleTap.call(aS, bf, bf.target)
                        }
                    }
                } else {
                    if (be == b) {
                        if (bc === q || bc === h) {
                            clearTimeout(aX);
                            O = null;
                            aS.trigger("longtap", [bf.target]);
                            if (aw.longTap) {
                                bd = aw.longTap.call(aS, bf, bf.target)
                            }
                        }
                    }
                }
            }
            return bd
        }
        function ao() {
            var bc = true;
            if (aw.threshold !== null) {
                bc = ah >= aw.threshold
            }
            return bc
        }
        function bb() {
            var bc = false;
            if (aw.cancelThreshold !== null && aQ !== null) {
                bc = (aU(aQ) - ah) >= aw.cancelThreshold
            }
            return bc
        }
        function af() {
            if (aw.pinchThreshold !== null) {
                return ar >= aw.pinchThreshold
            }
            return true
        }
        function aB() {
            var bc;
            if (aw.maxTimeThreshold) {
                if (ac >= aw.maxTimeThreshold) {
                    bc = false
                } else {
                    bc = true
                }
            } else {
                bc = true
            }
            return bc
        }
        function am(bc, bd) {
            if (aw.preventDefaultEvents === false) {
                return
            }
            if (aw.allowPageScroll === m) {
                bc.preventDefault()
            } else {
                var be = aw.allowPageScroll === s;
                switch (bd) {
                case p:
                    if ((aw.swipeLeft && be) || (!be && aw.allowPageScroll != E)) {
                        bc.preventDefault()
                    }
                    break;
                case o:
                    if ((aw.swipeRight && be) || (!be && aw.allowPageScroll != E)) {
                        bc.preventDefault()
                    }
                    break;
                case e:
                    if ((aw.swipeUp && be) || (!be && aw.allowPageScroll != u)) {
                        bc.preventDefault()
                    }
                    break;
                case x:
                    if ((aw.swipeDown && be) || (!be && aw.allowPageScroll != u)) {
                        bc.preventDefault()
                    }
                    break
                }
            }
        }
        function a9() {
            var bd = aP();
            var bc = Y();
            var be = af();
            return bd && bc && be
        }
        function aY() {
            return !!(aw.pinchStatus || aw.pinchIn || aw.pinchOut)
        }
        function Q() {
            return !!(a9() && aY())
        }
        function aW() {
            var bf = aB();
            var bh = ao();
            var be = aP();
            var bc = Y();
            var bd = bb();
            var bg = !bd && bc && be && bh && bf;
            return bg
        }
        function W() {
            return !!(aw.swipe || aw.swipeStatus || aw.swipeLeft || aw.swipeRight || aw.swipeUp || aw.swipeDown)
        }
        function J() {
            return !!(aW() && W())
        }
        function aP() {
            return ((X === aw.fingers || aw.fingers === i) || !a)
        }
        function Y() {
            return aR[0].end.x !== 0
        }
        function a7() {
            return !!(aw.tap)
        }
        function Z() {
            return !!(aw.doubleTap)
        }
        function aV() {
            return !!(aw.longTap)
        }
        function R() {
            if (O == null) {
                return false
            }
            var bc = au();
            return (Z() && ((bc - O) <= aw.doubleTapThreshold))
        }
        function I() {
            return R()
        }
        function ay() {
            return ((X === 1 || !a) && (isNaN(ah) || ah < aw.threshold))
        }
        function a1() {
            return ((ac > aw.longTapThreshold) && (ah < r))
        }
        function ai() {
            return !!(ay() && a7())
        }
        function aH() {
            return !!(R() && Z())
        }
        function aq() {
            return !!(a1() && aV())
        }
        function G() {
            a6 = au();
            ae = event.touches.length + 1
        }
        function S() {
            a6 = 0;
            ae = 0
        }
        function an() {
            var bc = false;
            if (a6) {
                var bd = au() - a6;
                if (bd <= aw.fingerReleaseThreshold) {
                    bc = true
                }
            }
            return bc
        }
        function aC() {
            return !!(aS.data(C + "_intouch") === true)
        }
        function ap(bc) {
            if (bc === true) {
                aS.bind(az, a4);
                aS.bind(V, M);
                if (T) {
                    aS.bind(T, L)
                }
            } else {
                aS.unbind(az, a4, false);
                aS.unbind(V, M, false);
                if (T) {
                    aS.unbind(T, L, false)
                }
            }
            aS.data(C + "_intouch", bc === true)
        }
        function aj(bd, bc) {
            var be = bc.identifier !== undefined ? bc.identifier : 0;
            aR[bd].identifier = be;
            aR[bd].start.x = aR[bd].end.x = bc.pageX || bc.clientX;
            aR[bd].start.y = aR[bd].end.y = bc.pageY || bc.clientY;
            return aR[bd]
        }
        function aI(bc) {
            var be = bc.identifier !== undefined ? bc.identifier : 0;
            var bd = ad(be);
            bd.end.x = bc.pageX || bc.clientX;
            bd.end.y = bc.pageY || bc.clientY;
            return bd
        }
        function ad(bd) {
            for (var bc = 0; bc < aR.length; bc++) {
                if (aR[bc].identifier == bd) {
                    return aR[bc]
                }
            }
        }
        function ak() {
            var bc = [];
            for (var bd = 0; bd <= 5; bd++) {
                bc.push({
                    start: {
                        x: 0,
                        y: 0
                    },
                    end: {
                        x: 0,
                        y: 0
                    },
                    identifier: 0
                })
            }
            return bc
        }
        function aJ(bc, bd) {
            bd = Math.max(bd, aU(bc));
            N[bc].distance = bd
        }
        function aU(bc) {
            if (N[bc]) {
                return N[bc].distance
            }
            return undefined
        }
        function ab() {
            var bc = {};
            bc[p] = ax(p);
            bc[o] = ax(o);
            bc[e] = ax(e);
            bc[x] = ax(x);
            return bc
        }
        function ax(bc) {
            return {
                direction: bc,
                distance: 0
            }
        }
        function aN() {
            return a3 - U
        }
        function av(bf, be) {
            var bd = Math.abs(bf.x - be.x);
            var bc = Math.abs(bf.y - be.y);
            return Math.round(Math.sqrt(bd * bd + bc * bc))
        }
        function a8(bc, bd) {
            var be = (bd / bc) * 1;
            return be.toFixed(2)
        }
        function at() {
            if (H < 1) {
                return A
            } else {
                return c
            }
        }
        function aT(bd, bc) {
            return Math.round(Math.sqrt(Math.pow(bc.x - bd.x, 2) + Math.pow(bc.y - bd.y, 2)))
        }
        function aF(bf, bd) {
            var bc = bf.x - bd.x;
            var bh = bd.y - bf.y;
            var be = Math.atan2(bh, bc);
            var bg = Math.round(be * 180 / Math.PI);
            if (bg < 0) {
                bg = 360 - Math.abs(bg)
            }
            return bg
        }
        function aM(bd, bc) {
            var be = aF(bd, bc);
            if ((be <= 45) && (be >= 0)) {
                return p
            } else {
                if ((be <= 360) && (be >= 315)) {
                    return p
                } else {
                    if ((be >= 135) && (be <= 225)) {
                        return o
                    } else {
                        if ((be > 45) && (be < 135)) {
                            return x
                        } else {
                            return e
                        }
                    }
                }
            }
        }
        function au() {
            var bc = new Date();
            return bc.getTime()
        }
        function aZ(bc) {
            bc = f(bc);
            var be = bc.offset();
            var bd = {
                left: be.left,
                right: be.left + bc.outerWidth(),
                top: be.top,
                bottom: be.top + bc.outerHeight()
            };
            return bd
        }
        function F(bc, bd) {
            return (bc.x > bd.left && bc.x < bd.right && bc.y > bd.top && bc.y < bd.bottom)
        }
    }
}));
if (typeof (console) === 'undefined') {
    var console = {};
    console.log = console.error = console.info = console.debug = console.warn = console.trace = console.dir = console.dirxml = console.group = console.groupEnd = console.time = console.timeEnd = console.assert = console.profile = console.groupCollapsed = function() {}
    ;
}
if (window.tplogs == true)
    try {
        console.groupCollapsed("ThemePunch GreenSocks Logs");
    } catch (e) {}
var oldgs = window.GreenSockGlobals;
oldgs_queue = window._gsQueue;
var punchgs = window.GreenSockGlobals = {};
if (window.tplogs == true)
    try {
        console.info("Build GreenSock SandBox for ThemePunch Plugins");
        console.info("GreenSock TweenLite Engine Initalised by ThemePunch Plugin");
    } catch (e) {}
/*!
* VERSION: 1.18.5
* DATE: 2016-05-24
* UPDATES AND DOCS AT: http://greensock.com
*
* @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
* This work is subject to the terms at http://greensock.com/standard-license or for
* Club GreenSock members, the software agreement that was issued with your membership.
*
* @author: Jack Doyle, jack@greensock.com
*/
!function(a, b) {
    "use strict";
    var c = {}
      , d = a.GreenSockGlobals = a.GreenSockGlobals || a;
    if (!d.TweenLite) {
        var e, f, g, h, i, j = function(a) {
            var b, c = a.split("."), e = d;
            for (b = 0; b < c.length; b++)
                e[c[b]] = e = e[c[b]] || {};
            return e
        }, k = j("com.greensock"), l = 1e-10, m = function(a) {
            var b, c = [], d = a.length;
            for (b = 0; b !== d; c.push(a[b++]))
                ;
            return c
        }, n = function() {}, o = function() {
            var a = Object.prototype.toString
              , b = a.call([]);
            return function(c) {
                return null != c && (c instanceof Array || "object" == typeof c && !!c.push && a.call(c) === b)
            }
        }(), p = {}, q = function(e, f, g, h) {
            this.sc = p[e] ? p[e].sc : [],
            p[e] = this,
            this.gsClass = null,
            this.func = g;
            var i = [];
            this.check = function(k) {
                for (var l, m, n, o, r, s = f.length, t = s; --s > -1; )
                    (l = p[f[s]] || new q(f[s],[])).gsClass ? (i[s] = l.gsClass,
                    t--) : k && l.sc.push(this);
                if (0 === t && g) {
                    if (m = ("com.greensock." + e).split("."),
                    n = m.pop(),
                    o = j(m.join("."))[n] = this.gsClass = g.apply(g, i),
                    h)
                        if (d[n] = o,
                        r = "undefined" != typeof module && module.exports,
                        !r && "function" == typeof define && define.amd)
                            define((a.GreenSockAMDPath ? a.GreenSockAMDPath + "/" : "") + e.split(".").pop(), [], function() {
                                return o
                            });
                        else if (r)
                            if (e === b) {
                                module.exports = c[b] = o;
                                for (s in c)
                                    o[s] = c[s]
                            } else
                                c[b] && (c[b][n] = o);
                    for (s = 0; s < this.sc.length; s++)
                        this.sc[s].check()
                }
            }
            ,
            this.check(!0)
        }, r = a._gsDefine = function(a, b, c, d) {
            return new q(a,b,c,d)
        }
        , s = k._class = function(a, b, c) {
            return b = b || function() {}
            ,
            r(a, [], function() {
                return b
            }, c),
            b
        }
        ;
        r.globals = d;
        var t = [0, 0, 1, 1]
          , u = []
          , v = s("easing.Ease", function(a, b, c, d) {
            this._func = a,
            this._type = c || 0,
            this._power = d || 0,
            this._params = b ? t.concat(b) : t
        }, !0)
          , w = v.map = {}
          , x = v.register = function(a, b, c, d) {
            for (var e, f, g, h, i = b.split(","), j = i.length, l = (c || "easeIn,easeOut,easeInOut").split(","); --j > -1; )
                for (f = i[j],
                e = d ? s("easing." + f, null, !0) : k.easing[f] || {},
                g = l.length; --g > -1; )
                    h = l[g],
                    w[f + "." + h] = w[h + f] = e[h] = a.getRatio ? a : a[h] || new a
        }
        ;
        for (g = v.prototype,
        g._calcEnd = !1,
        g.getRatio = function(a) {
            if (this._func)
                return this._params[0] = a,
                this._func.apply(null, this._params);
            var b = this._type
              , c = this._power
              , d = 1 === b ? 1 - a : 2 === b ? a : .5 > a ? 2 * a : 2 * (1 - a);
            return 1 === c ? d *= d : 2 === c ? d *= d * d : 3 === c ? d *= d * d * d : 4 === c && (d *= d * d * d * d),
            1 === b ? 1 - d : 2 === b ? d : .5 > a ? d / 2 : 1 - d / 2
        }
        ,
        e = ["Linear", "Quad", "Cubic", "Quart", "Quint,Strong"],
        f = e.length; --f > -1; )
            g = e[f] + ",Power" + f,
            x(new v(null,null,1,f), g, "easeOut", !0),
            x(new v(null,null,2,f), g, "easeIn" + (0 === f ? ",easeNone" : "")),
            x(new v(null,null,3,f), g, "easeInOut");
        w.linear = k.easing.Linear.easeIn,
        w.swing = k.easing.Quad.easeInOut;
        var y = s("events.EventDispatcher", function(a) {
            this._listeners = {},
            this._eventTarget = a || this
        });
        g = y.prototype,
        g.addEventListener = function(a, b, c, d, e) {
            e = e || 0;
            var f, g, j = this._listeners[a], k = 0;
            for (this !== h || i || h.wake(),
            null == j && (this._listeners[a] = j = []),
            g = j.length; --g > -1; )
                f = j[g],
                f.c === b && f.s === c ? j.splice(g, 1) : 0 === k && f.pr < e && (k = g + 1);
            j.splice(k, 0, {
                c: b,
                s: c,
                up: d,
                pr: e
            })
        }
        ,
        g.removeEventListener = function(a, b) {
            var c, d = this._listeners[a];
            if (d)
                for (c = d.length; --c > -1; )
                    if (d[c].c === b)
                        return void d.splice(c, 1)
        }
        ,
        g.dispatchEvent = function(a) {
            var b, c, d, e = this._listeners[a];
            if (e)
                for (b = e.length,
                c = this._eventTarget; --b > -1; )
                    d = e[b],
                    d && (d.up ? d.c.call(d.s || c, {
                        type: a,
                        target: c
                    }) : d.c.call(d.s || c))
        }
        ;
        var z = a.requestAnimationFrame
          , A = a.cancelAnimationFrame
          , B = Date.now || function() {
            return (new Date).getTime()
        }
          , C = B();
        for (e = ["ms", "moz", "webkit", "o"],
        f = e.length; --f > -1 && !z; )
            z = a[e[f] + "RequestAnimationFrame"],
            A = a[e[f] + "CancelAnimationFrame"] || a[e[f] + "CancelRequestAnimationFrame"];
        s("Ticker", function(a, b) {
            var c, d, e, f, g, j = this, k = B(), m = b !== !1 && z ? "auto" : !1, o = 500, p = 33, q = "tick", r = function(a) {
                var b, h, i = B() - C;
                i > o && (k += i - p),
                C += i,
                j.time = (C - k) / 1e3,
                b = j.time - g,
                (!c || b > 0 || a === !0) && (j.frame++,
                g += b + (b >= f ? .004 : f - b),
                h = !0),
                a !== !0 && (e = d(r)),
                h && j.dispatchEvent(q)
            };
            y.call(j),
            j.time = j.frame = 0,
            j.tick = function() {
                r(!0)
            }
            ,
            j.lagSmoothing = function(a, b) {
                o = a || 1 / l,
                p = Math.min(b, o, 0)
            }
            ,
            j.sleep = function() {
                null != e && (m && A ? A(e) : clearTimeout(e),
                d = n,
                e = null,
                j === h && (i = !1))
            }
            ,
            j.wake = function(a) {
                null !== e ? j.sleep() : a ? k += -C + (C = B()) : j.frame > 10 && (C = B() - o + 5),
                d = 0 === c ? n : m && z ? z : function(a) {
                    return setTimeout(a, 1e3 * (g - j.time) + 1 | 0)
                }
                ,
                j === h && (i = !0),
                r(2)
            }
            ,
            j.fps = function(a) {
                return arguments.length ? (c = a,
                f = 1 / (c || 60),
                g = this.time + f,
                void j.wake()) : c
            }
            ,
            j.useRAF = function(a) {
                return arguments.length ? (j.sleep(),
                m = a,
                void j.fps(c)) : m
            }
            ,
            j.fps(a),
            setTimeout(function() {
                "auto" === m && j.frame < 5 && "hidden" !== document.visibilityState && j.useRAF(!1)
            }, 1500)
        }),
        g = k.Ticker.prototype = new k.events.EventDispatcher,
        g.constructor = k.Ticker;
        var D = s("core.Animation", function(a, b) {
            if (this.vars = b = b || {},
            this._duration = this._totalDuration = a || 0,
            this._delay = Number(b.delay) || 0,
            this._timeScale = 1,
            this._active = b.immediateRender === !0,
            this.data = b.data,
            this._reversed = b.reversed === !0,
            W) {
                i || h.wake();
                var c = this.vars.useFrames ? V : W;
                c.add(this, c._time),
                this.vars.paused && this.paused(!0)
            }
        });
        h = D.ticker = new k.Ticker,
        g = D.prototype,
        g._dirty = g._gc = g._initted = g._paused = !1,
        g._totalTime = g._time = 0,
        g._rawPrevTime = -1,
        g._next = g._last = g._onUpdate = g._timeline = g.timeline = null,
        g._paused = !1;
        var E = function() {
            i && B() - C > 2e3 && h.wake(),
            setTimeout(E, 2e3)
        };
        E(),
        g.play = function(a, b) {
            return null != a && this.seek(a, b),
            this.reversed(!1).paused(!1)
        }
        ,
        g.pause = function(a, b) {
            return null != a && this.seek(a, b),
            this.paused(!0)
        }
        ,
        g.resume = function(a, b) {
            return null != a && this.seek(a, b),
            this.paused(!1)
        }
        ,
        g.seek = function(a, b) {
            return this.totalTime(Number(a), b !== !1)
        }
        ,
        g.restart = function(a, b) {
            return this.reversed(!1).paused(!1).totalTime(a ? -this._delay : 0, b !== !1, !0)
        }
        ,
        g.reverse = function(a, b) {
            return null != a && this.seek(a || this.totalDuration(), b),
            this.reversed(!0).paused(!1)
        }
        ,
        g.render = function(a, b, c) {}
        ,
        g.invalidate = function() {
            return this._time = this._totalTime = 0,
            this._initted = this._gc = !1,
            this._rawPrevTime = -1,
            (this._gc || !this.timeline) && this._enabled(!0),
            this
        }
        ,
        g.isActive = function() {
            var a, b = this._timeline, c = this._startTime;
            return !b || !this._gc && !this._paused && b.isActive() && (a = b.rawTime()) >= c && a < c + this.totalDuration() / this._timeScale
        }
        ,
        g._enabled = function(a, b) {
            return i || h.wake(),
            this._gc = !a,
            this._active = this.isActive(),
            b !== !0 && (a && !this.timeline ? this._timeline.add(this, this._startTime - this._delay) : !a && this.timeline && this._timeline._remove(this, !0)),
            !1
        }
        ,
        g._kill = function(a, b) {
            return this._enabled(!1, !1)
        }
        ,
        g.kill = function(a, b) {
            return this._kill(a, b),
            this
        }
        ,
        g._uncache = function(a) {
            for (var b = a ? this : this.timeline; b; )
                b._dirty = !0,
                b = b.timeline;
            return this
        }
        ,
        g._swapSelfInParams = function(a) {
            for (var b = a.length, c = a.concat(); --b > -1; )
                "{self}" === a[b] && (c[b] = this);
            return c
        }
        ,
        g._callback = function(a) {
            var b = this.vars;
            b[a].apply(b[a + "Scope"] || b.callbackScope || this, b[a + "Params"] || u)
        }
        ,
        g.eventCallback = function(a, b, c, d) {
            if ("on" === (a || "").substr(0, 2)) {
                var e = this.vars;
                if (1 === arguments.length)
                    return e[a];
                null == b ? delete e[a] : (e[a] = b,
                e[a + "Params"] = o(c) && -1 !== c.join("").indexOf("{self}") ? this._swapSelfInParams(c) : c,
                e[a + "Scope"] = d),
                "onUpdate" === a && (this._onUpdate = b)
            }
            return this
        }
        ,
        g.delay = function(a) {
            return arguments.length ? (this._timeline.smoothChildTiming && this.startTime(this._startTime + a - this._delay),
            this._delay = a,
            this) : this._delay
        }
        ,
        g.duration = function(a) {
            return arguments.length ? (this._duration = this._totalDuration = a,
            this._uncache(!0),
            this._timeline.smoothChildTiming && this._time > 0 && this._time < this._duration && 0 !== a && this.totalTime(this._totalTime * (a / this._duration), !0),
            this) : (this._dirty = !1,
            this._duration)
        }
        ,
        g.totalDuration = function(a) {
            return this._dirty = !1,
            arguments.length ? this.duration(a) : this._totalDuration
        }
        ,
        g.time = function(a, b) {
            return arguments.length ? (this._dirty && this.totalDuration(),
            this.totalTime(a > this._duration ? this._duration : a, b)) : this._time
        }
        ,
        g.totalTime = function(a, b, c) {
            if (i || h.wake(),
            !arguments.length)
                return this._totalTime;
            if (this._timeline) {
                if (0 > a && !c && (a += this.totalDuration()),
                this._timeline.smoothChildTiming) {
                    this._dirty && this.totalDuration();
                    var d = this._totalDuration
                      , e = this._timeline;
                    if (a > d && !c && (a = d),
                    this._startTime = (this._paused ? this._pauseTime : e._time) - (this._reversed ? d - a : a) / this._timeScale,
                    e._dirty || this._uncache(!1),
                    e._timeline)
                        for (; e._timeline; )
                            e._timeline._time !== (e._startTime + e._totalTime) / e._timeScale && e.totalTime(e._totalTime, !0),
                            e = e._timeline
                }
                this._gc && this._enabled(!0, !1),
                (this._totalTime !== a || 0 === this._duration) && (J.length && Y(),
                this.render(a, b, !1),
                J.length && Y())
            }
            return this
        }
        ,
        g.progress = g.totalProgress = function(a, b) {
            var c = this.duration();
            return arguments.length ? this.totalTime(c * a, b) : c ? this._time / c : this.ratio
        }
        ,
        g.startTime = function(a) {
            return arguments.length ? (a !== this._startTime && (this._startTime = a,
            this.timeline && this.timeline._sortChildren && this.timeline.add(this, a - this._delay)),
            this) : this._startTime
        }
        ,
        g.endTime = function(a) {
            return this._startTime + (0 != a ? this.totalDuration() : this.duration()) / this._timeScale
        }
        ,
        g.timeScale = function(a) {
            if (!arguments.length)
                return this._timeScale;
            if (a = a || l,
            this._timeline && this._timeline.smoothChildTiming) {
                var b = this._pauseTime
                  , c = b || 0 === b ? b : this._timeline.totalTime();
                this._startTime = c - (c - this._startTime) * this._timeScale / a
            }
            return this._timeScale = a,
            this._uncache(!1)
        }
        ,
        g.reversed = function(a) {
            return arguments.length ? (a != this._reversed && (this._reversed = a,
            this.totalTime(this._timeline && !this._timeline.smoothChildTiming ? this.totalDuration() - this._totalTime : this._totalTime, !0)),
            this) : this._reversed
        }
        ,
        g.paused = function(a) {
            if (!arguments.length)
                return this._paused;
            var b, c, d = this._timeline;
            return a != this._paused && d && (i || a || h.wake(),
            b = d.rawTime(),
            c = b - this._pauseTime,
            !a && d.smoothChildTiming && (this._startTime += c,
            this._uncache(!1)),
            this._pauseTime = a ? b : null,
            this._paused = a,
            this._active = this.isActive(),
            !a && 0 !== c && this._initted && this.duration() && (b = d.smoothChildTiming ? this._totalTime : (b - this._startTime) / this._timeScale,
            this.render(b, b === this._totalTime, !0))),
            this._gc && !a && this._enabled(!0, !1),
            this
        }
        ;
        var F = s("core.SimpleTimeline", function(a) {
            D.call(this, 0, a),
            this.autoRemoveChildren = this.smoothChildTiming = !0
        });
        g = F.prototype = new D,
        g.constructor = F,
        g.kill()._gc = !1,
        g._first = g._last = g._recent = null,
        g._sortChildren = !1,
        g.add = g.insert = function(a, b, c, d) {
            var e, f;
            if (a._startTime = Number(b || 0) + a._delay,
            a._paused && this !== a._timeline && (a._pauseTime = a._startTime + (this.rawTime() - a._startTime) / a._timeScale),
            a.timeline && a.timeline._remove(a, !0),
            a.timeline = a._timeline = this,
            a._gc && a._enabled(!0, !0),
            e = this._last,
            this._sortChildren)
                for (f = a._startTime; e && e._startTime > f; )
                    e = e._prev;
            return e ? (a._next = e._next,
            e._next = a) : (a._next = this._first,
            this._first = a),
            a._next ? a._next._prev = a : this._last = a,
            a._prev = e,
            this._recent = a,
            this._timeline && this._uncache(!0),
            this
        }
        ,
        g._remove = function(a, b) {
            return a.timeline === this && (b || a._enabled(!1, !0),
            a._prev ? a._prev._next = a._next : this._first === a && (this._first = a._next),
            a._next ? a._next._prev = a._prev : this._last === a && (this._last = a._prev),
            a._next = a._prev = a.timeline = null,
            a === this._recent && (this._recent = this._last),
            this._timeline && this._uncache(!0)),
            this
        }
        ,
        g.render = function(a, b, c) {
            var d, e = this._first;
            for (this._totalTime = this._time = this._rawPrevTime = a; e; )
                d = e._next,
                (e._active || a >= e._startTime && !e._paused) && (e._reversed ? e.render((e._dirty ? e.totalDuration() : e._totalDuration) - (a - e._startTime) * e._timeScale, b, c) : e.render((a - e._startTime) * e._timeScale, b, c)),
                e = d
        }
        ,
        g.rawTime = function() {
            return i || h.wake(),
            this._totalTime
        }
        ;
        var G = s("TweenLite", function(b, c, d) {
            if (D.call(this, c, d),
            this.render = G.prototype.render,
            null == b)
                throw "Cannot tween a null target.";
            this.target = b = "string" != typeof b ? b : G.selector(b) || b;
            var e, f, g, h = b.jquery || b.length && b !== a && b[0] && (b[0] === a || b[0].nodeType && b[0].style && !b.nodeType), i = this.vars.overwrite;
            if (this._overwrite = i = null == i ? U[G.defaultOverwrite] : "number" == typeof i ? i >> 0 : U[i],
            (h || b instanceof Array || b.push && o(b)) && "number" != typeof b[0])
                for (this._targets = g = m(b),
                this._propLookup = [],
                this._siblings = [],
                e = 0; e < g.length; e++)
                    f = g[e],
                    f ? "string" != typeof f ? f.length && f !== a && f[0] && (f[0] === a || f[0].nodeType && f[0].style && !f.nodeType) ? (g.splice(e--, 1),
                    this._targets = g = g.concat(m(f))) : (this._siblings[e] = Z(f, this, !1),
                    1 === i && this._siblings[e].length > 1 && _(f, this, null, 1, this._siblings[e])) : (f = g[e--] = G.selector(f),
                    "string" == typeof f && g.splice(e + 1, 1)) : g.splice(e--, 1);
            else
                this._propLookup = {},
                this._siblings = Z(b, this, !1),
                1 === i && this._siblings.length > 1 && _(b, this, null, 1, this._siblings);
            (this.vars.immediateRender || 0 === c && 0 === this._delay && this.vars.immediateRender !== !1) && (this._time = -l,
            this.render(Math.min(0, -this._delay)))
        }, !0)
          , H = function(b) {
            return b && b.length && b !== a && b[0] && (b[0] === a || b[0].nodeType && b[0].style && !b.nodeType)
        }
          , I = function(a, b) {
            var c, d = {};
            for (c in a)
                T[c] || c in b && "transform" !== c && "x" !== c && "y" !== c && "width" !== c && "height" !== c && "className" !== c && "border" !== c || !(!Q[c] || Q[c] && Q[c]._autoCSS) || (d[c] = a[c],
                delete a[c]);
            a.css = d
        };
        g = G.prototype = new D,
        g.constructor = G,
        g.kill()._gc = !1,
        g.ratio = 0,
        g._firstPT = g._targets = g._overwrittenProps = g._startAt = null,
        g._notifyPluginsOfEnabled = g._lazy = !1,
        G.version = "1.18.5",
        G.defaultEase = g._ease = new v(null,null,1,1),
        G.defaultOverwrite = "auto",
        G.ticker = h,
        G.autoSleep = 120,
        G.lagSmoothing = function(a, b) {
            h.lagSmoothing(a, b)
        }
        ,
        G.selector = a.$ || a.jQuery || function(b) {
            var c = a.$ || a.jQuery;
            return c ? (G.selector = c,
            c(b)) : "undefined" == typeof document ? b : document.querySelectorAll ? document.querySelectorAll(b) : document.getElementById("#" === b.charAt(0) ? b.substr(1) : b)
        }
        ;
        var J = []
          , K = {}
          , L = /(?:(-|-=|\+=)?\d*\.?\d*(?:e[\-+]?\d+)?)[0-9]/gi
          , M = function(a) {
            for (var b, c = this._firstPT, d = 1e-6; c; )
                b = c.blob ? a ? this.join("") : this.start : c.c * a + c.s,
                c.r ? b = Math.round(b) : d > b && b > -d && (b = 0),
                c.f ? c.fp ? c.t[c.p](c.fp, b) : c.t[c.p](b) : c.t[c.p] = b,
                c = c._next
        }
          , N = function(a, b, c, d) {
            var e, f, g, h, i, j, k, l = [a, b], m = 0, n = "", o = 0;
            for (l.start = a,
            c && (c(l),
            a = l[0],
            b = l[1]),
            l.length = 0,
            e = a.match(L) || [],
            f = b.match(L) || [],
            d && (d._next = null,
            d.blob = 1,
            l._firstPT = d),
            i = f.length,
            h = 0; i > h; h++)
                k = f[h],
                j = b.substr(m, b.indexOf(k, m) - m),
                n += j || !h ? j : ",",
                m += j.length,
                o ? o = (o + 1) % 5 : "rgba(" === j.substr(-5) && (o = 1),
                k === e[h] || e.length <= h ? n += k : (n && (l.push(n),
                n = ""),
                g = parseFloat(e[h]),
                l.push(g),
                l._firstPT = {
                    _next: l._firstPT,
                    t: l,
                    p: l.length - 1,
                    s: g,
                    c: ("=" === k.charAt(1) ? parseInt(k.charAt(0) + "1", 10) * parseFloat(k.substr(2)) : parseFloat(k) - g) || 0,
                    f: 0,
                    r: o && 4 > o
                }),
                m += k.length;
            return n += b.substr(m),
            n && l.push(n),
            l.setRatio = M,
            l
        }
          , O = function(a, b, c, d, e, f, g, h) {
            var i, j, k = "get" === c ? a[b] : c, l = typeof a[b], m = "string" == typeof d && "=" === d.charAt(1), n = {
                t: a,
                p: b,
                s: k,
                f: "function" === l,
                pg: 0,
                n: e || b,
                r: f,
                pr: 0,
                c: m ? parseInt(d.charAt(0) + "1", 10) * parseFloat(d.substr(2)) : parseFloat(d) - k || 0
            };
            return "number" !== l && ("function" === l && "get" === c && (j = b.indexOf("set") || "function" != typeof a["get" + b.substr(3)] ? b : "get" + b.substr(3),
            n.s = k = g ? a[j](g) : a[j]()),
            "string" == typeof k && (g || isNaN(k)) ? (n.fp = g,
            i = N(k, d, h || G.defaultStringFilter, n),
            n = {
                t: i,
                p: "setRatio",
                s: 0,
                c: 1,
                f: 2,
                pg: 0,
                n: e || b,
                pr: 0
            }) : m || (n.s = parseFloat(k),
            n.c = parseFloat(d) - n.s || 0)),
            n.c ? ((n._next = this._firstPT) && (n._next._prev = n),
            this._firstPT = n,
            n) : void 0
        }
          , P = G._internals = {
            isArray: o,
            isSelector: H,
            lazyTweens: J,
            blobDif: N
        }
          , Q = G._plugins = {}
          , R = P.tweenLookup = {}
          , S = 0
          , T = P.reservedProps = {
            ease: 1,
            delay: 1,
            overwrite: 1,
            onComplete: 1,
            onCompleteParams: 1,
            onCompleteScope: 1,
            useFrames: 1,
            runBackwards: 1,
            startAt: 1,
            onUpdate: 1,
            onUpdateParams: 1,
            onUpdateScope: 1,
            onStart: 1,
            onStartParams: 1,
            onStartScope: 1,
            onReverseComplete: 1,
            onReverseCompleteParams: 1,
            onReverseCompleteScope: 1,
            onRepeat: 1,
            onRepeatParams: 1,
            onRepeatScope: 1,
            easeParams: 1,
            yoyo: 1,
            immediateRender: 1,
            repeat: 1,
            repeatDelay: 1,
            data: 1,
            paused: 1,
            reversed: 1,
            autoCSS: 1,
            lazy: 1,
            onOverwrite: 1,
            callbackScope: 1,
            stringFilter: 1,
            id: 1
        }
          , U = {
            none: 0,
            all: 1,
            auto: 2,
            concurrent: 3,
            allOnStart: 4,
            preexisting: 5,
            "true": 1,
            "false": 0
        }
          , V = D._rootFramesTimeline = new F
          , W = D._rootTimeline = new F
          , X = 30
          , Y = P.lazyRender = function() {
            var a, b = J.length;
            for (K = {}; --b > -1; )
                a = J[b],
                a && a._lazy !== !1 && (a.render(a._lazy[0], a._lazy[1], !0),
                a._lazy = !1);
            J.length = 0
        }
        ;
        W._startTime = h.time,
        V._startTime = h.frame,
        W._active = V._active = !0,
        setTimeout(Y, 1),
        D._updateRoot = G.render = function() {
            var a, b, c;
            if (J.length && Y(),
            W.render((h.time - W._startTime) * W._timeScale, !1, !1),
            V.render((h.frame - V._startTime) * V._timeScale, !1, !1),
            J.length && Y(),
            h.frame >= X) {
                X = h.frame + (parseInt(G.autoSleep, 10) || 120);
                for (c in R) {
                    for (b = R[c].tweens,
                    a = b.length; --a > -1; )
                        b[a]._gc && b.splice(a, 1);
                    0 === b.length && delete R[c]
                }
                if (c = W._first,
                (!c || c._paused) && G.autoSleep && !V._first && 1 === h._listeners.tick.length) {
                    for (; c && c._paused; )
                        c = c._next;
                    c || h.sleep()
                }
            }
        }
        ,
        h.addEventListener("tick", D._updateRoot);
        var Z = function(a, b, c) {
            var d, e, f = a._gsTweenID;
            if (R[f || (a._gsTweenID = f = "t" + S++)] || (R[f] = {
                target: a,
                tweens: []
            }),
            b && (d = R[f].tweens,
            d[e = d.length] = b,
            c))
                for (; --e > -1; )
                    d[e] === b && d.splice(e, 1);
            return R[f].tweens
        }
          , $ = function(a, b, c, d) {
            var e, f, g = a.vars.onOverwrite;
            return g && (e = g(a, b, c, d)),
            g = G.onOverwrite,
            g && (f = g(a, b, c, d)),
            e !== !1 && f !== !1
        }
          , _ = function(a, b, c, d, e) {
            var f, g, h, i;
            if (1 === d || d >= 4) {
                for (i = e.length,
                f = 0; i > f; f++)
                    if ((h = e[f]) !== b)
                        h._gc || h._kill(null, a, b) && (g = !0);
                    else if (5 === d)
                        break;
                return g
            }
            var j, k = b._startTime + l, m = [], n = 0, o = 0 === b._duration;
            for (f = e.length; --f > -1; )
                (h = e[f]) === b || h._gc || h._paused || (h._timeline !== b._timeline ? (j = j || aa(b, 0, o),
                0 === aa(h, j, o) && (m[n++] = h)) : h._startTime <= k && h._startTime + h.totalDuration() / h._timeScale > k && ((o || !h._initted) && k - h._startTime <= 2e-10 || (m[n++] = h)));
            for (f = n; --f > -1; )
                if (h = m[f],
                2 === d && h._kill(c, a, b) && (g = !0),
                2 !== d || !h._firstPT && h._initted) {
                    if (2 !== d && !$(h, b))
                        continue;
                    h._enabled(!1, !1) && (g = !0)
                }
            return g
        }
          , aa = function(a, b, c) {
            for (var d = a._timeline, e = d._timeScale, f = a._startTime; d._timeline; ) {
                if (f += d._startTime,
                e *= d._timeScale,
                d._paused)
                    return -100;
                d = d._timeline
            }
            return f /= e,
            f > b ? f - b : c && f === b || !a._initted && 2 * l > f - b ? l : (f += a.totalDuration() / a._timeScale / e) > b + l ? 0 : f - b - l
        };
        g._init = function() {
            var a, b, c, d, e, f = this.vars, g = this._overwrittenProps, h = this._duration, i = !!f.immediateRender, j = f.ease;
            if (f.startAt) {
                this._startAt && (this._startAt.render(-1, !0),
                this._startAt.kill()),
                e = {};
                for (d in f.startAt)
                    e[d] = f.startAt[d];
                if (e.overwrite = !1,
                e.immediateRender = !0,
                e.lazy = i && f.lazy !== !1,
                e.startAt = e.delay = null,
                this._startAt = G.to(this.target, 0, e),
                i)
                    if (this._time > 0)
                        this._startAt = null;
                    else if (0 !== h)
                        return
            } else if (f.runBackwards && 0 !== h)
                if (this._startAt)
                    this._startAt.render(-1, !0),
                    this._startAt.kill(),
                    this._startAt = null;
                else {
                    0 !== this._time && (i = !1),
                    c = {};
                    for (d in f)
                        T[d] && "autoCSS" !== d || (c[d] = f[d]);
                    if (c.overwrite = 0,
                    c.data = "isFromStart",
                    c.lazy = i && f.lazy !== !1,
                    c.immediateRender = i,
                    this._startAt = G.to(this.target, 0, c),
                    i) {
                        if (0 === this._time)
                            return
                    } else
                        this._startAt._init(),
                        this._startAt._enabled(!1),
                        this.vars.immediateRender && (this._startAt = null)
                }
            if (this._ease = j = j ? j instanceof v ? j : "function" == typeof j ? new v(j,f.easeParams) : w[j] || G.defaultEase : G.defaultEase,
            f.easeParams instanceof Array && j.config && (this._ease = j.config.apply(j, f.easeParams)),
            this._easeType = this._ease._type,
            this._easePower = this._ease._power,
            this._firstPT = null,
            this._targets)
                for (a = this._targets.length; --a > -1; )
                    this._initProps(this._targets[a], this._propLookup[a] = {}, this._siblings[a], g ? g[a] : null) && (b = !0);
            else
                b = this._initProps(this.target, this._propLookup, this._siblings, g);
            if (b && G._onPluginEvent("_onInitAllProps", this),
            g && (this._firstPT || "function" != typeof this.target && this._enabled(!1, !1)),
            f.runBackwards)
                for (c = this._firstPT; c; )
                    c.s += c.c,
                    c.c = -c.c,
                    c = c._next;
            this._onUpdate = f.onUpdate,
            this._initted = !0
        }
        ,
        g._initProps = function(b, c, d, e) {
            var f, g, h, i, j, k;
            if (null == b)
                return !1;
            K[b._gsTweenID] && Y(),
            this.vars.css || b.style && b !== a && b.nodeType && Q.css && this.vars.autoCSS !== !1 && I(this.vars, b);
            for (f in this.vars)
                if (k = this.vars[f],
                T[f])
                    k && (k instanceof Array || k.push && o(k)) && -1 !== k.join("").indexOf("{self}") && (this.vars[f] = k = this._swapSelfInParams(k, this));
                else if (Q[f] && (i = new Q[f])._onInitTween(b, this.vars[f], this)) {
                    for (this._firstPT = j = {
                        _next: this._firstPT,
                        t: i,
                        p: "setRatio",
                        s: 0,
                        c: 1,
                        f: 1,
                        n: f,
                        pg: 1,
                        pr: i._priority
                    },
                    g = i._overwriteProps.length; --g > -1; )
                        c[i._overwriteProps[g]] = this._firstPT;
                    (i._priority || i._onInitAllProps) && (h = !0),
                    (i._onDisable || i._onEnable) && (this._notifyPluginsOfEnabled = !0),
                    j._next && (j._next._prev = j)
                } else
                    c[f] = O.call(this, b, f, "get", k, f, 0, null, this.vars.stringFilter);
            return e && this._kill(e, b) ? this._initProps(b, c, d, e) : this._overwrite > 1 && this._firstPT && d.length > 1 && _(b, this, c, this._overwrite, d) ? (this._kill(c, b),
            this._initProps(b, c, d, e)) : (this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration) && (K[b._gsTweenID] = !0),
            h)
        }
        ,
        g.render = function(a, b, c) {
            var d, e, f, g, h = this._time, i = this._duration, j = this._rawPrevTime;
            if (a >= i - 1e-7)
                this._totalTime = this._time = i,
                this.ratio = this._ease._calcEnd ? this._ease.getRatio(1) : 1,
                this._reversed || (d = !0,
                e = "onComplete",
                c = c || this._timeline.autoRemoveChildren),
                0 === i && (this._initted || !this.vars.lazy || c) && (this._startTime === this._timeline._duration && (a = 0),
                (0 > j || 0 >= a && a >= -1e-7 || j === l && "isPause" !== this.data) && j !== a && (c = !0,
                j > l && (e = "onReverseComplete")),
                this._rawPrevTime = g = !b || a || j === a ? a : l);
            else if (1e-7 > a)
                this._totalTime = this._time = 0,
                this.ratio = this._ease._calcEnd ? this._ease.getRatio(0) : 0,
                (0 !== h || 0 === i && j > 0) && (e = "onReverseComplete",
                d = this._reversed),
                0 > a && (this._active = !1,
                0 === i && (this._initted || !this.vars.lazy || c) && (j >= 0 && (j !== l || "isPause" !== this.data) && (c = !0),
                this._rawPrevTime = g = !b || a || j === a ? a : l)),
                this._initted || (c = !0);
            else if (this._totalTime = this._time = a,
            this._easeType) {
                var k = a / i
                  , m = this._easeType
                  , n = this._easePower;
                (1 === m || 3 === m && k >= .5) && (k = 1 - k),
                3 === m && (k *= 2),
                1 === n ? k *= k : 2 === n ? k *= k * k : 3 === n ? k *= k * k * k : 4 === n && (k *= k * k * k * k),
                1 === m ? this.ratio = 1 - k : 2 === m ? this.ratio = k : .5 > a / i ? this.ratio = k / 2 : this.ratio = 1 - k / 2
            } else
                this.ratio = this._ease.getRatio(a / i);
            if (this._time !== h || c) {
                if (!this._initted) {
                    if (this._init(),
                    !this._initted || this._gc)
                        return;
                    if (!c && this._firstPT && (this.vars.lazy !== !1 && this._duration || this.vars.lazy && !this._duration))
                        return this._time = this._totalTime = h,
                        this._rawPrevTime = j,
                        J.push(this),
                        void (this._lazy = [a, b]);
                    this._time && !d ? this.ratio = this._ease.getRatio(this._time / i) : d && this._ease._calcEnd && (this.ratio = this._ease.getRatio(0 === this._time ? 0 : 1))
                }
                for (this._lazy !== !1 && (this._lazy = !1),
                this._active || !this._paused && this._time !== h && a >= 0 && (this._active = !0),
                0 === h && (this._startAt && (a >= 0 ? this._startAt.render(a, b, c) : e || (e = "_dummyGS")),
                this.vars.onStart && (0 !== this._time || 0 === i) && (b || this._callback("onStart"))),
                f = this._firstPT; f; )
                    f.f ? f.t[f.p](f.c * this.ratio + f.s) : f.t[f.p] = f.c * this.ratio + f.s,
                    f = f._next;
                this._onUpdate && (0 > a && this._startAt && a !== -1e-4 && this._startAt.render(a, b, c),
                b || (this._time !== h || d || c) && this._callback("onUpdate")),
                e && (!this._gc || c) && (0 > a && this._startAt && !this._onUpdate && a !== -1e-4 && this._startAt.render(a, b, c),
                d && (this._timeline.autoRemoveChildren && this._enabled(!1, !1),
                this._active = !1),
                !b && this.vars[e] && this._callback(e),
                0 === i && this._rawPrevTime === l && g !== l && (this._rawPrevTime = 0))
            }
        }
        ,
        g._kill = function(a, b, c) {
            if ("all" === a && (a = null),
            null == a && (null == b || b === this.target))
                return this._lazy = !1,
                this._enabled(!1, !1);
            b = "string" != typeof b ? b || this._targets || this.target : G.selector(b) || b;
            var d, e, f, g, h, i, j, k, l, m = c && this._time && c._startTime === this._startTime && this._timeline === c._timeline;
            if ((o(b) || H(b)) && "number" != typeof b[0])
                for (d = b.length; --d > -1; )
                    this._kill(a, b[d], c) && (i = !0);
            else {
                if (this._targets) {
                    for (d = this._targets.length; --d > -1; )
                        if (b === this._targets[d]) {
                            h = this._propLookup[d] || {},
                            this._overwrittenProps = this._overwrittenProps || [],
                            e = this._overwrittenProps[d] = a ? this._overwrittenProps[d] || {} : "all";
                            break
                        }
                } else {
                    if (b !== this.target)
                        return !1;
                    h = this._propLookup,
                    e = this._overwrittenProps = a ? this._overwrittenProps || {} : "all"
                }
                if (h) {
                    if (j = a || h,
                    k = a !== e && "all" !== e && a !== h && ("object" != typeof a || !a._tempKill),
                    c && (G.onOverwrite || this.vars.onOverwrite)) {
                        for (f in j)
                            h[f] && (l || (l = []),
                            l.push(f));
                        if ((l || !a) && !$(this, c, b, l))
                            return !1
                    }
                    for (f in j)
                        (g = h[f]) && (m && (g.f ? g.t[g.p](g.s) : g.t[g.p] = g.s,
                        i = !0),
                        g.pg && g.t._kill(j) && (i = !0),
                        g.pg && 0 !== g.t._overwriteProps.length || (g._prev ? g._prev._next = g._next : g === this._firstPT && (this._firstPT = g._next),
                        g._next && (g._next._prev = g._prev),
                        g._next = g._prev = null),
                        delete h[f]),
                        k && (e[f] = 1);
                    !this._firstPT && this._initted && this._enabled(!1, !1)
                }
            }
            return i
        }
        ,
        g.invalidate = function() {
            return this._notifyPluginsOfEnabled && G._onPluginEvent("_onDisable", this),
            this._firstPT = this._overwrittenProps = this._startAt = this._onUpdate = null,
            this._notifyPluginsOfEnabled = this._active = this._lazy = !1,
            this._propLookup = this._targets ? {} : [],
            D.prototype.invalidate.call(this),
            this.vars.immediateRender && (this._time = -l,
            this.render(Math.min(0, -this._delay))),
            this
        }
        ,
        g._enabled = function(a, b) {
            if (i || h.wake(),
            a && this._gc) {
                var c, d = this._targets;
                if (d)
                    for (c = d.length; --c > -1; )
                        this._siblings[c] = Z(d[c], this, !0);
                else
                    this._siblings = Z(this.target, this, !0)
            }
            return D.prototype._enabled.call(this, a, b),
            this._notifyPluginsOfEnabled && this._firstPT ? G._onPluginEvent(a ? "_onEnable" : "_onDisable", this) : !1
        }
        ,
        G.to = function(a, b, c) {
            return new G(a,b,c)
        }
        ,
        G.from = function(a, b, c) {
            return c.runBackwards = !0,
            c.immediateRender = 0 != c.immediateRender,
            new G(a,b,c)
        }
        ,
        G.fromTo = function(a, b, c, d) {
            return d.startAt = c,
            d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender,
            new G(a,b,d)
        }
        ,
        G.delayedCall = function(a, b, c, d, e) {
            return new G(b,0,{
                delay: a,
                onComplete: b,
                onCompleteParams: c,
                callbackScope: d,
                onReverseComplete: b,
                onReverseCompleteParams: c,
                immediateRender: !1,
                lazy: !1,
                useFrames: e,
                overwrite: 0
            })
        }
        ,
        G.set = function(a, b) {
            return new G(a,0,b)
        }
        ,
        G.getTweensOf = function(a, b) {
            if (null == a)
                return [];
            a = "string" != typeof a ? a : G.selector(a) || a;
            var c, d, e, f;
            if ((o(a) || H(a)) && "number" != typeof a[0]) {
                for (c = a.length,
                d = []; --c > -1; )
                    d = d.concat(G.getTweensOf(a[c], b));
                for (c = d.length; --c > -1; )
                    for (f = d[c],
                    e = c; --e > -1; )
                        f === d[e] && d.splice(c, 1)
            } else
                for (d = Z(a).concat(),
                c = d.length; --c > -1; )
                    (d[c]._gc || b && !d[c].isActive()) && d.splice(c, 1);
            return d
        }
        ,
        G.killTweensOf = G.killDelayedCallsTo = function(a, b, c) {
            "object" == typeof b && (c = b,
            b = !1);
            for (var d = G.getTweensOf(a, b), e = d.length; --e > -1; )
                d[e]._kill(c, a)
        }
        ;
        var ba = s("plugins.TweenPlugin", function(a, b) {
            this._overwriteProps = (a || "").split(","),
            this._propName = this._overwriteProps[0],
            this._priority = b || 0,
            this._super = ba.prototype
        }, !0);
        if (g = ba.prototype,
        ba.version = "1.18.0",
        ba.API = 2,
        g._firstPT = null,
        g._addTween = O,
        g.setRatio = M,
        g._kill = function(a) {
            var b, c = this._overwriteProps, d = this._firstPT;
            if (null != a[this._propName])
                this._overwriteProps = [];
            else
                for (b = c.length; --b > -1; )
                    null != a[c[b]] && c.splice(b, 1);
            for (; d; )
                null != a[d.n] && (d._next && (d._next._prev = d._prev),
                d._prev ? (d._prev._next = d._next,
                d._prev = null) : this._firstPT === d && (this._firstPT = d._next)),
                d = d._next;
            return !1
        }
        ,
        g._roundProps = function(a, b) {
            for (var c = this._firstPT; c; )
                (a[this._propName] || null != c.n && a[c.n.split(this._propName + "_").join("")]) && (c.r = b),
                c = c._next
        }
        ,
        G._onPluginEvent = function(a, b) {
            var c, d, e, f, g, h = b._firstPT;
            if ("_onInitAllProps" === a) {
                for (; h; ) {
                    for (g = h._next,
                    d = e; d && d.pr > h.pr; )
                        d = d._next;
                    (h._prev = d ? d._prev : f) ? h._prev._next = h : e = h,
                    (h._next = d) ? d._prev = h : f = h,
                    h = g
                }
                h = b._firstPT = e
            }
            for (; h; )
                h.pg && "function" == typeof h.t[a] && h.t[a]() && (c = !0),
                h = h._next;
            return c
        }
        ,
        ba.activate = function(a) {
            for (var b = a.length; --b > -1; )
                a[b].API === ba.API && (Q[(new a[b])._propName] = a[b]);
            return !0
        }
        ,
        r.plugin = function(a) {
            if (!(a && a.propName && a.init && a.API))
                throw "illegal plugin definition.";
            var b, c = a.propName, d = a.priority || 0, e = a.overwriteProps, f = {
                init: "_onInitTween",
                set: "setRatio",
                kill: "_kill",
                round: "_roundProps",
                initAll: "_onInitAllProps"
            }, g = s("plugins." + c.charAt(0).toUpperCase() + c.substr(1) + "Plugin", function() {
                ba.call(this, c, d),
                this._overwriteProps = e || []
            }, a.global === !0), h = g.prototype = new ba(c);
            h.constructor = g,
            g.API = a.API;
            for (b in f)
                "function" == typeof a[b] && (h[f[b]] = a[b]);
            return g.version = a.version,
            ba.activate([g]),
            g
        }
        ,
        e = a._gsQueue) {
            for (f = 0; f < e.length; f++)
                e[f]();
            for (g in p)
                p[g].func || a.console.log("GSAP encountered missing dependency: com.greensock." + g)
        }
        i = !1
    }
}("undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window, "TweenLite");
/*!
* VERSION: 1.18.5
* DATE: 2016-05-24
* UPDATES AND DOCS AT: http://greensock.com
*
* @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
* This work is subject to the terms at http://greensock.com/standard-license or for
* Club GreenSock members, the software agreement that was issued with your membership.
*
* @author: Jack Doyle, jack@greensock.com
*/
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {
    "use strict";
    _gsScope._gsDefine("TimelineLite", ["core.Animation", "core.SimpleTimeline", "TweenLite"], function(a, b, c) {
        var d = function(a) {
            b.call(this, a),
            this._labels = {},
            this.autoRemoveChildren = this.vars.autoRemoveChildren === !0,
            this.smoothChildTiming = this.vars.smoothChildTiming === !0,
            this._sortChildren = !0,
            this._onUpdate = this.vars.onUpdate;
            var c, d, e = this.vars;
            for (d in e)
                c = e[d],
                i(c) && -1 !== c.join("").indexOf("{self}") && (e[d] = this._swapSelfInParams(c));
            i(e.tweens) && this.add(e.tweens, 0, e.align, e.stagger)
        }
          , e = 1e-10
          , f = c._internals
          , g = d._internals = {}
          , h = f.isSelector
          , i = f.isArray
          , j = f.lazyTweens
          , k = f.lazyRender
          , l = _gsScope._gsDefine.globals
          , m = function(a) {
            var b, c = {};
            for (b in a)
                c[b] = a[b];
            return c
        }
          , n = function(a, b, c) {
            var d, e, f = a.cycle;
            for (d in f)
                e = f[d],
                a[d] = "function" == typeof e ? e.call(b[c], c) : e[c % e.length];
            delete a.cycle
        }
          , o = g.pauseCallback = function() {}
          , p = function(a) {
            var b, c = [], d = a.length;
            for (b = 0; b !== d; c.push(a[b++]))
                ;
            return c
        }
          , q = d.prototype = new b;
        return d.version = "1.18.5",
        q.constructor = d,
        q.kill()._gc = q._forcingPlayhead = q._hasPause = !1,
        q.to = function(a, b, d, e) {
            var f = d.repeat && l.TweenMax || c;
            return b ? this.add(new f(a,b,d), e) : this.set(a, d, e)
        }
        ,
        q.from = function(a, b, d, e) {
            return this.add((d.repeat && l.TweenMax || c).from(a, b, d), e)
        }
        ,
        q.fromTo = function(a, b, d, e, f) {
            var g = e.repeat && l.TweenMax || c;
            return b ? this.add(g.fromTo(a, b, d, e), f) : this.set(a, e, f)
        }
        ,
        q.staggerTo = function(a, b, e, f, g, i, j, k) {
            var l, o, q = new d({
                onComplete: i,
                onCompleteParams: j,
                callbackScope: k,
                smoothChildTiming: this.smoothChildTiming
            }), r = e.cycle;
            for ("string" == typeof a && (a = c.selector(a) || a),
            a = a || [],
            h(a) && (a = p(a)),
            f = f || 0,
            0 > f && (a = p(a),
            a.reverse(),
            f *= -1),
            o = 0; o < a.length; o++)
                l = m(e),
                l.startAt && (l.startAt = m(l.startAt),
                l.startAt.cycle && n(l.startAt, a, o)),
                r && (n(l, a, o),
                null != l.duration && (b = l.duration,
                delete l.duration)),
                q.to(a[o], b, l, o * f);
            return this.add(q, g)
        }
        ,
        q.staggerFrom = function(a, b, c, d, e, f, g, h) {
            return c.immediateRender = 0 != c.immediateRender,
            c.runBackwards = !0,
            this.staggerTo(a, b, c, d, e, f, g, h)
        }
        ,
        q.staggerFromTo = function(a, b, c, d, e, f, g, h, i) {
            return d.startAt = c,
            d.immediateRender = 0 != d.immediateRender && 0 != c.immediateRender,
            this.staggerTo(a, b, d, e, f, g, h, i)
        }
        ,
        q.call = function(a, b, d, e) {
            return this.add(c.delayedCall(0, a, b, d), e)
        }
        ,
        q.set = function(a, b, d) {
            return d = this._parseTimeOrLabel(d, 0, !0),
            null == b.immediateRender && (b.immediateRender = d === this._time && !this._paused),
            this.add(new c(a,0,b), d)
        }
        ,
        d.exportRoot = function(a, b) {
            a = a || {},
            null == a.smoothChildTiming && (a.smoothChildTiming = !0);
            var e, f, g = new d(a), h = g._timeline;
            for (null == b && (b = !0),
            h._remove(g, !0),
            g._startTime = 0,
            g._rawPrevTime = g._time = g._totalTime = h._time,
            e = h._first; e; )
                f = e._next,
                b && e instanceof c && e.target === e.vars.onComplete || g.add(e, e._startTime - e._delay),
                e = f;
            return h.add(g, 0),
            g
        }
        ,
        q.add = function(e, f, g, h) {
            var j, k, l, m, n, o;
            if ("number" != typeof f && (f = this._parseTimeOrLabel(f, 0, !0, e)),
            !(e instanceof a)) {
                if (e instanceof Array || e && e.push && i(e)) {
                    for (g = g || "normal",
                    h = h || 0,
                    j = f,
                    k = e.length,
                    l = 0; k > l; l++)
                        i(m = e[l]) && (m = new d({
                            tweens: m
                        })),
                        this.add(m, j),
                        "string" != typeof m && "function" != typeof m && ("sequence" === g ? j = m._startTime + m.totalDuration() / m._timeScale : "start" === g && (m._startTime -= m.delay())),
                        j += h;
                    return this._uncache(!0)
                }
                if ("string" == typeof e)
                    return this.addLabel(e, f);
                if ("function" != typeof e)
                    throw "Cannot add " + e + " into the timeline; it is not a tween, timeline, function, or string.";
                e = c.delayedCall(0, e)
            }
            if (b.prototype.add.call(this, e, f),
            (this._gc || this._time === this._duration) && !this._paused && this._duration < this.duration())
                for (n = this,
                o = n.rawTime() > e._startTime; n._timeline; )
                    o && n._timeline.smoothChildTiming ? n.totalTime(n._totalTime, !0) : n._gc && n._enabled(!0, !1),
                    n = n._timeline;
            return this
        }
        ,
        q.remove = function(b) {
            if (b instanceof a) {
                this._remove(b, !1);
                var c = b._timeline = b.vars.useFrames ? a._rootFramesTimeline : a._rootTimeline;
                return b._startTime = (b._paused ? b._pauseTime : c._time) - (b._reversed ? b.totalDuration() - b._totalTime : b._totalTime) / b._timeScale,
                this
            }
            if (b instanceof Array || b && b.push && i(b)) {
                for (var d = b.length; --d > -1; )
                    this.remove(b[d]);
                return this
            }
            return "string" == typeof b ? this.removeLabel(b) : this.kill(null, b)
        }
        ,
        q._remove = function(a, c) {
            b.prototype._remove.call(this, a, c);
            var d = this._last;
            return d ? this._time > d._startTime + d._totalDuration / d._timeScale && (this._time = this.duration(),
            this._totalTime = this._totalDuration) : this._time = this._totalTime = this._duration = this._totalDuration = 0,
            this
        }
        ,
        q.append = function(a, b) {
            return this.add(a, this._parseTimeOrLabel(null, b, !0, a))
        }
        ,
        q.insert = q.insertMultiple = function(a, b, c, d) {
            return this.add(a, b || 0, c, d)
        }
        ,
        q.appendMultiple = function(a, b, c, d) {
            return this.add(a, this._parseTimeOrLabel(null, b, !0, a), c, d)
        }
        ,
        q.addLabel = function(a, b) {
            return this._labels[a] = this._parseTimeOrLabel(b),
            this
        }
        ,
        q.addPause = function(a, b, d, e) {
            var f = c.delayedCall(0, o, d, e || this);
            return f.vars.onComplete = f.vars.onReverseComplete = b,
            f.data = "isPause",
            this._hasPause = !0,
            this.add(f, a)
        }
        ,
        q.removeLabel = function(a) {
            return delete this._labels[a],
            this
        }
        ,
        q.getLabelTime = function(a) {
            return null != this._labels[a] ? this._labels[a] : -1
        }
        ,
        q._parseTimeOrLabel = function(b, c, d, e) {
            var f;
            if (e instanceof a && e.timeline === this)
                this.remove(e);
            else if (e && (e instanceof Array || e.push && i(e)))
                for (f = e.length; --f > -1; )
                    e[f]instanceof a && e[f].timeline === this && this.remove(e[f]);
            if ("string" == typeof c)
                return this._parseTimeOrLabel(c, d && "number" == typeof b && null == this._labels[c] ? b - this.duration() : 0, d);
            if (c = c || 0,
            "string" != typeof b || !isNaN(b) && null == this._labels[b])
                null == b && (b = this.duration());
            else {
                if (f = b.indexOf("="),
                -1 === f)
                    return null == this._labels[b] ? d ? this._labels[b] = this.duration() + c : c : this._labels[b] + c;
                c = parseInt(b.charAt(f - 1) + "1", 10) * Number(b.substr(f + 1)),
                b = f > 1 ? this._parseTimeOrLabel(b.substr(0, f - 1), 0, d) : this.duration()
            }
            return Number(b) + c
        }
        ,
        q.seek = function(a, b) {
            return this.totalTime("number" == typeof a ? a : this._parseTimeOrLabel(a), b !== !1)
        }
        ,
        q.stop = function() {
            return this.paused(!0)
        }
        ,
        q.gotoAndPlay = function(a, b) {
            return this.play(a, b)
        }
        ,
        q.gotoAndStop = function(a, b) {
            return this.pause(a, b)
        }
        ,
        q.render = function(a, b, c) {
            this._gc && this._enabled(!0, !1);
            var d, f, g, h, i, l, m, n = this._dirty ? this.totalDuration() : this._totalDuration, o = this._time, p = this._startTime, q = this._timeScale, r = this._paused;
            if (a >= n - 1e-7)
                this._totalTime = this._time = n,
                this._reversed || this._hasPausedChild() || (f = !0,
                h = "onComplete",
                i = !!this._timeline.autoRemoveChildren,
                0 === this._duration && (0 >= a && a >= -1e-7 || this._rawPrevTime < 0 || this._rawPrevTime === e) && this._rawPrevTime !== a && this._first && (i = !0,
                this._rawPrevTime > e && (h = "onReverseComplete"))),
                this._rawPrevTime = this._duration || !b || a || this._rawPrevTime === a ? a : e,
                a = n + 1e-4;
            else if (1e-7 > a)
                if (this._totalTime = this._time = 0,
                (0 !== o || 0 === this._duration && this._rawPrevTime !== e && (this._rawPrevTime > 0 || 0 > a && this._rawPrevTime >= 0)) && (h = "onReverseComplete",
                f = this._reversed),
                0 > a)
                    this._active = !1,
                    this._timeline.autoRemoveChildren && this._reversed ? (i = f = !0,
                    h = "onReverseComplete") : this._rawPrevTime >= 0 && this._first && (i = !0),
                    this._rawPrevTime = a;
                else {
                    if (this._rawPrevTime = this._duration || !b || a || this._rawPrevTime === a ? a : e,
                    0 === a && f)
                        for (d = this._first; d && 0 === d._startTime; )
                            d._duration || (f = !1),
                            d = d._next;
                    a = 0,
                    this._initted || (i = !0)
                }
            else {
                if (this._hasPause && !this._forcingPlayhead && !b) {
                    if (a >= o)
                        for (d = this._first; d && d._startTime <= a && !l; )
                            d._duration || "isPause" !== d.data || d.ratio || 0 === d._startTime && 0 === this._rawPrevTime || (l = d),
                            d = d._next;
                    else
                        for (d = this._last; d && d._startTime >= a && !l; )
                            d._duration || "isPause" === d.data && d._rawPrevTime > 0 && (l = d),
                            d = d._prev;
                    l && (this._time = a = l._startTime,
                    this._totalTime = a + this._cycle * (this._totalDuration + this._repeatDelay))
                }
                this._totalTime = this._time = this._rawPrevTime = a
            }
            if (this._time !== o && this._first || c || i || l) {
                if (this._initted || (this._initted = !0),
                this._active || !this._paused && this._time !== o && a > 0 && (this._active = !0),
                0 === o && this.vars.onStart && (0 === this._time && this._duration || b || this._callback("onStart")),
                m = this._time,
                m >= o)
                    for (d = this._first; d && (g = d._next,
                    m === this._time && (!this._paused || r)); )
                        (d._active || d._startTime <= m && !d._paused && !d._gc) && (l === d && this.pause(),
                        d._reversed ? d.render((d._dirty ? d.totalDuration() : d._totalDuration) - (a - d._startTime) * d._timeScale, b, c) : d.render((a - d._startTime) * d._timeScale, b, c)),
                        d = g;
                else
                    for (d = this._last; d && (g = d._prev,
                    m === this._time && (!this._paused || r)); ) {
                        if (d._active || d._startTime <= o && !d._paused && !d._gc) {
                            if (l === d) {
                                for (l = d._prev; l && l.endTime() > this._time; )
                                    l.render(l._reversed ? l.totalDuration() - (a - l._startTime) * l._timeScale : (a - l._startTime) * l._timeScale, b, c),
                                    l = l._prev;
                                l = null,
                                this.pause()
                            }
                            d._reversed ? d.render((d._dirty ? d.totalDuration() : d._totalDuration) - (a - d._startTime) * d._timeScale, b, c) : d.render((a - d._startTime) * d._timeScale, b, c)
                        }
                        d = g
                    }
                this._onUpdate && (b || (j.length && k(),
                this._callback("onUpdate"))),
                h && (this._gc || (p === this._startTime || q !== this._timeScale) && (0 === this._time || n >= this.totalDuration()) && (f && (j.length && k(),
                this._timeline.autoRemoveChildren && this._enabled(!1, !1),
                this._active = !1),
                !b && this.vars[h] && this._callback(h)))
            }
        }
        ,
        q._hasPausedChild = function() {
            for (var a = this._first; a; ) {
                if (a._paused || a instanceof d && a._hasPausedChild())
                    return !0;
                a = a._next
            }
            return !1
        }
        ,
        q.getChildren = function(a, b, d, e) {
            e = e || -9999999999;
            for (var f = [], g = this._first, h = 0; g; )
                g._startTime < e || (g instanceof c ? b !== !1 && (f[h++] = g) : (d !== !1 && (f[h++] = g),
                a !== !1 && (f = f.concat(g.getChildren(!0, b, d)),
                h = f.length))),
                g = g._next;
            return f
        }
        ,
        q.getTweensOf = function(a, b) {
            var d, e, f = this._gc, g = [], h = 0;
            for (f && this._enabled(!0, !0),
            d = c.getTweensOf(a),
            e = d.length; --e > -1; )
                (d[e].timeline === this || b && this._contains(d[e])) && (g[h++] = d[e]);
            return f && this._enabled(!1, !0),
            g
        }
        ,
        q.recent = function() {
            return this._recent
        }
        ,
        q._contains = function(a) {
            for (var b = a.timeline; b; ) {
                if (b === this)
                    return !0;
                b = b.timeline
            }
            return !1
        }
        ,
        q.shiftChildren = function(a, b, c) {
            c = c || 0;
            for (var d, e = this._first, f = this._labels; e; )
                e._startTime >= c && (e._startTime += a),
                e = e._next;
            if (b)
                for (d in f)
                    f[d] >= c && (f[d] += a);
            return this._uncache(!0)
        }
        ,
        q._kill = function(a, b) {
            if (!a && !b)
                return this._enabled(!1, !1);
            for (var c = b ? this.getTweensOf(b) : this.getChildren(!0, !0, !1), d = c.length, e = !1; --d > -1; )
                c[d]._kill(a, b) && (e = !0);
            return e
        }
        ,
        q.clear = function(a) {
            var b = this.getChildren(!1, !0, !0)
              , c = b.length;
            for (this._time = this._totalTime = 0; --c > -1; )
                b[c]._enabled(!1, !1);
            return a !== !1 && (this._labels = {}),
            this._uncache(!0)
        }
        ,
        q.invalidate = function() {
            for (var b = this._first; b; )
                b.invalidate(),
                b = b._next;
            return a.prototype.invalidate.call(this)
        }
        ,
        q._enabled = function(a, c) {
            if (a === this._gc)
                for (var d = this._first; d; )
                    d._enabled(a, !0),
                    d = d._next;
            return b.prototype._enabled.call(this, a, c)
        }
        ,
        q.totalTime = function(b, c, d) {
            this._forcingPlayhead = !0;
            var e = a.prototype.totalTime.apply(this, arguments);
            return this._forcingPlayhead = !1,
            e
        }
        ,
        q.duration = function(a) {
            return arguments.length ? (0 !== this.duration() && 0 !== a && this.timeScale(this._duration / a),
            this) : (this._dirty && this.totalDuration(),
            this._duration)
        }
        ,
        q.totalDuration = function(a) {
            if (!arguments.length) {
                if (this._dirty) {
                    for (var b, c, d = 0, e = this._last, f = 999999999999; e; )
                        b = e._prev,
                        e._dirty && e.totalDuration(),
                        e._startTime > f && this._sortChildren && !e._paused ? this.add(e, e._startTime - e._delay) : f = e._startTime,
                        e._startTime < 0 && !e._paused && (d -= e._startTime,
                        this._timeline.smoothChildTiming && (this._startTime += e._startTime / this._timeScale),
                        this.shiftChildren(-e._startTime, !1, -9999999999),
                        f = 0),
                        c = e._startTime + e._totalDuration / e._timeScale,
                        c > d && (d = c),
                        e = b;
                    this._duration = this._totalDuration = d,
                    this._dirty = !1
                }
                return this._totalDuration
            }
            return a && this.totalDuration() ? this.timeScale(this._totalDuration / a) : this
        }
        ,
        q.paused = function(b) {
            if (!b)
                for (var c = this._first, d = this._time; c; )
                    c._startTime === d && "isPause" === c.data && (c._rawPrevTime = 0),
                    c = c._next;
            return a.prototype.paused.apply(this, arguments)
        }
        ,
        q.usesFrames = function() {
            for (var b = this._timeline; b._timeline; )
                b = b._timeline;
            return b === a._rootFramesTimeline
        }
        ,
        q.rawTime = function() {
            return this._paused ? this._totalTime : (this._timeline.rawTime() - this._startTime) * this._timeScale
        }
        ,
        d
    }, !0)
}),
_gsScope._gsDefine && _gsScope._gsQueue.pop()(),
function(a) {
    "use strict";
    var b = function() {
        return (_gsScope.GreenSockGlobals || _gsScope)[a]
    };
    "function" == typeof define && define.amd ? define(["./TweenLite"], b) : "undefined" != typeof module && module.exports && (require("./TweenLite.js"),
    module.exports = b())
}("TimelineLite");
/*!
* VERSION: 1.15.4
* DATE: 2016-05-24
* UPDATES AND DOCS AT: http://greensock.com
*
* @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
* This work is subject to the terms at http://greensock.com/standard-license or for
* Club GreenSock members, the software agreement that was issued with your membership.
*
* @author: Jack Doyle, jack@greensock.com
**/
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {
    "use strict";
    _gsScope._gsDefine("easing.Back", ["easing.Ease"], function(a) {
        var b, c, d, e = _gsScope.GreenSockGlobals || _gsScope, f = e.com.greensock, g = 2 * Math.PI, h = Math.PI / 2, i = f._class, j = function(b, c) {
            var d = i("easing." + b, function() {}, !0)
              , e = d.prototype = new a;
            return e.constructor = d,
            e.getRatio = c,
            d
        }, k = a.register || function() {}
        , l = function(a, b, c, d, e) {
            var f = i("easing." + a, {
                easeOut: new b,
                easeIn: new c,
                easeInOut: new d
            }, !0);
            return k(f, a),
            f
        }, m = function(a, b, c) {
            this.t = a,
            this.v = b,
            c && (this.next = c,
            c.prev = this,
            this.c = c.v - b,
            this.gap = c.t - a)
        }, n = function(b, c) {
            var d = i("easing." + b, function(a) {
                this._p1 = a || 0 === a ? a : 1.70158,
                this._p2 = 1.525 * this._p1
            }, !0)
              , e = d.prototype = new a;
            return e.constructor = d,
            e.getRatio = c,
            e.config = function(a) {
                return new d(a)
            }
            ,
            d
        }, o = l("Back", n("BackOut", function(a) {
            return (a -= 1) * a * ((this._p1 + 1) * a + this._p1) + 1
        }), n("BackIn", function(a) {
            return a * a * ((this._p1 + 1) * a - this._p1)
        }), n("BackInOut", function(a) {
            return (a *= 2) < 1 ? .5 * a * a * ((this._p2 + 1) * a - this._p2) : .5 * ((a -= 2) * a * ((this._p2 + 1) * a + this._p2) + 2)
        })), p = i("easing.SlowMo", function(a, b, c) {
            b = b || 0 === b ? b : .7,
            null == a ? a = .7 : a > 1 && (a = 1),
            this._p = 1 !== a ? b : 0,
            this._p1 = (1 - a) / 2,
            this._p2 = a,
            this._p3 = this._p1 + this._p2,
            this._calcEnd = c === !0
        }, !0), q = p.prototype = new a;
        return q.constructor = p,
        q.getRatio = function(a) {
            var b = a + (.5 - a) * this._p;
            return a < this._p1 ? this._calcEnd ? 1 - (a = 1 - a / this._p1) * a : b - (a = 1 - a / this._p1) * a * a * a * b : a > this._p3 ? this._calcEnd ? 1 - (a = (a - this._p3) / this._p1) * a : b + (a - b) * (a = (a - this._p3) / this._p1) * a * a * a : this._calcEnd ? 1 : b
        }
        ,
        p.ease = new p(.7,.7),
        q.config = p.config = function(a, b, c) {
            return new p(a,b,c)
        }
        ,
        b = i("easing.SteppedEase", function(a) {
            a = a || 1,
            this._p1 = 1 / a,
            this._p2 = a + 1
        }, !0),
        q = b.prototype = new a,
        q.constructor = b,
        q.getRatio = function(a) {
            return 0 > a ? a = 0 : a >= 1 && (a = .999999999),
            (this._p2 * a >> 0) * this._p1
        }
        ,
        q.config = b.config = function(a) {
            return new b(a)
        }
        ,
        c = i("easing.RoughEase", function(b) {
            b = b || {};
            for (var c, d, e, f, g, h, i = b.taper || "none", j = [], k = 0, l = 0 | (b.points || 20), n = l, o = b.randomize !== !1, p = b.clamp === !0, q = b.template instanceof a ? b.template : null, r = "number" == typeof b.strength ? .4 * b.strength : .4; --n > -1; )
                c = o ? Math.random() : 1 / l * n,
                d = q ? q.getRatio(c) : c,
                "none" === i ? e = r : "out" === i ? (f = 1 - c,
                e = f * f * r) : "in" === i ? e = c * c * r : .5 > c ? (f = 2 * c,
                e = f * f * .5 * r) : (f = 2 * (1 - c),
                e = f * f * .5 * r),
                o ? d += Math.random() * e - .5 * e : n % 2 ? d += .5 * e : d -= .5 * e,
                p && (d > 1 ? d = 1 : 0 > d && (d = 0)),
                j[k++] = {
                    x: c,
                    y: d
                };
            for (j.sort(function(a, b) {
                return a.x - b.x
            }),
            h = new m(1,1,null),
            n = l; --n > -1; )
                g = j[n],
                h = new m(g.x,g.y,h);
            this._prev = new m(0,0,0 !== h.t ? h : h.next)
        }, !0),
        q = c.prototype = new a,
        q.constructor = c,
        q.getRatio = function(a) {
            var b = this._prev;
            if (a > b.t) {
                for (; b.next && a >= b.t; )
                    b = b.next;
                b = b.prev
            } else
                for (; b.prev && a <= b.t; )
                    b = b.prev;
            return this._prev = b,
            b.v + (a - b.t) / b.gap * b.c
        }
        ,
        q.config = function(a) {
            return new c(a)
        }
        ,
        c.ease = new c,
        l("Bounce", j("BounceOut", function(a) {
            return 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375
        }), j("BounceIn", function(a) {
            return (a = 1 - a) < 1 / 2.75 ? 1 - 7.5625 * a * a : 2 / 2.75 > a ? 1 - (7.5625 * (a -= 1.5 / 2.75) * a + .75) : 2.5 / 2.75 > a ? 1 - (7.5625 * (a -= 2.25 / 2.75) * a + .9375) : 1 - (7.5625 * (a -= 2.625 / 2.75) * a + .984375)
        }), j("BounceInOut", function(a) {
            var b = .5 > a;
            return a = b ? 1 - 2 * a : 2 * a - 1,
            a = 1 / 2.75 > a ? 7.5625 * a * a : 2 / 2.75 > a ? 7.5625 * (a -= 1.5 / 2.75) * a + .75 : 2.5 / 2.75 > a ? 7.5625 * (a -= 2.25 / 2.75) * a + .9375 : 7.5625 * (a -= 2.625 / 2.75) * a + .984375,
            b ? .5 * (1 - a) : .5 * a + .5
        })),
        l("Circ", j("CircOut", function(a) {
            return Math.sqrt(1 - (a -= 1) * a)
        }), j("CircIn", function(a) {
            return -(Math.sqrt(1 - a * a) - 1)
        }), j("CircInOut", function(a) {
            return (a *= 2) < 1 ? -.5 * (Math.sqrt(1 - a * a) - 1) : .5 * (Math.sqrt(1 - (a -= 2) * a) + 1)
        })),
        d = function(b, c, d) {
            var e = i("easing." + b, function(a, b) {
                this._p1 = a >= 1 ? a : 1,
                this._p2 = (b || d) / (1 > a ? a : 1),
                this._p3 = this._p2 / g * (Math.asin(1 / this._p1) || 0),
                this._p2 = g / this._p2
            }, !0)
              , f = e.prototype = new a;
            return f.constructor = e,
            f.getRatio = c,
            f.config = function(a, b) {
                return new e(a,b)
            }
            ,
            e
        }
        ,
        l("Elastic", d("ElasticOut", function(a) {
            return this._p1 * Math.pow(2, -10 * a) * Math.sin((a - this._p3) * this._p2) + 1
        }, .3), d("ElasticIn", function(a) {
            return -(this._p1 * Math.pow(2, 10 * (a -= 1)) * Math.sin((a - this._p3) * this._p2))
        }, .3), d("ElasticInOut", function(a) {
            return (a *= 2) < 1 ? -.5 * (this._p1 * Math.pow(2, 10 * (a -= 1)) * Math.sin((a - this._p3) * this._p2)) : this._p1 * Math.pow(2, -10 * (a -= 1)) * Math.sin((a - this._p3) * this._p2) * .5 + 1
        }, .45)),
        l("Expo", j("ExpoOut", function(a) {
            return 1 - Math.pow(2, -10 * a)
        }), j("ExpoIn", function(a) {
            return Math.pow(2, 10 * (a - 1)) - .001
        }), j("ExpoInOut", function(a) {
            return (a *= 2) < 1 ? .5 * Math.pow(2, 10 * (a - 1)) : .5 * (2 - Math.pow(2, -10 * (a - 1)))
        })),
        l("Sine", j("SineOut", function(a) {
            return Math.sin(a * h)
        }), j("SineIn", function(a) {
            return -Math.cos(a * h) + 1
        }), j("SineInOut", function(a) {
            return -.5 * (Math.cos(Math.PI * a) - 1)
        })),
        i("easing.EaseLookup", {
            find: function(b) {
                return a.map[b]
            }
        }, !0),
        k(e.SlowMo, "SlowMo", "ease,"),
        k(c, "RoughEase", "ease,"),
        k(b, "SteppedEase", "ease,"),
        o
    }, !0)
}),
_gsScope._gsDefine && _gsScope._gsQueue.pop()(),
function() {
    "use strict";
    var a = function() {
        return _gsScope.GreenSockGlobals || _gsScope
    };
    "function" == typeof define && define.amd ? define(["../TweenLite"], a) : "undefined" != typeof module && module.exports && (require("../TweenLite.js"),
    module.exports = a())
}();
/*!
* VERSION: 1.18.5
* DATE: 2016-05-24
* UPDATES AND DOCS AT: http://greensock.com
*
* @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
* This work is subject to the terms at http://greensock.com/standard-license or for
* Club GreenSock members, the software agreement that was issued with your membership.
*
* @author: Jack Doyle, jack@greensock.com
*/
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
(_gsScope._gsQueue || (_gsScope._gsQueue = [])).push(function() {
    "use strict";
    _gsScope._gsDefine("plugins.CSSPlugin", ["plugins.TweenPlugin", "TweenLite"], function(a, b) {
        var c, d, e, f, g = function() {
            a.call(this, "css"),
            this._overwriteProps.length = 0,
            this.setRatio = g.prototype.setRatio
        }, h = _gsScope._gsDefine.globals, i = {}, j = g.prototype = new a("css");
        j.constructor = g,
        g.version = "1.18.5",
        g.API = 2,
        g.defaultTransformPerspective = 0,
        g.defaultSkewType = "compensated",
        g.defaultSmoothOrigin = !0,
        j = "px",
        g.suffixMap = {
            top: j,
            right: j,
            bottom: j,
            left: j,
            width: j,
            height: j,
            fontSize: j,
            padding: j,
            margin: j,
            perspective: j,
            lineHeight: ""
        };
        var k, l, m, n, o, p, q = /(?:\-|\.|\b)(\d|\.|e\-)+/g, r = /(?:\d|\-\d|\.\d|\-\.\d|\+=\d|\-=\d|\+=.\d|\-=\.\d)+/g, s = /(?:\+=|\-=|\-|\b)[\d\-\.]+[a-zA-Z0-9]*(?:%|\b)/gi, t = /(?![+-]?\d*\.?\d+|[+-]|e[+-]\d+)[^0-9]/g, u = /(?:\d|\-|\+|=|#|\.)*/g, v = /opacity *= *([^)]*)/i, w = /opacity:([^;]*)/i, x = /alpha\(opacity *=.+?\)/i, y = /^(rgb|hsl)/, z = /([A-Z])/g, A = /-([a-z])/gi, B = /(^(?:url\(\"|url\())|(?:(\"\))$|\)$)/gi, C = function(a, b) {
            return b.toUpperCase()
        }, D = /(?:Left|Right|Width)/i, E = /(M11|M12|M21|M22)=[\d\-\.e]+/gi, F = /progid\:DXImageTransform\.Microsoft\.Matrix\(.+?\)/i, G = /,(?=[^\)]*(?:\(|$))/gi, H = /[\s,\(]/i, I = Math.PI / 180, J = 180 / Math.PI, K = {}, L = document, M = function(a) {
            return L.createElementNS ? L.createElementNS("http://www.w3.org/1999/xhtml", a) : L.createElement(a)
        }, N = M("div"), O = M("img"), P = g._internals = {
            _specialProps: i
        }, Q = navigator.userAgent, R = function() {
            var a = Q.indexOf("Android")
              , b = M("a");
            return m = -1 !== Q.indexOf("Safari") && -1 === Q.indexOf("Chrome") && (-1 === a || Number(Q.substr(a + 8, 1)) > 3),
            o = m && Number(Q.substr(Q.indexOf("Version/") + 8, 1)) < 6,
            n = -1 !== Q.indexOf("Firefox"),
            (/MSIE ([0-9]{1,}[\.0-9]{0,})/.exec(Q) || /Trident\/.*rv:([0-9]{1,}[\.0-9]{0,})/.exec(Q)) && (p = parseFloat(RegExp.$1)),
            b ? (b.style.cssText = "top:1px;opacity:.55;",
            /^0.55/.test(b.style.opacity)) : !1
        }(), S = function(a) {
            return v.test("string" == typeof a ? a : (a.currentStyle ? a.currentStyle.filter : a.style.filter) || "") ? parseFloat(RegExp.$1) / 100 : 1
        }, T = function(a) {
            window.console && console.log(a)
        }, U = "", V = "", W = function(a, b) {
            b = b || N;
            var c, d, e = b.style;
            if (void 0 !== e[a])
                return a;
            for (a = a.charAt(0).toUpperCase() + a.substr(1),
            c = ["O", "Moz", "ms", "Ms", "Webkit"],
            d = 5; --d > -1 && void 0 === e[c[d] + a]; )
                ;
            return d >= 0 ? (V = 3 === d ? "ms" : c[d],
            U = "-" + V.toLowerCase() + "-",
            V + a) : null
        }, X = L.defaultView ? L.defaultView.getComputedStyle : function() {}
        , Y = g.getStyle = function(a, b, c, d, e) {
            var f;
            return R || "opacity" !== b ? (!d && a.style[b] ? f = a.style[b] : (c = c || X(a)) ? f = c[b] || c.getPropertyValue(b) || c.getPropertyValue(b.replace(z, "-$1").toLowerCase()) : a.currentStyle && (f = a.currentStyle[b]),
            null == e || f && "none" !== f && "auto" !== f && "auto auto" !== f ? f : e) : S(a)
        }
        , Z = P.convertToPixels = function(a, c, d, e, f) {
            if ("px" === e || !e)
                return d;
            if ("auto" === e || !d)
                return 0;
            var h, i, j, k = D.test(c), l = a, m = N.style, n = 0 > d, o = 1 === d;
            if (n && (d = -d),
            o && (d *= 100),
            "%" === e && -1 !== c.indexOf("border"))
                h = d / 100 * (k ? a.clientWidth : a.clientHeight);
            else {
                if (m.cssText = "border:0 solid red;position:" + Y(a, "position") + ";line-height:0;",
                "%" !== e && l.appendChild && "v" !== e.charAt(0) && "rem" !== e)
                    m[k ? "borderLeftWidth" : "borderTopWidth"] = d + e;
                else {
                    if (l = a.parentNode || L.body,
                    i = l._gsCache,
                    j = b.ticker.frame,
                    i && k && i.time === j)
                        return i.width * d / 100;
                    m[k ? "width" : "height"] = d + e
                }
                l.appendChild(N),
                h = parseFloat(N[k ? "offsetWidth" : "offsetHeight"]),
                l.removeChild(N),
                k && "%" === e && g.cacheWidths !== !1 && (i = l._gsCache = l._gsCache || {},
                i.time = j,
                i.width = h / d * 100),
                0 !== h || f || (h = Z(a, c, d, e, !0))
            }
            return o && (h /= 100),
            n ? -h : h
        }
        , $ = P.calculateOffset = function(a, b, c) {
            if ("absolute" !== Y(a, "position", c))
                return 0;
            var d = "left" === b ? "Left" : "Top"
              , e = Y(a, "margin" + d, c);
            return a["offset" + d] - (Z(a, b, parseFloat(e), e.replace(u, "")) || 0)
        }
        , _ = function(a, b) {
            var c, d, e, f = {};
            if (b = b || X(a, null))
                if (c = b.length)
                    for (; --c > -1; )
                        e = b[c],
                        (-1 === e.indexOf("-transform") || Aa === e) && (f[e.replace(A, C)] = b.getPropertyValue(e));
                else
                    for (c in b)
                        (-1 === c.indexOf("Transform") || za === c) && (f[c] = b[c]);
            else if (b = a.currentStyle || a.style)
                for (c in b)
                    "string" == typeof c && void 0 === f[c] && (f[c.replace(A, C)] = b[c]);
            return R || (f.opacity = S(a)),
            d = Na(a, b, !1),
            f.rotation = d.rotation,
            f.skewX = d.skewX,
            f.scaleX = d.scaleX,
            f.scaleY = d.scaleY,
            f.x = d.x,
            f.y = d.y,
            Ca && (f.z = d.z,
            f.rotationX = d.rotationX,
            f.rotationY = d.rotationY,
            f.scaleZ = d.scaleZ),
            f.filters && delete f.filters,
            f
        }, aa = function(a, b, c, d, e) {
            var f, g, h, i = {}, j = a.style;
            for (g in c)
                "cssText" !== g && "length" !== g && isNaN(g) && (b[g] !== (f = c[g]) || e && e[g]) && -1 === g.indexOf("Origin") && ("number" == typeof f || "string" == typeof f) && (i[g] = "auto" !== f || "left" !== g && "top" !== g ? "" !== f && "auto" !== f && "none" !== f || "string" != typeof b[g] || "" === b[g].replace(t, "") ? f : 0 : $(a, g),
                void 0 !== j[g] && (h = new pa(j,g,j[g],h)));
            if (d)
                for (g in d)
                    "className" !== g && (i[g] = d[g]);
            return {
                difs: i,
                firstMPT: h
            }
        }, ba = {
            width: ["Left", "Right"],
            height: ["Top", "Bottom"]
        }, ca = ["marginLeft", "marginRight", "marginTop", "marginBottom"], da = function(a, b, c) {
            if ("svg" === (a.nodeName + "").toLowerCase())
                return (c || X(a))[b] || 0;
            if (a.getBBox && Ka(a))
                return a.getBBox()[b] || 0;
            var d = parseFloat("width" === b ? a.offsetWidth : a.offsetHeight)
              , e = ba[b]
              , f = e.length;
            for (c = c || X(a, null); --f > -1; )
                d -= parseFloat(Y(a, "padding" + e[f], c, !0)) || 0,
                d -= parseFloat(Y(a, "border" + e[f] + "Width", c, !0)) || 0;
            return d
        }, ea = function(a, b) {
            if ("contain" === a || "auto" === a || "auto auto" === a)
                return a + " ";
            (null == a || "" === a) && (a = "0 0");
            var c, d = a.split(" "), e = -1 !== a.indexOf("left") ? "0%" : -1 !== a.indexOf("right") ? "100%" : d[0], f = -1 !== a.indexOf("top") ? "0%" : -1 !== a.indexOf("bottom") ? "100%" : d[1];
            if (d.length > 3 && !b) {
                for (d = a.split(", ").join(",").split(","),
                a = [],
                c = 0; c < d.length; c++)
                    a.push(ea(d[c]));
                return a.join(",")
            }
            return null == f ? f = "center" === e ? "50%" : "0" : "center" === f && (f = "50%"),
            ("center" === e || isNaN(parseFloat(e)) && -1 === (e + "").indexOf("=")) && (e = "50%"),
            a = e + " " + f + (d.length > 2 ? " " + d[2] : ""),
            b && (b.oxp = -1 !== e.indexOf("%"),
            b.oyp = -1 !== f.indexOf("%"),
            b.oxr = "=" === e.charAt(1),
            b.oyr = "=" === f.charAt(1),
            b.ox = parseFloat(e.replace(t, "")),
            b.oy = parseFloat(f.replace(t, "")),
            b.v = a),
            b || a
        }, fa = function(a, b) {
            return "string" == typeof a && "=" === a.charAt(1) ? parseInt(a.charAt(0) + "1", 10) * parseFloat(a.substr(2)) : parseFloat(a) - parseFloat(b) || 0
        }, ga = function(a, b) {
            return null == a ? b : "string" == typeof a && "=" === a.charAt(1) ? parseInt(a.charAt(0) + "1", 10) * parseFloat(a.substr(2)) + b : parseFloat(a) || 0
        }, ha = function(a, b, c, d) {
            var e, f, g, h, i, j = 1e-6;
            return null == a ? h = b : "number" == typeof a ? h = a : (e = 360,
            f = a.split("_"),
            i = "=" === a.charAt(1),
            g = (i ? parseInt(a.charAt(0) + "1", 10) * parseFloat(f[0].substr(2)) : parseFloat(f[0])) * (-1 === a.indexOf("rad") ? 1 : J) - (i ? 0 : b),
            f.length && (d && (d[c] = b + g),
            -1 !== a.indexOf("short") && (g %= e,
            g !== g % (e / 2) && (g = 0 > g ? g + e : g - e)),
            -1 !== a.indexOf("_cw") && 0 > g ? g = (g + 9999999999 * e) % e - (g / e | 0) * e : -1 !== a.indexOf("ccw") && g > 0 && (g = (g - 9999999999 * e) % e - (g / e | 0) * e)),
            h = b + g),
            j > h && h > -j && (h = 0),
            h
        }, ia = {
            aqua: [0, 255, 255],
            lime: [0, 255, 0],
            silver: [192, 192, 192],
            black: [0, 0, 0],
            maroon: [128, 0, 0],
            teal: [0, 128, 128],
            blue: [0, 0, 255],
            navy: [0, 0, 128],
            white: [255, 255, 255],
            fuchsia: [255, 0, 255],
            olive: [128, 128, 0],
            yellow: [255, 255, 0],
            orange: [255, 165, 0],
            gray: [128, 128, 128],
            purple: [128, 0, 128],
            green: [0, 128, 0],
            red: [255, 0, 0],
            pink: [255, 192, 203],
            cyan: [0, 255, 255],
            transparent: [255, 255, 255, 0]
        }, ja = function(a, b, c) {
            return a = 0 > a ? a + 1 : a > 1 ? a - 1 : a,
            255 * (1 > 6 * a ? b + (c - b) * a * 6 : .5 > a ? c : 2 > 3 * a ? b + (c - b) * (2 / 3 - a) * 6 : b) + .5 | 0
        }, ka = g.parseColor = function(a, b) {
            var c, d, e, f, g, h, i, j, k, l, m;
            if (a)
                if ("number" == typeof a)
                    c = [a >> 16, a >> 8 & 255, 255 & a];
                else {
                    if ("," === a.charAt(a.length - 1) && (a = a.substr(0, a.length - 1)),
                    ia[a])
                        c = ia[a];
                    else if ("#" === a.charAt(0))
                        4 === a.length && (d = a.charAt(1),
                        e = a.charAt(2),
                        f = a.charAt(3),
                        a = "#" + d + d + e + e + f + f),
                        a = parseInt(a.substr(1), 16),
                        c = [a >> 16, a >> 8 & 255, 255 & a];
                    else if ("hsl" === a.substr(0, 3))
                        if (c = m = a.match(q),
                        b) {
                            if (-1 !== a.indexOf("="))
                                return a.match(r)
                        } else
                            g = Number(c[0]) % 360 / 360,
                            h = Number(c[1]) / 100,
                            i = Number(c[2]) / 100,
                            e = .5 >= i ? i * (h + 1) : i + h - i * h,
                            d = 2 * i - e,
                            c.length > 3 && (c[3] = Number(a[3])),
                            c[0] = ja(g + 1 / 3, d, e),
                            c[1] = ja(g, d, e),
                            c[2] = ja(g - 1 / 3, d, e);
                    else
                        c = a.match(q) || ia.transparent;
                    c[0] = Number(c[0]),
                    c[1] = Number(c[1]),
                    c[2] = Number(c[2]),
                    c.length > 3 && (c[3] = Number(c[3]))
                }
            else
                c = ia.black;
            return b && !m && (d = c[0] / 255,
            e = c[1] / 255,
            f = c[2] / 255,
            j = Math.max(d, e, f),
            k = Math.min(d, e, f),
            i = (j + k) / 2,
            j === k ? g = h = 0 : (l = j - k,
            h = i > .5 ? l / (2 - j - k) : l / (j + k),
            g = j === d ? (e - f) / l + (f > e ? 6 : 0) : j === e ? (f - d) / l + 2 : (d - e) / l + 4,
            g *= 60),
            c[0] = g + .5 | 0,
            c[1] = 100 * h + .5 | 0,
            c[2] = 100 * i + .5 | 0),
            c
        }
        , la = function(a, b) {
            var c, d, e, f = a.match(ma) || [], g = 0, h = f.length ? "" : a;
            for (c = 0; c < f.length; c++)
                d = f[c],
                e = a.substr(g, a.indexOf(d, g) - g),
                g += e.length + d.length,
                d = ka(d, b),
                3 === d.length && d.push(1),
                h += e + (b ? "hsla(" + d[0] + "," + d[1] + "%," + d[2] + "%," + d[3] : "rgba(" + d.join(",")) + ")";
            return h + a.substr(g)
        }, ma = "(?:\\b(?:(?:rgb|rgba|hsl|hsla)\\(.+?\\))|\\B#(?:[0-9a-f]{3}){1,2}\\b";
        for (j in ia)
            ma += "|" + j + "\\b";
        ma = new RegExp(ma + ")","gi"),
        g.colorStringFilter = function(a) {
            var b, c = a[0] + a[1];
            ma.test(c) && (b = -1 !== c.indexOf("hsl(") || -1 !== c.indexOf("hsla("),
            a[0] = la(a[0], b),
            a[1] = la(a[1], b)),
            ma.lastIndex = 0
        }
        ,
        b.defaultStringFilter || (b.defaultStringFilter = g.colorStringFilter);
        var na = function(a, b, c, d) {
            if (null == a)
                return function(a) {
                    return a
                }
                ;
            var e, f = b ? (a.match(ma) || [""])[0] : "", g = a.split(f).join("").match(s) || [], h = a.substr(0, a.indexOf(g[0])), i = ")" === a.charAt(a.length - 1) ? ")" : "", j = -1 !== a.indexOf(" ") ? " " : ",", k = g.length, l = k > 0 ? g[0].replace(q, "") : "";
            return k ? e = b ? function(a) {
                var b, m, n, o;
                if ("number" == typeof a)
                    a += l;
                else if (d && G.test(a)) {
                    for (o = a.replace(G, "|").split("|"),
                    n = 0; n < o.length; n++)
                        o[n] = e(o[n]);
                    return o.join(",")
                }
                if (b = (a.match(ma) || [f])[0],
                m = a.split(b).join("").match(s) || [],
                n = m.length,
                k > n--)
                    for (; ++n < k; )
                        m[n] = c ? m[(n - 1) / 2 | 0] : g[n];
                return h + m.join(j) + j + b + i + (-1 !== a.indexOf("inset") ? " inset" : "")
            }
            : function(a) {
                var b, f, m;
                if ("number" == typeof a)
                    a += l;
                else if (d && G.test(a)) {
                    for (f = a.replace(G, "|").split("|"),
                    m = 0; m < f.length; m++)
                        f[m] = e(f[m]);
                    return f.join(",")
                }
                if (b = a.match(s) || [],
                m = b.length,
                k > m--)
                    for (; ++m < k; )
                        b[m] = c ? b[(m - 1) / 2 | 0] : g[m];
                return h + b.join(j) + i
            }
            : function(a) {
                return a
            }
        }
          , oa = function(a) {
            return a = a.split(","),
            function(b, c, d, e, f, g, h) {
                var i, j = (c + "").split(" ");
                for (h = {},
                i = 0; 4 > i; i++)
                    h[a[i]] = j[i] = j[i] || j[(i - 1) / 2 >> 0];
                return e.parse(b, h, f, g)
            }
        }
          , pa = (P._setPluginRatio = function(a) {
            this.plugin.setRatio(a);
            for (var b, c, d, e, f, g = this.data, h = g.proxy, i = g.firstMPT, j = 1e-6; i; )
                b = h[i.v],
                i.r ? b = Math.round(b) : j > b && b > -j && (b = 0),
                i.t[i.p] = b,
                i = i._next;
            if (g.autoRotate && (g.autoRotate.rotation = h.rotation),
            1 === a || 0 === a)
                for (i = g.firstMPT,
                f = 1 === a ? "e" : "b"; i; ) {
                    if (c = i.t,
                    c.type) {
                        if (1 === c.type) {
                            for (e = c.xs0 + c.s + c.xs1,
                            d = 1; d < c.l; d++)
                                e += c["xn" + d] + c["xs" + (d + 1)];
                            c[f] = e
                        }
                    } else
                        c[f] = c.s + c.xs0;
                    i = i._next
                }
        }
        ,
        function(a, b, c, d, e) {
            this.t = a,
            this.p = b,
            this.v = c,
            this.r = e,
            d && (d._prev = this,
            this._next = d)
        }
        )
          , qa = (P._parseToProxy = function(a, b, c, d, e, f) {
            var g, h, i, j, k, l = d, m = {}, n = {}, o = c._transform, p = K;
            for (c._transform = null,
            K = b,
            d = k = c.parse(a, b, d, e),
            K = p,
            f && (c._transform = o,
            l && (l._prev = null,
            l._prev && (l._prev._next = null))); d && d !== l; ) {
                if (d.type <= 1 && (h = d.p,
                n[h] = d.s + d.c,
                m[h] = d.s,
                f || (j = new pa(d,"s",h,j,d.r),
                d.c = 0),
                1 === d.type))
                    for (g = d.l; --g > 0; )
                        i = "xn" + g,
                        h = d.p + "_" + i,
                        n[h] = d.data[i],
                        m[h] = d[i],
                        f || (j = new pa(d,i,h,j,d.rxp[i]));
                d = d._next
            }
            return {
                proxy: m,
                end: n,
                firstMPT: j,
                pt: k
            }
        }
        ,
        P.CSSPropTween = function(a, b, d, e, g, h, i, j, k, l, m) {
            this.t = a,
            this.p = b,
            this.s = d,
            this.c = e,
            this.n = i || b,
            a instanceof qa || f.push(this.n),
            this.r = j,
            this.type = h || 0,
            k && (this.pr = k,
            c = !0),
            this.b = void 0 === l ? d : l,
            this.e = void 0 === m ? d + e : m,
            g && (this._next = g,
            g._prev = this)
        }
        )
          , ra = function(a, b, c, d, e, f) {
            var g = new qa(a,b,c,d - c,e,-1,f);
            return g.b = c,
            g.e = g.xs0 = d,
            g
        }
          , sa = g.parseComplex = function(a, b, c, d, e, f, h, i, j, l) {
            c = c || f || "",
            h = new qa(a,b,0,0,h,l ? 2 : 1,null,!1,i,c,d),
            d += "",
            e && ma.test(d + c) && (d = [c, d],
            g.colorStringFilter(d),
            c = d[0],
            d = d[1]);
            var m, n, o, p, s, t, u, v, w, x, y, z, A, B = c.split(", ").join(",").split(" "), C = d.split(", ").join(",").split(" "), D = B.length, E = k !== !1;
            for ((-1 !== d.indexOf(",") || -1 !== c.indexOf(",")) && (B = B.join(" ").replace(G, ", ").split(" "),
            C = C.join(" ").replace(G, ", ").split(" "),
            D = B.length),
            D !== C.length && (B = (f || "").split(" "),
            D = B.length),
            h.plugin = j,
            h.setRatio = l,
            ma.lastIndex = 0,
            m = 0; D > m; m++)
                if (p = B[m],
                s = C[m],
                v = parseFloat(p),
                v || 0 === v)
                    h.appendXtra("", v, fa(s, v), s.replace(r, ""), E && -1 !== s.indexOf("px"), !0);
                else if (e && ma.test(p))
                    z = s.indexOf(")") + 1,
                    z = ")" + (z ? s.substr(z) : ""),
                    A = -1 !== s.indexOf("hsl") && R,
                    p = ka(p, A),
                    s = ka(s, A),
                    w = p.length + s.length > 6,
                    w && !R && 0 === s[3] ? (h["xs" + h.l] += h.l ? " transparent" : "transparent",
                    h.e = h.e.split(C[m]).join("transparent")) : (R || (w = !1),
                    A ? h.appendXtra(w ? "hsla(" : "hsl(", p[0], fa(s[0], p[0]), ",", !1, !0).appendXtra("", p[1], fa(s[1], p[1]), "%,", !1).appendXtra("", p[2], fa(s[2], p[2]), w ? "%," : "%" + z, !1) : h.appendXtra(w ? "rgba(" : "rgb(", p[0], s[0] - p[0], ",", !0, !0).appendXtra("", p[1], s[1] - p[1], ",", !0).appendXtra("", p[2], s[2] - p[2], w ? "," : z, !0),
                    w && (p = p.length < 4 ? 1 : p[3],
                    h.appendXtra("", p, (s.length < 4 ? 1 : s[3]) - p, z, !1))),
                    ma.lastIndex = 0;
                else if (t = p.match(q)) {
                    if (u = s.match(r),
                    !u || u.length !== t.length)
                        return h;
                    for (o = 0,
                    n = 0; n < t.length; n++)
                        y = t[n],
                        x = p.indexOf(y, o),
                        h.appendXtra(p.substr(o, x - o), Number(y), fa(u[n], y), "", E && "px" === p.substr(x + y.length, 2), 0 === n),
                        o = x + y.length;
                    h["xs" + h.l] += p.substr(o)
                } else
                    h["xs" + h.l] += h.l || h["xs" + h.l] ? " " + s : s;
            if (-1 !== d.indexOf("=") && h.data) {
                for (z = h.xs0 + h.data.s,
                m = 1; m < h.l; m++)
                    z += h["xs" + m] + h.data["xn" + m];
                h.e = z + h["xs" + m]
            }
            return h.l || (h.type = -1,
            h.xs0 = h.e),
            h.xfirst || h
        }
          , ta = 9;
        for (j = qa.prototype,
        j.l = j.pr = 0; --ta > 0; )
            j["xn" + ta] = 0,
            j["xs" + ta] = "";
        j.xs0 = "",
        j._next = j._prev = j.xfirst = j.data = j.plugin = j.setRatio = j.rxp = null,
        j.appendXtra = function(a, b, c, d, e, f) {
            var g = this
              , h = g.l;
            return g["xs" + h] += f && (h || g["xs" + h]) ? " " + a : a || "",
            c || 0 === h || g.plugin ? (g.l++,
            g.type = g.setRatio ? 2 : 1,
            g["xs" + g.l] = d || "",
            h > 0 ? (g.data["xn" + h] = b + c,
            g.rxp["xn" + h] = e,
            g["xn" + h] = b,
            g.plugin || (g.xfirst = new qa(g,"xn" + h,b,c,g.xfirst || g,0,g.n,e,g.pr),
            g.xfirst.xs0 = 0),
            g) : (g.data = {
                s: b + c
            },
            g.rxp = {},
            g.s = b,
            g.c = c,
            g.r = e,
            g)) : (g["xs" + h] += b + (d || ""),
            g)
        }
        ;
        var ua = function(a, b) {
            b = b || {},
            this.p = b.prefix ? W(a) || a : a,
            i[a] = i[this.p] = this,
            this.format = b.formatter || na(b.defaultValue, b.color, b.collapsible, b.multi),
            b.parser && (this.parse = b.parser),
            this.clrs = b.color,
            this.multi = b.multi,
            this.keyword = b.keyword,
            this.dflt = b.defaultValue,
            this.pr = b.priority || 0
        }
          , va = P._registerComplexSpecialProp = function(a, b, c) {
            "object" != typeof b && (b = {
                parser: c
            });
            var d, e, f = a.split(","), g = b.defaultValue;
            for (c = c || [g],
            d = 0; d < f.length; d++)
                b.prefix = 0 === d && b.prefix,
                b.defaultValue = c[d] || g,
                e = new ua(f[d],b)
        }
          , wa = function(a) {
            if (!i[a]) {
                var b = a.charAt(0).toUpperCase() + a.substr(1) + "Plugin";
                va(a, {
                    parser: function(a, c, d, e, f, g, j) {
                        var k = h.com.greensock.plugins[b];
                        return k ? (k._cssRegister(),
                        i[d].parse(a, c, d, e, f, g, j)) : (T("Error: " + b + " js file not loaded."),
                        f)
                    }
                })
            }
        };
        j = ua.prototype,
        j.parseComplex = function(a, b, c, d, e, f) {
            var g, h, i, j, k, l, m = this.keyword;
            if (this.multi && (G.test(c) || G.test(b) ? (h = b.replace(G, "|").split("|"),
            i = c.replace(G, "|").split("|")) : m && (h = [b],
            i = [c])),
            i) {
                for (j = i.length > h.length ? i.length : h.length,
                g = 0; j > g; g++)
                    b = h[g] = h[g] || this.dflt,
                    c = i[g] = i[g] || this.dflt,
                    m && (k = b.indexOf(m),
                    l = c.indexOf(m),
                    k !== l && (-1 === l ? h[g] = h[g].split(m).join("") : -1 === k && (h[g] += " " + m)));
                b = h.join(", "),
                c = i.join(", ")
            }
            return sa(a, this.p, b, c, this.clrs, this.dflt, d, this.pr, e, f)
        }
        ,
        j.parse = function(a, b, c, d, f, g, h) {
            return this.parseComplex(a.style, this.format(Y(a, this.p, e, !1, this.dflt)), this.format(b), f, g)
        }
        ,
        g.registerSpecialProp = function(a, b, c) {
            va(a, {
                parser: function(a, d, e, f, g, h, i) {
                    var j = new qa(a,e,0,0,g,2,e,!1,c);
                    return j.plugin = h,
                    j.setRatio = b(a, d, f._tween, e),
                    j
                },
                priority: c
            })
        }
        ,
        g.useSVGTransformAttr = m || n;
        var xa, ya = "scaleX,scaleY,scaleZ,x,y,z,skewX,skewY,rotation,rotationX,rotationY,perspective,xPercent,yPercent".split(","), za = W("transform"), Aa = U + "transform", Ba = W("transformOrigin"), Ca = null !== W("perspective"), Da = P.Transform = function() {
            this.perspective = parseFloat(g.defaultTransformPerspective) || 0,
            this.force3D = g.defaultForce3D !== !1 && Ca ? g.defaultForce3D || "auto" : !1
        }
        , Ea = window.SVGElement, Fa = function(a, b, c) {
            var d, e = L.createElementNS("http://www.w3.org/2000/svg", a), f = /([a-z])([A-Z])/g;
            for (d in c)
                e.setAttributeNS(null, d.replace(f, "$1-$2").toLowerCase(), c[d]);
            return b.appendChild(e),
            e
        }, Ga = L.documentElement, Ha = function() {
            var a, b, c, d = p || /Android/i.test(Q) && !window.chrome;
            return L.createElementNS && !d && (a = Fa("svg", Ga),
            b = Fa("rect", a, {
                width: 100,
                height: 50,
                x: 100
            }),
            c = b.getBoundingClientRect().width,
            b.style[Ba] = "50% 50%",
            b.style[za] = "scaleX(0.5)",
            d = c === b.getBoundingClientRect().width && !(n && Ca),
            Ga.removeChild(a)),
            d
        }(), Ia = function(a, b, c, d, e, f) {
            var h, i, j, k, l, m, n, o, p, q, r, s, t, u, v = a._gsTransform, w = Ma(a, !0);
            v && (t = v.xOrigin,
            u = v.yOrigin),
            (!d || (h = d.split(" ")).length < 2) && (n = a.getBBox(),
            b = ea(b).split(" "),
            h = [(-1 !== b[0].indexOf("%") ? parseFloat(b[0]) / 100 * n.width : parseFloat(b[0])) + n.x, (-1 !== b[1].indexOf("%") ? parseFloat(b[1]) / 100 * n.height : parseFloat(b[1])) + n.y]),
            c.xOrigin = k = parseFloat(h[0]),
            c.yOrigin = l = parseFloat(h[1]),
            d && w !== La && (m = w[0],
            n = w[1],
            o = w[2],
            p = w[3],
            q = w[4],
            r = w[5],
            s = m * p - n * o,
            i = k * (p / s) + l * (-o / s) + (o * r - p * q) / s,
            j = k * (-n / s) + l * (m / s) - (m * r - n * q) / s,
            k = c.xOrigin = h[0] = i,
            l = c.yOrigin = h[1] = j),
            v && (f && (c.xOffset = v.xOffset,
            c.yOffset = v.yOffset,
            v = c),
            e || e !== !1 && g.defaultSmoothOrigin !== !1 ? (i = k - t,
            j = l - u,
            v.xOffset += i * w[0] + j * w[2] - i,
            v.yOffset += i * w[1] + j * w[3] - j) : v.xOffset = v.yOffset = 0),
            f || a.setAttribute("data-svg-origin", h.join(" "))
        }, Ja = function(a) {
            try {
                return a.getBBox()
            } catch (a) {}
        }, Ka = function(a) {
            return !!(Ea && a.getBBox && a.getCTM && Ja(a) && (!a.parentNode || a.parentNode.getBBox && a.parentNode.getCTM))
        }, La = [1, 0, 0, 1, 0, 0], Ma = function(a, b) {
            var c, d, e, f, g, h, i = a._gsTransform || new Da, j = 1e5, k = a.style;
            if (za ? d = Y(a, Aa, null, !0) : a.currentStyle && (d = a.currentStyle.filter.match(E),
            d = d && 4 === d.length ? [d[0].substr(4), Number(d[2].substr(4)), Number(d[1].substr(4)), d[3].substr(4), i.x || 0, i.y || 0].join(",") : ""),
            c = !d || "none" === d || "matrix(1, 0, 0, 1, 0, 0)" === d,
            c && za && ((h = "none" === X(a).display) || !a.parentNode) && (h && (f = k.display,
            k.display = "block"),
            a.parentNode || (g = 1,
            Ga.appendChild(a)),
            d = Y(a, Aa, null, !0),
            c = !d || "none" === d || "matrix(1, 0, 0, 1, 0, 0)" === d,
            f ? k.display = f : h && Ra(k, "display"),
            g && Ga.removeChild(a)),
            (i.svg || a.getBBox && Ka(a)) && (c && -1 !== (k[za] + "").indexOf("matrix") && (d = k[za],
            c = 0),
            e = a.getAttribute("transform"),
            c && e && (-1 !== e.indexOf("matrix") ? (d = e,
            c = 0) : -1 !== e.indexOf("translate") && (d = "matrix(1,0,0,1," + e.match(/(?:\-|\b)[\d\-\.e]+\b/gi).join(",") + ")",
            c = 0))),
            c)
                return La;
            for (e = (d || "").match(q) || [],
            ta = e.length; --ta > -1; )
                f = Number(e[ta]),
                e[ta] = (g = f - (f |= 0)) ? (g * j + (0 > g ? -.5 : .5) | 0) / j + f : f;
            return b && e.length > 6 ? [e[0], e[1], e[4], e[5], e[12], e[13]] : e
        }, Na = P.getTransform = function(a, c, d, e) {
            if (a._gsTransform && d && !e)
                return a._gsTransform;
            var f, h, i, j, k, l, m = d ? a._gsTransform || new Da : new Da, n = m.scaleX < 0, o = 2e-5, p = 1e5, q = Ca ? parseFloat(Y(a, Ba, c, !1, "0 0 0").split(" ")[2]) || m.zOrigin || 0 : 0, r = parseFloat(g.defaultTransformPerspective) || 0;
            if (m.svg = !(!a.getBBox || !Ka(a)),
            m.svg && (Ia(a, Y(a, Ba, c, !1, "50% 50%") + "", m, a.getAttribute("data-svg-origin")),
            xa = g.useSVGTransformAttr || Ha),
            f = Ma(a),
            f !== La) {
                if (16 === f.length) {
                    var s, t, u, v, w, x = f[0], y = f[1], z = f[2], A = f[3], B = f[4], C = f[5], D = f[6], E = f[7], F = f[8], G = f[9], H = f[10], I = f[12], K = f[13], L = f[14], M = f[11], N = Math.atan2(D, H);
                    m.zOrigin && (L = -m.zOrigin,
                    I = F * L - f[12],
                    K = G * L - f[13],
                    L = H * L + m.zOrigin - f[14]),
                    m.rotationX = N * J,
                    N && (v = Math.cos(-N),
                    w = Math.sin(-N),
                    s = B * v + F * w,
                    t = C * v + G * w,
                    u = D * v + H * w,
                    F = B * -w + F * v,
                    G = C * -w + G * v,
                    H = D * -w + H * v,
                    M = E * -w + M * v,
                    B = s,
                    C = t,
                    D = u),
                    N = Math.atan2(-z, H),
                    m.rotationY = N * J,
                    N && (v = Math.cos(-N),
                    w = Math.sin(-N),
                    s = x * v - F * w,
                    t = y * v - G * w,
                    u = z * v - H * w,
                    G = y * w + G * v,
                    H = z * w + H * v,
                    M = A * w + M * v,
                    x = s,
                    y = t,
                    z = u),
                    N = Math.atan2(y, x),
                    m.rotation = N * J,
                    N && (v = Math.cos(-N),
                    w = Math.sin(-N),
                    x = x * v + B * w,
                    t = y * v + C * w,
                    C = y * -w + C * v,
                    D = z * -w + D * v,
                    y = t),
                    m.rotationX && Math.abs(m.rotationX) + Math.abs(m.rotation) > 359.9 && (m.rotationX = m.rotation = 0,
                    m.rotationY = 180 - m.rotationY),
                    m.scaleX = (Math.sqrt(x * x + y * y) * p + .5 | 0) / p,
                    m.scaleY = (Math.sqrt(C * C + G * G) * p + .5 | 0) / p,
                    m.scaleZ = (Math.sqrt(D * D + H * H) * p + .5 | 0) / p,
                    m.rotationX || m.rotationY ? m.skewX = 0 : (m.skewX = B || C ? Math.atan2(B, C) * J + m.rotation : m.skewX || 0,
                    Math.abs(m.skewX) > 90 && Math.abs(m.skewX) < 270 && (n ? (m.scaleX *= -1,
                    m.skewX += m.rotation <= 0 ? 180 : -180,
                    m.rotation += m.rotation <= 0 ? 180 : -180) : (m.scaleY *= -1,
                    m.skewX += m.skewX <= 0 ? 180 : -180))),
                    m.perspective = M ? 1 / (0 > M ? -M : M) : 0,
                    m.x = I,
                    m.y = K,
                    m.z = L,
                    m.svg && (m.x -= m.xOrigin - (m.xOrigin * x - m.yOrigin * B),
                    m.y -= m.yOrigin - (m.yOrigin * y - m.xOrigin * C))
                } else if (!Ca || e || !f.length || m.x !== f[4] || m.y !== f[5] || !m.rotationX && !m.rotationY) {
                    var O = f.length >= 6
                      , P = O ? f[0] : 1
                      , Q = f[1] || 0
                      , R = f[2] || 0
                      , S = O ? f[3] : 1;
                    m.x = f[4] || 0,
                    m.y = f[5] || 0,
                    i = Math.sqrt(P * P + Q * Q),
                    j = Math.sqrt(S * S + R * R),
                    k = P || Q ? Math.atan2(Q, P) * J : m.rotation || 0,
                    l = R || S ? Math.atan2(R, S) * J + k : m.skewX || 0,
                    Math.abs(l) > 90 && Math.abs(l) < 270 && (n ? (i *= -1,
                    l += 0 >= k ? 180 : -180,
                    k += 0 >= k ? 180 : -180) : (j *= -1,
                    l += 0 >= l ? 180 : -180)),
                    m.scaleX = i,
                    m.scaleY = j,
                    m.rotation = k,
                    m.skewX = l,
                    Ca && (m.rotationX = m.rotationY = m.z = 0,
                    m.perspective = r,
                    m.scaleZ = 1),
                    m.svg && (m.x -= m.xOrigin - (m.xOrigin * P + m.yOrigin * R),
                    m.y -= m.yOrigin - (m.xOrigin * Q + m.yOrigin * S))
                }
                m.zOrigin = q;
                for (h in m)
                    m[h] < o && m[h] > -o && (m[h] = 0)
            }
            return d && (a._gsTransform = m,
            m.svg && (xa && a.style[za] ? b.delayedCall(.001, function() {
                Ra(a.style, za)
            }) : !xa && a.getAttribute("transform") && b.delayedCall(.001, function() {
                a.removeAttribute("transform")
            }))),
            m
        }
        , Oa = function(a) {
            var b, c, d = this.data, e = -d.rotation * I, f = e + d.skewX * I, g = 1e5, h = (Math.cos(e) * d.scaleX * g | 0) / g, i = (Math.sin(e) * d.scaleX * g | 0) / g, j = (Math.sin(f) * -d.scaleY * g | 0) / g, k = (Math.cos(f) * d.scaleY * g | 0) / g, l = this.t.style, m = this.t.currentStyle;
            if (m) {
                c = i,
                i = -j,
                j = -c,
                b = m.filter,
                l.filter = "";
                var n, o, q = this.t.offsetWidth, r = this.t.offsetHeight, s = "absolute" !== m.position, t = "progid:DXImageTransform.Microsoft.Matrix(M11=" + h + ", M12=" + i + ", M21=" + j + ", M22=" + k, w = d.x + q * d.xPercent / 100, x = d.y + r * d.yPercent / 100;
                if (null != d.ox && (n = (d.oxp ? q * d.ox * .01 : d.ox) - q / 2,
                o = (d.oyp ? r * d.oy * .01 : d.oy) - r / 2,
                w += n - (n * h + o * i),
                x += o - (n * j + o * k)),
                s ? (n = q / 2,
                o = r / 2,
                t += ", Dx=" + (n - (n * h + o * i) + w) + ", Dy=" + (o - (n * j + o * k) + x) + ")") : t += ", sizingMethod='auto expand')",
                -1 !== b.indexOf("DXImageTransform.Microsoft.Matrix(") ? l.filter = b.replace(F, t) : l.filter = t + " " + b,
                (0 === a || 1 === a) && 1 === h && 0 === i && 0 === j && 1 === k && (s && -1 === t.indexOf("Dx=0, Dy=0") || v.test(b) && 100 !== parseFloat(RegExp.$1) || -1 === b.indexOf(b.indexOf("Alpha")) && l.removeAttribute("filter")),
                !s) {
                    var y, z, A, B = 8 > p ? 1 : -1;
                    for (n = d.ieOffsetX || 0,
                    o = d.ieOffsetY || 0,
                    d.ieOffsetX = Math.round((q - ((0 > h ? -h : h) * q + (0 > i ? -i : i) * r)) / 2 + w),
                    d.ieOffsetY = Math.round((r - ((0 > k ? -k : k) * r + (0 > j ? -j : j) * q)) / 2 + x),
                    ta = 0; 4 > ta; ta++)
                        z = ca[ta],
                        y = m[z],
                        c = -1 !== y.indexOf("px") ? parseFloat(y) : Z(this.t, z, parseFloat(y), y.replace(u, "")) || 0,
                        A = c !== d[z] ? 2 > ta ? -d.ieOffsetX : -d.ieOffsetY : 2 > ta ? n - d.ieOffsetX : o - d.ieOffsetY,
                        l[z] = (d[z] = Math.round(c - A * (0 === ta || 2 === ta ? 1 : B))) + "px"
                }
            }
        }, Pa = P.set3DTransformRatio = P.setTransformRatio = function(a) {
            var b, c, d, e, f, g, h, i, j, k, l, m, o, p, q, r, s, t, u, v, w, x, y, z = this.data, A = this.t.style, B = z.rotation, C = z.rotationX, D = z.rotationY, E = z.scaleX, F = z.scaleY, G = z.scaleZ, H = z.x, J = z.y, K = z.z, L = z.svg, M = z.perspective, N = z.force3D;
            if (((1 === a || 0 === a) && "auto" === N && (this.tween._totalTime === this.tween._totalDuration || !this.tween._totalTime) || !N) && !K && !M && !D && !C && 1 === G || xa && L || !Ca)
                return void (B || z.skewX || L ? (B *= I,
                x = z.skewX * I,
                y = 1e5,
                b = Math.cos(B) * E,
                e = Math.sin(B) * E,
                c = Math.sin(B - x) * -F,
                f = Math.cos(B - x) * F,
                x && "simple" === z.skewType && (s = Math.tan(x),
                s = Math.sqrt(1 + s * s),
                c *= s,
                f *= s,
                z.skewY && (b *= s,
                e *= s)),
                L && (H += z.xOrigin - (z.xOrigin * b + z.yOrigin * c) + z.xOffset,
                J += z.yOrigin - (z.xOrigin * e + z.yOrigin * f) + z.yOffset,
                xa && (z.xPercent || z.yPercent) && (p = this.t.getBBox(),
                H += .01 * z.xPercent * p.width,
                J += .01 * z.yPercent * p.height),
                p = 1e-6,
                p > H && H > -p && (H = 0),
                p > J && J > -p && (J = 0)),
                u = (b * y | 0) / y + "," + (e * y | 0) / y + "," + (c * y | 0) / y + "," + (f * y | 0) / y + "," + H + "," + J + ")",
                L && xa ? this.t.setAttribute("transform", "matrix(" + u) : A[za] = (z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) matrix(" : "matrix(") + u) : A[za] = (z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) matrix(" : "matrix(") + E + ",0,0," + F + "," + H + "," + J + ")");
            if (n && (p = 1e-4,
            p > E && E > -p && (E = G = 2e-5),
            p > F && F > -p && (F = G = 2e-5),
            !M || z.z || z.rotationX || z.rotationY || (M = 0)),
            B || z.skewX)
                B *= I,
                q = b = Math.cos(B),
                r = e = Math.sin(B),
                z.skewX && (B -= z.skewX * I,
                q = Math.cos(B),
                r = Math.sin(B),
                "simple" === z.skewType && (s = Math.tan(z.skewX * I),
                s = Math.sqrt(1 + s * s),
                q *= s,
                r *= s,
                z.skewY && (b *= s,
                e *= s))),
                c = -r,
                f = q;
            else {
                if (!(D || C || 1 !== G || M || L))
                    return void (A[za] = (z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) translate3d(" : "translate3d(") + H + "px," + J + "px," + K + "px)" + (1 !== E || 1 !== F ? " scale(" + E + "," + F + ")" : ""));
                b = f = 1,
                c = e = 0
            }
            j = 1,
            d = g = h = i = k = l = 0,
            m = M ? -1 / M : 0,
            o = z.zOrigin,
            p = 1e-6,
            v = ",",
            w = "0",
            B = D * I,
            B && (q = Math.cos(B),
            r = Math.sin(B),
            h = -r,
            k = m * -r,
            d = b * r,
            g = e * r,
            j = q,
            m *= q,
            b *= q,
            e *= q),
            B = C * I,
            B && (q = Math.cos(B),
            r = Math.sin(B),
            s = c * q + d * r,
            t = f * q + g * r,
            i = j * r,
            l = m * r,
            d = c * -r + d * q,
            g = f * -r + g * q,
            j *= q,
            m *= q,
            c = s,
            f = t),
            1 !== G && (d *= G,
            g *= G,
            j *= G,
            m *= G),
            1 !== F && (c *= F,
            f *= F,
            i *= F,
            l *= F),
            1 !== E && (b *= E,
            e *= E,
            h *= E,
            k *= E),
            (o || L) && (o && (H += d * -o,
            J += g * -o,
            K += j * -o + o),
            L && (H += z.xOrigin - (z.xOrigin * b + z.yOrigin * c) + z.xOffset,
            J += z.yOrigin - (z.xOrigin * e + z.yOrigin * f) + z.yOffset),
            p > H && H > -p && (H = w),
            p > J && J > -p && (J = w),
            p > K && K > -p && (K = 0)),
            u = z.xPercent || z.yPercent ? "translate(" + z.xPercent + "%," + z.yPercent + "%) matrix3d(" : "matrix3d(",
            u += (p > b && b > -p ? w : b) + v + (p > e && e > -p ? w : e) + v + (p > h && h > -p ? w : h),
            u += v + (p > k && k > -p ? w : k) + v + (p > c && c > -p ? w : c) + v + (p > f && f > -p ? w : f),
            C || D || 1 !== G ? (u += v + (p > i && i > -p ? w : i) + v + (p > l && l > -p ? w : l) + v + (p > d && d > -p ? w : d),
            u += v + (p > g && g > -p ? w : g) + v + (p > j && j > -p ? w : j) + v + (p > m && m > -p ? w : m) + v) : u += ",0,0,0,0,1,0,",
            u += H + v + J + v + K + v + (M ? 1 + -K / M : 1) + ")",
            A[za] = u
        }
        ;
        j = Da.prototype,
        j.x = j.y = j.z = j.skewX = j.skewY = j.rotation = j.rotationX = j.rotationY = j.zOrigin = j.xPercent = j.yPercent = j.xOffset = j.yOffset = 0,
        j.scaleX = j.scaleY = j.scaleZ = 1,
        va("transform,scale,scaleX,scaleY,scaleZ,x,y,z,rotation,rotationX,rotationY,rotationZ,skewX,skewY,shortRotation,shortRotationX,shortRotationY,shortRotationZ,transformOrigin,svgOrigin,transformPerspective,directionalRotation,parseTransform,force3D,skewType,xPercent,yPercent,smoothOrigin", {
            parser: function(a, b, c, d, f, h, i) {
                if (d._lastParsedTransform === i)
                    return f;
                d._lastParsedTransform = i;
                var j, k, l, m, n, o, p, q, r, s = a._gsTransform, t = a.style, u = 1e-6, v = ya.length, w = i, x = {}, y = "transformOrigin", z = Na(a, e, !0, i.parseTransform);
                if (d._transform = z,
                "string" == typeof w.transform && za)
                    k = N.style,
                    k[za] = w.transform,
                    k.display = "block",
                    k.position = "absolute",
                    L.body.appendChild(N),
                    j = Na(N, null, !1),
                    z.svg && (p = z.xOrigin,
                    q = z.yOrigin,
                    j.x -= z.xOffset,
                    j.y -= z.yOffset,
                    (w.transformOrigin || w.svgOrigin) && (l = {},
                    Ia(a, ea(w.transformOrigin), l, w.svgOrigin, w.smoothOrigin, !0),
                    p = l.xOrigin,
                    q = l.yOrigin,
                    j.x -= l.xOffset - z.xOffset,
                    j.y -= l.yOffset - z.yOffset),
                    (p || q) && (r = Ma(N, !0),
                    j.x -= p - (p * r[0] + q * r[2]),
                    j.y -= q - (p * r[1] + q * r[3]))),
                    L.body.removeChild(N),
                    j.perspective || (j.perspective = z.perspective),
                    null != w.xPercent && (j.xPercent = ga(w.xPercent, z.xPercent)),
                    null != w.yPercent && (j.yPercent = ga(w.yPercent, z.yPercent));
                else if ("object" == typeof w) {
                    if (j = {
                        scaleX: ga(null != w.scaleX ? w.scaleX : w.scale, z.scaleX),
                        scaleY: ga(null != w.scaleY ? w.scaleY : w.scale, z.scaleY),
                        scaleZ: ga(w.scaleZ, z.scaleZ),
                        x: ga(w.x, z.x),
                        y: ga(w.y, z.y),
                        z: ga(w.z, z.z),
                        xPercent: ga(w.xPercent, z.xPercent),
                        yPercent: ga(w.yPercent, z.yPercent),
                        perspective: ga(w.transformPerspective, z.perspective)
                    },
                    o = w.directionalRotation,
                    null != o)
                        if ("object" == typeof o)
                            for (k in o)
                                w[k] = o[k];
                        else
                            w.rotation = o;
                    "string" == typeof w.x && -1 !== w.x.indexOf("%") && (j.x = 0,
                    j.xPercent = ga(w.x, z.xPercent)),
                    "string" == typeof w.y && -1 !== w.y.indexOf("%") && (j.y = 0,
                    j.yPercent = ga(w.y, z.yPercent)),
                    j.rotation = ha("rotation"in w ? w.rotation : "shortRotation"in w ? w.shortRotation + "_short" : "rotationZ"in w ? w.rotationZ : z.rotation - z.skewY, z.rotation - z.skewY, "rotation", x),
                    Ca && (j.rotationX = ha("rotationX"in w ? w.rotationX : "shortRotationX"in w ? w.shortRotationX + "_short" : z.rotationX || 0, z.rotationX, "rotationX", x),
                    j.rotationY = ha("rotationY"in w ? w.rotationY : "shortRotationY"in w ? w.shortRotationY + "_short" : z.rotationY || 0, z.rotationY, "rotationY", x)),
                    j.skewX = ha(w.skewX, z.skewX - z.skewY),
                    (j.skewY = ha(w.skewY, z.skewY)) && (j.skewX += j.skewY,
                    j.rotation += j.skewY)
                }
                for (Ca && null != w.force3D && (z.force3D = w.force3D,
                n = !0),
                z.skewType = w.skewType || z.skewType || g.defaultSkewType,
                m = z.force3D || z.z || z.rotationX || z.rotationY || j.z || j.rotationX || j.rotationY || j.perspective,
                m || null == w.scale || (j.scaleZ = 1); --v > -1; )
                    c = ya[v],
                    l = j[c] - z[c],
                    (l > u || -u > l || null != w[c] || null != K[c]) && (n = !0,
                    f = new qa(z,c,z[c],l,f),
                    c in x && (f.e = x[c]),
                    f.xs0 = 0,
                    f.plugin = h,
                    d._overwriteProps.push(f.n));
                return l = w.transformOrigin,
                z.svg && (l || w.svgOrigin) && (p = z.xOffset,
                q = z.yOffset,
                Ia(a, ea(l), j, w.svgOrigin, w.smoothOrigin),
                f = ra(z, "xOrigin", (s ? z : j).xOrigin, j.xOrigin, f, y),
                f = ra(z, "yOrigin", (s ? z : j).yOrigin, j.yOrigin, f, y),
                (p !== z.xOffset || q !== z.yOffset) && (f = ra(z, "xOffset", s ? p : z.xOffset, z.xOffset, f, y),
                f = ra(z, "yOffset", s ? q : z.yOffset, z.yOffset, f, y)),
                l = xa ? null : "0px 0px"),
                (l || Ca && m && z.zOrigin) && (za ? (n = !0,
                c = Ba,
                l = (l || Y(a, c, e, !1, "50% 50%")) + "",
                f = new qa(t,c,0,0,f,-1,y),
                f.b = t[c],
                f.plugin = h,
                Ca ? (k = z.zOrigin,
                l = l.split(" "),
                z.zOrigin = (l.length > 2 && (0 === k || "0px" !== l[2]) ? parseFloat(l[2]) : k) || 0,
                f.xs0 = f.e = l[0] + " " + (l[1] || "50%") + " 0px",
                f = new qa(z,"zOrigin",0,0,f,-1,f.n),
                f.b = k,
                f.xs0 = f.e = z.zOrigin) : f.xs0 = f.e = l) : ea(l + "", z)),
                n && (d._transformType = z.svg && xa || !m && 3 !== this._transformType ? 2 : 3),
                f
            },
            prefix: !0
        }),
        va("boxShadow", {
            defaultValue: "0px 0px 0px 0px #999",
            prefix: !0,
            color: !0,
            multi: !0,
            keyword: "inset"
        }),
        va("borderRadius", {
            defaultValue: "0px",
            parser: function(a, b, c, f, g, h) {
                b = this.format(b);
                var i, j, k, l, m, n, o, p, q, r, s, t, u, v, w, x, y = ["borderTopLeftRadius", "borderTopRightRadius", "borderBottomRightRadius", "borderBottomLeftRadius"], z = a.style;
                for (q = parseFloat(a.offsetWidth),
                r = parseFloat(a.offsetHeight),
                i = b.split(" "),
                j = 0; j < y.length; j++)
                    this.p.indexOf("border") && (y[j] = W(y[j])),
                    m = l = Y(a, y[j], e, !1, "0px"),
                    -1 !== m.indexOf(" ") && (l = m.split(" "),
                    m = l[0],
                    l = l[1]),
                    n = k = i[j],
                    o = parseFloat(m),
                    t = m.substr((o + "").length),
                    u = "=" === n.charAt(1),
                    u ? (p = parseInt(n.charAt(0) + "1", 10),
                    n = n.substr(2),
                    p *= parseFloat(n),
                    s = n.substr((p + "").length - (0 > p ? 1 : 0)) || "") : (p = parseFloat(n),
                    s = n.substr((p + "").length)),
                    "" === s && (s = d[c] || t),
                    s !== t && (v = Z(a, "borderLeft", o, t),
                    w = Z(a, "borderTop", o, t),
                    "%" === s ? (m = v / q * 100 + "%",
                    l = w / r * 100 + "%") : "em" === s ? (x = Z(a, "borderLeft", 1, "em"),
                    m = v / x + "em",
                    l = w / x + "em") : (m = v + "px",
                    l = w + "px"),
                    u && (n = parseFloat(m) + p + s,
                    k = parseFloat(l) + p + s)),
                    g = sa(z, y[j], m + " " + l, n + " " + k, !1, "0px", g);
                return g
            },
            prefix: !0,
            formatter: na("0px 0px 0px 0px", !1, !0)
        }),
        va("borderBottomLeftRadius,borderBottomRightRadius,borderTopLeftRadius,borderTopRightRadius", {
            defaultValue: "0px",
            parser: function(a, b, c, d, f, g) {
                return sa(a.style, c, this.format(Y(a, c, e, !1, "0px 0px")), this.format(b), !1, "0px", f)
            },
            prefix: !0,
            formatter: na("0px 0px", !1, !0)
        }),
        va("backgroundPosition", {
            defaultValue: "0 0",
            parser: function(a, b, c, d, f, g) {
                var h, i, j, k, l, m, n = "background-position", o = e || X(a, null), q = this.format((o ? p ? o.getPropertyValue(n + "-x") + " " + o.getPropertyValue(n + "-y") : o.getPropertyValue(n) : a.currentStyle.backgroundPositionX + " " + a.currentStyle.backgroundPositionY) || "0 0"), r = this.format(b);
                if (-1 !== q.indexOf("%") != (-1 !== r.indexOf("%")) && r.split(",").length < 2 && (m = Y(a, "backgroundImage").replace(B, ""),
                m && "none" !== m)) {
                    for (h = q.split(" "),
                    i = r.split(" "),
                    O.setAttribute("src", m),
                    j = 2; --j > -1; )
                        q = h[j],
                        k = -1 !== q.indexOf("%"),
                        k !== (-1 !== i[j].indexOf("%")) && (l = 0 === j ? a.offsetWidth - O.width : a.offsetHeight - O.height,
                        h[j] = k ? parseFloat(q) / 100 * l + "px" : parseFloat(q) / l * 100 + "%");
                    q = h.join(" ")
                }
                return this.parseComplex(a.style, q, r, f, g)
            },
            formatter: ea
        }),
        va("backgroundSize", {
            defaultValue: "0 0",
            formatter: ea
        }),
        va("perspective", {
            defaultValue: "0px",
            prefix: !0
        }),
        va("perspectiveOrigin", {
            defaultValue: "50% 50%",
            prefix: !0
        }),
        va("transformStyle", {
            prefix: !0
        }),
        va("backfaceVisibility", {
            prefix: !0
        }),
        va("userSelect", {
            prefix: !0
        }),
        va("margin", {
            parser: oa("marginTop,marginRight,marginBottom,marginLeft")
        }),
        va("padding", {
            parser: oa("paddingTop,paddingRight,paddingBottom,paddingLeft")
        }),
        va("clip", {
            defaultValue: "rect(0px,0px,0px,0px)",
            parser: function(a, b, c, d, f, g) {
                var h, i, j;
                return 9 > p ? (i = a.currentStyle,
                j = 8 > p ? " " : ",",
                h = "rect(" + i.clipTop + j + i.clipRight + j + i.clipBottom + j + i.clipLeft + ")",
                b = this.format(b).split(",").join(j)) : (h = this.format(Y(a, this.p, e, !1, this.dflt)),
                b = this.format(b)),
                this.parseComplex(a.style, h, b, f, g)
            }
        }),
        va("textShadow", {
            defaultValue: "0px 0px 0px #999",
            color: !0,
            multi: !0
        }),
        va("autoRound,strictUnits", {
            parser: function(a, b, c, d, e) {
                return e
            }
        }),
        va("border", {
            defaultValue: "0px solid #000",
            parser: function(a, b, c, d, f, g) {
                var h = Y(a, "borderTopWidth", e, !1, "0px")
                  , i = this.format(b).split(" ")
                  , j = i[0].replace(u, "");
                return "px" !== j && (h = parseFloat(h) / Z(a, "borderTopWidth", 1, j) + j),
                this.parseComplex(a.style, this.format(h + " " + Y(a, "borderTopStyle", e, !1, "solid") + " " + Y(a, "borderTopColor", e, !1, "#000")), i.join(" "), f, g)
            },
            color: !0,
            formatter: function(a) {
                var b = a.split(" ");
                return b[0] + " " + (b[1] || "solid") + " " + (a.match(ma) || ["#000"])[0]
            }
        }),
        va("borderWidth", {
            parser: oa("borderTopWidth,borderRightWidth,borderBottomWidth,borderLeftWidth")
        }),
        va("float,cssFloat,styleFloat", {
            parser: function(a, b, c, d, e, f) {
                var g = a.style
                  , h = "cssFloat"in g ? "cssFloat" : "styleFloat";
                return new qa(g,h,0,0,e,-1,c,!1,0,g[h],b)
            }
        });
        var Qa = function(a) {
            var b, c = this.t, d = c.filter || Y(this.data, "filter") || "", e = this.s + this.c * a | 0;
            100 === e && (-1 === d.indexOf("atrix(") && -1 === d.indexOf("radient(") && -1 === d.indexOf("oader(") ? (c.removeAttribute("filter"),
            b = !Y(this.data, "filter")) : (c.filter = d.replace(x, ""),
            b = !0)),
            b || (this.xn1 && (c.filter = d = d || "alpha(opacity=" + e + ")"),
            -1 === d.indexOf("pacity") ? 0 === e && this.xn1 || (c.filter = d + " alpha(opacity=" + e + ")") : c.filter = d.replace(v, "opacity=" + e))
        };
        va("opacity,alpha,autoAlpha", {
            defaultValue: "1",
            parser: function(a, b, c, d, f, g) {
                var h = parseFloat(Y(a, "opacity", e, !1, "1"))
                  , i = a.style
                  , j = "autoAlpha" === c;
                return "string" == typeof b && "=" === b.charAt(1) && (b = ("-" === b.charAt(0) ? -1 : 1) * parseFloat(b.substr(2)) + h),
                j && 1 === h && "hidden" === Y(a, "visibility", e) && 0 !== b && (h = 0),
                R ? f = new qa(i,"opacity",h,b - h,f) : (f = new qa(i,"opacity",100 * h,100 * (b - h),f),
                f.xn1 = j ? 1 : 0,
                i.zoom = 1,
                f.type = 2,
                f.b = "alpha(opacity=" + f.s + ")",
                f.e = "alpha(opacity=" + (f.s + f.c) + ")",
                f.data = a,
                f.plugin = g,
                f.setRatio = Qa),
                j && (f = new qa(i,"visibility",0,0,f,-1,null,!1,0,0 !== h ? "inherit" : "hidden",0 === b ? "hidden" : "inherit"),
                f.xs0 = "inherit",
                d._overwriteProps.push(f.n),
                d._overwriteProps.push(c)),
                f
            }
        });
        var Ra = function(a, b) {
            b && (a.removeProperty ? (("ms" === b.substr(0, 2) || "webkit" === b.substr(0, 6)) && (b = "-" + b),
            a.removeProperty(b.replace(z, "-$1").toLowerCase())) : a.removeAttribute(b))
        }
          , Sa = function(a) {
            if (this.t._gsClassPT = this,
            1 === a || 0 === a) {
                this.t.setAttribute("class", 0 === a ? this.b : this.e);
                for (var b = this.data, c = this.t.style; b; )
                    b.v ? c[b.p] = b.v : Ra(c, b.p),
                    b = b._next;
                1 === a && this.t._gsClassPT === this && (this.t._gsClassPT = null)
            } else
                this.t.getAttribute("class") !== this.e && this.t.setAttribute("class", this.e)
        };
        va("className", {
            parser: function(a, b, d, f, g, h, i) {
                var j, k, l, m, n, o = a.getAttribute("class") || "", p = a.style.cssText;
                if (g = f._classNamePT = new qa(a,d,0,0,g,2),
                g.setRatio = Sa,
                g.pr = -11,
                c = !0,
                g.b = o,
                k = _(a, e),
                l = a._gsClassPT) {
                    for (m = {},
                    n = l.data; n; )
                        m[n.p] = 1,
                        n = n._next;
                    l.setRatio(1)
                }
                return a._gsClassPT = g,
                g.e = "=" !== b.charAt(1) ? b : o.replace(new RegExp("(?:\\s|^)" + b.substr(2) + "(?![\\w-])"), "") + ("+" === b.charAt(0) ? " " + b.substr(2) : ""),
                a.setAttribute("class", g.e),
                j = aa(a, k, _(a), i, m),
                a.setAttribute("class", o),
                g.data = j.firstMPT,
                a.style.cssText = p,
                g = g.xfirst = f.parse(a, j.difs, g, h)
            }
        });
        var Ta = function(a) {
            if ((1 === a || 0 === a) && this.data._totalTime === this.data._totalDuration && "isFromStart" !== this.data.data) {
                var b, c, d, e, f, g = this.t.style, h = i.transform.parse;
                if ("all" === this.e)
                    g.cssText = "",
                    e = !0;
                else
                    for (b = this.e.split(" ").join("").split(","),
                    d = b.length; --d > -1; )
                        c = b[d],
                        i[c] && (i[c].parse === h ? e = !0 : c = "transformOrigin" === c ? Ba : i[c].p),
                        Ra(g, c);
                e && (Ra(g, za),
                f = this.t._gsTransform,
                f && (f.svg && (this.t.removeAttribute("data-svg-origin"),
                this.t.removeAttribute("transform")),
                delete this.t._gsTransform))
            }
        };
        for (va("clearProps", {
            parser: function(a, b, d, e, f) {
                return f = new qa(a,d,0,0,f,2),
                f.setRatio = Ta,
                f.e = b,
                f.pr = -10,
                f.data = e._tween,
                c = !0,
                f
            }
        }),
        j = "bezier,throwProps,physicsProps,physics2D".split(","),
        ta = j.length; ta--; )
            wa(j[ta]);
        j = g.prototype,
        j._firstPT = j._lastParsedTransform = j._transform = null,
        j._onInitTween = function(a, b, h) {
            if (!a.nodeType)
                return !1;
            this._target = a,
            this._tween = h,
            this._vars = b,
            k = b.autoRound,
            c = !1,
            d = b.suffixMap || g.suffixMap,
            e = X(a, ""),
            f = this._overwriteProps;
            var j, n, p, q, r, s, t, u, v, x = a.style;
            if (l && "" === x.zIndex && (j = Y(a, "zIndex", e),
            ("auto" === j || "" === j) && this._addLazySet(x, "zIndex", 0)),
            "string" == typeof b && (q = x.cssText,
            j = _(a, e),
            x.cssText = q + ";" + b,
            j = aa(a, j, _(a)).difs,
            !R && w.test(b) && (j.opacity = parseFloat(RegExp.$1)),
            b = j,
            x.cssText = q),
            b.className ? this._firstPT = n = i.className.parse(a, b.className, "className", this, null, null, b) : this._firstPT = n = this.parse(a, b, null),
            this._transformType) {
                for (v = 3 === this._transformType,
                za ? m && (l = !0,
                "" === x.zIndex && (t = Y(a, "zIndex", e),
                ("auto" === t || "" === t) && this._addLazySet(x, "zIndex", 0)),
                o && this._addLazySet(x, "WebkitBackfaceVisibility", this._vars.WebkitBackfaceVisibility || (v ? "visible" : "hidden"))) : x.zoom = 1,
                p = n; p && p._next; )
                    p = p._next;
                u = new qa(a,"transform",0,0,null,2),
                this._linkCSSP(u, null, p),
                u.setRatio = za ? Pa : Oa,
                u.data = this._transform || Na(a, e, !0),
                u.tween = h,
                u.pr = -1,
                f.pop()
            }
            if (c) {
                for (; n; ) {
                    for (s = n._next,
                    p = q; p && p.pr > n.pr; )
                        p = p._next;
                    (n._prev = p ? p._prev : r) ? n._prev._next = n : q = n,
                    (n._next = p) ? p._prev = n : r = n,
                    n = s
                }
                this._firstPT = q
            }
            return !0
        }
        ,
        j.parse = function(a, b, c, f) {
            var g, h, j, l, m, n, o, p, q, r, s = a.style;
            for (g in b)
                n = b[g],
                h = i[g],
                h ? c = h.parse(a, n, g, this, c, f, b) : (m = Y(a, g, e) + "",
                q = "string" == typeof n,
                "color" === g || "fill" === g || "stroke" === g || -1 !== g.indexOf("Color") || q && y.test(n) ? (q || (n = ka(n),
                n = (n.length > 3 ? "rgba(" : "rgb(") + n.join(",") + ")"),
                c = sa(s, g, m, n, !0, "transparent", c, 0, f)) : q && H.test(n) ? c = sa(s, g, m, n, !0, null, c, 0, f) : (j = parseFloat(m),
                o = j || 0 === j ? m.substr((j + "").length) : "",
                ("" === m || "auto" === m) && ("width" === g || "height" === g ? (j = da(a, g, e),
                o = "px") : "left" === g || "top" === g ? (j = $(a, g, e),
                o = "px") : (j = "opacity" !== g ? 0 : 1,
                o = "")),
                r = q && "=" === n.charAt(1),
                r ? (l = parseInt(n.charAt(0) + "1", 10),
                n = n.substr(2),
                l *= parseFloat(n),
                p = n.replace(u, "")) : (l = parseFloat(n),
                p = q ? n.replace(u, "") : ""),
                "" === p && (p = g in d ? d[g] : o),
                n = l || 0 === l ? (r ? l + j : l) + p : b[g],
                o !== p && "" !== p && (l || 0 === l) && j && (j = Z(a, g, j, o),
                "%" === p ? (j /= Z(a, g, 100, "%") / 100,
                b.strictUnits !== !0 && (m = j + "%")) : "em" === p || "rem" === p || "vw" === p || "vh" === p ? j /= Z(a, g, 1, p) : "px" !== p && (l = Z(a, g, l, p),
                p = "px"),
                r && (l || 0 === l) && (n = l + j + p)),
                r && (l += j),
                !j && 0 !== j || !l && 0 !== l ? void 0 !== s[g] && (n || n + "" != "NaN" && null != n) ? (c = new qa(s,g,l || j || 0,0,c,-1,g,!1,0,m,n),
                c.xs0 = "none" !== n || "display" !== g && -1 === g.indexOf("Style") ? n : m) : T("invalid " + g + " tween value: " + b[g]) : (c = new qa(s,g,j,l - j,c,0,g,k !== !1 && ("px" === p || "zIndex" === g),0,m,n),
                c.xs0 = p))),
                f && c && !c.plugin && (c.plugin = f);
            return c
        }
        ,
        j.setRatio = function(a) {
            var b, c, d, e = this._firstPT, f = 1e-6;
            if (1 !== a || this._tween._time !== this._tween._duration && 0 !== this._tween._time)
                if (a || this._tween._time !== this._tween._duration && 0 !== this._tween._time || this._tween._rawPrevTime === -1e-6)
                    for (; e; ) {
                        if (b = e.c * a + e.s,
                        e.r ? b = Math.round(b) : f > b && b > -f && (b = 0),
                        e.type)
                            if (1 === e.type)
                                if (d = e.l,
                                2 === d)
                                    e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2;
                                else if (3 === d)
                                    e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3;
                                else if (4 === d)
                                    e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3 + e.xn3 + e.xs4;
                                else if (5 === d)
                                    e.t[e.p] = e.xs0 + b + e.xs1 + e.xn1 + e.xs2 + e.xn2 + e.xs3 + e.xn3 + e.xs4 + e.xn4 + e.xs5;
                                else {
                                    for (c = e.xs0 + b + e.xs1,
                                    d = 1; d < e.l; d++)
                                        c += e["xn" + d] + e["xs" + (d + 1)];
                                    e.t[e.p] = c
                                }
                            else
                                -1 === e.type ? e.t[e.p] = e.xs0 : e.setRatio && e.setRatio(a);
                        else
                            e.t[e.p] = b + e.xs0;
                        e = e._next
                    }
                else
                    for (; e; )
                        2 !== e.type ? e.t[e.p] = e.b : e.setRatio(a),
                        e = e._next;
            else
                for (; e; ) {
                    if (2 !== e.type)
                        if (e.r && -1 !== e.type)
                            if (b = Math.round(e.s + e.c),
                            e.type) {
                                if (1 === e.type) {
                                    for (d = e.l,
                                    c = e.xs0 + b + e.xs1,
                                    d = 1; d < e.l; d++)
                                        c += e["xn" + d] + e["xs" + (d + 1)];
                                    e.t[e.p] = c
                                }
                            } else
                                e.t[e.p] = b + e.xs0;
                        else
                            e.t[e.p] = e.e;
                    else
                        e.setRatio(a);
                    e = e._next
                }
        }
        ,
        j._enableTransforms = function(a) {
            this._transform = this._transform || Na(this._target, e, !0),
            this._transformType = this._transform.svg && xa || !a && 3 !== this._transformType ? 2 : 3
        }
        ;
        var Ua = function(a) {
            this.t[this.p] = this.e,
            this.data._linkCSSP(this, this._next, null, !0)
        };
        j._addLazySet = function(a, b, c) {
            var d = this._firstPT = new qa(a,b,0,0,this._firstPT,2);
            d.e = c,
            d.setRatio = Ua,
            d.data = this
        }
        ,
        j._linkCSSP = function(a, b, c, d) {
            return a && (b && (b._prev = a),
            a._next && (a._next._prev = a._prev),
            a._prev ? a._prev._next = a._next : this._firstPT === a && (this._firstPT = a._next,
            d = !0),
            c ? c._next = a : d || null !== this._firstPT || (this._firstPT = a),
            a._next = b,
            a._prev = c),
            a
        }
        ,
        j._kill = function(b) {
            var c, d, e, f = b;
            if (b.autoAlpha || b.alpha) {
                f = {};
                for (d in b)
                    f[d] = b[d];
                f.opacity = 1,
                f.autoAlpha && (f.visibility = 1)
            }
            return b.className && (c = this._classNamePT) && (e = c.xfirst,
            e && e._prev ? this._linkCSSP(e._prev, c._next, e._prev._prev) : e === this._firstPT && (this._firstPT = c._next),
            c._next && this._linkCSSP(c._next, c._next._next, e._prev),
            this._classNamePT = null),
            a.prototype._kill.call(this, f)
        }
        ;
        var Va = function(a, b, c) {
            var d, e, f, g;
            if (a.slice)
                for (e = a.length; --e > -1; )
                    Va(a[e], b, c);
            else
                for (d = a.childNodes,
                e = d.length; --e > -1; )
                    f = d[e],
                    g = f.type,
                    f.style && (b.push(_(f)),
                    c && c.push(f)),
                    1 !== g && 9 !== g && 11 !== g || !f.childNodes.length || Va(f, b, c)
        };
        return g.cascadeTo = function(a, c, d) {
            var e, f, g, h, i = b.to(a, c, d), j = [i], k = [], l = [], m = [], n = b._internals.reservedProps;
            for (a = i._targets || i.target,
            Va(a, k, m),
            i.render(c, !0, !0),
            Va(a, l),
            i.render(0, !0, !0),
            i._enabled(!0),
            e = m.length; --e > -1; )
                if (f = aa(m[e], k[e], l[e]),
                f.firstMPT) {
                    f = f.difs;
                    for (g in d)
                        n[g] && (f[g] = d[g]);
                    h = {};
                    for (g in f)
                        h[g] = k[e][g];
                    j.push(b.fromTo(m[e], c, h, f))
                }
            return j
        }
        ,
        a.activate([g]),
        g
    }, !0)
}),
_gsScope._gsDefine && _gsScope._gsQueue.pop()(),
function(a) {
    "use strict";
    var b = function() {
        return (_gsScope.GreenSockGlobals || _gsScope)[a]
    };
    "function" == typeof define && define.amd ? define(["../TweenLite"], b) : "undefined" != typeof module && module.exports && (require("../TweenLite.js"),
    module.exports = b())
}("CSSPlugin");
/*!
* VERSION: 0.3.5
* DATE: 2016-05-24
* UPDATES AND DOCS AT: http://greensock.com
*
* @license Copyright (c) 2008-2016, GreenSock. All rights reserved.
* SplitText is a Club GreenSock membership benefit; You must have a valid membership to use
* this code without violating the terms of use. Visit http://www.greensock.com/club/ to sign up or get more details.
* This work is subject to the software agreement that was issued with your membership.
*
* @author: Jack Doyle, jack@greensock.com
*/
var _gsScope = "undefined" != typeof module && module.exports && "undefined" != typeof global ? global : this || window;
!function(a) {
    "use strict";
    var b = a.GreenSockGlobals || a
      , c = function(a) {
        var c, d = a.split("."), e = b;
        for (c = 0; c < d.length; c++)
            e[d[c]] = e = e[d[c]] || {};
        return e
    }
      , d = c("com.greensock.utils")
      , e = function(a) {
        var b = a.nodeType
          , c = "";
        if (1 === b || 9 === b || 11 === b) {
            if ("string" == typeof a.textContent)
                return a.textContent;
            for (a = a.firstChild; a; a = a.nextSibling)
                c += e(a)
        } else if (3 === b || 4 === b)
            return a.nodeValue;
        return c
    }
      , f = document
      , g = f.defaultView ? f.defaultView.getComputedStyle : function() {}
      , h = /([A-Z])/g
      , i = function(a, b, c, d) {
        var e;
        return (c = c || g(a, null)) ? (a = c.getPropertyValue(b.replace(h, "-$1").toLowerCase()),
        e = a || c.length ? a : c[b]) : a.currentStyle && (c = a.currentStyle,
        e = c[b]),
        d ? e : parseInt(e, 10) || 0
    }
      , j = function(a) {
        return a.length && a[0] && (a[0].nodeType && a[0].style && !a.nodeType || a[0].length && a[0][0]) ? !0 : !1
    }
      , k = function(a) {
        var b, c, d, e = [], f = a.length;
        for (b = 0; f > b; b++)
            if (c = a[b],
            j(c))
                for (d = c.length,
                d = 0; d < c.length; d++)
                    e.push(c[d]);
            else
                e.push(c);
        return e
    }
      , l = /(?:\r|\n|\s\s|\t\t)/g
      , m = ")eefec303079ad17405c"
      , n = /(?:<br>|<br\/>|<br \/>)/gi
      , o = f.all && !f.addEventListener
      , p = "<div style='position:relative;display:inline-block;" + (o ? "*display:inline;*zoom:1;'" : "'")
      , q = function(a) {
        a = a || "";
        var b = -1 !== a.indexOf("++")
          , c = 1;
        return b && (a = a.split("++").join("")),
        function() {
            return p + (a ? " class='" + a + (b ? c++ : "") + "'>" : ">")
        }
    }
      , r = d.SplitText = b.SplitText = function(a, b) {
        if ("string" == typeof a && (a = r.selector(a)),
        !a)
            throw "cannot split a null element.";
        this.elements = j(a) ? k(a) : [a],
        this.chars = [],
        this.words = [],
        this.lines = [],
        this._originals = [],
        this.vars = b || {},
        this.split(b)
    }
      , s = function(a, b, c) {
        var d = a.nodeType;
        if (1 === d || 9 === d || 11 === d)
            for (a = a.firstChild; a; a = a.nextSibling)
                s(a, b, c);
        else
            (3 === d || 4 === d) && (a.nodeValue = a.nodeValue.split(b).join(c))
    }
      , t = function(a, b) {
        for (var c = b.length; --c > -1; )
            a.push(b[c])
    }
      , u = function(a, b, c, d, h) {
        n.test(a.innerHTML) && (a.innerHTML = a.innerHTML.replace(n, m));
        var j, k, o, p, r, u, v, w, x, y, z, A, B, C, D = e(a), E = b.type || b.split || "chars,words,lines", F = -1 !== E.indexOf("lines") ? [] : null, G = -1 !== E.indexOf("words"), H = -1 !== E.indexOf("chars"), I = "absolute" === b.position || b.absolute === !0, J = I ? "&#173; " : " ", K = -999, L = g(a), M = i(a, "paddingLeft", L), N = i(a, "borderBottomWidth", L) + i(a, "borderTopWidth", L), O = i(a, "borderLeftWidth", L) + i(a, "borderRightWidth", L), P = i(a, "paddingTop", L) + i(a, "paddingBottom", L), Q = i(a, "paddingLeft", L) + i(a, "paddingRight", L), R = i(a, "textAlign", L, !0), S = a.clientHeight, T = a.clientWidth, U = "</div>", V = q(b.wordsClass), W = q(b.charsClass), X = -1 !== (b.linesClass || "").indexOf("++"), Y = b.linesClass, Z = -1 !== D.indexOf("<"), $ = !0, _ = [], aa = [], ba = [];
        for (!b.reduceWhiteSpace != !1 && (D = D.replace(l, "")),
        X && (Y = Y.split("++").join("")),
        Z && (D = D.split("<").join("{{LT}}")),
        j = D.length,
        p = V(),
        r = 0; j > r; r++)
            if (v = D.charAt(r),
            ")" === v && D.substr(r, 20) === m)
                p += ($ ? U : "") + "<BR/>",
                $ = !1,
                r !== j - 20 && D.substr(r + 20, 20) !== m && (p += " " + V(),
                $ = !0),
                r += 19;
            else if (" " === v && " " !== D.charAt(r - 1) && r !== j - 1 && D.substr(r - 20, 20) !== m) {
                for (p += $ ? U : "",
                $ = !1; " " === D.charAt(r + 1); )
                    p += J,
                    r++;
                (")" !== D.charAt(r + 1) || D.substr(r + 1, 20) !== m) && (p += J + V(),
                $ = !0)
            } else
                "{" === v && "{{LT}}" === D.substr(r, 6) ? (p += H ? W() + "{{LT}}</div>" : "{{LT}}",
                r += 5) : p += H && " " !== v ? W() + v + "</div>" : v;
        for (a.innerHTML = p + ($ ? U : ""),
        Z && s(a, "{{LT}}", "<"),
        u = a.getElementsByTagName("*"),
        j = u.length,
        w = [],
        r = 0; j > r; r++)
            w[r] = u[r];
        if (F || I)
            for (r = 0; j > r; r++)
                x = w[r],
                o = x.parentNode === a,
                (o || I || H && !G) && (y = x.offsetTop,
                F && o && y !== K && "BR" !== x.nodeName && (k = [],
                F.push(k),
                K = y),
                I && (x._x = x.offsetLeft,
                x._y = y,
                x._w = x.offsetWidth,
                x._h = x.offsetHeight),
                F && (G !== o && H || (k.push(x),
                x._x -= M),
                o && r && (w[r - 1]._wordEnd = !0),
                "BR" === x.nodeName && x.nextSibling && "BR" === x.nextSibling.nodeName && F.push([])));
        for (r = 0; j > r; r++)
            x = w[r],
            o = x.parentNode === a,
            "BR" !== x.nodeName ? (I && (A = x.style,
            G || o || (x._x += x.parentNode._x,
            x._y += x.parentNode._y),
            A.left = x._x + "px",
            A.top = x._y + "px",
            A.position = "absolute",
            A.display = "block",
            A.width = x._w + 1 + "px",
            A.height = x._h + "px"),
            G ? o && "" !== x.innerHTML ? aa.push(x) : H && _.push(x) : o ? (a.removeChild(x),
            w.splice(r--, 1),
            j--) : !o && H && (y = !F && !I && x.nextSibling,
            a.appendChild(x),
            y || a.appendChild(f.createTextNode(" ")),
            _.push(x))) : F || I ? (a.removeChild(x),
            w.splice(r--, 1),
            j--) : G || a.appendChild(x);
        if (F) {
            for (I && (z = f.createElement("div"),
            a.appendChild(z),
            B = z.offsetWidth + "px",
            y = z.offsetParent === a ? 0 : a.offsetLeft,
            a.removeChild(z)),
            A = a.style.cssText,
            a.style.cssText = "display:none;"; a.firstChild; )
                a.removeChild(a.firstChild);
            for (C = !I || !G && !H,
            r = 0; r < F.length; r++) {
                for (k = F[r],
                z = f.createElement("div"),
                z.style.cssText = "display:block;text-align:" + R + ";position:" + (I ? "absolute;" : "relative;"),
                Y && (z.className = Y + (X ? r + 1 : "")),
                ba.push(z),
                j = k.length,
                u = 0; j > u; u++)
                    "BR" !== k[u].nodeName && (x = k[u],
                    z.appendChild(x),
                    C && (x._wordEnd || G) && z.appendChild(f.createTextNode(" ")),
                    I && (0 === u && (z.style.top = x._y + "px",
                    z.style.left = M + y + "px"),
                    x.style.top = "0px",
                    y && (x.style.left = x._x - y + "px")));
                0 === j && (z.innerHTML = "&nbsp;"),
                G || H || (z.innerHTML = e(z).split(String.fromCharCode(160)).join(" ")),
                I && (z.style.width = B,
                z.style.height = x._h + "px"),
                a.appendChild(z)
            }
            a.style.cssText = A
        }
        I && (S > a.clientHeight && (a.style.height = S - P + "px",
        a.clientHeight < S && (a.style.height = S + N + "px")),
        T > a.clientWidth && (a.style.width = T - Q + "px",
        a.clientWidth < T && (a.style.width = T + O + "px"))),
        t(c, _),
        t(d, aa),
        t(h, ba)
    }
      , v = r.prototype;
    v.split = function(a) {
        this.isSplit && this.revert(),
        this.vars = a || this.vars,
        this._originals.length = this.chars.length = this.words.length = this.lines.length = 0;
        for (var b = this.elements.length; --b > -1; )
            this._originals[b] = this.elements[b].innerHTML,
            u(this.elements[b], this.vars, this.chars, this.words, this.lines);
        return this.chars.reverse(),
        this.words.reverse(),
        this.lines.reverse(),
        this.isSplit = !0,
        this
    }
    ,
    v.revert = function() {
        if (!this._originals)
            throw "revert() call wasn't scoped properly.";
        for (var a = this._originals.length; --a > -1; )
            this.elements[a].innerHTML = this._originals[a];
        return this.chars = [],
        this.words = [],
        this.lines = [],
        this.isSplit = !1,
        this
    }
    ,
    r.selector = a.$ || a.jQuery || function(b) {
        var c = a.$ || a.jQuery;
        return c ? (r.selector = c,
        c(b)) : "undefined" == typeof document ? b : document.querySelectorAll ? document.querySelectorAll(b) : document.getElementById("#" === b.charAt(0) ? b.substr(1) : b)
    }
    ,
    r.version = "0.3.5"
}(_gsScope),
function(a) {
    "use strict";
    var b = function() {
        return (_gsScope.GreenSockGlobals || _gsScope)[a]
    };
    "function" == typeof define && define.amd ? define([], b) : "undefined" != typeof module && module.exports && (module.exports = b())
}("SplitText");
try {
    window.GreenSockGlobals = null;
    window._gsQueue = null;
    window._gsDefine = null;
    delete (window.GreenSockGlobals);
    delete (window._gsQueue);
    delete (window._gsDefine);
} catch (e) {}
try {
    window.GreenSockGlobals = oldgs;
    window._gsQueue = oldgs_queue;
} catch (e) {}
if (window.tplogs == true)
    try {
        console.groupEnd();
    } catch (e) {}
(function(e, t) {
    e.waitForImages = {
        hasImageProperties: ["backgroundImage", "listStyleImage", "borderImage", "borderCornerImage"]
    };
    e.expr[":"].uncached = function(t) {
        var n = document.createElement("img");
        n.src = t.src;
        return e(t).is('img[src!=""]') && !n.complete
    }
    ;
    e.fn.waitForImages = function(t, n, r) {
        if (e.isPlainObject(arguments[0])) {
            n = t.each;
            r = t.waitForAll;
            t = t.finished
        }
        t = t || e.noop;
        n = n || e.noop;
        r = !!r;
        if (!e.isFunction(t) || !e.isFunction(n)) {
            throw new TypeError("An invalid callback was supplied.")
        }
        return this.each(function() {
            var i = e(this)
              , s = [];
            if (r) {
                var o = e.waitForImages.hasImageProperties || []
                  , u = /url\((['"]?)(.*?)\1\)/g;
                i.find("*").each(function() {
                    var t = e(this);
                    if (t.is("img:uncached")) {
                        s.push({
                            src: t.attr("src"),
                            element: t[0]
                        })
                    }
                    e.each(o, function(e, n) {
                        var r = t.css(n);
                        if (!r) {
                            return true
                        }
                        var i;
                        while (i = u.exec(r)) {
                            s.push({
                                src: i[2],
                                element: t[0]
                            })
                        }
                    })
                })
            } else {
                i.find("img:uncached").each(function() {
                    s.push({
                        src: this.src,
                        element: this
                    })
                })
            }
            var f = s.length
              , l = 0;
            if (f == 0) {
                t.call(i[0])
            }
            e.each(s, function(r, s) {
                var o = new Image;
                e(o).bind("load error", function(e) {
                    l++;
                    n.call(s.element, l, f, e.type == "load");
                    if (l == f) {
                        t.call(i[0]);
                        return false
                    }
                });
                o.src = s.src
            })
        })
    }
    ;
}
)(jQuery);
!function(jQuery, undefined) {
    "use strict";
    jQuery.fn.extend({
        revolution: function(e) {
            var t = {
                delay: 9e3,
                responsiveLevels: 4064,
                visibilityLevels: [2048, 1024, 778, 480],
                gridwidth: 960,
                gridheight: 500,
                minHeight: 0,
                autoHeight: "off",
                sliderType: "standard",
                sliderLayout: "auto",
                fullScreenAutoWidth: "off",
                fullScreenAlignForce: "off",
                fullScreenOffsetContainer: "",
                fullScreenOffset: "0",
                hideCaptionAtLimit: 0,
                hideAllCaptionAtLimit: 0,
                hideSliderAtLimit: 0,
                disableProgressBar: "off",
                stopAtSlide: -1,
                stopAfterLoops: -1,
                shadow: 0,
                dottedOverlay: "none",
                startDelay: 0,
                lazyType: "smart",
                spinner: "spinner0",
                shuffle: "off",
                viewPort: {
                    enable: !1,
                    outof: "wait",
                    visible_area: "60%"
                },
                fallbacks: {
                    isJoomla: !1,
                    panZoomDisableOnMobile: "off",
                    simplifyAll: "on",
                    nextSlideOnWindowFocus: "off",
                    disableFocusListener: !0,
                    ignoreHeightChanges: "off",
                    ignoreHeightChangesSize: 0
                },
                parallax: {
                    type: "off",
                    levels: [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60, 65, 70, 75, 80, 85],
                    origo: "enterpoint",
                    speed: 400,
                    bgparallax: "off",
                    opacity: "on",
                    disable_onmobile: "off",
                    ddd_shadow: "on",
                    ddd_bgfreeze: "off",
                    ddd_overflow: "visible",
                    ddd_layer_overflow: "visible",
                    ddd_z_correction: 65,
                    ddd_path: "mouse"
                },
                carousel: {
                    horizontal_align: "center",
                    vertical_align: "center",
                    infinity: "on",
                    space: 0,
                    maxVisibleItems: 3,
                    stretch: "off",
                    fadeout: "on",
                    maxRotation: 0,
                    minScale: 0,
                    vary_fade: "off",
                    vary_rotation: "on",
                    vary_scale: "off",
                    border_radius: "0px",
                    padding_top: 0,
                    padding_bottom: 0
                },
                navigation: {
                    keyboardNavigation: "off",
                    keyboard_direction: "horizontal",
                    mouseScrollNavigation: "off",
                    onHoverStop: "on",
                    touch: {
                        touchenabled: "off",
                        swipe_treshold: 75,
                        swipe_min_touches: 1,
                        drag_block_vertical: !1,
                        swipe_direction: "horizontal"
                    },
                    arrows: {
                        style: "",
                        enable: !1,
                        hide_onmobile: !1,
                        hide_onleave: !0,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        hide_under: 0,
                        hide_over: 9999,
                        tmp: "",
                        rtl: !1,
                        left: {
                            h_align: "left",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0,
                            container: "slider"
                        },
                        right: {
                            h_align: "right",
                            v_align: "center",
                            h_offset: 20,
                            v_offset: 0,
                            container: "slider"
                        }
                    },
                    bullets: {
                        container: "slider",
                        rtl: !1,
                        style: "",
                        enable: !1,
                        hide_onmobile: !1,
                        hide_onleave: !0,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        hide_under: 0,
                        hide_over: 9999,
                        direction: "horizontal",
                        h_align: "left",
                        v_align: "center",
                        space: 0,
                        h_offset: 20,
                        v_offset: 0,
                        tmp: '<span class="tp-bullet-image"></span><span class="tp-bullet-title"></span>'
                    },
                    thumbnails: {
                        container: "slider",
                        rtl: !1,
                        style: "",
                        enable: !1,
                        width: 100,
                        height: 50,
                        min_width: 100,
                        wrapper_padding: 2,
                        wrapper_color: "#f5f5f5",
                        wrapper_opacity: 1,
                        tmp: '<span class="tp-thumb-image"></span><span class="tp-thumb-title"></span>',
                        visibleAmount: 5,
                        hide_onmobile: !1,
                        hide_onleave: !0,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        hide_under: 0,
                        hide_over: 9999,
                        direction: "horizontal",
                        span: !1,
                        position: "inner",
                        space: 2,
                        h_align: "left",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    },
                    tabs: {
                        container: "slider",
                        rtl: !1,
                        style: "",
                        enable: !1,
                        width: 100,
                        min_width: 100,
                        height: 50,
                        wrapper_padding: 10,
                        wrapper_color: "#f5f5f5",
                        wrapper_opacity: 1,
                        tmp: '<span class="tp-tab-image"></span>',
                        visibleAmount: 5,
                        hide_onmobile: !1,
                        hide_onleave: !0,
                        hide_delay: 200,
                        hide_delay_mobile: 1200,
                        hide_under: 0,
                        hide_over: 9999,
                        direction: "horizontal",
                        span: !1,
                        space: 0,
                        position: "inner",
                        h_align: "left",
                        v_align: "center",
                        h_offset: 20,
                        v_offset: 0
                    }
                },
                extensions: "extensions/",
                extensions_suffix: ".min.js",
                debugMode: !1
            };
            return e = jQuery.extend(!0, {}, t, e),
            this.each(function() {
                var t = jQuery(this);
                e.minHeight = e.minHeight != undefined ? parseInt(e.minHeight, 0) : e.minHeight,
                "hero" == e.sliderType && t.find(">ul>li").each(function(e) {
                    e > 0 && jQuery(this).remove()
                }),
                e.jsFileLocation = e.jsFileLocation || getScriptLocation("themepunch.revolution.min.js"),
                e.jsFileLocation = e.jsFileLocation + e.extensions,
                e.scriptsneeded = getNeededScripts(e, t),
                e.curWinRange = 0,
                e.rtl = !0,
                e.navigation != undefined && e.navigation.touch != undefined && (e.navigation.touch.swipe_min_touches = e.navigation.touch.swipe_min_touches > 5 ? 1 : e.navigation.touch.swipe_min_touches),
                jQuery(this).on("scriptsloaded", function() {
                    return e.modulesfailing ? (t.html('<div style="margin:auto;line-height:40px;font-size:14px;color:#fff;padding:15px;background:#e74c3c;margin:20px 0px;">!! Error at loading Slider Revolution 5.0 Extrensions.' + e.errorm + "</div>").show(),
                    !1) : (_R.migration != undefined && (e = _R.migration(t, e)),
                    punchgs.force3D = !0,
                    "on" !== e.simplifyAll && punchgs.TweenLite.lagSmoothing(1e3, 16),
                    prepareOptions(t, e),
                    void initSlider(t, e))
                }),
                t.data("opt", e),
                waitForScripts(t, e)
            })
        },
        revremoveslide: function(e) {
            return this.each(function() {
                var t = jQuery(this);
                if (t != undefined && t.length > 0 && jQuery("body").find("#" + t.attr("id")).length > 0) {
                    var i = t.parent().find(".tp-bannertimer")
                      , n = i.data("opt");
                    if (n && n.li.length > 0 && (e > 0 || e <= n.li.length)) {
                        var a = jQuery(n.li[e])
                          , r = a.data("index")
                          , o = !1;
                        n.slideamount = n.slideamount - 1,
                        removeNavWithLiref(".tp-bullet", r, n),
                        removeNavWithLiref(".tp-tab", r, n),
                        removeNavWithLiref(".tp-thumb", r, n),
                        a.hasClass("active-revslide") && (o = !0),
                        a.remove(),
                        n.li = removeArray(n.li, e),
                        n.carousel && n.carousel.slides && (n.carousel.slides = removeArray(n.carousel.slides, e)),
                        n.thumbs = removeArray(n.thumbs, e),
                        _R.updateNavIndexes && _R.updateNavIndexes(n),
                        o && t.revnext(),
                        punchgs.TweenLite.set(n.li, {
                            minWidth: "99%"
                        }),
                        punchgs.TweenLite.set(n.li, {
                            minWidth: "100%"
                        })
                    }
                }
            })
        },
        revaddcallback: function(e) {
            return this.each(function() {
                var t = jQuery(this);
                if (t != undefined && t.length > 0 && jQuery("body").find("#" + t.attr("id")).length > 0) {
                    var i = t.parent().find(".tp-bannertimer")
                      , n = i.data("opt");
                    n.callBackArray === undefined && (n.callBackArray = new Array),
                    n.callBackArray.push(e)
                }
            })
        },
        revgetparallaxproc: function() {
            var e = jQuery(this);
            if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                var t = e.parent().find(".tp-bannertimer")
                  , i = t.data("opt");
                return i.scrollproc
            }
        },
        revdebugmode: function() {
            return this.each(function() {
                var e = jQuery(this);
                if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                    var t = e.parent().find(".tp-bannertimer")
                      , i = t.data("opt");
                    i.debugMode = !0,
                    containerResized(e, i)
                }
            })
        },
        revscroll: function(e) {
            return this.each(function() {
                var t = jQuery(this);
                t != undefined && t.length > 0 && jQuery("body").find("#" + t.attr("id")).length > 0 && jQuery("body,html").animate({
                    scrollTop: t.offset().top + t.height() - e + "px"
                }, {
                    duration: 400
                })
            })
        },
        revredraw: function(e) {
            return this.each(function() {
                var e = jQuery(this);
                if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                    var t = e.parent().find(".tp-bannertimer")
                      , i = t.data("opt");
                    containerResized(e, i)
                }
            })
        },
        revkill: function(e) {
            var t = this
              , i = jQuery(this);
            if (punchgs.TweenLite.killDelayedCallsTo(_R.showHideNavElements),
            _R.endMoveCaption && a.endtimeouts && a.endtimeouts.length > 0 && jQuery.each(a.endtimeouts, function(e, t) {
                clearTimeout(t)
            }),
            i != undefined && i.length > 0 && jQuery("body").find("#" + i.attr("id")).length > 0) {
                i.data("conthover", 1),
                i.data("conthover-changed", 1),
                i.trigger("revolution.slide.onpause");
                var n = i.parent().find(".tp-bannertimer")
                  , a = n.data("opt");
                a.tonpause = !0,
                i.trigger("stoptimer"),
                punchgs.TweenLite.killTweensOf(i.find("*"), !1),
                punchgs.TweenLite.killTweensOf(i, !1),
                i.unbind("hover, mouseover, mouseenter,mouseleave, resize");
                var r = "resize.revslider-" + i.attr("id");
                jQuery(window).off(r),
                i.find("*").each(function() {
                    var e = jQuery(this);
                    e.unbind("on, hover, mouseenter,mouseleave,mouseover, resize,restarttimer, stoptimer"),
                    e.off("on, hover, mouseenter,mouseleave,mouseover, resize"),
                    e.data("mySplitText", null),
                    e.data("ctl", null),
                    e.data("tween") != undefined && e.data("tween").kill(),
                    e.data("kenburn") != undefined && e.data("kenburn").kill(),
                    e.data("timeline_out") != undefined && e.data("timeline_out").kill(),
                    e.data("timeline") != undefined && e.data("timeline").kill(),
                    e.remove(),
                    e.empty(),
                    e = null
                }),
                punchgs.TweenLite.killTweensOf(i.find("*"), !1),
                punchgs.TweenLite.killTweensOf(i, !1),
                n.remove();
                try {
                    i.closest(".forcefullwidth_wrapper_tp_banner").remove()
                } catch (o) {}
                try {
                    i.closest(".rev_slider_wrapper").remove()
                } catch (o) {}
                try {
                    i.remove()
                } catch (o) {}
                return i.empty(),
                i.html(),
                i = null,
                a = null,
                delete t.c,
                delete t.opt,
                !0
            }
            return !1
        },
        revpause: function() {
            return this.each(function() {
                var e = jQuery(this);
                if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                    e.data("conthover", 1),
                    e.data("conthover-changed", 1),
                    e.trigger("revolution.slide.onpause");
                    var t = e.parent().find(".tp-bannertimer")
                      , i = t.data("opt");
                    i.tonpause = !0,
                    e.trigger("stoptimer")
                }
            })
        },
        revresume: function() {
            return this.each(function() {
                var e = jQuery(this);
                if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                    e.data("conthover", 0),
                    e.data("conthover-changed", 1),
                    e.trigger("revolution.slide.onresume");
                    var t = e.parent().find(".tp-bannertimer")
                      , i = t.data("opt");
                    i.tonpause = !1,
                    e.trigger("starttimer")
                }
            })
        },
        revstart: function() {
            var e = jQuery(this);
            return e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0 && e.data("opt") ? e.data("opt").sliderisrunning ? (console.log("Slider Is Running Already"),
            !1) : (runSlider(e, e.data("opt")),
            !0) : void 0
        },
        revnext: function() {
            return this.each(function() {
                var e = jQuery(this);
                if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                    var t = e.parent().find(".tp-bannertimer")
                      , i = t.data("opt");
                    _R.callingNewSlide(i, e, 1)
                }
            })
        },
        revprev: function() {
            return this.each(function() {
                var e = jQuery(this);
                if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                    var t = e.parent().find(".tp-bannertimer")
                      , i = t.data("opt");
                    _R.callingNewSlide(i, e, -1)
                }
            })
        },
        revmaxslide: function() {
            return jQuery(this).find(".tp-revslider-mainul >li").length
        },
        revcurrentslide: function() {
            var e = jQuery(this);
            if (e != undefined && e.length > 0 && jQuery("body").find("#" + e.attr("id")).length > 0) {
                var t = e.parent().find(".tp-bannertimer")
                  , i = t.data("opt");
                return parseInt(i.act, 0) + 1
            }
        },
        revlastslide: function() {
            return jQuery(this).find(".tp-revslider-mainul >li").length
        },
        revshowslide: function(e) {
            return this.each(function() {
                var t = jQuery(this);
                if (t != undefined && t.length > 0 && jQuery("body").find("#" + t.attr("id")).length > 0) {
                    var i = t.parent().find(".tp-bannertimer")
                      , n = i.data("opt");
                    _R.callingNewSlide(n, t, "to" + (e - 1))
                }
            })
        },
        revcallslidewithid: function(e) {
            return this.each(function() {
                var t = jQuery(this);
                if (t != undefined && t.length > 0 && jQuery("body").find("#" + t.attr("id")).length > 0) {
                    var i = t.parent().find(".tp-bannertimer")
                      , n = i.data("opt");
                    _R.callingNewSlide(n, t, e)
                }
            })
        }
    });
    var _R = jQuery.fn.revolution;
    jQuery.extend(!0, _R, {
        simp: function(e, t, i) {
            var n = Math.abs(e) - Math.floor(Math.abs(e / t)) * t;
            return i ? n : 0 > e ? -1 * n : n
        },
        iOSVersion: function() {
            var e = !1;
            return navigator.userAgent.match(/iPhone/i) || navigator.userAgent.match(/iPod/i) || navigator.userAgent.match(/iPad/i) ? navigator.userAgent.match(/OS 4_\d like Mac OS X/i) && (e = !0) : e = !1,
            e
        },
        isIE: function(e, t) {
            var i = jQuery('<div style="display:none;"/>').appendTo(jQuery("body"));
            i.html("<!--[if " + (t || "") + " IE " + (e || "") + "]><a>&nbsp;</a><![endif]-->");
            var n = i.find("a").length;
            return i.remove(),
            n
        },
        is_mobile: function() {
            var e = ["android", "webos", "iphone", "ipad", "blackberry", "Android", "webos", , "iPod", "iPhone", "iPad", "Blackberry", "BlackBerry"]
              , t = !1;
            for (var i in e)
                navigator.userAgent.split(e[i]).length > 1 && (t = !0);
            return t
        },
        callBackHandling: function(e, t, i) {
            try {
                e.callBackArray && jQuery.each(e.callBackArray, function(e, n) {
                    n && n.inmodule && n.inmodule === t && n.atposition && n.atposition === i && n.callback && n.callback.call()
                })
            } catch (n) {
                console.log("Call Back Failed")
            }
        },
        get_browser: function() {
            var e, t = navigator.appName, i = navigator.userAgent, n = i.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
            return n && null != (e = i.match(/version\/([\.\d]+)/i)) && (n[2] = e[1]),
            n = n ? [n[1], n[2]] : [t, navigator.appVersion, "-?"],
            n[0]
        },
        get_browser_version: function() {
            var e, t = navigator.appName, i = navigator.userAgent, n = i.match(/(opera|chrome|safari|firefox|msie)\/?\s*(\.?\d+(\.\d+)*)/i);
            return n && null != (e = i.match(/version\/([\.\d]+)/i)) && (n[2] = e[1]),
            n = n ? [n[1], n[2]] : [t, navigator.appVersion, "-?"],
            n[1]
        },
        getHorizontalOffset: function(e, t) {
            var i = gWiderOut(e, ".outer-left")
              , n = gWiderOut(e, ".outer-right");
            switch (t) {
            case "left":
                return i;
            case "right":
                return n;
            case "both":
                return i + n
            }
        },
        callingNewSlide: function(e, t, i) {
            var n = t.find(".next-revslide").length > 0 ? t.find(".next-revslide").index() : t.find(".processing-revslide").length > 0 ? t.find(".processing-revslide").index() : t.find(".active-revslide").index()
              , a = 0;
            t.find(".next-revslide").removeClass("next-revslide"),
            t.find(".active-revslide").hasClass("tp-invisible-slide") && (n = e.last_shown_slide),
            i && jQuery.isNumeric(i) || i.match(/to/g) ? (1 === i || -1 === i ? (a = n + i,
            a = 0 > a ? e.slideamount - 1 : a >= e.slideamount ? 0 : a) : (i = jQuery.isNumeric(i) ? i : parseInt(i.split("to")[1], 0),
            a = 0 > i ? 0 : i > e.slideamount - 1 ? e.slideamount - 1 : i),
            t.find(".tp-revslider-slidesli:eq(" + a + ")").addClass("next-revslide")) : i && t.find(".tp-revslider-slidesli").each(function() {
                var e = jQuery(this);
                e.data("index") === i && e.addClass("next-revslide")
            }),
            a = t.find(".next-revslide").index(),
            t.trigger("revolution.nextslide.waiting"),
            n === a && n === e.last_shown_slide || a !== n && -1 != a ? swapSlide(t, e) : t.find(".next-revslide").removeClass("next-revslide")
        },
        slotSize: function(e, t) {
            t.slotw = Math.ceil(t.width / t.slots),
            "fullscreen" == t.sliderLayout ? t.sloth = Math.ceil(jQuery(window).height() / t.slots) : t.sloth = Math.ceil(t.height / t.slots),
            "on" == t.autoHeight && e !== undefined && "" !== e && (t.sloth = Math.ceil(e.height() / t.slots))
        },
        setSize: function(e) {
            var t = (e.top_outer || 0) + (e.bottom_outer || 0)
              , i = parseInt(e.carousel.padding_top || 0, 0)
              , n = parseInt(e.carousel.padding_bottom || 0, 0)
              , a = e.gridheight[e.curWinRange];
            if (e.paddings = e.paddings === undefined ? {
                top: parseInt(e.c.parent().css("paddingTop"), 0) || 0,
                bottom: parseInt(e.c.parent().css("paddingBottom"), 0) || 0
            } : e.paddings,
            a = a < e.minHeight ? e.minHeight : a,
            "fullwidth" == e.sliderLayout && "off" == e.autoHeight && punchgs.TweenLite.set(e.c, {
                maxHeight: a + "px"
            }),
            e.c.css({
                marginTop: i,
                marginBottom: n
            }),
            e.width = e.ul.width(),
            e.height = e.ul.height(),
            setScale(e),
            e.height = Math.round(e.gridheight[e.curWinRange] * (e.width / e.gridwidth[e.curWinRange])),
            e.height > e.gridheight[e.curWinRange] && "on" != e.autoHeight && (e.height = e.gridheight[e.curWinRange]),
            "fullscreen" == e.sliderLayout || e.infullscreenmode) {
                e.height = e.bw * e.gridheight[e.curWinRange];
                var r = (e.c.parent().width(),
                jQuery(window).height());
                if (e.fullScreenOffsetContainer != undefined) {
                    try {
                        var o = e.fullScreenOffsetContainer.split(",");
                        o && jQuery.each(o, function(e, t) {
                            r = jQuery(t).length > 0 ? r - jQuery(t).outerHeight(!0) : r
                        })
                    } catch (s) {}
                    try {
                        e.fullScreenOffset.split("%").length > 1 && e.fullScreenOffset != undefined && e.fullScreenOffset.length > 0 ? r -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : e.fullScreenOffset != undefined && e.fullScreenOffset.length > 0 && (r -= parseInt(e.fullScreenOffset, 0))
                    } catch (s) {}
                }
                r = r < e.minHeight ? e.minHeight : r,
                r -= t,
                e.c.parent().height(r),
                e.c.closest(".rev_slider_wrapper").height(r),
                e.c.css({
                    height: "100%"
                }),
                e.height = r,
                e.minHeight != undefined && e.height < e.minHeight && (e.height = e.minHeight)
            } else
                e.minHeight != undefined && e.height < e.minHeight && (e.height = e.minHeight),
                e.c.height(e.height);
            var d = {
                height: i + n + t + e.height + e.paddings.top + e.paddings.bottom
            };
            e.c.closest(".forcefullwidth_wrapper_tp_banner").find(".tp-fullwidth-forcer").css(d),
            e.c.closest(".rev_slider_wrapper").css(d),
            setScale(e)
        },
        enterInViewPort: function(e) {
            e.waitForCountDown && (countDown(e.c, e),
            e.waitForCountDown = !1),
            e.waitForFirstSlide && (swapSlide(e.c, e),
            e.waitForFirstSlide = !1),
            ("playing" == e.sliderlaststatus || e.sliderlaststatus == undefined) && e.c.trigger("starttimer"),
            e.lastplayedvideos != undefined && e.lastplayedvideos.length > 0 && jQuery.each(e.lastplayedvideos, function(t, i) {
                _R.playVideo(i, e)
            })
        },
        leaveViewPort: function(e) {
            e.sliderlaststatus = e.sliderstatus,
            e.c.trigger("stoptimer"),
            e.playingvideos != undefined && e.playingvideos.length > 0 && (e.lastplayedvideos = jQuery.extend(!0, [], e.playingvideos),
            e.playingvideos && jQuery.each(e.playingvideos, function(t, i) {
                e.leaveViewPortBasedStop = !0,
                _R.stopVideo && _R.stopVideo(i, e)
            }))
        },
        unToggleState: function(e) {
            e != undefined && e.length > 0 && jQuery.each(e, function(e, t) {
                t.removeClass("rs-toggle-content-active")
            })
        },
        toggleState: function(e) {
            e != undefined && e.length > 0 && jQuery.each(e, function(e, t) {
                t.addClass("rs-toggle-content-active")
            })
        },
        lastToggleState: function(e) {
            var t = 0;
            return e != undefined && e.length > 0 && jQuery.each(e, function(e, i) {
                t = i.hasClass("rs-toggle-content-active")
            }),
            t
        }
    });
    var _ISM = _R.is_mobile()
      , removeArray = function(e, t) {
        var i = [];
        return jQuery.each(e, function(e, n) {
            e != t && i.push(n)
        }),
        i
    }
      , removeNavWithLiref = function(e, t, i) {
        i.c.find(e).each(function() {
            var e = jQuery(this);
            e.data("liref") === t && e.remove()
        })
    }
      , lAjax = function(e, t) {
        return jQuery("body").data(e) ? !1 : t.filesystem ? (t.errorm === undefined && (t.errorm = "<br>Local Filesystem Detected !<br>Put this to your header:"),
        console.warn("Local Filesystem detected !"),
        t.errorm = t.errorm + '<br>&lt;script type="text/javascript" src="' + 'https://uptblkpasuruan.com/assets/front/js/' + e + t.extensions_suffix + '"&gt;&lt;/script&gt;',
        console.warn(t.jsFileLocation + e + t.extensions_suffix + " could not be loaded !"),
        console.warn("Please use a local Server or work online or make sure that you load all needed Libraries manually in your Document."),
        console.log(" "),
        t.modulesfailing = !0,
        !1) : (jQuery.ajax({
            url: 'https://uptblkpasuruan.com/assets/front/js/' + e + t.extensions_suffix,
            dataType: "script",
            cache: !0,
            error: function(i) {
                console.warn("Slider Revolution 5.0 Error !"),
                console.error("Failure at Loading:" + e + t.extensions_suffix + " on Path:" + t.jsFileLocation),
                console.info(i)
            }
        }),
        void jQuery("body").data(e, !0))
    }
      , getNeededScripts = function(e, t) {
        var i = new Object
          , n = e.navigation;
        return i.kenburns = !1,
        i.parallax = !1,
        i.carousel = !1,
        i.navigation = !1,
        i.videos = !1,
        i.actions = !1,
        i.layeranim = !1,
        i.migration = !1,
        t.data("version") && t.data("version").toString().match(/5./gi) ? (t.find("img").each(function() {
            "on" == jQuery(this).data("kenburns") && (i.kenburns = !0)
        }),
        ("carousel" == e.sliderType || "on" == n.keyboardNavigation || "on" == n.mouseScrollNavigation || "on" == n.touch.touchenabled || n.arrows.enable || n.bullets.enable || n.thumbnails.enable || n.tabs.enable) && (i.navigation = !0),
        t.find(".tp-caption, .tp-static-layer, .rs-background-video-layer").each(function() {
            var e = jQuery(this);
            (e.data("ytid") != undefined || e.find("iframe").length > 0 && e.find("iframe").attr("src").toLowerCase().indexOf("youtube") > 0) && (i.videos = !0),
            (e.data("vimeoid") != undefined || e.find("iframe").length > 0 && e.find("iframe").attr("src").toLowerCase().indexOf("vimeo") > 0) && (i.videos = !0),
            e.data("actions") !== undefined && (i.actions = !0),
            i.layeranim = !0
        }),
        t.find("li").each(function() {
            jQuery(this).data("link") && jQuery(this).data("link") != undefined && (i.layeranim = !0,
            i.actions = !0)
        }),
        !i.videos && (t.find(".rs-background-video-layer").length > 0 || t.find(".tp-videolayer").length > 0 || t.find(".tp-audiolayer").length > 0 || t.find("iframe").length > 0 || t.find("video").length > 0) && (i.videos = !0),
        "carousel" == e.sliderType && (i.carousel = !0),
        ("off" !== e.parallax.type || e.viewPort.enable || "true" == e.viewPort.enable) && (i.parallax = !0)) : (i.kenburns = !0,
        i.parallax = !0,
        i.carousel = !1,
        i.navigation = !0,
        i.videos = !0,
        i.actions = !0,
        i.layeranim = !0,
        i.migration = !0),
        "hero" == e.sliderType && (i.carousel = !1,
        i.navigation = !1),
        window.location.href.match(/file:/gi) && (i.filesystem = !0,
        e.filesystem = !0),
        i.videos && "undefined" == typeof _R.isVideoPlaying && lAjax("revolution.extension.video", e),
        i.carousel && "undefined" == typeof _R.prepareCarousel && lAjax("revolution.extension.carousel", e),
        i.carousel || "undefined" != typeof _R.animateSlide || lAjax("revolution.extension.slideanims", e),
        i.actions && "undefined" == typeof _R.checkActions && lAjax("revolution.extension.actions", e),
        i.layeranim && "undefined" == typeof _R.handleStaticLayers && lAjax("revolution.extension.layeranimation", e),
        i.kenburns && "undefined" == typeof _R.stopKenBurn && lAjax("revolution.extension.kenburn", e),
        i.navigation && "undefined" == typeof _R.createNavigation && lAjax("revolution.extension.navigation", e),
        i.migration && "undefined" == typeof _R.migration && lAjax("revolution.extension.migration", e),
        i.parallax && "undefined" == typeof _R.checkForParallax && lAjax("revolution.extension.parallax", e),
        e.addons != undefined && e.addons.length > 0 && jQuery.each(e.addons, function(t, i) {
            "object" == typeof i && i.fileprefix != undefined && lAjax(i.fileprefix, e)
        }),
        i
    }
      , waitForScripts = function(e, t) {
        var i = !0
          , n = t.scriptsneeded;
        t.addons != undefined && t.addons.length > 0 && jQuery.each(t.addons, function(e, t) {
            "object" == typeof t && t.init != undefined && _R[t.init] === undefined && (i = !1)
        }),
        n.filesystem || "undefined" != typeof punchgs && i && (!n.kenburns || n.kenburns && "undefined" != typeof _R.stopKenBurn) && (!n.navigation || n.navigation && "undefined" != typeof _R.createNavigation) && (!n.carousel || n.carousel && "undefined" != typeof _R.prepareCarousel) && (!n.videos || n.videos && "undefined" != typeof _R.resetVideo) && (!n.actions || n.actions && "undefined" != typeof _R.checkActions) && (!n.layeranim || n.layeranim && "undefined" != typeof _R.handleStaticLayers) && (!n.migration || n.migration && "undefined" != typeof _R.migration) && (!n.parallax || n.parallax && "undefined" != typeof _R.checkForParallax) && (n.carousel || !n.carousel && "undefined" != typeof _R.animateSlide) ? e.trigger("scriptsloaded") : setTimeout(function() {
            waitForScripts(e, t)
        }, 50)
    }
      , getScriptLocation = function(e) {
        var t = new RegExp("themepunch.revolution.min.js","gi")
          , i = "";
        return jQuery("script").each(function() {
            var e = jQuery(this).attr("src");
            e && e.match(t) && (i = e)
        }),
        i = i.replace("jquery.themepunch.revolution.min.js", ""),
        i = i.replace("jquery.themepunch.revolution.js", ""),
        i = i.split("?")[0]
    }
      , setCurWinRange = function(e, t) {
        var i = 9999
          , n = 0
          , a = 0
          , r = 0
          , o = jQuery(window).width()
          , s = t && 9999 == e.responsiveLevels ? e.visibilityLevels : e.responsiveLevels;
        s && s.length && jQuery.each(s, function(e, t) {
            t > o && (0 == n || n > t) && (i = t,
            r = e,
            n = t),
            o > t && t > n && (n = t,
            a = e)
        }),
        i > n && (r = a),
        t ? e.forcedWinRange = r : e.curWinRange = r
    }
      , prepareOptions = function(e, t) {
        t.carousel.maxVisibleItems = t.carousel.maxVisibleItems < 1 ? 999 : t.carousel.maxVisibleItems,
        t.carousel.vertical_align = "top" === t.carousel.vertical_align ? "0%" : "bottom" === t.carousel.vertical_align ? "100%" : "50%"
    }
      , gWiderOut = function(e, t) {
        var i = 0;
        return e.find(t).each(function() {
            var e = jQuery(this);
            !e.hasClass("tp-forcenotvisible") && i < e.outerWidth() && (i = e.outerWidth())
        }),
        i
    }
      , initSlider = function(container, opt) {
        return container == undefined ? !1 : (container.data("aimg") != undefined && ("enabled" == container.data("aie8") && _R.isIE(8) || "enabled" == container.data("amobile") && _ISM) && container.html('<img class="tp-slider-alternative-image" src="' + container.data("aimg") + '">'),
        container.find(">ul").addClass("tp-revslider-mainul"),
        opt.c = container,
        opt.ul = container.find(".tp-revslider-mainul"),
        opt.ul.find(">li").each(function(e) {
            var t = jQuery(this);
            "on" == t.data("hideslideonmobile") && _ISM && t.remove(),
            (t.data("invisible") || t.data("invisible") === !0) && (t.addClass("tp-invisible-slide"),
            t.appendTo(opt.ul))
        }),
        opt.addons != undefined && opt.addons.length > 0 && jQuery.each(opt.addons, function(i, obj) {
            "object" == typeof obj && obj.init != undefined && _R[obj.init](eval(obj.params))
        }),
        opt.cid = container.attr("id"),
        opt.ul.css({
            visibility: "visible"
        }),
        opt.slideamount = opt.ul.find(">li").not(".tp-invisible-slide").length,
        opt.slayers = container.find(".tp-static-layers"),
        void (1 != opt.waitForInit && (container.data("opt", opt),
        runSlider(container, opt))))
    }
      , onFullScreenChange = function() {
        jQuery("body").data("rs-fullScreenMode", !jQuery("body").data("rs-fullScreenMode")),
        jQuery("body").data("rs-fullScreenMode") && setTimeout(function() {
            jQuery(window).trigger("resize")
        }, 200)
    }
      , runSlider = function(e, t) {
        if (t.sliderisrunning = !0,
        t.ul.find(">li").each(function(e) {
            jQuery(this).data("originalindex", e)
        }),
        "on" == t.shuffle) {
            var i = new Object
              , n = t.ul.find(">li:first-child");
            i.fstransition = n.data("fstransition"),
            i.fsmasterspeed = n.data("fsmasterspeed"),
            i.fsslotamount = n.data("fsslotamount");
            for (var a = 0; a < t.slideamount; a++) {
                var r = Math.round(Math.random() * t.slideamount);
                t.ul.find(">li:eq(" + r + ")").prependTo(t.ul)
            }
            var o = t.ul.find(">li:first-child");
            o.data("fstransition", i.fstransition),
            o.data("fsmasterspeed", i.fsmasterspeed),
            o.data("fsslotamount", i.fsslotamount),
            t.li = t.ul.find(">li").not(".tp-invisible-slide")
        }
        if (t.allli = t.ul.find(">li"),
        t.li = t.ul.find(">li").not(".tp-invisible-slide"),
        t.inli = t.ul.find(">li.tp-invisible-slide"),
        t.thumbs = new Array,
        t.slots = 4,
        t.act = -1,
        t.firststart = 1,
        t.loadqueue = new Array,
        t.syncload = 0,
        t.conw = e.width(),
        t.conh = e.height(),
        t.responsiveLevels.length > 1 ? t.responsiveLevels[0] = 9999 : t.responsiveLevels = 9999,
        jQuery.each(t.allli, function(e, i) {
            var i = jQuery(i)
              , n = i.find(".rev-slidebg") || i.find("img").first()
              , a = 0;
            i.addClass("tp-revslider-slidesli"),
            i.data("index") === undefined && i.data("index", "rs-" + Math.round(999999 * Math.random()));
            var r = new Object;
            r.params = new Array,
            r.id = i.data("index"),
            r.src = i.data("thumb") !== undefined ? i.data("thumb") : n.data("lazyload") !== undefined ? n.data("lazyload") : n.attr("src"),
            i.data("title") !== undefined && r.params.push({
                from: RegExp("\\{\\{title\\}\\}", "g"),
                to: i.data("title")
            }),
            i.data("description") !== undefined && r.params.push({
                from: RegExp("\\{\\{description\\}\\}", "g"),
                to: i.data("description")
            });
            for (var a = 1; 10 >= a; a++)
                i.data("param" + a) !== undefined && r.params.push({
                    from: RegExp("\\{\\{param" + a + "\\}\\}", "g"),
                    to: i.data("param" + a)
                });
            if (t.thumbs.push(r),
            i.data("origindex", i.index()),
            i.data("link") != undefined) {
                var o = i.data("link")
                  , s = i.data("target") || "_self"
                  , d = "back" === i.data("slideindex") ? 0 : 60
                  , l = i.data("linktoslide")
                  , u = l;
                l != undefined && "next" != l && "prev" != l && t.allli.each(function() {
                    var e = jQuery(this);
                    e.data("origindex") + 1 == u && (l = e.data("index"))
                }),
                "slide" != o && (l = "no");
                var c = '<div class="tp-caption slidelink" style="cursor:pointer;width:100%;height:100%;z-index:' + d + ';" data-x="center" data-y="center" data-basealign="slide" '
                  , p = "scroll_under" === l ? '[{"event":"click","action":"scrollbelow","offset":"100px","delay":"0"}]' : "prev" === l ? '[{"event":"click","action":"jumptoslide","slide":"prev","delay":"0.2"}]' : "next" === l ? '[{"event":"click","action":"jumptoslide","slide":"next","delay":"0.2"}]' : '[{"event":"click","action":"jumptoslide","slide":"' + l + '","delay":"0.2"}]';
                c = "no" == l ? c + ' data-start="0">' : c + "data-actions='" + p + '\' data-start="0">',
                c += '<a style="width:100%;height:100%;display:block"',
                c = "slide" != o ? c + ' target="' + s + '" href="' + o + '"' : c,
                c += '><span style="width:100%;height:100%;display:block"></span></a></div>',
                i.append(c)
            }
        }),
        t.rle = t.responsiveLevels.length || 1,
        t.gridwidth = cArray(t.gridwidth, t.rle),
        t.gridheight = cArray(t.gridheight, t.rle),
        "on" == t.simplifyAll && (_R.isIE(8) || _R.iOSVersion()) && (e.find(".tp-caption").each(function() {
            var e = jQuery(this);
            e.removeClass("customin customout").addClass("fadein fadeout"),
            e.data("splitin", ""),
            e.data("speed", 400)
        }),
        t.allli.each(function() {
            var e = jQuery(this);
            e.data("transition", "fade"),
            e.data("masterspeed", 500),
            e.data("slotamount", 1);
            var t = e.find(".rev-slidebg") || e.find(">img").first();
            t.data("kenburns", "off")
        })),
        t.desktop = !navigator.userAgent.match(/(iPhone|iPod|iPad|Android|BlackBerry|BB10|mobi|tablet|opera mini|nexus 7)/i),
        t.autoHeight = "fullscreen" == t.sliderLayout ? "on" : t.autoHeight,
        "fullwidth" == t.sliderLayout && "off" == t.autoHeight && e.css({
            maxHeight: t.gridheight[t.curWinRange] + "px"
        }),
        "auto" != t.sliderLayout && 0 == e.closest(".forcefullwidth_wrapper_tp_banner").length && ("fullscreen" !== t.sliderLayout || "on" != t.fullScreenAutoWidth)) {
            var s = e.parent()
              , d = s.css("marginBottom")
              , l = s.css("marginTop")
              , u = e.attr("id") + "_forcefullwidth";
            d = d === undefined ? 0 : d,
            l = l === undefined ? 0 : l,
            s.wrap('<div class="forcefullwidth_wrapper_tp_banner" id="' + u + '" style="position:relative;width:100%;height:auto;margin-top:' + l + ";margin-bottom:" + d + '"></div>'),
            e.closest(".forcefullwidth_wrapper_tp_banner").append('<div class="tp-fullwidth-forcer" style="width:100%;height:' + e.height() + 'px"></div>'),
            e.parent().css({
                marginTop: "0px",
                marginBottom: "0px"
            }),
            e.parent().css({
                position: "absolute"
            })
        }
        if (t.shadow !== undefined && t.shadow > 0 && (e.parent().addClass("tp-shadow" + t.shadow),
        e.parent().append('<div class="tp-shadowcover"></div>'),
        e.parent().find(".tp-shadowcover").css({
            backgroundColor: e.parent().css("backgroundColor"),
            backgroundImage: e.parent().css("backgroundImage")
        })),
        setCurWinRange(t),
        setCurWinRange(t, !0),
        !e.hasClass("revslider-initialised")) {
            e.addClass("revslider-initialised"),
            e.addClass("tp-simpleresponsive"),
            e.attr("id") == undefined && e.attr("id", "revslider-" + Math.round(1e3 * Math.random() + 5)),
            t.firefox13 = !1,
            t.ie = !jQuery.support.opacity,
            t.ie9 = 9 == document.documentMode,
            t.origcd = t.delay;
            var c = jQuery.fn.jquery.split(".")
              , p = parseFloat(c[0])
              , f = parseFloat(c[1]);
            parseFloat(c[2] || "0");
            1 == p && 7 > f && e.html('<div style="text-align:center; padding:40px 0px; font-size:20px; color:#992222;"> The Current Version of jQuery:' + c + " <br>Please update your jQuery Version to min. 1.7 in Case you wish to use the Revolution Slider Plugin</div>"),
            p > 1 && (t.ie = !1);
            var h = new Object;
            h.addedyt = 0,
            h.addedvim = 0,
            h.addedvid = 0,
            e.find(".tp-caption, .rs-background-video-layer").each(function(e) {
                var i = jQuery(this)
                  , n = i.data("autoplayonlyfirsttime")
                  , a = i.data("autoplay")
                  , r = i.hasClass("tp-audiolayer")
                  , o = i.data("videoloop");
                i.hasClass("tp-static-layer") && _R.handleStaticLayers && _R.handleStaticLayers(i, t);
                var s = i.data("noposteronmobile") || i.data("noPosterOnMobile") || i.data("posteronmobile") || i.data("posterOnMobile") || i.data("posterOnMObile");
                i.data("noposteronmobile", s);
                var d = 0;
                if (i.find("iframe").each(function() {
                    punchgs.TweenLite.set(jQuery(this), {
                        autoAlpha: 0
                    }),
                    d++
                }),
                d > 0 && i.data("iframes", !0),
                i.hasClass("tp-caption")) {
                    var l = i.hasClass("slidelink") ? "width:100% !important;height:100% !important;" : "";
                    i.wrap('<div class="tp-parallax-wrap" style="' + l + 'position:absolute;visibility:hidden"><div class="tp-loop-wrap" style="' + l + 'position:absolute;"><div class="tp-mask-wrap" style="' + l + 'position:absolute" ></div></div></div>');
                    var u = ["pendulum", "rotate", "slideloop", "pulse", "wave"]
                      , c = i.closest(".tp-loop-wrap");
                    jQuery.each(u, function(e, t) {
                        var n = i.find(".rs-" + t)
                          , a = n.data() || "";
                        "" != a && (c.data(a),
                        c.addClass("rs-" + t),
                        n.children(0).unwrap(),
                        i.data("loopanimation", "on"))
                    }),
                    punchgs.TweenLite.set(i, {
                        visibility: "hidden"
                    })
                }
                var p = i.data("actions");
                p !== undefined && _R.checkActions(i, t, p),
                checkHoverDependencies(i, t),
                _R.checkVideoApis && (h = _R.checkVideoApis(i, t, h)),
                _ISM && ((1 == n || "true" == n) && (i.data("autoplayonlyfirsttime", "false"),
                n = !1),
                (1 == a || "true" == a || "on" == a || "1sttime" == a) && (i.data("autoplay", "off"),
                a = "off")),
                i.data("videoloop", o),
                r || 1 != n && "true" != n && "1sttime" != a || "loopandnoslidestop" == o || i.closest("li.tp-revslider-slidesli").addClass("rs-pause-timer-once"),
                r || 1 != a && "true" != a && "on" != a && "no1sttime" != a || "loopandnoslidestop" == o || i.closest("li.tp-revslider-slidesli").addClass("rs-pause-timer-always")
            }),
            e[0].addEventListener("mouseenter", function() {
                e.trigger("tp-mouseenter"),
                t.overcontainer = !0
            }, {
                passive: !0
            }),
            e[0].addEventListener("mouseover", function() {
                e.trigger("tp-mouseover"),
                t.overcontainer = !0
            }, {
                passive: !0
            }),
            e[0].addEventListener("mouseleave", function() {
                e.trigger("tp-mouseleft"),
                t.overcontainer = !1
            }, {
                passive: !0
            }),
            e.find(".tp-caption video").each(function(e) {
                var t = jQuery(this);
                t.removeClass("video-js vjs-default-skin"),
                t.attr("preload", ""),
                t.css({
                    display: "none"
                })
            }),
            "standard" !== t.sliderType && (t.lazyType = "all"),
            loadImages(e.find(".tp-static-layers"), t, 0, !0),
            waitForCurrentImages(e.find(".tp-static-layers"), t, function() {
                e.find(".tp-static-layers img").each(function() {
                    var e = jQuery(this)
                      , i = e.data("lazyload") != undefined ? e.data("lazyload") : e.attr("src")
                      , n = getLoadObj(t, i);
                    e.attr("src", n.src)
                })
            }),
            t.allli.each(function(e) {
                var i = jQuery(this);
                ("all" == t.lazyType || "smart" == t.lazyType && (0 == e || 1 == e || e == t.slideamount || e == t.slideamount - 1)) && (loadImages(i, t, e),
                waitForCurrentImages(i, t, function() {
                    "carousel" == t.sliderType && punchgs.TweenLite.to(i, 1, {
                        autoAlpha: 1,
                        ease: punchgs.Power3.easeInOut
                    })
                }))
            });
            var g = getUrlVars("#")[0];
            if (g.length < 9 && g.split("slide").length > 1) {
                var v = parseInt(g.split("slide")[1], 0);
                1 > v && (v = 1),
                v > t.slideamount && (v = t.slideamount),
                t.startWithSlide = v - 1
            }
            e.append('<div class="tp-loader ' + t.spinner + '"><div class="dot1"></div><div class="dot2"></div><div class="bounce1"></div><div class="bounce2"></div><div class="bounce3"></div></div>'),
            0 === e.find(".tp-bannertimer").length && e.append('<div class="tp-bannertimer" style="visibility:hidden"></div>'),
            e.find(".tp-bannertimer").css({
                width: "0%"
            }),
            e.find(".tp-bannertimer").data("opt", t),
            t.ul.css({
                display: "block"
            }),
            prepareSlides(e, t),
            "off" !== t.parallax.type && _R.checkForParallax && _R.checkForParallax(e, t),
            _R.setSize(t),
            "hero" !== t.sliderType && _R.createNavigation && _R.createNavigation(e, t),
            _R.resizeThumbsTabs && _R.resizeThumbsTabs && _R.resizeThumbsTabs(t),
            contWidthManager(t);
            var m = t.viewPort;
            t.inviewport = !1,
            m != undefined && m.enable && (jQuery.isNumeric(m.visible_area) || -1 !== m.visible_area.indexOf("%") && (m.visible_area = parseInt(m.visible_area) / 100),
            _R.scrollTicker && _R.scrollTicker(t, e)),
            setTimeout(function() {
                "carousel" == t.sliderType && _R.prepareCarousel && _R.prepareCarousel(t),
                !m.enable || m.enable && t.inviewport || m.enable && !t.inviewport && "wait" == !m.outof ? swapSlide(e, t) : t.waitForFirstSlide = !0,
                _R.manageNavigation && _R.manageNavigation(t),
                t.slideamount > 1 && (!m.enable || m.enable && t.inviewport ? countDown(e, t) : t.waitForCountDown = !0),
                setTimeout(function() {
                    e.trigger("revolution.slide.onloaded")
                }, 100)
            }, t.startDelay),
            t.startDelay = 0,
            jQuery("body").data("rs-fullScreenMode", !1),
            window.addEventListener("fullscreenchange", onFullScreenChange, {
                passive: !0
            }),
            window.addEventListener("mozfullscreenchange", onFullScreenChange, {
                passive: !0
            }),
            window.addEventListener("webkitfullscreenchange", onFullScreenChange, {
                passive: !0
            });
            var y = "resize.revslider-" + e.attr("id");
            jQuery(window).on(y, function() {
                if (console.log("happening"),
                e == undefined)
                    return !1;
                0 != jQuery("body").find(e) && contWidthManager(t);
                var i = !1;
                if ("fullscreen" == t.sliderLayout) {
                    var n = jQuery(window).height();
                    "mobile" == t.fallbacks.ignoreHeightChanges && _ISM || "always" == t.fallbacks.ignoreHeightChanges ? (t.fallbacks.ignoreHeightChangesSize = t.fallbacks.ignoreHeightChangesSize == undefined ? 0 : t.fallbacks.ignoreHeightChangesSize,
                    i = n != t.lastwindowheight && Math.abs(n - t.lastwindowheight) > t.fallbacks.ignoreHeightChangesSize) : i = n != t.lastwindowheight
                }
                (e.outerWidth(!0) != t.width || e.is(":hidden") || i) && (t.lastwindowheight = jQuery(window).height(),
                containerResized(e, t))
            }),
            hideSliderUnder(e, t),
            contWidthManager(t),
            t.fallbacks.disableFocusListener || "true" == t.fallbacks.disableFocusListener || t.fallbacks.disableFocusListener === !0 || tabBlurringCheck(e, t)
        }
    }
      , cArray = function(e, t) {
        if (!jQuery.isArray(e)) {
            var i = e;
            e = new Array,
            e.push(i)
        }
        if (e.length < t)
            for (var i = e[e.length - 1], n = 0; n < t - e.length + 2; n++)
                e.push(i);
        return e
    }
      , checkHoverDependencies = function(e, t) {
        "sliderenter" === e.data("start") && (t.layersonhover === undefined && (t.c.on("tp-mouseenter", function() {
            t.layersonhover && jQuery.each(t.layersonhover, function(e, i) {
                i.data("animdirection", "in");
                var n = i.data("timeline_out")
                  , a = "carousel" === t.sliderType ? 0 : t.width / 2 - t.gridwidth[t.curWinRange] * t.bw / 2
                  , r = 0
                  , o = i.closest(".tp-revslider-slidesli")
                  , s = i.closest(".tp-static-layers");
                if (o.length > 0 && o.hasClass("active-revslide") || o.hasClass("processing-revslide") || s.length > 0) {
                    n != undefined && (n.pause(0),
                    n.kill()),
                    _R.animateSingleCaption(i, t, a, r, 0, !1, !0);
                    var d = i.data("timeline");
                    i.data("triggerstate", "on"),
                    d.play(0)
                }
            })
        }),
        t.c.on("tp-mouseleft", function() {
            t.layersonhover && jQuery.each(t.layersonhover, function(e, i) {
                i.data("animdirection", "out"),
                i.data("triggered", !0),
                i.data("triggerstate", "off"),
                _R.stopVideo && _R.stopVideo(i, t),
                _R.endMoveCaption && _R.endMoveCaption(i, null, null, t)
            })
        }),
        t.layersonhover = new Array),
        t.layersonhover.push(e))
    }
      , contWidthManager = function(e) {
        var t = _R.getHorizontalOffset(e.c, "left");
        if ("auto" == e.sliderLayout || "fullscreen" === e.sliderLayout && "on" == e.fullScreenAutoWidth)
            "fullscreen" == e.sliderLayout && "on" == e.fullScreenAutoWidth ? punchgs.TweenLite.set(e.ul, {
                left: 0,
                width: e.c.width()
            }) : punchgs.TweenLite.set(e.ul, {
                left: t,
                width: e.c.width() - _R.getHorizontalOffset(e.c, "both")
            });
        else {
            var i = Math.ceil(e.c.closest(".forcefullwidth_wrapper_tp_banner").offset().left - t);
            punchgs.TweenLite.set(e.c.parent(), {
                left: 0 - i + "px",
                width: jQuery(window).width() - _R.getHorizontalOffset(e.c, "both")
            })
        }
        e.slayers && "fullwidth" != e.sliderLayout && "fullscreen" != e.sliderLayout && punchgs.TweenLite.set(e.slayers, {
            left: t
        })
    }
      , cv = function(e, t) {
        return e === undefined ? t : e
    }
      , hideSliderUnder = function(e, t, i) {
        var n = e.parent();
        jQuery(window).width() < t.hideSliderAtLimit ? (e.trigger("stoptimer"),
        "none" != n.css("display") && n.data("olddisplay", n.css("display")),
        n.css({
            display: "none"
        })) : e.is(":hidden") && i && (n.data("olddisplay") != undefined && "undefined" != n.data("olddisplay") && "none" != n.data("olddisplay") ? n.css({
            display: n.data("olddisplay")
        }) : n.css({
            display: "block"
        }),
        e.trigger("restarttimer"),
        setTimeout(function() {
            containerResized(e, t)
        }, 150)),
        _R.hideUnHideNav && _R.hideUnHideNav(t)
    }
      , containerResized = function(e, t) {
        if (1 == t.infullscreenmode && (t.minHeight = jQuery(window).height()),
        setCurWinRange(t),
        setCurWinRange(t, !0),
        !_R.resizeThumbsTabs || _R.resizeThumbsTabs(t) === !0) {
            if (hideSliderUnder(e, t, !0),
            contWidthManager(t),
            "carousel" == t.sliderType && _R.prepareCarousel(t, !0),
            e === undefined)
                return !1;
            _R.setSize(t),
            t.conw = t.c.width(),
            t.conh = t.infullscreenmode ? t.minHeight : t.c.height();
            var i = e.find(".active-revslide .slotholder")
              , n = e.find(".processing-revslide .slotholder");
            removeSlots(e, t, e, 2),
            "standard" === t.sliderType && (punchgs.TweenLite.set(n.find(".defaultimg"), {
                opacity: 0
            }),
            i.find(".defaultimg").css({
                opacity: 1
            })),
            "carousel" == t.sliderType && t.lastconw != t.conw && (clearTimeout(t.pcartimer),
            t.pcartimer = setTimeout(function() {
                _R.prepareCarousel(t, !0)
            }, 100),
            t.lastconw = t.conw),
            _R.manageNavigation && _R.manageNavigation(t),
            _R.animateTheCaptions && _R.animateTheCaptions(e.find(".active-revslide"), t, !0),
            "on" == n.data("kenburns") && _R.startKenBurn(n, t, n.data("kbtl").progress()),
            "on" == i.data("kenburns") && _R.startKenBurn(i, t, i.data("kbtl").progress()),
            _R.animateTheCaptions && _R.animateTheCaptions(n.closest("li"), t, !0),
            _R.manageNavigation && _R.manageNavigation(t)
        }
    }
      , setScale = function(e) {
        e.bw = e.width / e.gridwidth[e.curWinRange],
        e.bh = e.height / e.gridheight[e.curWinRange],
        e.bh > e.bw ? e.bh = e.bw : e.bw = e.bh,
        (e.bh > 1 || e.bw > 1) && (e.bw = 1,
        e.bh = 1)
    }
      , prepareSlides = function(e, t) {
        if (e.find(".tp-caption").each(function() {
            var e = jQuery(this);
            e.data("transition") !== undefined && e.addClass(e.data("transition"))
        }),
        t.ul.css({
            overflow: "hidden",
            width: "100%",
            height: "100%",
            maxHeight: e.parent().css("maxHeight")
        }),
        "on" == t.autoHeight && (t.ul.css({
            overflow: "hidden",
            width: "100%",
            height: "100%",
            maxHeight: "none"
        }),
        e.css({
            maxHeight: "none"
        }),
        e.parent().css({
            maxHeight: "none"
        })),
        t.allli.each(function(e) {
            var i = jQuery(this)
              , n = i.data("originalindex");
            (t.startWithSlide != undefined && n == t.startWithSlide || t.startWithSlide === undefined && 0 == e) && i.addClass("next-revslide"),
            i.css({
                width: "100%",
                height: "100%",
                overflow: "hidden"
            })
        }),
        "carousel" === t.sliderType) {
            t.ul.css({
                overflow: "visible"
            }).wrap('<div class="tp-carousel-wrapper" style="width:100%;height:100%;position:absolute;top:0px;left:0px;overflow:hidden;"></div>');
            var i = '<div style="clear:both;display:block;width:100%;height:1px;position:relative;margin-bottom:-1px"></div>';
            t.c.parent().prepend(i),
            t.c.parent().append(i),
            _R.prepareCarousel(t)
        }
        e.parent().css({
            overflow: "visible"
        }),
        t.allli.find(">img").each(function(e) {
            var i = jQuery(this)
              , n = i.closest("li").find(".rs-background-video-layer");
            n.addClass("defaultvid").css({
                zIndex: 30
            }),
            i.addClass("defaultimg"),
            "on" == t.fallbacks.panZoomDisableOnMobile && _ISM && (i.data("kenburns", "off"),
            i.data("bgfit", "cover")),
            i.wrap('<div class="slotholder" style="position:absolute; top:0px; left:0px; z-index:0;width:100%;height:100%;"></div>'),
            n.appendTo(i.closest("li").find(".slotholder"));
            var a = i.data();
            i.closest(".slotholder").data(a),
            n.length > 0 && a.bgparallax != undefined && n.data("bgparallax", a.bgparallax),
            "none" != t.dottedOverlay && t.dottedOverlay != undefined && i.closest(".slotholder").append('<div class="tp-dottedoverlay ' + t.dottedOverlay + '"></div>');
            var r = i.attr("src");
            a.src = r,
            a.bgfit = a.bgfit || "cover",
            a.bgrepeat = a.bgrepeat || "no-repeat",
            a.bgposition = a.bgposition || "center center";
            var o = i.closest(".slotholder");
            i.parent().append('<div class="tp-bgimg defaultimg" style="background-color:' + i.css("backgroundColor") + ";background-repeat:" + a.bgrepeat + ";background-image:url(" + r + ");background-size:" + a.bgfit + ";background-position:" + a.bgposition + ';width:100%;height:100%;"></div>');
            var s = document.createComment("Runtime Modification - Img tag is Still Available for SEO Goals in Source - " + i.get(0).outerHTML);
            i.replaceWith(s),
            i = o.find(".tp-bgimg"),
            i.data(a),
            i.attr("src", r),
            ("standard" === t.sliderType || "undefined" === t.sliderType) && i.css({
                opacity: 0
            })
        })
    }
      , removeSlots = function(e, t, i, n) {
        t.removePrepare = t.removePrepare + n,
        i.find(".slot, .slot-circle-wrapper").each(function() {
            jQuery(this).remove()
        }),
        t.transition = 0,
        t.removePrepare = 0
    }
      , cutParams = function(e) {
        var t = e;
        return e != undefined && e.length > 0 && (t = e.split("?")[0]),
        t
    }
      , relativeRedir = function(e) {
        return location.pathname.replace(/(.*)\/[^\/]*/, "$1/" + e)
    }
      , abstorel = function(e, t) {
        var i = e.split("/")
          , n = t.split("/");
        i.pop();
        for (var a = 0; a < n.length; a++)
            "." != n[a] && (".." == n[a] ? i.pop() : i.push(n[a]));
        return i.join("/")
    }
      , imgLoaded = function(e, t, i) {
        t.syncload--,
        t.loadqueue && jQuery.each(t.loadqueue, function(t, n) {
            var a = n.src.replace(/\.\.\/\.\.\//gi, "")
              , r = self.location.href
              , o = document.location.origin
              , s = r.substring(0, r.length - 1) + "/" + a
              , d = o + "/" + a
              , l = abstorel(self.location.href, n.src);
            r = r.substring(0, r.length - 1) + a,
            o += a,
            (cutParams(o) === cutParams(decodeURIComponent(e.src)) || cutParams(r) === cutParams(decodeURIComponent(e.src)) || cutParams(l) === cutParams(decodeURIComponent(e.src)) || cutParams(d) === cutParams(decodeURIComponent(e.src)) || cutParams(s) === cutParams(decodeURIComponent(e.src)) || cutParams(n.src) === cutParams(decodeURIComponent(e.src)) || cutParams(n.src).replace(/^.*\/\/[^\/]+/, "") === cutParams(decodeURIComponent(e.src)).replace(/^.*\/\/[^\/]+/, "") || "file://" === window.location.origin && cutParams(e.src).match(new RegExp(a))) && (n.progress = i,
            n.width = e.width,
            n.height = e.height)
        }),
        progressImageLoad(t)
    }
      , progressImageLoad = function(e) {
        3 != e.syncload && e.loadqueue && jQuery.each(e.loadqueue, function(t, i) {
            if (i.progress.match(/prepared/g) && e.syncload <= 3) {
                if (e.syncload++,
                "img" == i.type) {
                    var n = new Image;
                    n.onload = function() {
                        imgLoaded(this, e, "loaded"),
                        i.error = !1
                    }
                    ,
                    n.onerror = function() {
                        imgLoaded(this, e, "failed"),
                        i.error = !0
                    }
                    ,
                    n.src = i.src
                } else
                    jQuery.get(i.src, function(t) {
                        i.innerHTML = (new XMLSerializer).serializeToString(t.documentElement),
                        i.progress = "loaded",
                        e.syncload--,
                        progressImageLoad(e)
                    }).fail(function() {
                        i.progress = "failed",
                        e.syncload--,
                        progressImageLoad(e)
                    });
                i.progress = "inload"
            }
        })
    }
      , addToLoadQueue = function(e, t, i, n, a) {
        var r = !1;
        if (t.loadqueue && jQuery.each(t.loadqueue, function(t, i) {
            i.src === e && (r = !0)
        }),
        !r) {
            var o = new Object;
            o.src = e,
            o.starttoload = jQuery.now(),
            o.type = n || "img",
            o.prio = i,
            o.progress = "prepared",
            o["static"] = a,
            t.loadqueue.push(o)
        }
    }
      , loadImages = function(e, t, i, n) {
        e.find("img,.defaultimg, .tp-svg-layer").each(function() {
            var e = jQuery(this)
              , a = e.data("lazyload") !== undefined && "undefined" !== e.data("lazyload") ? e.data("lazyload") : e.data("svg_src") != undefined ? e.data("svg_src") : e.attr("src")
              , r = e.data("svg_src") != undefined ? "svg" : "img";
            e.data("start-to-load", jQuery.now()),
            addToLoadQueue(a, t, i, r, n)
        }),
        progressImageLoad(t)
    }
      , getLoadObj = function(e, t) {
        var i = new Object;
        return e.loadqueue && jQuery.each(e.loadqueue, function(e, n) {
            n.src == t && (i = n)
        }),
        i
    }
      , waitForCurrentImages = function(e, t, i) {
        var n = !1;
        e.find("img,.defaultimg, .tp-svg-layer").each(function() {
            var i = jQuery(this)
              , a = i.data("lazyload") != undefined ? i.data("lazyload") : i.data("svg_src") != undefined ? i.data("svg_src") : i.attr("src")
              , r = getLoadObj(t, a);
            if (i.data("loaded") === undefined && r !== undefined && r.progress && r.progress.match(/loaded/g)) {
                if (i.attr("src", r.src),
                "img" == r.type)
                    if (i.hasClass("defaultimg"))
                        _R.isIE(8) ? defimg.attr("src", r.src) : i.css({
                            backgroundImage: 'url("' + r.src + '")'
                        }),
                        e.data("owidth", r.width),
                        e.data("oheight", r.height),
                        e.find(".slotholder").data("owidth", r.width),
                        e.find(".slotholder").data("oheight", r.height);
                    else {
                        var o = i.data("ww")
                          , s = i.data("hh");
                        i.data("owidth", r.width),
                        i.data("oheight", r.height),
                        o = o == undefined || "auto" == o || "" == o ? r.width : o,
                        s = s == undefined || "auto" == s || "" == s ? r.height : s,
                        i.data("ww", o),
                        i.data("hh", s)
                    }
                else
                    "svg" == r.type && "loaded" == r.progress && (i.append('<div class="tp-svg-innercontainer"></div>'),
                    i.find(".tp-svg-innercontainer").append(r.innerHTML));
                i.data("loaded", !0)
            }
            if (r && r.progress && r.progress.match(/inprogress|inload|prepared/g) && (!r.error && jQuery.now() - i.data("start-to-load") < 5e3 ? n = !0 : (r.progress = "failed",
            r.reported_img || (r.reported_img = !0,
            console.warn(a + "  Could not be loaded !")))),
            1 == t.youtubeapineeded && (!window.YT || YT.Player == undefined) && (n = !0,
            jQuery.now() - t.youtubestarttime > 5e3 && 1 != t.youtubewarning)) {
                t.youtubewarning = !0;
                var d = "YouTube Api Could not be loaded !";
                "https:" === location.protocol && (d += " Please Check and Renew SSL Certificate !"),
                console.error(d),
                t.c.append('<div style="position:absolute;top:50%;width:100%;color:#e74c3c;  font-size:16px; text-align:center; padding:15px;background:#000; display:block;"><strong>' + d + "</strong></div>")
            }
            if (1 == t.vimeoapineeded && !window.Froogaloop && (n = !0,
            jQuery.now() - t.vimeostarttime > 5e3 && 1 != t.vimeowarning)) {
                t.vimeowarning = !0;
                var d = "Vimeo Froogaloop Api Could not be loaded !";
                "https:" === location.protocol && (d += " Please Check and Renew SSL Certificate !"),
                console.error(d),
                t.c.append('<div style="position:absolute;top:50%;width:100%;color:#e74c3c;  font-size:16px; text-align:center; padding:15px;background:#000; display:block;"><strong>' + d + "</strong></div>")
            }
        }),
        !_ISM && t.audioqueue && t.audioqueue.length > 0 && jQuery.each(t.audioqueue, function(e, t) {
            t.status && "prepared" === t.status && jQuery.now() - t.start < t.waittime && (n = !0)
        }),
        jQuery.each(t.loadqueue, function(e, t) {
            t["static"] !== !0 || "loaded" == t.progress && "failed" !== t.progress || ("failed" == t.progress ? t.reported || (t.reported = !0,
            console.warn("Static Image " + t.src + "  Could not be loaded in time. Error Exists:" + t.error)) : !t.error && jQuery.now() - t.starttoload < 5e3 ? n = !0 : t.reported || (t.reported = !0,
            console.warn("Static Image " + t.src + "  Could not be loaded within 5s! Error Exists:" + t.error)))
        }),
        n ? setTimeout(function() {
            waitForCurrentImages(e, t, i)
        }, 19) : setTimeout(i, 19)
    }
      , swapSlide = function(e, t) {
        if (clearTimeout(t.waitWithSwapSlide),
        e.find(".processing-revslide").length > 0)
            return t.waitWithSwapSlide = setTimeout(function() {
                swapSlide(e, t)
            }, 150),
            !1;
        var i = e.find(".active-revslide")
          , n = e.find(".next-revslide")
          , a = n.find(".defaultimg");
        return n.index() === i.index() ? (n.removeClass("next-revslide"),
        !1) : (n.removeClass("next-revslide").addClass("processing-revslide"),
        n.data("slide_on_focus_amount", n.data("slide_on_focus_amount") + 1 || 1),
        "on" == t.stopLoop && n.index() == t.lastslidetoshow - 1 && (e.find(".tp-bannertimer").css({
            visibility: "hidden"
        }),
        e.trigger("revolution.slide.onstop"),
        t.noloopanymore = 1),
        n.index() === t.slideamount - 1 && (t.looptogo = t.looptogo - 1,
        t.looptogo <= 0 && (t.stopLoop = "on")),
        t.tonpause = !0,
        e.trigger("stoptimer"),
        t.cd = 0,
        "off" === t.spinner ? e.find(".tp-loader").css({
            display: "none"
        }) : e.find(".tp-loader").css({
            display: "block"
        }),
        loadImages(n, t, 1),
        _R.preLoadAudio && _R.preLoadAudio(n, t, 1),
        void waitForCurrentImages(n, t, function() {
            n.find(".rs-background-video-layer").each(function() {
                var e = jQuery(this);
                e.hasClass("HasListener") || (e.data("bgvideo", 1),
                _R.manageVideoLayer && _R.manageVideoLayer(e, t)),
                0 == e.find(".rs-fullvideo-cover").length && e.append('<div class="rs-fullvideo-cover"></div>')
            }),
            swapSlideProgress(t, a, e)
        }))
    }
      , swapSlideProgress = function(e, t, i) {
        var n = i.find(".active-revslide")
          , a = i.find(".processing-revslide")
          , r = n.find(".slotholder")
          , o = a.find(".slotholder");
        e.tonpause = !1,
        e.cd = 0,
        i.find(".tp-loader").css({
            display: "none"
        }),
        _R.setSize(e),
        _R.slotSize(t, e),
        _R.manageNavigation && _R.manageNavigation(e);
        var s = {};
        s.nextslide = a,
        s.currentslide = n,
        i.trigger("revolution.slide.onbeforeswap", s),
        e.transition = 1,
        e.videoplaying = !1,
        a.data("delay") != undefined ? (e.cd = 0,
        e.delay = a.data("delay")) : e.delay = e.origcd,
        "true" == a.data("ssop") || a.data("ssop") === !0 ? e.ssop = !0 : e.ssop = !1,
        i.trigger("nulltimer");
        var d = n.index()
          , l = a.index();
        e.sdir = d > l ? 1 : 0,
        "arrow" == e.sc_indicator && (0 == d && l == e.slideamount - 1 && (e.sdir = 1),
        d == e.slideamount - 1 && 0 == l && (e.sdir = 0)),
        e.lsdir = e.lsdir === undefined ? e.sdir : e.lsdir,
        e.dirc = e.lsdir != e.sdir,
        e.lsdir = e.sdir,
        n.index() != a.index() && 1 != e.firststart && _R.removeTheCaptions && _R.removeTheCaptions(n, e),
        a.hasClass("rs-pause-timer-once") || a.hasClass("rs-pause-timer-always") ? e.videoplaying = !0 : i.trigger("restarttimer"),
        a.removeClass("rs-pause-timer-once");
        var u, c;
        if ("carousel" == e.sliderType)
            c = new punchgs.TimelineLite,
            _R.prepareCarousel(e, c),
            letItFree(i, e, o, r, a, n, c),
            e.transition = 0,
            e.firststart = 0;
        else {
            c = new punchgs.TimelineLite({
                onComplete: function() {
                    letItFree(i, e, o, r, a, n, c)
                }
            }),
            c.add(punchgs.TweenLite.set(o.find(".defaultimg"), {
                opacity: 0
            })),
            c.pause(),
            1 == e.firststart && (punchgs.TweenLite.set(n, {
                autoAlpha: 0
            }),
            e.firststart = 0),
            punchgs.TweenLite.set(n, {
                zIndex: 18
            }),
            punchgs.TweenLite.set(a, {
                autoAlpha: 0,
                zIndex: 20
            }),
            "prepared" == a.data("differentissplayed") && (a.data("differentissplayed", "done"),
            a.data("transition", a.data("savedtransition")),
            a.data("slotamount", a.data("savedslotamount")),
            a.data("masterspeed", a.data("savedmasterspeed"))),
            a.data("fstransition") != undefined && "done" != a.data("differentissplayed") && (a.data("savedtransition", a.data("transition")),
            a.data("savedslotamount", a.data("slotamount")),
            a.data("savedmasterspeed", a.data("masterspeed")),
            a.data("transition", a.data("fstransition")),
            a.data("slotamount", a.data("fsslotamount")),
            a.data("masterspeed", a.data("fsmasterspeed")),
            a.data("differentissplayed", "prepared")),
            a.data("transition") == undefined && a.data("transition", "random"),
            u = 0;
            var p = a.data("transition") !== undefined ? a.data("transition").split(",") : "fade"
              , f = a.data("nexttransid") == undefined ? -1 : a.data("nexttransid");
            "on" == a.data("randomtransition") ? f = Math.round(Math.random() * p.length) : f += 1,
            f == p.length && (f = 0),
            a.data("nexttransid", f);
            var h = p[f];
            e.ie && ("boxfade" == h && (h = "boxslide"),
            "slotfade-vertical" == h && (h = "slotzoom-vertical"),
            "slotfade-horizontal" == h && (h = "slotzoom-horizontal")),
            _R.isIE(8) && (h = 11),
            c = _R.animateSlide(u, h, i, e, a, n, o, r, c),
            "on" == o.data("kenburns") && (_R.startKenBurn(o, e),
            c.add(punchgs.TweenLite.set(o, {
                autoAlpha: 0
            }))),
            c.pause()
        }
        _R.scrollHandling && (_R.scrollHandling(e, !0),
        c.eventCallback("onUpdate", function() {
            _R.scrollHandling(e, !0)
        })),
        "off" != e.parallax.type && e.parallax.firstgo == undefined && _R.scrollHandling && (e.parallax.firstgo = !0,
        e.lastscrolltop = -999,
        _R.scrollHandling(e, !0),
        setTimeout(function() {
            e.lastscrolltop = -999,
            _R.scrollHandling(e, !0)
        }, 210),
        setTimeout(function() {
            e.lastscrolltop = -999,
            _R.scrollHandling(e, !0)
        }, 420)),
        _R.animateTheCaptions ? _R.animateTheCaptions(a, e, null, c) : c != undefined && setTimeout(function() {
            c.resume()
        }, 30),
        punchgs.TweenLite.to(a, .001, {
            autoAlpha: 1
        })
    }
      , letItFree = function(e, t, i, n, a, r, o) {
        "carousel" === t.sliderType || (t.removePrepare = 0,
        punchgs.TweenLite.to(i.find(".defaultimg"), .001, {
            zIndex: 20,
            autoAlpha: 1,
            onComplete: function() {
                removeSlots(e, t, a, 1)
            }
        }),
        a.index() != r.index() && punchgs.TweenLite.to(r, .2, {
            zIndex: 18,
            autoAlpha: 0,
            onComplete: function() {
                removeSlots(e, t, r, 1)
            }
        })),
        e.find(".active-revslide").removeClass("active-revslide"),
        e.find(".processing-revslide").removeClass("processing-revslide").addClass("active-revslide"),
        t.act = a.index(),
        t.c.attr("data-slideactive", e.find(".active-revslide").data("index")),
        ("scroll" == t.parallax.type || "scroll+mouse" == t.parallax.type || "mouse+scroll" == t.parallax.type) && (t.lastscrolltop = -999,
        _R.scrollHandling(t)),
        o.clear(),
        n.data("kbtl") != undefined && (n.data("kbtl").reverse(),
        n.data("kbtl").timeScale(25)),
        "on" == i.data("kenburns") && (i.data("kbtl") != undefined ? (i.data("kbtl").timeScale(1),
        i.data("kbtl").play()) : _R.startKenBurn(i, t)),
        a.find(".rs-background-video-layer").each(function(e) {
            if (_ISM)
                return !1;
            var i = jQuery(this);
            _R.resetVideo(i, t),
            punchgs.TweenLite.fromTo(i, 1, {
                autoAlpha: 0
            }, {
                autoAlpha: 1,
                ease: punchgs.Power3.easeInOut,
                delay: .2,
                onComplete: function() {
                    _R.animcompleted && _R.animcompleted(i, t)
                }
            })
        }),
        r.find(".rs-background-video-layer").each(function(e) {
            if (_ISM)
                return !1;
            var i = jQuery(this);
            _R.stopVideo && (_R.resetVideo(i, t),
            _R.stopVideo(i, t)),
            punchgs.TweenLite.to(i, 1, {
                autoAlpha: 0,
                ease: punchgs.Power3.easeInOut,
                delay: .2
            })
        });
        var s = {};
        s.slideIndex = a.index() + 1,
        s.slideLIIndex = a.index(),
        s.slide = a,
        s.currentslide = a,
        s.prevslide = r,
        t.last_shown_slide = r.index(),
        e.trigger("revolution.slide.onchange", s),
        e.trigger("revolution.slide.onafterswap", s),
        t.duringslidechange = !1;
        var d = r.data("slide_on_focus_amount")
          , l = r.data("hideafterloop");
        0 != l && d >= l && t.c.revremoveslide(r.index())
    }
      , removeAllListeners = function(e, t) {
        e.children().each(function() {
            try {
                jQuery(this).die("click")
            } catch (e) {}
            try {
                jQuery(this).die("mouseenter")
            } catch (e) {}
            try {
                jQuery(this).die("mouseleave")
            } catch (e) {}
            try {
                jQuery(this).unbind("hover")
            } catch (e) {}
        });
        try {
            e.die("click", "mouseenter", "mouseleave")
        } catch (i) {}
        clearInterval(t.cdint),
        e = null
    }
      , countDown = function(e, t) {
        t.cd = 0,
        t.loop = 0,
        t.stopAfterLoops != undefined && t.stopAfterLoops > -1 ? t.looptogo = t.stopAfterLoops : t.looptogo = 9999999,
        t.stopAtSlide != undefined && t.stopAtSlide > -1 ? t.lastslidetoshow = t.stopAtSlide : t.lastslidetoshow = 999,
        t.stopLoop = "off",
        0 == t.looptogo && (t.stopLoop = "on");
        var i = e.find(".tp-bannertimer");
        e.on("stoptimer", function() {
            var e = jQuery(this).find(".tp-bannertimer");
            e.data("tween").pause(),
            "on" == t.disableProgressBar && e.css({
                visibility: "hidden"
            }),
            t.sliderstatus = "paused",
            _R.unToggleState(t.slidertoggledby)
        }),
        e.on("starttimer", function() {
            t.forcepause_viatoggle || (1 != t.conthover && 1 != t.videoplaying && t.width > t.hideSliderAtLimit && 1 != t.tonpause && 1 != t.overnav && 1 != t.ssop && (1 === t.noloopanymore || t.viewPort.enable && !t.inviewport || (i.css({
                visibility: "visible"
            }),
            i.data("tween").resume(),
            t.sliderstatus = "playing")),
            "on" == t.disableProgressBar && i.css({
                visibility: "hidden"
            }),
            _R.toggleState(t.slidertoggledby))
        }),
        e.on("restarttimer", function() {
            if (!t.forcepause_viatoggle) {
                var e = jQuery(this).find(".tp-bannertimer");
                if (t.mouseoncontainer && "on" == t.navigation.onHoverStop && !_ISM)
                    return !1;
                1 === t.noloopanymore || t.viewPort.enable && !t.inviewport || 1 == t.ssop || (e.css({
                    visibility: "visible"
                }),
                e.data("tween").kill(),
                e.data("tween", punchgs.TweenLite.fromTo(e, t.delay / 1e3, {
                    width: "0%"
                }, {
                    force3D: "auto",
                    width: "100%",
                    ease: punchgs.Linear.easeNone,
                    onComplete: n,
                    delay: 1
                })),
                t.sliderstatus = "playing"),
                "on" == t.disableProgressBar && e.css({
                    visibility: "hidden"
                }),
                _R.toggleState(t.slidertoggledby)
            }
        }),
        e.on("nulltimer", function() {
            i.data("tween").kill(),
            i.data("tween", punchgs.TweenLite.fromTo(i, t.delay / 1e3, {
                width: "0%"
            }, {
                force3D: "auto",
                width: "100%",
                ease: punchgs.Linear.easeNone,
                onComplete: n,
                delay: 1
            })),
            i.data("tween").pause(0),
            "on" == t.disableProgressBar && i.css({
                visibility: "hidden"
            }),
            t.sliderstatus = "paused"
        });
        var n = function() {
            0 == jQuery("body").find(e).length && (removeAllListeners(e, t),
            clearInterval(t.cdint)),
            e.trigger("revolution.slide.slideatend"),
            1 == e.data("conthover-changed") && (t.conthover = e.data("conthover"),
            e.data("conthover-changed", 0)),
            _R.callingNewSlide(t, e, 1)
        };
        i.data("tween", punchgs.TweenLite.fromTo(i, t.delay / 1e3, {
            width: "0%"
        }, {
            force3D: "auto",
            width: "100%",
            ease: punchgs.Linear.easeNone,
            onComplete: n,
            delay: 1
        })),
        i.data("opt", t),
        t.slideamount > 1 && (0 != t.stopAfterLoops || 1 != t.stopAtSlide) ? e.trigger("starttimer") : (t.noloopanymore = 1,
        e.trigger("nulltimer")),
        e.on("tp-mouseenter", function() {
            t.mouseoncontainer = !0,
            "on" != t.navigation.onHoverStop || _ISM || (e.trigger("stoptimer"),
            e.trigger("revolution.slide.onpause"))
        }),
        e.on("tp-mouseleft", function() {
            t.mouseoncontainer = !1,
            1 != e.data("conthover") && "on" == t.navigation.onHoverStop && (1 == t.viewPort.enable && t.inviewport || 0 == t.viewPort.enable) && (e.trigger("revolution.slide.onresume"),
            e.trigger("starttimer"))
        })
    }
      , vis = function() {
        var e, t, i = {
            hidden: "visibilitychange",
            webkitHidden: "webkitvisibilitychange",
            mozHidden: "mozvisibilitychange",
            msHidden: "msvisibilitychange"
        };
        for (e in i)
            if (e in document) {
                t = i[e];
                break
            }
        return function(i) {
            return i && document.addEventListener(t, i, {
                pasive: !0
            }),
            !document[e]
        }
    }()
      , restartOnFocus = function(e) {
        return e == undefined || e.c == undefined ? !1 : void (1 != e.windowfocused && (e.windowfocused = !0,
        punchgs.TweenLite.delayedCall(.3, function() {
            "on" == e.fallbacks.nextSlideOnWindowFocus && e.c.revnext(),
            e.c.revredraw(),
            "playing" == e.lastsliderstatus && e.c.revresume()
        })))
    }
      , lastStatBlur = function(e) {
        e.windowfocused = !1,
        e.lastsliderstatus = e.sliderstatus,
        e.c.revpause();
        var t = e.c.find(".active-revslide .slotholder")
          , i = e.c.find(".processing-revslide .slotholder");
        "on" == i.data("kenburns") && _R.stopKenBurn(i, e),
        "on" == t.data("kenburns") && _R.stopKenBurn(t, e)
    }
      , tabBlurringCheck = function(e, t) {
        var i = document.documentMode === undefined
          , n = window.chrome;
        i && !n ? jQuery(window).on("focusin", function() {
            restartOnFocus(t)
        }).on("focusout", function() {
            lastStatBlur(t)
        }) : window.addEventListener ? (window.addEventListener("focus", function(e) {
            restartOnFocus(t)
        }, {
            capture: !1,
            passive: !0
        }),
        window.addEventListener("blur", function(e) {
            lastStatBlur(t)
        }, {
            capture: !1,
            passive: !0
        })) : (window.attachEvent("focus", function(e) {
            restartOnFocus(t)
        }),
        window.attachEvent("blur", function(e) {
            lastStatBlur(t)
        }))
    }
      , getUrlVars = function(e) {
        for (var t, i = [], n = window.location.href.slice(window.location.href.indexOf(e) + 1).split("_"), a = 0; a < n.length; a++)
            n[a] = n[a].replace("%3D", "="),
            t = n[a].split("="),
            i.push(t[0]),
            i[t[0]] = t[1];
        return i
    }
}(jQuery);
jQuery(document).ready(function($) {
    $('.zilla-likes').live('click', function() {
        var link = $(this);
        if (link.hasClass('active'))
            return false;
        var id = $(this).attr('id')
          , postfix = link.find('.zilla-likes-postfix').text();
        $.post(zilla_likes.ajaxurl, {
            action: 'zilla-likes',
            likes_id: id,
            postfix: postfix
        }, function(data) {
            link.html(data).addClass('active').attr('title', 'You already like this');
        });
        return false;
    });
    if ($('body.ajax-zilla-likes').length) {
        $('.zilla-likes').each(function() {
            var id = $(this).attr('id');
            $(this).load(zilla_likes.ajaxurl, {
                action: 'zilla-likes',
                post_id: id
            });
        });
    }
});
;/*!
* jQuery blockUI plugin
* Version 2.70.0-2014.11.23
* Requires jQuery v1.7 or later
*
* Examples at: http://malsup.com/jquery/block/
* Copyright (c) 2007-2013 M. Alsup
* Dual licensed under the MIT and GPL licenses:
* http://www.opensource.org/licenses/mit-license.php
* http://www.gnu.org/licenses/gpl.html
*
* Thanks to Amir-Hossein Sobhi for some excellent contributions!
*/
(function() {
    "use strict";
    function setup($) {
        $.fn._fadeIn = $.fn.fadeIn;
        var noOp = $.noop || function() {}
        ;
        var msie = /MSIE/.test(navigator.userAgent);
        var ie6 = /MSIE 6.0/.test(navigator.userAgent) && !/MSIE 8.0/.test(navigator.userAgent);
        var mode = document.documentMode || 0;
        var setExpr = $.isFunction(document.createElement('div').style.setExpression);
        $.blockUI = function(opts) {
            install(window, opts);
        }
        ;
        $.unblockUI = function(opts) {
            remove(window, opts);
        }
        ;
        $.growlUI = function(title, message, timeout, onClose) {
            var $m = $('<div class="growlUI"></div>');
            if (title)
                $m.append('<h1>' + title + '</h1>');
            if (message)
                $m.append('<h2>' + message + '</h2>');
            if (timeout === undefined)
                timeout = 3000;
            var callBlock = function(opts) {
                opts = opts || {};
                $.blockUI({
                    message: $m,
                    fadeIn: typeof opts.fadeIn !== 'undefined' ? opts.fadeIn : 700,
                    fadeOut: typeof opts.fadeOut !== 'undefined' ? opts.fadeOut : 1000,
                    timeout: typeof opts.timeout !== 'undefined' ? opts.timeout : timeout,
                    centerY: false,
                    showOverlay: false,
                    onUnblock: onClose,
                    css: $.blockUI.defaults.growlCSS
                });
            };
            callBlock();
            var nonmousedOpacity = $m.css('opacity');
            $m.mouseover(function() {
                callBlock({
                    fadeIn: 0,
                    timeout: 30000
                });
                var displayBlock = $('.blockMsg');
                displayBlock.stop();
                displayBlock.fadeTo(300, 1);
            }).mouseout(function() {
                $('.blockMsg').fadeOut(1000);
            });
        }
        ;
        $.fn.block = function(opts) {
            if (this[0] === window) {
                $.blockUI(opts);
                return this;
            }
            var fullOpts = $.extend({}, $.blockUI.defaults, opts || {});
            this.each(function() {
                var $el = $(this);
                if (fullOpts.ignoreIfBlocked && $el.data('blockUI.isBlocked'))
                    return;
                $el.unblock({
                    fadeOut: 0
                });
            });
            return this.each(function() {
                if ($.css(this, 'position') == 'static') {
                    this.style.position = 'relative';
                    $(this).data('blockUI.static', true);
                }
                this.style.zoom = 1;
                install(this, opts);
            });
        }
        ;
        $.fn.unblock = function(opts) {
            if (this[0] === window) {
                $.unblockUI(opts);
                return this;
            }
            return this.each(function() {
                remove(this, opts);
            });
        }
        ;
        $.blockUI.version = 2.70;
        $.blockUI.defaults = {
            message: '<h1>Please wait...</h1>',
            title: null,
            draggable: true,
            theme: false,
            css: {
                padding: 0,
                margin: 0,
                width: '30%',
                top: '40%',
                left: '35%',
                textAlign: 'center',
                color: '#000',
                border: '3px solid #aaa',
                backgroundColor: '#fff',
                cursor: 'wait'
            },
            themedCSS: {
                width: '30%',
                top: '40%',
                left: '35%'
            },
            overlayCSS: {
                backgroundColor: '#000',
                opacity: 0.6,
                cursor: 'wait'
            },
            cursorReset: 'default',
            growlCSS: {
                width: '350px',
                top: '10px',
                left: '',
                right: '10px',
                border: 'none',
                padding: '5px',
                opacity: 0.6,
                cursor: 'default',
                color: '#fff',
                backgroundColor: '#000',
                '-webkit-border-radius': '10px',
                '-moz-border-radius': '10px',
                'border-radius': '10px'
            },
            iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',
            forceIframe: false,
            baseZ: 1000,
            centerX: true,
            centerY: true,
            allowBodyStretch: true,
            bindEvents: true,
            constrainTabKey: true,
            fadeIn: 200,
            fadeOut: 400,
            timeout: 0,
            showOverlay: true,
            focusInput: true,
            focusableElements: ':input:enabled:visible',
            onBlock: null,
            onUnblock: null,
            onOverlayClick: null,
            quirksmodeOffsetHack: 4,
            blockMsgClass: 'blockMsg',
            ignoreIfBlocked: false
        };
        var pageBlock = null;
        var pageBlockEls = [];
        function install(el, opts) {
            var css, themedCSS;
            var full = (el == window);
            var msg = (opts && opts.message !== undefined ? opts.message : undefined);
            opts = $.extend({}, $.blockUI.defaults, opts || {});
            if (opts.ignoreIfBlocked && $(el).data('blockUI.isBlocked'))
                return;
            opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
            css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
            if (opts.onOverlayClick)
                opts.overlayCSS.cursor = 'pointer';
            themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
            msg = msg === undefined ? opts.message : msg;
            if (full && pageBlock)
                remove(window, {
                    fadeOut: 0
                });
            if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
                var node = msg.jquery ? msg[0] : msg;
                var data = {};
                $(el).data('blockUI.history', data);
                data.el = node;
                data.parent = node.parentNode;
                data.display = node.style.display;
                data.position = node.style.position;
                if (data.parent)
                    data.parent.removeChild(node);
            }
            $(el).data('blockUI.onUnblock', opts.onUnblock);
            var z = opts.baseZ;
            var lyr1, lyr2, lyr3, s;
            if (msie || opts.forceIframe)
                lyr1 = $('<iframe class="blockUI" style="z-index:' + (z++) + ';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="' + opts.iframeSrc + '"></iframe>');
            else
                lyr1 = $('<div class="blockUI" style="display:none"></div>');
            if (opts.theme)
                lyr2 = $('<div class="blockUI blockOverlay ui-widget-overlay" style="z-index:' + (z++) + ';display:none"></div>');
            else
                lyr2 = $('<div class="blockUI blockOverlay" style="z-index:' + (z++) + ';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');
            if (opts.theme && full) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage ui-dialog ui-widget ui-corner-all" style="z-index:' + (z + 10) + ';display:none;position:fixed">';
                if (opts.title) {
                    s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (opts.title || '&nbsp;') + '</div>';
                }
                s += '<div class="ui-widget-content ui-dialog-content"></div>';
                s += '</div>';
            } else if (opts.theme) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement ui-dialog ui-widget ui-corner-all" style="z-index:' + (z + 10) + ';display:none;position:absolute">';
                if (opts.title) {
                    s += '<div class="ui-widget-header ui-dialog-titlebar ui-corner-all blockTitle">' + (opts.title || '&nbsp;') + '</div>';
                }
                s += '<div class="ui-widget-content ui-dialog-content"></div>';
                s += '</div>';
            } else if (full) {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockPage" style="z-index:' + (z + 10) + ';display:none;position:fixed"></div>';
            } else {
                s = '<div class="blockUI ' + opts.blockMsgClass + ' blockElement" style="z-index:' + (z + 10) + ';display:none;position:absolute"></div>';
            }
            lyr3 = $(s);
            if (msg) {
                if (opts.theme) {
                    lyr3.css(themedCSS);
                    lyr3.addClass('ui-widget-content');
                } else
                    lyr3.css(css);
            }
            if (!opts.theme)
                lyr2.css(opts.overlayCSS);
            lyr2.css('position', full ? 'fixed' : 'absolute');
            if (msie || opts.forceIframe)
                lyr1.css('opacity', 0.0);
            var layers = [lyr1, lyr2, lyr3]
              , $par = full ? $('body') : $(el);
            $.each(layers, function() {
                this.appendTo($par);
            });
            if (opts.theme && opts.draggable && $.fn.draggable) {
                lyr3.draggable({
                    handle: '.ui-dialog-titlebar',
                    cancel: 'li'
                });
            }
            var expr = setExpr && (!$.support.boxModel || $('object,embed', full ? null : el).length > 0);
            if (ie6 || expr) {
                if (full && opts.allowBodyStretch && $.support.boxModel)
                    $('html,body').css('height', '100%');
                if ((ie6 || !$.support.boxModel) && !full) {
                    var t = sz(el, 'borderTopWidth')
                      , l = sz(el, 'borderLeftWidth');
                    var fixT = t ? '(0 - ' + t + ')' : 0;
                    var fixL = l ? '(0 - ' + l + ')' : 0;
                }
                $.each(layers, function(i, o) {
                    var s = o[0].style;
                    s.position = 'absolute';
                    if (i < 2) {
                        if (full)
                            s.setExpression('height', 'Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.support.boxModel?0:' + opts.quirksmodeOffsetHack + ') + "px"');
                        else
                            s.setExpression('height', 'this.parentNode.offsetHeight + "px"');
                        if (full)
                            s.setExpression('width', 'jQuery.support.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"');
                        else
                            s.setExpression('width', 'this.parentNode.offsetWidth + "px"');
                        if (fixL)
                            s.setExpression('left', fixL);
                        if (fixT)
                            s.setExpression('top', fixT);
                    } else if (opts.centerY) {
                        if (full)
                            s.setExpression('top', '(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
                        s.marginTop = 0;
                    } else if (!opts.centerY && full) {
                        var top = (opts.css && opts.css.top) ? parseInt(opts.css.top, 10) : 0;
                        var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + ' + top + ') + "px"';
                        s.setExpression('top', expression);
                    }
                });
            }
            if (msg) {
                if (opts.theme)
                    lyr3.find('.ui-widget-content').append(msg);
                else
                    lyr3.append(msg);
                if (msg.jquery || msg.nodeType)
                    $(msg).show();
            }
            if ((msie || opts.forceIframe) && opts.showOverlay)
                lyr1.show();
            if (opts.fadeIn) {
                var cb = opts.onBlock ? opts.onBlock : noOp;
                var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
                var cb2 = msg ? cb : noOp;
                if (opts.showOverlay)
                    lyr2._fadeIn(opts.fadeIn, cb1);
                if (msg)
                    lyr3._fadeIn(opts.fadeIn, cb2);
            } else {
                if (opts.showOverlay)
                    lyr2.show();
                if (msg)
                    lyr3.show();
                if (opts.onBlock)
                    opts.onBlock.bind(lyr3)();
            }
            bind(1, el, opts);
            if (full) {
                pageBlock = lyr3[0];
                pageBlockEls = $(opts.focusableElements, pageBlock);
                if (opts.focusInput)
                    setTimeout(focus, 20);
            } else
                center(lyr3[0], opts.centerX, opts.centerY);
            if (opts.timeout) {
                var to = setTimeout(function() {
                    if (full)
                        $.unblockUI(opts);
                    else
                        $(el).unblock(opts);
                }, opts.timeout);
                $(el).data('blockUI.timeout', to);
            }
        }
        function remove(el, opts) {
            var count;
            var full = (el == window);
            var $el = $(el);
            var data = $el.data('blockUI.history');
            var to = $el.data('blockUI.timeout');
            if (to) {
                clearTimeout(to);
                $el.removeData('blockUI.timeout');
            }
            opts = $.extend({}, $.blockUI.defaults, opts || {});
            bind(0, el, opts);
            if (opts.onUnblock === null) {
                opts.onUnblock = $el.data('blockUI.onUnblock');
                $el.removeData('blockUI.onUnblock');
            }
            var els;
            if (full)
                els = $(document.body).children().filter('.blockUI').add('body > .blockUI');
            else
                els = $el.find('>.blockUI');
            if (opts.cursorReset) {
                if (els.length > 1)
                    els[1].style.cursor = opts.cursorReset;
                if (els.length > 2)
                    els[2].style.cursor = opts.cursorReset;
            }
            if (full)
                pageBlock = pageBlockEls = null;
            if (opts.fadeOut) {
                count = els.length;
                els.stop().fadeOut(opts.fadeOut, function() {
                    if (--count === 0)
                        reset(els, data, opts, el);
                });
            } else
                reset(els, data, opts, el);
        }
        function reset(els, data, opts, el) {
            var $el = $(el);
            if ($el.data('blockUI.isBlocked'))
                return;
            els.each(function(i, o) {
                if (this.parentNode)
                    this.parentNode.removeChild(this);
            });
            if (data && data.el) {
                data.el.style.display = data.display;
                data.el.style.position = data.position;
                data.el.style.cursor = 'default';
                if (data.parent)
                    data.parent.appendChild(data.el);
                $el.removeData('blockUI.history');
            }
            if ($el.data('blockUI.static')) {
                $el.css('position', 'static');
            }
            if (typeof opts.onUnblock == 'function')
                opts.onUnblock(el, opts);
            var body = $(document.body)
              , w = body.width()
              , cssW = body[0].style.width;
            body.width(w - 1).width(w);
            body[0].style.width = cssW;
        }
        function bind(b, el, opts) {
            var full = el == window
              , $el = $(el);
            if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
                return;
            $el.data('blockUI.isBlocked', b);
            if (!full || !opts.bindEvents || (b && !opts.showOverlay))
                return;
            var events = 'mousedown mouseup keydown keypress keyup touchstart touchend touchmove';
            if (b)
                $(document).bind(events, opts, handler);
            else
                $(document).unbind(events, handler);
        }
        function handler(e) {
            if (e.type === 'keydown' && e.keyCode && e.keyCode == 9) {
                if (pageBlock && e.data.constrainTabKey) {
                    var els = pageBlockEls;
                    var fwd = !e.shiftKey && e.target === els[els.length - 1];
                    var back = e.shiftKey && e.target === els[0];
                    if (fwd || back) {
                        setTimeout(function() {
                            focus(back);
                        }, 10);
                        return false;
                    }
                }
            }
            var opts = e.data;
            var target = $(e.target);
            if (target.hasClass('blockOverlay') && opts.onOverlayClick)
                opts.onOverlayClick(e);
            if (target.parents('div.' + opts.blockMsgClass).length > 0)
                return true;
            return target.parents().children().filter('div.blockUI').length === 0;
        }
        function focus(back) {
            if (!pageBlockEls)
                return;
            var e = pageBlockEls[back === true ? pageBlockEls.length - 1 : 0];
            if (e)
                e.focus();
        }
        function center(el, x, y) {
            var p = el.parentNode
              , s = el.style;
            var l = ((p.offsetWidth - el.offsetWidth) / 2) - sz(p, 'borderLeftWidth');
            var t = ((p.offsetHeight - el.offsetHeight) / 2) - sz(p, 'borderTopWidth');
            if (x)
                s.left = l > 0 ? (l + 'px') : '0';
            if (y)
                s.top = t > 0 ? (t + 'px') : '0';
        }
        function sz(el, p) {
            return parseInt($.css(el, p), 10) || 0;
        }
    }
    if (typeof define === 'function' && define.amd && define.amd.jQuery) {
        define(['jquery'], setup);
    } else {
        setup(jQuery);
    }
}
)();
jQuery(function($) {
    if (typeof wc_add_to_cart_params === 'undefined') {
        return false;
    }
    var AddToCartHandler = function() {
        $(document.body).on('click', '.add_to_cart_button', this.onAddToCart).on('click', '.remove_from_cart_button', this.onRemoveFromCart).on('added_to_cart', this.updateButton).on('added_to_cart', this.updateCartPage).on('added_to_cart removed_from_cart', this.updateFragments);
    };
    AddToCartHandler.prototype.onAddToCart = function(e) {
        var $thisbutton = $(this);
        if ($thisbutton.is('.ajax_add_to_cart')) {
            if (!$thisbutton.attr('data-product_id')) {
                return true;
            }
            e.preventDefault();
            $thisbutton.removeClass('added');
            $thisbutton.addClass('loading');
            var data = {};
            $.each($thisbutton.data(), function(key, value) {
                data[key] = value;
            });
            $(document.body).trigger('adding_to_cart', [$thisbutton, data]);
            $.post(wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'add_to_cart'), data, function(response) {
                if (!response) {
                    return;
                }
                if (response.error && response.product_url) {
                    window.location = response.product_url;
                    return;
                }
                if (wc_add_to_cart_params.cart_redirect_after_add === 'yes') {
                    window.location = wc_add_to_cart_params.cart_url;
                    return;
                }
                $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
            });
        }
    }
    ;
    AddToCartHandler.prototype.onRemoveFromCart = function(e) {
        var $thisbutton = $(this)
          , $row = $thisbutton.closest('.woocommerce-mini-cart-item');
        e.preventDefault();
        $row.block({
            message: null,
            overlayCSS: {
                opacity: 0.6
            }
        });
        $.post(wc_add_to_cart_params.wc_ajax_url.toString().replace('%%endpoint%%', 'remove_from_cart'), {
            cart_item_key: $thisbutton.data('cart_item_key')
        }, function(response) {
            if (!response || !response.fragments) {
                window.location = $thisbutton.attr('href');
                return;
            }
            $(document.body).trigger('removed_from_cart', [response.fragments, response.cart_hash, $thisbutton]);
        }).fail(function() {
            window.location = $thisbutton.attr('href');
            return;
        });
    }
    ;
    AddToCartHandler.prototype.updateButton = function(e, fragments, cart_hash, $button) {
        $button = typeof $button === 'undefined' ? false : $button;
        if ($button) {
            $button.removeClass('loading');
            $button.addClass('added');
            if (!wc_add_to_cart_params.is_cart && $button.parent().find('.added_to_cart').length === 0) {
                $button.after(' <a href="' + wc_add_to_cart_params.cart_url + '" class="added_to_cart wc-forward" title="' + wc_add_to_cart_params.i18n_view_cart + '">' + wc_add_to_cart_params.i18n_view_cart + '</a>');
            }
            $(document.body).trigger('wc_cart_button_updated', [$button]);
        }
    }
    ;
    AddToCartHandler.prototype.updateCartPage = function() {
        var page = window.location.toString().replace('add-to-cart', 'added-to-cart');
        $.get(page, function(data) {
            $('.shop_table.cart:eq(0)').replaceWith($(data).find('.shop_table.cart:eq(0)'));
            $('.cart_totals:eq(0)').replaceWith($(data).find('.cart_totals:eq(0)'));
            $('.cart_totals, .shop_table.cart').stop(true).css('opacity', '1').unblock();
            $(document.body).trigger('cart_page_refreshed');
            $(document.body).trigger('cart_totals_refreshed');
        });
    }
    ;
    AddToCartHandler.prototype.updateFragments = function(e, fragments) {
        if (fragments) {
            $.each(fragments, function(key) {
                $(key).addClass('updating').fadeTo('400', '0.6').block({
                    message: null,
                    overlayCSS: {
                        opacity: 0.6
                    }
                });
            });
            $.each(fragments, function(key, value) {
                $(key).replaceWith(value);
                $(key).stop(true).css('opacity', '1').unblock();
            });
            $(document.body).trigger('wc_fragments_loaded');
        }
    }
    ;
    new AddToCartHandler();
});
window.jQuery(document).ready(function($) {
    $('body').on('adding_to_cart', function(event, $button, data) {
        $button && $button.hasClass('vc_gitem-link') && $button.addClass('vc-gitem-add-to-cart-loading-btn').parents('.vc_grid-item-mini').addClass('vc-woocommerce-add-to-cart-loading').append($('<div class="vc_wc-load-add-to-loader-wrapper"><div class="vc_wc-load-add-to-loader"></div></div>'));
    }).on('added_to_cart', function(event, fragments, cart_hash, $button) {
        if ('undefined' === typeof ($button)) {
            $button = $('.vc-gitem-add-to-cart-loading-btn');
        }
        $button && $button.hasClass('vc_gitem-link') && $button.removeClass('vc-gitem-add-to-cart-loading-btn').parents('.vc_grid-item-mini').removeClass('vc-woocommerce-add-to-cart-loading').find('.vc_wc-load-add-to-loader-wrapper').remove();
    });
});
(function($) {
    'use strict';
    if (typeof wpcf7 === 'undefined' || wpcf7 === null) {
        return;
    }
    wpcf7 = $.extend({
        cached: 0,
        inputs: []
    }, wpcf7);
    $(function() {
        wpcf7.supportHtml5 = (function() {
            var features = {};
            var input = document.createElement('input');
            features.placeholder = 'placeholder'in input;
            var inputTypes = ['email', 'url', 'tel', 'number', 'range', 'date'];
            $.each(inputTypes, function(index, value) {
                input.setAttribute('type', value);
                features[value] = input.type !== 'text';
            });
            return features;
        }
        )();
        $('div.wpcf7 > form').each(function() {
            var $form = $(this);
            wpcf7.initForm($form);
            if (wpcf7.cached) {
                wpcf7.refill($form);
            }
        });
    });
    wpcf7.getId = function(form) {
        return parseInt($('input[name="_wpcf7"]', form).val(), 10);
    }
    ;
    wpcf7.initForm = function(form) {
        var $form = $(form);
        $form.submit(function(event) {
            if (!wpcf7.supportHtml5.placeholder) {
                $('[placeholder].placeheld', $form).each(function(i, n) {
                    $(n).val('').removeClass('placeheld');
                });
            }
            if (typeof window.FormData === 'function') {
                wpcf7.submit($form);
                event.preventDefault();
            }
        });
        $('.wpcf7-submit', $form).after('<span class="ajax-loader"></span>');
        wpcf7.toggleSubmit($form);
        $form.on('click', '.wpcf7-acceptance', function() {
            wpcf7.toggleSubmit($form);
        });
        $('.wpcf7-exclusive-checkbox', $form).on('click', 'input:checkbox', function() {
            var name = $(this).attr('name');
            $form.find('input:checkbox[name="' + name + '"]').not(this).prop('checked', false);
        });
        $('.wpcf7-list-item.has-free-text', $form).each(function() {
            var $freetext = $(':input.wpcf7-free-text', this);
            var $wrap = $(this).closest('.wpcf7-form-control');
            if ($(':checkbox, :radio', this).is(':checked')) {
                $freetext.prop('disabled', false);
            } else {
                $freetext.prop('disabled', true);
            }
            $wrap.on('change', ':checkbox, :radio', function() {
                var $cb = $('.has-free-text', $wrap).find(':checkbox, :radio');
                if ($cb.is(':checked')) {
                    $freetext.prop('disabled', false).focus();
                } else {
                    $freetext.prop('disabled', true);
                }
            });
        });
        if (!wpcf7.supportHtml5.placeholder) {
            $('[placeholder]', $form).each(function() {
                $(this).val($(this).attr('placeholder'));
                $(this).addClass('placeheld');
                $(this).focus(function() {
                    if ($(this).hasClass('placeheld')) {
                        $(this).val('').removeClass('placeheld');
                    }
                });
                $(this).blur(function() {
                    if ('' === $(this).val()) {
                        $(this).val($(this).attr('placeholder'));
                        $(this).addClass('placeheld');
                    }
                });
            });
        }
        if (wpcf7.jqueryUi && !wpcf7.supportHtml5.date) {
            $form.find('input.wpcf7-date[type="date"]').each(function() {
                $(this).datepicker({
                    dateFormat: 'yy-mm-dd',
                    minDate: new Date($(this).attr('min')),
                    maxDate: new Date($(this).attr('max'))
                });
            });
        }
        if (wpcf7.jqueryUi && !wpcf7.supportHtml5.number) {
            $form.find('input.wpcf7-number[type="number"]').each(function() {
                $(this).spinner({
                    min: $(this).attr('min'),
                    max: $(this).attr('max'),
                    step: $(this).attr('step')
                });
            });
        }
        $('.wpcf7-character-count', $form).each(function() {
            var $count = $(this);
            var name = $count.attr('data-target-name');
            var down = $count.hasClass('down');
            var starting = parseInt($count.attr('data-starting-value'), 10);
            var maximum = parseInt($count.attr('data-maximum-value'), 10);
            var minimum = parseInt($count.attr('data-minimum-value'), 10);
            var updateCount = function(target) {
                var $target = $(target);
                var length = $target.val().length;
                var count = down ? starting - length : length;
                $count.attr('data-current-value', count);
                $count.text(count);
                if (maximum && maximum < length) {
                    $count.addClass('too-long');
                } else {
                    $count.removeClass('too-long');
                }
                if (minimum && length < minimum) {
                    $count.addClass('too-short');
                } else {
                    $count.removeClass('too-short');
                }
            };
            $(':input[name="' + name + '"]', $form).each(function() {
                updateCount(this);
                $(this).keyup(function() {
                    updateCount(this);
                });
            });
        });
        $form.on('change', '.wpcf7-validates-as-url', function() {
            var val = $.trim($(this).val());
            if (val && !val.match(/^[a-z][a-z0-9.+-]*:/i) && -1 !== val.indexOf('.')) {
                val = val.replace(/^\/+/, '');
                val = 'http://' + val;
            }
            $(this).val(val);
        });
    }
    ;
    wpcf7.submit = function(form) {
        if (typeof window.FormData !== 'function') {
            return;
        }
        var $form = $(form);
        $('.ajax-loader', $form).addClass('is-active');
        wpcf7.clearResponse($form);
        var formData = new FormData($form.get(0));
        var detail = {
            id: $form.closest('div.wpcf7').attr('id'),
            status: 'init',
            inputs: [],
            formData: formData
        };
        $.each($form.serializeArray(), function(i, field) {
            if ('_wpcf7' == field.name) {
                detail.contactFormId = field.value;
            } else if ('_wpcf7_version' == field.name) {
                detail.pluginVersion = field.value;
            } else if ('_wpcf7_locale' == field.name) {
                detail.contactFormLocale = field.value;
            } else if ('_wpcf7_unit_tag' == field.name) {
                detail.unitTag = field.value;
            } else if ('_wpcf7_container_post' == field.name) {
                detail.containerPostId = field.value;
            } else if (field.name.match(/^_wpcf7_\w+_free_text_/)) {
                var owner = field.name.replace(/^_wpcf7_\w+_free_text_/, '');
                detail.inputs.push({
                    name: owner + '-free-text',
                    value: field.value
                });
            } else if (field.name.match(/^_/)) {} else {
                detail.inputs.push(field);
            }
        });
        wpcf7.triggerEvent($form.closest('div.wpcf7'), 'beforesubmit', detail);
        var ajaxSuccess = function(data, status, xhr, $form) {
            detail.id = $(data.into).attr('id');
            detail.status = data.status;
            detail.apiResponse = data;
            var $message = $('.wpcf7-response-output', $form);
            switch (data.status) {
            case 'validation_failed':
                $.each(data.invalidFields, function(i, n) {
                    $(n.into, $form).each(function() {
                        wpcf7.notValidTip(this, n.message);
                        $('.wpcf7-form-control', this).addClass('wpcf7-not-valid');
                        $('[aria-invalid]', this).attr('aria-invalid', 'true');
                    });
                });
                $message.addClass('wpcf7-validation-errors');
                $form.addClass('invalid');
                wpcf7.triggerEvent(data.into, 'invalid', detail);
                break;
            case 'acceptance_missing':
                $message.addClass('wpcf7-acceptance-missing');
                $form.addClass('unaccepted');
                wpcf7.triggerEvent(data.into, 'unaccepted', detail);
                break;
            case 'spam':
                $message.addClass('wpcf7-spam-blocked');
                $form.addClass('spam');
                wpcf7.triggerEvent(data.into, 'spam', detail);
                break;
            case 'aborted':
                $message.addClass('wpcf7-aborted');
                $form.addClass('aborted');
                wpcf7.triggerEvent(data.into, 'aborted', detail);
                break;
            case 'mail_sent':
                $message.addClass('wpcf7-mail-sent-ok');
                $form.addClass('sent');
                wpcf7.triggerEvent(data.into, 'mailsent', detail);
                break;
            case 'mail_failed':
                $message.addClass('wpcf7-mail-sent-ng');
                $form.addClass('failed');
                wpcf7.triggerEvent(data.into, 'mailfailed', detail);
                break;
            default:
                var customStatusClass = 'custom-' + data.status.replace(/[^0-9a-z]+/i, '-');
                $message.addClass('wpcf7-' + customStatusClass);
                $form.addClass(customStatusClass);
            }
            wpcf7.refill($form, data);
            wpcf7.triggerEvent(data.into, 'submit', detail);
            if ('mail_sent' == data.status) {
                $form.each(function() {
                    this.reset();
                });
                wpcf7.toggleSubmit($form);
            }
            if (!wpcf7.supportHtml5.placeholder) {
                $form.find('[placeholder].placeheld').each(function(i, n) {
                    $(n).val($(n).attr('placeholder'));
                });
            }
            $message.html('').append(data.message).slideDown('fast');
            $message.attr('role', 'alert');
            $('.screen-reader-response', $form.closest('.wpcf7')).each(function() {
                var $response = $(this);
                $response.html('').attr('role', '').append(data.message);
                if (data.invalidFields) {
                    var $invalids = $('<ul></ul>');
                    $.each(data.invalidFields, function(i, n) {
                        if (n.idref) {
                            var $li = $('<li></li>').append($('<a></a>').attr('href', '#' + n.idref).append(n.message));
                        } else {
                            var $li = $('<li></li>').append(n.message);
                        }
                        $invalids.append($li);
                    });
                    $response.append($invalids);
                }
                $response.attr('role', 'alert').focus();
            });
        };
        $.ajax({
            type: 'POST',
            url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/feedback'),
            data: formData,
            dataType: 'json',
            processData: false,
            contentType: false
        }).done(function(data, status, xhr) {
            ajaxSuccess(data, status, xhr, $form);
            $('.ajax-loader', $form).removeClass('is-active');
        }).fail(function(xhr, status, error) {
            var $e = $('<div class="ajax-error"></div>').text(error.message);
            $form.after($e);
        });
    }
    ;
    wpcf7.triggerEvent = function(target, name, detail) {
        var $target = $(target);
        var event = new CustomEvent('wpcf7' + name,{
            bubbles: true,
            detail: detail
        });
        $target.get(0).dispatchEvent(event);
        $target.trigger('wpcf7:' + name, detail);
        $target.trigger(name + '.wpcf7', detail);
    }
    ;
    wpcf7.toggleSubmit = function(form, state) {
        var $form = $(form);
        var $submit = $('input:submit', $form);
        if (typeof state !== 'undefined') {
            $submit.prop('disabled', !state);
            return;
        }
        if ($form.hasClass('wpcf7-acceptance-as-validation')) {
            return;
        }
        $submit.prop('disabled', false);
        $('.wpcf7-acceptance', $form).each(function() {
            var $span = $(this);
            var $input = $('input:checkbox', $span);
            if (!$span.hasClass('optional')) {
                if ($span.hasClass('invert') && $input.is(':checked') || !$span.hasClass('invert') && !$input.is(':checked')) {
                    $submit.prop('disabled', true);
                    return false;
                }
            }
        });
    }
    ;
    wpcf7.notValidTip = function(target, message) {
        var $target = $(target);
        $('.wpcf7-not-valid-tip', $target).remove();
        $('<span role="alert" class="wpcf7-not-valid-tip"></span>').text(message).appendTo($target);
        if ($target.is('.use-floating-validation-tip *')) {
            var fadeOut = function(target) {
                $(target).not(':hidden').animate({
                    opacity: 0
                }, 'fast', function() {
                    $(this).css({
                        'z-index': -100
                    });
                });
            };
            $target.on('mouseover', '.wpcf7-not-valid-tip', function() {
                fadeOut(this);
            });
            $target.on('focus', ':input', function() {
                fadeOut($('.wpcf7-not-valid-tip', $target));
            });
        }
    }
    ;
    wpcf7.refill = function(form, data) {
        var $form = $(form);
        var refillCaptcha = function($form, items) {
            $.each(items, function(i, n) {
                $form.find(':input[name="' + i + '"]').val('');
                $form.find('img.wpcf7-captcha-' + i).attr('src', n);
                var match = /([0-9]+)\.(png|gif|jpeg)$/.exec(n);
                $form.find('input:hidden[name="_wpcf7_captcha_challenge_' + i + '"]').attr('value', match[1]);
            });
        };
        var refillQuiz = function($form, items) {
            $.each(items, function(i, n) {
                $form.find(':input[name="' + i + '"]').val('');
                $form.find(':input[name="' + i + '"]').siblings('span.wpcf7-quiz-label').text(n[0]);
                $form.find('input:hidden[name="_wpcf7_quiz_answer_' + i + '"]').attr('value', n[1]);
            });
        };
        if (typeof data === 'undefined') {
            $.ajax({
                type: 'GET',
                url: wpcf7.apiSettings.getRoute('/contact-forms/' + wpcf7.getId($form) + '/refill'),
                beforeSend: function(xhr) {
                    var nonce = $form.find(':input[name="_wpnonce"]').val();
                    if (nonce) {
                        xhr.setRequestHeader('X-WP-Nonce', nonce);
                    }
                },
                dataType: 'json'
            }).done(function(data, status, xhr) {
                if (data.captcha) {
                    refillCaptcha($form, data.captcha);
                }
                if (data.quiz) {
                    refillQuiz($form, data.quiz);
                }
            });
        } else {
            if (data.captcha) {
                refillCaptcha($form, data.captcha);
            }
            if (data.quiz) {
                refillQuiz($form, data.quiz);
            }
        }
    }
    ;
    wpcf7.clearResponse = function(form) {
        var $form = $(form);
        $form.removeClass('invalid spam sent failed');
        $form.siblings('.screen-reader-response').html('').attr('role', '');
        $('.wpcf7-not-valid-tip', $form).remove();
        $('[aria-invalid]', $form).attr('aria-invalid', 'false');
        $('.wpcf7-form-control', $form).removeClass('wpcf7-not-valid');
        $('.wpcf7-response-output', $form).hide().empty().removeAttr('role').removeClass('wpcf7-mail-sent-ok wpcf7-mail-sent-ng wpcf7-validation-errors wpcf7-spam-blocked');
    }
    ;
    wpcf7.apiSettings.getRoute = function(path) {
        var url = wpcf7.apiSettings.root;
        url = url.replace(wpcf7.apiSettings.namespace, wpcf7.apiSettings.namespace + path);
        return url;
    }
    ;
}
)(jQuery);
(function() {
    if (typeof window.CustomEvent === "function")
        return false;
    function CustomEvent(event, params) {
        params = params || {
            bubbles: false,
            cancelable: false,
            detail: undefined
        };
        var evt = document.createEvent('CustomEvent');
        evt.initCustomEvent(event, params.bubbles, params.cancelable, params.detail);
        return evt;
    }
    CustomEvent.prototype = window.Event.prototype;
    window.CustomEvent = CustomEvent;
}
)();
jQuery(function($) {
    'use strict';
    (function() {
        $(".ttfi-btn").on("mouseenter", function() {
            $(this).closest(".tt-floating-info ul li").addClass("ttfi-hover")
        });
        $(".tt-floating-info ul li").on("mouseleave", function() {
            $(this).removeClass("ttfi-hover");
        });
    }());
});
;/*!
* JavaScript Cookie v2.1.4
* https://github.com/js-cookie/js-cookie
*
* Copyright 2006, 2015 Klaus Hartl & Fagner Brack
* Released under the MIT license
*/
(function(factory) {
    var registeredInModuleLoader = false;
    if (typeof define === 'function' && define.amd) {
        define(factory);
        registeredInModuleLoader = true;
    }
    if (typeof exports === 'object') {
        module.exports = factory();
        registeredInModuleLoader = true;
    }
    if (!registeredInModuleLoader) {
        var OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function() {
            window.Cookies = OldCookies;
            return api;
        }
        ;
    }
}(function() {
    function extend() {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[i];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }
    function init(converter) {
        function api(key, value, attributes) {
            var result;
            if (typeof document === 'undefined') {
                return;
            }
            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);
                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }
                attributes.expires = attributes.expires ? attributes.expires.toUTCString() : '';
                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {}
                if (!converter.write) {
                    value = encodeURIComponent(String(value)).replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
                } else {
                    value = converter.write(value, key);
                }
                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);
                var stringifiedAttributes = '';
                for (var attributeName in attributes) {
                    if (!attributes[attributeName]) {
                        continue;
                    }
                    stringifiedAttributes += '; ' + attributeName;
                    if (attributes[attributeName] === true) {
                        continue;
                    }
                    stringifiedAttributes += '=' + attributes[attributeName];
                }
                return (document.cookie = key + '=' + value + stringifiedAttributes);
            }
            if (!key) {
                result = {};
            }
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;
            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var cookie = parts.slice(1).join('=');
                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }
                try {
                    var name = parts[0].replace(rdecode, decodeURIComponent);
                    cookie = converter.read ? converter.read(cookie, name) : converter(cookie, name) || cookie.replace(rdecode, decodeURIComponent);
                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {}
                    }
                    if (key === name) {
                        result = cookie;
                        break;
                    }
                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {}
            }
            return result;
        }
        api.set = api;
        api.get = function(key) {
            return api.call(api, key);
        }
        ;
        api.getJSON = function() {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        }
        ;
        api.defaults = {};
        api.remove = function(key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        }
        ;
        api.withConverter = init;
        return api;
    }
    return init(function() {});
}));
jQuery(function($) {
    $('.woocommerce-ordering').on('change', 'select.orderby', function() {
        $(this).closest('form').submit();
    });
    $('input.qty:not(.product-quantity input.qty)').each(function() {
        var min = parseFloat($(this).attr('min'));
        if (min >= 0 && parseFloat($(this).val()) < min) {
            $(this).val(min);
        }
    });
    var noticeID = $('.woocommerce-store-notice').data('notice-id') || ''
      , cookieName = 'store_notice' + noticeID;
    if ('hidden' === Cookies.get(cookieName)) {
        $('.woocommerce-store-notice').hide();
    } else {
        $('.woocommerce-store-notice').show();
    }
    $('.woocommerce-store-notice__dismiss-link').click(function(event) {
        Cookies.set(cookieName, 'hidden', {
            path: '/'
        });
        $('.woocommerce-store-notice').hide();
        event.preventDefault();
    });
    $(document.body).on('click', function() {
        $('.woocommerce-input-wrapper span.description:visible').prop('aria-hidden', true).slideUp(250);
    });
    $('.woocommerce-input-wrapper').on('click', function(event) {
        event.stopPropagation();
    });
    $('.woocommerce-input-wrapper :input').on('keydown', function(event) {
        var input = $(this)
          , parent = input.parent()
          , description = parent.find('span.description');
        if (27 === event.which && description.length && description.is(':visible')) {
            description.prop('aria-hidden', true).slideUp(250);
            event.preventDefault();
            return false;
        }
    }).on('click focus', function() {
        var input = $(this)
          , parent = input.parent()
          , description = parent.find('span.description');
        parent.addClass('currentTarget');
        $('.woocommerce-input-wrapper:not(.currentTarget) span.description:visible').prop('aria-hidden', true).slideUp(250);
        if (description.length && description.is(':hidden')) {
            description.prop('aria-hidden', false).slideDown(250);
        }
        parent.removeClass('currentTarget');
    });
    $.scroll_to_notices = function(scrollElement) {
        if (scrollElement.length) {
            $('html, body').animate({
                scrollTop: (scrollElement.offset().top - 100)
            }, 1000);
        }
    }
    ;
});
jQuery(function($) {
    if (typeof wc_cart_fragments_params === 'undefined') {
        return false;
    }
    var $supports_html5_storage = true
      , cart_hash_key = wc_cart_fragments_params.cart_hash_key;
    try {
        $supports_html5_storage = ('sessionStorage'in window && window.sessionStorage !== null);
        window.sessionStorage.setItem('wc', 'test');
        window.sessionStorage.removeItem('wc');
        window.localStorage.setItem('wc', 'test');
        window.localStorage.removeItem('wc');
    } catch (err) {
        $supports_html5_storage = false;
    }
    function set_cart_creation_timestamp() {
        if ($supports_html5_storage) {
            sessionStorage.setItem('wc_cart_created', (new Date()).getTime());
        }
    }
    function set_cart_hash(cart_hash) {
        if ($supports_html5_storage) {
            localStorage.setItem(cart_hash_key, cart_hash);
            sessionStorage.setItem(cart_hash_key, cart_hash);
        }
    }
    var $fragment_refresh = {
        url: wc_cart_fragments_params.wc_ajax_url.toString().replace('%%endpoint%%', 'get_refreshed_fragments'),
        type: 'POST',
        data: {
            time: new Date().getTime()
        },
        timeout: wc_cart_fragments_params.request_timeout,
        success: function(data) {
            if (data && data.fragments) {
                $.each(data.fragments, function(key, value) {
                    $(key).replaceWith(value);
                });
                if ($supports_html5_storage) {
                    sessionStorage.setItem(wc_cart_fragments_params.fragment_name, JSON.stringify(data.fragments));
                    set_cart_hash(data.cart_hash);
                    if (data.cart_hash) {
                        set_cart_creation_timestamp();
                    }
                }
                $(document.body).trigger('wc_fragments_refreshed');
            }
        },
        error: function() {
            $(document.body).trigger('wc_fragments_ajax_error');
        }
    };
    function refresh_cart_fragment() {
        $.ajax($fragment_refresh);
    }
    if ($supports_html5_storage) {
        var cart_timeout = null
          , day_in_ms = (24 * 60 * 60 * 1000);
        $(document.body).on('wc_fragment_refresh updated_wc_div', function() {
            refresh_cart_fragment();
        });
        $(document.body).on('added_to_cart removed_from_cart', function(event, fragments, cart_hash) {
            var prev_cart_hash = sessionStorage.getItem(cart_hash_key);
            if (prev_cart_hash === null || prev_cart_hash === undefined || prev_cart_hash === '') {
                set_cart_creation_timestamp();
            }
            sessionStorage.setItem(wc_cart_fragments_params.fragment_name, JSON.stringify(fragments));
            set_cart_hash(cart_hash);
        });
        $(document.body).on('wc_fragments_refreshed', function() {
            clearTimeout(cart_timeout);
            cart_timeout = setTimeout(refresh_cart_fragment, day_in_ms);
        });
        $(window).on('storage onstorage', function(e) {
            if (cart_hash_key === e.originalEvent.key && localStorage.getItem(cart_hash_key) !== sessionStorage.getItem(cart_hash_key)) {
                refresh_cart_fragment();
            }
        });
        $(window).on('pageshow', function(e) {
            if (e.originalEvent.persisted) {
                $('.widget_shopping_cart_content').empty();
                $(document.body).trigger('wc_fragment_refresh');
            }
        });
        try {
            var wc_fragments = $.parseJSON(sessionStorage.getItem(wc_cart_fragments_params.fragment_name))
              , cart_hash = sessionStorage.getItem(cart_hash_key)
              , cookie_hash = Cookies.get('woocommerce_cart_hash')
              , cart_created = sessionStorage.getItem('wc_cart_created');
            if (cart_hash === null || cart_hash === undefined || cart_hash === '') {
                cart_hash = '';
            }
            if (cookie_hash === null || cookie_hash === undefined || cookie_hash === '') {
                cookie_hash = '';
            }
            if (cart_hash && (cart_created === null || cart_created === undefined || cart_created === '')) {
                throw 'No cart_created';
            }
            if (cart_created) {
                var cart_expiration = ((1 * cart_created) + day_in_ms)
                  , timestamp_now = (new Date()).getTime();
                if (cart_expiration < timestamp_now) {
                    throw 'Fragment expired';
                }
                cart_timeout = setTimeout(refresh_cart_fragment, (cart_expiration - timestamp_now));
            }
            if (wc_fragments && wc_fragments['div.widget_shopping_cart_content'] && cart_hash === cookie_hash) {
                $.each(wc_fragments, function(key, value) {
                    $(key).replaceWith(value);
                });
                $(document.body).trigger('wc_fragments_loaded');
            } else {
                throw 'No fragment';
            }
        } catch (err) {
            refresh_cart_fragment();
        }
    } else {
        refresh_cart_fragment();
    }
    if (Cookies.get('woocommerce_items_in_cart') > 0) {
        $('.hide_cart_widget_if_empty').closest('.widget_shopping_cart').show();
    } else {
        $('.hide_cart_widget_if_empty').closest('.widget_shopping_cart').hide();
    }
    $(document.body).on('adding_to_cart', function() {
        $('.hide_cart_widget_if_empty').closest('.widget_shopping_cart').show();
    });
    var hasSelectiveRefresh = ('undefined' !== typeof wp && wp.customize && wp.customize.selectiveRefresh && wp.customize.widgetsPreview && wp.customize.widgetsPreview.WidgetPartial);
    if (hasSelectiveRefresh) {
        wp.customize.selectiveRefresh.bind('partial-content-rendered', function() {
            refresh_cart_fragment();
        });
    }
});
/*!
* Bootstrap v3.3.7 (http://getbootstrap.com)
* Copyright 2011-2016 Twitter, Inc.
* Licensed under the MIT license
*/
if ("undefined" == typeof jQuery)
    throw new Error("Bootstrap's JavaScript requires jQuery");
+function(a) {
    "use strict";
    var b = a.fn.jquery.split(" ")[0].split(".");
    if (b[0] < 2 && b[1] < 9 || 1 == b[0] && 9 == b[1] && b[2] < 1 || b[0] > 3)
        throw new Error("Bootstrap's JavaScript requires jQuery version 1.9.1 or higher, but lower than version 4")
}(jQuery),
+function(a) {
    "use strict";
    function b() {
        var a = document.createElement("bootstrap")
          , b = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd otransitionend",
            transition: "transitionend"
        };
        for (var c in b)
            if (void 0 !== a.style[c])
                return {
                    end: b[c]
                };
        return !1
    }
    a.fn.emulateTransitionEnd = function(b) {
        var c = !1
          , d = this;
        a(this).one("bsTransitionEnd", function() {
            c = !0
        });
        var e = function() {
            c || a(d).trigger(a.support.transition.end)
        };
        return setTimeout(e, b),
        this
    }
    ,
    a(function() {
        a.support.transition = b(),
        a.support.transition && (a.event.special.bsTransitionEnd = {
            bindType: a.support.transition.end,
            delegateType: a.support.transition.end,
            handle: function(b) {
                if (a(b.target).is(this))
                    return b.handleObj.handler.apply(this, arguments)
            }
        })
    })
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var c = a(this)
              , e = c.data("bs.alert");
            e || c.data("bs.alert", e = new d(this)),
            "string" == typeof b && e[b].call(c)
        })
    }
    var c = '[data-dismiss="alert"]'
      , d = function(b) {
        a(b).on("click", c, this.close)
    };
    d.VERSION = "3.3.7",
    d.TRANSITION_DURATION = 150,
    d.prototype.close = function(b) {
        function c() {
            g.detach().trigger("closed.bs.alert").remove()
        }
        var e = a(this)
          , f = e.attr("data-target");
        f || (f = e.attr("href"),
        f = f && f.replace(/.*(?=#[^\s]*$)/, ""));
        var g = a("#" === f ? [] : f);
        b && b.preventDefault(),
        g.length || (g = e.closest(".alert")),
        g.trigger(b = a.Event("close.bs.alert")),
        b.isDefaultPrevented() || (g.removeClass("in"),
        a.support.transition && g.hasClass("fade") ? g.one("bsTransitionEnd", c).emulateTransitionEnd(d.TRANSITION_DURATION) : c())
    }
    ;
    var e = a.fn.alert;
    a.fn.alert = b,
    a.fn.alert.Constructor = d,
    a.fn.alert.noConflict = function() {
        return a.fn.alert = e,
        this
    }
    ,
    a(document).on("click.bs.alert.data-api", c, d.prototype.close)
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.button")
              , f = "object" == typeof b && b;
            e || d.data("bs.button", e = new c(this,f)),
            "toggle" == b ? e.toggle() : b && e.setState(b)
        })
    }
    var c = function(b, d) {
        this.$element = a(b),
        this.options = a.extend({}, c.DEFAULTS, d),
        this.isLoading = !1
    };
    c.VERSION = "3.3.7",
    c.DEFAULTS = {
        loadingText: "loading..."
    },
    c.prototype.setState = function(b) {
        var c = "disabled"
          , d = this.$element
          , e = d.is("input") ? "val" : "html"
          , f = d.data();
        b += "Text",
        null == f.resetText && d.data("resetText", d[e]()),
        setTimeout(a.proxy(function() {
            d[e](null == f[b] ? this.options[b] : f[b]),
            "loadingText" == b ? (this.isLoading = !0,
            d.addClass(c).attr(c, c).prop(c, !0)) : this.isLoading && (this.isLoading = !1,
            d.removeClass(c).removeAttr(c).prop(c, !1))
        }, this), 0)
    }
    ,
    c.prototype.toggle = function() {
        var a = !0
          , b = this.$element.closest('[data-toggle="buttons"]');
        if (b.length) {
            var c = this.$element.find("input");
            "radio" == c.prop("type") ? (c.prop("checked") && (a = !1),
            b.find(".active").removeClass("active"),
            this.$element.addClass("active")) : "checkbox" == c.prop("type") && (c.prop("checked") !== this.$element.hasClass("active") && (a = !1),
            this.$element.toggleClass("active")),
            c.prop("checked", this.$element.hasClass("active")),
            a && c.trigger("change")
        } else
            this.$element.attr("aria-pressed", !this.$element.hasClass("active")),
            this.$element.toggleClass("active")
    }
    ;
    var d = a.fn.button;
    a.fn.button = b,
    a.fn.button.Constructor = c,
    a.fn.button.noConflict = function() {
        return a.fn.button = d,
        this
    }
    ,
    a(document).on("click.bs.button.data-api", '[data-toggle^="button"]', function(c) {
        var d = a(c.target).closest(".btn");
        b.call(d, "toggle"),
        a(c.target).is('input[type="radio"], input[type="checkbox"]') || (c.preventDefault(),
        d.is("input,button") ? d.trigger("focus") : d.find("input:visible,button:visible").first().trigger("focus"))
    }).on("focus.bs.button.data-api blur.bs.button.data-api", '[data-toggle^="button"]', function(b) {
        a(b.target).closest(".btn").toggleClass("focus", /^focus(in)?$/.test(b.type))
    })
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.carousel")
              , f = a.extend({}, c.DEFAULTS, d.data(), "object" == typeof b && b)
              , g = "string" == typeof b ? b : f.slide;
            e || d.data("bs.carousel", e = new c(this,f)),
            "number" == typeof b ? e.to(b) : g ? e[g]() : f.interval && e.pause().cycle()
        })
    }
    var c = function(b, c) {
        this.$element = a(b),
        this.$indicators = this.$element.find(".carousel-indicators"),
        this.options = c,
        this.paused = null,
        this.sliding = null,
        this.interval = null,
        this.$active = null,
        this.$items = null,
        this.options.keyboard && this.$element.on("keydown.bs.carousel", a.proxy(this.keydown, this)),
        "hover" == this.options.pause && !("ontouchstart"in document.documentElement) && this.$element.on("mouseenter.bs.carousel", a.proxy(this.pause, this)).on("mouseleave.bs.carousel", a.proxy(this.cycle, this))
    };
    c.VERSION = "3.3.7",
    c.TRANSITION_DURATION = 600,
    c.DEFAULTS = {
        interval: 5e3,
        pause: "hover",
        wrap: !0,
        keyboard: !0
    },
    c.prototype.keydown = function(a) {
        if (!/input|textarea/i.test(a.target.tagName)) {
            switch (a.which) {
            case 37:
                this.prev();
                break;
            case 39:
                this.next();
                break;
            default:
                return
            }
            a.preventDefault()
        }
    }
    ,
    c.prototype.cycle = function(b) {
        return b || (this.paused = !1),
        this.interval && clearInterval(this.interval),
        this.options.interval && !this.paused && (this.interval = setInterval(a.proxy(this.next, this), this.options.interval)),
        this
    }
    ,
    c.prototype.getItemIndex = function(a) {
        return this.$items = a.parent().children(".item"),
        this.$items.index(a || this.$active)
    }
    ,
    c.prototype.getItemForDirection = function(a, b) {
        var c = this.getItemIndex(b)
          , d = "prev" == a && 0 === c || "next" == a && c == this.$items.length - 1;
        if (d && !this.options.wrap)
            return b;
        var e = "prev" == a ? -1 : 1
          , f = (c + e) % this.$items.length;
        return this.$items.eq(f)
    }
    ,
    c.prototype.to = function(a) {
        var b = this
          , c = this.getItemIndex(this.$active = this.$element.find(".item.active"));
        if (!(a > this.$items.length - 1 || a < 0))
            return this.sliding ? this.$element.one("slid.bs.carousel", function() {
                b.to(a)
            }) : c == a ? this.pause().cycle() : this.slide(a > c ? "next" : "prev", this.$items.eq(a))
    }
    ,
    c.prototype.pause = function(b) {
        return b || (this.paused = !0),
        this.$element.find(".next, .prev").length && a.support.transition && (this.$element.trigger(a.support.transition.end),
        this.cycle(!0)),
        this.interval = clearInterval(this.interval),
        this
    }
    ,
    c.prototype.next = function() {
        if (!this.sliding)
            return this.slide("next")
    }
    ,
    c.prototype.prev = function() {
        if (!this.sliding)
            return this.slide("prev")
    }
    ,
    c.prototype.slide = function(b, d) {
        var e = this.$element.find(".item.active")
          , f = d || this.getItemForDirection(b, e)
          , g = this.interval
          , h = "next" == b ? "left" : "right"
          , i = this;
        if (f.hasClass("active"))
            return this.sliding = !1;
        var j = f[0]
          , k = a.Event("slide.bs.carousel", {
            relatedTarget: j,
            direction: h
        });
        if (this.$element.trigger(k),
        !k.isDefaultPrevented()) {
            if (this.sliding = !0,
            g && this.pause(),
            this.$indicators.length) {
                this.$indicators.find(".active").removeClass("active");
                var l = a(this.$indicators.children()[this.getItemIndex(f)]);
                l && l.addClass("active")
            }
            var m = a.Event("slid.bs.carousel", {
                relatedTarget: j,
                direction: h
            });
            return a.support.transition && this.$element.hasClass("slide") ? (f.addClass(b),
            f[0].offsetWidth,
            e.addClass(h),
            f.addClass(h),
            e.one("bsTransitionEnd", function() {
                f.removeClass([b, h].join(" ")).addClass("active"),
                e.removeClass(["active", h].join(" ")),
                i.sliding = !1,
                setTimeout(function() {
                    i.$element.trigger(m)
                }, 0)
            }).emulateTransitionEnd(c.TRANSITION_DURATION)) : (e.removeClass("active"),
            f.addClass("active"),
            this.sliding = !1,
            this.$element.trigger(m)),
            g && this.cycle(),
            this
        }
    }
    ;
    var d = a.fn.carousel;
    a.fn.carousel = b,
    a.fn.carousel.Constructor = c,
    a.fn.carousel.noConflict = function() {
        return a.fn.carousel = d,
        this
    }
    ;
    var e = function(c) {
        var d, e = a(this), f = a(e.attr("data-target") || (d = e.attr("href")) && d.replace(/.*(?=#[^\s]+$)/, ""));
        if (f.hasClass("carousel")) {
            var g = a.extend({}, f.data(), e.data())
              , h = e.attr("data-slide-to");
            h && (g.interval = !1),
            b.call(f, g),
            h && f.data("bs.carousel").to(h),
            c.preventDefault()
        }
    };
    a(document).on("click.bs.carousel.data-api", "[data-slide]", e).on("click.bs.carousel.data-api", "[data-slide-to]", e),
    a(window).on("load", function() {
        a('[data-ride="carousel"]').each(function() {
            var c = a(this);
            b.call(c, c.data())
        })
    })
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        var c, d = b.attr("data-target") || (c = b.attr("href")) && c.replace(/.*(?=#[^\s]+$)/, "");
        return a(d)
    }
    function c(b) {
        return this.each(function() {
            var c = a(this)
              , e = c.data("bs.collapse")
              , f = a.extend({}, d.DEFAULTS, c.data(), "object" == typeof b && b);
            !e && f.toggle && /show|hide/.test(b) && (f.toggle = !1),
            e || c.data("bs.collapse", e = new d(this,f)),
            "string" == typeof b && e[b]()
        })
    }
    var d = function(b, c) {
        this.$element = a(b),
        this.options = a.extend({}, d.DEFAULTS, c),
        this.$trigger = a('[data-toggle="collapse"][href="#' + b.id + '"],[data-toggle="collapse"][data-target="#' + b.id + '"]'),
        this.transitioning = null,
        this.options.parent ? this.$parent = this.getParent() : this.addAriaAndCollapsedClass(this.$element, this.$trigger),
        this.options.toggle && this.toggle()
    };
    d.VERSION = "3.3.7",
    d.TRANSITION_DURATION = 350,
    d.DEFAULTS = {
        toggle: !0
    },
    d.prototype.dimension = function() {
        var a = this.$element.hasClass("width");
        return a ? "width" : "height"
    }
    ,
    d.prototype.show = function() {
        if (!this.transitioning && !this.$element.hasClass("in")) {
            var b, e = this.$parent && this.$parent.children(".panel").children(".in, .collapsing");
            if (!(e && e.length && (b = e.data("bs.collapse"),
            b && b.transitioning))) {
                var f = a.Event("show.bs.collapse");
                if (this.$element.trigger(f),
                !f.isDefaultPrevented()) {
                    e && e.length && (c.call(e, "hide"),
                    b || e.data("bs.collapse", null));
                    var g = this.dimension();
                    this.$element.removeClass("collapse").addClass("collapsing")[g](0).attr("aria-expanded", !0),
                    this.$trigger.removeClass("collapsed").attr("aria-expanded", !0),
                    this.transitioning = 1;
                    var h = function() {
                        this.$element.removeClass("collapsing").addClass("collapse in")[g](""),
                        this.transitioning = 0,
                        this.$element.trigger("shown.bs.collapse")
                    };
                    if (!a.support.transition)
                        return h.call(this);
                    var i = a.camelCase(["scroll", g].join("-"));
                    this.$element.one("bsTransitionEnd", a.proxy(h, this)).emulateTransitionEnd(d.TRANSITION_DURATION)[g](this.$element[0][i])
                }
            }
        }
    }
    ,
    d.prototype.hide = function() {
        if (!this.transitioning && this.$element.hasClass("in")) {
            var b = a.Event("hide.bs.collapse");
            if (this.$element.trigger(b),
            !b.isDefaultPrevented()) {
                var c = this.dimension();
                this.$element[c](this.$element[c]())[0].offsetHeight,
                this.$element.addClass("collapsing").removeClass("collapse in").attr("aria-expanded", !1),
                this.$trigger.addClass("collapsed").attr("aria-expanded", !1),
                this.transitioning = 1;
                var e = function() {
                    this.transitioning = 0,
                    this.$element.removeClass("collapsing").addClass("collapse").trigger("hidden.bs.collapse")
                };
                return a.support.transition ? void this.$element[c](0).one("bsTransitionEnd", a.proxy(e, this)).emulateTransitionEnd(d.TRANSITION_DURATION) : e.call(this)
            }
        }
    }
    ,
    d.prototype.toggle = function() {
        this[this.$element.hasClass("in") ? "hide" : "show"]()
    }
    ,
    d.prototype.getParent = function() {
        return a(this.options.parent).find('[data-toggle="collapse"][data-parent="' + this.options.parent + '"]').each(a.proxy(function(c, d) {
            var e = a(d);
            this.addAriaAndCollapsedClass(b(e), e)
        }, this)).end()
    }
    ,
    d.prototype.addAriaAndCollapsedClass = function(a, b) {
        var c = a.hasClass("in");
        a.attr("aria-expanded", c),
        b.toggleClass("collapsed", !c).attr("aria-expanded", c)
    }
    ;
    var e = a.fn.collapse;
    a.fn.collapse = c,
    a.fn.collapse.Constructor = d,
    a.fn.collapse.noConflict = function() {
        return a.fn.collapse = e,
        this
    }
    ,
    a(document).on("click.bs.collapse.data-api", '[data-toggle="collapse"]', function(d) {
        var e = a(this);
        e.attr("data-target") || d.preventDefault();
        var f = b(e)
          , g = f.data("bs.collapse")
          , h = g ? "toggle" : e.data();
        c.call(f, h)
    })
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        var c = b.attr("data-target");
        c || (c = b.attr("href"),
        c = c && /#[A-Za-z]/.test(c) && c.replace(/.*(?=#[^\s]*$)/, ""));
        var d = c && a(c);
        return d && d.length ? d : b.parent()
    }
    function c(c) {
        c && 3 === c.which || (a(e).remove(),
        a(f).each(function() {
            var d = a(this)
              , e = b(d)
              , f = {
                relatedTarget: this
            };
            e.hasClass("open") && (c && "click" == c.type && /input|textarea/i.test(c.target.tagName) && a.contains(e[0], c.target) || (e.trigger(c = a.Event("hide.bs.dropdown", f)),
            c.isDefaultPrevented() || (d.attr("aria-expanded", "false"),
            e.removeClass("open").trigger(a.Event("hidden.bs.dropdown", f)))))
        }))
    }
    function d(b) {
        return this.each(function() {
            var c = a(this)
              , d = c.data("bs.dropdown");
            d || c.data("bs.dropdown", d = new g(this)),
            "string" == typeof b && d[b].call(c)
        })
    }
    var e = ".dropdown-backdrop"
      , f = '[data-toggle="dropdown"]'
      , g = function(b) {
        a(b).on("click.bs.dropdown", this.toggle)
    };
    g.VERSION = "3.3.7",
    g.prototype.toggle = function(d) {
        var e = a(this);
        if (!e.is(".disabled, :disabled")) {
            var f = b(e)
              , g = f.hasClass("open");
            if (c(),
            !g) {
                "ontouchstart"in document.documentElement && !f.closest(".navbar-nav").length && a(document.createElement("div")).addClass("dropdown-backdrop").insertAfter(a(this)).on("click", c);
                var h = {
                    relatedTarget: this
                };
                if (f.trigger(d = a.Event("show.bs.dropdown", h)),
                d.isDefaultPrevented())
                    return;
                e.trigger("focus").attr("aria-expanded", "true"),
                f.toggleClass("open").trigger(a.Event("shown.bs.dropdown", h))
            }
            return !1
        }
    }
    ,
    g.prototype.keydown = function(c) {
        if (/(38|40|27|32)/.test(c.which) && !/input|textarea/i.test(c.target.tagName)) {
            var d = a(this);
            if (c.preventDefault(),
            c.stopPropagation(),
            !d.is(".disabled, :disabled")) {
                var e = b(d)
                  , g = e.hasClass("open");
                if (!g && 27 != c.which || g && 27 == c.which)
                    return 27 == c.which && e.find(f).trigger("focus"),
                    d.trigger("click");
                var h = " li:not(.disabled):visible a"
                  , i = e.find(".dropdown-menu" + h);
                if (i.length) {
                    var j = i.index(c.target);
                    38 == c.which && j > 0 && j--,
                    40 == c.which && j < i.length - 1 && j++,
                    ~j || (j = 0),
                    i.eq(j).trigger("focus")
                }
            }
        }
    }
    ;
    var h = a.fn.dropdown;
    a.fn.dropdown = d,
    a.fn.dropdown.Constructor = g,
    a.fn.dropdown.noConflict = function() {
        return a.fn.dropdown = h,
        this
    }
    ,
    a(document).on("click.bs.dropdown.data-api", c).on("click.bs.dropdown.data-api", ".dropdown form", function(a) {
        a.stopPropagation()
    }).on("click.bs.dropdown.data-api", f, g.prototype.toggle).on("keydown.bs.dropdown.data-api", f, g.prototype.keydown).on("keydown.bs.dropdown.data-api", ".dropdown-menu", g.prototype.keydown)
}(jQuery),
+function(a) {
    "use strict";
    function b(b, d) {
        return this.each(function() {
            var e = a(this)
              , f = e.data("bs.modal")
              , g = a.extend({}, c.DEFAULTS, e.data(), "object" == typeof b && b);
            f || e.data("bs.modal", f = new c(this,g)),
            "string" == typeof b ? f[b](d) : g.show && f.show(d)
        })
    }
    var c = function(b, c) {
        this.options = c,
        this.$body = a(document.body),
        this.$element = a(b),
        this.$dialog = this.$element.find(".modal-dialog"),
        this.$backdrop = null,
        this.isShown = null,
        this.originalBodyPad = null,
        this.scrollbarWidth = 0,
        this.ignoreBackdropClick = !1,
        this.options.remote && this.$element.find(".modal-content").load(this.options.remote, a.proxy(function() {
            this.$element.trigger("loaded.bs.modal")
        }, this))
    };
    c.VERSION = "3.3.7",
    c.TRANSITION_DURATION = 300,
    c.BACKDROP_TRANSITION_DURATION = 150,
    c.DEFAULTS = {
        backdrop: !0,
        keyboard: !0,
        show: !0
    },
    c.prototype.toggle = function(a) {
        return this.isShown ? this.hide() : this.show(a)
    }
    ,
    c.prototype.show = function(b) {
        var d = this
          , e = a.Event("show.bs.modal", {
            relatedTarget: b
        });
        this.$element.trigger(e),
        this.isShown || e.isDefaultPrevented() || (this.isShown = !0,
        this.checkScrollbar(),
        this.setScrollbar(),
        this.$body.addClass("modal-open"),
        this.escape(),
        this.resize(),
        this.$element.on("click.dismiss.bs.modal", '[data-dismiss="modal"]', a.proxy(this.hide, this)),
        this.$dialog.on("mousedown.dismiss.bs.modal", function() {
            d.$element.one("mouseup.dismiss.bs.modal", function(b) {
                a(b.target).is(d.$element) && (d.ignoreBackdropClick = !0)
            })
        }),
        this.backdrop(function() {
            var e = a.support.transition && d.$element.hasClass("fade");
            d.$element.parent().length || d.$element.appendTo(d.$body),
            d.$element.show().scrollTop(0),
            d.adjustDialog(),
            e && d.$element[0].offsetWidth,
            d.$element.addClass("in"),
            d.enforceFocus();
            var f = a.Event("shown.bs.modal", {
                relatedTarget: b
            });
            e ? d.$dialog.one("bsTransitionEnd", function() {
                d.$element.trigger("focus").trigger(f)
            }).emulateTransitionEnd(c.TRANSITION_DURATION) : d.$element.trigger("focus").trigger(f)
        }))
    }
    ,
    c.prototype.hide = function(b) {
        b && b.preventDefault(),
        b = a.Event("hide.bs.modal"),
        this.$element.trigger(b),
        this.isShown && !b.isDefaultPrevented() && (this.isShown = !1,
        this.escape(),
        this.resize(),
        a(document).off("focusin.bs.modal"),
        this.$element.removeClass("in").off("click.dismiss.bs.modal").off("mouseup.dismiss.bs.modal"),
        this.$dialog.off("mousedown.dismiss.bs.modal"),
        a.support.transition && this.$element.hasClass("fade") ? this.$element.one("bsTransitionEnd", a.proxy(this.hideModal, this)).emulateTransitionEnd(c.TRANSITION_DURATION) : this.hideModal())
    }
    ,
    c.prototype.enforceFocus = function() {
        a(document).off("focusin.bs.modal").on("focusin.bs.modal", a.proxy(function(a) {
            document === a.target || this.$element[0] === a.target || this.$element.has(a.target).length || this.$element.trigger("focus")
        }, this))
    }
    ,
    c.prototype.escape = function() {
        this.isShown && this.options.keyboard ? this.$element.on("keydown.dismiss.bs.modal", a.proxy(function(a) {
            27 == a.which && this.hide()
        }, this)) : this.isShown || this.$element.off("keydown.dismiss.bs.modal")
    }
    ,
    c.prototype.resize = function() {
        this.isShown ? a(window).on("resize.bs.modal", a.proxy(this.handleUpdate, this)) : a(window).off("resize.bs.modal")
    }
    ,
    c.prototype.hideModal = function() {
        var a = this;
        this.$element.hide(),
        this.backdrop(function() {
            a.$body.removeClass("modal-open"),
            a.resetAdjustments(),
            a.resetScrollbar(),
            a.$element.trigger("hidden.bs.modal")
        })
    }
    ,
    c.prototype.removeBackdrop = function() {
        this.$backdrop && this.$backdrop.remove(),
        this.$backdrop = null
    }
    ,
    c.prototype.backdrop = function(b) {
        var d = this
          , e = this.$element.hasClass("fade") ? "fade" : "";
        if (this.isShown && this.options.backdrop) {
            var f = a.support.transition && e;
            if (this.$backdrop = a(document.createElement("div")).addClass("modal-backdrop " + e).appendTo(this.$body),
            this.$element.on("click.dismiss.bs.modal", a.proxy(function(a) {
                return this.ignoreBackdropClick ? void (this.ignoreBackdropClick = !1) : void (a.target === a.currentTarget && ("static" == this.options.backdrop ? this.$element[0].focus() : this.hide()))
            }, this)),
            f && this.$backdrop[0].offsetWidth,
            this.$backdrop.addClass("in"),
            !b)
                return;
            f ? this.$backdrop.one("bsTransitionEnd", b).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : b()
        } else if (!this.isShown && this.$backdrop) {
            this.$backdrop.removeClass("in");
            var g = function() {
                d.removeBackdrop(),
                b && b()
            };
            a.support.transition && this.$element.hasClass("fade") ? this.$backdrop.one("bsTransitionEnd", g).emulateTransitionEnd(c.BACKDROP_TRANSITION_DURATION) : g()
        } else
            b && b()
    }
    ,
    c.prototype.handleUpdate = function() {
        this.adjustDialog()
    }
    ,
    c.prototype.adjustDialog = function() {
        var a = this.$element[0].scrollHeight > document.documentElement.clientHeight;
        this.$element.css({
            paddingLeft: !this.bodyIsOverflowing && a ? this.scrollbarWidth : "",
            paddingRight: this.bodyIsOverflowing && !a ? this.scrollbarWidth : ""
        })
    }
    ,
    c.prototype.resetAdjustments = function() {
        this.$element.css({
            paddingLeft: "",
            paddingRight: ""
        })
    }
    ,
    c.prototype.checkScrollbar = function() {
        var a = window.innerWidth;
        if (!a) {
            var b = document.documentElement.getBoundingClientRect();
            a = b.right - Math.abs(b.left)
        }
        this.bodyIsOverflowing = document.body.clientWidth < a,
        this.scrollbarWidth = this.measureScrollbar()
    }
    ,
    c.prototype.setScrollbar = function() {
        var a = parseInt(this.$body.css("padding-right") || 0, 10);
        this.originalBodyPad = document.body.style.paddingRight || "",
        this.bodyIsOverflowing && this.$body.css("padding-right", a + this.scrollbarWidth)
    }
    ,
    c.prototype.resetScrollbar = function() {
        this.$body.css("padding-right", this.originalBodyPad)
    }
    ,
    c.prototype.measureScrollbar = function() {
        var a = document.createElement("div");
        a.className = "modal-scrollbar-measure",
        this.$body.append(a);
        var b = a.offsetWidth - a.clientWidth;
        return this.$body[0].removeChild(a),
        b
    }
    ;
    var d = a.fn.modal;
    a.fn.modal = b,
    a.fn.modal.Constructor = c,
    a.fn.modal.noConflict = function() {
        return a.fn.modal = d,
        this
    }
    ,
    a(document).on("click.bs.modal.data-api", '[data-toggle="modal"]', function(c) {
        var d = a(this)
          , e = d.attr("href")
          , f = a(d.attr("data-target") || e && e.replace(/.*(?=#[^\s]+$)/, ""))
          , g = f.data("bs.modal") ? "toggle" : a.extend({
            remote: !/#/.test(e) && e
        }, f.data(), d.data());
        d.is("a") && c.preventDefault(),
        f.one("show.bs.modal", function(a) {
            a.isDefaultPrevented() || f.one("hidden.bs.modal", function() {
                d.is(":visible") && d.trigger("focus")
            })
        }),
        b.call(f, g, this)
    })
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.tooltip")
              , f = "object" == typeof b && b;
            !e && /destroy|hide/.test(b) || (e || d.data("bs.tooltip", e = new c(this,f)),
            "string" == typeof b && e[b]())
        })
    }
    var c = function(a, b) {
        this.type = null,
        this.options = null,
        this.enabled = null,
        this.timeout = null,
        this.hoverState = null,
        this.$element = null,
        this.inState = null,
        this.init("tooltip", a, b)
    };
    c.VERSION = "3.3.7",
    c.TRANSITION_DURATION = 150,
    c.DEFAULTS = {
        animation: !0,
        placement: "top",
        selector: !1,
        template: '<div class="tooltip" role="tooltip"><div class="tooltip-arrow"></div><div class="tooltip-inner"></div></div>',
        trigger: "hover focus",
        title: "",
        delay: 0,
        html: !1,
        container: !1,
        viewport: {
            selector: "body",
            padding: 0
        }
    },
    c.prototype.init = function(b, c, d) {
        if (this.enabled = !0,
        this.type = b,
        this.$element = a(c),
        this.options = this.getOptions(d),
        this.$viewport = this.options.viewport && a(a.isFunction(this.options.viewport) ? this.options.viewport.call(this, this.$element) : this.options.viewport.selector || this.options.viewport),
        this.inState = {
            click: !1,
            hover: !1,
            focus: !1
        },
        this.$element[0]instanceof document.constructor && !this.options.selector)
            throw new Error("`selector` option must be specified when initializing " + this.type + " on the window.document object!");
        for (var e = this.options.trigger.split(" "), f = e.length; f--; ) {
            var g = e[f];
            if ("click" == g)
                this.$element.on("click." + this.type, this.options.selector, a.proxy(this.toggle, this));
            else if ("manual" != g) {
                var h = "hover" == g ? "mouseenter" : "focusin"
                  , i = "hover" == g ? "mouseleave" : "focusout";
                this.$element.on(h + "." + this.type, this.options.selector, a.proxy(this.enter, this)),
                this.$element.on(i + "." + this.type, this.options.selector, a.proxy(this.leave, this))
            }
        }
        this.options.selector ? this._options = a.extend({}, this.options, {
            trigger: "manual",
            selector: ""
        }) : this.fixTitle()
    }
    ,
    c.prototype.getDefaults = function() {
        return c.DEFAULTS
    }
    ,
    c.prototype.getOptions = function(b) {
        return b = a.extend({}, this.getDefaults(), this.$element.data(), b),
        b.delay && "number" == typeof b.delay && (b.delay = {
            show: b.delay,
            hide: b.delay
        }),
        b
    }
    ,
    c.prototype.getDelegateOptions = function() {
        var b = {}
          , c = this.getDefaults();
        return this._options && a.each(this._options, function(a, d) {
            c[a] != d && (b[a] = d)
        }),
        b
    }
    ,
    c.prototype.enter = function(b) {
        var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);
        return c || (c = new this.constructor(b.currentTarget,this.getDelegateOptions()),
        a(b.currentTarget).data("bs." + this.type, c)),
        b instanceof a.Event && (c.inState["focusin" == b.type ? "focus" : "hover"] = !0),
        c.tip().hasClass("in") || "in" == c.hoverState ? void (c.hoverState = "in") : (clearTimeout(c.timeout),
        c.hoverState = "in",
        c.options.delay && c.options.delay.show ? void (c.timeout = setTimeout(function() {
            "in" == c.hoverState && c.show()
        }, c.options.delay.show)) : c.show())
    }
    ,
    c.prototype.isInStateTrue = function() {
        for (var a in this.inState)
            if (this.inState[a])
                return !0;
        return !1
    }
    ,
    c.prototype.leave = function(b) {
        var c = b instanceof this.constructor ? b : a(b.currentTarget).data("bs." + this.type);
        if (c || (c = new this.constructor(b.currentTarget,this.getDelegateOptions()),
        a(b.currentTarget).data("bs." + this.type, c)),
        b instanceof a.Event && (c.inState["focusout" == b.type ? "focus" : "hover"] = !1),
        !c.isInStateTrue())
            return clearTimeout(c.timeout),
            c.hoverState = "out",
            c.options.delay && c.options.delay.hide ? void (c.timeout = setTimeout(function() {
                "out" == c.hoverState && c.hide()
            }, c.options.delay.hide)) : c.hide()
    }
    ,
    c.prototype.show = function() {
        var b = a.Event("show.bs." + this.type);
        if (this.hasContent() && this.enabled) {
            this.$element.trigger(b);
            var d = a.contains(this.$element[0].ownerDocument.documentElement, this.$element[0]);
            if (b.isDefaultPrevented() || !d)
                return;
            var e = this
              , f = this.tip()
              , g = this.getUID(this.type);
            this.setContent(),
            f.attr("id", g),
            this.$element.attr("aria-describedby", g),
            this.options.animation && f.addClass("fade");
            var h = "function" == typeof this.options.placement ? this.options.placement.call(this, f[0], this.$element[0]) : this.options.placement
              , i = /\s?auto?\s?/i
              , j = i.test(h);
            j && (h = h.replace(i, "") || "top"),
            f.detach().css({
                top: 0,
                left: 0,
                display: "block"
            }).addClass(h).data("bs." + this.type, this),
            this.options.container ? f.appendTo(this.options.container) : f.insertAfter(this.$element),
            this.$element.trigger("inserted.bs." + this.type);
            var k = this.getPosition()
              , l = f[0].offsetWidth
              , m = f[0].offsetHeight;
            if (j) {
                var n = h
                  , o = this.getPosition(this.$viewport);
                h = "bottom" == h && k.bottom + m > o.bottom ? "top" : "top" == h && k.top - m < o.top ? "bottom" : "right" == h && k.right + l > o.width ? "left" : "left" == h && k.left - l < o.left ? "right" : h,
                f.removeClass(n).addClass(h)
            }
            var p = this.getCalculatedOffset(h, k, l, m);
            this.applyPlacement(p, h);
            var q = function() {
                var a = e.hoverState;
                e.$element.trigger("shown.bs." + e.type),
                e.hoverState = null,
                "out" == a && e.leave(e)
            };
            a.support.transition && this.$tip.hasClass("fade") ? f.one("bsTransitionEnd", q).emulateTransitionEnd(c.TRANSITION_DURATION) : q()
        }
    }
    ,
    c.prototype.applyPlacement = function(b, c) {
        var d = this.tip()
          , e = d[0].offsetWidth
          , f = d[0].offsetHeight
          , g = parseInt(d.css("margin-top"), 10)
          , h = parseInt(d.css("margin-left"), 10);
        isNaN(g) && (g = 0),
        isNaN(h) && (h = 0),
        b.top += g,
        b.left += h,
        a.offset.setOffset(d[0], a.extend({
            using: function(a) {
                d.css({
                    top: Math.round(a.top),
                    left: Math.round(a.left)
                })
            }
        }, b), 0),
        d.addClass("in");
        var i = d[0].offsetWidth
          , j = d[0].offsetHeight;
        "top" == c && j != f && (b.top = b.top + f - j);
        var k = this.getViewportAdjustedDelta(c, b, i, j);
        k.left ? b.left += k.left : b.top += k.top;
        var l = /top|bottom/.test(c)
          , m = l ? 2 * k.left - e + i : 2 * k.top - f + j
          , n = l ? "offsetWidth" : "offsetHeight";
        d.offset(b),
        this.replaceArrow(m, d[0][n], l)
    }
    ,
    c.prototype.replaceArrow = function(a, b, c) {
        this.arrow().css(c ? "left" : "top", 50 * (1 - a / b) + "%").css(c ? "top" : "left", "")
    }
    ,
    c.prototype.setContent = function() {
        var a = this.tip()
          , b = this.getTitle();
        a.find(".tooltip-inner")[this.options.html ? "html" : "text"](b),
        a.removeClass("fade in top bottom left right")
    }
    ,
    c.prototype.hide = function(b) {
        function d() {
            "in" != e.hoverState && f.detach(),
            e.$element && e.$element.removeAttr("aria-describedby").trigger("hidden.bs." + e.type),
            b && b()
        }
        var e = this
          , f = a(this.$tip)
          , g = a.Event("hide.bs." + this.type);
        if (this.$element.trigger(g),
        !g.isDefaultPrevented())
            return f.removeClass("in"),
            a.support.transition && f.hasClass("fade") ? f.one("bsTransitionEnd", d).emulateTransitionEnd(c.TRANSITION_DURATION) : d(),
            this.hoverState = null,
            this
    }
    ,
    c.prototype.fixTitle = function() {
        var a = this.$element;
        (a.attr("title") || "string" != typeof a.attr("data-original-title")) && a.attr("data-original-title", a.attr("title") || "").attr("title", "")
    }
    ,
    c.prototype.hasContent = function() {
        return this.getTitle()
    }
    ,
    c.prototype.getPosition = function(b) {
        b = b || this.$element;
        var c = b[0]
          , d = "BODY" == c.tagName
          , e = c.getBoundingClientRect();
        null == e.width && (e = a.extend({}, e, {
            width: e.right - e.left,
            height: e.bottom - e.top
        }));
        var f = window.SVGElement && c instanceof window.SVGElement
          , g = d ? {
            top: 0,
            left: 0
        } : f ? null : b.offset()
          , h = {
            scroll: d ? document.documentElement.scrollTop || document.body.scrollTop : b.scrollTop()
        }
          , i = d ? {
            width: a(window).width(),
            height: a(window).height()
        } : null;
        return a.extend({}, e, h, i, g)
    }
    ,
    c.prototype.getCalculatedOffset = function(a, b, c, d) {
        return "bottom" == a ? {
            top: b.top + b.height,
            left: b.left + b.width / 2 - c / 2
        } : "top" == a ? {
            top: b.top - d,
            left: b.left + b.width / 2 - c / 2
        } : "left" == a ? {
            top: b.top + b.height / 2 - d / 2,
            left: b.left - c
        } : {
            top: b.top + b.height / 2 - d / 2,
            left: b.left + b.width
        }
    }
    ,
    c.prototype.getViewportAdjustedDelta = function(a, b, c, d) {
        var e = {
            top: 0,
            left: 0
        };
        if (!this.$viewport)
            return e;
        var f = this.options.viewport && this.options.viewport.padding || 0
          , g = this.getPosition(this.$viewport);
        if (/right|left/.test(a)) {
            var h = b.top - f - g.scroll
              , i = b.top + f - g.scroll + d;
            h < g.top ? e.top = g.top - h : i > g.top + g.height && (e.top = g.top + g.height - i)
        } else {
            var j = b.left - f
              , k = b.left + f + c;
            j < g.left ? e.left = g.left - j : k > g.right && (e.left = g.left + g.width - k)
        }
        return e
    }
    ,
    c.prototype.getTitle = function() {
        var a, b = this.$element, c = this.options;
        return a = b.attr("data-original-title") || ("function" == typeof c.title ? c.title.call(b[0]) : c.title)
    }
    ,
    c.prototype.getUID = function(a) {
        do
            a += ~~(1e6 * Math.random());
        while (document.getElementById(a));
        return a
    }
    ,
    c.prototype.tip = function() {
        if (!this.$tip && (this.$tip = a(this.options.template),
        1 != this.$tip.length))
            throw new Error(this.type + " `template` option must consist of exactly 1 top-level element!");
        return this.$tip
    }
    ,
    c.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".tooltip-arrow")
    }
    ,
    c.prototype.enable = function() {
        this.enabled = !0
    }
    ,
    c.prototype.disable = function() {
        this.enabled = !1
    }
    ,
    c.prototype.toggleEnabled = function() {
        this.enabled = !this.enabled
    }
    ,
    c.prototype.toggle = function(b) {
        var c = this;
        b && (c = a(b.currentTarget).data("bs." + this.type),
        c || (c = new this.constructor(b.currentTarget,this.getDelegateOptions()),
        a(b.currentTarget).data("bs." + this.type, c))),
        b ? (c.inState.click = !c.inState.click,
        c.isInStateTrue() ? c.enter(c) : c.leave(c)) : c.tip().hasClass("in") ? c.leave(c) : c.enter(c)
    }
    ,
    c.prototype.destroy = function() {
        var a = this;
        clearTimeout(this.timeout),
        this.hide(function() {
            a.$element.off("." + a.type).removeData("bs." + a.type),
            a.$tip && a.$tip.detach(),
            a.$tip = null,
            a.$arrow = null,
            a.$viewport = null,
            a.$element = null
        })
    }
    ;
    var d = a.fn.tooltip;
    a.fn.tooltip = b,
    a.fn.tooltip.Constructor = c,
    a.fn.tooltip.noConflict = function() {
        return a.fn.tooltip = d,
        this
    }
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.popover")
              , f = "object" == typeof b && b;
            !e && /destroy|hide/.test(b) || (e || d.data("bs.popover", e = new c(this,f)),
            "string" == typeof b && e[b]())
        })
    }
    var c = function(a, b) {
        this.init("popover", a, b)
    };
    if (!a.fn.tooltip)
        throw new Error("Popover requires tooltip.js");
    c.VERSION = "3.3.7",
    c.DEFAULTS = a.extend({}, a.fn.tooltip.Constructor.DEFAULTS, {
        placement: "right",
        trigger: "click",
        content: "",
        template: '<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
    }),
    c.prototype = a.extend({}, a.fn.tooltip.Constructor.prototype),
    c.prototype.constructor = c,
    c.prototype.getDefaults = function() {
        return c.DEFAULTS
    }
    ,
    c.prototype.setContent = function() {
        var a = this.tip()
          , b = this.getTitle()
          , c = this.getContent();
        a.find(".popover-title")[this.options.html ? "html" : "text"](b),
        a.find(".popover-content").children().detach().end()[this.options.html ? "string" == typeof c ? "html" : "append" : "text"](c),
        a.removeClass("fade top bottom left right in"),
        a.find(".popover-title").html() || a.find(".popover-title").hide()
    }
    ,
    c.prototype.hasContent = function() {
        return this.getTitle() || this.getContent()
    }
    ,
    c.prototype.getContent = function() {
        var a = this.$element
          , b = this.options;
        return a.attr("data-content") || ("function" == typeof b.content ? b.content.call(a[0]) : b.content)
    }
    ,
    c.prototype.arrow = function() {
        return this.$arrow = this.$arrow || this.tip().find(".arrow")
    }
    ;
    var d = a.fn.popover;
    a.fn.popover = b,
    a.fn.popover.Constructor = c,
    a.fn.popover.noConflict = function() {
        return a.fn.popover = d,
        this
    }
}(jQuery),
+function(a) {
    "use strict";
    function b(c, d) {
        this.$body = a(document.body),
        this.$scrollElement = a(a(c).is(document.body) ? window : c),
        this.options = a.extend({}, b.DEFAULTS, d),
        this.selector = (this.options.target || "") + " .nav li > a",
        this.offsets = [],
        this.targets = [],
        this.activeTarget = null,
        this.scrollHeight = 0,
        this.$scrollElement.on("scroll.bs.scrollspy", a.proxy(this.process, this)),
        this.refresh(),
        this.process()
    }
    function c(c) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.scrollspy")
              , f = "object" == typeof c && c;
            e || d.data("bs.scrollspy", e = new b(this,f)),
            "string" == typeof c && e[c]()
        })
    }
    b.VERSION = "3.3.7",
    b.DEFAULTS = {
        offset: 10
    },
    b.prototype.getScrollHeight = function() {
        return this.$scrollElement[0].scrollHeight || Math.max(this.$body[0].scrollHeight, document.documentElement.scrollHeight)
    }
    ,
    b.prototype.refresh = function() {
        var b = this
          , c = "offset"
          , d = 0;
        this.offsets = [],
        this.targets = [],
        this.scrollHeight = this.getScrollHeight(),
        a.isWindow(this.$scrollElement[0]) || (c = "position",
        d = this.$scrollElement.scrollTop()),
        this.$body.find(this.selector).map(function() {
            var b = a(this)
              , e = b.data("target") || b.attr("href")
              , f = /^#./.test(e) && a(e);
            return f && f.length && f.is(":visible") && [[f[c]().top + d, e]] || null
        }).sort(function(a, b) {
            return a[0] - b[0]
        }).each(function() {
            b.offsets.push(this[0]),
            b.targets.push(this[1])
        })
    }
    ,
    b.prototype.process = function() {
        var a, b = this.$scrollElement.scrollTop() + this.options.offset, c = this.getScrollHeight(), d = this.options.offset + c - this.$scrollElement.height(), e = this.offsets, f = this.targets, g = this.activeTarget;
        if (this.scrollHeight != c && this.refresh(),
        b >= d)
            return g != (a = f[f.length - 1]) && this.activate(a);
        if (g && b < e[0])
            return this.activeTarget = null,
            this.clear();
        for (a = e.length; a--; )
            g != f[a] && b >= e[a] && (void 0 === e[a + 1] || b < e[a + 1]) && this.activate(f[a])
    }
    ,
    b.prototype.activate = function(b) {
        this.activeTarget = b,
        this.clear();
        var c = this.selector + '[data-target="' + b + '"],' + this.selector + '[href="' + b + '"]'
          , d = a(c).parents("li").addClass("active");
        d.parent(".dropdown-menu").length && (d = d.closest("li.dropdown").addClass("active")),
        d.trigger("activate.bs.scrollspy")
    }
    ,
    b.prototype.clear = function() {
        a(this.selector).parentsUntil(this.options.target, ".active").removeClass("active")
    }
    ;
    var d = a.fn.scrollspy;
    a.fn.scrollspy = c,
    a.fn.scrollspy.Constructor = b,
    a.fn.scrollspy.noConflict = function() {
        return a.fn.scrollspy = d,
        this
    }
    ,
    a(window).on("load.bs.scrollspy.data-api", function() {
        a('[data-spy="scroll"]').each(function() {
            var b = a(this);
            c.call(b, b.data())
        })
    })
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.tab");
            e || d.data("bs.tab", e = new c(this)),
            "string" == typeof b && e[b]()
        })
    }
    var c = function(b) {
        this.element = a(b)
    };
    c.VERSION = "3.3.7",
    c.TRANSITION_DURATION = 150,
    c.prototype.show = function() {
        var b = this.element
          , c = b.closest("ul:not(.dropdown-menu)")
          , d = b.data("target");
        if (d || (d = b.attr("href"),
        d = d && d.replace(/.*(?=#[^\s]*$)/, "")),
        !b.parent("li").hasClass("active")) {
            var e = c.find(".active:last a")
              , f = a.Event("hide.bs.tab", {
                relatedTarget: b[0]
            })
              , g = a.Event("show.bs.tab", {
                relatedTarget: e[0]
            });
            if (e.trigger(f),
            b.trigger(g),
            !g.isDefaultPrevented() && !f.isDefaultPrevented()) {
                var h = a(d);
                this.activate(b.closest("li"), c),
                this.activate(h, h.parent(), function() {
                    e.trigger({
                        type: "hidden.bs.tab",
                        relatedTarget: b[0]
                    }),
                    b.trigger({
                        type: "shown.bs.tab",
                        relatedTarget: e[0]
                    })
                })
            }
        }
    }
    ,
    c.prototype.activate = function(b, d, e) {
        function f() {
            g.removeClass("active").find("> .dropdown-menu > .active").removeClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !1),
            b.addClass("active").find('[data-toggle="tab"]').attr("aria-expanded", !0),
            h ? (b[0].offsetWidth,
            b.addClass("in")) : b.removeClass("fade"),
            b.parent(".dropdown-menu").length && b.closest("li.dropdown").addClass("active").end().find('[data-toggle="tab"]').attr("aria-expanded", !0),
            e && e()
        }
        var g = d.find("> .active")
          , h = e && a.support.transition && (g.length && g.hasClass("fade") || !!d.find("> .fade").length);
        g.length && h ? g.one("bsTransitionEnd", f).emulateTransitionEnd(c.TRANSITION_DURATION) : f(),
        g.removeClass("in")
    }
    ;
    var d = a.fn.tab;
    a.fn.tab = b,
    a.fn.tab.Constructor = c,
    a.fn.tab.noConflict = function() {
        return a.fn.tab = d,
        this
    }
    ;
    var e = function(c) {
        c.preventDefault(),
        b.call(a(this), "show")
    };
    a(document).on("click.bs.tab.data-api", '[data-toggle="tab"]', e).on("click.bs.tab.data-api", '[data-toggle="pill"]', e)
}(jQuery),
+function(a) {
    "use strict";
    function b(b) {
        return this.each(function() {
            var d = a(this)
              , e = d.data("bs.affix")
              , f = "object" == typeof b && b;
            e || d.data("bs.affix", e = new c(this,f)),
            "string" == typeof b && e[b]()
        })
    }
    var c = function(b, d) {
        this.options = a.extend({}, c.DEFAULTS, d),
        this.$target = a(this.options.target).on("scroll.bs.affix.data-api", a.proxy(this.checkPosition, this)).on("click.bs.affix.data-api", a.proxy(this.checkPositionWithEventLoop, this)),
        this.$element = a(b),
        this.affixed = null,
        this.unpin = null,
        this.pinnedOffset = null,
        this.checkPosition()
    };
    c.VERSION = "3.3.7",
    c.RESET = "affix affix-top affix-bottom",
    c.DEFAULTS = {
        offset: 0,
        target: window
    },
    c.prototype.getState = function(a, b, c, d) {
        var e = this.$target.scrollTop()
          , f = this.$element.offset()
          , g = this.$target.height();
        if (null != c && "top" == this.affixed)
            return e < c && "top";
        if ("bottom" == this.affixed)
            return null != c ? !(e + this.unpin <= f.top) && "bottom" : !(e + g <= a - d) && "bottom";
        var h = null == this.affixed
          , i = h ? e : f.top
          , j = h ? g : b;
        return null != c && e <= c ? "top" : null != d && i + j >= a - d && "bottom"
    }
    ,
    c.prototype.getPinnedOffset = function() {
        if (this.pinnedOffset)
            return this.pinnedOffset;
        this.$element.removeClass(c.RESET).addClass("affix");
        var a = this.$target.scrollTop()
          , b = this.$element.offset();
        return this.pinnedOffset = b.top - a
    }
    ,
    c.prototype.checkPositionWithEventLoop = function() {
        setTimeout(a.proxy(this.checkPosition, this), 1)
    }
    ,
    c.prototype.checkPosition = function() {
        if (this.$element.is(":visible")) {
            var b = this.$element.height()
              , d = this.options.offset
              , e = d.top
              , f = d.bottom
              , g = Math.max(a(document).height(), a(document.body).height());
            "object" != typeof d && (f = e = d),
            "function" == typeof e && (e = d.top(this.$element)),
            "function" == typeof f && (f = d.bottom(this.$element));
            var h = this.getState(g, b, e, f);
            if (this.affixed != h) {
                null != this.unpin && this.$element.css("top", "");
                var i = "affix" + (h ? "-" + h : "")
                  , j = a.Event(i + ".bs.affix");
                if (this.$element.trigger(j),
                j.isDefaultPrevented())
                    return;
                this.affixed = h,
                this.unpin = "bottom" == h ? this.getPinnedOffset() : null,
                this.$element.removeClass(c.RESET).addClass(i).trigger(i.replace("affix", "affixed") + ".bs.affix")
            }
            "bottom" == h && this.$element.offset({
                top: g - b - f
            })
        }
    }
    ;
    var d = a.fn.affix;
    a.fn.affix = b,
    a.fn.affix.Constructor = c,
    a.fn.affix.noConflict = function() {
        return a.fn.affix = d,
        this
    }
    ,
    a(window).on("load", function() {
        a('[data-spy="affix"]').each(function() {
            var c = a(this)
              , d = c.data();
            d.offset = d.offset || {},
            null != d.offsetBottom && (d.offset.bottom = d.offsetBottom),
            null != d.offsetTop && (d.offset.top = d.offsetTop),
            b.call(c, d)
        })
    })
}(jQuery);
!function(s) {
    "use strict";
    function e(s) {
        return new RegExp("(^|\\s+)" + s + "(\\s+|$)")
    }
    function n(s, e) {
        (a(s, e) ? c : t)(s, e)
    }
    var a, t, c;
    "classList"in document.documentElement ? (a = function(s, e) {
        return s.classList.contains(e)
    }
    ,
    t = function(s, e) {
        s.classList.add(e)
    }
    ,
    c = function(s, e) {
        s.classList.remove(e)
    }
    ) : (a = function(s, n) {
        return e(n).test(s.className)
    }
    ,
    t = function(s, e) {
        a(s, e) || (s.className = s.className + " " + e)
    }
    ,
    c = function(s, n) {
        s.className = s.className.replace(e(n), " ")
    }
    );
    var i = {
        hasClass: a,
        addClass: t,
        removeClass: c,
        toggleClass: n,
        has: a,
        add: t,
        remove: c,
        toggle: n
    };
    "function" == typeof define && define.amd ? define(i) : s.classie = i
}(window);
!function(e) {
    e.fn.jflickrfeed = function(i, a) {
        var t = (i = e.extend(!0, {
            flickrbase: "http://api.flickr.com/services/feeds/",
            feedapi: "photos_public.gne",
            limit: 20,
            qstrings: {
                lang: "en-us",
                format: "json",
                jsoncallback: "?"
            },
            cleanDescription: !0,
            useTemplate: !0,
            itemTemplate: "",
            itemCallback: function() {}
        }, i)).flickrbase + i.feedapi + "?"
          , c = !0;
        for (var n in i.qstrings)
            c || (t += "&"),
            t += n + "=" + i.qstrings[n],
            c = !1;
        return e(this).each(function() {
            var c = e(this)
              , n = this;
            e.getJSON(t, function(t) {
                e.each(t.items, function(e, a) {
                    if (e < i.limit) {
                        if (i.cleanDescription) {
                            var t = /<p>(.*?)<\/p>/g
                              , m = a.description;
                            t.test(m) && (a.description = m.match(t)[2],
                            void 0 != a.description && (a.description = a.description.replace("<p>", "").replace("</p>", "")))
                        }
                        if (a.image_s = a.media.m.replace("_m", "_s"),
                        a.image_t = a.media.m.replace("_m", "_t"),
                        a.image_m = a.media.m.replace("_m", "_m"),
                        a.image = a.media.m.replace("_m", ""),
                        a.image_b = a.media.m.replace("_m", "_b"),
                        delete a.media,
                        i.useTemplate) {
                            var r = i.itemTemplate;
                            for (var l in a) {
                                var p = new RegExp("{{" + l + "}}","g");
                                r = r.replace(p, a[l])
                            }
                            c.append(r)
                        }
                        i.itemCallback.call(n, a)
                    }
                }),
                e.isFunction(a) && a.call(n, t)
            })
        })
    }
}(jQuery);
!function(t) {
    function e(t, e) {
        return t.toFixed(e.decimals)
    }
    t.fn.countTo = function(e) {
        return e = e || {},
        t(this).each(function() {
            function a() {
                s += l,
                c++,
                n(s),
                "function" == typeof o.onUpdate && o.onUpdate.call(f, s),
                c >= r && (i.removeData("countTo"),
                clearInterval(d.interval),
                s = o.to,
                "function" == typeof o.onComplete && o.onComplete.call(f, s))
            }
            function n(t) {
                var e = o.formatter.call(f, t, o);
                i.text(e)
            }
            var o = t.extend({}, t.fn.countTo.defaults, {
                from: t(this).data("from"),
                to: t(this).data("to"),
                speed: t(this).data("speed"),
                refreshInterval: t(this).data("refresh-interval"),
                decimals: t(this).data("decimals")
            }, e)
              , r = Math.ceil(o.speed / o.refreshInterval)
              , l = (o.to - o.from) / r
              , f = this
              , i = t(this)
              , c = 0
              , s = o.from
              , d = i.data("countTo") || {};
            i.data("countTo", d),
            d.interval && clearInterval(d.interval),
            d.interval = setInterval(a, o.refreshInterval),
            n(s)
        })
    }
    ,
    t.fn.countTo.defaults = {
        from: 0,
        to: 0,
        speed: 1e3,
        refreshInterval: 100,
        decimals: 0,
        formatter: e,
        onUpdate: null,
        onComplete: null
    }
}(jQuery);
!function(t) {
    function e() {
        var e, i, n = {
            height: a.innerHeight,
            width: a.innerWidth
        };
        return n.height || ((e = r.compatMode) || !t.support.boxModel) && (i = "CSS1Compat" === e ? f : r.body,
        n = {
            height: i.clientHeight,
            width: i.clientWidth
        }),
        n
    }
    function i() {
        return {
            top: a.pageYOffset || f.scrollTop || r.body.scrollTop,
            left: a.pageXOffset || f.scrollLeft || r.body.scrollLeft
        }
    }
    function n() {
        var n, l = t(), r = 0;
        if (t.each(d, function(t, e) {
            var i = e.data.selector
              , n = e.$element;
            l = l.add(i ? n.find(i) : n)
        }),
        n = l.length)
            for (o = o || e(),
            h = h || i(); n > r; r++)
                if (t.contains(f, l[r])) {
                    var a, c, p, s = t(l[r]), u = {
                        height: s.height(),
                        width: s.width()
                    }, g = s.offset(), v = s.data("inview");
                    if (!h || !o)
                        return;
                    g.top + u.height > h.top && g.top < h.top + o.height && g.left + u.width > h.left && g.left < h.left + o.width ? (a = h.left > g.left ? "right" : h.left + o.width < g.left + u.width ? "left" : "both",
                    c = h.top > g.top ? "bottom" : h.top + o.height < g.top + u.height ? "top" : "both",
                    p = a + "-" + c,
                    v && v === p || s.data("inview", p).trigger("inview", [!0, a, c])) : v && s.data("inview", !1).trigger("inview", [!1])
                }
    }
    var o, h, l, d = {}, r = document, a = window, f = r.documentElement, c = t.expando;
    t.event.special.inview = {
        add: function(e) {
            d[e.guid + "-" + this[c]] = {
                data: e,
                $element: t(this)
            },
            l || t.isEmptyObject(d) || (l = setInterval(n, 250))
        },
        remove: function(e) {
            try {
                delete d[e.guid + "-" + this[c]]
            } catch (t) {}
            t.isEmptyObject(d) && (clearInterval(l),
            l = null)
        }
    },
    t(a).bind("scroll resize scrollstop", function() {
        o = h = null
    }),
    !f.addEventListener && f.attachEvent && f.attachEvent("onfocusin", function() {
        h = null
    })
}(jQuery);
!function(e) {
    var t = {
        animation: "dissolve",
        separator: ",",
        speed: 2e3
    };
    e.fx.step.textShadowBlur = function(t) {
        e(t.elem).prop("textShadowBlur", t.now).css({
            textShadow: "0 0 " + Math.floor(t.now) + "px black"
        })
    }
    ;
    e.fn.textrotator = function(n) {
        var r = e.extend({}, t, n);
        return this.each(function() {
            var t = e(this);
            var n = [];
            e.each(t.text().split(r.separator), function(e, t) {
                n.push(t)
            });
            t.text(n[0]);
            var i = function() {
                switch (r.animation) {
                case "dissolve":
                    t.animate({
                        textShadowBlur: 20,
                        opacity: 0
                    }, 500, function() {
                        s = e.inArray(t.text(), n);
                        if (s + 1 == n.length)
                            s = -1;
                        t.text(n[s + 1]).animate({
                            textShadowBlur: 0,
                            opacity: 1
                        }, 500)
                    });
                    break;
                case "flip":
                    if (t.find(".back").length > 0) {
                        t.html(t.find(".back").html())
                    }
                    var i = t.text();
                    var s = e.inArray(i, n);
                    if (s + 1 == n.length)
                        s = -1;
                    t.html("");
                    e("<span class='front'>" + i + "</span>").appendTo(t);
                    e("<span class='back'>" + n[s + 1] + "</span>").appendTo(t);
                    t.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip").show().css({
                        "-webkit-transform": " rotateY(-180deg)",
                        "-moz-transform": " rotateY(-180deg)",
                        "-o-transform": " rotateY(-180deg)",
                        transform: " rotateY(-180deg)"
                    });
                    break;
                case "flipUp":
                    if (t.find(".back").length > 0) {
                        t.html(t.find(".back").html())
                    }
                    var i = t.text();
                    var s = e.inArray(i, n);
                    if (s + 1 == n.length)
                        s = -1;
                    t.html("");
                    e("<span class='front'>" + i + "</span>").appendTo(t);
                    e("<span class='back'>" + n[s + 1] + "</span>").appendTo(t);
                    t.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip up").show().css({
                        "-webkit-transform": " rotateX(-180deg)",
                        "-moz-transform": " rotateX(-180deg)",
                        "-o-transform": " rotateX(-180deg)",
                        transform: " rotateX(-180deg)"
                    });
                    break;
                case "flipCube":
                    if (t.find(".back").length > 0) {
                        t.html(t.find(".back").html())
                    }
                    var i = t.text();
                    var s = e.inArray(i, n);
                    if (s + 1 == n.length)
                        s = -1;
                    t.html("");
                    e("<span class='front'>" + i + "</span>").appendTo(t);
                    e("<span class='back'>" + n[s + 1] + "</span>").appendTo(t);
                    t.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip cube").show().css({
                        "-webkit-transform": " rotateY(180deg)",
                        "-moz-transform": " rotateY(180deg)",
                        "-o-transform": " rotateY(180deg)",
                        transform: " rotateY(180deg)"
                    });
                    break;
                case "flipCubeUp":
                    if (t.find(".back").length > 0) {
                        t.html(t.find(".back").html())
                    }
                    var i = t.text();
                    var s = e.inArray(i, n);
                    if (s + 1 == n.length)
                        s = -1;
                    t.html("");
                    e("<span class='front'>" + i + "</span>").appendTo(t);
                    e("<span class='back'>" + n[s + 1] + "</span>").appendTo(t);
                    t.wrapInner("<span class='rotating' />").find(".rotating").hide().addClass("flip cube up").show().css({
                        "-webkit-transform": " rotateX(180deg)",
                        "-moz-transform": " rotateX(180deg)",
                        "-o-transform": " rotateX(180deg)",
                        transform: " rotateX(180deg)"
                    });
                    break;
                case "spin":
                    if (t.find(".rotating").length > 0) {
                        t.html(t.find(".rotating").html())
                    }
                    s = e.inArray(t.text(), n);
                    if (s + 1 == n.length)
                        s = -1;
                    t.wrapInner("<span class='rotating spin' />").find(".rotating").hide().text(n[s + 1]).show().css({
                        "-webkit-transform": " rotate(0) scale(1)",
                        "-moz-transform": "rotate(0) scale(1)",
                        "-o-transform": "rotate(0) scale(1)",
                        transform: "rotate(0) scale(1)"
                    });
                    break;
                case "fade":
                    t.fadeOut(r.speed, function() {
                        s = e.inArray(t.text(), n);
                        if (s + 1 == n.length)
                            s = -1;
                        t.text(n[s + 1]).fadeIn(r.speed)
                    });
                    break
                }
            };
            setInterval(i, r.speed)
        })
    }
}(window.jQuery);
!function(t, e, i, s) {
    function o(e, i) {
        this.element = e,
        this.options = t.extend({}, r, i),
        this._defaults = r,
        this._name = n,
        this.init()
    }
    var n = "stellar"
      , r = {
        scrollProperty: "scroll",
        positionProperty: "position",
        horizontalScrolling: !0,
        verticalScrolling: !0,
        horizontalOffset: 0,
        verticalOffset: 0,
        responsive: !1,
        parallaxBackgrounds: !0,
        parallaxElements: !0,
        hideDistantElements: !0,
        hideElement: function(t) {
            t.hide()
        },
        showElement: function(t) {
            t.show()
        }
    }
      , a = {
        scroll: {
            getLeft: function(t) {
                return t.scrollLeft()
            },
            setLeft: function(t, e) {
                t.scrollLeft(e)
            },
            getTop: function(t) {
                return t.scrollTop()
            },
            setTop: function(t, e) {
                t.scrollTop(e)
            }
        },
        position: {
            getLeft: function(t) {
                return -1 * parseInt(t.css("left"), 10)
            },
            getTop: function(t) {
                return -1 * parseInt(t.css("top"), 10)
            }
        },
        margin: {
            getLeft: function(t) {
                return -1 * parseInt(t.css("margin-left"), 10)
            },
            getTop: function(t) {
                return -1 * parseInt(t.css("margin-top"), 10)
            }
        },
        transform: {
            getLeft: function(t) {
                var e = getComputedStyle(t[0])[c];
                return "none" !== e ? -1 * parseInt(e.match(/(-?[0-9]+)/g)[4], 10) : 0
            },
            getTop: function(t) {
                var e = getComputedStyle(t[0])[c];
                return "none" !== e ? -1 * parseInt(e.match(/(-?[0-9]+)/g)[5], 10) : 0
            }
        }
    }
      , l = {
        position: {
            setLeft: function(t, e) {
                t.css("left", e)
            },
            setTop: function(t, e) {
                t.css("top", e)
            }
        },
        transform: {
            setPosition: function(t, e, i, s, o) {
                t[0].style[c] = "translate3d(" + (e - i) + "px, " + (s - o) + "px, 0)"
            }
        }
    }
      , f = function() {
        var e, i = /^(Moz|Webkit|Khtml|O|ms|Icab)(?=[A-Z])/, s = t("script")[0].style, o = "";
        for (e in s)
            if (i.test(e)) {
                o = e.match(i)[0];
                break
            }
        return "WebkitOpacity"in s && (o = "Webkit"),
        "KhtmlOpacity"in s && (o = "Khtml"),
        function(t) {
            return o + (o.length > 0 ? t.charAt(0).toUpperCase() + t.slice(1) : t)
        }
    }()
      , c = f("transform")
      , h = t("<div />", {
        style: "background:#fff"
    }).css("background-position-x") !== s
      , p = h ? function(t, e, i) {
        t.css({
            "background-position-x": e,
            "background-position-y": i
        })
    }
    : function(t, e, i) {
        t.css("background-position", e + " " + i)
    }
      , d = h ? function(t) {
        return [t.css("background-position-x"), t.css("background-position-y")]
    }
    : function(t) {
        return t.css("background-position").split(" ")
    }
      , u = e.requestAnimationFrame || e.webkitRequestAnimationFrame || e.mozRequestAnimationFrame || e.oRequestAnimationFrame || e.msRequestAnimationFrame || function(t) {
        setTimeout(t, 1e3 / 60)
    }
    ;
    o.prototype = {
        init: function() {
            this.options.name = n + "_" + Math.floor(1e9 * Math.random()),
            this._defineElements(),
            this._defineGetters(),
            this._defineSetters(),
            this._handleWindowLoadAndResize(),
            this._detectViewport(),
            this.refresh({
                firstLoad: !0
            }),
            "scroll" === this.options.scrollProperty ? this._handleScrollEvent() : this._startAnimationLoop()
        },
        _defineElements: function() {
            this.element === i.body && (this.element = e),
            this.$scrollElement = t(this.element),
            this.$element = this.element === e ? t("body") : this.$scrollElement,
            this.$viewportElement = this.options.viewportElement !== s ? t(this.options.viewportElement) : this.$scrollElement[0] === e || "scroll" === this.options.scrollProperty ? this.$scrollElement : this.$scrollElement.parent()
        },
        _defineGetters: function() {
            var t = this
              , e = a[t.options.scrollProperty];
            this._getScrollLeft = function() {
                return e.getLeft(t.$scrollElement)
            }
            ,
            this._getScrollTop = function() {
                return e.getTop(t.$scrollElement)
            }
        },
        _defineSetters: function() {
            var e = this
              , i = a[e.options.scrollProperty]
              , s = l[e.options.positionProperty]
              , o = i.setLeft
              , n = i.setTop;
            this._setScrollLeft = "function" == typeof o ? function(t) {
                o(e.$scrollElement, t)
            }
            : t.noop,
            this._setScrollTop = "function" == typeof n ? function(t) {
                n(e.$scrollElement, t)
            }
            : t.noop,
            this._setPosition = s.setPosition || function(t, i, o, n, r) {
                e.options.horizontalScrolling && s.setLeft(t, i, o),
                e.options.verticalScrolling && s.setTop(t, n, r)
            }
        },
        _handleWindowLoadAndResize: function() {
            var i = this
              , s = t(e);
            i.options.responsive && s.bind("load." + this.name, function() {
                i.refresh()
            }),
            s.bind("resize." + this.name, function() {
                i._detectViewport(),
                i.options.responsive && i.refresh()
            })
        },
        refresh: function(i) {
            var s = this
              , o = s._getScrollLeft()
              , n = s._getScrollTop();
            i && i.firstLoad || this._reset(),
            this._setScrollLeft(0),
            this._setScrollTop(0),
            this._setOffsets(),
            this._findParticles(),
            this._findBackgrounds(),
            i && i.firstLoad && /WebKit/.test(navigator.userAgent) && t(e).load(function() {
                var t = s._getScrollLeft()
                  , e = s._getScrollTop();
                s._setScrollLeft(t + 1),
                s._setScrollTop(e + 1),
                s._setScrollLeft(t),
                s._setScrollTop(e)
            }),
            this._setScrollLeft(o),
            this._setScrollTop(n)
        },
        _detectViewport: function() {
            var t = this.$viewportElement.offset()
              , e = null !== t && t !== s;
            this.viewportWidth = this.$viewportElement.width(),
            this.viewportHeight = this.$viewportElement.height(),
            this.viewportOffsetTop = e ? t.top : 0,
            this.viewportOffsetLeft = e ? t.left : 0
        },
        _findParticles: function() {
            {
                var e = this;
                this._getScrollLeft(),
                this._getScrollTop()
            }
            if (this.particles !== s)
                for (var i = this.particles.length - 1; i >= 0; i--)
                    this.particles[i].$element.data("stellar-elementIsActive", s);
            this.particles = [],
            this.options.parallaxElements && this.$element.find("[data-stellar-ratio]").each(function() {
                var i, o, n, r, a, l, f, c, h, p = t(this), d = 0, u = 0, g = 0, m = 0;
                if (p.data("stellar-elementIsActive")) {
                    if (p.data("stellar-elementIsActive") !== this)
                        return
                } else
                    p.data("stellar-elementIsActive", this);
                e.options.showElement(p),
                p.data("stellar-startingLeft") ? (p.css("left", p.data("stellar-startingLeft")),
                p.css("top", p.data("stellar-startingTop"))) : (p.data("stellar-startingLeft", p.css("left")),
                p.data("stellar-startingTop", p.css("top"))),
                n = p.position().left,
                r = p.position().top,
                a = "auto" === p.css("margin-left") ? 0 : parseInt(p.css("margin-left"), 10),
                l = "auto" === p.css("margin-top") ? 0 : parseInt(p.css("margin-top"), 10),
                c = p.offset().left - a,
                h = p.offset().top - l,
                p.parents().each(function() {
                    var e = t(this);
                    return e.data("stellar-offset-parent") === !0 ? (d = g,
                    u = m,
                    f = e,
                    !1) : (g += e.position().left,
                    void (m += e.position().top))
                }),
                i = p.data("stellar-horizontal-offset") !== s ? p.data("stellar-horizontal-offset") : f !== s && f.data("stellar-horizontal-offset") !== s ? f.data("stellar-horizontal-offset") : e.horizontalOffset,
                o = p.data("stellar-vertical-offset") !== s ? p.data("stellar-vertical-offset") : f !== s && f.data("stellar-vertical-offset") !== s ? f.data("stellar-vertical-offset") : e.verticalOffset,
                e.particles.push({
                    $element: p,
                    $offsetParent: f,
                    isFixed: "fixed" === p.css("position"),
                    horizontalOffset: i,
                    verticalOffset: o,
                    startingPositionLeft: n,
                    startingPositionTop: r,
                    startingOffsetLeft: c,
                    startingOffsetTop: h,
                    parentOffsetLeft: d,
                    parentOffsetTop: u,
                    stellarRatio: p.data("stellar-ratio") !== s ? p.data("stellar-ratio") : 1,
                    width: p.outerWidth(!0),
                    height: p.outerHeight(!0),
                    isHidden: !1
                })
            })
        },
        _findBackgrounds: function() {
            var e, i = this, o = this._getScrollLeft(), n = this._getScrollTop();
            this.backgrounds = [],
            this.options.parallaxBackgrounds && (e = this.$element.find("[data-stellar-background-ratio]"),
            this.$element.data("stellar-background-ratio") && (e = e.add(this.$element)),
            e.each(function() {
                var e, r, a, l, f, c, h, u = t(this), g = d(u), m = 0, v = 0, L = 0, _ = 0;
                if (u.data("stellar-backgroundIsActive")) {
                    if (u.data("stellar-backgroundIsActive") !== this)
                        return
                } else
                    u.data("stellar-backgroundIsActive", this);
                u.data("stellar-backgroundStartingLeft") ? p(u, u.data("stellar-backgroundStartingLeft"), u.data("stellar-backgroundStartingTop")) : (u.data("stellar-backgroundStartingLeft", g[0]),
                u.data("stellar-backgroundStartingTop", g[1])),
                a = "auto" === u.css("margin-left") ? 0 : parseInt(u.css("margin-left"), 10),
                l = "auto" === u.css("margin-top") ? 0 : parseInt(u.css("margin-top"), 10),
                f = u.offset().left - a - o,
                c = u.offset().top - l - n,
                u.parents().each(function() {
                    var e = t(this);
                    return e.data("stellar-offset-parent") === !0 ? (m = L,
                    v = _,
                    h = e,
                    !1) : (L += e.position().left,
                    void (_ += e.position().top))
                }),
                e = u.data("stellar-horizontal-offset") !== s ? u.data("stellar-horizontal-offset") : h !== s && h.data("stellar-horizontal-offset") !== s ? h.data("stellar-horizontal-offset") : i.horizontalOffset,
                r = u.data("stellar-vertical-offset") !== s ? u.data("stellar-vertical-offset") : h !== s && h.data("stellar-vertical-offset") !== s ? h.data("stellar-vertical-offset") : i.verticalOffset,
                i.backgrounds.push({
                    $element: u,
                    $offsetParent: h,
                    isFixed: "fixed" === u.css("background-attachment"),
                    horizontalOffset: e,
                    verticalOffset: r,
                    startingValueLeft: g[0],
                    startingValueTop: g[1],
                    startingBackgroundPositionLeft: isNaN(parseInt(g[0], 10)) ? 0 : parseInt(g[0], 10),
                    startingBackgroundPositionTop: isNaN(parseInt(g[1], 10)) ? 0 : parseInt(g[1], 10),
                    startingPositionLeft: u.position().left,
                    startingPositionTop: u.position().top,
                    startingOffsetLeft: f,
                    startingOffsetTop: c,
                    parentOffsetLeft: m,
                    parentOffsetTop: v,
                    stellarRatio: u.data("stellar-background-ratio") === s ? 1 : u.data("stellar-background-ratio")
                })
            }))
        },
        _reset: function() {
            var t, e, i, s, o;
            for (o = this.particles.length - 1; o >= 0; o--)
                t = this.particles[o],
                e = t.$element.data("stellar-startingLeft"),
                i = t.$element.data("stellar-startingTop"),
                this._setPosition(t.$element, e, e, i, i),
                this.options.showElement(t.$element),
                t.$element.data("stellar-startingLeft", null).data("stellar-elementIsActive", null).data("stellar-backgroundIsActive", null);
            for (o = this.backgrounds.length - 1; o >= 0; o--)
                s = this.backgrounds[o],
                s.$element.data("stellar-backgroundStartingLeft", null).data("stellar-backgroundStartingTop", null),
                p(s.$element, s.startingValueLeft, s.startingValueTop)
        },
        destroy: function() {
            this._reset(),
            this.$scrollElement.unbind("resize." + this.name).unbind("scroll." + this.name),
            this._animationLoop = t.noop,
            t(e).unbind("load." + this.name).unbind("resize." + this.name)
        },
        _setOffsets: function() {
            var i = this
              , s = t(e);
            s.unbind("resize.horizontal-" + this.name).unbind("resize.vertical-" + this.name),
            "function" == typeof this.options.horizontalOffset ? (this.horizontalOffset = this.options.horizontalOffset(),
            s.bind("resize.horizontal-" + this.name, function() {
                i.horizontalOffset = i.options.horizontalOffset()
            })) : this.horizontalOffset = this.options.horizontalOffset,
            "function" == typeof this.options.verticalOffset ? (this.verticalOffset = this.options.verticalOffset(),
            s.bind("resize.vertical-" + this.name, function() {
                i.verticalOffset = i.options.verticalOffset()
            })) : this.verticalOffset = this.options.verticalOffset
        },
        _repositionElements: function() {
            var t, e, i, s, o, n, r, a, l, f, c = this._getScrollLeft(), h = this._getScrollTop(), d = !0, u = !0;
            if (this.currentScrollLeft !== c || this.currentScrollTop !== h || this.currentWidth !== this.viewportWidth || this.currentHeight !== this.viewportHeight) {
                for (this.currentScrollLeft = c,
                this.currentScrollTop = h,
                this.currentWidth = this.viewportWidth,
                this.currentHeight = this.viewportHeight,
                f = this.particles.length - 1; f >= 0; f--)
                    t = this.particles[f],
                    e = t.isFixed ? 1 : 0,
                    this.options.horizontalScrolling ? (n = (c + t.horizontalOffset + this.viewportOffsetLeft + t.startingPositionLeft - t.startingOffsetLeft + t.parentOffsetLeft) * -(t.stellarRatio + e - 1) + t.startingPositionLeft,
                    a = n - t.startingPositionLeft + t.startingOffsetLeft) : (n = t.startingPositionLeft,
                    a = t.startingOffsetLeft),
                    this.options.verticalScrolling ? (r = (h + t.verticalOffset + this.viewportOffsetTop + t.startingPositionTop - t.startingOffsetTop + t.parentOffsetTop) * -(t.stellarRatio + e - 1) + t.startingPositionTop,
                    l = r - t.startingPositionTop + t.startingOffsetTop) : (r = t.startingPositionTop,
                    l = t.startingOffsetTop),
                    this.options.hideDistantElements && (u = !this.options.horizontalScrolling || a + t.width > (t.isFixed ? 0 : c) && a < (t.isFixed ? 0 : c) + this.viewportWidth + this.viewportOffsetLeft,
                    d = !this.options.verticalScrolling || l + t.height > (t.isFixed ? 0 : h) && l < (t.isFixed ? 0 : h) + this.viewportHeight + this.viewportOffsetTop),
                    u && d ? (t.isHidden && (this.options.showElement(t.$element),
                    t.isHidden = !1),
                    this._setPosition(t.$element, n, t.startingPositionLeft, r, t.startingPositionTop)) : t.isHidden || (this.options.hideElement(t.$element),
                    t.isHidden = !0);
                for (f = this.backgrounds.length - 1; f >= 0; f--)
                    i = this.backgrounds[f],
                    e = i.isFixed ? 0 : 1,
                    s = this.options.horizontalScrolling ? (c + i.horizontalOffset - this.viewportOffsetLeft - i.startingOffsetLeft + i.parentOffsetLeft - i.startingBackgroundPositionLeft) * (e - i.stellarRatio) + "px" : i.startingValueLeft,
                    o = this.options.verticalScrolling ? (h + i.verticalOffset - this.viewportOffsetTop - i.startingOffsetTop + i.parentOffsetTop - i.startingBackgroundPositionTop) * (e - i.stellarRatio) + "px" : i.startingValueTop,
                    p(i.$element, s, o)
            }
        },
        _handleScrollEvent: function() {
            var t = this
              , e = !1
              , i = function() {
                t._repositionElements(),
                e = !1
            }
              , s = function() {
                e || (u(i),
                e = !0)
            };
            this.$scrollElement.bind("scroll." + this.name, s),
            s()
        },
        _startAnimationLoop: function() {
            var t = this;
            this._animationLoop = function() {
                u(t._animationLoop),
                t._repositionElements()
            }
            ,
            this._animationLoop()
        }
    },
    t.fn[n] = function(e) {
        var i = arguments;
        return e === s || "object" == typeof e ? this.each(function() {
            t.data(this, "plugin_" + n) || t.data(this, "plugin_" + n, new o(this,e))
        }) : "string" == typeof e && "_" !== e[0] && "init" !== e ? this.each(function() {
            var s = t.data(this, "plugin_" + n);
            s instanceof o && "function" == typeof s[e] && s[e].apply(s, Array.prototype.slice.call(i, 1)),
            "destroy" === e && t.data(this, "plugin_" + n, null)
        }) : void 0
    }
    ,
    t[n] = function() {
        var i = t(e);
        return i.stellar.apply(i, Array.prototype.slice.call(arguments, 0))
    }
    ,
    t[n].scrollProperty = a,
    t[n].positionProperty = l,
    e.Stellar = o
}(jQuery, this, document);
!function(t) {
    var e = {
        topSpacing: 0,
        bottomSpacing: 0,
        className: "is-sticky",
        wrapperClassName: "sticky-wrapper",
        center: !1,
        getWidthFrom: "",
        responsiveWidth: !1
    }
      , i = t(window)
      , n = t(document)
      , s = []
      , r = i.height()
      , o = function() {
        for (var e = i.scrollTop(), o = n.height(), a = o - r, c = e > a ? a - e : 0, p = 0; p < s.length; p++) {
            var l = s[p];
            if (e <= l.stickyWrapper.offset().top - l.topSpacing - c)
                null !== l.currentTop && (l.stickyElement.css("position", "").css("top", ""),
                l.stickyElement.trigger("sticky-end", [l]).parent().removeClass(l.className),
                l.currentTop = null);
            else {
                var d = o - l.stickyElement.outerHeight() - l.topSpacing - l.bottomSpacing - e - c;
                d < 0 ? d += l.topSpacing : d = l.topSpacing,
                l.currentTop != d && (l.stickyElement.css("position", "fixed").css("top", d),
                void 0 !== l.getWidthFrom && l.stickyElement.css("width", t(l.getWidthFrom).width()),
                l.stickyElement.trigger("sticky-start", [l]).parent().addClass(l.className),
                l.currentTop = d)
            }
        }
    }
      , a = function() {
        r = i.height();
        for (var e = 0; e < s.length; e++) {
            var n = s[e];
            void 0 !== n.getWidthFrom && !0 === n.responsiveWidth && n.stickyElement.css("width", t(n.getWidthFrom).width())
        }
    }
      , c = {
        init: function(i) {
            var n = t.extend({}, e, i);
            return this.each(function() {
                var e = t(this)
                  , i = e.attr("id")
                  , r = t("<div></div>").attr("id", i + "-sticky-wrapper").addClass(n.wrapperClassName);
                e.wrapAll(r),
                n.center && e.parent().css({
                    width: e.outerWidth(),
                    marginLeft: "auto",
                    marginRight: "auto"
                }),
                "right" == e.css("float") && e.css({
                    float: "none"
                }).parent().css({
                    float: "right"
                });
                var o = e.parent();
                o.css("height", e.outerHeight()),
                s.push({
                    topSpacing: n.topSpacing,
                    bottomSpacing: n.bottomSpacing,
                    stickyElement: e,
                    currentTop: null,
                    stickyWrapper: o,
                    className: n.className,
                    getWidthFrom: n.getWidthFrom,
                    responsiveWidth: n.responsiveWidth
                })
            })
        },
        update: o,
        unstick: function(e) {
            return this.each(function() {
                for (var e = t(this), i = -1, n = 0; n < s.length; n++)
                    s[n].stickyElement.get(0) == e.get(0) && (i = n);
                -1 != i && (s.splice(i, 1),
                e.unwrap(),
                e.removeAttr("style"))
            })
        }
    };
    window.addEventListener ? (window.addEventListener("scroll", o, !1),
    window.addEventListener("resize", a, !1)) : window.attachEvent && (window.attachEvent("onscroll", o),
    window.attachEvent("onresize", a)),
    t.fn.sticky = function(e) {
        return c[e] ? c[e].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof e && e ? void t.error("Method " + e + " does not exist on jQuery.sticky") : c.init.apply(this, arguments)
    }
    ,
    t.fn.unstick = function(e) {
        return c[e] ? c[e].apply(this, Array.prototype.slice.call(arguments, 1)) : "object" != typeof e && e ? void t.error("Method " + e + " does not exist on jQuery.sticky") : c.unstick.apply(this, arguments)
    }
    ,
    t(function() {
        setTimeout(o, 0)
    })
}(jQuery);
!function(i, t) {
    var n, e = "superslides";
    (n = function(n, e) {
        this.options = t.extend({
            play: !1,
            animation_speed: 600,
            animation_easing: "swing",
            animation: "slide",
            inherit_width_from: i,
            inherit_height_from: i,
            pagination: !0,
            hashchange: !1,
            scrollable: !0,
            elements: {
                preserve: ".preserve",
                nav: ".slides-navigation",
                container: ".slides-container",
                pagination: ".slides-pagination"
            }
        }, e);
        var s = this
          , o = t("<div>", {
            class: "slides-control"
        })
          , a = 1;
        this.$el = t(n),
        this.$container = this.$el.find(this.options.elements.container);
        var r = {
            containers: function() {
                s.init ? (s.$el.css({
                    height: s.height
                }),
                s.$control.css({
                    width: s.width * a,
                    left: -s.width
                }),
                s.$container.css({})) : (t("body").css({
                    margin: 0
                }),
                s.$el.css({
                    position: "relative",
                    overflow: "hidden",
                    width: "100%",
                    height: s.height
                }),
                s.$control.css({
                    position: "relative",
                    transform: "translate3d(0)",
                    height: "100%",
                    width: s.width * a,
                    left: -s.width
                }),
                s.$container.css({
                    display: "none",
                    margin: "0",
                    padding: "0",
                    listStyle: "none",
                    position: "relative",
                    height: "100%"
                })),
                1 === s.size() && s.$el.find(s.options.elements.nav).hide()
            },
            images: function() {
                var i = s.$container.find("img").not(s.options.elements.preserve);
                i.removeAttr("width").removeAttr("height").css({
                    "-webkit-backface-visibility": "hidden",
                    "-ms-interpolation-mode": "bicubic",
                    position: "absolute",
                    left: "0",
                    top: "0",
                    "z-index": "-1",
                    "max-width": "none"
                }),
                i.each(function() {
                    var i = s.image._aspectRatio(this)
                      , n = this;
                    if (t.data(this, "processed"))
                        s.image._scale(n, i),
                        s.image._center(n, i);
                    else {
                        var e = new Image;
                        e.onload = function() {
                            s.image._scale(n, i),
                            s.image._center(n, i),
                            t.data(n, "processed", !0)
                        }
                        ,
                        e.src = this.src
                    }
                })
            },
            children: function() {
                var i = s.$container.children();
                i.is("img") && (i.each(function() {
                    if (t(this).is("img")) {
                        t(this).wrap("<div>");
                        var i = t(this).attr("id");
                        t(this).removeAttr("id"),
                        t(this).parent().attr("id", i)
                    }
                }),
                i = s.$container.children()),
                s.init || i.css({
                    display: "none",
                    left: 2 * s.width
                }),
                i.css({
                    position: "absolute",
                    overflow: "hidden",
                    height: "100%",
                    width: s.width,
                    top: 0,
                    zIndex: 0
                })
            }
        }
          , h = {
            slide: function(i, t) {
                var n = s.$container.children();
                n.eq(i.upcoming_slide).css({
                    left: i.upcoming_position,
                    display: "block"
                }),
                s.$control.animate({
                    left: i.offset
                }, s.options.animation_speed, s.options.animation_easing, function() {
                    s.size() > 1 && (s.$control.css({
                        left: -s.width
                    }),
                    n.eq(i.upcoming_slide).css({
                        left: s.width,
                        zIndex: 2
                    }),
                    i.outgoing_slide >= 0 && n.eq(i.outgoing_slide).css({
                        left: s.width,
                        display: "none",
                        zIndex: 0
                    })),
                    t()
                })
            },
            fade: function(i, t) {
                var n = this
                  , e = n.$container.children()
                  , s = e.eq(i.outgoing_slide)
                  , o = e.eq(i.upcoming_slide);
                o.css({
                    left: this.width,
                    opacity: 1,
                    display: "block"
                }),
                i.outgoing_slide >= 0 ? s.animate({
                    opacity: 0
                }, n.options.animation_speed, n.options.animation_easing, function() {
                    n.size() > 1 && (e.eq(i.upcoming_slide).css({
                        zIndex: 2
                    }),
                    i.outgoing_slide >= 0 && e.eq(i.outgoing_slide).css({
                        opacity: 1,
                        display: "none",
                        zIndex: 0
                    })),
                    t()
                }) : (o.css({
                    zIndex: 2
                }),
                t())
            }
        };
        h = t.extend(h, t.fn.superslides.fx);
        var c = {
            _centerY: function(i) {
                var n = t(i);
                n.css({
                    top: (s.height - n.height()) / 2
                })
            },
            _centerX: function(i) {
                var n = t(i);
                n.css({
                    left: (s.width - n.width()) / 2
                })
            },
            _center: function(i) {
                s.image._centerX(i),
                s.image._centerY(i)
            },
            _aspectRatio: function(i) {
                if (!i.naturalHeight && !i.naturalWidth) {
                    var t = new Image;
                    t.src = i.src,
                    i.naturalHeight = t.height,
                    i.naturalWidth = t.width
                }
                return i.naturalHeight / i.naturalWidth
            },
            _scale: function(i, n) {
                n = n || s.image._aspectRatio(i);
                var e = s.height / s.width
                  , o = t(i);
                e > n ? o.css({
                    height: s.height,
                    width: s.height / n
                }) : o.css({
                    height: s.width * n,
                    width: s.width
                })
            }
        }
          , d = {
            _setCurrent: function(i) {
                if (s.$pagination) {
                    var t = s.$pagination.children();
                    t.removeClass("current"),
                    t.eq(i).addClass("current")
                }
            },
            _addItem: function(i) {
                var n = i + 1
                  , e = s.$container.children().eq(i).attr("id");
                e && (n = e),
                t("<a>", {
                    href: "#" + n,
                    text: n
                }).appendTo(s.$pagination)
            },
            _setup: function() {
                if (s.options.pagination && 1 !== s.size()) {
                    var i = t("<nav>", {
                        class: s.options.elements.pagination.replace(/^\./, "")
                    });
                    s.$pagination = i.appendTo(s.$el);
                    for (var n = 0; s.size() > n; n++)
                        s.pagination._addItem(n)
                }
            },
            _events: function() {
                s.$el.on("click", s.options.elements.pagination + " a", function(i) {
                    i.preventDefault();
                    var t = s._parseHash(this.hash)
                      , n = s._upcomingSlide(t - 1);
                    n !== s.current && s.animate(n, function() {
                        s.start()
                    })
                })
            }
        };
        return this.css = r,
        this.image = c,
        this.pagination = d,
        this.fx = h,
        this.animation = this.fx[this.options.animation],
        this.$control = this.$container.wrap(o).parent(".slides-control"),
        s._findPositions(),
        s.width = s._findWidth(),
        s.height = s._findHeight(),
        this.css.children(),
        this.css.containers(),
        this.css.images(),
        this.pagination._setup(),
        a = s._findMultiplier(),
        s.$el.on("click", s.options.elements.nav + " a", function(i) {
            i.preventDefault(),
            s.stop(),
            t(this).hasClass("next") ? s.animate("next", function() {
                s.start()
            }) : s.animate("prev", function() {
                s.start()
            })
        }),
        t(document).on("keyup", function(i) {
            37 === i.keyCode && s.animate("prev"),
            39 === i.keyCode && s.animate("next")
        }),
        t(i).on("resize", function() {
            setTimeout(function() {
                var i = s.$container.children();
                s.width = s._findWidth(),
                s.height = s._findHeight(),
                i.css({
                    width: s.width,
                    left: s.width
                }),
                s.css.containers(),
                s.css.images()
            }, 10)
        }),
        t(i).on("hashchange", function() {
            var i, t = s._parseHash();
            (i = t && !isNaN(t) ? s._upcomingSlide(t - 1) : s._upcomingSlide(t)) >= 0 && i !== s.current && s.animate(i)
        }),
        s.pagination._events(),
        s.start(),
        s
    }
    ).prototype = {
        _findWidth: function() {
            return t(this.options.inherit_width_from).width()
        },
        _findHeight: function() {
            return t(this.options.inherit_height_from).height()
        },
        _findMultiplier: function() {
            return 1 === this.size() ? 1 : 3
        },
        _upcomingSlide: function(i) {
            if (/next/.test(i))
                return this._nextInDom();
            if (/prev/.test(i))
                return this._prevInDom();
            if (/\d/.test(i))
                return +i;
            if (i && /\w/.test(i)) {
                var t = this._findSlideById(i);
                return t >= 0 ? t : 0
            }
            return 0
        },
        _findSlideById: function(i) {
            return this.$container.find("#" + i).index()
        },
        _findPositions: function(i, t) {
            t = t || this,
            void 0 === i && (i = -1),
            t.current = i,
            t.next = t._nextInDom(),
            t.prev = t._prevInDom()
        },
        _nextInDom: function() {
            var i = this.current + 1;
            return i === this.size() && (i = 0),
            i
        },
        _prevInDom: function() {
            var i = this.current - 1;
            return 0 > i && (i = this.size() - 1),
            i
        },
        _parseHash: function(t) {
            return t = t || i.location.hash,
            (t = t.replace(/^#/, "")) && !isNaN(+t) && (t = +t),
            t
        },
        size: function() {
            return this.$container.children().length
        },
        destroy: function() {
            return this.$el.removeData()
        },
        update: function() {
            this.css.children(),
            this.css.containers(),
            this.css.images(),
            this.pagination._addItem(this.size()),
            this._findPositions(this.current),
            this.$el.trigger("updated.slides")
        },
        stop: function() {
            clearInterval(this.play_id),
            delete this.play_id,
            this.$el.trigger("stopped.slides")
        },
        start: function() {
            var n = this;
            n.options.hashchange ? t(i).trigger("hashchange") : this.animate(),
            this.options.play && (this.play_id && this.stop(),
            this.play_id = setInterval(function() {
                n.animate()
            }, this.options.play)),
            this.$el.trigger("started.slides")
        },
        animate: function(t, n) {
            var e = this
              , s = {};
            if (!(this.animating || (this.animating = !0,
            void 0 === t && (t = "next"),
            s.upcoming_slide = this._upcomingSlide(t),
            s.upcoming_slide >= this.size()))) {
                if (s.outgoing_slide = this.current,
                s.upcoming_position = 2 * this.width,
                s.offset = -s.upcoming_position,
                ("prev" === t || s.outgoing_slide > t) && (s.upcoming_position = 0,
                s.offset = 0),
                e.size() > 1 && e.pagination._setCurrent(s.upcoming_slide),
                e.options.hashchange) {
                    var o = s.upcoming_slide + 1
                      , a = e.$container.children(":eq(" + s.upcoming_slide + ")").attr("id");
                    i.location.hash = a || o
                }
                e.$el.trigger("animating.slides", [s]),
                e.animation(s, function() {
                    e._findPositions(s.upcoming_slide, e),
                    "function" == typeof n && n(),
                    e.animating = !1,
                    e.$el.trigger("animated.slides"),
                    e.init || (e.$el.trigger("init.slides"),
                    e.init = !0,
                    e.$container.fadeIn("fast"))
                })
            }
        }
    },
    t.fn[e] = function(i, s) {
        var o = [];
        return this.each(function() {
            var a, r, h;
            return a = t(this),
            r = a.data(e),
            h = "object" == typeof i && i,
            r || (o = a.data(e, r = new n(this,h))),
            "string" == typeof i && "function" == typeof (o = r[i]) ? o = o.call(r, s) : void 0
        }),
        o
    }
    ,
    t.fn[e].fx = {}
}(this, jQuery);
!function(e) {
    "function" == typeof define && define.amd ? define(["jquery"], e) : e("object" == typeof exports ? require("jquery") : window.jQuery || window.Zepto)
}(function(e) {
    var t, n, i, o, r, a, s = "Close", l = "BeforeClose", c = "MarkupParse", d = "Open", u = "Change", p = "mfp", f = "." + p, m = "mfp-ready", g = "mfp-removing", h = "mfp-prevent-close", v = function() {}, C = !!window.jQuery, y = e(window), w = function(e, n) {
        t.ev.on(p + e + f, n)
    }, b = function(t, n, i, o) {
        var r = document.createElement("div");
        return r.className = "mfp-" + t,
        i && (r.innerHTML = i),
        o ? n && n.appendChild(r) : (r = e(r),
        n && r.appendTo(n)),
        r
    }, I = function(n, i) {
        t.ev.triggerHandler(p + n, i),
        t.st.callbacks && (n = n.charAt(0).toLowerCase() + n.slice(1),
        t.st.callbacks[n] && t.st.callbacks[n].apply(t, e.isArray(i) ? i : [i]))
    }, x = function(n) {
        return n === a && t.currTemplate.closeBtn || (t.currTemplate.closeBtn = e(t.st.closeMarkup.replace("%title%", t.st.tClose)),
        a = n),
        t.currTemplate.closeBtn
    }, k = function() {
        e.magnificPopup.instance || ((t = new v).init(),
        e.magnificPopup.instance = t)
    }, T = function() {
        var e = document.createElement("p").style
          , t = ["ms", "O", "Moz", "Webkit"];
        if (void 0 !== e.transition)
            return !0;
        for (; t.length; )
            if (t.pop() + "Transition"in e)
                return !0;
        return !1
    };
    v.prototype = {
        constructor: v,
        init: function() {
            var n = navigator.appVersion;
            t.isIE7 = -1 !== n.indexOf("MSIE 7."),
            t.isIE8 = -1 !== n.indexOf("MSIE 8."),
            t.isLowIE = t.isIE7 || t.isIE8,
            t.isAndroid = /android/gi.test(n),
            t.isIOS = /iphone|ipad|ipod/gi.test(n),
            t.supportsTransition = T(),
            t.probablyMobile = t.isAndroid || t.isIOS || /(Opera Mini)|Kindle|webOS|BlackBerry|(Opera Mobi)|(Windows Phone)|IEMobile/i.test(navigator.userAgent),
            i = e(document),
            t.popupsCache = {}
        },
        open: function(n) {
            var o;
            if (!1 === n.isObj) {
                t.items = n.items.toArray(),
                t.index = 0;
                var a, s = n.items;
                for (o = 0; o < s.length; o++)
                    if ((a = s[o]).parsed && (a = a.el[0]),
                    a === n.el[0]) {
                        t.index = o;
                        break
                    }
            } else
                t.items = e.isArray(n.items) ? n.items : [n.items],
                t.index = n.index || 0;
            {
                if (!t.isOpen) {
                    t.types = [],
                    r = "",
                    t.ev = n.mainEl && n.mainEl.length ? n.mainEl.eq(0) : i,
                    n.key ? (t.popupsCache[n.key] || (t.popupsCache[n.key] = {}),
                    t.currTemplate = t.popupsCache[n.key]) : t.currTemplate = {},
                    t.st = e.extend(!0, {}, e.magnificPopup.defaults, n),
                    t.fixedContentPos = "auto" === t.st.fixedContentPos ? !t.probablyMobile : t.st.fixedContentPos,
                    t.st.modal && (t.st.closeOnContentClick = !1,
                    t.st.closeOnBgClick = !1,
                    t.st.showCloseBtn = !1,
                    t.st.enableEscapeKey = !1),
                    t.bgOverlay || (t.bgOverlay = b("bg").on("click" + f, function() {
                        t.close()
                    }),
                    t.wrap = b("wrap").attr("tabindex", -1).on("click" + f, function(e) {
                        t._checkIfClose(e.target) && t.close()
                    }),
                    t.container = b("container", t.wrap)),
                    t.contentContainer = b("content"),
                    t.st.preloader && (t.preloader = b("preloader", t.container, t.st.tLoading));
                    var l = e.magnificPopup.modules;
                    for (o = 0; o < l.length; o++) {
                        var u = l[o];
                        u = u.charAt(0).toUpperCase() + u.slice(1),
                        t["init" + u].call(t)
                    }
                    I("BeforeOpen"),
                    t.st.showCloseBtn && (t.st.closeBtnInside ? (w(c, function(e, t, n, i) {
                        n.close_replaceWith = x(i.type)
                    }),
                    r += " mfp-close-btn-in") : t.wrap.append(x())),
                    t.st.alignTop && (r += " mfp-align-top"),
                    t.wrap.css(t.fixedContentPos ? {
                        overflow: t.st.overflowY,
                        overflowX: "hidden",
                        overflowY: t.st.overflowY
                    } : {
                        top: y.scrollTop(),
                        position: "absolute"
                    }),
                    (!1 === t.st.fixedBgPos || "auto" === t.st.fixedBgPos && !t.fixedContentPos) && t.bgOverlay.css({
                        height: i.height(),
                        position: "absolute"
                    }),
                    t.st.enableEscapeKey && i.on("keyup" + f, function(e) {
                        27 === e.keyCode && t.close()
                    }),
                    y.on("resize" + f, function() {
                        t.updateSize()
                    }),
                    t.st.closeOnContentClick || (r += " mfp-auto-cursor"),
                    r && t.wrap.addClass(r);
                    var p = t.wH = y.height()
                      , g = {};
                    if (t.fixedContentPos && t._hasScrollBar(p)) {
                        var h = t._getScrollbarSize();
                        h && (g.marginRight = h)
                    }
                    t.fixedContentPos && (t.isIE7 ? e("body, html").css("overflow", "hidden") : g.overflow = "hidden");
                    var v = t.st.mainClass;
                    return t.isIE7 && (v += " mfp-ie7"),
                    v && t._addClassToMFP(v),
                    t.updateItemHTML(),
                    I("BuildControls"),
                    e("html").css(g),
                    t.bgOverlay.add(t.wrap).prependTo(t.st.prependTo || e(document.body)),
                    t._lastFocusedEl = document.activeElement,
                    setTimeout(function() {
                        t.content ? (t._addClassToMFP(m),
                        t._setFocus()) : t.bgOverlay.addClass(m),
                        i.on("focusin" + f, t._onFocusIn)
                    }, 16),
                    t.isOpen = !0,
                    t.updateSize(p),
                    I(d),
                    n
                }
                t.updateItemHTML()
            }
        },
        close: function() {
            t.isOpen && (I(l),
            t.isOpen = !1,
            t.st.removalDelay && !t.isLowIE && t.supportsTransition ? (t._addClassToMFP(g),
            setTimeout(function() {
                t._close()
            }, t.st.removalDelay)) : t._close())
        },
        _close: function() {
            I(s);
            var n = g + " " + m + " ";
            if (t.bgOverlay.detach(),
            t.wrap.detach(),
            t.container.empty(),
            t.st.mainClass && (n += t.st.mainClass + " "),
            t._removeClassFromMFP(n),
            t.fixedContentPos) {
                var o = {
                    marginRight: ""
                };
                t.isIE7 ? e("body, html").css("overflow", "") : o.overflow = "",
                e("html").css(o)
            }
            i.off("keyup.mfp focusin" + f),
            t.ev.off(f),
            t.wrap.attr("class", "mfp-wrap").removeAttr("style"),
            t.bgOverlay.attr("class", "mfp-bg"),
            t.container.attr("class", "mfp-container"),
            !t.st.showCloseBtn || t.st.closeBtnInside && !0 !== t.currTemplate[t.currItem.type] || t.currTemplate.closeBtn && t.currTemplate.closeBtn.detach(),
            t._lastFocusedEl && e(t._lastFocusedEl).focus(),
            t.currItem = null,
            t.content = null,
            t.currTemplate = null,
            t.prevHeight = 0,
            I("AfterClose")
        },
        updateSize: function(e) {
            if (t.isIOS) {
                var n = document.documentElement.clientWidth / window.innerWidth
                  , i = window.innerHeight * n;
                t.wrap.css("height", i),
                t.wH = i
            } else
                t.wH = e || y.height();
            t.fixedContentPos || t.wrap.css("height", t.wH),
            I("Resize")
        },
        updateItemHTML: function() {
            var n = t.items[t.index];
            t.contentContainer.detach(),
            t.content && t.content.detach(),
            n.parsed || (n = t.parseEl(t.index));
            var i = n.type;
            if (I("BeforeChange", [t.currItem ? t.currItem.type : "", i]),
            t.currItem = n,
            !t.currTemplate[i]) {
                var r = !!t.st[i] && t.st[i].markup;
                I("FirstMarkupParse", r),
                t.currTemplate[i] = !r || e(r)
            }
            o && o !== n.type && t.container.removeClass("mfp-" + o + "-holder");
            var a = t["get" + i.charAt(0).toUpperCase() + i.slice(1)](n, t.currTemplate[i]);
            t.appendContent(a, i),
            n.preloaded = !0,
            I(u, n),
            o = n.type,
            t.container.prepend(t.contentContainer),
            I("AfterChange")
        },
        appendContent: function(e, n) {
            t.content = e,
            e ? t.st.showCloseBtn && t.st.closeBtnInside && !0 === t.currTemplate[n] ? t.content.find(".mfp-close").length || t.content.append(x()) : t.content = e : t.content = "",
            I("BeforeAppend"),
            t.container.addClass("mfp-" + n + "-holder"),
            t.contentContainer.append(t.content)
        },
        parseEl: function(n) {
            var i, o = t.items[n];
            if (o.tagName ? o = {
                el: e(o)
            } : (i = o.type,
            o = {
                data: o,
                src: o.src
            }),
            o.el) {
                for (var r = t.types, a = 0; a < r.length; a++)
                    if (o.el.hasClass("mfp-" + r[a])) {
                        i = r[a];
                        break
                    }
                o.src = o.el.attr("data-mfp-src"),
                o.src || (o.src = o.el.attr("href"))
            }
            return o.type = i || t.st.type || "inline",
            o.index = n,
            o.parsed = !0,
            t.items[n] = o,
            I("ElementParse", o),
            t.items[n]
        },
        addGroup: function(e, n) {
            var i = function(i) {
                i.mfpEl = this,
                t._openClick(i, e, n)
            };
            n || (n = {});
            var o = "click.magnificPopup";
            n.mainEl = e,
            n.items ? (n.isObj = !0,
            e.off(o).on(o, i)) : (n.isObj = !1,
            n.delegate ? e.off(o).on(o, n.delegate, i) : (n.items = e,
            e.off(o).on(o, i)))
        },
        _openClick: function(n, i, o) {
            if ((void 0 !== o.midClick ? o.midClick : e.magnificPopup.defaults.midClick) || 2 !== n.which && !n.ctrlKey && !n.metaKey) {
                var r = void 0 !== o.disableOn ? o.disableOn : e.magnificPopup.defaults.disableOn;
                if (r)
                    if (e.isFunction(r)) {
                        if (!r.call(t))
                            return !0
                    } else if (y.width() < r)
                        return !0;
                n.type && (n.preventDefault(),
                t.isOpen && n.stopPropagation()),
                o.el = e(n.mfpEl),
                o.delegate && (o.items = i.find(o.delegate)),
                t.open(o)
            }
        },
        updateStatus: function(e, i) {
            if (t.preloader) {
                n !== e && t.container.removeClass("mfp-s-" + n),
                i || "loading" !== e || (i = t.st.tLoading);
                var o = {
                    status: e,
                    text: i
                };
                I("UpdateStatus", o),
                e = o.status,
                i = o.text,
                t.preloader.html(i),
                t.preloader.find("a").on("click", function(e) {
                    e.stopImmediatePropagation()
                }),
                t.container.addClass("mfp-s-" + e),
                n = e
            }
        },
        _checkIfClose: function(n) {
            if (!e(n).hasClass(h)) {
                var i = t.st.closeOnContentClick
                  , o = t.st.closeOnBgClick;
                if (i && o)
                    return !0;
                if (!t.content || e(n).hasClass("mfp-close") || t.preloader && n === t.preloader[0])
                    return !0;
                if (n === t.content[0] || e.contains(t.content[0], n)) {
                    if (i)
                        return !0
                } else if (o && e.contains(document, n))
                    return !0;
                return !1
            }
        },
        _addClassToMFP: function(e) {
            t.bgOverlay.addClass(e),
            t.wrap.addClass(e)
        },
        _removeClassFromMFP: function(e) {
            this.bgOverlay.removeClass(e),
            t.wrap.removeClass(e)
        },
        _hasScrollBar: function(e) {
            return (t.isIE7 ? i.height() : document.body.scrollHeight) > (e || y.height())
        },
        _setFocus: function() {
            (t.st.focus ? t.content.find(t.st.focus).eq(0) : t.wrap).focus()
        },
        _onFocusIn: function(n) {
            return n.target === t.wrap[0] || e.contains(t.wrap[0], n.target) ? void 0 : (t._setFocus(),
            !1)
        },
        _parseMarkup: function(t, n, i) {
            var o;
            i.data && (n = e.extend(i.data, n)),
            I(c, [t, n, i]),
            e.each(n, function(e, n) {
                if (void 0 === n || !1 === n)
                    return !0;
                if ((o = e.split("_")).length > 1) {
                    var i = t.find(f + "-" + o[0]);
                    if (i.length > 0) {
                        var r = o[1];
                        "replaceWith" === r ? i[0] !== n[0] && i.replaceWith(n) : "img" === r ? i.is("img") ? i.attr("src", n) : i.replaceWith('<img src="' + n + '" class="' + i.attr("class") + '" />') : i.attr(o[1], n)
                    }
                } else
                    t.find(f + "-" + e).html(n)
            })
        },
        _getScrollbarSize: function() {
            if (void 0 === t.scrollbarSize) {
                var e = document.createElement("div");
                e.style.cssText = "width: 99px; height: 99px; overflow: scroll; position: absolute; top: -9999px;",
                document.body.appendChild(e),
                t.scrollbarSize = e.offsetWidth - e.clientWidth,
                document.body.removeChild(e)
            }
            return t.scrollbarSize
        }
    },
    e.magnificPopup = {
        instance: null,
        proto: v.prototype,
        modules: [],
        open: function(t, n) {
            return k(),
            t = t ? e.extend(!0, {}, t) : {},
            t.isObj = !0,
            t.index = n || 0,
            this.instance.open(t)
        },
        close: function() {
            return e.magnificPopup.instance && e.magnificPopup.instance.close()
        },
        registerModule: function(t, n) {
            n.options && (e.magnificPopup.defaults[t] = n.options),
            e.extend(this.proto, n.proto),
            this.modules.push(t)
        },
        defaults: {
            disableOn: 0,
            key: null,
            midClick: !1,
            mainClass: "",
            preloader: !0,
            focus: "",
            closeOnContentClick: !1,
            closeOnBgClick: !0,
            closeBtnInside: !0,
            showCloseBtn: !0,
            enableEscapeKey: !0,
            modal: !1,
            alignTop: !1,
            removalDelay: 0,
            prependTo: null,
            fixedContentPos: "auto",
            fixedBgPos: "auto",
            overflowY: "auto",
            closeMarkup: '<button title="%title%" type="button" class="mfp-close">&times;</button>',
            tClose: "Close (Esc)",
            tLoading: "Loading..."
        }
    },
    e.fn.magnificPopup = function(n) {
        k();
        var i = e(this);
        if ("string" == typeof n)
            if ("open" === n) {
                var o, r = C ? i.data("magnificPopup") : i[0].magnificPopup, a = parseInt(arguments[1], 10) || 0;
                r.items ? o = r.items[a] : (o = i,
                r.delegate && (o = o.find(r.delegate)),
                o = o.eq(a)),
                t._openClick({
                    mfpEl: o
                }, i, r)
            } else
                t.isOpen && t[n].apply(t, Array.prototype.slice.call(arguments, 1));
        else
            n = e.extend(!0, {}, n),
            C ? i.data("magnificPopup", n) : i[0].magnificPopup = n,
            t.addGroup(i, n);
        return i
    }
    ;
    var E, _, S, P = "inline", O = function() {
        S && (_.after(S.addClass(E)).detach(),
        S = null)
    };
    e.magnificPopup.registerModule(P, {
        options: {
            hiddenClass: "hide",
            markup: "",
            tNotFound: "Content not found"
        },
        proto: {
            initInline: function() {
                t.types.push(P),
                w(s + "." + P, function() {
                    O()
                })
            },
            getInline: function(n, i) {
                if (O(),
                n.src) {
                    var o = t.st.inline
                      , r = e(n.src);
                    if (r.length) {
                        var a = r[0].parentNode;
                        a && a.tagName && (_ || (E = o.hiddenClass,
                        _ = b(E),
                        E = "mfp-" + E),
                        S = r.after(_).detach().removeClass(E)),
                        t.updateStatus("ready")
                    } else
                        t.updateStatus("error", o.tNotFound),
                        r = e("<div>");
                    return n.inlineElement = r,
                    r
                }
                return t.updateStatus("ready"),
                t._parseMarkup(i, {}, n),
                i
            }
        }
    });
    var z, M = "ajax", B = function() {
        z && e(document.body).removeClass(z)
    }, F = function() {
        B(),
        t.req && t.req.abort()
    };
    e.magnificPopup.registerModule(M, {
        options: {
            settings: null,
            cursor: "mfp-ajax-cur",
            tError: '<a href="%url%">The content</a> could not be loaded.'
        },
        proto: {
            initAjax: function() {
                t.types.push(M),
                z = t.st.ajax.cursor,
                w(s + "." + M, F),
                w("BeforeChange." + M, F)
            },
            getAjax: function(n) {
                z && e(document.body).addClass(z),
                t.updateStatus("loading");
                var i = e.extend({
                    url: n.src,
                    success: function(i, o, r) {
                        var a = {
                            data: i,
                            xhr: r
                        };
                        I("ParseAjax", a),
                        t.appendContent(e(a.data), M),
                        n.finished = !0,
                        B(),
                        t._setFocus(),
                        setTimeout(function() {
                            t.wrap.addClass(m)
                        }, 16),
                        t.updateStatus("ready"),
                        I("AjaxContentAdded")
                    },
                    error: function() {
                        B(),
                        n.finished = n.loadError = !0,
                        t.updateStatus("error", t.st.ajax.tError.replace("%url%", n.src))
                    }
                }, t.st.ajax.settings);
                return t.req = e.ajax(i),
                ""
            }
        }
    });
    var H, L = function(n) {
        if (n.data && void 0 !== n.data.title)
            return n.data.title;
        var i = t.st.image.titleSrc;
        if (i) {
            if (e.isFunction(i))
                return i.call(t, n);
            if (n.el)
                return n.el.attr(i) || ""
        }
        return ""
    };
    e.magnificPopup.registerModule("image", {
        options: {
            markup: '<div class="mfp-figure"><div class="mfp-close"></div><figure><div class="mfp-img"></div><figcaption><div class="mfp-bottom-bar"><div class="mfp-title"></div><div class="mfp-counter"></div></div></figcaption></figure></div>',
            cursor: "mfp-zoom-out-cur",
            titleSrc: "title",
            verticalFit: !0,
            tError: '<a href="%url%">The image</a> could not be loaded.'
        },
        proto: {
            initImage: function() {
                var n = t.st.image
                  , i = ".image";
                t.types.push("image"),
                w(d + i, function() {
                    "image" === t.currItem.type && n.cursor && e(document.body).addClass(n.cursor)
                }),
                w(s + i, function() {
                    n.cursor && e(document.body).removeClass(n.cursor),
                    y.off("resize" + f)
                }),
                w("Resize" + i, t.resizeImage),
                t.isLowIE && w("AfterChange", t.resizeImage)
            },
            resizeImage: function() {
                var e = t.currItem;
                if (e && e.img && t.st.image.verticalFit) {
                    var n = 0;
                    t.isLowIE && (n = parseInt(e.img.css("padding-top"), 10) + parseInt(e.img.css("padding-bottom"), 10)),
                    e.img.css("max-height", t.wH - n)
                }
            },
            _onImageHasSize: function(e) {
                e.img && (e.hasSize = !0,
                H && clearInterval(H),
                e.isCheckingImgSize = !1,
                I("ImageHasSize", e),
                e.imgHidden && (t.content && t.content.removeClass("mfp-loading"),
                e.imgHidden = !1))
            },
            findImageSize: function(e) {
                var n = 0
                  , i = e.img[0]
                  , o = function(r) {
                    H && clearInterval(H),
                    H = setInterval(function() {
                        return i.naturalWidth > 0 ? void t._onImageHasSize(e) : (n > 200 && clearInterval(H),
                        n++,
                        void (3 === n ? o(10) : 40 === n ? o(50) : 100 === n && o(500)))
                    }, r)
                };
                o(1)
            },
            getImage: function(n, i) {
                var o = 0
                  , r = function() {
                    n && (n.img[0].complete ? (n.img.off(".mfploader"),
                    n === t.currItem && (t._onImageHasSize(n),
                    t.updateStatus("ready")),
                    n.hasSize = !0,
                    n.loaded = !0,
                    I("ImageLoadComplete")) : (o++,
                    200 > o ? setTimeout(r, 100) : a()))
                }
                  , a = function() {
                    n && (n.img.off(".mfploader"),
                    n === t.currItem && (t._onImageHasSize(n),
                    t.updateStatus("error", s.tError.replace("%url%", n.src))),
                    n.hasSize = !0,
                    n.loaded = !0,
                    n.loadError = !0)
                }
                  , s = t.st.image
                  , l = i.find(".mfp-img");
                if (l.length) {
                    var c = document.createElement("img");
                    c.className = "mfp-img",
                    n.el && n.el.find("img").length && (c.alt = n.el.find("img").attr("alt")),
                    n.img = e(c).on("load.mfploader", r).on("error.mfploader", a),
                    c.src = n.src,
                    l.is("img") && (n.img = n.img.clone()),
                    (c = n.img[0]).naturalWidth > 0 ? n.hasSize = !0 : c.width || (n.hasSize = !1)
                }
                return t._parseMarkup(i, {
                    title: L(n),
                    img_replaceWith: n.img
                }, n),
                t.resizeImage(),
                n.hasSize ? (H && clearInterval(H),
                n.loadError ? (i.addClass("mfp-loading"),
                t.updateStatus("error", s.tError.replace("%url%", n.src))) : (i.removeClass("mfp-loading"),
                t.updateStatus("ready")),
                i) : (t.updateStatus("loading"),
                n.loading = !0,
                n.hasSize || (n.imgHidden = !0,
                i.addClass("mfp-loading"),
                t.findImageSize(n)),
                i)
            }
        }
    });
    var A, j = function() {
        return void 0 === A && (A = void 0 !== document.createElement("p").style.MozTransform),
        A
    };
    e.magnificPopup.registerModule("zoom", {
        options: {
            enabled: !1,
            easing: "ease-in-out",
            duration: 300,
            opener: function(e) {
                return e.is("img") ? e : e.find("img")
            }
        },
        proto: {
            initZoom: function() {
                var e, n = t.st.zoom, i = ".zoom";
                if (n.enabled && t.supportsTransition) {
                    var o, r, a = n.duration, c = function(e) {
                        var t = e.clone().removeAttr("style").removeAttr("class").addClass("mfp-animated-image")
                          , i = "all " + n.duration / 1e3 + "s " + n.easing
                          , o = {
                            position: "fixed",
                            zIndex: 9999,
                            left: 0,
                            top: 0,
                            "-webkit-backface-visibility": "hidden"
                        }
                          , r = "transition";
                        return o["-webkit-" + r] = o["-moz-" + r] = o["-o-" + r] = o[r] = i,
                        t.css(o),
                        t
                    }, d = function() {
                        t.content.css("visibility", "visible")
                    };
                    w("BuildControls" + i, function() {
                        if (t._allowZoom()) {
                            if (clearTimeout(o),
                            t.content.css("visibility", "hidden"),
                            !(e = t._getItemToZoom()))
                                return void d();
                            (r = c(e)).css(t._getOffset()),
                            t.wrap.append(r),
                            o = setTimeout(function() {
                                r.css(t._getOffset(!0)),
                                o = setTimeout(function() {
                                    d(),
                                    setTimeout(function() {
                                        r.remove(),
                                        e = r = null,
                                        I("ZoomAnimationEnded")
                                    }, 16)
                                }, a)
                            }, 16)
                        }
                    }),
                    w(l + i, function() {
                        if (t._allowZoom()) {
                            if (clearTimeout(o),
                            t.st.removalDelay = a,
                            !e) {
                                if (!(e = t._getItemToZoom()))
                                    return;
                                r = c(e)
                            }
                            r.css(t._getOffset(!0)),
                            t.wrap.append(r),
                            t.content.css("visibility", "hidden"),
                            setTimeout(function() {
                                r.css(t._getOffset())
                            }, 16)
                        }
                    }),
                    w(s + i, function() {
                        t._allowZoom() && (d(),
                        r && r.remove(),
                        e = null)
                    })
                }
            },
            _allowZoom: function() {
                return "image" === t.currItem.type
            },
            _getItemToZoom: function() {
                return !!t.currItem.hasSize && t.currItem.img
            },
            _getOffset: function(n) {
                var i, o = (i = n ? t.currItem.img : t.st.zoom.opener(t.currItem.el || t.currItem)).offset(), r = parseInt(i.css("padding-top"), 10), a = parseInt(i.css("padding-bottom"), 10);
                o.top -= e(window).scrollTop() - r;
                var s = {
                    width: i.width(),
                    height: (C ? i.innerHeight() : i[0].offsetHeight) - a - r
                };
                return j() ? s["-moz-transform"] = s.transform = "translate(" + o.left + "px," + o.top + "px)" : (s.left = o.left,
                s.top = o.top),
                s
            }
        }
    });
    var N = "iframe"
      , W = function(e) {
        if (t.currTemplate[N]) {
            var n = t.currTemplate[N].find("iframe");
            n.length && (e || (n[0].src = "//about:blank"),
            t.isIE8 && n.css("display", e ? "block" : "none"))
        }
    };
    e.magnificPopup.registerModule(N, {
        options: {
            markup: '<div class="mfp-iframe-scaler"><div class="mfp-close"></div><iframe class="mfp-iframe" src="//about:blank" frameborder="0" allowfullscreen></iframe></div>',
            srcAction: "iframe_src",
            patterns: {
                youtube: {
                    index: "youtube.com",
                    id: "v=",
                    src: "//www.youtube.com/embed/%id%?autoplay=1"
                },
                vimeo: {
                    index: "vimeo.com/",
                    id: "/",
                    src: "//player.vimeo.com/video/%id%?autoplay=1"
                },
                gmaps: {
                    index: "//maps.google.",
                    src: "%id%&output=embed"
                }
            }
        },
        proto: {
            initIframe: function() {
                t.types.push(N),
                w("BeforeChange", function(e, t, n) {
                    t !== n && (t === N ? W() : n === N && W(!0))
                }),
                w(s + "." + N, function() {
                    W()
                })
            },
            getIframe: function(n, i) {
                var o = n.src
                  , r = t.st.iframe;
                e.each(r.patterns, function() {
                    return o.indexOf(this.index) > -1 ? (this.id && (o = "string" == typeof this.id ? o.substr(o.lastIndexOf(this.id) + this.id.length, o.length) : this.id.call(this, o)),
                    o = this.src.replace("%id%", o),
                    !1) : void 0
                });
                var a = {};
                return r.srcAction && (a[r.srcAction] = o),
                t._parseMarkup(i, a, n),
                t.updateStatus("ready"),
                i
            }
        }
    });
    var R = function(e) {
        var n = t.items.length;
        return e > n - 1 ? e - n : 0 > e ? n + e : e
    }
      , Z = function(e, t, n) {
        return e.replace(/%curr%/gi, t + 1).replace(/%total%/gi, n)
    };
    e.magnificPopup.registerModule("gallery", {
        options: {
            enabled: !1,
            arrowMarkup: '<button title="%title%" type="button" class="mfp-arrow mfp-arrow-%dir%"></button>',
            preload: [0, 2],
            navigateByImgClick: !0,
            arrows: !0,
            tPrev: "Previous (Left arrow key)",
            tNext: "Next (Right arrow key)",
            tCounter: "%curr% of %total%"
        },
        proto: {
            initGallery: function() {
                var n = t.st.gallery
                  , o = ".mfp-gallery"
                  , a = Boolean(e.fn.mfpFastClick);
                return t.direction = !0,
                !(!n || !n.enabled) && (r += " mfp-gallery",
                w(d + o, function() {
                    n.navigateByImgClick && t.wrap.on("click" + o, ".mfp-img", function() {
                        return t.items.length > 1 ? (t.next(),
                        !1) : void 0
                    }),
                    i.on("keydown" + o, function(e) {
                        37 === e.keyCode ? t.prev() : 39 === e.keyCode && t.next()
                    })
                }),
                w("UpdateStatus" + o, function(e, n) {
                    n.text && (n.text = Z(n.text, t.currItem.index, t.items.length))
                }),
                w(c + o, function(e, i, o, r) {
                    var a = t.items.length;
                    o.counter = a > 1 ? Z(n.tCounter, r.index, a) : ""
                }),
                w("BuildControls" + o, function() {
                    if (t.items.length > 1 && n.arrows && !t.arrowLeft) {
                        var i = n.arrowMarkup
                          , o = t.arrowLeft = e(i.replace(/%title%/gi, n.tPrev).replace(/%dir%/gi, "left")).addClass(h)
                          , r = t.arrowRight = e(i.replace(/%title%/gi, n.tNext).replace(/%dir%/gi, "right")).addClass(h)
                          , s = a ? "mfpFastClick" : "click";
                        o[s](function() {
                            t.prev()
                        }),
                        r[s](function() {
                            t.next()
                        }),
                        t.isIE7 && (b("b", o[0], !1, !0),
                        b("a", o[0], !1, !0),
                        b("b", r[0], !1, !0),
                        b("a", r[0], !1, !0)),
                        t.container.append(o.add(r))
                    }
                }),
                w(u + o, function() {
                    t._preloadTimeout && clearTimeout(t._preloadTimeout),
                    t._preloadTimeout = setTimeout(function() {
                        t.preloadNearbyImages(),
                        t._preloadTimeout = null
                    }, 16)
                }),
                void w(s + o, function() {
                    i.off(o),
                    t.wrap.off("click" + o),
                    t.arrowLeft && a && t.arrowLeft.add(t.arrowRight).destroyMfpFastClick(),
                    t.arrowRight = t.arrowLeft = null
                }))
            },
            next: function() {
                t.direction = !0,
                t.index = R(t.index + 1),
                t.updateItemHTML()
            },
            prev: function() {
                t.direction = !1,
                t.index = R(t.index - 1),
                t.updateItemHTML()
            },
            goTo: function(e) {
                t.direction = e >= t.index,
                t.index = e,
                t.updateItemHTML()
            },
            preloadNearbyImages: function() {
                var e, n = t.st.gallery.preload, i = Math.min(n[0], t.items.length), o = Math.min(n[1], t.items.length);
                for (e = 1; e <= (t.direction ? o : i); e++)
                    t._preloadItem(t.index + e);
                for (e = 1; e <= (t.direction ? i : o); e++)
                    t._preloadItem(t.index - e)
            },
            _preloadItem: function(n) {
                if (n = R(n),
                !t.items[n].preloaded) {
                    var i = t.items[n];
                    i.parsed || (i = t.parseEl(n)),
                    I("LazyLoad", i),
                    "image" === i.type && (i.img = e('<img class="mfp-img" />').on("load.mfploader", function() {
                        i.hasSize = !0
                    }).on("error.mfploader", function() {
                        i.hasSize = !0,
                        i.loadError = !0,
                        I("LazyLoadError", i)
                    }).attr("src", i.src)),
                    i.preloaded = !0
                }
            }
        }
    });
    var q = "retina";
    e.magnificPopup.registerModule(q, {
        options: {
            replaceSrc: function(e) {
                return e.src.replace(/\.\w+$/, function(e) {
                    return "@2x" + e
                })
            },
            ratio: 1
        },
        proto: {
            initRetina: function() {
                if (window.devicePixelRatio > 1) {
                    var e = t.st.retina
                      , n = e.ratio;
                    (n = isNaN(n) ? n() : n) > 1 && (w("ImageHasSize." + q, function(e, t) {
                        t.img.css({
                            "max-width": t.img[0].naturalWidth / n,
                            width: "100%"
                        })
                    }),
                    w("ElementParse." + q, function(t, i) {
                        i.src = e.replaceSrc(i, n)
                    }))
                }
            }
        }
    }),
    function() {
        var t = "ontouchstart"in window
          , n = function() {
            y.off("touchmove" + i + " touchend" + i)
        }
          , i = ".mfpFastClick";
        e.fn.mfpFastClick = function(o) {
            return e(this).each(function() {
                var r, a = e(this);
                if (t) {
                    var s, l, c, d, u, p;
                    a.on("touchstart" + i, function(e) {
                        d = !1,
                        p = 1,
                        u = e.originalEvent ? e.originalEvent.touches[0] : e.touches[0],
                        l = u.clientX,
                        c = u.clientY,
                        y.on("touchmove" + i, function(e) {
                            u = e.originalEvent ? e.originalEvent.touches : e.touches,
                            p = u.length,
                            u = u[0],
                            (Math.abs(u.clientX - l) > 10 || Math.abs(u.clientY - c) > 10) && (d = !0,
                            n())
                        }).on("touchend" + i, function(e) {
                            n(),
                            d || p > 1 || (r = !0,
                            e.preventDefault(),
                            clearTimeout(s),
                            s = setTimeout(function() {
                                r = !1
                            }, 1e3),
                            o())
                        })
                    })
                }
                a.on("click" + i, function() {
                    r || o()
                })
            })
        }
        ,
        e.fn.destroyMfpFastClick = function() {
            e(this).off("touchstart" + i + " click" + i),
            t && y.off("touchmove" + i + " touchend" + i)
        }
    }(),
    k()
});
"function" != typeof Object.create && (Object.create = function(t) {
    function i() {}
    return i.prototype = t,
    new i
}
),
function(t, i, s) {
    var e = {
        init: function(i, s) {
            this.$elem = t(s),
            this.options = t.extend({}, t.fn.owlCarousel.options, this.$elem.data(), i),
            this.userOptions = i,
            this.loadContent()
        },
        loadContent: function() {
            var i, s = this;
            "function" == typeof s.options.beforeInit && s.options.beforeInit.apply(this, [s.$elem]),
            "string" == typeof s.options.jsonPath ? (i = s.options.jsonPath,
            t.getJSON(i, function(t) {
                var i, e = "";
                if ("function" == typeof s.options.jsonSuccess)
                    s.options.jsonSuccess.apply(this, [t]);
                else {
                    for (i in t.owl)
                        t.owl.hasOwnProperty(i) && (e += t.owl[i].item);
                    s.$elem.html(e)
                }
                s.logIn()
            })) : s.logIn()
        },
        logIn: function() {
            this.$elem.data("owl-originalStyles", this.$elem.attr("style")),
            this.$elem.data("owl-originalClasses", this.$elem.attr("class")),
            this.$elem.css({
                opacity: 0
            }),
            this.orignalItems = this.options.items,
            this.checkBrowser(),
            this.wrapperWidth = 0,
            this.checkVisible = null,
            this.setVars()
        },
        setVars: function() {
            if (0 === this.$elem.children().length)
                return !1;
            this.baseClass(),
            this.eventTypes(),
            this.$userItems = this.$elem.children(),
            this.itemsAmount = this.$userItems.length,
            this.wrapItems(),
            this.$owlItems = this.$elem.find(".owl-item"),
            this.$owlWrapper = this.$elem.find(".owl-wrapper"),
            this.playDirection = "next",
            this.prevItem = 0,
            this.prevArr = [0],
            this.currentItem = 0,
            this.customEvents(),
            this.onStartup()
        },
        onStartup: function() {
            this.updateItems(),
            this.calculateAll(),
            this.buildControls(),
            this.updateControls(),
            this.response(),
            this.moveEvents(),
            this.stopOnHover(),
            this.owlStatus(),
            !1 !== this.options.transitionStyle && this.transitionTypes(this.options.transitionStyle),
            !0 === this.options.autoPlay && (this.options.autoPlay = 5e3),
            this.play(),
            this.$elem.find(".owl-wrapper").css("display", "block"),
            this.$elem.is(":visible") ? this.$elem.css("opacity", 1) : this.watchVisibility(),
            this.onstartup = !1,
            this.eachMoveUpdate(),
            "function" == typeof this.options.afterInit && this.options.afterInit.apply(this, [this.$elem])
        },
        eachMoveUpdate: function() {
            !0 === this.options.lazyLoad && this.lazyLoad(),
            !0 === this.options.autoHeight && this.autoHeight(),
            this.onVisibleItems(),
            "function" == typeof this.options.afterAction && this.options.afterAction.apply(this, [this.$elem])
        },
        updateVars: function() {
            "function" == typeof this.options.beforeUpdate && this.options.beforeUpdate.apply(this, [this.$elem]),
            this.watchVisibility(),
            this.updateItems(),
            this.calculateAll(),
            this.updatePosition(),
            this.updateControls(),
            this.eachMoveUpdate(),
            "function" == typeof this.options.afterUpdate && this.options.afterUpdate.apply(this, [this.$elem])
        },
        reload: function() {
            var t = this;
            i.setTimeout(function() {
                t.updateVars()
            }, 0)
        },
        watchVisibility: function() {
            var t = this;
            if (!1 !== t.$elem.is(":visible"))
                return !1;
            t.$elem.css({
                opacity: 0
            }),
            i.clearInterval(t.autoPlayInterval),
            i.clearInterval(t.checkVisible),
            t.checkVisible = i.setInterval(function() {
                t.$elem.is(":visible") && (t.reload(),
                t.$elem.animate({
                    opacity: 1
                }, 200),
                i.clearInterval(t.checkVisible))
            }, 500)
        },
        wrapItems: function() {
            this.$userItems.wrapAll('<div class="owl-wrapper">').wrap('<div class="owl-item"></div>'),
            this.$elem.find(".owl-wrapper").wrap('<div class="owl-wrapper-outer">'),
            this.wrapperOuter = this.$elem.find(".owl-wrapper-outer"),
            this.$elem.css("display", "block")
        },
        baseClass: function() {
            var t = this.$elem.hasClass(this.options.baseClass)
              , i = this.$elem.hasClass(this.options.theme);
            t || this.$elem.addClass(this.options.baseClass),
            i || this.$elem.addClass(this.options.theme)
        },
        updateItems: function() {
            var i, s;
            if (!1 === this.options.responsive)
                return !1;
            if (!0 === this.options.singleItem)
                return this.options.items = this.orignalItems = 1,
                this.options.itemsCustom = !1,
                this.options.itemsDesktop = !1,
                this.options.itemsDesktopSmall = !1,
                this.options.itemsTablet = !1,
                this.options.itemsTabletSmall = !1,
                this.options.itemsMobile = !1;
            if ((i = t(this.options.responsiveBaseWidth).width()) > (this.options.itemsDesktop[0] || this.orignalItems) && (this.options.items = this.orignalItems),
            !1 !== this.options.itemsCustom)
                for (this.options.itemsCustom.sort(function(t, i) {
                    return t[0] - i[0]
                }),
                s = 0; s < this.options.itemsCustom.length; s += 1)
                    this.options.itemsCustom[s][0] <= i && (this.options.items = this.options.itemsCustom[s][1]);
            else
                i <= this.options.itemsDesktop[0] && !1 !== this.options.itemsDesktop && (this.options.items = this.options.itemsDesktop[1]),
                i <= this.options.itemsDesktopSmall[0] && !1 !== this.options.itemsDesktopSmall && (this.options.items = this.options.itemsDesktopSmall[1]),
                i <= this.options.itemsTablet[0] && !1 !== this.options.itemsTablet && (this.options.items = this.options.itemsTablet[1]),
                i <= this.options.itemsTabletSmall[0] && !1 !== this.options.itemsTabletSmall && (this.options.items = this.options.itemsTabletSmall[1]),
                i <= this.options.itemsMobile[0] && !1 !== this.options.itemsMobile && (this.options.items = this.options.itemsMobile[1]);
            this.options.items > this.itemsAmount && !0 === this.options.itemsScaleUp && (this.options.items = this.itemsAmount)
        },
        response: function() {
            var s, e, o = this;
            if (!0 !== o.options.responsive)
                return !1;
            e = t(i).width(),
            o.resizer = function() {
                t(i).width() !== e && (!1 !== o.options.autoPlay && i.clearInterval(o.autoPlayInterval),
                i.clearTimeout(s),
                s = i.setTimeout(function() {
                    e = t(i).width(),
                    o.updateVars()
                }, o.options.responsiveRefreshRate))
            }
            ,
            t(i).resize(o.resizer)
        },
        updatePosition: function() {
            this.jumpTo(this.currentItem),
            !1 !== this.options.autoPlay && this.checkAp()
        },
        appendItemsSizes: function() {
            var i = this
              , s = 0
              , e = i.itemsAmount - i.options.items;
            i.$owlItems.each(function(o) {
                var n = t(this);
                n.css({
                    width: i.itemWidth
                }).data("owl-item", Number(o)),
                0 != o % i.options.items && o !== e || o > e || (s += 1),
                n.data("owl-roundPages", s)
            })
        },
        appendWrapperSizes: function() {
            this.$owlWrapper.css({
                width: this.$owlItems.length * this.itemWidth * 2,
                left: 0
            }),
            this.appendItemsSizes()
        },
        calculateAll: function() {
            this.calculateWidth(),
            this.appendWrapperSizes(),
            this.loops(),
            this.max()
        },
        calculateWidth: function() {
            this.itemWidth = Math.round(this.$elem.width() / this.options.items)
        },
        max: function() {
            var t = -1 * (this.itemsAmount * this.itemWidth - this.options.items * this.itemWidth);
            return this.options.items > this.itemsAmount ? this.maximumPixels = t = this.maximumItem = 0 : (this.maximumItem = this.itemsAmount - this.options.items,
            this.maximumPixels = t),
            t
        },
        min: function() {
            return 0
        },
        loops: function() {
            var i, s, e = 0, o = 0;
            for (this.positionsInArray = [0],
            this.pagesInArray = [],
            i = 0; i < this.itemsAmount; i += 1)
                o += this.itemWidth,
                this.positionsInArray.push(-o),
                !0 === this.options.scrollPerPage && (s = t(this.$owlItems[i]),
                (s = s.data("owl-roundPages")) !== e && (this.pagesInArray[e] = this.positionsInArray[i],
                e = s))
        },
        buildControls: function() {
            !0 !== this.options.navigation && !0 !== this.options.pagination || (this.owlControls = t('<div class="owl-controls"/>').toggleClass("clickable", !this.browser.isTouch).appendTo(this.$elem)),
            !0 === this.options.pagination && this.buildPagination(),
            !0 === this.options.navigation && this.buildButtons()
        },
        buildButtons: function() {
            var i = this
              , s = t('<div class="owl-buttons"/>');
            i.owlControls.append(s),
            i.buttonPrev = t("<div/>", {
                class: "owl-prev",
                html: i.options.navigationText[0] || ""
            }),
            i.buttonNext = t("<div/>", {
                class: "owl-next",
                html: i.options.navigationText[1] || ""
            }),
            s.append(i.buttonPrev).append(i.buttonNext),
            s.on("touchstart.owlControls mousedown.owlControls", 'div[class^="owl"]', function(t) {
                t.preventDefault()
            }),
            s.on("touchend.owlControls mouseup.owlControls", 'div[class^="owl"]', function(s) {
                s.preventDefault(),
                t(this).hasClass("owl-next") ? i.next() : i.prev()
            })
        },
        buildPagination: function() {
            var i = this;
            i.paginationWrapper = t('<div class="owl-pagination"/>'),
            i.owlControls.append(i.paginationWrapper),
            i.paginationWrapper.on("touchend.owlControls mouseup.owlControls", ".owl-page", function(s) {
                s.preventDefault(),
                Number(t(this).data("owl-page")) !== i.currentItem && i.goTo(Number(t(this).data("owl-page")), !0)
            })
        },
        updatePagination: function() {
            var i, s, e, o, n, a;
            if (!1 === this.options.pagination)
                return !1;
            for (this.paginationWrapper.html(""),
            i = 0,
            s = this.itemsAmount - this.itemsAmount % this.options.items,
            o = 0; o < this.itemsAmount; o += 1)
                0 == o % this.options.items && (i += 1,
                s === o && (e = this.itemsAmount - this.options.items),
                n = t("<div/>", {
                    class: "owl-page"
                }),
                a = t("<span></span>", {
                    text: !0 === this.options.paginationNumbers ? i : "",
                    class: !0 === this.options.paginationNumbers ? "owl-numbers" : ""
                }),
                n.append(a),
                n.data("owl-page", s === o ? e : o),
                n.data("owl-roundPages", i),
                this.paginationWrapper.append(n));
            this.checkPagination()
        },
        checkPagination: function() {
            var i = this;
            if (!1 === i.options.pagination)
                return !1;
            i.paginationWrapper.find(".owl-page").each(function() {
                t(this).data("owl-roundPages") === t(i.$owlItems[i.currentItem]).data("owl-roundPages") && (i.paginationWrapper.find(".owl-page").removeClass("active"),
                t(this).addClass("active"))
            })
        },
        checkNavigation: function() {
            if (!1 === this.options.navigation)
                return !1;
            !1 === this.options.rewindNav && (0 === this.currentItem && 0 === this.maximumItem ? (this.buttonPrev.addClass("disabled"),
            this.buttonNext.addClass("disabled")) : 0 === this.currentItem && 0 !== this.maximumItem ? (this.buttonPrev.addClass("disabled"),
            this.buttonNext.removeClass("disabled")) : this.currentItem === this.maximumItem ? (this.buttonPrev.removeClass("disabled"),
            this.buttonNext.addClass("disabled")) : 0 !== this.currentItem && this.currentItem !== this.maximumItem && (this.buttonPrev.removeClass("disabled"),
            this.buttonNext.removeClass("disabled")))
        },
        updateControls: function() {
            this.updatePagination(),
            this.checkNavigation(),
            this.owlControls && (this.options.items >= this.itemsAmount ? this.owlControls.hide() : this.owlControls.show())
        },
        destroyControls: function() {
            this.owlControls && this.owlControls.remove()
        },
        next: function(t) {
            if (this.isTransition)
                return !1;
            if (this.currentItem += !0 === this.options.scrollPerPage ? this.options.items : 1,
            this.currentItem > this.maximumItem + (!0 === this.options.scrollPerPage ? this.options.items - 1 : 0)) {
                if (!0 !== this.options.rewindNav)
                    return this.currentItem = this.maximumItem,
                    !1;
                this.currentItem = 0,
                t = "rewind"
            }
            this.goTo(this.currentItem, t)
        },
        prev: function(t) {
            if (this.isTransition)
                return !1;
            if (this.currentItem = !0 === this.options.scrollPerPage && 0 < this.currentItem && this.currentItem < this.options.items ? 0 : this.currentItem - (!0 === this.options.scrollPerPage ? this.options.items : 1),
            0 > this.currentItem) {
                if (!0 !== this.options.rewindNav)
                    return this.currentItem = 0,
                    !1;
                this.currentItem = this.maximumItem,
                t = "rewind"
            }
            this.goTo(this.currentItem, t)
        },
        goTo: function(t, s, e) {
            var o = this;
            return !o.isTransition && ("function" == typeof o.options.beforeMove && o.options.beforeMove.apply(this, [o.$elem]),
            t >= o.maximumItem ? t = o.maximumItem : 0 >= t && (t = 0),
            o.currentItem = o.owl.currentItem = t,
            !1 !== o.options.transitionStyle && "drag" !== e && 1 === o.options.items && !0 === o.browser.support3d ? (o.swapSpeed(0),
            !0 === o.browser.support3d ? o.transition3d(o.positionsInArray[t]) : o.css2slide(o.positionsInArray[t], 1),
            o.afterGo(),
            o.singleItemTransition(),
            !1) : (t = o.positionsInArray[t],
            !0 === o.browser.support3d ? (o.isCss3Finish = !1,
            !0 === s ? (o.swapSpeed("paginationSpeed"),
            i.setTimeout(function() {
                o.isCss3Finish = !0
            }, o.options.paginationSpeed)) : "rewind" === s ? (o.swapSpeed(o.options.rewindSpeed),
            i.setTimeout(function() {
                o.isCss3Finish = !0
            }, o.options.rewindSpeed)) : (o.swapSpeed("slideSpeed"),
            i.setTimeout(function() {
                o.isCss3Finish = !0
            }, o.options.slideSpeed)),
            o.transition3d(t)) : !0 === s ? o.css2slide(t, o.options.paginationSpeed) : "rewind" === s ? o.css2slide(t, o.options.rewindSpeed) : o.css2slide(t, o.options.slideSpeed),
            void o.afterGo()))
        },
        jumpTo: function(t) {
            "function" == typeof this.options.beforeMove && this.options.beforeMove.apply(this, [this.$elem]),
            t >= this.maximumItem || -1 === t ? t = this.maximumItem : 0 >= t && (t = 0),
            this.swapSpeed(0),
            !0 === this.browser.support3d ? this.transition3d(this.positionsInArray[t]) : this.css2slide(this.positionsInArray[t], 1),
            this.currentItem = this.owl.currentItem = t,
            this.afterGo()
        },
        afterGo: function() {
            this.prevArr.push(this.currentItem),
            this.prevItem = this.owl.prevItem = this.prevArr[this.prevArr.length - 2],
            this.prevArr.shift(0),
            this.prevItem !== this.currentItem && (this.checkPagination(),
            this.checkNavigation(),
            this.eachMoveUpdate(),
            !1 !== this.options.autoPlay && this.checkAp()),
            "function" == typeof this.options.afterMove && this.prevItem !== this.currentItem && this.options.afterMove.apply(this, [this.$elem])
        },
        stop: function() {
            this.apStatus = "stop",
            i.clearInterval(this.autoPlayInterval)
        },
        checkAp: function() {
            "stop" !== this.apStatus && this.play()
        },
        play: function() {
            var t = this;
            if (t.apStatus = "play",
            !1 === t.options.autoPlay)
                return !1;
            i.clearInterval(t.autoPlayInterval),
            t.autoPlayInterval = i.setInterval(function() {
                t.next(!0)
            }, t.options.autoPlay)
        },
        swapSpeed: function(t) {
            "slideSpeed" === t ? this.$owlWrapper.css(this.addCssSpeed(this.options.slideSpeed)) : "paginationSpeed" === t ? this.$owlWrapper.css(this.addCssSpeed(this.options.paginationSpeed)) : "string" != typeof t && this.$owlWrapper.css(this.addCssSpeed(t))
        },
        addCssSpeed: function(t) {
            return {
                "-webkit-transition": "all " + t + "ms ease",
                "-moz-transition": "all " + t + "ms ease",
                "-o-transition": "all " + t + "ms ease",
                transition: "all " + t + "ms ease"
            }
        },
        removeTransition: function() {
            return {
                "-webkit-transition": "",
                "-moz-transition": "",
                "-o-transition": "",
                transition: ""
            }
        },
        doTranslate: function(t) {
            return {
                "-webkit-transform": "translate3d(" + t + "px, 0px, 0px)",
                "-moz-transform": "translate3d(" + t + "px, 0px, 0px)",
                "-o-transform": "translate3d(" + t + "px, 0px, 0px)",
                "-ms-transform": "translate3d(" + t + "px, 0px, 0px)",
                transform: "translate3d(" + t + "px, 0px,0px)"
            }
        },
        transition3d: function(t) {
            this.$owlWrapper.css(this.doTranslate(t))
        },
        css2move: function(t) {
            this.$owlWrapper.css({
                left: t
            })
        },
        css2slide: function(t, i) {
            var s = this;
            s.isCssFinish = !1,
            s.$owlWrapper.stop(!0, !0).animate({
                left: t
            }, {
                duration: i || s.options.slideSpeed,
                complete: function() {
                    s.isCssFinish = !0
                }
            })
        },
        checkBrowser: function() {
            var t = s.createElement("div");
            t.style.cssText = "  -moz-transform:translate3d(0px, 0px, 0px); -ms-transform:translate3d(0px, 0px, 0px); -o-transform:translate3d(0px, 0px, 0px); -webkit-transform:translate3d(0px, 0px, 0px); transform:translate3d(0px, 0px, 0px)",
            t = t.style.cssText.match(/translate3d\(0px, 0px, 0px\)/g),
            this.browser = {
                support3d: null !== t && 1 === t.length,
                isTouch: "ontouchstart"in i || i.navigator.msMaxTouchPoints
            }
        },
        moveEvents: function() {
            !1 === this.options.mouseDrag && !1 === this.options.touchDrag || (this.gestures(),
            this.disabledEvents())
        },
        eventTypes: function() {
            var t = ["s", "e", "x"];
            this.ev_types = {},
            !0 === this.options.mouseDrag && !0 === this.options.touchDrag ? t = ["touchstart.owl mousedown.owl", "touchmove.owl mousemove.owl", "touchend.owl touchcancel.owl mouseup.owl"] : !1 === this.options.mouseDrag && !0 === this.options.touchDrag ? t = ["touchstart.owl", "touchmove.owl", "touchend.owl touchcancel.owl"] : !0 === this.options.mouseDrag && !1 === this.options.touchDrag && (t = ["mousedown.owl", "mousemove.owl", "mouseup.owl"]),
            this.ev_types.start = t[0],
            this.ev_types.move = t[1],
            this.ev_types.end = t[2]
        },
        disabledEvents: function() {
            this.$elem.on("dragstart.owl", function(t) {
                t.preventDefault()
            }),
            this.$elem.on("mousedown.disableTextSelect", function(i) {
                return t(i.target).is("input, textarea, select, option")
            })
        },
        gestures: function() {
            function e(t) {
                if (void 0 !== t.touches)
                    return {
                        x: t.touches[0].pageX,
                        y: t.touches[0].pageY
                    };
                if (void 0 === t.touches) {
                    if (void 0 !== t.pageX)
                        return {
                            x: t.pageX,
                            y: t.pageY
                        };
                    if (void 0 === t.pageX)
                        return {
                            x: t.clientX,
                            y: t.clientY
                        }
                }
            }
            function o(i) {
                "on" === i ? (t(s).on(r.ev_types.move, n),
                t(s).on(r.ev_types.end, a)) : "off" === i && (t(s).off(r.ev_types.move),
                t(s).off(r.ev_types.end))
            }
            function n(o) {
                o = o.originalEvent || o || i.event,
                r.newPosX = e(o).x - l.offsetX,
                r.newPosY = e(o).y - l.offsetY,
                r.newRelativeX = r.newPosX - l.relativePos,
                "function" == typeof r.options.startDragging && !0 !== l.dragging && 0 !== r.newRelativeX && (l.dragging = !0,
                r.options.startDragging.apply(r, [r.$elem])),
                (8 < r.newRelativeX || -8 > r.newRelativeX) && !0 === r.browser.isTouch && (void 0 !== o.preventDefault ? o.preventDefault() : o.returnValue = !1,
                l.sliding = !0),
                (10 < r.newPosY || -10 > r.newPosY) && !1 === l.sliding && t(s).off("touchmove.owl"),
                r.newPosX = Math.max(Math.min(r.newPosX, r.newRelativeX / 5), r.maximumPixels + r.newRelativeX / 5),
                !0 === r.browser.support3d ? r.transition3d(r.newPosX) : r.css2move(r.newPosX)
            }
            function a(s) {
                var e;
                (s = s.originalEvent || s || i.event).target = s.target || s.srcElement,
                l.dragging = !1,
                !0 !== r.browser.isTouch && r.$owlWrapper.removeClass("grabbing"),
                r.dragDirection = 0 > r.newRelativeX ? r.owl.dragDirection = "left" : r.owl.dragDirection = "right",
                0 !== r.newRelativeX && (e = r.getNewPosition(),
                r.goTo(e, !1, "drag"),
                l.targetElement === s.target && !0 !== r.browser.isTouch && (t(s.target).on("click.disable", function(i) {
                    i.stopImmediatePropagation(),
                    i.stopPropagation(),
                    i.preventDefault(),
                    t(i.target).off("click.disable")
                }),
                s = t._data(s.target, "events").click,
                e = s.pop(),
                s.splice(0, 0, e))),
                o("off")
            }
            var r = this
              , l = {
                offsetX: 0,
                offsetY: 0,
                baseElWidth: 0,
                relativePos: 0,
                position: null,
                minSwipe: null,
                maxSwipe: null,
                sliding: null,
                dargging: null,
                targetElement: null
            };
            r.isCssFinish = !0,
            r.$elem.on(r.ev_types.start, ".owl-wrapper", function(s) {
                var n;
                if (3 === (s = s.originalEvent || s || i.event).which)
                    return !1;
                if (!(r.itemsAmount <= r.options.items)) {
                    if (!1 === r.isCssFinish && !r.options.dragBeforeAnimFinish || !1 === r.isCss3Finish && !r.options.dragBeforeAnimFinish)
                        return !1;
                    !1 !== r.options.autoPlay && i.clearInterval(r.autoPlayInterval),
                    !0 === r.browser.isTouch || r.$owlWrapper.hasClass("grabbing") || r.$owlWrapper.addClass("grabbing"),
                    r.newPosX = 0,
                    r.newRelativeX = 0,
                    t(this).css(r.removeTransition()),
                    n = t(this).position(),
                    l.relativePos = n.left,
                    l.offsetX = e(s).x - n.left,
                    l.offsetY = e(s).y - n.top,
                    o("on"),
                    l.sliding = !1,
                    l.targetElement = s.target || s.srcElement
                }
            })
        },
        getNewPosition: function() {
            var t = this.closestItem();
            return t > this.maximumItem ? t = this.currentItem = this.maximumItem : 0 <= this.newPosX && (this.currentItem = t = 0),
            t
        },
        closestItem: function() {
            var i = this
              , s = !0 === i.options.scrollPerPage ? i.pagesInArray : i.positionsInArray
              , e = i.newPosX
              , o = null;
            return t.each(s, function(n, a) {
                e - i.itemWidth / 20 > s[n + 1] && e - i.itemWidth / 20 < a && "left" === i.moveDirection() ? (o = a,
                i.currentItem = !0 === i.options.scrollPerPage ? t.inArray(o, i.positionsInArray) : n) : e + i.itemWidth / 20 < a && e + i.itemWidth / 20 > (s[n + 1] || s[n] - i.itemWidth) && "right" === i.moveDirection() && (!0 === i.options.scrollPerPage ? (o = s[n + 1] || s[s.length - 1],
                i.currentItem = t.inArray(o, i.positionsInArray)) : (o = s[n + 1],
                i.currentItem = n + 1))
            }),
            i.currentItem
        },
        moveDirection: function() {
            var t;
            return 0 > this.newRelativeX ? (t = "right",
            this.playDirection = "next") : (t = "left",
            this.playDirection = "prev"),
            t
        },
        customEvents: function() {
            var t = this;
            t.$elem.on("owl.next", function() {
                t.next()
            }),
            t.$elem.on("owl.prev", function() {
                t.prev()
            }),
            t.$elem.on("owl.play", function(i, s) {
                t.options.autoPlay = s,
                t.play(),
                t.hoverStatus = "play"
            }),
            t.$elem.on("owl.stop", function() {
                t.stop(),
                t.hoverStatus = "stop"
            }),
            t.$elem.on("owl.goTo", function(i, s) {
                t.goTo(s)
            }),
            t.$elem.on("owl.jumpTo", function(i, s) {
                t.jumpTo(s)
            })
        },
        stopOnHover: function() {
            var t = this;
            !0 === t.options.stopOnHover && !0 !== t.browser.isTouch && !1 !== t.options.autoPlay && (t.$elem.on("mouseover", function() {
                t.stop()
            }),
            t.$elem.on("mouseout", function() {
                "stop" !== t.hoverStatus && t.play()
            }))
        },
        lazyLoad: function() {
            var i, s, e, o;
            if (!1 === this.options.lazyLoad)
                return !1;
            for (i = 0; i < this.itemsAmount; i += 1)
                "loaded" !== (s = t(this.$owlItems[i])).data("owl-loaded") && (e = s.data("owl-item"),
                o = s.find(".lazyOwl"),
                "string" != typeof o.data("src") ? s.data("owl-loaded", "loaded") : (void 0 === s.data("owl-loaded") && (o.hide(),
                s.addClass("loading").data("owl-loaded", "checked")),
                (!0 !== this.options.lazyFollow || e >= this.currentItem) && e < this.currentItem + this.options.items && o.length && this.lazyPreload(s, o)))
        },
        lazyPreload: function(t, s) {
            function e() {
                t.data("owl-loaded", "loaded").removeClass("loading"),
                s.removeAttr("data-src"),
                "fade" === a.options.lazyEffect ? s.fadeIn(400) : s.show(),
                "function" == typeof a.options.afterLazyLoad && a.options.afterLazyLoad.apply(this, [a.$elem])
            }
            function o() {
                r += 1,
                a.completeImg(s.get(0)) || !0 === n ? e() : 100 >= r ? i.setTimeout(o, 100) : e()
            }
            var n, a = this, r = 0;
            "DIV" === s.prop("tagName") ? (s.css("background-image", "url(" + s.data("src") + ")"),
            n = !0) : s[0].src = s.data("src"),
            o()
        },
        autoHeight: function() {
            function s() {
                var s = t(n.$owlItems[n.currentItem]).height();
                n.wrapperOuter.css("height", s + "px"),
                n.wrapperOuter.hasClass("autoHeight") || i.setTimeout(function() {
                    n.wrapperOuter.addClass("autoHeight")
                }, 0)
            }
            function e() {
                o += 1,
                n.completeImg(a.get(0)) ? s() : 100 >= o ? i.setTimeout(e, 100) : n.wrapperOuter.css("height", "")
            }
            var o, n = this, a = t(n.$owlItems[n.currentItem]).find("img");
            void 0 !== a.get(0) ? (o = 0,
            e()) : s()
        },
        completeImg: function(t) {
            return !(!t.complete || void 0 !== t.naturalWidth && 0 === t.naturalWidth)
        },
        onVisibleItems: function() {
            var i;
            for (!0 === this.options.addClassActive && this.$owlItems.removeClass("active"),
            this.visibleItems = [],
            i = this.currentItem; i < this.currentItem + this.options.items; i += 1)
                this.visibleItems.push(i),
                !0 === this.options.addClassActive && t(this.$owlItems[i]).addClass("active");
            this.owl.visibleItems = this.visibleItems
        },
        transitionTypes: function(t) {
            this.outClass = "owl-" + t + "-out",
            this.inClass = "owl-" + t + "-in"
        },
        singleItemTransition: function() {
            var t = this
              , i = t.outClass
              , s = t.inClass
              , e = t.$owlItems.eq(t.currentItem)
              , o = t.$owlItems.eq(t.prevItem)
              , n = Math.abs(t.positionsInArray[t.currentItem]) + t.positionsInArray[t.prevItem]
              , a = Math.abs(t.positionsInArray[t.currentItem]) + t.itemWidth / 2;
            t.isTransition = !0,
            t.$owlWrapper.addClass("owl-origin").css({
                "-webkit-transform-origin": a + "px",
                "-moz-perspective-origin": a + "px",
                "perspective-origin": a + "px"
            }),
            o.css({
                position: "relative",
                left: n + "px"
            }).addClass(i).on("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend", function() {
                t.endPrev = !0,
                o.off("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend"),
                t.clearTransStyle(o, i)
            }),
            e.addClass(s).on("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend", function() {
                t.endCurrent = !0,
                e.off("webkitAnimationEnd oAnimationEnd MSAnimationEnd animationend"),
                t.clearTransStyle(e, s)
            })
        },
        clearTransStyle: function(t, i) {
            t.css({
                position: "",
                left: ""
            }).removeClass(i),
            this.endPrev && this.endCurrent && (this.$owlWrapper.removeClass("owl-origin"),
            this.isTransition = this.endCurrent = this.endPrev = !1)
        },
        owlStatus: function() {
            this.owl = {
                userOptions: this.userOptions,
                baseElement: this.$elem,
                userItems: this.$userItems,
                owlItems: this.$owlItems,
                currentItem: this.currentItem,
                prevItem: this.prevItem,
                visibleItems: this.visibleItems,
                isTouch: this.browser.isTouch,
                browser: this.browser,
                dragDirection: this.dragDirection
            }
        },
        clearEvents: function() {
            this.$elem.off(".owl owl mousedown.disableTextSelect"),
            t(s).off(".owl owl"),
            t(i).off("resize", this.resizer)
        },
        unWrap: function() {
            0 !== this.$elem.children().length && (this.$owlWrapper.unwrap(),
            this.$userItems.unwrap().unwrap(),
            this.owlControls && this.owlControls.remove()),
            this.clearEvents(),
            this.$elem.attr("style", this.$elem.data("owl-originalStyles") || "").attr("class", this.$elem.data("owl-originalClasses"))
        },
        destroy: function() {
            this.stop(),
            i.clearInterval(this.checkVisible),
            this.unWrap(),
            this.$elem.removeData()
        },
        reinit: function(i) {
            i = t.extend({}, this.userOptions, i),
            this.unWrap(),
            this.init(i, this.$elem)
        },
        addItem: function(t, i) {
            var s;
            return !!t && (0 === this.$elem.children().length ? (this.$elem.append(t),
            this.setVars(),
            !1) : (this.unWrap(),
            (s = void 0 === i || -1 === i ? -1 : i) >= this.$userItems.length || -1 === s ? this.$userItems.eq(-1).after(t) : this.$userItems.eq(s).before(t),
            void this.setVars()))
        },
        removeItem: function(t) {
            if (0 === this.$elem.children().length)
                return !1;
            t = void 0 === t || -1 === t ? -1 : t,
            this.unWrap(),
            this.$userItems.eq(t).remove(),
            this.setVars()
        }
    };
    t.fn.owlCarousel = function(i) {
        return this.each(function() {
            if (!0 === t(this).data("owl-init"))
                return !1;
            t(this).data("owl-init", !0);
            var s = Object.create(e);
            s.init(i, this),
            t.data(this, "owlCarousel", s)
        })
    }
    ,
    t.fn.owlCarousel.options = {
        items: 5,
        itemsCustom: !1,
        itemsDesktop: [1199, 4],
        itemsDesktopSmall: [979, 3],
        itemsTablet: [768, 2],
        itemsTabletSmall: !1,
        itemsMobile: [479, 1],
        singleItem: !1,
        itemsScaleUp: !1,
        slideSpeed: 200,
        paginationSpeed: 800,
        rewindSpeed: 1e3,
        autoPlay: !1,
        stopOnHover: !1,
        navigation: !1,
        navigationText: ["prev", "next"],
        rewindNav: !0,
        scrollPerPage: !1,
        pagination: !0,
        paginationNumbers: !1,
        responsive: !0,
        responsiveRefreshRate: 200,
        responsiveBaseWidth: i,
        baseClass: "owl-carousel",
        theme: "owl-theme",
        lazyLoad: !1,
        lazyFollow: !0,
        lazyEffect: "fade",
        autoHeight: !1,
        jsonPath: !1,
        jsonSuccess: !1,
        dragBeforeAnimFinish: !0,
        mouseDrag: !0,
        touchDrag: !0,
        addClassActive: !1,
        transitionStyle: !1,
        beforeUpdate: !1,
        afterUpdate: !1,
        beforeInit: !1,
        afterInit: !1,
        beforeMove: !1,
        afterMove: !1,
        afterAction: !1,
        startDragging: !1,
        afterLazyLoad: !1
    }
}(jQuery, window, document);
function hexToRgb(e) {
    var a = /^#?([a-f\d])([a-f\d])([a-f\d])$/i;
    e = e.replace(a, function(e, a, t, i) {
        return a + a + t + t + i + i
    });
    var t = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(e);
    return t ? {
        r: parseInt(t[1], 16),
        g: parseInt(t[2], 16),
        b: parseInt(t[3], 16)
    } : null
}
function clamp(e, a, t) {
    return Math.min(Math.max(e, a), t)
}
function isInArray(e, a) {
    return a.indexOf(e) > -1
}
var pJS = function(e, a) {
    var t = document.querySelector("#" + e + " > .particles-js-canvas-el");
    this.pJS = {
        canvas: {
            el: t,
            w: t.offsetWidth,
            h: t.offsetHeight
        },
        particles: {
            number: {
                value: 400,
                density: {
                    enable: !0,
                    value_area: 800
                }
            },
            color: {
                value: "#fff"
            },
            shape: {
                type: "circle",
                stroke: {
                    width: 0,
                    color: "#ff0000"
                },
                polygon: {
                    nb_sides: 5
                },
                image: {
                    src: "",
                    width: 100,
                    height: 100
                }
            },
            opacity: {
                value: 1,
                random: !1,
                anim: {
                    enable: !1,
                    speed: 2,
                    opacity_min: 0,
                    sync: !1
                }
            },
            size: {
                value: 20,
                random: !1,
                anim: {
                    enable: !1,
                    speed: 20,
                    size_min: 0,
                    sync: !1
                }
            },
            line_linked: {
                enable: !0,
                distance: 100,
                color: "#fff",
                opacity: 1,
                width: 1
            },
            move: {
                enable: !0,
                speed: 2,
                direction: "none",
                random: !1,
                straight: !1,
                out_mode: "out",
                bounce: !1,
                attract: {
                    enable: !1,
                    rotateX: 3e3,
                    rotateY: 3e3
                }
            },
            array: []
        },
        interactivity: {
            detect_on: "canvas",
            events: {
                onhover: {
                    enable: !0,
                    mode: "grab"
                },
                onclick: {
                    enable: !0,
                    mode: "push"
                },
                resize: !0
            },
            modes: {
                grab: {
                    distance: 100,
                    line_linked: {
                        opacity: 1
                    }
                },
                bubble: {
                    distance: 200,
                    size: 80,
                    duration: .4
                },
                repulse: {
                    distance: 200,
                    duration: .4
                },
                push: {
                    particles_nb: 4
                },
                remove: {
                    particles_nb: 2
                }
            },
            mouse: {}
        },
        retina_detect: !1,
        fn: {
            interact: {},
            modes: {},
            vendors: {}
        },
        tmp: {}
    };
    var i = this.pJS;
    a && Object.deepExtend(i, a),
    i.tmp.obj = {
        size_value: i.particles.size.value,
        size_anim_speed: i.particles.size.anim.speed,
        move_speed: i.particles.move.speed,
        line_linked_distance: i.particles.line_linked.distance,
        line_linked_width: i.particles.line_linked.width,
        mode_grab_distance: i.interactivity.modes.grab.distance,
        mode_bubble_distance: i.interactivity.modes.bubble.distance,
        mode_bubble_size: i.interactivity.modes.bubble.size,
        mode_repulse_distance: i.interactivity.modes.repulse.distance
    },
    i.fn.retinaInit = function() {
        i.retina_detect && window.devicePixelRatio > 1 ? (i.canvas.pxratio = window.devicePixelRatio,
        i.tmp.retina = !0) : (i.canvas.pxratio = 1,
        i.tmp.retina = !1),
        i.canvas.w = i.canvas.el.offsetWidth * i.canvas.pxratio,
        i.canvas.h = i.canvas.el.offsetHeight * i.canvas.pxratio,
        i.particles.size.value = i.tmp.obj.size_value * i.canvas.pxratio,
        i.particles.size.anim.speed = i.tmp.obj.size_anim_speed * i.canvas.pxratio,
        i.particles.move.speed = i.tmp.obj.move_speed * i.canvas.pxratio,
        i.particles.line_linked.distance = i.tmp.obj.line_linked_distance * i.canvas.pxratio,
        i.interactivity.modes.grab.distance = i.tmp.obj.mode_grab_distance * i.canvas.pxratio,
        i.interactivity.modes.bubble.distance = i.tmp.obj.mode_bubble_distance * i.canvas.pxratio,
        i.particles.line_linked.width = i.tmp.obj.line_linked_width * i.canvas.pxratio,
        i.interactivity.modes.bubble.size = i.tmp.obj.mode_bubble_size * i.canvas.pxratio,
        i.interactivity.modes.repulse.distance = i.tmp.obj.mode_repulse_distance * i.canvas.pxratio
    }
    ,
    i.fn.canvasInit = function() {
        i.canvas.ctx = i.canvas.el.getContext("2d")
    }
    ,
    i.fn.canvasSize = function() {
        i.canvas.el.width = i.canvas.w,
        i.canvas.el.height = i.canvas.h,
        i && i.interactivity.events.resize && window.addEventListener("resize", function() {
            i.canvas.w = i.canvas.el.offsetWidth,
            i.canvas.h = i.canvas.el.offsetHeight,
            i.tmp.retina && (i.canvas.w *= i.canvas.pxratio,
            i.canvas.h *= i.canvas.pxratio),
            i.canvas.el.width = i.canvas.w,
            i.canvas.el.height = i.canvas.h,
            i.particles.move.enable || (i.fn.particlesEmpty(),
            i.fn.particlesCreate(),
            i.fn.particlesDraw(),
            i.fn.vendors.densityAutoParticles()),
            i.fn.vendors.densityAutoParticles()
        })
    }
    ,
    i.fn.canvasPaint = function() {
        i.canvas.ctx.fillRect(0, 0, i.canvas.w, i.canvas.h)
    }
    ,
    i.fn.canvasClear = function() {
        i.canvas.ctx.clearRect(0, 0, i.canvas.w, i.canvas.h)
    }
    ,
    i.fn.particle = function(e, a, t) {
        if (this.radius = (i.particles.size.random ? Math.random() : 1) * i.particles.size.value,
        i.particles.size.anim.enable && (this.size_status = !1,
        this.vs = i.particles.size.anim.speed / 100,
        i.particles.size.anim.sync || (this.vs = this.vs * Math.random())),
        this.x = t ? t.x : Math.random() * i.canvas.w,
        this.y = t ? t.y : Math.random() * i.canvas.h,
        this.x > i.canvas.w - 2 * this.radius ? this.x = this.x - this.radius : this.x < 2 * this.radius && (this.x = this.x + this.radius),
        this.y > i.canvas.h - 2 * this.radius ? this.y = this.y - this.radius : this.y < 2 * this.radius && (this.y = this.y + this.radius),
        i.particles.move.bounce && i.fn.vendors.checkOverlap(this, t),
        this.color = {},
        "object" == typeof e.value)
            if (e.value instanceof Array) {
                var s = e.value[Math.floor(Math.random() * i.particles.color.value.length)];
                this.color.rgb = hexToRgb(s)
            } else
                void 0 != e.value.r && void 0 != e.value.g && void 0 != e.value.b && (this.color.rgb = {
                    r: e.value.r,
                    g: e.value.g,
                    b: e.value.b
                }),
                void 0 != e.value.h && void 0 != e.value.s && void 0 != e.value.l && (this.color.hsl = {
                    h: e.value.h,
                    s: e.value.s,
                    l: e.value.l
                });
        else
            "random" == e.value ? this.color.rgb = {
                r: Math.floor(256 * Math.random()) + 0,
                g: Math.floor(256 * Math.random()) + 0,
                b: Math.floor(256 * Math.random()) + 0
            } : "string" == typeof e.value && (this.color = e,
            this.color.rgb = hexToRgb(this.color.value));
        this.opacity = (i.particles.opacity.random ? Math.random() : 1) * i.particles.opacity.value,
        i.particles.opacity.anim.enable && (this.opacity_status = !1,
        this.vo = i.particles.opacity.anim.speed / 100,
        i.particles.opacity.anim.sync || (this.vo = this.vo * Math.random()));
        var n = {};
        switch (i.particles.move.direction) {
        case "top":
            n = {
                x: 0,
                y: -1
            };
            break;
        case "top-right":
            n = {
                x: .5,
                y: -.5
            };
            break;
        case "right":
            n = {
                x: 1,
                y: -0
            };
            break;
        case "bottom-right":
            n = {
                x: .5,
                y: .5
            };
            break;
        case "bottom":
            n = {
                x: 0,
                y: 1
            };
            break;
        case "bottom-left":
            n = {
                x: -.5,
                y: 1
            };
            break;
        case "left":
            n = {
                x: -1,
                y: 0
            };
            break;
        case "top-left":
            n = {
                x: -.5,
                y: -.5
            };
            break;
        default:
            n = {
                x: 0,
                y: 0
            }
        }
        i.particles.move.straight ? (this.vx = n.x,
        this.vy = n.y,
        i.particles.move.random && (this.vx = this.vx * Math.random(),
        this.vy = this.vy * Math.random())) : (this.vx = n.x + Math.random() - .5,
        this.vy = n.y + Math.random() - .5),
        this.vx_i = this.vx,
        this.vy_i = this.vy;
        var r = i.particles.shape.type;
        if ("object" == typeof r) {
            if (r instanceof Array) {
                var c = r[Math.floor(Math.random() * r.length)];
                this.shape = c
            }
        } else
            this.shape = r;
        if ("image" == this.shape) {
            var o = i.particles.shape;
            this.img = {
                src: o.image.src,
                ratio: o.image.width / o.image.height
            },
            this.img.ratio || (this.img.ratio = 1),
            "svg" == i.tmp.img_type && void 0 != i.tmp.source_svg && (i.fn.vendors.createSvgImg(this),
            i.tmp.pushing && (this.img.loaded = !1))
        }
    }
    ,
    i.fn.particle.prototype.draw = function() {
        var e = this;
        if (void 0 != e.radius_bubble)
            a = e.radius_bubble;
        else
            var a = e.radius;
        if (void 0 != e.opacity_bubble)
            t = e.opacity_bubble;
        else
            var t = e.opacity;
        if (e.color.rgb)
            s = "rgba(" + e.color.rgb.r + "," + e.color.rgb.g + "," + e.color.rgb.b + "," + t + ")";
        else
            var s = "hsla(" + e.color.hsl.h + "," + e.color.hsl.s + "%," + e.color.hsl.l + "%," + t + ")";
        switch (i.canvas.ctx.fillStyle = s,
        i.canvas.ctx.beginPath(),
        e.shape) {
        case "circle":
            i.canvas.ctx.arc(e.x, e.y, a, 0, 2 * Math.PI, !1);
            break;
        case "edge":
            i.canvas.ctx.rect(e.x - a, e.y - a, 2 * a, 2 * a);
            break;
        case "triangle":
            i.fn.vendors.drawShape(i.canvas.ctx, e.x - a, e.y + a / 1.66, 2 * a, 3, 2);
            break;
        case "polygon":
            i.fn.vendors.drawShape(i.canvas.ctx, e.x - a / (i.particles.shape.polygon.nb_sides / 3.5), e.y - a / .76, 2.66 * a / (i.particles.shape.polygon.nb_sides / 3), i.particles.shape.polygon.nb_sides, 1);
            break;
        case "star":
            i.fn.vendors.drawShape(i.canvas.ctx, e.x - 2 * a / (i.particles.shape.polygon.nb_sides / 4), e.y - a / 1.52, 2 * a * 2.66 / (i.particles.shape.polygon.nb_sides / 3), i.particles.shape.polygon.nb_sides, 2);
            break;
        case "image":
            if ("svg" == i.tmp.img_type)
                n = e.img.obj;
            else
                var n = i.tmp.img_obj;
            n && i.canvas.ctx.drawImage(n, e.x - a, e.y - a, 2 * a, 2 * a / e.img.ratio)
        }
        i.canvas.ctx.closePath(),
        i.particles.shape.stroke.width > 0 && (i.canvas.ctx.strokeStyle = i.particles.shape.stroke.color,
        i.canvas.ctx.lineWidth = i.particles.shape.stroke.width,
        i.canvas.ctx.stroke()),
        i.canvas.ctx.fill()
    }
    ,
    i.fn.particlesCreate = function() {
        for (var e = 0; e < i.particles.number.value; e++)
            i.particles.array.push(new i.fn.particle(i.particles.color,i.particles.opacity.value))
    }
    ,
    i.fn.particlesUpdate = function() {
        for (var e = 0; e < i.particles.array.length; e++) {
            var a = i.particles.array[e];
            if (i.particles.move.enable) {
                var t = i.particles.move.speed / 2;
                a.x += a.vx * t,
                a.y += a.vy * t
            }
            if (i.particles.opacity.anim.enable && (1 == a.opacity_status ? (a.opacity >= i.particles.opacity.value && (a.opacity_status = !1),
            a.opacity += a.vo) : (a.opacity <= i.particles.opacity.anim.opacity_min && (a.opacity_status = !0),
            a.opacity -= a.vo),
            a.opacity < 0 && (a.opacity = 0)),
            i.particles.size.anim.enable && (1 == a.size_status ? (a.radius >= i.particles.size.value && (a.size_status = !1),
            a.radius += a.vs) : (a.radius <= i.particles.size.anim.size_min && (a.size_status = !0),
            a.radius -= a.vs),
            a.radius < 0 && (a.radius = 0)),
            "bounce" == i.particles.move.out_mode)
                s = {
                    x_left: a.radius,
                    x_right: i.canvas.w,
                    y_top: a.radius,
                    y_bottom: i.canvas.h
                };
            else
                var s = {
                    x_left: -a.radius,
                    x_right: i.canvas.w + a.radius,
                    y_top: -a.radius,
                    y_bottom: i.canvas.h + a.radius
                };
            switch (a.x - a.radius > i.canvas.w ? (a.x = s.x_left,
            a.y = Math.random() * i.canvas.h) : a.x + a.radius < 0 && (a.x = s.x_right,
            a.y = Math.random() * i.canvas.h),
            a.y - a.radius > i.canvas.h ? (a.y = s.y_top,
            a.x = Math.random() * i.canvas.w) : a.y + a.radius < 0 && (a.y = s.y_bottom,
            a.x = Math.random() * i.canvas.w),
            i.particles.move.out_mode) {
            case "bounce":
                a.x + a.radius > i.canvas.w ? a.vx = -a.vx : a.x - a.radius < 0 && (a.vx = -a.vx),
                a.y + a.radius > i.canvas.h ? a.vy = -a.vy : a.y - a.radius < 0 && (a.vy = -a.vy)
            }
            if (isInArray("grab", i.interactivity.events.onhover.mode) && i.fn.modes.grabParticle(a),
            (isInArray("bubble", i.interactivity.events.onhover.mode) || isInArray("bubble", i.interactivity.events.onclick.mode)) && i.fn.modes.bubbleParticle(a),
            (isInArray("repulse", i.interactivity.events.onhover.mode) || isInArray("repulse", i.interactivity.events.onclick.mode)) && i.fn.modes.repulseParticle(a),
            i.particles.line_linked.enable || i.particles.move.attract.enable)
                for (var n = e + 1; n < i.particles.array.length; n++) {
                    var r = i.particles.array[n];
                    i.particles.line_linked.enable && i.fn.interact.linkParticles(a, r),
                    i.particles.move.attract.enable && i.fn.interact.attractParticles(a, r),
                    i.particles.move.bounce && i.fn.interact.bounceParticles(a, r)
                }
        }
    }
    ,
    i.fn.particlesDraw = function() {
        i.canvas.ctx.clearRect(0, 0, i.canvas.w, i.canvas.h),
        i.fn.particlesUpdate();
        for (var e = 0; e < i.particles.array.length; e++)
            i.particles.array[e].draw()
    }
    ,
    i.fn.particlesEmpty = function() {
        i.particles.array = []
    }
    ,
    i.fn.particlesRefresh = function() {
        cancelRequestAnimFrame(i.fn.checkAnimFrame),
        cancelRequestAnimFrame(i.fn.drawAnimFrame),
        i.tmp.source_svg = void 0,
        i.tmp.img_obj = void 0,
        i.tmp.count_svg = 0,
        i.fn.particlesEmpty(),
        i.fn.canvasClear(),
        i.fn.vendors.start()
    }
    ,
    i.fn.interact.linkParticles = function(e, a) {
        var t = e.x - a.x
          , s = e.y - a.y
          , n = Math.sqrt(t * t + s * s);
        if (n <= i.particles.line_linked.distance) {
            var r = i.particles.line_linked.opacity - n / (1 / i.particles.line_linked.opacity) / i.particles.line_linked.distance;
            if (r > 0) {
                var c = i.particles.line_linked.color_rgb_line;
                i.canvas.ctx.strokeStyle = "rgba(" + c.r + "," + c.g + "," + c.b + "," + r + ")",
                i.canvas.ctx.lineWidth = i.particles.line_linked.width,
                i.canvas.ctx.beginPath(),
                i.canvas.ctx.moveTo(e.x, e.y),
                i.canvas.ctx.lineTo(a.x, a.y),
                i.canvas.ctx.stroke(),
                i.canvas.ctx.closePath()
            }
        }
    }
    ,
    i.fn.interact.attractParticles = function(e, a) {
        var t = e.x - a.x
          , s = e.y - a.y;
        if (Math.sqrt(t * t + s * s) <= i.particles.line_linked.distance) {
            var n = t / (1e3 * i.particles.move.attract.rotateX)
              , r = s / (1e3 * i.particles.move.attract.rotateY);
            e.vx -= n,
            e.vy -= r,
            a.vx += n,
            a.vy += r
        }
    }
    ,
    i.fn.interact.bounceParticles = function(e, a) {
        var t = e.x - a.x
          , i = e.y - a.y
          , s = Math.sqrt(t * t + i * i);
        e.radius + a.radius >= s && (e.vx = -e.vx,
        e.vy = -e.vy,
        a.vx = -a.vx,
        a.vy = -a.vy)
    }
    ,
    i.fn.modes.pushParticles = function(e, a) {
        i.tmp.pushing = !0;
        for (var t = 0; e > t; t++)
            i.particles.array.push(new i.fn.particle(i.particles.color,i.particles.opacity.value,{
                x: a ? a.pos_x : Math.random() * i.canvas.w,
                y: a ? a.pos_y : Math.random() * i.canvas.h
            })),
            t == e - 1 && (i.particles.move.enable || i.fn.particlesDraw(),
            i.tmp.pushing = !1)
    }
    ,
    i.fn.modes.removeParticles = function(e) {
        i.particles.array.splice(0, e),
        i.particles.move.enable || i.fn.particlesDraw()
    }
    ,
    i.fn.modes.bubbleParticle = function(e) {
        function a() {
            e.opacity_bubble = e.opacity,
            e.radius_bubble = e.radius
        }
        function t(a, t, s, n, r) {
            if (a != t)
                if (i.tmp.bubble_duration_end)
                    void 0 != s && (o = a + (a - (n - p * (n - a) / i.interactivity.modes.bubble.duration)),
                    "size" == r && (e.radius_bubble = o),
                    "opacity" == r && (e.opacity_bubble = o));
                else if (v <= i.interactivity.modes.bubble.distance) {
                    if (void 0 != s)
                        c = s;
                    else
                        var c = n;
                    if (c != a) {
                        var o = n - p * (n - a) / i.interactivity.modes.bubble.duration;
                        "size" == r && (e.radius_bubble = o),
                        "opacity" == r && (e.opacity_bubble = o)
                    }
                } else
                    "size" == r && (e.radius_bubble = void 0),
                    "opacity" == r && (e.opacity_bubble = void 0)
        }
        if (i.interactivity.events.onhover.enable && isInArray("bubble", i.interactivity.events.onhover.mode)) {
            var s = e.x - i.interactivity.mouse.pos_x
              , n = e.y - i.interactivity.mouse.pos_y
              , r = 1 - (v = Math.sqrt(s * s + n * n)) / i.interactivity.modes.bubble.distance;
            if (v <= i.interactivity.modes.bubble.distance) {
                if (r >= 0 && "mousemove" == i.interactivity.status) {
                    if (i.interactivity.modes.bubble.size != i.particles.size.value)
                        if (i.interactivity.modes.bubble.size > i.particles.size.value)
                            (o = e.radius + i.interactivity.modes.bubble.size * r) >= 0 && (e.radius_bubble = o);
                        else {
                            var c = e.radius - i.interactivity.modes.bubble.size
                              , o = e.radius - c * r;
                            e.radius_bubble = o > 0 ? o : 0
                        }
                    if (i.interactivity.modes.bubble.opacity != i.particles.opacity.value)
                        if (i.interactivity.modes.bubble.opacity > i.particles.opacity.value)
                            (l = i.interactivity.modes.bubble.opacity * r) > e.opacity && l <= i.interactivity.modes.bubble.opacity && (e.opacity_bubble = l);
                        else {
                            var l = e.opacity - (i.particles.opacity.value - i.interactivity.modes.bubble.opacity) * r;
                            l < e.opacity && l >= i.interactivity.modes.bubble.opacity && (e.opacity_bubble = l)
                        }
                }
            } else
                a();
            "mouseleave" == i.interactivity.status && a()
        } else if (i.interactivity.events.onclick.enable && isInArray("bubble", i.interactivity.events.onclick.mode)) {
            if (i.tmp.bubble_clicking) {
                var s = e.x - i.interactivity.mouse.click_pos_x
                  , n = e.y - i.interactivity.mouse.click_pos_y
                  , v = Math.sqrt(s * s + n * n)
                  , p = ((new Date).getTime() - i.interactivity.mouse.click_time) / 1e3;
                p > i.interactivity.modes.bubble.duration && (i.tmp.bubble_duration_end = !0),
                p > 2 * i.interactivity.modes.bubble.duration && (i.tmp.bubble_clicking = !1,
                i.tmp.bubble_duration_end = !1)
            }
            i.tmp.bubble_clicking && (t(i.interactivity.modes.bubble.size, i.particles.size.value, e.radius_bubble, e.radius, "size"),
            t(i.interactivity.modes.bubble.opacity, i.particles.opacity.value, e.opacity_bubble, e.opacity, "opacity"))
        }
    }
    ,
    i.fn.modes.repulseParticle = function(e) {
        if (i.interactivity.events.onhover.enable && isInArray("repulse", i.interactivity.events.onhover.mode) && "mousemove" == i.interactivity.status) {
            var a = e.x - i.interactivity.mouse.pos_x
              , t = e.y - i.interactivity.mouse.pos_y
              , s = Math.sqrt(a * a + t * t)
              , n = {
                x: a / s,
                y: t / s
            }
              , r = clamp(1 / (o = i.interactivity.modes.repulse.distance) * (-1 * Math.pow(s / o, 2) + 1) * o * 100, 0, 50)
              , c = {
                x: e.x + n.x * r,
                y: e.y + n.y * r
            };
            "bounce" == i.particles.move.out_mode ? (c.x - e.radius > 0 && c.x + e.radius < i.canvas.w && (e.x = c.x),
            c.y - e.radius > 0 && c.y + e.radius < i.canvas.h && (e.y = c.y)) : (e.x = c.x,
            e.y = c.y)
        } else if (i.interactivity.events.onclick.enable && isInArray("repulse", i.interactivity.events.onclick.mode))
            if (i.tmp.repulse_finish || ++i.tmp.repulse_count == i.particles.array.length && (i.tmp.repulse_finish = !0),
            i.tmp.repulse_clicking) {
                var o = Math.pow(i.interactivity.modes.repulse.distance / 6, 3)
                  , l = i.interactivity.mouse.click_pos_x - e.x
                  , v = i.interactivity.mouse.click_pos_y - e.y
                  , p = l * l + v * v
                  , d = -o / p * 1;
                o >= p && function() {
                    var a = Math.atan2(v, l);
                    if (e.vx = d * Math.cos(a),
                    e.vy = d * Math.sin(a),
                    "bounce" == i.particles.move.out_mode) {
                        var t = {
                            x: e.x + e.vx,
                            y: e.y + e.vy
                        };
                        t.x + e.radius > i.canvas.w ? e.vx = -e.vx : t.x - e.radius < 0 && (e.vx = -e.vx),
                        t.y + e.radius > i.canvas.h ? e.vy = -e.vy : t.y - e.radius < 0 && (e.vy = -e.vy)
                    }
                }()
            } else
                0 == i.tmp.repulse_clicking && (e.vx = e.vx_i,
                e.vy = e.vy_i)
    }
    ,
    i.fn.modes.grabParticle = function(e) {
        if (i.interactivity.events.onhover.enable && "mousemove" == i.interactivity.status) {
            var a = e.x - i.interactivity.mouse.pos_x
              , t = e.y - i.interactivity.mouse.pos_y
              , s = Math.sqrt(a * a + t * t);
            if (s <= i.interactivity.modes.grab.distance) {
                var n = i.interactivity.modes.grab.line_linked.opacity - s / (1 / i.interactivity.modes.grab.line_linked.opacity) / i.interactivity.modes.grab.distance;
                if (n > 0) {
                    var r = i.particles.line_linked.color_rgb_line;
                    i.canvas.ctx.strokeStyle = "rgba(" + r.r + "," + r.g + "," + r.b + "," + n + ")",
                    i.canvas.ctx.lineWidth = i.particles.line_linked.width,
                    i.canvas.ctx.beginPath(),
                    i.canvas.ctx.moveTo(e.x, e.y),
                    i.canvas.ctx.lineTo(i.interactivity.mouse.pos_x, i.interactivity.mouse.pos_y),
                    i.canvas.ctx.stroke(),
                    i.canvas.ctx.closePath()
                }
            }
        }
    }
    ,
    i.fn.vendors.eventsListeners = function() {
        "window" == i.interactivity.detect_on ? i.interactivity.el = window : i.interactivity.el = i.canvas.el,
        (i.interactivity.events.onhover.enable || i.interactivity.events.onclick.enable) && (i.interactivity.el.addEventListener("mousemove", function(e) {
            if (i.interactivity.el == window)
                var a = e.clientX
                  , t = e.clientY;
            else
                var a = e.offsetX || e.clientX
                  , t = e.offsetY || e.clientY;
            i.interactivity.mouse.pos_x = a,
            i.interactivity.mouse.pos_y = t,
            i.tmp.retina && (i.interactivity.mouse.pos_x *= i.canvas.pxratio,
            i.interactivity.mouse.pos_y *= i.canvas.pxratio),
            i.interactivity.status = "mousemove"
        }),
        i.interactivity.el.addEventListener("mouseleave", function(e) {
            i.interactivity.mouse.pos_x = null,
            i.interactivity.mouse.pos_y = null,
            i.interactivity.status = "mouseleave"
        })),
        i.interactivity.events.onclick.enable && i.interactivity.el.addEventListener("click", function() {
            if (i.interactivity.mouse.click_pos_x = i.interactivity.mouse.pos_x,
            i.interactivity.mouse.click_pos_y = i.interactivity.mouse.pos_y,
            i.interactivity.mouse.click_time = (new Date).getTime(),
            i.interactivity.events.onclick.enable)
                switch (i.interactivity.events.onclick.mode) {
                case "push":
                    i.particles.move.enable ? i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb, i.interactivity.mouse) : 1 == i.interactivity.modes.push.particles_nb ? i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb, i.interactivity.mouse) : i.interactivity.modes.push.particles_nb > 1 && i.fn.modes.pushParticles(i.interactivity.modes.push.particles_nb);
                    break;
                case "remove":
                    i.fn.modes.removeParticles(i.interactivity.modes.remove.particles_nb);
                    break;
                case "bubble":
                    i.tmp.bubble_clicking = !0;
                    break;
                case "repulse":
                    i.tmp.repulse_clicking = !0,
                    i.tmp.repulse_count = 0,
                    i.tmp.repulse_finish = !1,
                    setTimeout(function() {
                        i.tmp.repulse_clicking = !1
                    }, 1e3 * i.interactivity.modes.repulse.duration)
                }
        })
    }
    ,
    i.fn.vendors.densityAutoParticles = function() {
        if (i.particles.number.density.enable) {
            var e = i.canvas.el.width * i.canvas.el.height / 1e3;
            i.tmp.retina && (e /= 2 * i.canvas.pxratio);
            var a = e * i.particles.number.value / i.particles.number.density.value_area
              , t = i.particles.array.length - a;
            0 > t ? i.fn.modes.pushParticles(Math.abs(t)) : i.fn.modes.removeParticles(t)
        }
    }
    ,
    i.fn.vendors.checkOverlap = function(e, a) {
        for (var t = 0; t < i.particles.array.length; t++) {
            var s = i.particles.array[t]
              , n = e.x - s.x
              , r = e.y - s.y;
            Math.sqrt(n * n + r * r) <= e.radius + s.radius && (e.x = a ? a.x : Math.random() * i.canvas.w,
            e.y = a ? a.y : Math.random() * i.canvas.h,
            i.fn.vendors.checkOverlap(e))
        }
    }
    ,
    i.fn.vendors.createSvgImg = function(e) {
        var a = /#([0-9A-F]{3,6})/gi
          , t = i.tmp.source_svg.replace(a, function(a, t, i, s) {
            if (e.color.rgb)
                n = "rgba(" + e.color.rgb.r + "," + e.color.rgb.g + "," + e.color.rgb.b + "," + e.opacity + ")";
            else
                var n = "hsla(" + e.color.hsl.h + "," + e.color.hsl.s + "%," + e.color.hsl.l + "%," + e.opacity + ")";
            return n
        })
          , s = new Blob([t],{
            type: "image/svg+xml;charset=utf-8"
        })
          , n = window.URL || window.webkitURL || window
          , r = n.createObjectURL(s)
          , c = new Image;
        c.addEventListener("load", function() {
            e.img.obj = c,
            e.img.loaded = !0,
            n.revokeObjectURL(r),
            i.tmp.count_svg++
        }),
        c.src = r
    }
    ,
    i.fn.vendors.destroypJS = function() {
        cancelAnimationFrame(i.fn.drawAnimFrame),
        t.remove(),
        pJSDom = null
    }
    ,
    i.fn.vendors.drawShape = function(e, a, t, i, s, n) {
        var r = s * n
          , c = s / n
          , o = 180 * (c - 2) / c
          , l = Math.PI - Math.PI * o / 180;
        e.save(),
        e.beginPath(),
        e.translate(a, t),
        e.moveTo(0, 0);
        for (var v = 0; r > v; v++)
            e.lineTo(i, 0),
            e.translate(i, 0),
            e.rotate(l);
        e.fill(),
        e.restore()
    }
    ,
    i.fn.vendors.exportImg = function() {
        window.open(i.canvas.el.toDataURL("image/png"), "_blank")
    }
    ,
    i.fn.vendors.loadImg = function(e) {
        if (i.tmp.img_error = void 0,
        "" != i.particles.shape.image.src)
            if ("svg" == e) {
                var a = new XMLHttpRequest;
                a.open("GET", i.particles.shape.image.src),
                a.onreadystatechange = function(e) {
                    4 == a.readyState && (200 == a.status ? (i.tmp.source_svg = e.currentTarget.response,
                    i.fn.vendors.checkBeforeDraw()) : (console.log("Error pJS - Image not found"),
                    i.tmp.img_error = !0))
                }
                ,
                a.send()
            } else {
                var t = new Image;
                t.addEventListener("load", function() {
                    i.tmp.img_obj = t,
                    i.fn.vendors.checkBeforeDraw()
                }),
                t.src = i.particles.shape.image.src
            }
        else
            console.log("Error pJS - No image.src"),
            i.tmp.img_error = !0
    }
    ,
    i.fn.vendors.draw = function() {
        "image" == i.particles.shape.type ? "svg" == i.tmp.img_type ? i.tmp.count_svg >= i.particles.number.value ? (i.fn.particlesDraw(),
        i.particles.move.enable ? i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw) : cancelRequestAnimFrame(i.fn.drawAnimFrame)) : i.tmp.img_error || (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw)) : void 0 != i.tmp.img_obj ? (i.fn.particlesDraw(),
        i.particles.move.enable ? i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw) : cancelRequestAnimFrame(i.fn.drawAnimFrame)) : i.tmp.img_error || (i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw)) : (i.fn.particlesDraw(),
        i.particles.move.enable ? i.fn.drawAnimFrame = requestAnimFrame(i.fn.vendors.draw) : cancelRequestAnimFrame(i.fn.drawAnimFrame))
    }
    ,
    i.fn.vendors.checkBeforeDraw = function() {
        "image" == i.particles.shape.type ? "svg" == i.tmp.img_type && void 0 == i.tmp.source_svg ? i.tmp.checkAnimFrame = requestAnimFrame(check) : (cancelRequestAnimFrame(i.tmp.checkAnimFrame),
        i.tmp.img_error || (i.fn.vendors.init(),
        i.fn.vendors.draw())) : (i.fn.vendors.init(),
        i.fn.vendors.draw())
    }
    ,
    i.fn.vendors.init = function() {
        i.fn.retinaInit(),
        i.fn.canvasInit(),
        i.fn.canvasSize(),
        i.fn.canvasPaint(),
        i.fn.particlesCreate(),
        i.fn.vendors.densityAutoParticles(),
        i.particles.line_linked.color_rgb_line = hexToRgb(i.particles.line_linked.color)
    }
    ,
    i.fn.vendors.start = function() {
        isInArray("image", i.particles.shape.type) ? (i.tmp.img_type = i.particles.shape.image.src.substr(i.particles.shape.image.src.length - 3),
        i.fn.vendors.loadImg(i.tmp.img_type)) : i.fn.vendors.checkBeforeDraw()
    }
    ,
    i.fn.vendors.eventsListeners(),
    i.fn.vendors.start()
};
Object.deepExtend = function(e, a) {
    for (var t in a)
        a[t] && a[t].constructor && a[t].constructor === Object ? (e[t] = e[t] || {},
        arguments.callee(e[t], a[t])) : e[t] = a[t];
    return e
}
,
window.requestAnimFrame = window.requestAnimationFrame || window.webkitRequestAnimationFrame || window.mozRequestAnimationFrame || window.oRequestAnimationFrame || window.msRequestAnimationFrame || function(e) {
    window.setTimeout(e, 1e3 / 60)
}
,
window.cancelRequestAnimFrame = window.cancelAnimationFrame || window.webkitCancelRequestAnimationFrame || window.mozCancelRequestAnimationFrame || window.oCancelRequestAnimationFrame || window.msCancelRequestAnimationFrame || clearTimeout,
window.pJSDom = [],
window.particlesJS = function(e, a) {
    "string" != typeof e && (a = e,
    e = "particles-js"),
    e || (e = "particles-js");
    var t = document.getElementById(e)
      , i = "particles-js-canvas-el"
      , s = t.getElementsByClassName(i);
    if (s.length)
        for (; s.length > 0; )
            t.removeChild(s[0]);
    var n = document.createElement("canvas");
    n.className = i,
    n.style.width = "100%",
    n.style.height = "100%",
    null != document.getElementById(e).appendChild(n) && pJSDom.push(new pJS(e,a))
}
,
window.particlesJS.load = function(e, a, t) {
    var i = new XMLHttpRequest;
    i.open("GET", a),
    i.onreadystatechange = function(a) {
        if (4 == i.readyState)
            if (200 == i.status) {
                var s = JSON.parse(a.currentTarget.response);
                window.particlesJS(e, s),
                t && t()
            } else
                console.log("Error pJS - XMLHttpRequest status: " + i.status),
                console.log("Error pJS - File config not found")
    }
    ,
    i.send()
}
;
!function(i) {
    i.fn.ttStickySidebar = function(t) {
        function o(t, o) {
            var a = e(t, o);
            a || (console.log("TSS: Body width smaller than options.minWidth. Init is delayed."),
            i(document).scroll(function(t, o) {
                return function(a) {
                    var n = e(t, o);
                    n && i(this).unbind(a)
                }
            }(t, o)),
            i(window).resize(function(t, o) {
                return function(a) {
                    var n = e(t, o);
                    n && i(this).unbind(a)
                }
            }(t, o)))
        }
        function e(t, o) {
            return t.initialized === !0 || !(i("body").width() < t.minWidth) && (a(t, o),
            !0)
        }
        function a(t, o) {
            t.initialized = !0,
            i("head").append(i('<style>.ttStickySidebar:after {content: ""; display: table; clear: both;}</style>')),
            o.each(function() {
                function o() {
                    a.fixedScrollTop = 0,
                    a.sidebar.css({
                        "min-height": "1px"
                    }),
                    a.stickySidebar.css({
                        position: "static",
                        width: "",
                        transform: "none"
                    })
                }
                function e(t) {
                    var o = t.height();
                    return t.children().each(function() {
                        o = Math.max(o, i(this).height())
                    }),
                    o
                }
                var a = {};
                if (a.sidebar = i(this),
                a.options = t || {},
                a.container = i(a.options.containerSelector),
                0 == a.container.length && (a.container = a.sidebar.parent()),
                a.sidebar.parents().css("-webkit-transform", "none"),
                a.sidebar.css({
                    position: "relative",
                    overflow: "visible",
                    "-webkit-box-sizing": "border-box",
                    "-moz-box-sizing": "border-box",
                    "box-sizing": "border-box"
                }),
                a.stickySidebar = a.sidebar.find(".ttStickySidebar"),
                0 == a.stickySidebar.length) {
                    var r = /(?:text|application)\/(?:x-)?(?:javascript|ecmascript)/i;
                    a.sidebar.find("script").filter(function(i, t) {
                        return 0 === t.type.length || t.type.match(r)
                    }).remove(),
                    a.stickySidebar = i("<div>").addClass("ttStickySidebar").append(a.sidebar.children()),
                    a.sidebar.append(a.stickySidebar)
                }
                a.marginBottom = parseInt(a.sidebar.css("margin-bottom")),
                a.paddingTop = parseInt(a.sidebar.css("padding-top")),
                a.paddingBottom = parseInt(a.sidebar.css("padding-bottom"));
                var d = a.stickySidebar.offset().top
                  , s = a.stickySidebar.outerHeight();
                a.stickySidebar.css("padding-top", 1),
                a.stickySidebar.css("padding-bottom", 1),
                d -= a.stickySidebar.offset().top,
                s = a.stickySidebar.outerHeight() - s - d,
                0 == d ? (a.stickySidebar.css("padding-top", 0),
                a.stickySidebarPaddingTop = 0) : a.stickySidebarPaddingTop = 1,
                0 == s ? (a.stickySidebar.css("padding-bottom", 0),
                a.stickySidebarPaddingBottom = 0) : a.stickySidebarPaddingBottom = 1,
                a.previousScrollTop = null,
                a.fixedScrollTop = 0,
                o(),
                a.onScroll = function(a) {
                    if (a.stickySidebar.is(":visible")) {
                        if (i("body").width() < a.options.minWidth)
                            return void o();
                        if (a.options.disableOnResponsiveLayouts) {
                            var r = a.sidebar.outerWidth("none" == a.sidebar.css("float"));
                            if (r + 50 > a.container.width())
                                return void o()
                        }
                        var d = i(document).scrollTop()
                          , s = "static";
                        if (d >= a.sidebar.offset().top + (a.paddingTop - a.options.additionalMarginTop)) {
                            var c, p = a.paddingTop + t.additionalMarginTop, b = a.paddingBottom + a.marginBottom + t.additionalMarginBottom, l = a.sidebar.offset().top, f = a.sidebar.offset().top + e(a.container), h = 0 + t.additionalMarginTop, g = a.stickySidebar.outerHeight() + p + b < i(window).height();
                            c = g ? h + a.stickySidebar.outerHeight() : i(window).height() - a.marginBottom - a.paddingBottom - t.additionalMarginBottom;
                            var u = l - d + a.paddingTop
                              , S = f - d - a.paddingBottom - a.marginBottom
                              , y = a.stickySidebar.offset().top - d
                              , m = a.previousScrollTop - d;
                            "fixed" == a.stickySidebar.css("position") && "modern" == a.options.sidebarBehavior && (y += m),
                            "stick-to-top" == a.options.sidebarBehavior && (y = t.additionalMarginTop),
                            "stick-to-bottom" == a.options.sidebarBehavior && (y = c - a.stickySidebar.outerHeight()),
                            y = m > 0 ? Math.min(y, h) : Math.max(y, c - a.stickySidebar.outerHeight()),
                            y = Math.max(y, u),
                            y = Math.min(y, S - a.stickySidebar.outerHeight());
                            var k = a.container.height() == a.stickySidebar.outerHeight();
                            s = (k || y != h) && (k || y != c - a.stickySidebar.outerHeight()) ? d + y - a.sidebar.offset().top - a.paddingTop <= t.additionalMarginTop ? "static" : "absolute" : "fixed"
                        }
                        if ("fixed" == s) {
                            var v = i(document).scrollLeft();
                            a.stickySidebar.css({
                                position: "fixed",
                                width: n(a.stickySidebar) + "px",
                                transform: "translateY(" + y + "px)",
                                left: a.sidebar.offset().left + parseInt(a.sidebar.css("padding-left")) - v + "px",
                                top: "0px"
                            })
                        } else if ("absolute" == s) {
                            var x = {};
                            "absolute" != a.stickySidebar.css("position") && (x.position = "absolute",
                            x.transform = "translateY(" + (d + y - a.sidebar.offset().top - a.stickySidebarPaddingTop - a.stickySidebarPaddingBottom) + "px)",
                            x.top = "0px"),
                            x.width = n(a.stickySidebar) + "px",
                            x.left = "",
                            a.stickySidebar.css(x)
                        } else
                            "static" == s && o();
                        "static" != s && 1 == a.options.updateSidebarHeight && a.sidebar.css({
                            "min-height": a.stickySidebar.outerHeight() + a.stickySidebar.offset().top - a.sidebar.offset().top + a.paddingBottom
                        }),
                        a.previousScrollTop = d
                    }
                }
                ,
                a.onScroll(a),
                i(document).scroll(function(i) {
                    return function() {
                        i.onScroll(i)
                    }
                }(a)),
                i(window).resize(function(i) {
                    return function() {
                        i.stickySidebar.css({
                            position: "static"
                        }),
                        i.onScroll(i)
                    }
                }(a)),
                "undefined" != typeof ResizeSensor && new ResizeSensor(a.stickySidebar[0],function(i) {
                    return function() {
                        i.onScroll(i)
                    }
                }(a))
            })
        }
        function n(i) {
            var t;
            try {
                t = i[0].getBoundingClientRect().width
            } catch (i) {}
            return "undefined" == typeof t && (t = i.width()),
            t
        }
        var r = {
            containerSelector: "",
            additionalMarginTop: 0,
            additionalMarginBottom: 0,
            updateSidebarHeight: !0,
            minWidth: 0,
            disableOnResponsiveLayouts: !0,
            sidebarBehavior: "modern"
        };
        t = i.extend(r, t),
        t.additionalMarginTop = parseInt(t.additionalMarginTop) || 0,
        t.additionalMarginBottom = parseInt(t.additionalMarginBottom) || 0,
        o(t, this)
    }
}(jQuery);
!function($) {
    $.flexslider = function(e, t) {
        var a = $(e);
        a.vars = $.extend({}, $.flexslider.defaults, t);
        var n = a.vars.namespace, i = window.navigator && window.navigator.msPointerEnabled && window.MSGesture, s = ("ontouchstart"in window || i || window.DocumentTouch && document instanceof DocumentTouch) && a.vars.touch, r = "click touchend MSPointerUp keyup", o = "", l, c = "vertical" === a.vars.direction, d = a.vars.reverse, u = a.vars.itemWidth > 0, v = "fade" === a.vars.animation, p = "" !== a.vars.asNavFor, m = {}, f = !0;
        $.data(e, "flexslider", a),
        m = {
            init: function() {
                a.animating = !1,
                a.currentSlide = parseInt(a.vars.startAt ? a.vars.startAt : 0, 10),
                isNaN(a.currentSlide) && (a.currentSlide = 0),
                a.animatingTo = a.currentSlide,
                a.atEnd = 0 === a.currentSlide || a.currentSlide === a.last,
                a.containerSelector = a.vars.selector.substr(0, a.vars.selector.search(" ")),
                a.slides = $(a.vars.selector, a),
                a.container = $(a.containerSelector, a),
                a.count = a.slides.length,
                a.syncExists = $(a.vars.sync).length > 0,
                "slide" === a.vars.animation && (a.vars.animation = "swing"),
                a.prop = c ? "top" : "marginLeft",
                a.args = {},
                a.manualPause = !1,
                a.stopped = !1,
                a.started = !1,
                a.startTimeout = null,
                a.transitions = !a.vars.video && !v && a.vars.useCSS && function() {
                    var e = document.createElement("div")
                      , t = ["perspectiveProperty", "WebkitPerspective", "MozPerspective", "OPerspective", "msPerspective"];
                    for (var n in t)
                        if (void 0 !== e.style[t[n]])
                            return a.pfx = t[n].replace("Perspective", "").toLowerCase(),
                            a.prop = "-" + a.pfx + "-transform",
                            !0;
                    return !1
                }(),
                a.ensureAnimationEnd = "",
                "" !== a.vars.controlsContainer && (a.controlsContainer = $(a.vars.controlsContainer).length > 0 && $(a.vars.controlsContainer)),
                "" !== a.vars.manualControls && (a.manualControls = $(a.vars.manualControls).length > 0 && $(a.vars.manualControls)),
                a.vars.randomize && (a.slides.sort(function() {
                    return Math.round(Math.random()) - .5
                }),
                a.container.empty().append(a.slides)),
                a.doMath(),
                a.setup("init"),
                a.vars.controlNav && m.controlNav.setup(),
                a.vars.directionNav && m.directionNav.setup(),
                a.vars.keyboard && (1 === $(a.containerSelector).length || a.vars.multipleKeyboard) && $(document).bind("keyup", function(e) {
                    var t = e.keyCode;
                    if (!a.animating && (39 === t || 37 === t)) {
                        var n = 39 === t ? a.getTarget("next") : 37 === t ? a.getTarget("prev") : !1;
                        a.flexAnimate(n, a.vars.pauseOnAction)
                    }
                }),
                a.vars.mousewheel && a.bind("mousewheel", function(e, t, n, i) {
                    e.preventDefault();
                    var s = a.getTarget(0 > t ? "next" : "prev");
                    a.flexAnimate(s, a.vars.pauseOnAction)
                }),
                a.vars.pausePlay && m.pausePlay.setup(),
                a.vars.slideshow && a.vars.pauseInvisible && m.pauseInvisible.init(),
                a.vars.slideshow && (a.vars.pauseOnHover && a.hover(function() {
                    a.manualPlay || a.manualPause || a.pause()
                }, function() {
                    a.manualPause || a.manualPlay || a.stopped || a.play()
                }),
                a.vars.pauseInvisible && m.pauseInvisible.isHidden() || (a.vars.initDelay > 0 ? a.startTimeout = setTimeout(a.play, a.vars.initDelay) : a.play())),
                p && m.asNav.setup(),
                s && a.vars.touch && m.touch(),
                (!v || v && a.vars.smoothHeight) && $(window).bind("resize orientationchange focus", m.resize),
                a.find("img").attr("draggable", "false"),
                setTimeout(function() {
                    a.vars.start(a)
                }, 200)
            },
            asNav: {
                setup: function() {
                    a.asNav = !0,
                    a.animatingTo = Math.floor(a.currentSlide / a.move),
                    a.currentItem = a.currentSlide,
                    a.slides.removeClass(n + "active-slide").eq(a.currentItem).addClass(n + "active-slide"),
                    i ? (e._slider = a,
                    a.slides.each(function() {
                        var e = this;
                        e._gesture = new MSGesture,
                        e._gesture.target = e,
                        e.addEventListener("MSPointerDown", function(e) {
                            e.preventDefault(),
                            e.currentTarget._gesture && e.currentTarget._gesture.addPointer(e.pointerId)
                        }, !1),
                        e.addEventListener("MSGestureTap", function(e) {
                            e.preventDefault();
                            var t = $(this)
                              , n = t.index();
                            $(a.vars.asNavFor).data("flexslider").animating || t.hasClass("active") || (a.direction = a.currentItem < n ? "next" : "prev",
                            a.flexAnimate(n, a.vars.pauseOnAction, !1, !0, !0))
                        })
                    })) : a.slides.on(r, function(e) {
                        e.preventDefault();
                        var t = $(this)
                          , i = t.index()
                          , s = t.offset().left - $(a).scrollLeft();
                        0 >= s && t.hasClass(n + "active-slide") ? a.flexAnimate(a.getTarget("prev"), !0) : $(a.vars.asNavFor).data("flexslider").animating || t.hasClass(n + "active-slide") || (a.direction = a.currentItem < i ? "next" : "prev",
                        a.flexAnimate(i, a.vars.pauseOnAction, !1, !0, !0))
                    })
                }
            },
            controlNav: {
                setup: function() {
                    a.manualControls ? m.controlNav.setupManual() : m.controlNav.setupPaging()
                },
                setupPaging: function() {
                    var e = "thumbnails" === a.vars.controlNav ? "control-thumbs" : "control-paging", t = 1, i, s;
                    if (a.controlNavScaffold = $('<ol class="' + n + "control-nav " + n + e + '"></ol>'),
                    a.pagingCount > 1)
                        for (var l = 0; l < a.pagingCount; l++) {
                            if (s = a.slides.eq(l),
                            i = "thumbnails" === a.vars.controlNav ? '<img src="' + s.attr("data-thumb") + '"/>' : "<a>" + t + "</a>",
                            "thumbnails" === a.vars.controlNav && !0 === a.vars.thumbCaptions) {
                                var c = s.attr("data-thumbcaption");
                                "" != c && void 0 != c && (i += '<span class="' + n + 'caption">' + c + "</span>")
                            }
                            a.controlNavScaffold.append("<li>" + i + "</li>"),
                            t++
                        }
                    a.controlsContainer ? $(a.controlsContainer).append(a.controlNavScaffold) : a.append(a.controlNavScaffold),
                    m.controlNav.set(),
                    m.controlNav.active(),
                    a.controlNavScaffold.delegate("a, img", r, function(e) {
                        if (e.preventDefault(),
                        "" === o || o === e.type) {
                            var t = $(this)
                              , i = a.controlNav.index(t);
                            t.hasClass(n + "active") || (a.direction = i > a.currentSlide ? "next" : "prev",
                            a.flexAnimate(i, a.vars.pauseOnAction))
                        }
                        "" === o && (o = e.type),
                        m.setToClearWatchedEvent()
                    })
                },
                setupManual: function() {
                    a.controlNav = a.manualControls,
                    m.controlNav.active(),
                    a.controlNav.bind(r, function(e) {
                        if (e.preventDefault(),
                        "" === o || o === e.type) {
                            var t = $(this)
                              , i = a.controlNav.index(t);
                            t.hasClass(n + "active") || (a.direction = i > a.currentSlide ? "next" : "prev",
                            a.flexAnimate(i, a.vars.pauseOnAction))
                        }
                        "" === o && (o = e.type),
                        m.setToClearWatchedEvent()
                    })
                },
                set: function() {
                    var e = "thumbnails" === a.vars.controlNav ? "img" : "a";
                    a.controlNav = $("." + n + "control-nav li " + e, a.controlsContainer ? a.controlsContainer : a)
                },
                active: function() {
                    a.controlNav.removeClass(n + "active").eq(a.animatingTo).addClass(n + "active")
                },
                update: function(e, t) {
                    a.pagingCount > 1 && "add" === e ? a.controlNavScaffold.append($("<li><a>" + a.count + "</a></li>")) : 1 === a.pagingCount ? a.controlNavScaffold.find("li").remove() : a.controlNav.eq(t).closest("li").remove(),
                    m.controlNav.set(),
                    a.pagingCount > 1 && a.pagingCount !== a.controlNav.length ? a.update(t, e) : m.controlNav.active()
                }
            },
            directionNav: {
                setup: function() {
                    var e = $('<ul class="' + n + 'direction-nav"><li class="' + n + 'nav-prev"><a class="' + n + 'prev" href="#">' + a.vars.prevText + '</a></li><li class="' + n + 'nav-next"><a class="' + n + 'next" href="#">' + a.vars.nextText + "</a></li></ul>");
                    a.controlsContainer ? ($(a.controlsContainer).append(e),
                    a.directionNav = $("." + n + "direction-nav li a", a.controlsContainer)) : (a.append(e),
                    a.directionNav = $("." + n + "direction-nav li a", a)),
                    m.directionNav.update(),
                    a.directionNav.bind(r, function(e) {
                        e.preventDefault();
                        var t;
                        ("" === o || o === e.type) && (t = a.getTarget($(this).hasClass(n + "next") ? "next" : "prev"),
                        a.flexAnimate(t, a.vars.pauseOnAction)),
                        "" === o && (o = e.type),
                        m.setToClearWatchedEvent()
                    })
                },
                update: function() {
                    var e = n + "disabled";
                    1 === a.pagingCount ? a.directionNav.addClass(e).attr("tabindex", "-1") : a.vars.animationLoop ? a.directionNav.removeClass(e).removeAttr("tabindex") : 0 === a.animatingTo ? a.directionNav.removeClass(e).filter("." + n + "prev").addClass(e).attr("tabindex", "-1") : a.animatingTo === a.last ? a.directionNav.removeClass(e).filter("." + n + "next").addClass(e).attr("tabindex", "-1") : a.directionNav.removeClass(e).removeAttr("tabindex")
                }
            },
            pausePlay: {
                setup: function() {
                    var e = $('<div class="' + n + 'pauseplay"><a></a></div>');
                    a.controlsContainer ? (a.controlsContainer.append(e),
                    a.pausePlay = $("." + n + "pauseplay a", a.controlsContainer)) : (a.append(e),
                    a.pausePlay = $("." + n + "pauseplay a", a)),
                    m.pausePlay.update(a.vars.slideshow ? n + "pause" : n + "play"),
                    a.pausePlay.bind(r, function(e) {
                        e.preventDefault(),
                        ("" === o || o === e.type) && ($(this).hasClass(n + "pause") ? (a.manualPause = !0,
                        a.manualPlay = !1,
                        a.pause()) : (a.manualPause = !1,
                        a.manualPlay = !0,
                        a.play())),
                        "" === o && (o = e.type),
                        m.setToClearWatchedEvent()
                    })
                },
                update: function(e) {
                    "play" === e ? a.pausePlay.removeClass(n + "pause").addClass(n + "play").html(a.vars.playText) : a.pausePlay.removeClass(n + "play").addClass(n + "pause").html(a.vars.pauseText)
                }
            },
            touch: function() {
                function t(t) {
                    a.animating ? t.preventDefault() : (window.navigator.msPointerEnabled || 1 === t.touches.length) && (a.pause(),
                    g = c ? a.h : a.w,
                    S = Number(new Date),
                    x = t.touches[0].pageX,
                    b = t.touches[0].pageY,
                    f = u && d && a.animatingTo === a.last ? 0 : u && d ? a.limit - (a.itemW + a.vars.itemMargin) * a.move * a.animatingTo : u && a.currentSlide === a.last ? a.limit : u ? (a.itemW + a.vars.itemMargin) * a.move * a.currentSlide : d ? (a.last - a.currentSlide + a.cloneOffset) * g : (a.currentSlide + a.cloneOffset) * g,
                    p = c ? b : x,
                    m = c ? x : b,
                    e.addEventListener("touchmove", n, !1),
                    e.addEventListener("touchend", s, !1))
                }
                function n(e) {
                    x = e.touches[0].pageX,
                    b = e.touches[0].pageY,
                    h = c ? p - b : p - x,
                    y = c ? Math.abs(h) < Math.abs(x - m) : Math.abs(h) < Math.abs(b - m);
                    var t = 500;
                    (!y || Number(new Date) - S > t) && (e.preventDefault(),
                    !v && a.transitions && (a.vars.animationLoop || (h /= 0 === a.currentSlide && 0 > h || a.currentSlide === a.last && h > 0 ? Math.abs(h) / g + 2 : 1),
                    a.setProps(f + h, "setTouch")))
                }
                function s(t) {
                    if (e.removeEventListener("touchmove", n, !1),
                    a.animatingTo === a.currentSlide && !y && null !== h) {
                        var i = d ? -h : h
                          , r = a.getTarget(i > 0 ? "next" : "prev");
                        a.canAdvance(r) && (Number(new Date) - S < 550 && Math.abs(i) > 50 || Math.abs(i) > g / 2) ? a.flexAnimate(r, a.vars.pauseOnAction) : v || a.flexAnimate(a.currentSlide, a.vars.pauseOnAction, !0)
                    }
                    e.removeEventListener("touchend", s, !1),
                    p = null,
                    m = null,
                    h = null,
                    f = null
                }
                function r(t) {
                    t.stopPropagation(),
                    a.animating ? t.preventDefault() : (a.pause(),
                    e._gesture.addPointer(t.pointerId),
                    w = 0,
                    g = c ? a.h : a.w,
                    S = Number(new Date),
                    f = u && d && a.animatingTo === a.last ? 0 : u && d ? a.limit - (a.itemW + a.vars.itemMargin) * a.move * a.animatingTo : u && a.currentSlide === a.last ? a.limit : u ? (a.itemW + a.vars.itemMargin) * a.move * a.currentSlide : d ? (a.last - a.currentSlide + a.cloneOffset) * g : (a.currentSlide + a.cloneOffset) * g)
                }
                function o(t) {
                    t.stopPropagation();
                    var a = t.target._slider;
                    if (a) {
                        var n = -t.translationX
                          , i = -t.translationY;
                        return w += c ? i : n,
                        h = w,
                        y = c ? Math.abs(w) < Math.abs(-n) : Math.abs(w) < Math.abs(-i),
                        t.detail === t.MSGESTURE_FLAG_INERTIA ? void setImmediate(function() {
                            e._gesture.stop()
                        }) : void ((!y || Number(new Date) - S > 500) && (t.preventDefault(),
                        !v && a.transitions && (a.vars.animationLoop || (h = w / (0 === a.currentSlide && 0 > w || a.currentSlide === a.last && w > 0 ? Math.abs(w) / g + 2 : 1)),
                        a.setProps(f + h, "setTouch"))))
                    }
                }
                function l(e) {
                    e.stopPropagation();
                    var t = e.target._slider;
                    if (t) {
                        if (t.animatingTo === t.currentSlide && !y && null !== h) {
                            var a = d ? -h : h
                              , n = t.getTarget(a > 0 ? "next" : "prev");
                            t.canAdvance(n) && (Number(new Date) - S < 550 && Math.abs(a) > 50 || Math.abs(a) > g / 2) ? t.flexAnimate(n, t.vars.pauseOnAction) : v || t.flexAnimate(t.currentSlide, t.vars.pauseOnAction, !0)
                        }
                        p = null,
                        m = null,
                        h = null,
                        f = null,
                        w = 0
                    }
                }
                var p, m, f, g, h, S, y = !1, x = 0, b = 0, w = 0;
                i ? (e.style.msTouchAction = "none",
                e._gesture = new MSGesture,
                e._gesture.target = e,
                e.addEventListener("MSPointerDown", r, !1),
                e._slider = a,
                e.addEventListener("MSGestureChange", o, !1),
                e.addEventListener("MSGestureEnd", l, !1)) : e.addEventListener("touchstart", t, !1)
            },
            resize: function() {
                !a.animating && a.is(":visible") && (u || a.doMath(),
                v ? m.smoothHeight() : u ? (a.slides.width(a.computedW),
                a.update(a.pagingCount),
                a.setProps()) : c ? (a.viewport.height(a.h),
                a.setProps(a.h, "setTotal")) : (a.vars.smoothHeight && m.smoothHeight(),
                a.newSlides.width(a.computedW),
                a.setProps(a.computedW, "setTotal")))
            },
            smoothHeight: function(e) {
                if (!c || v) {
                    var t = v ? a : a.viewport;
                    e ? t.animate({
                        height: a.slides.eq(a.animatingTo).height()
                    }, e) : t.height(a.slides.eq(a.animatingTo).height())
                }
            },
            sync: function(e) {
                var t = $(a.vars.sync).data("flexslider")
                  , n = a.animatingTo;
                switch (e) {
                case "animate":
                    t.flexAnimate(n, a.vars.pauseOnAction, !1, !0);
                    break;
                case "play":
                    t.playing || t.asNav || t.play();
                    break;
                case "pause":
                    t.pause()
                }
            },
            uniqueID: function(e) {
                return e.filter("[id]").add(e.find("[id]")).each(function() {
                    var e = $(this);
                    e.attr("id", e.attr("id") + "_clone")
                }),
                e
            },
            pauseInvisible: {
                visProp: null,
                init: function() {
                    var e = m.pauseInvisible.getHiddenProp();
                    if (e) {
                        var t = e.replace(/[H|h]idden/, "") + "visibilitychange";
                        document.addEventListener(t, function() {
                            m.pauseInvisible.isHidden() ? a.startTimeout ? clearTimeout(a.startTimeout) : a.pause() : a.started ? a.play() : a.vars.initDelay > 0 ? setTimeout(a.play, a.vars.initDelay) : a.play()
                        })
                    }
                },
                isHidden: function() {
                    var e = m.pauseInvisible.getHiddenProp();
                    return e ? document[e] : !1
                },
                getHiddenProp: function() {
                    var e = ["webkit", "moz", "ms", "o"];
                    if ("hidden"in document)
                        return "hidden";
                    for (var t = 0; t < e.length; t++)
                        if (e[t] + "Hidden"in document)
                            return e[t] + "Hidden";
                    return null
                }
            },
            setToClearWatchedEvent: function() {
                clearTimeout(l),
                l = setTimeout(function() {
                    o = ""
                }, 3e3)
            }
        },
        a.flexAnimate = function(e, t, i, r, o) {
            if (a.vars.animationLoop || e === a.currentSlide || (a.direction = e > a.currentSlide ? "next" : "prev"),
            p && 1 === a.pagingCount && (a.direction = a.currentItem < e ? "next" : "prev"),
            !a.animating && (a.canAdvance(e, o) || i) && a.is(":visible")) {
                if (p && r) {
                    var l = $(a.vars.asNavFor).data("flexslider");
                    if (a.atEnd = 0 === e || e === a.count - 1,
                    l.flexAnimate(e, !0, !1, !0, o),
                    a.direction = a.currentItem < e ? "next" : "prev",
                    l.direction = a.direction,
                    Math.ceil((e + 1) / a.visible) - 1 === a.currentSlide || 0 === e)
                        return a.currentItem = e,
                        a.slides.removeClass(n + "active-slide").eq(e).addClass(n + "active-slide"),
                        !1;
                    a.currentItem = e,
                    a.slides.removeClass(n + "active-slide").eq(e).addClass(n + "active-slide"),
                    e = Math.floor(e / a.visible)
                }
                if (a.animating = !0,
                a.animatingTo = e,
                t && a.pause(),
                a.vars.before(a),
                a.syncExists && !o && m.sync("animate"),
                a.vars.controlNav && m.controlNav.active(),
                u || a.slides.removeClass(n + "active-slide").eq(e).addClass(n + "active-slide"),
                a.atEnd = 0 === e || e === a.last,
                a.vars.directionNav && m.directionNav.update(),
                e === a.last && (a.vars.end(a),
                a.vars.animationLoop || a.pause()),
                v)
                    s ? (a.slides.eq(a.currentSlide).css({
                        opacity: 0,
                        zIndex: 1
                    }),
                    a.slides.eq(e).css({
                        opacity: 1,
                        zIndex: 2
                    }),
                    a.wrapup(f)) : (a.slides.eq(a.currentSlide).css({
                        zIndex: 1
                    }).animate({
                        opacity: 0
                    }, a.vars.animationSpeed, a.vars.easing),
                    a.slides.eq(e).css({
                        zIndex: 2
                    }).animate({
                        opacity: 1
                    }, a.vars.animationSpeed, a.vars.easing, a.wrapup));
                else {
                    var f = c ? a.slides.filter(":first").height() : a.computedW, g, h, S;
                    u ? (g = a.vars.itemMargin,
                    S = (a.itemW + g) * a.move * a.animatingTo,
                    h = S > a.limit && 1 !== a.visible ? a.limit : S) : h = 0 === a.currentSlide && e === a.count - 1 && a.vars.animationLoop && "next" !== a.direction ? d ? (a.count + a.cloneOffset) * f : 0 : a.currentSlide === a.last && 0 === e && a.vars.animationLoop && "prev" !== a.direction ? d ? 0 : (a.count + 1) * f : d ? (a.count - 1 - e + a.cloneOffset) * f : (e + a.cloneOffset) * f,
                    a.setProps(h, "", a.vars.animationSpeed),
                    a.transitions ? (a.vars.animationLoop && a.atEnd || (a.animating = !1,
                    a.currentSlide = a.animatingTo),
                    a.container.unbind("webkitTransitionEnd transitionend"),
                    a.container.bind("webkitTransitionEnd transitionend", function() {
                        clearTimeout(a.ensureAnimationEnd),
                        a.wrapup(f)
                    }),
                    clearTimeout(a.ensureAnimationEnd),
                    a.ensureAnimationEnd = setTimeout(function() {
                        a.wrapup(f)
                    }, a.vars.animationSpeed + 100)) : a.container.animate(a.args, a.vars.animationSpeed, a.vars.easing, function() {
                        a.wrapup(f)
                    })
                }
                a.vars.smoothHeight && m.smoothHeight(a.vars.animationSpeed)
            }
        }
        ,
        a.wrapup = function(e) {
            v || u || (0 === a.currentSlide && a.animatingTo === a.last && a.vars.animationLoop ? a.setProps(e, "jumpEnd") : a.currentSlide === a.last && 0 === a.animatingTo && a.vars.animationLoop && a.setProps(e, "jumpStart")),
            a.animating = !1,
            a.currentSlide = a.animatingTo,
            a.vars.after(a)
        }
        ,
        a.animateSlides = function() {
            !a.animating && f && a.flexAnimate(a.getTarget("next"))
        }
        ,
        a.pause = function() {
            clearInterval(a.animatedSlides),
            a.animatedSlides = null,
            a.playing = !1,
            a.vars.pausePlay && m.pausePlay.update("play"),
            a.syncExists && m.sync("pause")
        }
        ,
        a.play = function() {
            a.playing && clearInterval(a.animatedSlides),
            a.animatedSlides = a.animatedSlides || setInterval(a.animateSlides, a.vars.slideshowSpeed),
            a.started = a.playing = !0,
            a.vars.pausePlay && m.pausePlay.update("pause"),
            a.syncExists && m.sync("play")
        }
        ,
        a.stop = function() {
            a.pause(),
            a.stopped = !0
        }
        ,
        a.canAdvance = function(e, t) {
            var n = p ? a.pagingCount - 1 : a.last;
            return t ? !0 : p && a.currentItem === a.count - 1 && 0 === e && "prev" === a.direction ? !0 : p && 0 === a.currentItem && e === a.pagingCount - 1 && "next" !== a.direction ? !1 : e !== a.currentSlide || p ? a.vars.animationLoop ? !0 : a.atEnd && 0 === a.currentSlide && e === n && "next" !== a.direction ? !1 : a.atEnd && a.currentSlide === n && 0 === e && "next" === a.direction ? !1 : !0 : !1
        }
        ,
        a.getTarget = function(e) {
            return a.direction = e,
            "next" === e ? a.currentSlide === a.last ? 0 : a.currentSlide + 1 : 0 === a.currentSlide ? a.last : a.currentSlide - 1
        }
        ,
        a.setProps = function(e, t, n) {
            var i = function() {
                var n = e ? e : (a.itemW + a.vars.itemMargin) * a.move * a.animatingTo
                  , i = function() {
                    if (u)
                        return "setTouch" === t ? e : d && a.animatingTo === a.last ? 0 : d ? a.limit - (a.itemW + a.vars.itemMargin) * a.move * a.animatingTo : a.animatingTo === a.last ? a.limit : n;
                    switch (t) {
                    case "setTotal":
                        return d ? (a.count - 1 - a.currentSlide + a.cloneOffset) * e : (a.currentSlide + a.cloneOffset) * e;
                    case "setTouch":
                        return d ? e : e;
                    case "jumpEnd":
                        return d ? e : a.count * e;
                    case "jumpStart":
                        return d ? a.count * e : e;
                    default:
                        return e
                    }
                }();
                return -1 * i + "px"
            }();
            a.transitions && (i = c ? "translate3d(0," + i + ",0)" : "translate3d(" + i + ",0,0)",
            n = void 0 !== n ? n / 1e3 + "s" : "0s",
            a.container.css("-" + a.pfx + "-transition-duration", n),
            a.container.css("transition-duration", n)),
            a.args[a.prop] = i,
            (a.transitions || void 0 === n) && a.container.css(a.args),
            a.container.css("transform", i)
        }
        ,
        a.setup = function(e) {
            if (v)
                a.slides.css({
                    width: "100%",
                    "float": "left",
                    marginRight: "-100%",
                    position: "relative"
                }),
                "init" === e && (s ? a.slides.css({
                    opacity: 0,
                    display: "block",
                    webkitTransition: "opacity " + a.vars.animationSpeed / 1e3 + "s ease",
                    zIndex: 1
                }).eq(a.currentSlide).css({
                    opacity: 1,
                    zIndex: 2
                }) : 0 == a.vars.fadeFirstSlide ? a.slides.css({
                    opacity: 0,
                    display: "block",
                    zIndex: 1
                }).eq(a.currentSlide).css({
                    zIndex: 2
                }).css({
                    opacity: 1
                }) : a.slides.css({
                    opacity: 0,
                    display: "block",
                    zIndex: 1
                }).eq(a.currentSlide).css({
                    zIndex: 2
                }).animate({
                    opacity: 1
                }, a.vars.animationSpeed, a.vars.easing)),
                a.vars.smoothHeight && m.smoothHeight();
            else {
                var t, i;
                "init" === e && (a.viewport = $('<div class="' + n + 'viewport"></div>').css({
                    overflow: "hidden",
                    position: "relative"
                }).appendTo(a).append(a.container),
                a.cloneCount = 0,
                a.cloneOffset = 0,
                d && (i = $.makeArray(a.slides).reverse(),
                a.slides = $(i),
                a.container.empty().append(a.slides))),
                a.vars.animationLoop && !u && (a.cloneCount = 2,
                a.cloneOffset = 1,
                "init" !== e && a.container.find(".clone").remove(),
                a.container.append(m.uniqueID(a.slides.first().clone().addClass("clone")).attr("aria-hidden", "true")).prepend(m.uniqueID(a.slides.last().clone().addClass("clone")).attr("aria-hidden", "true"))),
                a.newSlides = $(a.vars.selector, a),
                t = d ? a.count - 1 - a.currentSlide + a.cloneOffset : a.currentSlide + a.cloneOffset,
                c && !u ? (a.container.height(200 * (a.count + a.cloneCount) + "%").css("position", "absolute").width("100%"),
                setTimeout(function() {
                    a.newSlides.css({
                        display: "block"
                    }),
                    a.doMath(),
                    a.viewport.height(a.h),
                    a.setProps(t * a.h, "init")
                }, "init" === e ? 100 : 0)) : (a.container.width(200 * (a.count + a.cloneCount) + "%"),
                a.setProps(t * a.computedW, "init"),
                setTimeout(function() {
                    a.doMath(),
                    a.newSlides.css({
                        width: a.computedW,
                        "float": "left",
                        display: "block"
                    }),
                    a.vars.smoothHeight && m.smoothHeight()
                }, "init" === e ? 100 : 0))
            }
            u || a.slides.removeClass(n + "active-slide").eq(a.currentSlide).addClass(n + "active-slide"),
            a.vars.init(a)
        }
        ,
        a.doMath = function() {
            var e = a.slides.first()
              , t = a.vars.itemMargin
              , n = a.vars.minItems
              , i = a.vars.maxItems;
            a.w = void 0 === a.viewport ? a.width() : a.viewport.width(),
            a.h = e.height(),
            a.boxPadding = e.outerWidth() - e.width(),
            u ? (a.itemT = a.vars.itemWidth + t,
            a.minW = n ? n * a.itemT : a.w,
            a.maxW = i ? i * a.itemT - t : a.w,
            a.itemW = a.minW > a.w ? (a.w - t * (n - 1)) / n : a.maxW < a.w ? (a.w - t * (i - 1)) / i : a.vars.itemWidth > a.w ? a.w : a.vars.itemWidth,
            a.visible = Math.floor(a.w / a.itemW),
            a.move = a.vars.move > 0 && a.vars.move < a.visible ? a.vars.move : a.visible,
            a.pagingCount = Math.ceil((a.count - a.visible) / a.move + 1),
            a.last = a.pagingCount - 1,
            a.limit = 1 === a.pagingCount ? 0 : a.vars.itemWidth > a.w ? a.itemW * (a.count - 1) + t * (a.count - 1) : (a.itemW + t) * a.count - a.w - t) : (a.itemW = a.w,
            a.pagingCount = a.count,
            a.last = a.count - 1),
            a.computedW = a.itemW - a.boxPadding
        }
        ,
        a.update = function(e, t) {
            a.doMath(),
            u || (e < a.currentSlide ? a.currentSlide += 1 : e <= a.currentSlide && 0 !== e && (a.currentSlide -= 1),
            a.animatingTo = a.currentSlide),
            a.vars.controlNav && !a.manualControls && ("add" === t && !u || a.pagingCount > a.controlNav.length ? m.controlNav.update("add") : ("remove" === t && !u || a.pagingCount < a.controlNav.length) && (u && a.currentSlide > a.last && (a.currentSlide -= 1,
            a.animatingTo -= 1),
            m.controlNav.update("remove", a.last))),
            a.vars.directionNav && m.directionNav.update()
        }
        ,
        a.addSlide = function(e, t) {
            var n = $(e);
            a.count += 1,
            a.last = a.count - 1,
            c && d ? void 0 !== t ? a.slides.eq(a.count - t).after(n) : a.container.prepend(n) : void 0 !== t ? a.slides.eq(t).before(n) : a.container.append(n),
            a.update(t, "add"),
            a.slides = $(a.vars.selector + ":not(.clone)", a),
            a.setup(),
            a.vars.added(a)
        }
        ,
        a.removeSlide = function(e) {
            var t = isNaN(e) ? a.slides.index($(e)) : e;
            a.count -= 1,
            a.last = a.count - 1,
            isNaN(e) ? $(e, a.slides).remove() : c && d ? a.slides.eq(a.last).remove() : a.slides.eq(e).remove(),
            a.doMath(),
            a.update(t, "remove"),
            a.slides = $(a.vars.selector + ":not(.clone)", a),
            a.setup(),
            a.vars.removed(a)
        }
        ,
        m.init()
    }
    ,
    $(window).blur(function(e) {
        focused = !1
    }).focus(function(e) {
        focused = !0
    }),
    $.flexslider.defaults = {
        namespace: "flex-",
        selector: ".slides > li",
        animation: "fade",
        easing: "swing",
        direction: "horizontal",
        reverse: !1,
        animationLoop: !0,
        smoothHeight: !1,
        startAt: 0,
        slideshow: !0,
        slideshowSpeed: 7e3,
        animationSpeed: 600,
        initDelay: 0,
        randomize: !1,
        fadeFirstSlide: !0,
        thumbCaptions: !1,
        pauseOnAction: !0,
        pauseOnHover: !1,
        pauseInvisible: !0,
        useCSS: !0,
        touch: !0,
        video: !1,
        controlNav: !0,
        directionNav: !0,
        prevText: "Previous",
        nextText: "Next",
        keyboard: !0,
        multipleKeyboard: !1,
        mousewheel: !1,
        pausePlay: !1,
        pauseText: "Pause",
        playText: "Play",
        controlsContainer: "",
        manualControls: "",
        sync: "",
        asNavFor: "",
        itemWidth: 0,
        itemMargin: 0,
        minItems: 1,
        maxItems: 0,
        move: 0,
        allowOneSlide: !0,
        start: function() {},
        before: function() {},
        after: function() {},
        end: function() {},
        added: function() {},
        removed: function() {},
        init: function() {}
    },
    $.fn.flexslider = function(e) {
        if (void 0 === e && (e = {}),
        "object" == typeof e)
            return this.each(function() {
                var t = $(this)
                  , a = e.selector ? e.selector : ".slides > li"
                  , n = t.find(a);
                1 === n.length && e.allowOneSlide === !0 || 0 === n.length ? (n.fadeIn(400),
                e.start && e.start(t)) : void 0 === t.data("flexslider") && new $.flexslider(this,e)
            });
        var t = $(this).data("flexslider");
        switch (e) {
        case "play":
            t.play();
            break;
        case "pause":
            t.pause();
            break;
        case "stop":
            t.stop();
            break;
        case "next":
            t.flexAnimate(t.getTarget("next"), !0);
            break;
        case "prev":
        case "previous":
            t.flexAnimate(t.getTarget("prev"), !0);
            break;
        default:
            "number" == typeof e && t.flexAnimate(e, !0)
        }
    }
}(jQuery);
/*!
* imagesLoaded PACKAGED v3.2.0
* JavaScript is all like "You images are done yet or what?"
* MIT License
*/
(function() {
    "use strict";
    function e() {}
    function t(e, t) {
        for (var n = e.length; n--; )
            if (e[n].listener === t)
                return n;
        return -1
    }
    function n(e) {
        return function() {
            return this[e].apply(this, arguments)
        }
    }
    var i = e.prototype
      , r = this
      , s = r.EventEmitter;
    i.getListeners = function(e) {
        var t, n, i = this._getEvents();
        if ("object" == typeof e) {
            t = {};
            for (n in i)
                i.hasOwnProperty(n) && e.test(n) && (t[n] = i[n])
        } else
            t = i[e] || (i[e] = []);
        return t
    }
    ,
    i.flattenListeners = function(e) {
        var t, n = [];
        for (t = 0; t < e.length; t += 1)
            n.push(e[t].listener);
        return n
    }
    ,
    i.getListenersAsObject = function(e) {
        var t, n = this.getListeners(e);
        return n instanceof Array && (t = {},
        t[e] = n),
        t || n
    }
    ,
    i.addListener = function(e, n) {
        var i, r = this.getListenersAsObject(e), s = "object" == typeof n;
        for (i in r)
            r.hasOwnProperty(i) && -1 === t(r[i], n) && r[i].push(s ? n : {
                listener: n,
                once: !1
            });
        return this
    }
    ,
    i.on = n("addListener"),
    i.addOnceListener = function(e, t) {
        return this.addListener(e, {
            listener: t,
            once: !0
        })
    }
    ,
    i.once = n("addOnceListener"),
    i.defineEvent = function(e) {
        return this.getListeners(e),
        this
    }
    ,
    i.defineEvents = function(e) {
        for (var t = 0; t < e.length; t += 1)
            this.defineEvent(e[t]);
        return this
    }
    ,
    i.removeListener = function(e, n) {
        var i, r, s = this.getListenersAsObject(e);
        for (r in s)
            s.hasOwnProperty(r) && (i = t(s[r], n),
            -1 !== i && s[r].splice(i, 1));
        return this
    }
    ,
    i.off = n("removeListener"),
    i.addListeners = function(e, t) {
        return this.manipulateListeners(!1, e, t)
    }
    ,
    i.removeListeners = function(e, t) {
        return this.manipulateListeners(!0, e, t)
    }
    ,
    i.manipulateListeners = function(e, t, n) {
        var i, r, s = e ? this.removeListener : this.addListener, o = e ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
            for (i = n.length; i--; )
                s.call(this, t, n[i]);
        else
            for (i in t)
                t.hasOwnProperty(i) && (r = t[i]) && ("function" == typeof r ? s.call(this, i, r) : o.call(this, i, r));
        return this
    }
    ,
    i.removeEvent = function(e) {
        var t, n = typeof e, i = this._getEvents();
        if ("string" === n)
            delete i[e];
        else if ("object" === n)
            for (t in i)
                i.hasOwnProperty(t) && e.test(t) && delete i[t];
        else
            delete this._events;
        return this
    }
    ,
    i.removeAllListeners = n("removeEvent"),
    i.emitEvent = function(e, t) {
        var n, i, r, s, o = this.getListenersAsObject(e);
        for (r in o)
            if (o.hasOwnProperty(r))
                for (i = o[r].length; i--; )
                    n = o[r][i],
                    n.once === !0 && this.removeListener(e, n.listener),
                    s = n.listener.apply(this, t || []),
                    s === this._getOnceReturnValue() && this.removeListener(e, n.listener);
        return this
    }
    ,
    i.trigger = n("emitEvent"),
    i.emit = function(e) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(e, t)
    }
    ,
    i.setOnceReturnValue = function(e) {
        return this._onceReturnValue = e,
        this
    }
    ,
    i._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }
    ,
    i._getEvents = function() {
        return this._events || (this._events = {})
    }
    ,
    e.noConflict = function() {
        return r.EventEmitter = s,
        e
    }
    ,
    "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
        return e
    }) : "object" == typeof module && module.exports ? module.exports = e : this.EventEmitter = e
}
).call(this),
function(e) {
    function t(t) {
        var n = e.event;
        return n.target = n.target || n.srcElement || t,
        n
    }
    var n = document.documentElement
      , i = function() {};
    n.addEventListener ? i = function(e, t, n) {
        e.addEventListener(t, n, !1)
    }
    : n.attachEvent && (i = function(e, n, i) {
        e[n + i] = i.handleEvent ? function() {
            var n = t(e);
            i.handleEvent.call(i, n)
        }
        : function() {
            var n = t(e);
            i.call(e, n)
        }
        ,
        e.attachEvent("on" + n, e[n + i])
    }
    );
    var r = function() {};
    n.removeEventListener ? r = function(e, t, n) {
        e.removeEventListener(t, n, !1)
    }
    : n.detachEvent && (r = function(e, t, n) {
        e.detachEvent("on" + t, e[t + n]);
        try {
            delete e[t + n]
        } catch (i) {
            e[t + n] = void 0
        }
    }
    );
    var s = {
        bind: i,
        unbind: r
    };
    "function" == typeof define && define.amd ? define("eventie/eventie", s) : e.eventie = s
}(this),
function(e, t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function(n, i) {
        return t(e, n, i)
    }) : "object" == typeof module && module.exports ? module.exports = t(e, require("wolfy87-eventemitter"), require("eventie")) : e.imagesLoaded = t(e, e.EventEmitter, e.eventie)
}(window, function(e, t, n) {
    function i(e, t) {
        for (var n in t)
            e[n] = t[n];
        return e
    }
    function r(e) {
        return "[object Array]" == f.call(e)
    }
    function s(e) {
        var t = [];
        if (r(e))
            t = e;
        else if ("number" == typeof e.length)
            for (var n = 0; n < e.length; n++)
                t.push(e[n]);
        else
            t.push(e);
        return t
    }
    function o(e, t, n) {
        if (!(this instanceof o))
            return new o(e,t,n);
        "string" == typeof e && (e = document.querySelectorAll(e)),
        this.elements = s(e),
        this.options = i({}, this.options),
        "function" == typeof t ? n = t : i(this.options, t),
        n && this.on("always", n),
        this.getImages(),
        u && (this.jqDeferred = new u.Deferred);
        var r = this;
        setTimeout(function() {
            r.check()
        })
    }
    function h(e) {
        this.img = e
    }
    function a(e, t) {
        this.url = e,
        this.element = t,
        this.img = new Image
    }
    var u = e.jQuery
      , c = e.console
      , f = Object.prototype.toString;
    o.prototype = new t,
    o.prototype.options = {},
    o.prototype.getImages = function() {
        this.images = [];
        for (var e = 0; e < this.elements.length; e++) {
            var t = this.elements[e];
            this.addElementImages(t)
        }
    }
    ,
    o.prototype.addElementImages = function(e) {
        "IMG" == e.nodeName && this.addImage(e),
        this.options.background === !0 && this.addElementBackgroundImages(e);
        var t = e.nodeType;
        if (t && d[t]) {
            for (var n = e.querySelectorAll("img"), i = 0; i < n.length; i++) {
                var r = n[i];
                this.addImage(r)
            }
            if ("string" == typeof this.options.background) {
                var s = e.querySelectorAll(this.options.background);
                for (i = 0; i < s.length; i++) {
                    var o = s[i];
                    this.addElementBackgroundImages(o)
                }
            }
        }
    }
    ;
    var d = {
        1: !0,
        9: !0,
        11: !0
    };
    o.prototype.addElementBackgroundImages = function(e) {
        for (var t = m(e), n = /url\(['"]*([^'"\)]+)['"]*\)/gi, i = n.exec(t.backgroundImage); null !== i; ) {
            var r = i && i[1];
            r && this.addBackground(r, e),
            i = n.exec(t.backgroundImage)
        }
    }
    ;
    var m = e.getComputedStyle || function(e) {
        return e.currentStyle
    }
    ;
    return o.prototype.addImage = function(e) {
        var t = new h(e);
        this.images.push(t)
    }
    ,
    o.prototype.addBackground = function(e, t) {
        var n = new a(e,t);
        this.images.push(n)
    }
    ,
    o.prototype.check = function() {
        function e(e, n, i) {
            setTimeout(function() {
                t.progress(e, n, i)
            })
        }
        var t = this;
        if (this.progressedCount = 0,
        this.hasAnyBroken = !1,
        !this.images.length)
            return void this.complete();
        for (var n = 0; n < this.images.length; n++) {
            var i = this.images[n];
            i.once("progress", e),
            i.check()
        }
    }
    ,
    o.prototype.progress = function(e, t, n) {
        this.progressedCount++,
        this.hasAnyBroken = this.hasAnyBroken || !e.isLoaded,
        this.emit("progress", this, e, t),
        this.jqDeferred && this.jqDeferred.notify && this.jqDeferred.notify(this, e),
        this.progressedCount == this.images.length && this.complete(),
        this.options.debug && c && c.log("progress: " + n, e, t)
    }
    ,
    o.prototype.complete = function() {
        var e = this.hasAnyBroken ? "fail" : "done";
        if (this.isComplete = !0,
        this.emit(e, this),
        this.emit("always", this),
        this.jqDeferred) {
            var t = this.hasAnyBroken ? "reject" : "resolve";
            this.jqDeferred[t](this)
        }
    }
    ,
    h.prototype = new t,
    h.prototype.check = function() {
        var e = this.getIsImageComplete();
        return e ? void this.confirm(0 !== this.img.naturalWidth, "naturalWidth") : (this.proxyImage = new Image,
        n.bind(this.proxyImage, "load", this),
        n.bind(this.proxyImage, "error", this),
        n.bind(this.img, "load", this),
        n.bind(this.img, "error", this),
        void (this.proxyImage.src = this.img.src))
    }
    ,
    h.prototype.getIsImageComplete = function() {
        return this.img.complete && void 0 !== this.img.naturalWidth
    }
    ,
    h.prototype.confirm = function(e, t) {
        this.isLoaded = e,
        this.emit("progress", this, this.img, t)
    }
    ,
    h.prototype.handleEvent = function(e) {
        var t = "on" + e.type;
        this[t] && this[t](e)
    }
    ,
    h.prototype.onload = function() {
        this.confirm(!0, "onload"),
        this.unbindEvents()
    }
    ,
    h.prototype.onerror = function() {
        this.confirm(!1, "onerror"),
        this.unbindEvents()
    }
    ,
    h.prototype.unbindEvents = function() {
        n.unbind(this.proxyImage, "load", this),
        n.unbind(this.proxyImage, "error", this),
        n.unbind(this.img, "load", this),
        n.unbind(this.img, "error", this)
    }
    ,
    a.prototype = new h,
    a.prototype.check = function() {
        n.bind(this.img, "load", this),
        n.bind(this.img, "error", this),
        this.img.src = this.url;
        var e = this.getIsImageComplete();
        e && (this.confirm(0 !== this.img.naturalWidth, "naturalWidth"),
        this.unbindEvents())
    }
    ,
    a.prototype.unbindEvents = function() {
        n.unbind(this.img, "load", this),
        n.unbind(this.img, "error", this)
    }
    ,
    a.prototype.confirm = function(e, t) {
        this.isLoaded = e,
        this.emit("progress", this, this.element, t)
    }
    ,
    o.makeJQueryPlugin = function(t) {
        t = t || e.jQuery,
        t && (u = t,
        u.fn.imagesLoaded = function(e, t) {
            var n = new o(this,e,t);
            return n.jqDeferred.promise(u(this))
        }
        )
    }
    ,
    o.makeJQueryPlugin(),
    o
});
/*!
* Masonry PACKAGED v3.3.2
* Cascading grid layout library
* http://masonry.desandro.com
* MIT License
* by David DeSandro
*/
!function(a) {
    function b() {}
    function c(a) {
        function c(b) {
            b.prototype.option || (b.prototype.option = function(b) {
                a.isPlainObject(b) && (this.options = a.extend(!0, this.options, b))
            }
            )
        }
        function e(b, c) {
            a.fn[b] = function(e) {
                if ("string" == typeof e) {
                    for (var g = d.call(arguments, 1), h = 0, i = this.length; i > h; h++) {
                        var j = this[h]
                          , k = a.data(j, b);
                        if (k)
                            if (a.isFunction(k[e]) && "_" !== e.charAt(0)) {
                                var l = k[e].apply(k, g);
                                if (void 0 !== l)
                                    return l
                            } else
                                f("no such method '" + e + "' for " + b + " instance");
                        else
                            f("cannot call methods on " + b + " prior to initialization; attempted to call '" + e + "'")
                    }
                    return this
                }
                return this.each(function() {
                    var d = a.data(this, b);
                    d ? (d.option(e),
                    d._init()) : (d = new c(this,e),
                    a.data(this, b, d))
                })
            }
        }
        if (a) {
            var f = "undefined" == typeof console ? b : function(a) {
                console.error(a)
            }
            ;
            return a.bridget = function(a, b) {
                c(b),
                e(a, b)
            }
            ,
            a.bridget
        }
    }
    var d = Array.prototype.slice;
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], c) : c("object" == typeof exports ? require("jquery") : a.jQuery)
}(window),
function(a) {
    function b(b) {
        var c = a.event;
        return c.target = c.target || c.srcElement || b,
        c
    }
    var c = document.documentElement
      , d = function() {};
    c.addEventListener ? d = function(a, b, c) {
        a.addEventListener(b, c, !1)
    }
    : c.attachEvent && (d = function(a, c, d) {
        a[c + d] = d.handleEvent ? function() {
            var c = b(a);
            d.handleEvent.call(d, c)
        }
        : function() {
            var c = b(a);
            d.call(a, c)
        }
        ,
        a.attachEvent("on" + c, a[c + d])
    }
    );
    var e = function() {};
    c.removeEventListener ? e = function(a, b, c) {
        a.removeEventListener(b, c, !1)
    }
    : c.detachEvent && (e = function(a, b, c) {
        a.detachEvent("on" + b, a[b + c]);
        try {
            delete a[b + c]
        } catch (d) {
            a[b + c] = void 0
        }
    }
    );
    var f = {
        bind: d,
        unbind: e
    };
    "function" == typeof define && define.amd ? define("eventie/eventie", f) : "object" == typeof exports ? module.exports = f : a.eventie = f
}(window),
function() {
    function a() {}
    function b(a, b) {
        for (var c = a.length; c--; )
            if (a[c].listener === b)
                return c;
        return -1
    }
    function c(a) {
        return function() {
            return this[a].apply(this, arguments)
        }
    }
    var d = a.prototype
      , e = this
      , f = e.EventEmitter;
    d.getListeners = function(a) {
        var b, c, d = this._getEvents();
        if (a instanceof RegExp) {
            b = {};
            for (c in d)
                d.hasOwnProperty(c) && a.test(c) && (b[c] = d[c])
        } else
            b = d[a] || (d[a] = []);
        return b
    }
    ,
    d.flattenListeners = function(a) {
        var b, c = [];
        for (b = 0; b < a.length; b += 1)
            c.push(a[b].listener);
        return c
    }
    ,
    d.getListenersAsObject = function(a) {
        var b, c = this.getListeners(a);
        return c instanceof Array && (b = {},
        b[a] = c),
        b || c
    }
    ,
    d.addListener = function(a, c) {
        var d, e = this.getListenersAsObject(a), f = "object" == typeof c;
        for (d in e)
            e.hasOwnProperty(d) && -1 === b(e[d], c) && e[d].push(f ? c : {
                listener: c,
                once: !1
            });
        return this
    }
    ,
    d.on = c("addListener"),
    d.addOnceListener = function(a, b) {
        return this.addListener(a, {
            listener: b,
            once: !0
        })
    }
    ,
    d.once = c("addOnceListener"),
    d.defineEvent = function(a) {
        return this.getListeners(a),
        this
    }
    ,
    d.defineEvents = function(a) {
        for (var b = 0; b < a.length; b += 1)
            this.defineEvent(a[b]);
        return this
    }
    ,
    d.removeListener = function(a, c) {
        var d, e, f = this.getListenersAsObject(a);
        for (e in f)
            f.hasOwnProperty(e) && (d = b(f[e], c),
            -1 !== d && f[e].splice(d, 1));
        return this
    }
    ,
    d.off = c("removeListener"),
    d.addListeners = function(a, b) {
        return this.manipulateListeners(!1, a, b)
    }
    ,
    d.removeListeners = function(a, b) {
        return this.manipulateListeners(!0, a, b)
    }
    ,
    d.manipulateListeners = function(a, b, c) {
        var d, e, f = a ? this.removeListener : this.addListener, g = a ? this.removeListeners : this.addListeners;
        if ("object" != typeof b || b instanceof RegExp)
            for (d = c.length; d--; )
                f.call(this, b, c[d]);
        else
            for (d in b)
                b.hasOwnProperty(d) && (e = b[d]) && ("function" == typeof e ? f.call(this, d, e) : g.call(this, d, e));
        return this
    }
    ,
    d.removeEvent = function(a) {
        var b, c = typeof a, d = this._getEvents();
        if ("string" === c)
            delete d[a];
        else if (a instanceof RegExp)
            for (b in d)
                d.hasOwnProperty(b) && a.test(b) && delete d[b];
        else
            delete this._events;
        return this
    }
    ,
    d.removeAllListeners = c("removeEvent"),
    d.emitEvent = function(a, b) {
        var c, d, e, f, g = this.getListenersAsObject(a);
        for (e in g)
            if (g.hasOwnProperty(e))
                for (d = g[e].length; d--; )
                    c = g[e][d],
                    c.once === !0 && this.removeListener(a, c.listener),
                    f = c.listener.apply(this, b || []),
                    f === this._getOnceReturnValue() && this.removeListener(a, c.listener);
        return this
    }
    ,
    d.trigger = c("emitEvent"),
    d.emit = function(a) {
        var b = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(a, b)
    }
    ,
    d.setOnceReturnValue = function(a) {
        return this._onceReturnValue = a,
        this
    }
    ,
    d._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }
    ,
    d._getEvents = function() {
        return this._events || (this._events = {})
    }
    ,
    a.noConflict = function() {
        return e.EventEmitter = f,
        a
    }
    ,
    "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
        return a
    }) : "object" == typeof module && module.exports ? module.exports = a : e.EventEmitter = a
}
.call(this),
function(a) {
    function b(a) {
        if (a) {
            if ("string" == typeof d[a])
                return a;
            a = a.charAt(0).toUpperCase() + a.slice(1);
            for (var b, e = 0, f = c.length; f > e; e++)
                if (b = c[e] + a,
                "string" == typeof d[b])
                    return b
        }
    }
    var c = "Webkit Moz ms Ms O".split(" ")
      , d = document.documentElement.style;
    "function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function() {
        return b
    }) : "object" == typeof exports ? module.exports = b : a.getStyleProperty = b
}(window),
function(a) {
    function b(a) {
        var b = parseFloat(a)
          , c = -1 === a.indexOf("%") && !isNaN(b);
        return c && b
    }
    function c() {}
    function d() {
        for (var a = {
            width: 0,
            height: 0,
            innerWidth: 0,
            innerHeight: 0,
            outerWidth: 0,
            outerHeight: 0
        }, b = 0, c = g.length; c > b; b++) {
            var d = g[b];
            a[d] = 0
        }
        return a
    }
    function e(c) {
        function e() {
            if (!m) {
                m = !0;
                var d = a.getComputedStyle;
                if (j = function() {
                    var a = d ? function(a) {
                        return d(a, null)
                    }
                    : function(a) {
                        return a.currentStyle
                    }
                    ;
                    return function(b) {
                        var c = a(b);
                        return c || f("Style returned " + c + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"),
                        c
                    }
                }(),
                k = c("boxSizing")) {
                    var e = document.createElement("div");
                    e.style.width = "200px",
                    e.style.padding = "1px 2px 3px 4px",
                    e.style.borderStyle = "solid",
                    e.style.borderWidth = "1px 2px 3px 4px",
                    e.style[k] = "border-box";
                    var g = document.body || document.documentElement;
                    g.appendChild(e);
                    var h = j(e);
                    l = 200 === b(h.width),
                    g.removeChild(e)
                }
            }
        }
        function h(a) {
            if (e(),
            "string" == typeof a && (a = document.querySelector(a)),
            a && "object" == typeof a && a.nodeType) {
                var c = j(a);
                if ("none" === c.display)
                    return d();
                var f = {};
                f.width = a.offsetWidth,
                f.height = a.offsetHeight;
                for (var h = f.isBorderBox = !(!k || !c[k] || "border-box" !== c[k]), m = 0, n = g.length; n > m; m++) {
                    var o = g[m]
                      , p = c[o];
                    p = i(a, p);
                    var q = parseFloat(p);
                    f[o] = isNaN(q) ? 0 : q
                }
                var r = f.paddingLeft + f.paddingRight
                  , s = f.paddingTop + f.paddingBottom
                  , t = f.marginLeft + f.marginRight
                  , u = f.marginTop + f.marginBottom
                  , v = f.borderLeftWidth + f.borderRightWidth
                  , w = f.borderTopWidth + f.borderBottomWidth
                  , x = h && l
                  , y = b(c.width);
                y !== !1 && (f.width = y + (x ? 0 : r + v));
                var z = b(c.height);
                return z !== !1 && (f.height = z + (x ? 0 : s + w)),
                f.innerWidth = f.width - (r + v),
                f.innerHeight = f.height - (s + w),
                f.outerWidth = f.width + t,
                f.outerHeight = f.height + u,
                f
            }
        }
        function i(b, c) {
            if (a.getComputedStyle || -1 === c.indexOf("%"))
                return c;
            var d = b.style
              , e = d.left
              , f = b.runtimeStyle
              , g = f && f.left;
            return g && (f.left = b.currentStyle.left),
            d.left = c,
            c = d.pixelLeft,
            d.left = e,
            g && (f.left = g),
            c
        }
        var j, k, l, m = !1;
        return h
    }
    var f = "undefined" == typeof console ? c : function(a) {
        console.error(a)
    }
      , g = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
    "function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], e) : "object" == typeof exports ? module.exports = e(require("desandro-get-style-property")) : a.getSize = e(a.getStyleProperty)
}(window),
function(a) {
    function b(a) {
        "function" == typeof a && (b.isReady ? a() : g.push(a))
    }
    function c(a) {
        var c = "readystatechange" === a.type && "complete" !== f.readyState;
        b.isReady || c || d()
    }
    function d() {
        b.isReady = !0;
        for (var a = 0, c = g.length; c > a; a++) {
            var d = g[a];
            d()
        }
    }
    function e(e) {
        return "complete" === f.readyState ? d() : (e.bind(f, "DOMContentLoaded", c),
        e.bind(f, "readystatechange", c),
        e.bind(a, "load", c)),
        b
    }
    var f = a.document
      , g = [];
    b.isReady = !1,
    "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], e) : "object" == typeof exports ? module.exports = e(require("eventie")) : a.docReady = e(a.eventie)
}(window),
function(a) {
    function b(a, b) {
        return a[g](b)
    }
    function c(a) {
        if (!a.parentNode) {
            var b = document.createDocumentFragment();
            b.appendChild(a)
        }
    }
    function d(a, b) {
        c(a);
        for (var d = a.parentNode.querySelectorAll(b), e = 0, f = d.length; f > e; e++)
            if (d[e] === a)
                return !0;
        return !1
    }
    function e(a, d) {
        return c(a),
        b(a, d)
    }
    var f, g = function() {
        if (a.matches)
            return "matches";
        if (a.matchesSelector)
            return "matchesSelector";
        for (var b = ["webkit", "moz", "ms", "o"], c = 0, d = b.length; d > c; c++) {
            var e = b[c]
              , f = e + "MatchesSelector";
            if (a[f])
                return f
        }
    }();
    if (g) {
        var h = document.createElement("div")
          , i = b(h, "div");
        f = i ? b : e
    } else
        f = d;
    "function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function() {
        return f
    }) : "object" == typeof exports ? module.exports = f : window.matchesSelector = f
}(Element.prototype),
function(a, b) {
    "function" == typeof define && define.amd ? define("fizzy-ui-utils/utils", ["doc-ready/doc-ready", "matches-selector/matches-selector"], function(c, d) {
        return b(a, c, d)
    }) : "object" == typeof exports ? module.exports = b(a, require("doc-ready"), require("desandro-matches-selector")) : a.fizzyUIUtils = b(a, a.docReady, a.matchesSelector)
}(window, function(a, b, c) {
    var d = {};
    d.extend = function(a, b) {
        for (var c in b)
            a[c] = b[c];
        return a
    }
    ,
    d.modulo = function(a, b) {
        return (a % b + b) % b
    }
    ;
    var e = Object.prototype.toString;
    d.isArray = function(a) {
        return "[object Array]" == e.call(a)
    }
    ,
    d.makeArray = function(a) {
        var b = [];
        if (d.isArray(a))
            b = a;
        else if (a && "number" == typeof a.length)
            for (var c = 0, e = a.length; e > c; c++)
                b.push(a[c]);
        else
            b.push(a);
        return b
    }
    ,
    d.indexOf = Array.prototype.indexOf ? function(a, b) {
        return a.indexOf(b)
    }
    : function(a, b) {
        for (var c = 0, d = a.length; d > c; c++)
            if (a[c] === b)
                return c;
        return -1
    }
    ,
    d.removeFrom = function(a, b) {
        var c = d.indexOf(a, b);
        -1 != c && a.splice(c, 1)
    }
    ,
    d.isElement = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function(a) {
        return a instanceof HTMLElement
    }
    : function(a) {
        return a && "object" == typeof a && 1 == a.nodeType && "string" == typeof a.nodeName
    }
    ,
    d.setText = function() {
        function a(a, c) {
            b = b || (void 0 !== document.documentElement.textContent ? "textContent" : "innerText"),
            a[b] = c
        }
        var b;
        return a
    }(),
    d.getParent = function(a, b) {
        for (; a != document.body; )
            if (a = a.parentNode,
            c(a, b))
                return a
    }
    ,
    d.getQueryElement = function(a) {
        return "string" == typeof a ? document.querySelector(a) : a
    }
    ,
    d.handleEvent = function(a) {
        var b = "on" + a.type;
        this[b] && this[b](a)
    }
    ,
    d.filterFindElements = function(a, b) {
        a = d.makeArray(a);
        for (var e = [], f = 0, g = a.length; g > f; f++) {
            var h = a[f];
            if (d.isElement(h))
                if (b) {
                    c(h, b) && e.push(h);
                    for (var i = h.querySelectorAll(b), j = 0, k = i.length; k > j; j++)
                        e.push(i[j])
                } else
                    e.push(h)
        }
        return e
    }
    ,
    d.debounceMethod = function(a, b, c) {
        var d = a.prototype[b]
          , e = b + "Timeout";
        a.prototype[b] = function() {
            var a = this[e];
            a && clearTimeout(a);
            var b = arguments
              , f = this;
            this[e] = setTimeout(function() {
                d.apply(f, b),
                delete f[e]
            }, c || 100)
        }
    }
    ,
    d.toDashed = function(a) {
        return a.replace(/(.)([A-Z])/g, function(a, b, c) {
            return b + "-" + c
        }).toLowerCase()
    }
    ;
    var f = a.console;
    return d.htmlInit = function(c, e) {
        b(function() {
            for (var b = d.toDashed(e), g = document.querySelectorAll(".js-" + b), h = "data-" + b + "-options", i = 0, j = g.length; j > i; i++) {
                var k, l = g[i], m = l.getAttribute(h);
                try {
                    k = m && JSON.parse(m)
                } catch (n) {
                    f && f.error("Error parsing " + h + " on " + l.nodeName.toLowerCase() + (l.id ? "#" + l.id : "") + ": " + n);
                    continue
                }
                var o = new c(l,k)
                  , p = a.jQuery;
                p && p.data(l, e, o)
            }
        })
    }
    ,
    d
}),
function(a, b) {
    "function" == typeof define && define.amd ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property", "fizzy-ui-utils/utils"], function(c, d, e, f) {
        return b(a, c, d, e, f)
    }) : "object" == typeof exports ? module.exports = b(a, require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property"), require("fizzy-ui-utils")) : (a.Outlayer = {},
    a.Outlayer.Item = b(a, a.EventEmitter, a.getSize, a.getStyleProperty, a.fizzyUIUtils))
}(window, function(a, b, c, d, e) {
    function f(a) {
        for (var b in a)
            return !1;
        return b = null,
        !0
    }
    function g(a, b) {
        a && (this.element = a,
        this.layout = b,
        this.position = {
            x: 0,
            y: 0
        },
        this._create())
    }
    function h(a) {
        return a.replace(/([A-Z])/g, function(a) {
            return "-" + a.toLowerCase()
        })
    }
    var i = a.getComputedStyle
      , j = i ? function(a) {
        return i(a, null)
    }
    : function(a) {
        return a.currentStyle
    }
      , k = d("transition")
      , l = d("transform")
      , m = k && l
      , n = !!d("perspective")
      , o = {
        WebkitTransition: "webkitTransitionEnd",
        MozTransition: "transitionend",
        OTransition: "otransitionend",
        transition: "transitionend"
    }[k]
      , p = ["transform", "transition", "transitionDuration", "transitionProperty"]
      , q = function() {
        for (var a = {}, b = 0, c = p.length; c > b; b++) {
            var e = p[b]
              , f = d(e);
            f && f !== e && (a[e] = f)
        }
        return a
    }();
    e.extend(g.prototype, b.prototype),
    g.prototype._create = function() {
        this._transn = {
            ingProperties: {},
            clean: {},
            onEnd: {}
        },
        this.css({
            position: "absolute"
        })
    }
    ,
    g.prototype.handleEvent = function(a) {
        var b = "on" + a.type;
        this[b] && this[b](a)
    }
    ,
    g.prototype.getSize = function() {
        this.size = c(this.element)
    }
    ,
    g.prototype.css = function(a) {
        var b = this.element.style;
        for (var c in a) {
            var d = q[c] || c;
            b[d] = a[c]
        }
    }
    ,
    g.prototype.getPosition = function() {
        var a = j(this.element)
          , b = this.layout.options
          , c = b.isOriginLeft
          , d = b.isOriginTop
          , e = a[c ? "left" : "right"]
          , f = a[d ? "top" : "bottom"]
          , g = this.layout.size
          , h = -1 != e.indexOf("%") ? parseFloat(e) / 100 * g.width : parseInt(e, 10)
          , i = -1 != f.indexOf("%") ? parseFloat(f) / 100 * g.height : parseInt(f, 10);
        h = isNaN(h) ? 0 : h,
        i = isNaN(i) ? 0 : i,
        h -= c ? g.paddingLeft : g.paddingRight,
        i -= d ? g.paddingTop : g.paddingBottom,
        this.position.x = h,
        this.position.y = i
    }
    ,
    g.prototype.layoutPosition = function() {
        var a = this.layout.size
          , b = this.layout.options
          , c = {}
          , d = b.isOriginLeft ? "paddingLeft" : "paddingRight"
          , e = b.isOriginLeft ? "left" : "right"
          , f = b.isOriginLeft ? "right" : "left"
          , g = this.position.x + a[d];
        c[e] = this.getXValue(g),
        c[f] = "";
        var h = b.isOriginTop ? "paddingTop" : "paddingBottom"
          , i = b.isOriginTop ? "top" : "bottom"
          , j = b.isOriginTop ? "bottom" : "top"
          , k = this.position.y + a[h];
        c[i] = this.getYValue(k),
        c[j] = "",
        this.css(c),
        this.emitEvent("layout", [this])
    }
    ,
    g.prototype.getXValue = function(a) {
        var b = this.layout.options;
        return b.percentPosition && !b.isHorizontal ? a / this.layout.size.width * 100 + "%" : a + "px"
    }
    ,
    g.prototype.getYValue = function(a) {
        var b = this.layout.options;
        return b.percentPosition && b.isHorizontal ? a / this.layout.size.height * 100 + "%" : a + "px"
    }
    ,
    g.prototype._transitionTo = function(a, b) {
        this.getPosition();
        var c = this.position.x
          , d = this.position.y
          , e = parseInt(a, 10)
          , f = parseInt(b, 10)
          , g = e === this.position.x && f === this.position.y;
        if (this.setPosition(a, b),
        g && !this.isTransitioning)
            return void this.layoutPosition();
        var h = a - c
          , i = b - d
          , j = {};
        j.transform = this.getTranslate(h, i),
        this.transition({
            to: j,
            onTransitionEnd: {
                transform: this.layoutPosition
            },
            isCleaning: !0
        })
    }
    ,
    g.prototype.getTranslate = function(a, b) {
        var c = this.layout.options;
        return a = c.isOriginLeft ? a : -a,
        b = c.isOriginTop ? b : -b,
        n ? "translate3d(" + a + "px, " + b + "px, 0)" : "translate(" + a + "px, " + b + "px)"
    }
    ,
    g.prototype.goTo = function(a, b) {
        this.setPosition(a, b),
        this.layoutPosition()
    }
    ,
    g.prototype.moveTo = m ? g.prototype._transitionTo : g.prototype.goTo,
    g.prototype.setPosition = function(a, b) {
        this.position.x = parseInt(a, 10),
        this.position.y = parseInt(b, 10)
    }
    ,
    g.prototype._nonTransition = function(a) {
        this.css(a.to),
        a.isCleaning && this._removeStyles(a.to);
        for (var b in a.onTransitionEnd)
            a.onTransitionEnd[b].call(this)
    }
    ,
    g.prototype._transition = function(a) {
        if (!parseFloat(this.layout.options.transitionDuration))
            return void this._nonTransition(a);
        var b = this._transn;
        for (var c in a.onTransitionEnd)
            b.onEnd[c] = a.onTransitionEnd[c];
        for (c in a.to)
            b.ingProperties[c] = !0,
            a.isCleaning && (b.clean[c] = !0);
        if (a.from) {
            this.css(a.from);
            var d = this.element.offsetHeight;
            d = null
        }
        this.enableTransition(a.to),
        this.css(a.to),
        this.isTransitioning = !0
    }
    ;
    var r = "opacity," + h(q.transform || "transform");
    g.prototype.enableTransition = function() {
        this.isTransitioning || (this.css({
            transitionProperty: r,
            transitionDuration: this.layout.options.transitionDuration
        }),
        this.element.addEventListener(o, this, !1))
    }
    ,
    g.prototype.transition = g.prototype[k ? "_transition" : "_nonTransition"],
    g.prototype.onwebkitTransitionEnd = function(a) {
        this.ontransitionend(a)
    }
    ,
    g.prototype.onotransitionend = function(a) {
        this.ontransitionend(a)
    }
    ;
    var s = {
        "-webkit-transform": "transform",
        "-moz-transform": "transform",
        "-o-transform": "transform"
    };
    g.prototype.ontransitionend = function(a) {
        if (a.target === this.element) {
            var b = this._transn
              , c = s[a.propertyName] || a.propertyName;
            if (delete b.ingProperties[c],
            f(b.ingProperties) && this.disableTransition(),
            c in b.clean && (this.element.style[a.propertyName] = "",
            delete b.clean[c]),
            c in b.onEnd) {
                var d = b.onEnd[c];
                d.call(this),
                delete b.onEnd[c]
            }
            this.emitEvent("transitionEnd", [this])
        }
    }
    ,
    g.prototype.disableTransition = function() {
        this.removeTransitionStyles(),
        this.element.removeEventListener(o, this, !1),
        this.isTransitioning = !1
    }
    ,
    g.prototype._removeStyles = function(a) {
        var b = {};
        for (var c in a)
            b[c] = "";
        this.css(b)
    }
    ;
    var t = {
        transitionProperty: "",
        transitionDuration: ""
    };
    return g.prototype.removeTransitionStyles = function() {
        this.css(t)
    }
    ,
    g.prototype.removeElem = function() {
        this.element.parentNode.removeChild(this.element),
        this.css({
            display: ""
        }),
        this.emitEvent("remove", [this])
    }
    ,
    g.prototype.remove = function() {
        if (!k || !parseFloat(this.layout.options.transitionDuration))
            return void this.removeElem();
        var a = this;
        this.once("transitionEnd", function() {
            a.removeElem()
        }),
        this.hide()
    }
    ,
    g.prototype.reveal = function() {
        delete this.isHidden,
        this.css({
            display: ""
        });
        var a = this.layout.options
          , b = {}
          , c = this.getHideRevealTransitionEndProperty("visibleStyle");
        b[c] = this.onRevealTransitionEnd,
        this.transition({
            from: a.hiddenStyle,
            to: a.visibleStyle,
            isCleaning: !0,
            onTransitionEnd: b
        })
    }
    ,
    g.prototype.onRevealTransitionEnd = function() {
        this.isHidden || this.emitEvent("reveal")
    }
    ,
    g.prototype.getHideRevealTransitionEndProperty = function(a) {
        var b = this.layout.options[a];
        if (b.opacity)
            return "opacity";
        for (var c in b)
            return c
    }
    ,
    g.prototype.hide = function() {
        this.isHidden = !0,
        this.css({
            display: ""
        });
        var a = this.layout.options
          , b = {}
          , c = this.getHideRevealTransitionEndProperty("hiddenStyle");
        b[c] = this.onHideTransitionEnd,
        this.transition({
            from: a.visibleStyle,
            to: a.hiddenStyle,
            isCleaning: !0,
            onTransitionEnd: b
        })
    }
    ,
    g.prototype.onHideTransitionEnd = function() {
        this.isHidden && (this.css({
            display: "none"
        }),
        this.emitEvent("hide"))
    }
    ,
    g.prototype.destroy = function() {
        this.css({
            position: "",
            left: "",
            right: "",
            top: "",
            bottom: "",
            transition: "",
            transform: ""
        })
    }
    ,
    g
}),
function(a, b) {
    "function" == typeof define && define.amd ? define("outlayer/outlayer", ["eventie/eventie", "eventEmitter/EventEmitter", "get-size/get-size", "fizzy-ui-utils/utils", "./item"], function(c, d, e, f, g) {
        return b(a, c, d, e, f, g)
    }) : "object" == typeof exports ? module.exports = b(a, require("eventie"), require("wolfy87-eventemitter"), require("get-size"), require("fizzy-ui-utils"), require("./item")) : a.Outlayer = b(a, a.eventie, a.EventEmitter, a.getSize, a.fizzyUIUtils, a.Outlayer.Item)
}(window, function(a, b, c, d, e, f) {
    function g(a, b) {
        var c = e.getQueryElement(a);
        if (!c)
            return void (h && h.error("Bad element for " + this.constructor.namespace + ": " + (c || a)));
        this.element = c,
        i && (this.$element = i(this.element)),
        this.options = e.extend({}, this.constructor.defaults),
        this.option(b);
        var d = ++k;
        this.element.outlayerGUID = d,
        l[d] = this,
        this._create(),
        this.options.isInitLayout && this.layout()
    }
    var h = a.console
      , i = a.jQuery
      , j = function() {}
      , k = 0
      , l = {};
    return g.namespace = "outlayer",
    g.Item = f,
    g.defaults = {
        containerStyle: {
            position: "relative"
        },
        isInitLayout: !0,
        isOriginLeft: !0,
        isOriginTop: !0,
        isResizeBound: !0,
        isResizingContainer: !0,
        transitionDuration: "0.4s",
        hiddenStyle: {
            opacity: 0,
            transform: "scale(0.001)"
        },
        visibleStyle: {
            opacity: 1,
            transform: "scale(1)"
        }
    },
    e.extend(g.prototype, c.prototype),
    g.prototype.option = function(a) {
        e.extend(this.options, a)
    }
    ,
    g.prototype._create = function() {
        this.reloadItems(),
        this.stamps = [],
        this.stamp(this.options.stamp),
        e.extend(this.element.style, this.options.containerStyle),
        this.options.isResizeBound && this.bindResize()
    }
    ,
    g.prototype.reloadItems = function() {
        this.items = this._itemize(this.element.children)
    }
    ,
    g.prototype._itemize = function(a) {
        for (var b = this._filterFindItemElements(a), c = this.constructor.Item, d = [], e = 0, f = b.length; f > e; e++) {
            var g = b[e]
              , h = new c(g,this);
            d.push(h)
        }
        return d
    }
    ,
    g.prototype._filterFindItemElements = function(a) {
        return e.filterFindElements(a, this.options.itemSelector)
    }
    ,
    g.prototype.getItemElements = function() {
        for (var a = [], b = 0, c = this.items.length; c > b; b++)
            a.push(this.items[b].element);
        return a
    }
    ,
    g.prototype.layout = function() {
        this._resetLayout(),
        this._manageStamps();
        var a = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
        this.layoutItems(this.items, a),
        this._isLayoutInited = !0
    }
    ,
    g.prototype._init = g.prototype.layout,
    g.prototype._resetLayout = function() {
        this.getSize()
    }
    ,
    g.prototype.getSize = function() {
        this.size = d(this.element)
    }
    ,
    g.prototype._getMeasurement = function(a, b) {
        var c, f = this.options[a];
        f ? ("string" == typeof f ? c = this.element.querySelector(f) : e.isElement(f) && (c = f),
        this[a] = c ? d(c)[b] : f) : this[a] = 0
    }
    ,
    g.prototype.layoutItems = function(a, b) {
        a = this._getItemsForLayout(a),
        this._layoutItems(a, b),
        this._postLayout()
    }
    ,
    g.prototype._getItemsForLayout = function(a) {
        for (var b = [], c = 0, d = a.length; d > c; c++) {
            var e = a[c];
            e.isIgnored || b.push(e)
        }
        return b
    }
    ,
    g.prototype._layoutItems = function(a, b) {
        if (this._emitCompleteOnItems("layout", a),
        a && a.length) {
            for (var c = [], d = 0, e = a.length; e > d; d++) {
                var f = a[d]
                  , g = this._getItemLayoutPosition(f);
                g.item = f,
                g.isInstant = b || f.isLayoutInstant,
                c.push(g)
            }
            this._processLayoutQueue(c)
        }
    }
    ,
    g.prototype._getItemLayoutPosition = function() {
        return {
            x: 0,
            y: 0
        }
    }
    ,
    g.prototype._processLayoutQueue = function(a) {
        for (var b = 0, c = a.length; c > b; b++) {
            var d = a[b];
            this._positionItem(d.item, d.x, d.y, d.isInstant)
        }
    }
    ,
    g.prototype._positionItem = function(a, b, c, d) {
        d ? a.goTo(b, c) : a.moveTo(b, c)
    }
    ,
    g.prototype._postLayout = function() {
        this.resizeContainer()
    }
    ,
    g.prototype.resizeContainer = function() {
        if (this.options.isResizingContainer) {
            var a = this._getContainerSize();
            a && (this._setContainerMeasure(a.width, !0),
            this._setContainerMeasure(a.height, !1))
        }
    }
    ,
    g.prototype._getContainerSize = j,
    g.prototype._setContainerMeasure = function(a, b) {
        if (void 0 !== a) {
            var c = this.size;
            c.isBorderBox && (a += b ? c.paddingLeft + c.paddingRight + c.borderLeftWidth + c.borderRightWidth : c.paddingBottom + c.paddingTop + c.borderTopWidth + c.borderBottomWidth),
            a = Math.max(a, 0),
            this.element.style[b ? "width" : "height"] = a + "px"
        }
    }
    ,
    g.prototype._emitCompleteOnItems = function(a, b) {
        function c() {
            e.dispatchEvent(a + "Complete", null, [b])
        }
        function d() {
            g++,
            g === f && c()
        }
        var e = this
          , f = b.length;
        if (!b || !f)
            return void c();
        for (var g = 0, h = 0, i = b.length; i > h; h++) {
            var j = b[h];
            j.once(a, d)
        }
    }
    ,
    g.prototype.dispatchEvent = function(a, b, c) {
        var d = b ? [b].concat(c) : c;
        if (this.emitEvent(a, d),
        i)
            if (this.$element = this.$element || i(this.element),
            b) {
                var e = i.Event(b);
                e.type = a,
                this.$element.trigger(e, c)
            } else
                this.$element.trigger(a, c)
    }
    ,
    g.prototype.ignore = function(a) {
        var b = this.getItem(a);
        b && (b.isIgnored = !0)
    }
    ,
    g.prototype.unignore = function(a) {
        var b = this.getItem(a);
        b && delete b.isIgnored
    }
    ,
    g.prototype.stamp = function(a) {
        if (a = this._find(a)) {
            this.stamps = this.stamps.concat(a);
            for (var b = 0, c = a.length; c > b; b++) {
                var d = a[b];
                this.ignore(d)
            }
        }
    }
    ,
    g.prototype.unstamp = function(a) {
        if (a = this._find(a))
            for (var b = 0, c = a.length; c > b; b++) {
                var d = a[b];
                e.removeFrom(this.stamps, d),
                this.unignore(d)
            }
    }
    ,
    g.prototype._find = function(a) {
        return a ? ("string" == typeof a && (a = this.element.querySelectorAll(a)),
        a = e.makeArray(a)) : void 0
    }
    ,
    g.prototype._manageStamps = function() {
        if (this.stamps && this.stamps.length) {
            this._getBoundingRect();
            for (var a = 0, b = this.stamps.length; b > a; a++) {
                var c = this.stamps[a];
                this._manageStamp(c)
            }
        }
    }
    ,
    g.prototype._getBoundingRect = function() {
        var a = this.element.getBoundingClientRect()
          , b = this.size;
        this._boundingRect = {
            left: a.left + b.paddingLeft + b.borderLeftWidth,
            top: a.top + b.paddingTop + b.borderTopWidth,
            right: a.right - (b.paddingRight + b.borderRightWidth),
            bottom: a.bottom - (b.paddingBottom + b.borderBottomWidth)
        }
    }
    ,
    g.prototype._manageStamp = j,
    g.prototype._getElementOffset = function(a) {
        var b = a.getBoundingClientRect()
          , c = this._boundingRect
          , e = d(a)
          , f = {
            left: b.left - c.left - e.marginLeft,
            top: b.top - c.top - e.marginTop,
            right: c.right - b.right - e.marginRight,
            bottom: c.bottom - b.bottom - e.marginBottom
        };
        return f
    }
    ,
    g.prototype.handleEvent = function(a) {
        var b = "on" + a.type;
        this[b] && this[b](a)
    }
    ,
    g.prototype.bindResize = function() {
        this.isResizeBound || (b.bind(a, "resize", this),
        this.isResizeBound = !0)
    }
    ,
    g.prototype.unbindResize = function() {
        this.isResizeBound && b.unbind(a, "resize", this),
        this.isResizeBound = !1
    }
    ,
    g.prototype.onresize = function() {
        function a() {
            b.resize(),
            delete b.resizeTimeout
        }
        this.resizeTimeout && clearTimeout(this.resizeTimeout);
        var b = this;
        this.resizeTimeout = setTimeout(a, 100)
    }
    ,
    g.prototype.resize = function() {
        this.isResizeBound && this.needsResizeLayout() && this.layout()
    }
    ,
    g.prototype.needsResizeLayout = function() {
        var a = d(this.element)
          , b = this.size && a;
        return b && a.innerWidth !== this.size.innerWidth
    }
    ,
    g.prototype.addItems = function(a) {
        var b = this._itemize(a);
        return b.length && (this.items = this.items.concat(b)),
        b
    }
    ,
    g.prototype.appended = function(a) {
        var b = this.addItems(a);
        b.length && (this.layoutItems(b, !0),
        this.reveal(b))
    }
    ,
    g.prototype.prepended = function(a) {
        var b = this._itemize(a);
        if (b.length) {
            var c = this.items.slice(0);
            this.items = b.concat(c),
            this._resetLayout(),
            this._manageStamps(),
            this.layoutItems(b, !0),
            this.reveal(b),
            this.layoutItems(c)
        }
    }
    ,
    g.prototype.reveal = function(a) {
        this._emitCompleteOnItems("reveal", a);
        for (var b = a && a.length, c = 0; b && b > c; c++) {
            var d = a[c];
            d.reveal()
        }
    }
    ,
    g.prototype.hide = function(a) {
        this._emitCompleteOnItems("hide", a);
        for (var b = a && a.length, c = 0; b && b > c; c++) {
            var d = a[c];
            d.hide()
        }
    }
    ,
    g.prototype.revealItemElements = function(a) {
        var b = this.getItems(a);
        this.reveal(b)
    }
    ,
    g.prototype.hideItemElements = function(a) {
        var b = this.getItems(a);
        this.hide(b)
    }
    ,
    g.prototype.getItem = function(a) {
        for (var b = 0, c = this.items.length; c > b; b++) {
            var d = this.items[b];
            if (d.element === a)
                return d
        }
    }
    ,
    g.prototype.getItems = function(a) {
        a = e.makeArray(a);
        for (var b = [], c = 0, d = a.length; d > c; c++) {
            var f = a[c]
              , g = this.getItem(f);
            g && b.push(g)
        }
        return b
    }
    ,
    g.prototype.remove = function(a) {
        var b = this.getItems(a);
        if (this._emitCompleteOnItems("remove", b),
        b && b.length)
            for (var c = 0, d = b.length; d > c; c++) {
                var f = b[c];
                f.remove(),
                e.removeFrom(this.items, f)
            }
    }
    ,
    g.prototype.destroy = function() {
        var a = this.element.style;
        a.height = "",
        a.position = "",
        a.width = "";
        for (var b = 0, c = this.items.length; c > b; b++) {
            var d = this.items[b];
            d.destroy()
        }
        this.unbindResize();
        var e = this.element.outlayerGUID;
        delete l[e],
        delete this.element.outlayerGUID,
        i && i.removeData(this.element, this.constructor.namespace)
    }
    ,
    g.data = function(a) {
        a = e.getQueryElement(a);
        var b = a && a.outlayerGUID;
        return b && l[b]
    }
    ,
    g.create = function(a, b) {
        function c() {
            g.apply(this, arguments)
        }
        return Object.create ? c.prototype = Object.create(g.prototype) : e.extend(c.prototype, g.prototype),
        c.prototype.constructor = c,
        c.defaults = e.extend({}, g.defaults),
        e.extend(c.defaults, b),
        c.prototype.settings = {},
        c.namespace = a,
        c.data = g.data,
        c.Item = function() {
            f.apply(this, arguments)
        }
        ,
        c.Item.prototype = new f,
        e.htmlInit(c, a),
        i && i.bridget && i.bridget(a, c),
        c
    }
    ,
    g.Item = f,
    g
}),
function(a, b) {
    "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size", "fizzy-ui-utils/utils"], b) : "object" == typeof exports ? module.exports = b(require("outlayer"), require("get-size"), require("fizzy-ui-utils")) : a.Masonry = b(a.Outlayer, a.getSize, a.fizzyUIUtils)
}(window, function(a, b, c) {
    var d = a.create("masonry");
    return d.prototype._resetLayout = function() {
        this.getSize(),
        this._getMeasurement("columnWidth", "outerWidth"),
        this._getMeasurement("gutter", "outerWidth"),
        this.measureColumns();
        var a = this.cols;
        for (this.colYs = []; a--; )
            this.colYs.push(0);
        this.maxY = 0
    }
    ,
    d.prototype.measureColumns = function() {
        if (this.getContainerWidth(),
        !this.columnWidth) {
            var a = this.items[0]
              , c = a && a.element;
            this.columnWidth = c && b(c).outerWidth || this.containerWidth
        }
        var d = this.columnWidth += this.gutter
          , e = this.containerWidth + this.gutter
          , f = e / d
          , g = d - e % d
          , h = g && 1 > g ? "round" : "floor";
        f = Math[h](f),
        this.cols = Math.max(f, 1)
    }
    ,
    d.prototype.getContainerWidth = function() {
        var a = this.options.isFitWidth ? this.element.parentNode : this.element
          , c = b(a);
        this.containerWidth = c && c.innerWidth
    }
    ,
    d.prototype._getItemLayoutPosition = function(a) {
        a.getSize();
        var b = a.size.outerWidth % this.columnWidth
          , d = b && 1 > b ? "round" : "ceil"
          , e = Math[d](a.size.outerWidth / this.columnWidth);
        e = Math.min(e, this.cols);
        for (var f = this._getColGroup(e), g = Math.min.apply(Math, f), h = c.indexOf(f, g), i = {
            x: this.columnWidth * h,
            y: g
        }, j = g + a.size.outerHeight, k = this.cols + 1 - f.length, l = 0; k > l; l++)
            this.colYs[h + l] = j;
        return i
    }
    ,
    d.prototype._getColGroup = function(a) {
        if (2 > a)
            return this.colYs;
        for (var b = [], c = this.cols + 1 - a, d = 0; c > d; d++) {
            var e = this.colYs.slice(d, d + a);
            b[d] = Math.max.apply(Math, e)
        }
        return b
    }
    ,
    d.prototype._manageStamp = function(a) {
        var c = b(a)
          , d = this._getElementOffset(a)
          , e = this.options.isOriginLeft ? d.left : d.right
          , f = e + c.outerWidth
          , g = Math.floor(e / this.columnWidth);
        g = Math.max(0, g);
        var h = Math.floor(f / this.columnWidth);
        h -= f % this.columnWidth ? 0 : 1,
        h = Math.min(this.cols - 1, h);
        for (var i = (this.options.isOriginTop ? d.top : d.bottom) + c.outerHeight, j = g; h >= j; j++)
            this.colYs[j] = Math.max(i, this.colYs[j])
    }
    ,
    d.prototype._getContainerSize = function() {
        this.maxY = Math.max.apply(Math, this.colYs);
        var a = {
            height: this.maxY
        };
        return this.options.isFitWidth && (a.width = this._getContainerFitWidth()),
        a
    }
    ,
    d.prototype._getContainerFitWidth = function() {
        for (var a = 0, b = this.cols; --b && 0 === this.colYs[b]; )
            a++;
        return (this.cols - a) * this.columnWidth - this.gutter
    }
    ,
    d.prototype.needsResizeLayout = function() {
        var a = this.containerWidth;
        return this.getContainerWidth(),
        a !== this.containerWidth
    }
    ,
    d
});
/*!
* Masonry v2 shim
* to maintain backwards compatibility
* as of Masonry v3.1.2
*
* Cascading grid layout library
* http://masonry.desandro.com
* MIT License
* by David DeSandro
*/
!function(a) {
    "use strict";
    var b = a.Masonry;
    b.prototype._remapV2Options = function() {
        this._remapOption("gutterWidth", "gutter"),
        this._remapOption("isResizable", "isResizeBound"),
        this._remapOption("isRTL", "isOriginLeft", function(a) {
            return !a
        });
        var a = this.options.isAnimated;
        if (void 0 !== a && (this.options.transitionDuration = a ? this.options.transitionDuration : 0),
        void 0 === a || a) {
            var b = this.options.animationOptions
              , c = b && b.duration;
            c && (this.options.transitionDuration = "string" == typeof c ? c : c + "ms")
        }
    }
    ,
    b.prototype._remapOption = function(a, b, c) {
        var d = this.options[a];
        void 0 !== d && (this.options[b] = c ? c(d) : d)
    }
    ;
    var c = b.prototype._create;
    b.prototype._create = function() {
        var a = this;
        this._remapV2Options(),
        c.apply(this, arguments),
        setTimeout(function() {
            jQuery(a.element).addClass("masonry")
        }, 0)
    }
    ;
    var d = b.prototype.layout;
    b.prototype.layout = function() {
        this._remapV2Options(),
        d.apply(this, arguments)
    }
    ;
    var e = b.prototype.option;
    b.prototype.option = function() {
        e.apply(this, arguments),
        this._remapV2Options()
    }
    ;
    var f = b.prototype._itemize;
    b.prototype._itemize = function(a) {
        var b = f.apply(this, arguments);
        return jQuery(a).addClass("masonry-brick"),
        b
    }
    ;
    var g = b.prototype.measureColumns;
    b.prototype.measureColumns = function() {
        var a = this.options.columnWidth;
        a && "function" == typeof a && (this.getContainerWidth(),
        this.columnWidth = a(this.containerWidth)),
        g.apply(this, arguments)
    }
    ,
    b.prototype.reload = function() {
        this.reloadItems.apply(this, arguments),
        this.layout.apply(this)
    }
    ;
    var h = b.prototype.destroy;
    b.prototype.destroy = function() {
        var a = this.getItemElements();
        jQuery(this.element).removeClass("masonry"),
        jQuery(a).removeClass("masonry-brick"),
        h.apply(this, arguments)
    }
}(window);
jQuery(function($) {
    'use strict';
    (function() {
        $('#status').fadeOut();
        $('#preloader').delay(200).fadeOut('slow');
    }());
    (function() {
        $('[data-toggle="tooltip"]').tooltip()
    }());
    (function() {
        function getIEVersion() {
            var match = navigator.userAgent.match(/(?:MSIE |Trident\/.*; rv:)(\d+)/);
            return match ? parseInt(match[1], 10) : false;
        }
        if (getIEVersion()) {
            $('html').addClass('ie' + getIEVersion());
        }
    }());
    (function() {
        $('.navbar-nav a[href^="#"], .tt-scroll').on('click', function(e) {
            e.preventDefault();
            var target = this.hash;
            var $target = $(target);
            var headerHeight = $('.navbar-header, .header-wrapper.sticky .navbar-header').outerHeight();
            if (target) {
                $('html, body').stop().animate({
                    'scrollTop': $target.offset().top - headerHeight + "px"
                }, 1200, function() {
                    window.location.hash = target;
                });
            }
        });
    }());
    (function() {
        $('.dropdown-menu-trigger').each(function() {
            $(this).on('click', function(e) {
                $(this).toggleClass('menu-collapsed');
            });
        });
    }());
    $(window).on('load resize', function() {
        $(".dropdown-wrapper > ul > li").each(function() {
            var $this = $(this)
              , $win = $(window);
            if ($this.offset().left + 195 > $win.width() + $win.scrollLeft() - $this.width()) {
                $this.addClass("dropdown-inverse");
            } else {
                $this.removeClass("dropdown-inverse");
            }
        });
    });
    $('.mobile-menu .navbar-nav > li > a[href^="#"]').on('click', function(e) {
        $('.mobile-toggle').removeClass('in');
    });
    (function() {
        if (sixtyninestudioJSObject.sixtyninestudio_sticky_menu == true) {
            if ($('.header-wrapper').length > 0) {
                $('.header-wrapper').sticky({
                    topSpacing: 0
                });
            }
        }
    }());
    (function() {
        var searchBox = $(".search-box-wrap .search-form");
        var searchIcon = $(".search-icon");
        searchBox.hide();
        searchIcon.on('click', function(event) {
            searchIcon.toggleClass("active");
            searchBox.slideToggle();
            $(this).closest('.search-box-wrap').find('input').focus();
        });
    }());
    $(".tt-fullHeight").height($(window).height());
    $(window).resize(function() {
        $(".tt-fullHeight").height($(window).height());
    });
    if ($('.rotate').length > 0) {
        $(".rotate").textrotator({
            animation: "dissolve",
            separator: "|",
            speed: 3000
        });
    }
    $('.counter-section').on('inview', function(event, visible, visiblePartX, visiblePartY) {
        if (visible) {
            $(this).find('.timer').each(function() {
                var $this = $(this);
                $({
                    Counter: 0
                }).animate({
                    Counter: $this.text()
                }, {
                    duration: 2000,
                    easing: 'swing',
                    step: function() {
                        $this.text(Math.ceil(this.Counter));
                    }
                });
            });
            $(this).off('inview');
        }
    });
    $(window).load(function() {
        $(".tt-lightbox, .image-link, .tt-flickr-photo a, .portfolio-extra-images > a").magnificPopup({
            type: 'image',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            fixedContentPos: false,
            gallery: {
                enabled: true
            }
        });
        if ($('.portfolio-extra-images').length > 0) {
            $(".portfolio-extra-images > a").magnificPopup({
                type: 'image',
                mainClass: 'mfp-fade',
                removalDelay: 160,
                fixedContentPos: false,
                gallery: {
                    enabled: true
                }
            });
        }
        if ($('.woocommerce-product-gallery__image').length > 0) {
            $(".woocommerce-product-gallery__image > a").magnificPopup({
                type: 'image',
                closeOnContentClick: true,
                closeBtnInside: false,
                fixedContentPos: true,
                mainClass: 'mfp-no-margins mfp-with-zoom',
                gallery: {
                    enabled: true
                },
                image: {
                    verticalFit: true
                },
                zoom: {
                    enabled: true,
                    duration: 300
                }
            });
        }
    });
    if ($('.portfolio-extra-images').length > 0) {
        $('.portfolio-extra-images').magnificPopup({
            type: 'image',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            fixedContentPos: false,
            gallery: {
                enabled: false
            }
        });
    }
    if ($('.tt-popup').length > 0) {
        $('.tt-popup').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: true,
            fixedContentPos: false
        });
    }
    $(window).load(function() {
        $('.masonry-wrap').masonry({
            "columnWidth": ".masonry-column"
        });
    });
    (function() {
        if ($('.recent-project-carousel').length > 0) {
            var projectCarousal = $(".recent-project-carousel");
            var projectCarousalItems = parseInt(projectCarousal.attr('data-largescreen'));
            var projectCarousalItemsDesktop = parseInt(projectCarousal.attr('data-desktop'));
            var projectCarousalItemsDesktopSmall = parseInt(projectCarousal.attr('data-desktopsmall'));
            var projectCarousalItemsTablet = parseInt(projectCarousal.attr('data-tablet'));
            projectCarousal.owlCarousel({
                items: projectCarousalItems,
                itemsDesktop: [1024, projectCarousalItemsDesktop],
                itemsDesktopSmall: [900, projectCarousalItemsDesktopSmall],
                itemsTablet: [600, projectCarousalItemsTablet],
                itemsMobile: [479, 1],
                pagination: false
            });
            $(".project-navigation .btn-next").on('click', function() {
                projectCarousal.trigger('owl.next');
            });
            $(".project-navigation .btn-prev").on('click', function() {
                projectCarousal.trigger('owl.prev');
            });
        }
    }());
    (function() {
        if ($('.partner-carousel').length > 0) {
            var owl = $(".partner-carousel");
            var partnerCarousalItems = parseInt(owl.attr('data-largescreen'));
            var partnerCarousalItemsDesktop = parseInt(owl.attr('data-desktop'));
            var partnerCarousalItemsDesktopSmall = parseInt(owl.attr('data-desktopsmall'));
            var partnerCarousalItemsTablet = parseInt(owl.attr('data-tablet'));
            owl.owlCarousel({
                autoPlay: true,
                items: partnerCarousalItems,
                itemsDesktop: [1024, partnerCarousalItemsDesktop],
                itemsDesktopSmall: [900, partnerCarousalItemsDesktopSmall],
                itemsTablet: [600, partnerCarousalItemsTablet],
                itemsMobile: [479, 1],
                pagination: false
            });
        }
    }());
    $(window).on('load', function() {
        if ($('.tt-gallery-wrapper').length > 0) {
            $('.tt-gallery-wrapper').each(function(i, e) {
                var ttGalleryNav = $(this).find('.tt-gallery-nav');
                var ttGalleryThumb = $(this).find('.tt-gallery-thumb');
                var ttGallery = $(this).find('.tt-gallery');
                ttGalleryThumb.flexslider({
                    animation: "slide",
                    controlNav: false,
                    animationLoop: true,
                    slideshow: false,
                    itemWidth: 185,
                    asNavFor: ttGallery
                });
                ttGallery.flexslider({
                    animation: "slide",
                    directionNav: false,
                    controlNav: false,
                    animationLoop: false,
                    slideshow: false,
                    sync: ttGalleryThumb
                });
                ttGalleryNav.find('.prev').on('click', function(e) {
                    ttGallery.flexslider('prev')
                    return false;
                });
                ttGalleryNav.find('.next').on('click', function(e) {
                    ttGallery.flexslider('next')
                    return false;
                });
            });
        }
    }());
    if ($('.client-slider-v3').length > 0) {
        $('.client-slider-v3').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: true
        })
    }
    if ($('.client-slider-v4').length > 0) {
        $('.client-slider-v4').flexslider({
            animation: "slide",
            controlNav: "thumbnails",
            directionNav: true
        })
    }
    (function() {
        if ($('.tt-home-slider').length > 0) {
            $('.tt-home-slider').superslides({
                play: 7000,
                pagination: false,
                hashchange: false,
                animation: 'fade'
            });
        }
    }());
    (function() {
        if ($('#ttmap').length > 0) {
            var $latitude = $('#ttmap').attr('data-map-latitude')
              , $longitude = $('#ttmap').attr('data-map-longitude')
              , $map_zoom = 12;
            var $marker_url = $('#ttmap').attr('data-map-marker');
            var style = [{
                stylers: [{
                    "hue": "#000"
                }, {
                    "saturation": -100
                }, {
                    "gamma": 2.15
                }, {
                    "lightness": 12
                }]
            }];
            var map_options = {
                center: new google.maps.LatLng($latitude,$longitude),
                zoom: $map_zoom,
                panControl: true,
                zoomControl: true,
                mapTypeControl: false,
                streetViewControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                scrollwheel: false,
                styles: style
            }
            var map = new google.maps.Map(document.getElementById('ttmap'),map_options);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng($latitude,$longitude),
                map: map,
                visible: true,
                icon: $marker_url
            });
        }
    }());
    $(window).on('load', function() {
        if ($('.portfolio-container').length > 0) {
            $('.portfolio-container').each(function(i, e) {
                var ttGrid = $(this).find('.tt-grid');
                var self = this;
                ttGrid.shuffle({
                    itemSelector: '.tt-item'
                });
                $(this).find('.tt-filter button').on('click', function(e) {
                    e.preventDefault();
                    $(self).find('.tt-filter button').removeClass('active');
                    $(this).addClass('active');
                    var ttGroupName = $(this).attr('data-group');
                    ttGrid.shuffle('shuffle', ttGroupName);
                });
            });
        }
    });
    (function() {
        $(window).on('load', function() {
            if (/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {} else {
                $(window).stellar({
                    horizontalScrolling: false,
                    responsive: true
                });
            }
        });
    }());
    (function() {
        var ttFlickr = $('.tt-flickr-photo');
        ttFlickr.jflickrfeed({
            limit: ttFlickr.attr('data-photo-limit'),
            qstrings: {
                id: ttFlickr.attr('data-flickr-id')
            },
            itemTemplate: '<li>' + '<a href="{{image}}" title="{{title}}">' + '<img src="{{image_s}}" alt="{{title}}" />' + '</a>' + '</li>'
        });
    }());
    (function() {
        if ($('#particles-js').length > 0) {
            particlesJS("particles-js", {
                "particles": {
                    "number": {
                        "value": 100,
                        "density": {
                            "enable": true,
                            "value_area": 1000
                        }
                    },
                    "color": {
                        "value": ["#aa73ff", "#f8c210", "#83d238", "#33b1f8"]
                    },
                    "shape": {
                        "type": "circle",
                        "stroke": {
                            "width": 0,
                            "color": "#fff"
                        }
                    },
                    "opacity": {
                        "value": 0.6,
                        "random": false,
                        "anim": {
                            "enable": false,
                            "speed": 1,
                            "opacity_min": 0.1,
                            "sync": false
                        }
                    },
                    "size": {
                        "value": 2,
                        "random": true,
                        "anim": {
                            "enable": false,
                            "speed": 40,
                            "size_min": 0.1,
                            "sync": false
                        }
                    },
                    "line_linked": {
                        "enable": true,
                        "distance": 120,
                        "color": "#ffffff",
                        "opacity": 0.4,
                        "width": 1
                    },
                },
                "interactivity": {
                    "detect_on": "canvas",
                    "events": {
                        "onhover": {
                            "enable": true,
                            "mode": "grab"
                        },
                        "onclick": {
                            "enable": true
                        },
                        "resize": true
                    },
                    "modes": {
                        "grab": {
                            "distance": 140,
                            "line_linked": {
                                "opacity": 1
                            }
                        },
                        "bubble": {
                            "distance": 400,
                            "size": 40,
                            "duration": 2,
                            "opacity": 8,
                            "speed": 3
                        },
                        "repulse": {
                            "distance": 200,
                            "duration": 0.4
                        },
                        "push": {
                            "particles_nb": 4
                        },
                        "remove": {
                            "particles_nb": 2
                        }
                    }
                },
                "retina_detect": true
            });
        }
    }());
    (function() {
        $('.sidebar-sticky').ttStickySidebar({
            additionalMarginTop: 30
        });
    }());
    (function() {
        $('body').append('<div id="toTop"><i class="fa fa-angle-up"></i></div>');
        $(window).scroll(function() {
            if ($(this).scrollTop() !== 0) {
                $('#toTop').fadeIn();
            } else {
                $('#toTop').fadeOut();
            }
        });
        $('#toTop').on('click', function() {
            $("html, body").animate({
                scrollTop: 0
            }, 600);
            return false;
        });
    }());
});
(function(window, document) {
    'use strict';
    var supportedBrowser = false
      , loaded = false;
    if (document.querySelector) {
        if (window.addEventListener) {
            supportedBrowser = true;
        }
    }
    window.wp = window.wp || {};
    if (!!window.wp.receiveEmbedMessage) {
        return;
    }
    window.wp.receiveEmbedMessage = function(e) {
        var data = e.data;
        if (!data) {
            return;
        }
        if (!(data.secret || data.message || data.value)) {
            return;
        }
        if (/[^a-zA-Z0-9]/.test(data.secret)) {
            return;
        }
        var iframes = document.querySelectorAll('iframe[data-secret="' + data.secret + '"]'), blockquotes = document.querySelectorAll('blockquote[data-secret="' + data.secret + '"]'), i, source, height, sourceURL, targetURL;
        for (i = 0; i < blockquotes.length; i++) {
            blockquotes[i].style.display = 'none';
        }
        for (i = 0; i < iframes.length; i++) {
            source = iframes[i];
            if (e.source !== source.contentWindow) {
                continue;
            }
            source.removeAttribute('style');
            if ('height' === data.message) {
                height = parseInt(data.value, 10);
                if (height > 1000) {
                    height = 1000;
                } else if (~~height < 200) {
                    height = 200;
                }
                source.height = height;
            }
            if ('link' === data.message) {
                sourceURL = document.createElement('a');
                targetURL = document.createElement('a');
                sourceURL.href = source.getAttribute('src');
                targetURL.href = data.value;
                if (targetURL.host === sourceURL.host) {
                    if (document.activeElement === source) {
                        window.top.location.href = data.value;
                    }
                }
            }
        }
    }
    ;
    function onLoad() {
        if (loaded) {
            return;
        }
        loaded = true;
        var isIE10 = -1 !== navigator.appVersion.indexOf('MSIE 10'), isIE11 = !!navigator.userAgent.match(/Trident.*rv:11\./), iframes = document.querySelectorAll('iframe.wp-embedded-content'), iframeClone, i, source, secret;
        for (i = 0; i < iframes.length; i++) {
            source = iframes[i];
            if (!source.getAttribute('data-secret')) {
                secret = Math.random().toString(36).substr(2, 10);
                source.src += '#?secret=' + secret;
                source.setAttribute('data-secret', secret);
            }
            if ((isIE10 || isIE11)) {
                iframeClone = source.cloneNode(true);
                iframeClone.removeAttribute('security');
                source.parentNode.replaceChild(iframeClone, source);
            }
        }
    }
    if (supportedBrowser) {
        window.addEventListener('message', window.wp.receiveEmbedMessage, false);
        document.addEventListener('DOMContentLoaded', onLoad, false);
        window.addEventListener('load', onLoad, false);
    }
}
)(window, document);
function vc_js() {
    vc_toggleBehaviour(),
    vc_tabsBehaviour(),
    vc_accordionBehaviour(),
    vc_teaserGrid(),
    vc_carouselBehaviour(),
    vc_slidersBehaviour(),
    vc_prettyPhoto(),
    vc_googleplus(),
    vc_pinterest(),
    vc_progress_bar(),
    vc_plugin_flexslider(),
    vc_google_fonts(),
    vc_gridBehaviour(),
    vc_rowBehaviour(),
    vc_prepareHoverBox(),
    vc_googleMapsPointer(),
    vc_ttaActivation(),
    jQuery(document).trigger("vc_js"),
    window.setTimeout(vc_waypoints, 500)
}
document.documentElement.className += " js_active ",
document.documentElement.className += "ontouchstart"in document.documentElement ? " vc_mobile " : " vc_desktop ",
function() {
    for (var prefix = ["-webkit-", "-moz-", "-ms-", "-o-", ""], i = 0; i < prefix.length; i++)
        prefix[i] + "transform"in document.documentElement.style && (document.documentElement.className += " vc_transform ")
}(),
"function" != typeof window.vc_plugin_flexslider && (window.vc_plugin_flexslider = function($parent) {
    ($parent ? $parent.find(".wpb_flexslider") : jQuery(".wpb_flexslider")).each(function() {
        var this_element = jQuery(this)
          , sliderTimeout = 1e3 * parseInt(this_element.attr("data-interval"))
          , sliderFx = this_element.attr("data-flex_fx")
          , slideshow = !0;
        0 === sliderTimeout && (slideshow = !1),
        this_element.is(":visible") && this_element.flexslider({
            animation: sliderFx,
            slideshow: slideshow,
            slideshowSpeed: sliderTimeout,
            sliderSpeed: 800,
            smoothHeight: !0
        })
    })
}
),
"function" != typeof window.vc_googleplus && (window.vc_googleplus = function() {
    0 < jQuery(".wpb_googleplus").length && function() {
        var po = document.createElement("script");
        po.type = "text/javascript",
        po.async = !0,
        po.src = "https://apis.google.com/js/plusone.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(po, s)
    }()
}
),
"function" != typeof window.vc_pinterest && (window.vc_pinterest = function() {
    0 < jQuery(".wpb_pinterest").length && function() {
        var po = document.createElement("script");
        po.type = "text/javascript",
        po.async = !0,
        po.src = "https://assets.pinterest.com/js/pinit.js";
        var s = document.getElementsByTagName("script")[0];
        s.parentNode.insertBefore(po, s)
    }()
}
),
"function" != typeof window.vc_progress_bar && (window.vc_progress_bar = function() {
    void 0 !== jQuery.fn.waypoint && jQuery(".vc_progress_bar").waypoint(function() {
        jQuery(this).find(".vc_single_bar").each(function(index) {
            var bar = jQuery(this).find(".vc_bar")
              , val = bar.data("percentage-value");
            setTimeout(function() {
                bar.css({
                    width: val + "%"
                })
            }, 200 * index)
        })
    }, {
        offset: "85%"
    })
}
),
"function" != typeof window.vc_waypoints && (window.vc_waypoints = function() {
    void 0 !== jQuery.fn.waypoint && jQuery(".wpb_animate_when_almost_visible:not(.wpb_start_animation)").waypoint(function() {
        jQuery(this).addClass("wpb_start_animation animated")
    }, {
        offset: "85%"
    })
}
),
"function" != typeof window.vc_toggleBehaviour && (window.vc_toggleBehaviour = function($el) {
    function event(e) {
        e && e.preventDefault && e.preventDefault();
        var element = jQuery(this).closest(".vc_toggle")
          , content = element.find(".vc_toggle_content");
        element.hasClass("vc_toggle_active") ? content.slideUp({
            duration: 300,
            complete: function() {
                element.removeClass("vc_toggle_active")
            }
        }) : content.slideDown({
            duration: 300,
            complete: function() {
                element.addClass("vc_toggle_active")
            }
        })
    }
    $el ? $el.hasClass("vc_toggle_title") ? $el.unbind("click").click(event) : $el.find(".vc_toggle_title").unbind("click").click(event) : jQuery(".vc_toggle_title").unbind("click").on("click", event)
}
),
"function" != typeof window.vc_tabsBehaviour && (window.vc_tabsBehaviour = function($tab) {
    if (jQuery.ui) {
        var $call = $tab || jQuery(".wpb_tabs, .wpb_tour")
          , ver = jQuery.ui && jQuery.ui.version ? jQuery.ui.version.split(".") : "1.10"
          , old_version = 1 === parseInt(ver[0]) && parseInt(ver[1]) < 9;
        $call.each(function(index) {
            var $tabs, interval = jQuery(this).attr("data-interval"), tabs_array = [];
            if ($tabs = jQuery(this).find(".wpb_tour_tabs_wrapper").tabs({
                show: function(event, ui) {
                    wpb_prepare_tab_content(event, ui)
                },
                beforeActivate: function(event, ui) {
                    1 !== ui.newPanel.index() && ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")
                },
                activate: function(event, ui) {
                    wpb_prepare_tab_content(event, ui)
                }
            }),
            interval && 0 < interval)
                try {
                    $tabs.tabs("rotate", 1e3 * interval)
                } catch (e) {
                    window.console && window.console.warn && console.warn(e)
                }
            jQuery(this).find(".wpb_tab").each(function() {
                tabs_array.push(this.id)
            }),
            jQuery(this).find(".wpb_tabs_nav li").click(function(e) {
                return e.preventDefault(),
                old_version ? $tabs.tabs("select", jQuery("a", this).attr("href")) : $tabs.tabs("option", "active", jQuery(this).index()),
                !1
            }),
            jQuery(this).find(".wpb_prev_slide a, .wpb_next_slide a").click(function(e) {
                if (e.preventDefault(),
                old_version) {
                    var index = $tabs.tabs("option", "selected");
                    jQuery(this).parent().hasClass("wpb_next_slide") ? index++ : index--,
                    index < 0 ? index = $tabs.tabs("length") - 1 : index >= $tabs.tabs("length") && (index = 0),
                    $tabs.tabs("select", index)
                } else {
                    index = $tabs.tabs("option", "active");
                    var length = $tabs.find(".wpb_tab").length;
                    index = jQuery(this).parent().hasClass("wpb_next_slide") ? length <= index + 1 ? 0 : index + 1 : index - 1 < 0 ? length - 1 : index - 1,
                    $tabs.tabs("option", "active", index)
                }
            })
        })
    }
}
),
"function" != typeof window.vc_accordionBehaviour && (window.vc_accordionBehaviour = function() {
    jQuery(".wpb_accordion").each(function(index) {
        var $tabs, $this = jQuery(this), active_tab = ($this.attr("data-interval"),
        !isNaN(jQuery(this).data("active-tab")) && 0 < parseInt($this.data("active-tab")) && parseInt($this.data("active-tab")) - 1), collapsible = !1 === active_tab || "yes" === $this.data("collapsible");
        $tabs = $this.find(".wpb_accordion_wrapper").accordion({
            header: "> div > h3",
            autoHeight: !1,
            heightStyle: "content",
            active: active_tab,
            collapsible: collapsible,
            navigation: !0,
            activate: vc_accordionActivate,
            change: function(event, ui) {
                void 0 !== jQuery.fn.isotope && ui.newContent.find(".isotope").isotope("layout"),
                vc_carouselBehaviour(ui.newPanel)
            }
        }),
        !0 === $this.data("vcDisableKeydown") && ($tabs.data("uiAccordion")._keydown = function() {}
        )
    })
}
),
"function" != typeof window.vc_teaserGrid && (window.vc_teaserGrid = function() {
    var layout_modes = {
        fitrows: "fitRows",
        masonry: "masonry"
    };
    jQuery(".wpb_grid .teaser_grid_container:not(.wpb_carousel), .wpb_filtered_grid .teaser_grid_container:not(.wpb_carousel)").each(function() {
        var $container = jQuery(this)
          , $thumbs = $container.find(".wpb_thumbnails")
          , layout_mode = $thumbs.attr("data-layout-mode");
        $thumbs.isotope({
            itemSelector: ".isotope-item",
            layoutMode: void 0 === layout_modes[layout_mode] ? "fitRows" : layout_modes[layout_mode]
        }),
        $container.find(".categories_filter a").data("isotope", $thumbs).click(function(e) {
            e.preventDefault();
            var $thumbs = jQuery(this).data("isotope");
            jQuery(this).parent().parent().find(".active").removeClass("active"),
            jQuery(this).parent().addClass("active"),
            $thumbs.isotope({
                filter: jQuery(this).attr("data-filter")
            })
        }),
        jQuery(window).bind("load resize", function() {
            $thumbs.isotope("layout")
        })
    })
}
),
"function" != typeof window.vc_carouselBehaviour && (window.vc_carouselBehaviour = function($parent) {
    ($parent ? $parent.find(".wpb_carousel") : jQuery(".wpb_carousel")).each(function() {
        var $this = jQuery(this);
        if (!0 !== $this.data("carousel_enabled") && $this.is(":visible")) {
            $this.data("carousel_enabled", !0);
            getColumnsCount(jQuery(this));
            jQuery(this).hasClass("columns_count_1") && 900;
            var carousele_li = jQuery(this).find(".wpb_thumbnails-fluid li");
            carousele_li.css({
                "margin-right": carousele_li.css("margin-left"),
                "margin-left": 0
            });
            var fluid_ul = jQuery(this).find("ul.wpb_thumbnails-fluid");
            fluid_ul.width(fluid_ul.width() + 300),
            jQuery(window).resize(function() {
                screen_size != (screen_size = getSizeName()) && window.setTimeout("location.reload()", 20)
            })
        }
    })
}
),
"function" != typeof window.vc_slidersBehaviour && (window.vc_slidersBehaviour = function() {
    jQuery(".wpb_gallery_slides").each(function(index) {
        var $imagesGrid, this_element = jQuery(this);
        if (this_element.hasClass("wpb_slider_nivo")) {
            var sliderTimeout = 1e3 * this_element.attr("data-interval");
            0 === sliderTimeout && (sliderTimeout = 9999999999),
            this_element.find(".nivoSlider").nivoSlider({
                effect: "boxRainGrow,boxRain,boxRainReverse,boxRainGrowReverse",
                slices: 15,
                boxCols: 8,
                boxRows: 4,
                animSpeed: 800,
                pauseTime: sliderTimeout,
                startSlide: 0,
                directionNav: !0,
                directionNavHide: !0,
                controlNav: !0,
                keyboardNav: !1,
                pauseOnHover: !0,
                manualAdvance: !1,
                prevText: "Prev",
                nextText: "Next"
            })
        } else
            this_element.hasClass("wpb_image_grid") && (jQuery.fn.imagesLoaded ? $imagesGrid = this_element.find(".wpb_image_grid_ul").imagesLoaded(function() {
                $imagesGrid.isotope({
                    itemSelector: ".isotope-item",
                    layoutMode: "fitRows"
                })
            }) : this_element.find(".wpb_image_grid_ul").isotope({
                itemSelector: ".isotope-item",
                layoutMode: "fitRows"
            }))
    })
}
),
"function" != typeof window.vc_prettyPhoto && (window.vc_prettyPhoto = function() {
    try {
        jQuery && jQuery.fn && jQuery.fn.prettyPhoto && jQuery('a.prettyphoto, .gallery-icon a[href*=".jpg"]').prettyPhoto({
            animationSpeed: "normal",
            hook: "data-rel",
            padding: 15,
            opacity: .7,
            showTitle: !0,
            allowresize: !0,
            counter_separator_label: "/",
            hideflash: !1,
            deeplinking: !1,
            modal: !1,
            callback: function() {
                -1 < location.href.indexOf("#!prettyPhoto") && (location.hash = "")
            },
            social_tools: ""
        })
    } catch (err) {
        window.console && window.console.warn && console.warn(err)
    }
}
),
"function" != typeof window.vc_google_fonts && (window.vc_google_fonts = function() {
    return !1
}
),
window.vcParallaxSkroll = !1,
"function" != typeof window.vc_rowBehaviour && (window.vc_rowBehaviour = function() {
    var vcSkrollrOptions, callSkrollInit, $ = window.jQuery;
    function fullWidthRow() {
        var $elements = $('[data-vc-full-width="true"]');
        $.each($elements, function(key, item) {
            var $el = $(this);
            $el.addClass("vc_hidden");
            var $el_full = $el.next(".vc_row-full-width");
            if ($el_full.length || ($el_full = $el.parent().next(".vc_row-full-width")),
            $el_full.length) {
                var padding, paddingRight, el_margin_left = parseInt($el.css("margin-left"), 10), el_margin_right = parseInt($el.css("margin-right"), 10), offset = 0 - $el_full.offset().left - el_margin_left, width = $(window).width();
                if ("rtl" === $el.css("direction") && (offset -= $el_full.width(),
                offset += width,
                offset += el_margin_left,
                offset += el_margin_right),
                $el.css({
                    position: "relative",
                    left: offset,
                    "box-sizing": "border-box",
                    width: width
                }),
                !$el.data("vcStretchContent"))
                    "rtl" === $el.css("direction") ? ((padding = offset) < 0 && (padding = 0),
                    (paddingRight = offset) < 0 && (paddingRight = 0)) : ((padding = -1 * offset) < 0 && (padding = 0),
                    (paddingRight = width - padding - $el_full.width() + el_margin_left + el_margin_right) < 0 && (paddingRight = 0)),
                    $el.css({
                        "padding-left": padding + "px",
                        "padding-right": paddingRight + "px"
                    });
                $el.attr("data-vc-full-width-init", "true"),
                $el.removeClass("vc_hidden"),
                $(document).trigger("vc-full-width-row-single", {
                    el: $el,
                    offset: offset,
                    marginLeft: el_margin_left,
                    marginRight: el_margin_right,
                    elFull: $el_full,
                    width: width
                })
            }
        }),
        $(document).trigger("vc-full-width-row", $elements)
    }
    function fullHeightRow() {
        var windowHeight, offsetTop, fullHeight, $element = $(".vc_row-o-full-height:first");
        $element.length && (windowHeight = $(window).height(),
        (offsetTop = $element.offset().top) < windowHeight && (fullHeight = 100 - offsetTop / (windowHeight / 100),
        $element.css("min-height", fullHeight + "vh")));
        $(document).trigger("vc-full-height-row", $element)
    }
    $(window).off("resize.vcRowBehaviour").on("resize.vcRowBehaviour", fullWidthRow).on("resize.vcRowBehaviour", fullHeightRow),
    fullWidthRow(),
    fullHeightRow(),
    (0 < window.navigator.userAgent.indexOf("MSIE ") || navigator.userAgent.match(/Trident.*rv\:11\./)) && $(".vc_row-o-full-height").each(function() {
        "flex" === $(this).css("display") && $(this).wrap('<div class="vc_ie-flexbox-fixer"></div>')
    }),
    vc_initVideoBackgrounds(),
    callSkrollInit = !1,
    window.vcParallaxSkroll && window.vcParallaxSkroll.destroy(),
    $(".vc_parallax-inner").remove(),
    $("[data-5p-top-bottom]").removeAttr("data-5p-top-bottom data-30p-top-bottom"),
    $("[data-vc-parallax]").each(function() {
        var skrollrSize, skrollrStart, $parallaxElement, parallaxImage, youtubeId;
        callSkrollInit = !0,
        "on" === $(this).data("vcParallaxOFade") && $(this).children().attr("data-5p-top-bottom", "opacity:0;").attr("data-30p-top-bottom", "opacity:1;"),
        skrollrSize = 100 * $(this).data("vcParallax"),
        ($parallaxElement = $("<div />").addClass("vc_parallax-inner").appendTo($(this))).height(skrollrSize + "%"),
        (youtubeId = vcExtractYoutubeId(parallaxImage = $(this).data("vcParallaxImage"))) ? insertYoutubeVideoAsBackground($parallaxElement, youtubeId) : void 0 !== parallaxImage && $parallaxElement.css("background-image", "url(" + parallaxImage + ")"),
        skrollrStart = -(skrollrSize - 100),
        $parallaxElement.attr("data-bottom-top", "top: " + skrollrStart + "%;").attr("data-top-bottom", "top: 0%;")
    }),
    callSkrollInit && window.skrollr && (vcSkrollrOptions = {
        forceHeight: !1,
        smoothScrolling: !1,
        mobileCheck: function() {
            return !1
        }
    },
    window.vcParallaxSkroll = skrollr.init(vcSkrollrOptions),
    window.vcParallaxSkroll)
}
),
"function" != typeof window.vc_gridBehaviour && (window.vc_gridBehaviour = function() {
    jQuery.fn.vcGrid && jQuery("[data-vc-grid]").vcGrid()
}
),
"function" != typeof window.getColumnsCount && (window.getColumnsCount = function(el) {
    for (var find = !1, i = 1; !1 === find; ) {
        if (el.hasClass("columns_count_" + i))
            return find = !0,
            i;
        i++
    }
}
);
var screen_size = getSizeName();
function getSizeName() {
    var screen_w = jQuery(window).width();
    return 1170 < screen_w ? "desktop_wide" : 960 < screen_w && screen_w < 1169 ? "desktop" : 768 < screen_w && screen_w < 959 ? "tablet" : 300 < screen_w && screen_w < 767 ? "mobile" : screen_w < 300 ? "mobile_portrait" : ""
}
function loadScript(url, $obj, callback) {
    var script = document.createElement("script");
    script.type = "text/javascript",
    script.readyState && (script.onreadystatechange = function() {
        "loaded" !== script.readyState && "complete" !== script.readyState || (script.onreadystatechange = null,
        callback())
    }
    ),
    script.src = url,
    $obj.get(0).appendChild(script)
}
function vc_ttaActivation() {
    jQuery("[data-vc-accordion]").on("show.vc.accordion", function(e) {
        var $ = window.jQuery
          , ui = {};
        ui.newPanel = $(this).data("vc.accordion").getTarget(),
        window.wpb_prepare_tab_content(e, ui)
    })
}
function vc_accordionActivate(event, ui) {
    if (ui.newPanel.length && ui.newHeader.length) {
        var $pie_charts = ui.newPanel.find(".vc_pie_chart:not(.vc_ready)")
          , $round_charts = ui.newPanel.find(".vc_round-chart")
          , $line_charts = ui.newPanel.find(".vc_line-chart")
          , $carousel = ui.newPanel.find('[data-ride="vc_carousel"]');
        void 0 !== jQuery.fn.isotope && ui.newPanel.find(".isotope, .wpb_image_grid_ul").isotope("layout"),
        ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function() {
            var grid = jQuery(this).data("vcGrid");
            grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
        }),
        vc_carouselBehaviour(ui.newPanel),
        vc_plugin_flexslider(ui.newPanel),
        $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(),
        $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({
            reload: !1
        }),
        $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({
            reload: !1
        }),
        $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"),
        ui.newPanel.parents(".isotope").length && ui.newPanel.parents(".isotope").each(function() {
            jQuery(this).isotope("layout")
        })
    }
}
function initVideoBackgrounds() {
    return window.console && window.console.warn && window.console.warn("this function is deprecated use vc_initVideoBackgrounds"),
    vc_initVideoBackgrounds()
}
function vc_initVideoBackgrounds() {
    jQuery("[data-vc-video-bg]").each(function() {
        var youtubeId, $element = jQuery(this);
        $element.data("vcVideoBg") ? ((youtubeId = vcExtractYoutubeId($element.data("vcVideoBg"))) && ($element.find(".vc_video-bg").remove(),
        insertYoutubeVideoAsBackground($element, youtubeId)),
        jQuery(window).on("grid:items:added", function(event, $grid) {
            $element.has($grid).length && vcResizeVideoBackground($element)
        })) : $element.find(".vc_video-bg").remove()
    })
}
function insertYoutubeVideoAsBackground($element, youtubeId, counter) {
    if ("undefined" == typeof YT || void 0 === YT.Player)
        return 100 < (counter = void 0 === counter ? 0 : counter) ? void console.warn("Too many attempts to load YouTube api") : void setTimeout(function() {
            insertYoutubeVideoAsBackground($element, youtubeId, counter++)
        }, 100);
    var $container = $element.prepend('<div class="vc_video-bg vc_hidden-xs"><div class="inner"></div></div>').find(".inner");
    new YT.Player($container[0],{
        width: "100%",
        height: "100%",
        videoId: youtubeId,
        playerVars: {
            playlist: youtubeId,
            iv_load_policy: 3,
            enablejsapi: 1,
            disablekb: 1,
            autoplay: 1,
            controls: 0,
            showinfo: 0,
            rel: 0,
            loop: 1,
            wmode: "transparent"
        },
        events: {
            onReady: function(event) {
                event.target.mute().setLoop(!0)
            }
        }
    }),
    vcResizeVideoBackground($element),
    jQuery(window).bind("resize", function() {
        vcResizeVideoBackground($element)
    })
}
function vcResizeVideoBackground($element) {
    var iframeW, iframeH, marginLeft, marginTop, containerW = $element.innerWidth(), containerH = $element.innerHeight();
    containerW / containerH < 16 / 9 ? (iframeW = containerH * (16 / 9),
    iframeH = containerH,
    marginLeft = -Math.round((iframeW - containerW) / 2) + "px",
    marginTop = -Math.round((iframeH - containerH) / 2) + "px") : (iframeH = (iframeW = containerW) * (9 / 16),
    marginTop = -Math.round((iframeH - containerH) / 2) + "px",
    marginLeft = -Math.round((iframeW - containerW) / 2) + "px"),
    iframeW += "px",
    iframeH += "px",
    $element.find(".vc_video-bg iframe").css({
        maxWidth: "1000%",
        marginLeft: marginLeft,
        marginTop: marginTop,
        width: iframeW,
        height: iframeH
    })
}
function vcExtractYoutubeId(url) {
    if (void 0 === url)
        return !1;
    var id = url.match(/(?:https?:\/{2})?(?:w{3}\.)?youtu(?:be)?\.(?:com|be)(?:\/watch\?v=|\/)([^\s&]+)/);
    return null !== id && id[1]
}
if ("function" != typeof window.wpb_prepare_tab_content && (window.wpb_prepare_tab_content = function(event, ui) {
    var $ui_panel, $google_maps, panel = ui.panel || ui.newPanel, $pie_charts = panel.find(".vc_pie_chart:not(.vc_ready)"), $round_charts = panel.find(".vc_round-chart"), $line_charts = panel.find(".vc_line-chart"), $carousel = panel.find('[data-ride="vc_carousel"]');
    if (vc_carouselBehaviour(),
    vc_plugin_flexslider(panel),
    ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && ui.newPanel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function() {
        var grid = jQuery(this).data("vcGrid");
        grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
    }),
    panel.find(".vc_masonry_media_grid, .vc_masonry_grid").length && panel.find(".vc_masonry_media_grid, .vc_masonry_grid").each(function() {
        var grid = jQuery(this).data("vcGrid");
        grid && grid.gridBuilder && grid.gridBuilder.setMasonry && grid.gridBuilder.setMasonry()
    }),
    $pie_charts.length && jQuery.fn.vcChat && $pie_charts.vcChat(),
    $round_charts.length && jQuery.fn.vcRoundChart && $round_charts.vcRoundChart({
        reload: !1
    }),
    $line_charts.length && jQuery.fn.vcLineChart && $line_charts.vcLineChart({
        reload: !1
    }),
    $carousel.length && jQuery.fn.carousel && $carousel.carousel("resizeAction"),
    $ui_panel = panel.find(".isotope, .wpb_image_grid_ul"),
    $google_maps = panel.find(".wpb_gmaps_widget"),
    0 < $ui_panel.length && $ui_panel.isotope("layout"),
    $google_maps.length && !$google_maps.is(".map_ready")) {
        var $frame = $google_maps.find("iframe");
        $frame.attr("src", $frame.attr("src")),
        $google_maps.addClass("map_ready")
    }
    panel.parents(".isotope").length && panel.parents(".isotope").each(function() {
        jQuery(this).isotope("layout")
    })
}
),
"function" != typeof window.vc_googleMapsPointer)
    function vc_googleMapsPointer() {
        var $ = window.jQuery
          , $wpbGmapsWidget = $(".wpb_gmaps_widget");
        $wpbGmapsWidget.click(function() {
            $("iframe", this).css("pointer-events", "auto")
        }),
        $wpbGmapsWidget.mouseleave(function() {
            $("iframe", this).css("pointer-events", "none")
        }),
        $(".wpb_gmaps_widget iframe").css("pointer-events", "none")
    }
function vc_setHoverBoxPerspective(hoverBox) {
    hoverBox.each(function() {
        var $this = jQuery(this)
          , perspective = 4 * $this.width() + "px";
        $this.css("perspective", perspective)
    })
}
function vc_setHoverBoxHeight(hoverBox) {
    hoverBox.each(function() {
        var $this = jQuery(this)
          , hoverBoxInner = $this.find(".vc-hoverbox-inner");
        hoverBoxInner.css("min-height", 0);
        var frontHeight = $this.find(".vc-hoverbox-front-inner").outerHeight()
          , backHeight = $this.find(".vc-hoverbox-back-inner").outerHeight()
          , hoverBoxHeight = backHeight < frontHeight ? frontHeight : backHeight;
        hoverBoxHeight < 250 && (hoverBoxHeight = 250),
        hoverBoxInner.css("min-height", hoverBoxHeight + "px")
    })
}
function vc_prepareHoverBox() {
    var hoverBox = jQuery(".vc-hoverbox");
    vc_setHoverBoxHeight(hoverBox),
    vc_setHoverBoxPerspective(hoverBox)
}
jQuery(document).ready(vc_prepareHoverBox),
jQuery(window).resize(vc_prepareHoverBox),
jQuery(document).ready(function($) {
    window.vc_js()
});
