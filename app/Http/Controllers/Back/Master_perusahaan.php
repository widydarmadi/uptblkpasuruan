<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_perusahaan;
use App\Models\M_sumber_dana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_perusahaan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Perusahaan',
            'page_title' => 'Perusahaan',
            'parent_menu_active' => 'Lowongan Kerja',
            'child_menu_active'   => 'Perusahaan',
        ];

        return view('back.m_perusahaan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Perusahaan',
            'page_title' => 'Perusahaan',
            'parent_menu_active' => 'Lowongan Kerja',
            'child_menu_active'   => 'Perusahaan',
        ];

    	return view('back.m_perusahaan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_perusahaan.required' => 'harap diisi',
            'alamat_m_perusahaan.required' => 'harap diisi',
            'logo.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_perusahaan' => ['required'],
            'alamat_m_perusahaan' => ['required'],
            'logo' => ['required', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_perusahaan' => $errors->first('nm_m_perusahaan'),
                    'alamat_m_perusahaan' => $errors->first('alamat_m_perusahaan'),
                    'logo' => $errors->first('logo'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_perusahaan;
        $object->id_m_perusahaan = M_perusahaan::MaxId();
        $object->nm_m_perusahaan = $request->nm_m_perusahaan;
        $object->aktif_m_perusahaan = $request->aktif_m_perusahaan;
        $object->alamat_m_perusahaan = $request->alamat_m_perusahaan;
        $object->telp_m_perusahaan = $request->telp_m_perusahaan;
        $object->deskripsi_m_perusahaan = $request->deskripsi_m_perusahaan;
        $object->hrd_m_perusahaan = $request->hrd_m_perusahaan;
        $object->is_mitra = $request->is_mitra;

        try{

            if($request->file('logo')){
                $filename1 = time() . '_' . $request->file('logo')->getClientOriginalName();
                $folder = 'upload';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('logo'), $filename1);
                $object->logo = $path1;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_perusahaan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_perusahaan') or !is_numeric(request('id_m_perusahaan')), 404);

        $old = M_perusahaan::where([
            'id_m_perusahaan' => request('id_m_perusahaan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Perusahaan',
            'page_title' => 'Perusahaan',
            'parent_menu_active' => 'Lowongan Kerja',
            'child_menu_active'   => 'Perusahaan',
            'old' => $old,
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
        ];

        return view('back.m_perusahaan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_perusahaan.required' => 'harap diisi',
            'alamat_m_perusahaan.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_perusahaan' => ['required'],
            'alamat_m_perusahaan' => ['required'],
            'logo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_perusahaan' => $errors->first('nm_m_perusahaan'),
                    'alamat_m_perusahaan' => $errors->first('alamat_m_perusahaan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_perusahaan::where([
            'id_m_perusahaan' => $request->id_m_perusahaan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_perusahaan' => 'Data not found !',
                ]
            ]);
        }

        $update->nm_m_perusahaan = $request->nm_m_perusahaan;
        $update->aktif_m_perusahaan = $request->aktif_m_perusahaan;
        $update->alamat_m_perusahaan = $request->alamat_m_perusahaan;
        $update->telp_m_perusahaan = $request->telp_m_perusahaan;
        $update->deskripsi_m_perusahaan = $request->deskripsi_m_perusahaan;
        $update->hrd_m_perusahaan = $request->hrd_m_perusahaan;
        $update->is_mitra = $request->is_mitra;
        try{
            if($request->file('logo')){
                \Storage::delete('public/'.$update->logo);
                $filename1 = time() . '_' . $request->file('logo')->getClientOriginalName();
                $folder = 'upload';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('logo'), $filename1);
                $update->logo = $path1;
            }

            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_perusahaan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_perusahaan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_perusahaan::where([
            'id_m_perusahaan' => $request->id_m_perusahaan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->logo);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_perusahaan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_perusahaan::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_perusahaan;
            $datas[$key][] = ($value->is_mitra == '1') ? 'Ya' : 'Tidak';
            $datas[$key][] = ($value->aktif_m_perusahaan=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_perusahaan.edit',['id_m_perusahaan' => $value->id_m_perusahaan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_perusahaan="'.$value->id_m_perusahaan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
