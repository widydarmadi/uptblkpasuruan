<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_welcome;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Master_welcome extends Controller
{
    

    public function index()
    {

        $old = M_welcome::where([
            'id_m_welcome' => 1
        ])->firstOrFail();


        $data = [
            'head_title' => 'Admin Menu',
            'page_title' => 'Admin Menu',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Admin Menu',
            'old' => $old,
        ];

        return view('back.m_welcome.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'judul_primary_m_welcome.required' => 'harap diisi',
            'judul_secondary_m_welcome.required' => 'harap diisi',
            'video_m_welcome.required' => 'harap diisi',
            'teks_m_welcome.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'judul_primary_m_welcome' => ['required'],
            'judul_secondary_m_welcome' => ['required'],
            'video_m_welcome' => ['required'],
            'teks_m_welcome' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'judul_primary_m_welcome' => $errors->first('judul_primary_m_welcome'),
                    'judul_secondary_m_welcome' => $errors->first('judul_secondary_m_welcome'),
                    'video_m_welcome' => $errors->first('video_m_welcome'),
                    'teks_m_welcome' => $errors->first('teks_m_welcome'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_welcome::where([
            'id_m_welcome' => 1,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'judul_primary_m_welcome' => 'Data not found !',
                    'judul_secondary_m_welcome' => 'Data not found !',
                    'video_m_welcome' => 'Data not found !',
                    'teks_m_welcome' => 'Data not found !',
                ]
            ]);
        }

        $update->judul_primary_m_welcome = $request->judul_primary_m_welcome;
        $update->judul_secondary_m_welcome = $request->judul_secondary_m_welcome;
        $update->video_m_welcome = $request->video_m_welcome;
        $update->teks_m_welcome = $request->teks_m_welcome;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_welcome.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

}
