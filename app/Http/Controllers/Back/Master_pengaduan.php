<?php

namespace App\Http\Controllers\Back;
use PDF;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_pengaduan;
use App\Models\M_permasalahan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_pengaduan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Pengaduan',
            'page_title' => 'Pengaduan',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Pengaduan',
            'id_m_permasalahan' => M_permasalahan::orderBy('nm_m_permasalahan')->get(),
        ];

        return view('back.m_pengaduan.index')->with($data);
    }



    public function edit()
    {
        abort_if(!request()->filled('id_m_pengaduan') or !is_numeric(request('id_m_pengaduan')), 404);

        $old = M_pengaduan::where([
            'id_m_pengaduan' => request('id_m_pengaduan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Pengaduan',
            'page_title' => 'Pengaduan',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Pengaduan',
            'old' => $old,
        ];

        return view('back.m_pengaduan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_pengaduan.required' => 'harap diisi',
            'narasi_m_pengaduan.required' => 'harap diisi',
            'aktif_m_pengaduan.required' => 'please choose one',
            'photo.image' => 'harus format .jpg / .png',
            'photo2.image' => 'harus format .jpg / .png',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_pengaduan' => ['required'],
            'narasi_m_pengaduan' => ['required'],
            'aktif_m_pengaduan' => ['required'],
            'photo' => ['image', 'nullable'],
            'photo2' => ['image', 'nullable'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_m_pengaduan' => $errors->first('nm_m_pengaduan'),
                'narasi_m_pengaduan' => $errors->first('narasi_m_pengaduan'),
                'aktif_m_pengaduan' => $errors->first('aktif_m_pengaduan'),
                'photo' => $errors->first('photo'),
                'photo2' => $errors->first('photo2'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_pengaduan::where('id_m_pengaduan', $request->id_m_pengaduan)->first();
        $object->nm_m_pengaduan = $request->nm_m_pengaduan;
        $object->aktif_m_pengaduan = $request->aktif_m_pengaduan;
        $object->narasi_m_pengaduan = $request->narasi_m_pengaduan;
        $object->slug_m_pengaduan = Str::slug($request->nm_m_pengaduan);

        try{
         

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_pengaduan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_pengaduan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_pengaduan::where([
            'id_m_pengaduan' => $request->id_m_pengaduan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pengaduan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {
        $tgl_awal = request()->get('tgl_awal');
        $tgl_akhir = request()->get('tgl_akhir');
        $permasalahan = $request->id_m_permasalahan;
        $table = M_pengaduan::when(isset($tgl_awal) and isset($tgl_akhir), function($q) use ($tgl_awal, $tgl_akhir) {
                                    $q->whereDate('created_at', '>=', $tgl_awal);
                                    $q->whereDate('created_at', '<=', $tgl_akhir);
                                })
                                ->when(isset($permasalahan), function($q) use ($permasalahan) {
                                    $q->where('id_m_permasalahan', $permasalahan);
                                })
                                ->with('permasalahan')
                                ->orderByDesc('id_m_pengaduan')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->email_m_pengaduan;
            $datas[$key][] = ($value->status_pelapor) ? $value->status_pelapor : '-';
            $datas[$key][] = ($value->id_m_permasalahan) ? M_permasalahan::select('nm_m_permasalahan')->where( 'id_m_permasalahan', $value->id_m_permasalahan)->first()->nm_m_permasalahan : '-';
            $datas[$key][] = ($value->lokasi_m_pengaduan) ? $value->lokasi_m_pengaduan : '-';
            $datas[$key][] = ($value->created_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_pengaduan.edit',['id_m_pengaduan' => $value->id_m_pengaduan]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_pengaduan="'.$value->id_m_pengaduan.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }

    public function iframe_print(Request $request)
    {
        $data = [
            'tgl_awal' => $request->tgl_awal,
            'tgl_akhir' => $request->tgl_akhir,
            'permasalahan' => $request->id_m_permasalahan,
        ];
        return view('back.m_pengaduan.iframe_print', $data);
    }

    public function iframe_pdf()
    {
        $tgl_awal = request()->get('tgl_awal');
        $tgl_akhir = request()->get('tgl_akhir');
        $permasalahan = request()->get('permasalahan');
        $table = M_pengaduan::when(isset($tgl_awal) and isset($tgl_akhir), function($q) use ($tgl_awal, $tgl_akhir) {
                    $q->whereDate('created_at', '>=', $tgl_awal);
                    $q->whereDate('created_at', '<=', $tgl_akhir);
                 })
                 ->when(isset($permasalahan), function($q) use ($permasalahan) {
                    $q->where('id_m_permasalahan', $permasalahan);
                 })
                 ->with('permasalahan')
                 ->orderByDesc('id_m_pengaduan')->get();

        
    	
        $data = [
            'table' => $table,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
        ];
        $pdf = PDF::loadView('back.m_pengaduan.print_pdf', $data);
        $pdf->setPaper('Legal', 'landscape');
        return $pdf->stream("Report_Pengaduan_".time().".pdf", array("Attachment" => false));
    }
}
