<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_album;
use App\Models\T_album;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_galeri extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Galeri',
            'page_title' => 'Galeri',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Galeri',
        ];

        return view('back.m_galeri.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Galeri',
            'page_title' => 'Galeri',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Galeri',
        ];

    	return view('back.m_galeri.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'judul_m_album.required' => 'harap diisi',
            'aktif_m_album.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_album' => ['required'],
            'aktif_m_album' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'judul_m_album' => $errors->first('judul_m_album'),
                    'aktif_m_album' => $errors->first('aktif_m_album'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_album;
        $object->id_m_album = M_album::MaxId();
        $object->judul_m_album = $request->judul_m_album;
        $object->aktif_m_album = $request->aktif_m_album;
        $object->deskripsi_m_album = $request->deskripsi_m_album;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_galeri.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_album') or !is_numeric(request('id_m_album')), 404);

        $old = M_album::where([
            'id_m_album' => request('id_m_album')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Galeri',
            'page_title' => 'Galeri',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Galeri',
            'old' => $old,
        ];

        return view('back.m_galeri.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'judul_m_album.required' => 'harap diisi',
            'aktif_m_album.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_album' => ['required'],
            'aktif_m_album' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'judul_m_album' => $errors->first('judul_m_album'),
                    'aktif_m_album' => $errors->first('aktif_m_album'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_album::where([
            'id_m_album' => $request->id_m_album,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_album' => 'Data not found !',
                ]
            ]);
        }

        $update->judul_m_album = $request->judul_m_album;
        $update->aktif_m_album = $request->aktif_m_album;
        $update->deskripsi_m_album = $request->deskripsi_m_album;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_galeri.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_album')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_album::where([
            'id_m_album' => $request->id_m_album
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            foreach($find->foto as $foto){
                \Storage::delete('public/'.$foto->photo);
            }
            $find->foto()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_galeri.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_album::orderByDesc('id_m_album')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->judul_m_album;
            $datas[$key][] = ($value->aktif_m_album=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_galeri.edit',['id_m_album' => $value->id_m_album]).'">edit</a>
                                <a class="dropdown-item" href="'.route('admin.m_galeri.upload',['id_m_album' => $value->id_m_album]).'">upload foto</a>
                                <a class="dropdown-item delete" data-id_m_album="'.$value->id_m_album.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


    public function upload()
    {
        abort_if(!request()->filled('id_m_album') or !is_numeric(request('id_m_album')), 404);

        $old = M_album::where([
            'id_m_album' => request('id_m_album')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Galeri',
            'page_title' => 'Galeri',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Galeri',
            'old' => $old,
            'album' => T_album::where('id_m_album', request()->get('id_m_album'))->orderByDesc('created_at')->get(),
        ];

        return view('back.m_galeri.upload')->with($data);
    }

    public function upload_post(Request $request)
    {
        $messages = [
            'caption.required' => 'harap diisi',
            'photo.required' => 'harap pilih foto dahulu',
            'photo.image' => 'harus berupa file gambar',
        ];

        $validator = Validator::make($request->all(), [
            'caption' => ['required'],
            'photo' => ['required', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'caption' => $errors->first('caption'),
                    'photo' => $errors->first('photo'),
                ]
            ]);
        }


        DB::beginTransaction();
        
        $update = new T_album;
        $update->id_t_album = T_album::maxId();
        $update->id_m_album = $request->id_m_album;
        $update->caption = $request->caption;
        try{

            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/galeri';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $update->photo = $path1;
            }


            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Photo Uploaded',
                'redirect' => route('admin.m_galeri.upload', ['id_m_album' => $request->id_m_album]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }





    public function hapus_foto(Request $request)
    {
        if(!$request->filled('id_t_album')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_album::where([
            'id_t_album' => $request->id_t_album
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->photo);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_galeri.upload', ['id_m_album' => $request->id_m_album]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'error',
            'status'  => false,
        ]);
    }


}
