<?php

namespace App\Http\Controllers\Back;
use DB;
use App\Models\M_user_group;
use App\Models\M_menu;
use App\Models\M_hak_akses;
use App\Models\M_module;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class Master_user_group extends Controller
{

    public function index()
    {
        $data = [
            'head_title' => 'User Group',
            'page_title' => 'User Group',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'User Group',
        ];

        return view('back.m_user_group.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'User Group',
            'page_title' => 'User Group',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'User Group',
        ];

    	return view('back.m_user_group.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_user_group.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_user_group' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_user_group' => $errors->first('nm_user_group'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_user_group;
        $object->id_m_user_group = M_user_group::MaxId();
        $object->aktif = $request->aktif;
        $object->nm_user_group = $request->nm_user_group;
        $object->keterangan = $request->keterangan;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_user_group.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_user_group') or !is_numeric(request('id_m_user_group')), 404);

        $old = M_user_group::where([
            'id_m_user_group' => request('id_m_user_group')
        ])->firstOrFail();


        $data = [
            'head_title' => 'User Group',
            'page_title' => 'User Group',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'User Group',
            'old' => $old,
        ];

        return view('back.m_user_group.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_user_group.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_user_group' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_user_group' => $errors->first('nm_user_group'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_user_group::where([
            'id_m_user_group' => $request->id_m_user_group,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_user_group' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif = $request->aktif;
        $update->nm_user_group = $request->nm_user_group;
        $update->keterangan = $request->keterangan;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_user_group.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_user_group')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_user_group::where([
            'id_m_user_group' => $request->id_m_user_group
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            M_hak_akses::where('id_m_user_group', $request->id_m_user_group)->delete();

            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_user_group.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_user_group::orderByDesc('id_m_user_group')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_user_group;
            $datas[$key][] = $value->keterangan;
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = ($value->aktif=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Non Active</span>';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item edit_hakakses" data-id_m_user_group="'.$value->id_m_user_group.'" href="javascript:void(0)">manage permission</a>
                                        <a class="dropdown-item" href="'.route('admin.m_user_group.edit',['id_m_user_group' => $value->id_m_user_group]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_user_group="'.$value->id_m_user_group.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }




    public function hakakses()
    {
        abort_if(!request()->filled('id_m_user_group') or !is_numeric(request('id_m_user_group')), 404);

        $old = M_user_group::where([
            'id_m_user_group' => request('id_m_user_group')
        ])->firstOrFail();

        $menu = M_menu::whereNull('id_parent')->get();

        $data = [
            'head_title' => 'User Group',
            'page_title' => 'User Group',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'User Group',
            'old' => $old,
            'menu' => $menu,
            // 'id_m_module' => M_module::where('active_m_module','ACTIVE')->get(),
        ];

        return view('back.m_user_group.hakakses')->with($data);
    }


    // public function hakakses_update(Request $request)
    // {
    //     $old = M_menu::get();
    //     foreach ($old as $value) {
    //         if($request->input('cek_'.$value->id_m_menu) != ''){
    //             $aksesUpdate = M_hak_akses::where([
    //                 'id_m_menu' => $value->id_m_menu,
    //                 'id_m_user_group' => $request->id_m_user_group,
    //             ])->first();

    //             if($aksesUpdate == null)
    //             {
    //                 $new = new M_hak_akses;
    //                 $new->id_m_hak_akses = M_hak_akses::maxId();
    //                 $new->id_m_menu = $value->id_m_menu;
    //                 $new->id_m_user_group = $request->id_m_user_group;
    //                 $new->save();
    //             }

    //         }else{

    //             $aksesUpdate = M_hak_akses::where([
    //                 'id_m_menu' => $value->id_m_menu,
    //                 'id_m_user_group' => $request->id_m_user_group,
    //             ])->first();


    //             if($aksesUpdate)
    //             {
    //                 $aksesUpdate->delete();
    //             }

    //         }

    //     }

    //     return response()->json([
    //         'status' => true,
    //         'redirect' => route('admin.m_user_group.index'),
    //     ]);
    // }


    public function manage()
    {
        $old = M_user_group::where([
            'id_m_user_group' => request()->get('id_m_user_group')
        ])->firstOrFail();

        $menu = M_menu::whereNull('id_parent')->get();

        $data = [
            'old' => $old,
            // 'id_m_module' => request()->get('id_m_module'),
            'menu' => $menu,
        ];

        return view('back.m_user_group.manage_permission_permodule')->with($data);
    }


    public function manage_post(Request $request)
    {
        $old = M_menu::get();
        foreach ($old as $value) {
            if($request->input('cek_'.$value->id_m_menu) != ''){
                $aksesUpdate = M_hak_akses::where([
                    'id_m_menu' => $value->id_m_menu,
                    'id_m_user_group' => $request->id_m_user_group,
                ])->first();

                if($aksesUpdate == null)
                {
                    $new = new M_hak_akses;
                    $new->id_m_hak_akses = M_hak_akses::maxId();
                    $new->id_m_menu = $value->id_m_menu;
                    $new->id_m_user_group = $request->id_m_user_group;
                    $new->save();
                }

            }else{

                M_hak_akses::where([
                    'id_m_menu' => $value->id_m_menu,
                    'id_m_user_group' => $request->id_m_user_group,
                ])->delete();

            }

        }
        return response()->json([
            'status' => true,
            'redirect' => route('admin.m_user_group.index'),
        ]);
    }


}
