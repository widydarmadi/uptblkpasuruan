<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_bg;
use App\Models\M_perusahaan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_bg extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Background Dinamis',
            'page_title' => 'Background Dinamis',
            'parent_menu_active' => 'Pengaturan',
            'child_menu_active'   => 'Background Dinamis',
        ];

        return view('back.m_bg.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Background Dinamis',
            'page_title' => 'Background Dinamis',
            'parent_menu_active' => 'Pengaturan',
            'child_menu_active'   => 'Background Dinamis',
        ];

    	return view('back.m_bg.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_posisi_m_lowongan.required' => 'harap diisi',
            'deskripsi_m_lowongan.required' => 'harap diisi',
            'id_m_perusahaan.required' => 'harap diisi',
            'date_start.required' => 'harap diisi',
            'date_end.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
        ];

        $validator = Validator::make($request->all(), [
            'nm_posisi_m_lowongan' => ['required'],
            'deskripsi_m_lowongan' => ['required'],
            'id_m_perusahaan' => ['required'],
            'date_start' => ['required'],
            'date_end' => ['required'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_posisi_m_lowongan' => $errors->first('nm_posisi_m_lowongan'),
                'deskripsi_m_lowongan' => $errors->first('deskripsi_m_lowongan'),
                'id_m_perusahaan' => $errors->first('id_m_perusahaan'),
                'date_start' => $errors->first('date_start'),
                'date_end' => $errors->first('date_end'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_bg;
        $object->id_m_bg = M_bg::MaxId();
        $object->nm_posisi_m_lowongan = $request->nm_posisi_m_lowongan;
        $object->deskripsi_m_lowongan = $request->deskripsi_m_lowongan;
        $object->id_m_perusahaan = $request->id_m_perusahaan;
        $object->date_start = $request->date_start;
        $object->date_end = $request->date_end;
        try{

            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/lowongan';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_bg.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_bg') or !is_numeric(request('id_m_bg')), 404);

        $old = M_bg::where([
            'id_m_bg' => request('id_m_bg')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Background Dinamis',
            'page_title' => 'Background Dinamis',
            'parent_menu_active' => 'Pengaturan',
            'child_menu_active'   => 'Background Dinamis',
            'old' => $old,
        ];

        return view('back.m_bg.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_bg.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_bg' => ['required'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_m_bg' => $errors->first('nm_m_bg'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_bg::where('id_m_bg', $request->id_m_bg)->first();
        $object->nm_m_bg = $request->nm_m_bg;

        try{

            if($request->file('photo')){
                \Storage::delete('public/'.$object->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/bg';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }
           
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_bg.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {
        $table = M_bg::orderByDesc('id_m_bg')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_bg;
            $datas[$key][] = isset($value->photo) ? '<img src="'.asset('storage'.'/'.$value->photo).'" width="100px" />' : '-';
            $datas[$key][] = ($value->updated_at) ? \Carbon\Carbon::parse($value->updated_at)->format('d-m-Y H:i:s') : \Carbon\Carbon::parse($value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_bg.edit',['id_m_bg' => $value->id_m_bg]).'">edit</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


    
    public function reset(Request $request)
    {
        if(!$request->filled('id_m_bg')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_bg::where([
            'id_m_bg' => $request->id_m_bg
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->photo);
            $find->photo = null;
            $find->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_bg.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }

}
