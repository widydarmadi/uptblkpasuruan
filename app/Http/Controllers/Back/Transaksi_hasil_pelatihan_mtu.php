<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_pendaftar;
use App\Models\M_program_pelatihan;
use App\Models\T_kelas_pelatihan;
use App\Models\T_peserta;
use App\Models\T_seleksi;
use App\Models\T_siswa_noninst;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Transaksi_hasil_pelatihan_mtu extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
        ->whereHas('kejuruan', function($q){
            $q->where('aktif_m_kejuruan', '1');
            $q->whereHas('program_pelatihan', function($q){
                $q->where('aktif', '1')
                  ->where('id_m_tipe_pelatihan', 2)
                  ->whereHas('jadwal', function($q){
                  });
            });
        })->get();

        $data = [
            'head_title' => 'Hasil Pelatihan',
            'page_title' => 'Hasil Pelatihan',
            'parent_menu_active' => 'Pelatihan MTU',
            'child_menu_active'   => 'Hasil Pelatihan',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
        ];

        return view('back.t_hasil_pelatihan_mtu.index')->with($data);
    }

    public function edit()
    {
        abort_if(!request()->filled('id_t_kelas_pelatihan') or !is_numeric(request('id_t_kelas_pelatihan')), 404);

        $old = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => request('id_t_kelas_pelatihan')
        ])
        ->with('peserta_mtu','pendaftar_mtu:no_register,nm_pemohon')
        ->with('jadwal', function($q){
            $q->select('id_m_jadwal', 'id_m_gelombang','tahun','id_m_program_pelatihan','nm_m_jadwal');
            $q->with('gelombang:id_m_gelombang,nm_m_gelombang');
        })
        ->firstOrFail();

        $jadwal = $old->jadwal->select('id_m_jadwal','nm_m_jadwal')->get();

        $prog = M_program_pelatihan::select('id_m_program_pelatihan','nm_m_program_pelatihan')
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 2)
                ->whereHas('jadwal', function($q){
                    // $q->where('status', 'OPEN');
                    // $q->where('tahun', date('Y'));
                })
                ->where('id_m_program_pelatihan', $old->jadwal->id_m_program_pelatihan)
                ->orderByDesc('id_m_program_pelatihan')
                ->get();


        $data = [
            'head_title' => 'Hasil Pelatihan',
            'page_title' => 'Hasil Pelatihan',
            'parent_menu_active' => 'Pelatihan MTU',
            'child_menu_active'   => 'Hasil Pelatihan',
            'old' => $old,
            'id_m_program_pelatihan' => $prog,
            'id_m_jadwal' => $jadwal,
            'selected_program' => $old->jadwal->id_m_program_pelatihan,
            'selected_jadwal' => $old->id_m_jadwal,
        ];

        return view('back.t_hasil_pelatihan_mtu.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nama_kelas.required' => 'harap diisi',
            'id_m_program_pelatihan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_kelas' => ['required'],
            'id_m_program_pelatihan' => ['required'],
            'id_m_jadwal' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nama_kelas' => $errors->first('nama_kelas'),
                    'id_m_program_pelatihan' => $errors->first('id_m_program_pelatihan'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                ]
            ]);
        }

        if(!is_array($request->pilih_siswa)){
            return response()->json([
                'status' => false,
                'message' => 'pilih setidaknya 1 (satu) peserta yang memenuhi standar kompetensi',
            ]);
        }


        DB::beginTransaction();
        $update = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => $request->id_t_kelas_pelatihan,
            ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nama_kelas' => 'Data not found !',
                ]
            ]);
        }

        try{

            //reset kelulusan data peserta
            $clear_peserta = T_peserta::where([
                        'id_t_kelas_pelatihan' => $request->id_t_kelas_pelatihan,
                      ])->update([
                        'status' => null
                      ]);

            //re-update data peserta
            for($i=0; $i < count($request->pilih_siswa); $i++){
                $id_seleksi = T_peserta::where('id_t_seleksi', $request->pilih_siswa[$i])
                                ->where('id_t_kelas_pelatihan', $request->id_t_kelas_pelatihan)
                                ->first();
                if($id_seleksi){
                    $id_seleksi->update(
                        [
                            'status' => 'KOMPETEN',
                        ]
                    );
                }
            }

            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data kelulusan telah diperbaharui !',
                'redirect' => route('admin.t_hasil_pelatihan_mtu.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan;
        $id_m_kejuruan = $request->id_m_kejuruan;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan;
        $id_m_jadwal = $request->id_m_jadwal;
        $table = T_kelas_pelatihan::select('id_t_kelas_pelatihan','id_m_jadwal','nama_kelas', 'status','created_at')
                                    ->whereHas('jadwal', function($q){
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('id_m_tipe_pelatihan', 2);
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        !isset($id_m_kejuruan) and
                                        !isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_kategori_kejuruan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                                                $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                                    $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                                                });
                                            });
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        !isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_kejuruan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                                                $q->where('id_m_kejuruan', $id_m_kejuruan);
                                            });
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_program_pelatihan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                                            $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        isset($id_m_program_pelatihan) and
                                        isset($id_m_jadwal)
                                    , function($q) use ($id_m_jadwal){
                                        $q->where('id_m_jadwal', $id_m_jadwal);
                                    })
                                    ->with('jadwal:id_m_jadwal,nm_m_jadwal')
                                    ->withCount('peserta')
                                    ->with('peserta:id_t_kelas_pelatihan,id_t_peserta,status')
                                    ->orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nama_kelas;
            $datas[$key][] = $value->jadwal->nm_m_jadwal;
            $datas[$key][] = number_format($value->peserta_count, 0, '.', '.');
            $datas[$key][] = ($value->peserta) ? $value->peserta->where('status', 'KOMPETEN')->count() : 0;
            $datas[$key][] = ($value->peserta) ? $value->peserta->where('status','<>' ,'KOMPETEN')->count() : 0;
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.t_hasil_pelatihan_mtu.edit',['id_t_kelas_pelatihan' => $value->id_t_kelas_pelatihan]).'">penilaian</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


    public function load_jadwal(Request $request)
    {
        if($request->id_m_program_pelatihan){
            $jadwal = M_jadwal::select('id_m_jadwal','nm_m_jadwal')->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)->where('status', 'OPEN')->get();
        }else{
            $jadwal = null;
        }

        $html = ($jadwal and count($jadwal) > 0) ? 
                '<option value="">-- pilih jadwal pelatihan --</option>' :
                '<option value="">-- tidak ada jadwal --</option>';

        if($jadwal != null){
            foreach($jadwal as $jad){
                $html .= '<option value="'.$jad->id_m_jadwal.'">'.$jad->nm_m_jadwal.'</option>';
            }
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }

    public function list_siswa(Request $request)
    {
        if($request->no_register){
            $list_siswa = T_siswa_noninst::select('id_t_siswa_noninst', 'nm_jurusan', 
                                'nm_t_siswa', 'nik_t_siswa', 'jk_t_siswa', 'alamat_t_siswa', 'id_m_pendidikan')
                                ->where('no_register', $request->no_register)
                                ->with('pendidikan:id_m_pendidikan,nm_m_pendidikan')
                                ->with('peserta_noninst')
                                ->orderBy('nm_t_siswa')
                                ->get();
            $data = [
                'list_siswa' => $list_siswa,
            ];
            
            return view('back.t_pemetaan_kelas_mtu.list_siswa', $data);
        }
    }


    public function load_kejuruan_available(Request $request)
    {
        $load = M_kejuruan::where('id_m_kategori_kejuruan', $request->id)
                ->where('aktif_m_kejuruan', '1')
                ->whereHas('program_pelatihan', function($q){
                    $q->where('aktif', '1')
                      ->where('id_m_tipe_pelatihan', 2)
                      ->whereHas('jadwal', function($q){
                      });
                })->get();
        $html = '';
        $html .= "<option value=''>Pilih Sub Kejuruan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_kejuruan."'>".$item->nm_m_kejuruan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    
    public function load_program_available(Request $request)
    {
        $load = M_program_pelatihan::select('id_m_program_pelatihan', 'nm_m_program_pelatihan')->where('id_m_kejuruan', $request->id)
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 2)
                ->whereHas('jadwal', function($q){
                })->get();

        $html = '';
        $html .= "<option value=''>Pilih Program Pelatihan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_program_pelatihan."'>".$item->nm_m_program_pelatihan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    public function load_jadwal_available(Request $request)
    {
        $load = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $request->id)
                ->get();

        $html = '';
        $html .= "<option value=''>Pilih Jadwal / Gelombang</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_jadwal."'>".$item->nm_m_jadwal."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    


}
