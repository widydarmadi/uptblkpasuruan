<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_agenda;
use App\Models\M_teks_footer;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_teks_footer extends Controller
{
   

    public function edit()
    {
        $old = M_teks_footer::where([
            'id_m_teks_footer' => 1
        ])->firstOrFail();


        $data = [
            'head_title' => 'Teks Footer',
            'page_title' => 'Teks Footer',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Teks Footer',
            'old' => $old,
        ];

        return view('back.m_teks_footer.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'alamat_kantor.required' => 'harap diisi',
            'jam_kerja.required' => 'harap diisi',
            'ig.required' => 'harap diisi',
            'email.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'alamat_kantor' => ['required'],
            'jam_kerja' => ['required'],
            'ig' => ['required'],
            'email' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'alamat_kantor' => $errors->first('alamat_kantor'),
                    'jam_kerja' => $errors->first('jam_kerja'),
                    'ig' => $errors->first('ig'),
                    'email' => $errors->first('email'),
                ]
            ]);
        }

        DB::beginTransaction();
        $object = M_teks_footer::where('id_m_teks_footer', 1)->first();
        $object->alamat_kantor = $request->alamat_kantor;
        $object->jam_kerja = $request->jam_kerja;
        $object->ig = $request->ig;
        $object->email = $request->email;
        $object->telp = $request->telp;
        $object->youtube = $request->youtube;
        try{
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_teks_footer.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

}
