<?php

namespace App\Http\Controllers\Back;

use App\Exports\PendaftarInsExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Cbt_user;
use App\Models\M_disabilitas;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_kota;
use App\Models\M_pendaftar;
use App\Models\M_program_pelatihan;
use App\Models\M_provinsi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class Master_pendaftar extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
                                    ->whereHas('kejuruan', function($q){
                                        $q->where('aktif_m_kejuruan', '1');
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('aktif', '1')
                                              ->where('id_m_tipe_pelatihan', 1)
                                              ->whereHas('jadwal');
                                        });
                                    })->get();
        /** 
         * original program pelatihan
         */
        if(request()->filled('id_m_jadwal')){
            $fk_program = M_jadwal::select('id_m_program_pelatihan')
                          ->where('id_m_jadwal', request()->get('id_m_jadwal'))
                          ->with('program_pelatihan', function($q){
                            $q->select('id_m_program_pelatihan','id_m_kejuruan');
                            $q->with('kejuruan', function($q){
                                $q->select('id_m_kejuruan', 'id_m_kategori_kejuruan');
                            });
                          })
                          ->first();
            $list_jadwal = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $fk_program->id_m_program_pelatihan)->get()->toArray();
            $list_program_pelatihan = ($fk_program->program_pelatihan) ? 
                                      M_program_pelatihan::where('id_m_kejuruan', $fk_program->program_pelatihan->id_m_kejuruan)->get()->toArray() : [];
            $list_kejuruan = ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? 
                             M_kejuruan::where('id_m_kategori_kejuruan', $fk_program->program_pelatihan->kejuruan->id_m_kategori_kejuruan)->get()->toArray() : [];
        }else{
            $fk_program = [];
            $list_jadwal = [];
            $list_program_pelatihan = [];
            $list_kejuruan = [];
        }
        $list_tahun = M_pendaftar::selectRaw('YEAR(created_at) as tahun')->groupBy('tahun')->get()->toArray();

        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Institusional',
            'child_menu_active'   => 'Data Pendaftaran',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
            'list_jadwal' => $list_jadwal,
            'list_program_pelatihan' => $list_program_pelatihan,
            'list_kejuruan' => $list_kejuruan,
            'id_m_program_pelatihan' => ($fk_program and $fk_program->program_pelatihan) ? $fk_program->program_pelatihan->id_m_program_pelatihan : null,
            'id_m_kejuruan' => ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? $fk_program->program_pelatihan->kejuruan->id_m_kejuruan : null,
            'id_m_kategori_kejuruan' => ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? $fk_program->program_pelatihan->kejuruan->id_m_kategori_kejuruan : null,
            'list_tahun' => $list_tahun,
        ];

        return view('back.m_pendaftar.index')->with($data);
    }

    public function add()
    {
        $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            // ->whereYear('created_at', date('Y'))
                            // ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            // ->where('status', 'OPEN')
                            ->with('gelombang')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 1);
                            })
                            ->orderByDesc('tgl_mulai')->get();

        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Institusional',
            'child_menu_active'   => 'Data Pendaftaran',
            'list_gelombang' => $list_gelombang,
            'id_m_jadwal' => null,
        ];

    	return view('back.m_pendaftar.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'cbt_grup_id.required' => "CBT ID masih kosong",
            'id_m_jadwal.required' => "pilih salah satu",
            'nm_m_pendaftar.required' => "Harap mengisi nama Anda",
            'nik_m_pendaftar.required' => "Harap mengisi nomor induk kependudukan Anda",
            // 'nik_m_pendaftar.size' => "Harap mengisikan 16 digit NIK",
            'nik_m_pendaftar.numeric' => "Harap mengisikan 16 digit NIK berupa angka",
            'id_m_provinsi.required' => "Pilih provinsi",
            'id_m_kota.required' => "Pilih kota",
            'alamat_m_pendaftar.required' => "Harap mengisi alamat sesuai domisili",
            'jk_m_pendaftar.required' => "Pilih salah satu",
            'wa_m_pendaftar.required' => "Harap mengisi nomor whatsapp aktif",
            'tempat_lahir_m_pendaftar.required' => "Harap mengisi tempat lahir",
            'tgl_lahir_m_pendaftar.required' => "Harap mengisi tanggal lahir",
            'email_m_pendaftar.required' => "Harap mengisi email Anda",
            'email_m_pendaftar.email' => "Alamat email yang dimasukkan tidak valid",
            'quest_pernah_blk.required' => "Pilih salah satu",
            'quest_masih_bekerja.required' => "Pilih salah satu",
            'id_m_kejuruan.required' => "Pilih salah satu",
            'quest_minat_setelah_kursus.required' => "Pilih salah satu",
            'is_disabilitas.required' => "Pilih salah satu",
            'id_m_disabilitas.required' => "Pilih salah satu",
            'tgl_daftar.required' => "harap diisi",
        ];


        $role_disabilitas = ($request->is_disabilitas == 'YA') ? ['required'] : ['nullable'];

        $validator = Validator::make($request->all(), [
            'cbt_grup_id' => ['required'],
            'id_m_jadwal' => ['required'],
            'nm_m_pendaftar' => ['required'],
            'nik_m_pendaftar' => ['required', 'numeric'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'alamat_m_pendaftar' => ['required'],
            'jk_m_pendaftar' => ['required'],
            'id_m_pendidikan' => ['required'],
            'wa_m_pendaftar' => ['required'],
            'tempat_lahir_m_pendaftar' => ['required'],
            'tgl_lahir_m_pendaftar' => ['required'],
            'email_m_pendaftar' => ['required','email'],
            'quest_pernah_blk' => ['required'],
            'quest_masih_bekerja' => ['required'],
            // 'id_m_kejuruan' => ['required'],
            'quest_minat_setelah_kursus' => ['required'],
            'is_disabilitas' => ['required'],
            'tgl_daftar' => ['required'],
            'id_m_disabilitas' => $role_disabilitas,
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'cbt_grup_id' => $errors->first('cbt_grup_id'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_m_pendaftar' => $errors->first('nm_m_pendaftar'),
                    'nik_m_pendaftar' => $errors->first('nik_m_pendaftar'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'alamat_m_pendaftar' => $errors->first('alamat_m_pendaftar'),
                    'jk_m_pendaftar' => $errors->first('jk_m_pendaftar'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'wa_m_pendaftar' => $errors->first('wa_m_pendaftar'),
                    'tempat_lahir_m_pendaftar' => $errors->first('tempat_lahir_m_pendaftar'),
                    'tgl_lahir_m_pendaftar' => $errors->first('tgl_lahir_m_pendaftar'),
                    'email_m_pendaftar' => $errors->first('email_m_pendaftar'),
                    'quest_pernah_blk' => $errors->first('quest_pernah_blk'),
                    'quest_masih_bekerja' => $errors->first('quest_masih_bekerja'),
                    'quest_minat_setelah_kursus' => $errors->first('quest_minat_setelah_kursus'),
                    'is_disabilitas' => $errors->first('is_disabilitas'),
                    'tgl_daftar' => $errors->first('tgl_daftar'),
                    'id_m_disabilitas' => $errors->first('id_m_disabilitas'),
                ]
            ]);
        }


        $find = M_pendaftar::select('id_m_pendaftar')
                ->where('nik_m_pendaftar', $request->nik_m_pendaftar)
                ->whereYear('created_at', date('Y'))
                ->with('seleksi', function($q){
                    $q->where('status', 'LULUS');
                    $q->with('peserta:id_t_seleksi,id_t_peserta');
                });

        if($find->count() > 0){
            $data_pendaftar = $find->first();
            if($data_pendaftar->seleksi and $data_pendaftar->seleksi->peserta){
                return response()->json([
                    'message' => 'Anda tidak dapat mendaftar dengan identitas kependudukan (NIK) yang sama lebih dari 1 (satu) kali dalam setahun',
                    'status'  => false,
                ]);
            }
        }
        
        $tahun = Carbon::parse($request->tgl_daftar)->format('Y');
        $bulan = Carbon::parse($request->tgl_daftar)->format('m');
        $tanggal = Carbon::parse($request->tgl_daftar)->format('d');
        $bulantanggal = Carbon::parse($request->tgl_daftar)->format('md');
        $bulantahun = Carbon::parse($request->tgl_daftar)->format('mY');
        // $tahun = date('Y');
        // $bulantanggal = date('md');
        // dd($tahun,$bulantanggal,$bulantahun);

        $max = M_pendaftar::whereYear('created_at', $tahun)->whereMonth('created_at', $bulan)->whereDay('created_at', $tanggal)->max('no_register_urut');
        $no_urut_reg = $max + 1;
        $tahun_short = substr($tahun,2,2);
        $no_reg_ok = sprintf("%04s", $no_urut_reg);
        $tipe_pelatihan = 'INS';

        $noreg = 'REG-'.$tahun_short.'-BLKPAS-'.$bulantanggal.'-'.$tipe_pelatihan.'-'.$no_reg_ok.'';
        $noreg_cbt = $tipe_pelatihan.$tahun_short.$bulantanggal.$no_reg_ok;

        $new = new M_pendaftar;
        $new->id_m_pendaftar = M_pendaftar::maxId();
        $new->no_register = $noreg;
        $new->no_register_urut = $no_urut_reg;
        $new->id_m_jadwal = $request->id_m_jadwal;
        $new->nm_m_pendaftar = Str::upper($request->nm_m_pendaftar);
        $new->nik_m_pendaftar = $request->nik_m_pendaftar;
        $new->id_m_provinsi = $request->id_m_provinsi;
        $new->id_m_kota = $request->id_m_kota;
        $new->alamat_m_pendaftar =  $request->alamat_m_pendaftar;
        $new->jk_m_pendaftar =  $request->jk_m_pendaftar;
        $new->id_m_pendidikan =  $request->id_m_pendidikan;
        $new->wa_m_pendaftar =  $request->wa_m_pendaftar;
        $new->nm_jurusan =  $request->nm_jurusan;
        $new->tempat_lahir_m_pendaftar =  $request->tempat_lahir_m_pendaftar;
        $new->tgl_lahir_m_pendaftar = $request->tgl_lahir_m_pendaftar;
        $new->email_m_pendaftar =  $request->email_m_pendaftar;
        $new->quest_pernah_blk =  $request->quest_pernah_blk;
        $new->quest_masih_bekerja =  $request->quest_masih_bekerja;
        $new->quest_minat_setelah_kursus =  $request->quest_minat_setelah_kursus;
        // $new->id_m_kejuruan =  $request->id_m_kejuruan;
        $new->kategori_m_pendaftar =  ($request->cat) ? $request->cat : 'PBK';
        $new->cbt_grup_id =  $request->cbt_grup_id;
        $new->is_disabilitas =  $request->is_disabilitas;
        $new->id_m_disabilitas =  $request->id_m_disabilitas;
        $new->created_at =  (date('Y-m-d') == $request->tgl_daftar) ? now() : $request->tgl_daftar;

        /** 
         * create or update cbt_user
         */

        $new_cbt = new Cbt_user;
        $new_cbt->user_id = Cbt_user::MaxId();
        $new_cbt->user_grup_id = $request->cbt_grup_id;
        $new_cbt->user_name = $noreg_cbt;
        $new_cbt->user_password = '123456';
        $new_cbt->user_email = $request->email_m_pendaftar;
        $new_cbt->user_regdate = now();
        $new_cbt->user_firstname = $request->nm_m_pendaftar;
        $new_cbt->user_level = 1;
        try{

            $pelatihan = M_jadwal::select('nm_m_jadwal')->where('id_m_jadwal', $request->id_m_jadwal)->first();
            app()->setLocale('id');
            $details = [
                'nama' => Str::upper($request->nm_m_pendaftar),
                'username' => $noreg_cbt,
                'pelatihan' => (($pelatihan) ? $pelatihan->nm_m_jadwal : ''),
                'tgl_lahir' => Carbon::parse(date('d-m-Y', strtotime($request->tgl_lahir_m_pendaftar)))->isoFormat('D MMMM YYYY'),
            ];

            //Mail::to($request->email_m_pendaftar)->send(new \App\Mail\MyTestMail($details));

            $new->save();
            $new_cbt->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pendaftar.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }



    public function edit()
    {
        $old = M_pendaftar::where('id_m_pendaftar', request()->get('id_m_pendaftar'))->firstOrFail();
        $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            // ->whereYear('created_at', date('Y'))
                            // ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            // ->where('status', 'OPEN')
                            ->with('gelombang')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 1);
                            })
                            ->orderByDesc('tgl_mulai')->get();

        $list_kab = M_kota::where('id_m_provinsi', $old->id_m_provinsi)->orderBy('nm_m_kota')->get();
        $list_prov = M_provinsi::orderBy('nm_m_provinsi')->get();
        $list_disabilitas = M_disabilitas::where('aktif', '1')->get();

        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Institusional',
            'child_menu_active'   => 'Data Pendaftaran',
            'list_gelombang' => $list_gelombang,
            'old' => $old,
            'id_m_jadwal' => null,
            'id_m_provinsi' => $old->id_m_provinsi,
            'list_kab' => $list_kab,
            'list_prov' => $list_prov,
            'list_disabilitas' => $list_disabilitas,
        ];

    	return view('back.m_pendaftar.edit')->with($data);
    }


    public function update(Request $request)
    {
        $messages = [
            'cbt_grup_id.required' => "CBT ID masih kosong",
            'id_m_jadwal.required' => "pilih salah satu",
            'nm_m_pendaftar.required' => "Harap mengisi nama Anda",
            'nik_m_pendaftar.required' => "Harap mengisi nomor induk kependudukan Anda",
            // 'nik_m_pendaftar.size' => "Harap mengisikan 16 digit NIK",
            'nik_m_pendaftar.numeric' => "Harap mengisikan 16 digit NIK berupa angka",
            'id_m_provinsi.required' => "Pilih provinsi",
            'id_m_kota.required' => "Pilih kota",
            'alamat_m_pendaftar.required' => "Harap mengisi alamat sesuai domisili",
            'jk_m_pendaftar.required' => "Pilih salah satu",
            'wa_m_pendaftar.required' => "Harap mengisi nomor whatsapp aktif",
            'tempat_lahir_m_pendaftar.required' => "Harap mengisi tempat lahir",
            'tgl_lahir_m_pendaftar.required' => "Harap mengisi tanggal lahir",
            'email_m_pendaftar.required' => "Harap mengisi email Anda",
            'email_m_pendaftar.email' => "Alamat email yang dimasukkan tidak valid",
            'quest_pernah_blk.required' => "Pilih salah satu",
            'quest_masih_bekerja.required' => "Pilih salah satu",
            'id_m_kejuruan.required' => "Pilih salah satu",
            'is_disabilitas.required' => "Pilih salah satu",
            'tgl_daftar.required' => "harap diisi",
            'id_m_disabilitas.required' => "Pilih salah satu",
        ];


        $role_disabilitas = ($request->is_disabilitas == 'YA') ? ['required'] : ['nullable'];

        $validator = Validator::make($request->all(), [
            'cbt_grup_id' => ['required'],
            'id_m_jadwal' => ['required'],
            'nm_m_pendaftar' => ['required'],
            'nik_m_pendaftar' => ['required', 'numeric'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'alamat_m_pendaftar' => ['required'],
            'jk_m_pendaftar' => ['required'],
            'id_m_pendidikan' => ['required'],
            'wa_m_pendaftar' => ['required'],
            'tempat_lahir_m_pendaftar' => ['required'],
            'tgl_lahir_m_pendaftar' => ['required'],
            'email_m_pendaftar' => ['required','email'],
            'quest_pernah_blk' => ['required'],
            'quest_masih_bekerja' => ['required'],
            // 'id_m_kejuruan' => ['required'],
            'quest_minat_setelah_kursus' => ['required'],
            'is_disabilitas' => ['required'],
            'tgl_daftar' => ['required'],
            'id_m_disabilitas' => $role_disabilitas,
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'cbt_grup_id' => $errors->first('cbt_grup_id'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_m_pendaftar' => $errors->first('nm_m_pendaftar'),
                    'nik_m_pendaftar' => $errors->first('nik_m_pendaftar'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'alamat_m_pendaftar' => $errors->first('alamat_m_pendaftar'),
                    'jk_m_pendaftar' => $errors->first('jk_m_pendaftar'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'wa_m_pendaftar' => $errors->first('wa_m_pendaftar'),
                    'tempat_lahir_m_pendaftar' => $errors->first('tempat_lahir_m_pendaftar'),
                    'tgl_lahir_m_pendaftar' => $errors->first('tgl_lahir_m_pendaftar'),
                    'email_m_pendaftar' => $errors->first('email_m_pendaftar'),
                    'quest_pernah_blk' => $errors->first('quest_pernah_blk'),
                    'quest_masih_bekerja' => $errors->first('quest_masih_bekerja'),
                    'quest_minat_setelah_kursus' => $errors->first('quest_minat_setelah_kursus'),
                    'is_disabilitas' => $errors->first('is_disabilitas'),
                    'id_m_disabilitas' => $errors->first('id_m_disabilitas'),
                    'tgl_daftar' => $errors->first('tgl_daftar'),
                ]
            ]);
        }

        $new = M_pendaftar::where('id_m_pendaftar', $request->id_m_pendaftar)->first();
        
        if(Carbon::parse($new->created_at)->format('Y-m-d') != $request->tgl_daftar){
            $tahun = Carbon::parse($request->tgl_daftar)->format('Y');
            $bulan = Carbon::parse($request->tgl_daftar)->format('m');
            $tanggal = Carbon::parse($request->tgl_daftar)->format('d');
            $bulantanggal = Carbon::parse($request->tgl_daftar)->format('md');

            $max = M_pendaftar::whereYear('created_at', $tahun)->whereMonth('created_at', $bulan)->whereDay('created_at', $tanggal)->max('no_register_urut');
            $no_urut_reg = $max + 1;
            $tahun_short = substr($tahun,2,2);
            $no_reg_ok = sprintf("%04s", $no_urut_reg);
            $tipe_pelatihan = 'INS';

            $noreg = 'REG-'.$tahun_short.'-BLKPAS-'.$bulantanggal.'-'.$tipe_pelatihan.'-'.$no_reg_ok.'';
            $noreg_cbt = $tipe_pelatihan.$tahun_short.$bulantanggal.$no_reg_ok;
            $new->created_at =  $request->tgl_daftar;
            $new->no_register = $noreg;
            $new->no_register_urut = $no_urut_reg;
        }



        $new->id_m_jadwal = $request->id_m_jadwal;
        $new->nm_m_pendaftar = Str::upper($request->nm_m_pendaftar);
        $new->nik_m_pendaftar = $request->nik_m_pendaftar;
        $new->id_m_provinsi = $request->id_m_provinsi;
        $new->id_m_kota = $request->id_m_kota;
        $new->alamat_m_pendaftar =  $request->alamat_m_pendaftar;
        $new->jk_m_pendaftar =  $request->jk_m_pendaftar;
        $new->id_m_pendidikan =  $request->id_m_pendidikan;
        $new->wa_m_pendaftar =  $request->wa_m_pendaftar;
        $new->nm_jurusan =  $request->nm_jurusan;
        $new->tempat_lahir_m_pendaftar =  $request->tempat_lahir_m_pendaftar;
        $new->tgl_lahir_m_pendaftar = $request->tgl_lahir_m_pendaftar;
        $new->email_m_pendaftar =  $request->email_m_pendaftar;
        $new->quest_pernah_blk =  $request->quest_pernah_blk;
        $new->quest_masih_bekerja =  $request->quest_masih_bekerja;
        $new->quest_minat_setelah_kursus =  $request->quest_minat_setelah_kursus;
        $new->kategori_m_pendaftar =  ($request->cat) ? $request->cat : 'PBK';
        $new->cbt_grup_id =  $request->cbt_grup_id;
        $new->is_disabilitas =  $request->is_disabilitas;
        $new->id_m_disabilitas =  ($request->is_disabilitas == 'YA') ? $request->id_m_disabilitas : null;

        /** 
         * create or update cbt_user
         */

        $expl = explode('-',$new->no_register);
        $tahun_daftar = $expl[1];
        $tanggal_daftar = $expl[3];
        $tipe_daftar = $expl[4];
        $urut_daftar = $expl[5];

        $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;

        try{
            $new->save();
            $new_cbt = Cbt_user::where('user_name', $username)->first();
            
            if($new_cbt){
                // $new_cbt->user_id = Cbt_user::MaxId();
                if(Carbon::parse($new->created_at)->format('Y-m-d') != $request->tgl_daftar){
                    $new_cbt->user_name = $noreg_cbt;
                }
                $new_cbt->user_grup_id = $request->cbt_grup_id;
                $new_cbt->user_email = $request->email_m_pendaftar;
                $new_cbt->user_firstname = $request->nm_m_pendaftar;
                $new_cbt->save();
            }

            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pendaftar.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }


    public function view()
    {
        abort_if(!request()->filled('id_m_pendaftar') or !is_numeric(request('id_m_pendaftar')), 404);

        $old = M_pendaftar::where([
            'id_m_pendaftar' => request('id_m_pendaftar')
        ])->with('jadwal')->firstOrFail();

        $expl = explode('-',$old->no_register);
        $tahun_daftar = $expl[1];
        $tanggal_daftar = $expl[3];
        $tipe_daftar = $expl[4];
        $urut_daftar = $expl[5];

        $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
        $cbt_user = Cbt_user::select('user_name', 'user_password')->where('user_name', $username)->first();


        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Institusional',
            'child_menu_active'   => 'Data Pendaftaran',
            'old' => $old,
            'username_cbt' => ($cbt_user) ? $cbt_user->user_name : null,
            'password_cbt' => ($cbt_user) ? $cbt_user->user_password : null,
        ];

        return view('back.m_pendaftar.view')->with($data);
    }

    
    public function delete(Request $request)
    {
        if(!$request->filled('id_m_pendaftar')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_pendaftar::where([
            'id_m_pendaftar' => $request->id_m_pendaftar
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        /** 
         * find relationship CBT_USER
         */
        $expl = explode('-',$find->no_register);
        $tahun_daftar = $expl[1];
        $tanggal_daftar = $expl[3];
        $tipe_daftar = $expl[4];
        $urut_daftar = $expl[5];

        $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
        $cbt_user = Cbt_user::where('user_name', $username)
                    ->with('cbt_tes_user')
                    ->with('cbt_tes_soal', function($q){
                        $q->with('cbt_tes_soal_jawaban');
                    })->first();

        DB::beginTransaction();

        try{

            if($cbt_user){
                
                ($cbt_user->cbt_tes_user) ? $cbt_user->cbt_tes_user->delete() : true;

                if(count($cbt_user->cbt_tes_soal) > 0){
                    foreach($cbt_user->cbt_tes_soal as $soal){
                        $soal->cbt_tes_soal_jawaban()->delete();
                    }
                    $cbt_user->cbt_tes_soal()->delete();
                }
                $cbt_user->delete();
            }

            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pendaftar.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan ?? null;
        $id_m_kejuruan = $request->id_m_kejuruan ?? null;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan ?? null;
        $id_m_jadwal = $request->id_m_jadwal ?? null;
        $tahun = $request->tahun ?? null;
        
        \DB::enablequerylog();
        $table = M_pendaftar::view_pendaftar_belum_test($id_m_kategori_kejuruan, $id_m_kejuruan, $id_m_program_pelatihan, $id_m_jadwal, $tahun);
        // dd(\DB::getquerylog());
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {
            
            /** 
             * check foreign key in cbt_user
             */
            
            $expl = explode('-',$value->no_register);
            $tahun_daftar = $expl[1];
            $tanggal_daftar = $expl[3];
            $tipe_daftar = $expl[4];
            $urut_daftar = $expl[5];

            $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
            $cbt_user = Cbt_user::select('user_name', 'user_password')->where('user_firstname',$value->nm_m_pendaftar)->where('user_name', $username)->first();

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_pendaftar;
            $datas[$key][] = $value->nik_m_pendaftar;
            $datas[$key][] = $value->wa_m_pendaftar;
            $datas[$key][] = ($value->jadwal) ? $value->jadwal->nm_m_jadwal : null;
            $datas[$key][] = ($value->pendidikan) ? $value->pendidikan->nm_m_pendidikan : null;
            $datas[$key][] = $value->nm_jurusan;
            $datas[$key][] = ($cbt_user) ? $cbt_user->user_name : '<span class="text-danger">'.$value->no_register.'</span>';
            $datas[$key][] = ($value->created_at) ? Carbon::parse($value->created_at)->format('d-m-Y H:i:s') : null;
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_pendaftar.view',['id_m_pendaftar' => $value->id_m_pendaftar]).'">tampilkan</a>
                                        <a class="dropdown-item" href="'.route('admin.m_pendaftar.edit',['id_m_pendaftar' => $value->id_m_pendaftar]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_pendaftar="'.$value->id_m_pendaftar.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }

    public function export_xls()
    {
        return Excel::download(new PendaftarInsExport, 'export_pendaftar_inst.xlsx');
    }
}
