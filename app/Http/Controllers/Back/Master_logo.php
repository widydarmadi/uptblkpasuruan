<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_logo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_logo extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Logo Website',
            'page_title' => 'Logo Website',
            'parent_menu_active' => 'Pengaturan',
            'child_menu_active'   => 'Logo Website',
        ];

        return view('back.m_logo.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Logo Website',
            'page_title' => 'Logo Website',
            'parent_menu_active' => 'Pengaturan',
            'child_menu_active'   => 'Logo Website',
        ];

    	return view('back.m_logo.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'photo.image' => 'harus berupa gambar',
            'photo.required' => 'harap diisi',
            'photo.dimensions' => 'dimensi gambar tidak sesuai dengan ketentuan',
        ];

        $validator = Validator::make($request->all(), [
            'photo' => ['image', 'required', 'dimensions:width=600,height=200'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'photo' => $errors->first('photo'),
                ]
            ]);
        }


        DB::beginTransaction();

        
        $object = new M_logo;
        $object->id_m_logo = M_logo::MaxId();
        $object->aktif_m_logo = $request->aktif_m_logo;
        try{

            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/logo';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }
            $object->save();

            if($request->aktif_m_logo == '1'){
                $block_another_logo = M_logo::where('id_m_logo', '!=', M_logo::MaxId())->update([
                    'aktif_m_logo' => '0'
                ]);
            }else{
                $count_active = M_logo::where('aktif_m_logo', '1')->count();
                if($count_active == 0){
                    $activate_latest_logo = M_logo::OrderByDesc('id_m_logo')->first();
                    $activate_latest_logo->aktif_m_logo = '1';
                    $activate_latest_logo->save();
                }
            }

            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_logo.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_logo') or !is_numeric(request('id_m_logo')), 404);

        $old = m_logo::where([
            'id_m_logo' => request('id_m_logo')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Logo Website',
            'page_title' => 'Logo Website',
            'parent_menu_active' => 'Pengaturan',
            'child_menu_active'   => 'Logo Website',
            'old' => $old,
        ];

        return view('back.m_logo.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'photo.image' => 'harus berupa gambar',
            'photo.dimensions' => 'dimensi gambar tidak sesuai dengan ketentuan',
        ];

        $validator = Validator::make($request->all(), [
            'photo' => ['image', 'nullable', 'dimensions:width=600,height=200'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'photo' => $errors->first('photo'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = m_logo::where([
            'id_m_logo' => $request->id_m_logo,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_logo' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif_m_logo = $request->aktif_m_logo;
        try{
            if($request->file('photo')){
                \Storage::delete('public/'.$update->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/logo';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $update->photo = $path1;
            }

            $update->save();

            if($request->aktif_m_logo == '1'){
                $block_another_logo = M_logo::where('id_m_logo', '!=' , $request->id_m_logo)->update([
                    'aktif_m_logo' => '0'
                ]);
            }else{
                $count_active = M_logo::where('aktif_m_logo', '1')->count();
                if($count_active == 0){
                    $activate_latest_logo = M_logo::OrderByDesc('id_m_logo')->first();
                    $activate_latest_logo->aktif_m_logo = '1';
                    $activate_latest_logo->save();
                }
            }


            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_logo.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_logo')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = m_logo::where([
            'id_m_logo' => $request->id_m_logo
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->photo);
            $find->delete();

            $count_active = M_logo::where('aktif_m_logo', '1')->count();
            if($count_active == 0){
                $activate_latest_logo = M_logo::where('id_m_logo', M_logo::MaxOnly())->update([
                    'aktif_m_logo' => '1'
                ]);
            }
            
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_logo.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_logo::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = isset($value->photo) ? '<img src="'.asset('storage'.'/'.$value->photo).'" width="100px" />' : '-';
            $datas[$key][] = ($value->aktif_m_logo=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_logo.edit',['id_m_logo' => $value->id_m_logo]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_logo="'.$value->id_m_logo.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
