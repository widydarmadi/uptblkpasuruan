<?php

namespace App\Http\Controllers\Back;

use App\Exports\WawancaraExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Cbt_tes_soal;
use App\Models\Cbt_tes_user;
use App\Models\Cbt_user;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use Illuminate\Support\Str;
use App\Models\M_pendaftar;
use App\Models\M_pendidikan;
use App\Models\M_program_pelatihan;
use App\Models\M_soal_wawancara;
use App\Models\T_kelas_pelatihan;
use App\Models\T_paket_soal_wawancara;
use App\Models\T_paket_soal_wawancara_det;
use App\Models\T_penilaian_wawancara;
use App\Models\T_peserta;
use App\Models\T_seleksi;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Storage;

class Transaksi_wawancara extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
                                    ->whereHas('kejuruan', function($q){
                                        $q->where('aktif_m_kejuruan', '1');
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('aktif', '1')
                                              ->whereHas('jadwal');
                                        });
                                    })->get();
        $data = [
            'head_title' => 'Wawancara',
            'page_title' => 'Wawancara',
            'parent_menu_active'  => 'Pelatihan Institusional',
            'child_menu_active'   => 'Wawancara',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
        ];

        return view('back.t_wawancara.index')->with($data);
    }

    public function history()
    {
        $old = M_pendaftar::where('id_m_pendaftar', request()->get('id_m_pendaftar'))
                ->with('seleksi:id_m_pendaftar,total_nilai_cbt,total_nilai_wawancara,total_nilai_keseluruhan,status,note')
                ->with('penilaian_wawancara', function($q){
                    $q->select('id_m_pendaftar','id_m_soal','nilai');
                    $q->with('soal_wawancara:id_m_soal_wawancara,soal_wawancara');
                })
                ->with('pendidikan')
                ->firstOrFail();

        if($old){
            $expl = explode('-',$old->no_register);
            $tahun_daftar = $expl[1];
            $tanggal_daftar = $expl[3];
            $tipe_daftar = $expl[4];
            $urut_daftar = $expl[5];

            $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
            $cbt_user = Cbt_user::select('user_id','user_name', 'user_password')->where('user_name', $username)
                        ->with('cbt_tes_user', function($q){
                            $q->with('cbt_tes_soal', function($q){
                                $q->select('tessoal_tesuser_id','tessoal_soal_id','tessoal_tesuser_id','tessoal_nilai');
                                $q->with('cbt_soal:soal_id,soal_detail');
                            });
                        })->first();
        }
        
        $data = [
            'head_title' => 'Wawancara',
            'page_title' => 'Wawancara',
            'parent_menu_active'  => 'Pelatihan Institusional',
            'child_menu_active'   => 'Wawancara',
            'old' => $old,
            'cbt_user' => $cbt_user,
        ];

        return view('back.t_wawancara.history')->with($data);
    }

    public function load_kejuruan_available(Request $request)
    {
        $load = M_kejuruan::where('id_m_kategori_kejuruan', $request->id)
                ->where('aktif_m_kejuruan', '1')
                ->whereHas('program_pelatihan', function($q){
                    $q->where('aktif', '1')
                    ->where('id_m_tipe_pelatihan', 1)
                    ->whereHas('jadwal', function($q){
                        // $q->where('status', 'OPEN');
                    });
                })->get();
        $html = '';
        $html .= "<option value=''>Pilih Sub Kejuruan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_kejuruan."'>".$item->nm_m_kejuruan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    
    public function load_program_available(Request $request)
    {
        $load = M_program_pelatihan::select('id_m_program_pelatihan', 'nm_m_program_pelatihan')->where('id_m_kejuruan', $request->id)
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 1)
                ->whereHas('jadwal', function($q){
                    // $q->where('status', 'OPEN');
                })->get();

        $html = '';
        $html .= "<option value=''>Pilih Program Pelatihan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_program_pelatihan."'>".$item->nm_m_program_pelatihan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    public function load_jadwal_available(Request $request)
    {
        $load = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $request->id)
                // ->where('status', 'OPEN')
                ->get();

        $html = '';
        $html .= "<option value=''>Pilih Jadwal / Gelombang</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_jadwal."'>".$item->nm_m_jadwal."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }



    public function edit()
    {
        abort_if(!request()->filled('id_m_pendaftar') or !is_numeric(request('id_m_pendaftar')), 404);
        abort_if(!request()->filled('paket_soal'), 404);

        $old = M_pendaftar::select('id_m_pendaftar', 'cbt_grup_id', 'email_m_pendaftar', 'nik_m_pendaftar','nm_m_pendaftar', 'alamat_m_pendaftar',
                                    'wa_m_pendaftar','nm_jurusan', 'no_register','id_m_pendidikan', 'is_disabilitas', 'id_m_disabilitas')
        ->where([
            'id_m_pendaftar' => request('id_m_pendaftar')
        ])
        ->with('pendidikan', 'disabilitas')
        ->firstOrFail();
        // $list_soal = M_soal_wawancara::where('status','1')->orderBy('id_m_soal_wawancara')->get();
        $list_soal = T_paket_soal_wawancara_det::with('soal')->where('id_t_paket_soal_wawancara', request()->get('paket_soal'))->orderBy('id_t_paket_soal_wawancara')->get();
        $data = [
            'head_title' => 'Wawancara',
            'page_title' => 'Wawancara',
            'parent_menu_active'  => 'Pelatihan Institusional',
            'child_menu_active'   => 'Wawancara',
            'old' => $old,
            'list_soal' => $list_soal,
        ];

        return view('back.t_wawancara.edit')->with($data);
    }
    
    public function edit_nilai()
    {
        abort_if(!request()->filled('id_m_pendaftar') or !is_numeric(request('id_m_pendaftar')), 404);
        
        $paket = M_pendaftar::select('id_t_paket_soal_wawancara')
                    ->where([
                        'id_m_pendaftar' => request('id_m_pendaftar')
                    ])->firstOrFail();
        abort_if($paket->id_t_paket_soal_wawancara == null, 404);

        $paket_soal = $paket->id_t_paket_soal_wawancara;

        $old = M_pendaftar::select('id_m_pendaftar', 'id_t_paket_soal_wawancara', 'cbt_grup_id', 'email_m_pendaftar', 
                                    'nik_m_pendaftar','nm_m_pendaftar','wa_m_pendaftar','nm_jurusan', 'no_register', 
                                    'id_m_pendidikan', 'is_disabilitas', 'id_m_disabilitas', 'alamat_m_pendaftar')
        ->where([
            'id_m_pendaftar' => request('id_m_pendaftar')
        ])
        ->with('pendidikan')
        ->with('seleksi')
        ->with('disabilitas')
        ->with('paket_soal_wawancara:id_t_paket_soal_wawancara,nm_paket_soal_wawancara')
        ->with('penilaian_wawancara', function($q) use ($paket_soal) {
            $q->select('id_m_pendaftar','id_t_penilaian_wawancara','nilai','id_m_soal');
            $q->with('paket_soal_wawancara_det', function($q) use ($paket_soal) {
                $q->where('id_t_paket_soal_wawancara', $paket_soal);
            });
            // $q->with('soal_wawancara',function($q){
            //     $q->with('paket_soal_wawancara:id_t_paket_soal_wawancara,nm_paket_soal_wawancara');
            // });
            
        })
        ->firstOrFail();

        $data = [
            'head_title' => 'Wawancara',
            'page_title' => 'Wawancara',
            'parent_menu_active'  => 'Pelatihan Institusional',
            'child_menu_active'   => 'Wawancara',
            'old' => $old,
        ];

        return view('back.t_wawancara.edit_nilai')->with($data);
    }

    public function update(Request $request)
    {
        FacadesDB::beginTransaction();
        
        try{

            //deleting first then re-insert T_penilaian_wawancara
            $delet = T_penilaian_wawancara::where([
                        'id_m_pendaftar' => $request->id_m_pendaftar
                    ])->delete();
                    
            for($i = 0; $i < count($request->id_m_soal); $i++){
                $object = new T_penilaian_wawancara;
                $object->id_t_penilaian_wawancara = T_penilaian_wawancara::maxId();
                $object->id_m_pendaftar = $request->id_m_pendaftar;
                $object->id_m_soal = $request->id_m_soal[$i];
                $object->nilai = $request->score[$i];
                $object->save();
            }

            /**
             * $cbt_score
             */

            $m_daft = M_pendaftar::select('no_register')->where('id_m_pendaftar', $request->id_m_pendaftar)->first();
            if($m_daft){
                $expl = explode('-',$m_daft->no_register);
                $tahun_daftar = $expl[1];
                $tanggal_daftar = $expl[3];
                $tipe_daftar = $expl[4];
                $urut_daftar = $expl[5];
                $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
            }else{
                $username = null;
            }
            

            $lookup_cbt_user = Cbt_user::select('user_id','user_name', 'user_password')->where('user_name', $username)
                        ->with('cbt_tes_user', function($q){
                            $q->withCount('cbt_tes_soal');
                            $q->with('cbt_tes_soal:tessoal_tesuser_id,tessoal_tesuser_id,tessoal_nilai');
                        })->first();
            // dd($cbt_user);
    		if($lookup_cbt_user and $lookup_cbt_user->cbt_tes_user and $lookup_cbt_user->cbt_tes_user->cbt_tes_soal){
                $sum_of_cbt = $lookup_cbt_user->cbt_tes_user->cbt_tes_soal->sum('tessoal_nilai');
            }else{
                $sum_of_cbt = 0;
            }

            //di get dulu dari tabel cbt_user untuk mendapatkan user_id lokal cbt, lalu lookup ke cbt_tes_soal
            // $lookup_cbt_user = Cbt_user::select('user_id')->where('user_email', $request->email_m_pendaftar)
            //                     ->where('user_grup_id', $request->cbt_grup_id)
            //                     // ->with('cbt_tes_soal')
            //                     ->first();

            // if($lookup_cbt_user){
            //     $total_nilai_cbt = Cbt_tes_user::select('tesuser_id')->where('tesuser_user_id', $lookup_cbt_user->user_id)
            //                         ->with('cbt_tes_soal')
            //                         ->withCount('cbt_tes_soal')
            //                         ->first();
            // }else{
            //     $total_nilai_cbt = null;
            // }
                                
            // $total_nilai_cbt = Cbt_tes_soal::where('tessoal_tesuser_id', $lookup_cbt_user->user_id)->count();
            // $jml_soal_cbt = ($lookup_cbt_user and $lookup_cbt_user->cbt_tes_user) ? $lookup_cbt_user->cbt_tes_user->cbt_tes_soal_count : 1;
            $total_nilai_wawancara = T_penilaian_wawancara::where('id_m_pendaftar', $request->id_m_pendaftar)->sum('nilai');

            $akumulasi_wawancara = ($total_nilai_wawancara) ? ($total_nilai_wawancara / count($request->id_m_soal)) : 0;
            $akumulasi_cbt = $sum_of_cbt;
            $all_nilai = ($akumulasi_wawancara * 0.6) + ($akumulasi_cbt  * 0.4);
            
            //create new seleksi
            $t_seleksi = new T_seleksi;
            $t_seleksi->id_t_seleksi = T_seleksi::maxId();
            $t_seleksi->id_m_pendaftar = $request->id_m_pendaftar;
            $t_seleksi->total_nilai_cbt = number_format($akumulasi_cbt, 2, '.','');
            $t_seleksi->total_nilai_wawancara = number_format($akumulasi_wawancara, 2, '.','');
            $t_seleksi->total_nilai_keseluruhan = number_format($all_nilai, 2, '.','');
            $t_seleksi->status = 'MENUNGGU';
            $t_seleksi->note = $request->note;
            $t_seleksi->save();

            //update m_pendaftar
            $pendaftar = M_pendaftar::where('id_m_pendaftar', $request->id_m_pendaftar)
                        ->update([
                            'id_t_paket_soal_wawancara' => $request->paket_soal
                        ]);

            FacadesDB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.t_wawancara.index'),
            ]);
        }catch(\Exception $e){
            FacadesDB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function update_nilai(Request $request)
    {
        FacadesDB::beginTransaction();
        
        try{

            //deleting first then re-insert T_penilaian_wawancara
            T_penilaian_wawancara::where([
                'id_m_pendaftar' => $request->id_m_pendaftar
            ])->delete();

            if(is_array($request->id_m_soal)){
                for($i = 0; $i < count($request->id_m_soal); $i++){
                    $object = new T_penilaian_wawancara;
                    $object->id_t_penilaian_wawancara = T_penilaian_wawancara::maxId();
                    $object->id_m_pendaftar = $request->id_m_pendaftar;
                    $object->id_m_soal = $request->id_m_soal[$i];
                    $object->nilai = $request->score[$i];
                    $object->save();
                }
            }

            /**
             * $cbt_score
             */

            $m_daft = M_pendaftar::select('no_register')->where('id_m_pendaftar', $request->id_m_pendaftar)->first();
            if($m_daft){
                $expl = explode('-',$m_daft->no_register);
                $tahun_daftar = $expl[1];
                $tanggal_daftar = $expl[3];
                $tipe_daftar = $expl[4];
                $urut_daftar = $expl[5];
                $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
            }else{
                $username = null;
            }
            

            $lookup_cbt_user = Cbt_user::select('user_id','user_name', 'user_password')->where('user_name', $username)
                        ->with('cbt_tes_user', function($q){
                            $q->withCount('cbt_tes_soal');
                            $q->with('cbt_tes_soal:tessoal_tesuser_id,tessoal_tesuser_id,tessoal_nilai');
                        })->first();
            
    		if($lookup_cbt_user and $lookup_cbt_user->cbt_tes_user and $lookup_cbt_user->cbt_tes_user->cbt_tes_soal){
                $sum_of_cbt = $lookup_cbt_user->cbt_tes_user->cbt_tes_soal->sum('tessoal_nilai');
            }else{
                $sum_of_cbt = 0;
            }

            $total_nilai_wawancara = T_penilaian_wawancara::where('id_m_pendaftar', $request->id_m_pendaftar)->sum('nilai');

            $akumulasi_wawancara = ($total_nilai_wawancara) ? ($total_nilai_wawancara / count($request->id_m_soal)) : 0;
            $akumulasi_cbt = $sum_of_cbt;
            $all_nilai = ($akumulasi_wawancara * 0.6) + ($akumulasi_cbt  * 0.4);
            
            //update seleksi
            $t_seleksi = T_seleksi::where(['id_t_seleksi' => $request->id_t_seleksi, 'id_m_pendaftar' => $request->id_m_pendaftar])->first();
            if($t_seleksi){
                $t_seleksi->total_nilai_cbt = number_format($akumulasi_cbt, 2, '.','');
                $t_seleksi->total_nilai_wawancara = number_format($akumulasi_wawancara, 2, '.','');
                $t_seleksi->total_nilai_keseluruhan = number_format($all_nilai, 2, '.','');
                $t_seleksi->status = 'MENUNGGU';
                $t_seleksi->note = $request->note;
                $t_seleksi->save();
            }

            FacadesDB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.t_wawancara.index'),
            ]);
        }catch(\Exception $e){
            FacadesDB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_t_wawancara')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_pendaftar::where([
            'id_t_wawancara' => $request->id_t_wawancara
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->photo);
            // Storage::delete('public/'.$find->foto_kanan);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.t_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan ?? null;
        $id_m_kejuruan = $request->id_m_kejuruan ?? null;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan ?? null;
        $id_m_jadwal = $request->id_m_jadwal ?? null;
        
        DB::enableQueryLog();

        $table = M_pendaftar::view_pendaftar($id_m_kategori_kejuruan, $id_m_kejuruan, $id_m_program_pelatihan, $id_m_jadwal);
    	$datas = [];
    	$i = 1;
        // $sorted = $table->sortByDesc(function($q){
        //     if($q->seleksi){
        //         return $q->seleksi->total_nilai_keseluruhan;
        //     }
        // })->all();
        // dd($sorted);
    	foreach ($table as $key => $value) {
            $cek_seleksi = T_seleksi::select('id_t_seleksi', 'total_nilai_cbt', 'total_nilai_wawancara', 'total_nilai_keseluruhan', 'status')->where('id_m_pendaftar', $value->id_m_pendaftar);
            if(
                ($cek_seleksi->count() == 0) OR
                ($cek_seleksi->first()) and $cek_seleksi->first()->status != 'OUT'
            ){
                $button_wawancara = ($cek_seleksi->count() > 0) ? 
                                    '
                                    <a class="btn btn-sm btn-info" href="javascript:void(0)"><i class="fas fa-check"></i> selesai</a>
                                    <a class="btn btn-sm bg-yellow" href="'.route('admin.t_wawancara.edit_nilai', ['id_m_pendaftar' => $value->id_m_pendaftar]).'"><i class="fas fa-edit"></i> ubah</a>
                                    <a class="btn btn-sm bg-pink" href="'.route('admin.t_wawancara.history', ['id_m_pendaftar' => $value->id_m_pendaftar]).'"><i class="fas fa-history"></i> histori nilai</a>
                                    <a class="btn btn-sm btn-danger undurdiri" 
                                    data-id_m_pendaftar="'.$value->id_m_pendaftar.'" 
                                    href="javascript:void(0);"
                                    ><i class="fas fa-window-close"></i> undur diri</a>
                                    ' :
                                    '<a class="btn btn-sm btn-success start_wawancara" 
                                    data-id_m_pendaftar="'.$value->id_m_pendaftar.'" 
                                    href="javascript:void(0);"
                                    ><i class="fas fa-comment"></i> Wawancara</a>
                                    
                                    <a class="btn btn-sm btn-danger undurdiri" 
                                    data-id_m_pendaftar="'.$value->id_m_pendaftar.'" 
                                    href="javascript:void(0);"
                                    ><i class="fas fa-window-close"></i> undur diri</a>
                                    ';

            }else if(
                ($cek_seleksi->first()) and $cek_seleksi->first()->status == 'OUT'
            ){
                $button_wawancara = '<span class="badge badge-danger">keluar</span>';
            }

            /**
             * $cbt_score
             */

            $expl = explode('-',$value->no_register);
            $tahun_daftar = $expl[1];
            $tanggal_daftar = $expl[3];
            $tipe_daftar = $expl[4];
            $urut_daftar = $expl[5];

            $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
            $cbt_user = Cbt_user::select('user_id','user_name', 'user_password')->where('user_name', $username)
                        ->with('cbt_tes_user', function($q){
                            $q->with('cbt_tes_soal:tessoal_tesuser_id,tessoal_tesuser_id,tessoal_nilai');
                        })->first();
    		if($cbt_user and $cbt_user->cbt_tes_user and $cbt_user->cbt_tes_user->cbt_tes_soal){
                $cbt_score = $cbt_user->cbt_tes_user->cbt_tes_soal->sum('tessoal_nilai');
            }else{
                $cbt_score = '<span class="text-danger">belum ada</span>';
            }


            $datas[$key][] = $i++;
            $datas[$key][] = Str::upper($value->nm_m_pendaftar);
            $datas[$key][] = $value->nik_m_pendaftar;
            // $datas[$key][] = $value->wa_m_pendaftar;
            $datas[$key][] = $value->jadwal->nm_m_jadwal ?? '-';
            $datas[$key][] = ($cbt_user) ? $cbt_user->user_name : '-';
            // $datas[$key][] = ($value->jadwal and $value->jadwal->gelombang) ? $value->jadwal->gelombang->nm_m_gelombang.' '.$value->jadwal->tahun : '-';
            $datas[$key][] = $cbt_score;
            $datas[$key][] = ($cek_seleksi->first()) ? number_format($cek_seleksi->first()->total_nilai_wawancara,2,',','.') : '<span class="text-danger">belum ada</span>';
            $datas[$key][] = ($cek_seleksi->first()) ? number_format($cek_seleksi->first()->total_nilai_keseluruhan,2,',','.') : '<span class="text-danger">belum ada</span>';
            $datas[$key][] = ($cek_seleksi->first()) ? $cek_seleksi->first()->status : '';
            $datas[$key][] = $button_wawancara;
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }

    public function perbarui_peringkat($id = null)
    {
        $group_jadwal = M_jadwal::select('id_m_jadwal', 'kuota')->where('tahun', date('Y'))
                                ->whereYear('tgl_mulai', date('Y'))->get();
        
        foreach($group_jadwal as $jadwal){
            $id_m_jadwal = $jadwal->id_m_jadwal ?? null;

            // set tidak lulus bagi all data seleksi yang lulus & tidak lulus, untuk mengkalkulasi ulang nilai
            // jika mengundurkan diri maka dibiarkan saja, tidak diupdate tidak lulus
            $reset_kelulusan = T_seleksi::select('id_t_seleksi')
                                ->whereIn('status', ['MENUNGGU','LULUS'])
                                ->whereYear('created_at', date('Y'))
                                ->whereYear('updated_at', date('Y'))
                                ->whereHas('pendaftar', function($q) use ($id_m_jadwal){
                                    $q->where('id_m_jadwal', $id_m_jadwal);
                                })->update([
                                    'status' => 'TIDAK LULUS'
                                ]);
            
            // meng-update t_seleksi menjadi out apabila ada peserta mengundurkan diri
            $updating = (isset($id)) ? T_seleksi::where('id_m_pendaftar', $id)->update(['status' => 'OUT']) : true;
            // menghapus data dari t_peserta apabila peserta lulus yang terlanjur masuk pemetaan kelas mengundurkan diri
            $updating2 = (isset($id)) ? T_peserta::select('id_t_peserta')->whereHas('seleksi', function($q) use ($id){
                $q->where('id_m_pendaftar', $id);
            })->delete() : true;
            //hapus kelas pelatihan yang tidak ada pesertanya supaya relasinya normal
            $del_kelas_kosong = T_kelas_pelatihan::where('id_m_jadwal', $id_m_jadwal)->withCount('peserta')->first();
            $hapus_kelas = ($del_kelas_kosong) ? ($del_kelas_kosong->peserta_count == 0 ? $del_kelas_kosong->delete() : true)
                            : true;            
            // set lulus bagi peserta top xx, apabila kuota belum di set, maka default 16
            $kuota = $jadwal->kuota ?? 16;

            $peserta_lulus = T_seleksi::select('id_t_seleksi', 'id_m_pendaftar')
                            ->whereIn('status', ['MENUNGGU', 'TIDAK LULUS'])
                            ->whereYear('created_at', date('Y'))
                            ->whereYear('updated_at', date('Y'))
                            ->whereHas('pendaftar', function($q) use ($id_m_jadwal){
                                $q->where('id_m_jadwal', $id_m_jadwal);
                            })
                            ->orderByDesc('total_nilai_keseluruhan')
                            ->take($kuota)
                            ->update([
                                'status' => 'LULUS'
                            ]);
        }

        return response()->json([
            'message' => 'Ranking telah diperbaharui !',
            'status'  => true,
            'redirect'  => route('admin.t_wawancara.index'),
        ]);

    }

    public function set_soal(Request $request)
    {
      
        $menu = T_paket_soal_wawancara::where('aktif', '1')->withCount('soal')->get();
        $data = [
            'menu' => $menu,
            'id_m_pendaftar' => $request->id_m_pendaftar,
        ];

        return view('back.t_wawancara.pilih_modul')->with($data);
    }

    public function set_undur_diri(Request $request)
    {
        $id = $request->id_m_pendaftar;
        if($request->filled('id_m_pendaftar')){
            $set = T_seleksi::select('status')->where('id_m_pendaftar', $id)->first();
            if($set){
                $set->status = 'OUT';
                $set->save();
            }else{
                $t_seleksi = new T_seleksi;
                $t_seleksi->id_t_seleksi = T_seleksi::maxId();
                $t_seleksi->id_m_pendaftar = $id;
                $t_seleksi->status = 'OUT';
                $t_seleksi->save();
            }

            
        }
        $this->perbarui_peringkat($id);
        return response()->json([
            'message' => 'Sukses mengubah status keikutsertaan',
            'status'  => true,
            'redirect'  => route('admin.t_wawancara.index'),
        ]);

    }


    public function export_xls()
    {
        return Excel::download(new WawancaraExport, 'export_wawancara.xlsx');
    }
    
}
