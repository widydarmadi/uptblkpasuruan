<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_info_lowongan;
use App\Models\M_perusahaan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_info_lowongan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Info Lowongan',
            'page_title' => 'Info Lowongan',
            'parent_menu_active' => 'Lowongan Kerja',
            'child_menu_active'   => 'Info Lowongan',
        ];

        return view('back.m_info_lowongan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Info Lowongan',
            'page_title' => 'Info Lowongan',
            'parent_menu_active' => 'Lowongan Kerja',
            'child_menu_active'   => 'Info Lowongan',
            'perusahaan' => M_perusahaan::select('id_m_perusahaan','nm_m_perusahaan')->orderBy('nm_m_perusahaan')->get(),
        ];

    	return view('back.m_info_lowongan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_posisi_m_lowongan.required' => 'harap diisi',
            'deskripsi_m_lowongan.required' => 'harap diisi',
            'id_m_perusahaan.required' => 'harap diisi',
            'date_start.required' => 'harap diisi',
            'date_end.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
        ];

        $validator = Validator::make($request->all(), [
            'nm_posisi_m_lowongan' => ['required'],
            'deskripsi_m_lowongan' => ['required'],
            'id_m_perusahaan' => ['required'],
            'date_start' => ['required'],
            'date_end' => ['required'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_posisi_m_lowongan' => $errors->first('nm_posisi_m_lowongan'),
                'deskripsi_m_lowongan' => $errors->first('deskripsi_m_lowongan'),
                'id_m_perusahaan' => $errors->first('id_m_perusahaan'),
                'date_start' => $errors->first('date_start'),
                'date_end' => $errors->first('date_end'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_info_lowongan;
        $object->id_m_lowongan = M_info_lowongan::MaxId();
        $object->nm_posisi_m_lowongan = $request->nm_posisi_m_lowongan;
        $object->deskripsi_m_lowongan = $request->deskripsi_m_lowongan;
        $object->id_m_perusahaan = $request->id_m_perusahaan;
        $object->date_start = $request->date_start;
        $object->date_end = $request->date_end;
        try{

            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/lowongan';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_info_lowongan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_lowongan') or !is_numeric(request('id_m_lowongan')), 404);

        $old = M_info_lowongan::where([
            'id_m_lowongan' => request('id_m_lowongan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Info Lowongan',
            'page_title' => 'Info Lowongan',
            'parent_menu_active' => 'Lowongan Kerja',
            'child_menu_active'   => 'Info Lowongan',
            'old' => $old,
            'perusahaan' => M_perusahaan::select('id_m_perusahaan','nm_m_perusahaan')->orderBy('nm_m_perusahaan')->get(),
        ];

        return view('back.m_info_lowongan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_posisi_m_lowongan.required' => 'harap diisi',
            'deskripsi_m_lowongan.required' => 'harap diisi',
            'id_m_perusahaan.required' => 'harap diisi',
            'date_start.required' => 'harap diisi',
            'date_end.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
        ];

        $validator = Validator::make($request->all(), [
            'nm_posisi_m_lowongan' => ['required'],
            'deskripsi_m_lowongan' => ['required'],
            'id_m_perusahaan' => ['required'],
            'date_start' => ['required'],
            'date_end' => ['required'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_posisi_m_lowongan' => $errors->first('nm_posisi_m_lowongan'),
                'deskripsi_m_lowongan' => $errors->first('deskripsi_m_lowongan'),
                'id_m_perusahaan' => $errors->first('id_m_perusahaan'),
                'date_start' => $errors->first('date_start'),
                'date_end' => $errors->first('date_end'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_info_lowongan::where('id_m_lowongan', $request->id_m_lowongan)->first();
        $object->nm_posisi_m_lowongan = $request->nm_posisi_m_lowongan;
        $object->deskripsi_m_lowongan = $request->deskripsi_m_lowongan;
        $object->id_m_perusahaan = $request->id_m_perusahaan;
        $object->date_start = $request->date_start;
        $object->date_end = $request->date_end;

        try{

            if($request->file('photo')){
                \Storage::delete('public/'.$object->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/lowongan';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }
           
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_info_lowongan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_lowongan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_info_lowongan::where([
            'id_m_lowongan' => $request->id_m_lowongan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->photo);
            Storage::delete('public/'.$find->photo2);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_info_lowongan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_info_lowongan::with('perusahaan')->orderByDesc('id_m_lowongan')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->perusahaan->nm_m_perusahaan;
            $datas[$key][] = $value->nm_posisi_m_lowongan;
            $datas[$key][] = \Carbon\Carbon::parse($value->date_start)->format('d-m-Y');
            $datas[$key][] = \Carbon\Carbon::parse($value->date_end)->format('d-m-Y');
            $datas[$key][] = ($value->updated_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_info_lowongan.edit',['id_m_lowongan' => $value->id_m_lowongan]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_lowongan="'.$value->id_m_lowongan.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
