<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_kejuruan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_kategori_kejuruan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Kategori Kejuruan',
            'page_title' => 'Kategori Kejuruan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Kategori Kejuruan',
        ];

        return view('back.m_kategori_kejuruan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Kategori Kejuruan',
            'page_title' => 'Kategori Kejuruan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Kategori Kejuruan',
        ];

    	return view('back.m_kategori_kejuruan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_kategori_kejuruan.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
            'photo.required' => 'harap diisi',
            'urut_tampil.required' => 'harap diisi',
            'desc_m_kategori_kejuruan.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_kategori_kejuruan' => ['required'],
            'photo' => ['image', 'required'],
            'urut_tampil' => ['required'],
            'desc_m_kategori_kejuruan' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kategori_kejuruan' => $errors->first('nm_m_kategori_kejuruan'),
                    'photo' => $errors->first('photo'),
                    'urut_tampil' => $errors->first('urut_tampil'),
                    'desc_m_kategori_kejuruan' => $errors->first('desc_m_kategori_kejuruan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_kategori_kejuruan;
        $object->id_m_kategori_kejuruan = M_kategori_kejuruan::MaxId();
        $object->aktif = $request->aktif;
        $object->nm_m_kategori_kejuruan = $request->nm_m_kategori_kejuruan;
        $object->urut_tampil = $request->urut_tampil;
        $object->desc_m_kategori_kejuruan = $request->desc_m_kategori_kejuruan;
        try{

            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/kejuruan';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_kategori_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_kategori_kejuruan') or !is_numeric(request('id_m_kategori_kejuruan')), 404);

        $old = m_kategori_kejuruan::where([
            'id_m_kategori_kejuruan' => request('id_m_kategori_kejuruan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Kategori Kejuruan',
            'page_title' => 'Kategori Kejuruan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Kategori Kejuruan',
            'old' => $old,
        ];

        return view('back.m_kategori_kejuruan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_kategori_kejuruan.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
            'urut_tampil.required' => 'harap diisi',
            'desc_m_kategori_kejuruan.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_kategori_kejuruan' => ['required'],
            'photo' => ['image', 'nullable'],
            'urut_tampil' => ['required'],
            'desc_m_kategori_kejuruan' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kategori_kejuruan' => $errors->first('nm_m_kategori_kejuruan'),
                    'photo' => $errors->first('photo'),
                    'urut_tampil' => $errors->first('urut_tampil'),
                    'desc_m_kategori_kejuruan' => $errors->first('desc_m_kategori_kejuruan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = m_kategori_kejuruan::where([
            'id_m_kategori_kejuruan' => $request->id_m_kategori_kejuruan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kategori_kejuruan' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif = $request->aktif;
        $update->nm_m_kategori_kejuruan = $request->nm_m_kategori_kejuruan;
        $update->urut_tampil = $request->urut_tampil;
        $update->desc_m_kategori_kejuruan = $request->desc_m_kategori_kejuruan;
        try{
            if($request->file('photo')){
                \Storage::delete('public/'.$update->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/kejuruan';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $update->photo = $path1;
            }
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_kategori_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_kategori_kejuruan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = m_kategori_kejuruan::where([
            'id_m_kategori_kejuruan' => $request->id_m_kategori_kejuruan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->photo);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_kategori_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_kategori_kejuruan::orderBy('urut_tampil')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_kategori_kejuruan;
            $datas[$key][] = $value->urut_tampil;
            $datas[$key][] = ($value->aktif=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_kategori_kejuruan.edit',['id_m_kategori_kejuruan' => $value->id_m_kategori_kejuruan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_kategori_kejuruan="'.$value->id_m_kategori_kejuruan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
