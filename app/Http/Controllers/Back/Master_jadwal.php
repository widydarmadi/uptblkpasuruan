<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\Cbt_user_grup;
use App\Models\M_gelombang;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_program_kegiatan;
use App\Models\M_program_pelatihan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_jadwal extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Jadwal Pelatihan',
            'page_title' => 'Jadwal Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Jadwal Pelatihan',
        ];

        return view('back.m_jadwal.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Jadwal Pelatihan',
            'page_title' => 'Jadwal Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Jadwal Pelatihan',
            'prog'   => M_program_pelatihan::where('aktif', '1')->with('tipe_pelatihan')->orderBy('nm_m_program_pelatihan')->get(),
            'gelombang'   => M_gelombang::where('aktif_m_gelombang', '1')->orderBy('nm_m_gelombang')->get(),
        ];

    	return view('back.m_jadwal.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'id_m_program_pelatihan.required' => 'pilih salah satu',
            'id_m_gelombang.required' => 'pilih salah satu',
            'nm_m_jadwal.required' => 'pilih salah satu',
            'text_gelombang.required' => 'harap diisi',
            'text_tahun.required' => 'harap diisi',
            'tahun.required' => 'harap diisi',
            'tahun.numeric' => 'diisi angka',
            'jml_paket.required' => 'harap diisi',
            'jml_paket.numeric' => 'diisi angka',
            'kuota.required' => 'harap diisi',
            'kuota.numeric' => 'diisi angka',
            // 'hari.required' => 'harap diisi',
            'hari.numeric' => 'diisi angka',
            'tgl_mulai.required' => 'harap diisi',
            'tgl_selesai.required' => 'harap diisi',
            'status.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'id_m_program_pelatihan' => ['required'],
            'id_m_gelombang' => ['required'],
            'nm_m_jadwal' => ['required'],
            'text_gelombang' => ['required'],
            'text_tahun' => ['required'],
            'tgl_mulai' => ['required'],
            'tgl_selesai' => ['required'],
            'status' => ['required'],
            'tahun' => ['required', 'numeric'],
            'jml_paket' => ['required', 'numeric'],
            'kuota' => ['required', 'numeric'],
            // 'hari' => ['required', 'numeric'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_program_pelatihan' => $errors->first('id_m_program_pelatihan'),
                    'id_m_gelombang' => $errors->first('id_m_gelombang'),
                    'nm_m_jadwal' => $errors->first('nm_m_jadwal'),
                    'text_gelombang' => $errors->first('text_gelombang'),
                    'text_tahun' => $errors->first('text_tahun'),
                    'tgl_mulai' => $errors->first('tgl_mulai'),
                    'tgl_selesai' => $errors->first('tgl_selesai'),
                    'tahun' => $errors->first('tahun'),
                    'jml_paket' => $errors->first('jml_paket'),
                    'kuota' => $errors->first('kuota'),
                    'hari' => $errors->first('hari'),
                    'status' => $errors->first('status'),
                ]
            ]);
        }

        $redundant_pelatihan = M_jadwal::select('id_m_jadwal')
                                ->where('id_m_gelombang', $request->id_m_gelombang)
                                ->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)
                                ->where('tahun', $request->tahun)
                                ->count();

        // if($redundant_pelatihan > 0){
        //     return response()->json([
        //         'status' => false,
        //         'message' => 'Pelatihan tersebut sudah ada di dalam jadwal Institusional Kategori PBK tahun '.$request->tahun,
        //     ]);
        // }

        $concate = $request->nm_m_jadwal . ' ' .$request->text_gelombang . ' ' . $request->text_tahun;

        
        /** 
         * get info from prog pelatihan
         */

        $prog_pelatihan = M_program_pelatihan::select('id_m_kejuruan')
                            ->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)
                            ->firstOrFail();

        $kejuruan = M_kejuruan::select('nm_m_kejuruan')->where('id_m_kejuruan', $prog_pelatihan->id_m_kejuruan)->first();
        $nm_kejuruan = ($kejuruan) ? $kejuruan->nm_m_kejuruan : '';

        $gel = M_gelombang::select('nm_m_gelombang')->where('id_m_gelombang', $request->id_m_gelombang)->first();
        $nm_gel = ($gel) ? $gel->nm_m_gelombang : '';


        /**
         * CHECK JIKA PENDAFTARAN MTU ATAU SWADAYA MAKA TIDAK MEMBUAT DATA DI CBT USER GRUP
         */
        $without_user_group = M_program_pelatihan::select('id_m_tipe_pelatihan')->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)->first();
        $no_cbt_user_group = ($without_user_group and $without_user_group->id_m_tipe_pelatihan == 1) ? true : false;

        if($no_cbt_user_group == true)
        {
            $find_redundant_cbt = Cbt_user_grup::select('grup_id')->where('grup_nama', $concate)->count();
            if($find_redundant_cbt > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'Sistem mengalami kesalahan pada sinkronisasi CBT ! (terjadi duplikasi data user group di CBT)',
                ]);
            }
        }

        DB::beginTransaction();
        $object = new M_jadwal;
        $object->id_m_jadwal = M_jadwal::MaxId();
        $object->id_m_gelombang = $request->id_m_gelombang;
        $object->id_m_program_pelatihan = $request->id_m_program_pelatihan;
        $object->nm_m_jadwal = $concate;
        $object->tahun = $request->tahun;
        $object->jml_paket = $request->jml_paket;
        $object->kuota = $request->kuota;
        $object->hari = $request->hari;
        $object->status = $request->status;
        $object->concat_nama = $request->nm_m_jadwal;
        $object->concat_gelombang = $request->text_gelombang;
        $object->concat_tahun = $request->text_tahun;

        $object->tgl_mulai = \Carbon\Carbon::parse($request->tgl_mulai)->format('Y-m-d');
        $object->tgl_selesai = \Carbon\Carbon::parse($request->tgl_selesai)->format('Y-m-d');
        $object->grup_id = Cbt_user_grup::maxId();
        
        $new_cbt = new Cbt_user_grup;
        $new_cbt->grup_id = Cbt_user_grup::maxId();
        // $new_cbt->grup_nama = $nm_kejuruan . ' ' . $nm_gel . ' ' . 'Tahun '. $request->tahun;
        $new_cbt->grup_nama = $concate;

        try{
            $object->save();
            
            if($no_cbt_user_group == true){
                $new_cbt->save();
            }
            
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_jadwal.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_jadwal') or !is_numeric(request('id_m_jadwal')), 404);

        $old = M_jadwal::where([
            'id_m_jadwal' => request('id_m_jadwal')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Jadwal Pelatihan',
            'page_title' => 'Jadwal Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Jadwal Pelatihan',
            'old' => $old,
            'prog'   => M_program_pelatihan::where('aktif', '1')->with('tipe_pelatihan')->orderBy('nm_m_program_pelatihan')->get(),
            'gelombang'   => M_gelombang::where('aktif_m_gelombang', '1')->orderBy('nm_m_gelombang')->get(),
        ];

        return view('back.m_jadwal.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'id_m_program_pelatihan.required' => 'pilih salah satu',
            'id_m_gelombang.required' => 'pilih salah satu',
            'nm_m_jadwal.required' => 'pilih salah satu',
            'tahun.required' => 'harap diisi',
            'tahun.numeric' => 'diisi angka',
            'jml_paket.required' => 'harap diisi',
            'jml_paket.numeric' => 'diisi angka',
            'kuota.required' => 'harap diisi',
            'kuota.numeric' => 'diisi angka',
            // 'hari.required' => 'harap diisi',
            'hari.numeric' => 'diisi angka',
            'tgl_mulai.required' => 'harap diisi',
            'tgl_selesai.required' => 'harap diisi',
            'status.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'id_m_program_pelatihan' => ['required'],
            'id_m_gelombang' => ['required'],
            'nm_m_jadwal' => ['required'],
            'tgl_mulai' => ['required'],
            'tgl_selesai' => ['required'],
            'status' => ['required'],
            'tahun' => ['required', 'numeric'],
            'jml_paket' => ['required', 'numeric'],
            'kuota' => ['required', 'numeric'],
            // 'hari' => ['required', 'numeric'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_program_pelatihan' => $errors->first('id_m_program_pelatihan'),
                    'id_m_gelombang' => $errors->first('id_m_gelombang'),
                    'nm_m_jadwal' => $errors->first('nm_m_jadwal'),
                    'tgl_mulai' => $errors->first('tgl_mulai'),
                    'tgl_selesai' => $errors->first('tgl_selesai'),
                    'tahun' => $errors->first('tahun'),
                    'jml_paket' => $errors->first('jml_paket'),
                    'kuota' => $errors->first('kuota'),
                    'hari' => $errors->first('hari'),
                    'status' => $errors->first('status'),
                ]
            ]);
        }

        $concate = $request->nm_m_jadwal . ' ' .$request->text_gelombang . ' ' . $request->text_tahun;

        $cek_pelatihan = M_jadwal::select('id_m_jadwal', 'id_m_gelombang', 'id_m_program_pelatihan', 'tahun')
                                ->where('id_m_jadwal', $request->id_m_jadwal)
                                ->firstOrFail();

        if(
            $cek_pelatihan->id_m_gelombang != $request->id_m_gelombang
            or $cek_pelatihan->id_m_program_pelatihan != $request->id_m_program_pelatihan
            or $cek_pelatihan->tahun != $request->tahun
        ){
            $redundant_pelatihan = M_jadwal::select('id_m_jadwal', 'id_m_gelombang', 'id_m_program_pelatihan', 'tahun')
                                ->where('id_m_gelombang', $request->id_m_gelombang)
                                ->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)
                                ->where('tahun', $request->tahun)
                                ->count();

            if($redundant_pelatihan > 0){
                return response()->json([
                    'status' => false,
                    'message' => 'Pelatihan tersebut sudah ada di dalam jadwal Institusional Kategori PBK tahun '.$request->tahun,
                ]);
            }
        }


        DB::beginTransaction();
        $update = M_jadwal::where([
            'id_m_jadwal' => $request->id_m_jadwal,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_jadwal' => 'Data not found !',
                ]
            ]);
        }

        $update->id_m_gelombang = $request->id_m_gelombang;
        $update->id_m_program_pelatihan = $request->id_m_program_pelatihan;
        $update->tahun = $request->tahun;
        $update->nm_m_jadwal = $concate;
        $update->jml_paket = $request->jml_paket;
        $update->kuota = $request->kuota;
        $update->hari = $request->hari;
        $update->status = $request->status;
        $update->tgl_mulai = \Carbon\Carbon::parse($request->tgl_mulai)->format('Y-m-d');
        $update->tgl_selesai = \Carbon\Carbon::parse($request->tgl_selesai)->format('Y-m-d');
        $update->concat_nama = $request->nm_m_jadwal;
        $update->concat_gelombang = $request->text_gelombang;
        $update->concat_tahun = $request->text_tahun;

        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_jadwal.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_jadwal')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_jadwal::where([
            'id_m_jadwal' => $request->id_m_jadwal
        ])
        ->with('kelas_pelatihan','pendaftar')
        ->with('cbt_user_grup', function($q){
            $q->with('cbt_user');
        })
        ->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            if($find->cbt_user_grup and count($find->cbt_user_grup) > 0){
                foreach($find->cbt_user_grup as $grup){
                    $grup->cbt_user()->delete();
                }
            }
            if($find->kelas_pelatihan and count($find->kelas_pelatihan) > 0){
                foreach($find->kelas_pelatihan as $kelas){
                    $kelas->peserta()->delete();
                }
            }

            if($find->pendaftar and count($find->pendaftar) > 0){
                foreach($find->pendaftar as $pendaftar){
                    $pendaftar->seleksi()->delete();
                    $pendaftar->penilaian_wawancara()->delete();
                }
            }

            if($find->pendaftar_mtu and count($find->pendaftar_mtu) > 0){
                foreach($find->pendaftar_mtu as $pendaftar_mtu){
                    $pendaftar_mtu->peserta()->delete();
                }
            }

            // $find->pendaftar()->delete();
            $find->kelas_pelatihan()->delete();
            $find->cbt_user_grup()->delete();
            // $find->pendaftar()->delete();
            $find->pendaftar_mtu()->delete();
            $find->delete();
            DB::commit();

            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_jadwal.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'terjadi kesalahan sistem',
            'status'  => false,
        ]);
    }
    
    
    
    public function publish(Request $request)
    {
        if(!$request->filled('id_m_jadwal')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_jadwal::where([
            'id_m_jadwal' => $request->id_m_jadwal
        ])
        ->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        
        $toggle = ($find->is_published == '1') ? '0' : '1';
        try{
            $find->is_published = $toggle;
            $find->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_jadwal.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'terjadi kesalahan sistem',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_jadwal::with('program_pelatihan', 'gelombang')->orderByDesc('id_m_jadwal')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

            if($value->is_published != '1'){
                $state = '<span class="badge badge-warning">belum tayang</span>';
                $test_status_publish = 'publish pengumuman';
            }else{
                $state = '<span class="badge badge-success">sudah tayang</span>';
                $test_status_publish = 'sembunyikan pengumuman';
            }

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->program_pelatihan->nm_m_program_pelatihan . ' - ' . $value->program_pelatihan->tipe_pelatihan->nm_m_tipe_pelatihan;
            $datas[$key][] = $value->gelombang->nm_m_gelombang;
            $datas[$key][] = $value->tahun;
            // $datas[$key][] = \Carbon\Carbon::parse($value->tgl_mulai)->format('d-m-Y');
            // $datas[$key][] = \Carbon\Carbon::parse($value->tgl_selesai)->format('d-m-Y');
            $datas[$key][] = $value->kuota;
            $datas[$key][] = $value->status;
            $datas[$key][] = $state;
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_jadwal.edit',['id_m_jadwal' => $value->id_m_jadwal]).'">edit</a>
                                <a class="dropdown-item publish" data-id_m_jadwal="'.$value->id_m_jadwal.'" href="#">'.$test_status_publish.'</a>
                                <a class="dropdown-item delete" data-id_m_jadwal="'.$value->id_m_jadwal.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }

    public function set_text_program(Request $request)
    {
        $val = $request->id_m_program_pelatihan;
        if(empty($val)){
            return response()->json([
                'status' => true,
                'html' => null,
            ]);
        }
        $get = M_program_pelatihan::select('nm_m_program_pelatihan')->where('id_m_program_pelatihan', $val)->firstOrFail();
        if($get){
            return response()->json([
                'status' => true,
                'html' => $get->nm_m_program_pelatihan.' ',
            ]);
        }
    }


}
