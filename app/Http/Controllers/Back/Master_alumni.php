<?php

namespace App\Http\Controllers\Back;

use App\Exports\PendaftarInsExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\Cbt_user;
use App\Models\M_disabilitas;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_kota;
use App\Models\M_alumni;
use App\Models\M_bidang_usaha;
use App\Models\M_program_pelatihan;
use App\Models\M_provinsi;
use App\Models\T_pekerjaan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class Master_alumni extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
                                    ->whereHas('kejuruan', function($q){
                                        $q->where('aktif_m_kejuruan', '1');
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('aktif', '1')
                                              ->where('id_m_tipe_pelatihan', 1)
                                              ->whereHas('jadwal');
                                        });
                                    })->get();
        /** 
         * original program pelatihan
         */
        if(request()->filled('id_m_jadwal')){
            $fk_program = M_jadwal::select('id_m_program_pelatihan')
                          ->where('id_m_jadwal', request()->get('id_m_jadwal'))
                          ->with('program_pelatihan', function($q){
                            $q->select('id_m_program_pelatihan','id_m_kejuruan');
                            $q->with('kejuruan', function($q){
                                $q->select('id_m_kejuruan', 'id_m_kategori_kejuruan');
                            });
                          })
                          ->first();
            $list_jadwal = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $fk_program->id_m_program_pelatihan)->get()->toArray();
            $list_program_pelatihan = ($fk_program->program_pelatihan) ? 
                                      M_program_pelatihan::where('id_m_kejuruan', $fk_program->program_pelatihan->id_m_kejuruan)->get()->toArray() : [];
            $list_kejuruan = ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? 
                             M_kejuruan::where('id_m_kategori_kejuruan', $fk_program->program_pelatihan->kejuruan->id_m_kategori_kejuruan)->get()->toArray() : [];
        }else{
            $fk_program = [];
            $list_jadwal = [];
            $list_program_pelatihan = [];
            $list_kejuruan = [];
        }
        $data = [
            'head_title' => 'Alumni',
            'page_title' => 'Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Alumni',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
            'list_jadwal' => $list_jadwal,
            'list_program_pelatihan' => $list_program_pelatihan,
            'list_kejuruan' => $list_kejuruan,
            'id_m_program_pelatihan' => ($fk_program and $fk_program->program_pelatihan) ? $fk_program->program_pelatihan->id_m_program_pelatihan : null,
            'id_m_kejuruan' => ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? $fk_program->program_pelatihan->kejuruan->id_m_kejuruan : null,
            'id_m_kategori_kejuruan' => ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? $fk_program->program_pelatihan->kejuruan->id_m_kategori_kejuruan : null,
        ];

        return view('back.m_alumni.index')->with($data);
    }

    public function add()
    {
        $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            ->whereYear('created_at', date('Y'))
                            // ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            // ->where('status', 'OPEN')
                            ->with('gelombang')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 1);
                            })
                            ->orderBy('created_at')->get();

        $data = [
            'head_title' => 'Alumni',
            'page_title' => 'Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Alumni',
            'list_gelombang' => $list_gelombang,
            'id_m_jadwal' => null,
        ];

    	return view('back.m_alumni.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            // 'id_m_jadwal.required' => "pilih salah satu",
            'nm_m_alumni.required' => "Harap mengisi nama Anda",
            'nik_m_alumni.required' => "Harap mengisi nomor induk kependudukan Anda",
            // 'nik_m_alumni.size' => "Harap mengisikan 16 digit NIK",
            'nik_m_alumni.numeric' => "Harap mengisikan 16 digit NIK berupa angka",
            'id_m_provinsi.required' => "Pilih provinsi",
            'id_m_kota.required' => "Pilih kota",
            'alamat_m_alumni.required' => "Harap mengisi alamat sesuai domisili",
            'jk_m_alumni.required' => "Pilih salah satu",
            'hp_m_alumni.required' => "Harap mengisi nomor whatsapp aktif",
            'tempat_lahir_m_alumni.required' => "Harap mengisi tempat lahir",
            'tgl_lahir_m_alumni.required' => "Harap mengisi tanggal lahir",
            'email_m_alumni.required' => "Harap mengisi email Anda",
            'email_m_alumni.email' => "Alamat email yang dimasukkan tidak valid",
            'is_bekerja_m_alumni.required' => "Pilih salah satu",
            'is_disabilitas.required' => "Pilih salah satu",
            'id_m_disabilitas.required' => "Pilih salah satu",
        ];


        $role_disabilitas = ($request->is_disabilitas == 'YA') ? ['required'] : ['nullable'];

        $validator = Validator::make($request->all(), [
            // 'id_m_jadwal' => ['required'],
            'nm_m_alumni' => ['required'],
            'nik_m_alumni' => ['required', 'numeric'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'alamat_m_alumni' => ['required'],
            'jk_m_alumni' => ['required'],
            'id_m_pendidikan' => ['required'],
            'hp_m_alumni' => ['required'],
            'tempat_lahir_m_alumni' => ['required'],
            'tgl_lahir_m_alumni' => ['required'],
            'email_m_alumni' => ['required','email'],
            'is_bekerja_m_alumni' => ['required'],
            'is_disabilitas' => ['required'],
            'id_m_disabilitas' => $role_disabilitas,
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    // 'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_m_alumni' => $errors->first('nm_m_alumni'),
                    'nik_m_alumni' => $errors->first('nik_m_alumni'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'alamat_m_alumni' => $errors->first('alamat_m_alumni'),
                    'jk_m_alumni' => $errors->first('jk_m_alumni'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'hp_m_alumni' => $errors->first('hp_m_alumni'),
                    'tempat_lahir_m_alumni' => $errors->first('tempat_lahir_m_alumni'),
                    'tgl_lahir_m_alumni' => $errors->first('tgl_lahir_m_alumni'),
                    'email_m_alumni' => $errors->first('email_m_alumni'),
                    'is_bekerja_m_alumni' => $errors->first('is_bekerja_m_alumni'),
                    'is_disabilitas' => $errors->first('is_disabilitas'),
                    'id_m_disabilitas' => $errors->first('id_m_disabilitas'),
                ]
            ]);
        }


        $find = M_alumni::select('id_m_alumni')
                ->where('nik_m_alumni', $request->nik_m_alumni);

        if($find->count() > 0){
            return response()->json([
                'message' => 'nomor identitas kependudukan (NIK) sudah ada di dalam database',
                'status'  => false,
            ]);
        }
        
        $new = new M_alumni;
        $new->id_m_alumni = M_alumni::maxId();
        $new->nik_m_alumni = $request->nik_m_alumni;
        $new->password_m_alumni = Hash::make($request->nik_m_alumni);
        $new->nm_m_alumni = Str::upper($request->nm_m_alumni);
        $new->hp_m_alumni =  $request->hp_m_alumni;
        $new->email_m_alumni =  $request->email_m_alumni;
        $new->aktif =  null;
        $new->token =  null;
        $new->is_bekerja_m_alumni =  $request->is_bekerja_m_alumni;
        $new->jk_m_alumni =  $request->jk_m_alumni;
        $new->id_m_pendidikan =  $request->id_m_pendidikan;
        $new->kejuruan_m_alumni =  $request->kejuruan_m_alumni;
        $new->asal_sekolah_m_alumni =  $request->asal_sekolah_m_alumni;
        $new->alamat_m_alumni =  $request->alamat_m_alumni;
        $new->id_m_kota = $request->id_m_kota;
        $new->tempat_lahir_m_alumni =  $request->tempat_lahir_m_alumni;
        $new->tgl_lahir_m_alumni = $request->tgl_lahir_m_alumni;
        $new->is_disabilitas =  $request->is_disabilitas;
        $new->id_m_disabilitas =  $request->id_m_disabilitas;
        $new->id_m_provinsi = $request->id_m_provinsi;

       
        try{
            app()->setLocale('id');
           
            $new->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }



    public function edit()
    {
        $old = M_alumni::where('id_m_alumni', request()->get('id_m_alumni'))->firstOrFail();
        $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            ->whereYear('created_at', date('Y'))
                            // ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            // ->where('status', 'OPEN')
                            ->with('gelombang')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 1);
                            })
                            ->orderBy('created_at')->get();

        $list_kab = M_kota::where('id_m_provinsi', $old->id_m_provinsi)->orderBy('nm_m_kota')->get();
        $list_prov = M_provinsi::orderBy('nm_m_provinsi')->get();
        $list_disabilitas = M_disabilitas::where('aktif', '1')->get();

        $data = [
            'head_title' => 'Alumni',
            'page_title' => 'Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Alumni',
            'list_gelombang' => $list_gelombang,
            'old' => $old,
            'id_m_jadwal' => null,
            'id_m_provinsi' => $old->id_m_provinsi,
            'list_kab' => $list_kab,
            'list_prov' => $list_prov,
            'list_disabilitas' => $list_disabilitas,
        ];

    	return view('back.m_alumni.edit')->with($data);
    }


    public function update(Request $request)
    {
        $messages = [
            // 'id_m_jadwal.required' => "pilih salah satu",
            'nm_m_alumni.required' => "Harap mengisi nama Anda",
            'nik_m_alumni.required' => "Harap mengisi nomor induk kependudukan Anda",
            // 'nik_m_alumni.size' => "Harap mengisikan 16 digit NIK",
            'nik_m_alumni.numeric' => "Harap mengisikan 16 digit NIK berupa angka",
            'id_m_provinsi.required' => "Pilih provinsi",
            'id_m_kota.required' => "Pilih kota",
            'alamat_m_alumni.required' => "Harap mengisi alamat sesuai domisili",
            'jk_m_alumni.required' => "Pilih salah satu",
            'hp_m_alumni.required' => "Harap mengisi nomor whatsapp aktif",
            'tempat_lahir_m_alumni.required' => "Harap mengisi tempat lahir",
            'tgl_lahir_m_alumni.required' => "Harap mengisi tanggal lahir",
            'email_m_alumni.required' => "Harap mengisi email Anda",
            'email_m_alumni.email' => "Alamat email yang dimasukkan tidak valid",
            'is_bekerja_m_alumni.required' => "Pilih salah satu",
            'is_disabilitas.required' => "Pilih salah satu",
            'id_m_disabilitas.required' => "Pilih salah satu",
        ];


        $role_disabilitas = ($request->is_disabilitas == 'YA') ? ['required'] : ['nullable'];

        $validator = Validator::make($request->all(), [
            // 'id_m_jadwal' => ['required'],
            'nm_m_alumni' => ['required'],
            'nik_m_alumni' => ['required', 'numeric'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'alamat_m_alumni' => ['required'],
            'jk_m_alumni' => ['required'],
            'id_m_pendidikan' => ['required'],
            'hp_m_alumni' => ['required'],
            'tempat_lahir_m_alumni' => ['required'],
            'tgl_lahir_m_alumni' => ['required'],
            'email_m_alumni' => ['required','email'],
            'is_bekerja_m_alumni' => ['required'],
            'is_disabilitas' => ['required'],
            'id_m_disabilitas' => $role_disabilitas,
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    // 'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_m_alumni' => $errors->first('nm_m_alumni'),
                    'nik_m_alumni' => $errors->first('nik_m_alumni'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'alamat_m_alumni' => $errors->first('alamat_m_alumni'),
                    'jk_m_alumni' => $errors->first('jk_m_alumni'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'hp_m_alumni' => $errors->first('hp_m_alumni'),
                    'tempat_lahir_m_alumni' => $errors->first('tempat_lahir_m_alumni'),
                    'tgl_lahir_m_alumni' => $errors->first('tgl_lahir_m_alumni'),
                    'email_m_alumni' => $errors->first('email_m_alumni'),
                    'is_bekerja_m_alumni' => $errors->first('is_bekerja_m_alumni'),
                    'is_disabilitas' => $errors->first('is_disabilitas'),
                    'id_m_disabilitas' => $errors->first('id_m_disabilitas'),
                ]
            ]);
        }

        $new = M_alumni::where('id_m_alumni', $request->id_m_alumni)->first();
        $new->nik_m_alumni = $request->nik_m_alumni;
        $new->password_m_alumni = Hash::make($request->nik_m_alumni);
        $new->nm_m_alumni = Str::upper($request->nm_m_alumni);
        $new->hp_m_alumni =  $request->hp_m_alumni;
        $new->email_m_alumni =  $request->email_m_alumni;
        $new->aktif =  null;
        $new->token =  null;
        $new->is_bekerja_m_alumni =  $request->is_bekerja_m_alumni;
        $new->jk_m_alumni =  $request->jk_m_alumni;
        $new->id_m_pendidikan =  $request->id_m_pendidikan;
        $new->kejuruan_m_alumni =  $request->kejuruan_m_alumni;
        $new->asal_sekolah_m_alumni =  $request->asal_sekolah_m_alumni;
        $new->alamat_m_alumni =  $request->alamat_m_alumni;
        $new->id_m_kota = $request->id_m_kota;
        $new->tempat_lahir_m_alumni =  $request->tempat_lahir_m_alumni;
        $new->tgl_lahir_m_alumni = $request->tgl_lahir_m_alumni;
        $new->is_disabilitas =  $request->is_disabilitas;
        $new->id_m_disabilitas =  ($request->is_disabilitas == 'YA') ? $request->id_m_disabilitas : null;
        $new->id_m_provinsi = $request->id_m_provinsi;

        try{
            $new->save();

            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }



    public function edit_pekerjaan()
    {
        $old = M_alumni::where('id_m_alumni', request()->get('id_m_alumni'))->firstOrFail();

        $data = [
            'head_title' => 'Alumni',
            'page_title' => 'Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Alumni',
            'old' => $old,
            'id_m_jadwal' => null,
        ];

    	return view('back.m_alumni.edit_pekerjaan')->with($data);
    }

    public function add_pekerjaan(Request $request)
    {
        $provinsi = M_provinsi::select('id_m_provinsi','nm_m_provinsi')->orderBy('nm_m_provinsi')->get();
        $data = [
            'head_title' => 'Data Pekerjaan Alumni',
            'page_title' => 'Data Pekerjaan Alumni',
            'parent_menu_active' => 'Alumni',
            'child_menu_active'   => 'Data Pekerjaan Alumni',
            'bidang_usaha' => M_bidang_usaha::where('aktif', '1')->get(),
            'id_m_provinsi' => $provinsi,
            'id_m_alumni' => $request->id_m_alumni,
        ];

    	return view('back.m_alumni.add_pekerjaan')->with($data);
    }


    public function add_pekerjaan_post(Request $request)
    {
        $messages = [
            'nm_t_pekerjaan.required' => 'harap diisi',
            'id_m_bidang_usaha.required' => 'harap diisi',
            'jabatan_t_pekerjaan.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_t_pekerjaan' => ['required'],
            'id_m_bidang_usaha' => ['required'],
            'jabatan_t_pekerjaan' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'id_m_kecamatan' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_t_pekerjaan' => $errors->first('nm_t_pekerjaan'),
                    'id_m_bidang_usaha' => $errors->first('id_m_bidang_usaha'),
                    'jabatan_t_pekerjaan' => $errors->first('jabatan_t_pekerjaan'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new T_pekerjaan;
        $object->id_t_pekerjaan = T_pekerjaan::MaxId();
        $object->nm_t_pekerjaan = $request->nm_t_pekerjaan;
        $object->jabatan_t_pekerjaan = $request->jabatan_t_pekerjaan;
        $object->telp_t_pekerjaan = $request->telp_t_pekerjaan;
        $object->tgl_mulai_bekerja = ($request->tgl_mulai_bekerja) ? Carbon::parse($request->tgl_mulai_bekerja)->format('Y-m-d') : null;
        $object->alamat_t_pekerjaan = $request->alamat_t_pekerjaan;
        $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_alumni = $request->id_m_alumni;
        $object->id_m_bidang_usaha = $request->id_m_bidang_usaha;

        try{
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data pekerjaan berhasil disimpan',
                'redirect' => route('admin.m_alumni.edit_pekerjaan', ['id_m_alumni' => $request->id_m_alumni])
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function delete_pekerjaan(Request $request)
    {
        if(!$request->filled('id_t_pekerjaan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_pekerjaan::where([
            'id_t_pekerjaan' => $request->id_t_pekerjaan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                // 'redirect' => route('admin.m_alumni.edit_pekerjaan', ['id_m_alumni' => $request->id_m_alumni])
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable_pekerjaan(Request $request)
    {
        $table = T_pekerjaan::where('id_m_alumni', $request->id_m_alumni)
                ->with('bidang')
                ->orderByDesc('id_t_pekerjaan')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_t_pekerjaan;
            $datas[$key][] = $value->bidang->nm_m_bidang_usaha;
            $datas[$key][] = $value->tgl_mulai_bekerja;
            $datas[$key][] = $value->jabatan_t_pekerjaan;
            $datas[$key][] = $value->telp_t_pekerjaan;
            $datas[$key][] = '<a class="btn-sm btn-danger delete" data-id_m_alumni="'.$value->id_m_alumni.'" data-id_t_pekerjaan="'.$value->id_t_pekerjaan.'" href="#">hapus</a>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


    public function view()
    {
        abort_if(!request()->filled('id_m_alumni') or !is_numeric(request('id_m_alumni')), 404);

        $old = M_alumni::where([
            'id_m_alumni' => request('id_m_alumni')
        ])->with('jadwal')->firstOrFail();

        $expl = explode('-',$old->no_register);
        $tahun_daftar = $expl[1];
        $tanggal_daftar = $expl[3];
        $tipe_daftar = $expl[4];
        $urut_daftar = $expl[5];

        $username = $tipe_daftar . $tahun_daftar . $tanggal_daftar . $urut_daftar;
        $cbt_user = Cbt_user::select('user_name', 'user_password')->where('user_name', $username)->first();


        $data = [
            'head_title' => 'Alumni',
            'page_title' => 'Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Alumni',
            'old' => $old,
            'username_cbt' => ($cbt_user) ? $cbt_user->user_name : null,
            'password_cbt' => ($cbt_user) ? $cbt_user->user_password : null,
        ];

        return view('back.m_alumni.view')->with($data);
    }

    
    public function delete(Request $request)
    {
        if(!$request->filled('id_m_alumni')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_alumni::where([
            'id_m_alumni' => $request->id_m_alumni
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->usaha()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan ?? null;
        $id_m_kejuruan = $request->id_m_kejuruan ?? null;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan ?? null;
        $id_m_jadwal = $request->id_m_jadwal ?? null;

        $table = M_alumni::view_alumni($id_m_kategori_kejuruan, $id_m_kejuruan, $id_m_program_pelatihan, $id_m_jadwal);
        
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {
            
    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nik_m_alumni;
            $datas[$key][] = $value->nm_m_alumni;
            $datas[$key][] = $value->hp_m_alumni;
            $datas[$key][] = $value->email_m_alumni;
            $datas[$key][] = $value->alamat_m_alumni;
            $datas[$key][] = $value->kejuruan_m_alumni;
            $datas[$key][] = ($value->created_at) ? Carbon::parse($value->created_at)->format('d-m-Y H:i:s') : null;
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_alumni.edit',['id_m_alumni' => $value->id_m_alumni]).'">edit</a>
                                        <a class="dropdown-item" href="'.route('admin.m_alumni.edit_pekerjaan',['id_m_alumni' => $value->id_m_alumni]).'">pekerjaan</a>
                                        <a class="dropdown-item delete" data-id_m_alumni="'.$value->id_m_alumni.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }

    public function export_xls()
    {
        return Excel::download(new PendaftarInsExport, 'export_alumni_inst.xlsx');
    }
}
