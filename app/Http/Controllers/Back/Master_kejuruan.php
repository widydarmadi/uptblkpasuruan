<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_kejuruan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Master_kejuruan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Kejuruan',
            'page_title' => 'Kejuruan',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'Kejuruan',
        ];

        return view('back.m_kejuruan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Admin Menu',
            'page_title' => 'Admin Menu',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'Admin Menu',
        ];

    	return view('back.m_kejuruan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_menu.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_menu' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_menu' => $errors->first('nm_menu'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_menu;
        $object->id_m_kejuruan = M_kejuruan::MaxId();
        $object->aktif = $request->aktif;
        $object->nm_menu = $request->nm_menu;
        $object->order_m_menu = $request->order_m_menu;
        $object->icon = $request->icon;
        $object->id_parent = $request->id_parent;
        $object->route = $request->route;
        $object->order_m_menu = $request->order_m_menu;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_kejuruan') or !is_numeric(request('id_m_kejuruan')), 404);

        $old = M_kejuruan::where([
            'id_m_kejuruan' => request('id_m_kejuruan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Admin Menu',
            'page_title' => 'Admin Menu',
            'parent_menu_active' => 'Master Data',
            'child_menu_active'   => 'Admin Menu',
            'old' => $old,
            'id_parent'   => M_kejuruan::whereNull('id_parent')->where('aktif','1')->get(),
        ];

        return view('back.m_kejuruan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_menu.required' => 'harap diisi',
            'icon.required' => 'harap diisi',
            'order_m_menu.numeric' => 'please fill number only',
        ];

        $validator = Validator::make($request->all(), [
            'nm_menu' => ['required'],
            'icon' => ['required'],
            'order_m_menu' => ['numeric'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_menu' => $errors->first('nm_menu'),
                    'icon' => $errors->first('icon'),
                    'order_m_menu' => $errors->first('order_m_menu'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_kejuruan::where([
            'id_m_kejuruan' => $request->id_m_kejuruan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_menu' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif = $request->aktif;
        $update->nm_menu = $request->nm_menu;
        $update->icon = $request->icon;
        $update->route = $request->route;
        $update->id_parent = $request->id_parent;
        $update->order_m_menu = $request->order_m_menu;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_kejuruan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_kejuruan::where([
            'id_m_kejuruan' => $request->id_m_kejuruan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_kejuruan::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_kejuruan;
            $datas[$key][] = ($value->aktif_m_kejuruan=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_menu.edit',['id_m_kejuruan' => $value->id_m_kejuruan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_kejuruan="'.$value->id_m_kejuruan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
