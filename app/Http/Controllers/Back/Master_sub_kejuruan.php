<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_sub_kejuruan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Sub Kejuruan',
            'page_title' => 'Sub Kejuruan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Sub Kejuruan',
        ];

        return view('back.m_sub_kejuruan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Sub Kejuruan',
            'page_title' => 'Sub Kejuruan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Sub Kejuruan',
            'kategori_kejuruan'   => M_kategori_kejuruan::where('aktif', '1')->orderBy('nm_m_kategori_kejuruan')->get(),
        ];

    	return view('back.m_sub_kejuruan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_kejuruan.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_kejuruan' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kejuruan' => $errors->first('nm_m_kejuruan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_kejuruan;
        $object->id_m_kejuruan = M_kejuruan::MaxId();
        $object->aktif_m_kejuruan = $request->aktif;
        $object->nm_m_kejuruan = $request->nm_m_kejuruan;
        $object->id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_sub_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_kejuruan') or !is_numeric(request('id_m_kejuruan')), 404);

        $old = M_kejuruan::where([
            'id_m_kejuruan' => request('id_m_kejuruan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Sub Kejuruan',
            'page_title' => 'Sub Kejuruan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Sub Kejuruan',
            'old' => $old,
            'kategori_kejuruan'   => M_kategori_kejuruan::where('aktif', '1')->orderBy('nm_m_kategori_kejuruan')->get(),
        ];

        return view('back.m_sub_kejuruan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_kejuruan.required' => 'harap diisi',
            'id_m_kategori_kejuruan.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_kejuruan' => ['required'],
            'id_m_kategori_kejuruan' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kejuruan' => $errors->first('nm_m_kejuruan'),
                    'id_m_kategori_kejuruan' => $errors->first('id_m_kategori_kejuruan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_kejuruan::where([
            'id_m_kejuruan' => $request->id_m_kejuruan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kejuruan' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif_m_kejuruan = $request->aktif;
        $update->nm_m_kejuruan = $request->nm_m_kejuruan;
        $update->id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_sub_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_kejuruan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_kejuruan::where([
            'id_m_kejuruan' => $request->id_m_kejuruan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_sub_kejuruan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_kejuruan::with('kategori_kejuruan')->orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_kejuruan;
            $datas[$key][] = $value->kategori_kejuruan->nm_m_kategori_kejuruan;
            $datas[$key][] = ($value->aktif_m_kejuruan=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_sub_kejuruan.edit',['id_m_kejuruan' => $value->id_m_kejuruan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_kejuruan="'.$value->id_m_kejuruan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
