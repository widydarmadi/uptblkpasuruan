<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_agenda;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_agenda extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Agenda',
            'page_title' => 'Agenda',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Agenda',
        ];

        return view('back.m_agenda.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Agenda',
            'page_title' => 'Agenda',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Agenda',
        ];

    	return view('back.m_agenda.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'judul_m_agenda.required' => 'harap diisi',
            'isi_m_agenda.required' => 'harap diisi',
            'aktif_m_agenda.required' => 'pilih satu',
            'tgl_mulai_m_agenda.required' => 'harap diisi',
            'tgl_selesai_m_agenda.required' => 'harap diisi',
            'lampiran_m_agenda.mimes' => 'format file tidak diizinkan',
            'photo.image' => 'format file tidak diizinkan',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_agenda' => ['required'],
            'isi_m_agenda' => ['required'],
            'aktif_m_agenda' => ['required'],
            'tgl_mulai_m_agenda' => ['required'],
            'tgl_selesai_m_agenda' => ['required'],
            'lampiran_m_agenda' => ['mimes:pdf,doc,docx,xls,xlsx,jpg,jpeg,png', 'nullable'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_agenda' => $errors->first('judul_m_agenda'),
                'isi_m_agenda' => $errors->first('isi_m_agenda'),
                'aktif_m_agenda' => $errors->first('aktif_m_agenda'),
                'lampiran_m_agenda' => $errors->first('lampiran_m_agenda'),
                'tgl_mulai_m_agenda' => $errors->first('tgl_mulai_m_agenda'),
                'tgl_selesai_m_agenda' => $errors->first('tgl_selesai_m_agenda'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_agenda;
        $object->id_m_agenda = M_agenda::MaxId();
        $object->judul_m_agenda = $request->judul_m_agenda;
        $object->isi_m_agenda = $request->isi_m_agenda;
        $object->slug_m_agenda = time().'-'.Str::slug($request->judul_m_agenda);
        $object->aktif_m_agenda = $request->aktif_m_agenda;
        $object->tgl_mulai_m_agenda = isset($request->tgl_mulai_m_agenda) ? \Carbon\Carbon::parse($request->tgl_mulai_m_agenda)->format('Y-m-d') : null;
        $object->tgl_selesai_m_agenda = isset($request->tgl_selesai_m_agenda) ? \Carbon\Carbon::parse($request->tgl_selesai_m_agenda)->format('Y-m-d') : null;
        

        try{
            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/agenda';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }
            if($request->file('lampiran_m_agenda')){
                $filename1 = time() . '_' . $request->file('lampiran_m_agenda')->getClientOriginalName();
                $folder = 'upload/agenda';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('lampiran_m_agenda'), $filename1);
                $object->lampiran_m_agenda = $path1;
            }
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_agenda.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_agenda') or !is_numeric(request('id_m_agenda')), 404);

        $old = M_agenda::where([
            'id_m_agenda' => request('id_m_agenda')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Agenda',
            'page_title' => 'Agenda',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Agenda',
            'old' => $old,
        ];

        return view('back.m_agenda.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'judul_m_agenda.required' => 'harap diisi',
            'isi_m_agenda.required' => 'harap diisi',
            'aktif_m_agenda.required' => 'pilih satu',
            'tgl_mulai_m_agenda.required' => 'harap diisi',
            'tgl_selesai_m_agenda.required' => 'harap diisi',
            'lampiran_m_agenda.mimes' => 'format file tidak diizinkan',
            'photo.image' => 'format file tidak diizinkan',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_agenda' => ['required'],
            'isi_m_agenda' => ['required'],
            'aktif_m_agenda' => ['required'],
            'tgl_mulai_m_agenda' => ['required'],
            'tgl_selesai_m_agenda' => ['required'],
            'lampiran_m_agenda' => ['mimes:pdf,doc,docx,xls,xlsx,jpg,jpeg,png', 'nullable'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_agenda' => $errors->first('judul_m_agenda'),
                'isi_m_agenda' => $errors->first('isi_m_agenda'),
                'aktif_m_agenda' => $errors->first('aktif_m_agenda'),
                'lampiran_m_agenda' => $errors->first('lampiran_m_agenda'),
                'tgl_mulai_m_agenda' => $errors->first('tgl_mulai_m_agenda'),
                'tgl_selesai_m_agenda' => $errors->first('tgl_selesai_m_agenda'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_agenda::where('id_m_agenda', $request->id_m_agenda)->first();
        $object->judul_m_agenda = $request->judul_m_agenda;
        $object->isi_m_agenda = $request->isi_m_agenda;
        $object->slug_m_agenda = time().'-'.Str::slug($request->judul_m_agenda);
        $object->aktif_m_agenda = $request->aktif_m_agenda;
        $object->tgl_mulai_m_agenda = isset($request->tgl_mulai_m_agenda) ? \Carbon\Carbon::parse($request->tgl_mulai_m_agenda)->format('Y-m-d') : null;
        $object->tgl_selesai_m_agenda = isset($request->tgl_selesai_m_agenda) ? \Carbon\Carbon::parse($request->tgl_selesai_m_agenda)->format('Y-m-d') : null;

        try{
            if($request->file('photo')){
                Storage::delete('public/'.$object->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/agenda';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            if($request->file('lampiran_m_agenda')){
                Storage::delete('public/'.$object->lampiran_m_agenda);
                $filename1 = time() . '_' . $request->file('lampiran_m_agenda')->getClientOriginalName();
                $folder = 'upload/agenda';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('lampiran_m_agenda'), $filename1);
                $object->lampiran_m_agenda = $path1;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_agenda.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_agenda')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_agenda::where([
            'id_m_agenda' => $request->id_m_agenda
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->lampiran_m_agenda);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_agenda.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_agenda::orderByDesc('id_m_agenda')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->judul_m_agenda;
            $datas[$key][] = \Carbon\Carbon::parse($value->tgl_mulai_m_agenda)->format('d-m-Y');
            $datas[$key][] = \Carbon\Carbon::parse($value->tgl_selesai_m_agenda)->format('d-m-Y');
            // $datas[$key][] = 'agenda/'.$value->slug_m_agenda;
            $datas[$key][] = ($value->aktif_m_agenda=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            // $datas[$key][] = ($value->updated_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_agenda.edit',['id_m_agenda' => $value->id_m_agenda]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_agenda="'.$value->id_m_agenda.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
