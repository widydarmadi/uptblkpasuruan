<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_sumber_dana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_sumber_dana extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Sumber Dana',
            'page_title' => 'Sumber Dana',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Sumber Dana',
        ];

        return view('back.m_sumber_dana.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Sumber Dana',
            'page_title' => 'Sumber Dana',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Sumber Dana',
        ];

    	return view('back.m_sumber_dana.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_sumber_dana.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_sumber_dana' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_sumber_dana' => $errors->first('nm_m_sumber_dana'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_sumber_dana;
        $object->id_m_sumber_dana = M_sumber_dana::MaxId();
        $object->aktif_m_sumber_dana = $request->aktif;
        $object->nm_m_sumber_dana = $request->nm_m_sumber_dana;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_sumber_dana.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_sumber_dana') or !is_numeric(request('id_m_sumber_dana')), 404);

        $old = m_sumber_dana::where([
            'id_m_sumber_dana' => request('id_m_sumber_dana')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Sumber Dana',
            'page_title' => 'Sumber Dana',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Sumber Dana',
            'old' => $old,
        ];

        return view('back.m_sumber_dana.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_sumber_dana.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_sumber_dana' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_sumber_dana' => $errors->first('nm_m_sumber_dana'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = m_sumber_dana::where([
            'id_m_sumber_dana' => $request->id_m_sumber_dana,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_sumber_dana' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif_m_sumber_dana = $request->aktif;
        $update->nm_m_sumber_dana = $request->nm_m_sumber_dana;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_sumber_dana.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_sumber_dana')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = m_sumber_dana::where([
            'id_m_sumber_dana' => $request->id_m_sumber_dana
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_sumber_dana.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = m_sumber_dana::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_sumber_dana;
            $datas[$key][] = ($value->aktif_m_sumber_dana=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_sumber_dana.edit',['id_m_sumber_dana' => $value->id_m_sumber_dana]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_sumber_dana="'.$value->id_m_sumber_dana.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
