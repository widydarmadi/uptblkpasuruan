<?php

namespace App\Http\Controllers\Back;

use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_branch;
use App\Models\M_slider;
use Illuminate\Support\Facades\DB as FacadesDB;
use Illuminate\Support\Facades\Storage;

class Master_slider extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Slider',
            'page_title' => 'Slider',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Slider',
        ];

        return view('back.m_slider.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Slider',
            'page_title' => 'Slider',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Slider',
        ];

    	return view('back.m_slider.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'judul_m_slider.required' => 'harap diisi',
            'deskripsi_m_slider.required' => 'harap diisi',
            'aktif_m_slider.required' => 'please choose one',
            'photo.required' => 'please choose one',
            'photo.image' => 'harus format .jpg / .png',
            // 'foto_kanan.required' => 'please choose one',
            // 'foto_kanan.image' => 'harus format .jpg / .png',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_slider' => ['required'],
            'deskripsi_m_slider' => ['required'],
            'aktif_m_slider' => ['required'],
            'photo' => ['required', 'image'],
            // 'foto_kanan' => ['required', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_slider' => $errors->first('judul_m_slider'),
                'deskripsi_m_slider' => $errors->first('deskripsi_m_slider'),
                'aktif_m_slider' => $errors->first('aktif_m_slider'),
                'photo' => $errors->first('photo'),
                // 'foto_kanan' => $errors->first('foto_kanan'),
            ]
            ]);
        }

        if($request->file('photo')->getSize() >= 2000000)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'photo' => 'file is too large',
                    // 'foto_kanan' => 'file is too large',
                ]
            ]);
        }

        DB::beginTransaction();
        $object = new M_slider;
        $object->id_m_slider = M_slider::MaxId();
        $object->judul_m_slider = $request->judul_m_slider;
        $object->aktif_m_slider = $request->aktif_m_slider;
        $object->deskripsi_m_slider = $request->deskripsi_m_slider;
        

        try{
            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                // $filename2 = time() . '_' . $request->file('foto_kanan')->getClientOriginalName();
                $folder = 'upload/slider';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                // $path2 = \Storage::disk('public')->putFileAs($f, $request->file('foto_kanan'), $filename2);
                $object->photo = $path1;
                // $object->foto_kanan = $path2;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_slider.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_slider') or !is_numeric(request('id_m_slider')), 404);

        $old = M_slider::where([
            'id_m_slider' => request('id_m_slider')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Slider',
            'page_title' => 'Slider',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Slider',
            'old' => $old,
        ];

        return view('back.m_slider.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'judul_m_slider.required' => 'harap diisi',
            'deskripsi_m_slider.required' => 'harap diisi',
            'aktif_m_slider.required' => 'please choose one',
            'photo.required' => 'please choose one',
            'photo.image' => 'harus format .jpg / .png',
            // 'foto_kanan.required' => 'please choose one',
            // 'foto_kanan.image' => 'harus format .jpg / .png',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_slider' => ['required'],
            'deskripsi_m_slider' => ['required'],
            'aktif_m_slider' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_slider' => $errors->first('judul_m_slider'),
                'deskripsi_m_slider' => $errors->first('deskripsi_m_slider'),
                'aktif_m_slider' => $errors->first('aktif_m_slider'),
                'photo' => $errors->first('photo'),
                // 'foto_kanan' => $errors->first('foto_kanan'),
            ]
            ]);
        }

        // if($request->file('photo')->getSize() >= 2000000 or $request->file('foto_kanan')->getSize() >= 2000000)
        // {
        //     $errors = $validator->errors();
        //     return response()->json([
        //         'error' => [
        //             'photo' => 'file is too large',
        //             'foto_kanan' => 'file is too large',
        //         ]
        //     ]);
        // }

        FacadesDB::beginTransaction();
        $object = M_slider::where('id_m_slider', $request->id_m_slider)->first();
        $object->judul_m_slider = $request->judul_m_slider;
        $object->aktif_m_slider = $request->aktif_m_slider;
        $object->deskripsi_m_slider = $request->deskripsi_m_slider;

        try{
            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/slider';
                $f = $folder;
                $path1 = Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            $object->save();
            FacadesDB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_slider.index'),
            ]);
        }catch(\Exception $e){
            FacadesDB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_slider')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_slider::where([
            'id_m_slider' => $request->id_m_slider
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->photo);
            // Storage::delete('public/'.$find->foto_kanan);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_slider.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_slider::orderByDesc('id_m_slider')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->judul_m_slider;
            $datas[$key][] = $value->deskripsi_m_slider;
            // dump($ug);
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = ($value->aktif_m_slider=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_slider.edit',['id_m_slider' => $value->id_m_slider]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_slider="'.$value->id_m_slider.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
