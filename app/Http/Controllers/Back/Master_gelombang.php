<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_gelombang;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_gelombang extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Gelombang',
            'page_title' => 'Gelombang',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Gelombang',
        ];

        return view('back.m_gelombang.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Gelombang',
            'page_title' => 'Gelombang',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Gelombang',
        ];

    	return view('back.m_gelombang.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_gelombang.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_gelombang' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_gelombang' => $errors->first('nm_m_gelombang'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_gelombang;
        $object->id_m_gelombang = M_gelombang::MaxId();
        $object->aktif_m_gelombang = $request->aktif;
        $object->nm_m_gelombang = $request->nm_m_gelombang;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_gelombang.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_gelombang') or !is_numeric(request('id_m_gelombang')), 404);

        $old = M_gelombang::where([
            'id_m_gelombang' => request('id_m_gelombang')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Gelombang',
            'page_title' => 'Gelombang',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Gelombang',
            'old' => $old,
        ];

        return view('back.m_gelombang.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_gelombang.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_gelombang' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_gelombang' => $errors->first('nm_m_gelombang'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_gelombang::where([
            'id_m_gelombang' => $request->id_m_gelombang,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_gelombang' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif_m_gelombang = $request->aktif;
        $update->nm_m_gelombang = $request->nm_m_gelombang;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_gelombang.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_gelombang')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_gelombang::where([
            'id_m_gelombang' => $request->id_m_gelombang
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_gelombang.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_gelombang::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_gelombang;
            $datas[$key][] = ($value->aktif_m_gelombang=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_gelombang.edit',['id_m_gelombang' => $value->id_m_gelombang]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_gelombang="'.$value->id_m_gelombang.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
