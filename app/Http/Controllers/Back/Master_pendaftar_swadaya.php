<?php

namespace App\Http\Controllers\Back;

use App\Exports\PendaftarSwadayaExport;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_disabilitas;
use App\Models\M_jadwal;
use App\Models\M_jenis_lokasi_mtu;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kecamatan;
use App\Models\M_kejuruan;
use App\Models\M_kelurahan;
use App\Models\M_kota;
use App\Models\M_pendaftar_mtu;
use App\Models\M_program_pelatihan;
use App\Models\M_provinsi;
use App\Models\T_siswa_noninst;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class Master_pendaftar_swadaya extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
                                    ->whereHas('kejuruan', function($q){
                                        $q->where('aktif_m_kejuruan', '1');
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('aktif', '1')
                                              ->where('id_m_tipe_pelatihan', 3)
                                              ->whereHas('jadwal');
                                        });
                                    })->get();
        /** 
         * original program pelatihan
         */
        if(request()->filled('id_m_jadwal')){
            $fk_program = M_jadwal::select('id_m_program_pelatihan')
                          ->where('id_m_jadwal', request()->get('id_m_jadwal'))
                          ->with('program_pelatihan', function($q){
                            $q->select('id_m_program_pelatihan','id_m_kejuruan');
                            $q->where('id_m_tipe_pelatihan', 3);
                            $q->with('kejuruan', function($q){
                                $q->select('id_m_kejuruan', 'id_m_kategori_kejuruan');
                            });
                          })
                          ->first();
            $list_jadwal = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $fk_program->id_m_program_pelatihan)->get()->toArray();
            $list_program_pelatihan = ($fk_program->program_pelatihan) ? 
                                      M_program_pelatihan::where('id_m_kejuruan', $fk_program->program_pelatihan->id_m_kejuruan)->where('id_m_tipe_pelatihan', 3)->get()->toArray() : [];
            $list_kejuruan = ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? 
                             M_kejuruan::where('id_m_kategori_kejuruan', $fk_program->program_pelatihan->kejuruan->id_m_kategori_kejuruan)->get()->toArray() : [];
        }else{
            $fk_program = [];
            $list_jadwal = [];
            $list_program_pelatihan = [];
            $list_kejuruan = [];
        }

        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Swadaya',
            'child_menu_active'   => 'Data Pendaftaran',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
            'list_jadwal' => $list_jadwal,
            'list_program_pelatihan' => $list_program_pelatihan,
            'list_kejuruan' => $list_kejuruan,
            'id_m_program_pelatihan' => ($fk_program and $fk_program->program_pelatihan) ? $fk_program->program_pelatihan->id_m_program_pelatihan : null,
            'id_m_kejuruan' => ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? $fk_program->program_pelatihan->kejuruan->id_m_kejuruan : null,
            'id_m_kategori_kejuruan' => ($fk_program and $fk_program->program_pelatihan and $fk_program->program_pelatihan->kejuruan) ? $fk_program->program_pelatihan->kejuruan->id_m_kategori_kejuruan : null,
        ];

        return view('back.m_pendaftar_swadaya.index')->with($data);
    }

    public function add()
    {
        $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            ->whereYear('created_at', date('Y'))
                            ->with('gelombang')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 3);
                            })
                            ->orderBy('created_at')->get();
        $jenis_lokasi = M_jenis_lokasi_mtu::where('aktif', '1')->where('is_swadaya', '1')->get();
        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Swadaya',
            'child_menu_active'   => 'Data Pendaftaran',
            'list_gelombang' => $list_gelombang,
            'id_m_jadwal' => null,
            'jenis_lokasi' => $jenis_lokasi,
        ];

    	return view('back.m_pendaftar_swadaya.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_pemohon.required' => 'harap diisi',
            'jabatan_pemohon.required' => 'harap diisi',
            'wa_pemohon.required' => 'harap diisi',
            'email_pemohon.email' => 'email tidak valid',
            'email_pemohon.required' => 'harap diisi',
            'id_m_jenis_lokasi_mtu.required' => 'harap diisi',
            'nm_lokasi_mtu.required' => 'harap diisi',
            'telp_lokasi_mtu.required' => 'harap diisi',
            'email_mtu.email' => 'email tidak valid',
            'email_mtu.required' => 'harap diisi',
            'alamat_lokasi_mtu.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
            'id_m_kelurahan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
            'file_proposal.required' => 'harap diisi',
            'progress.required' => 'harap diisi',
            'is_tampil.required' => 'harap diisi',
            'file_proposal.mimes' => 'format berkas harus PDF',
        ];

        $validator = Validator::make($request->all(), [
            'nm_pemohon' => ['required'],
            'jabatan_pemohon' => ['required'],
            'wa_pemohon' => ['required'],
            'email_pemohon' => ['required','email'],
            'id_m_jenis_lokasi_mtu' => ['required'],
            'nm_lokasi_mtu' => ['required'],
            'telp_lokasi_mtu' => ['required'],
            'email_mtu' => ['required','email'],
            'alamat_lokasi_mtu' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'id_m_kecamatan' => ['required'],
            'id_m_kelurahan' => ['required'],
            'id_m_jadwal' => ['required'],
            'progress' => ['required'],
            'is_tampil' => ['required'],
            'file_proposal' => 'required|mimes:pdf',
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_pemohon' => $errors->first('nm_pemohon'),
                    'jabatan_pemohon' => $errors->first('jabatan_pemohon'),
                    'wa_pemohon' => $errors->first('wa_pemohon'),
                    'email_pemohon' => $errors->first('email_pemohon'),
                    'id_m_jenis_lokasi_mtu' => $errors->first('id_m_jenis_lokasi_mtu'),
                    'nm_lokasi_mtu' => $errors->first('nm_lokasi_mtu'),
                    'telp_lokasi_mtu' => $errors->first('telp_lokasi_mtu'),
                    'email_mtu' => $errors->first('email_mtu'),
                    'alamat_lokasi_mtu' => $errors->first('alamat_lokasi_mtu'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                    'id_m_kelurahan' => $errors->first('id_m_kelurahan'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'progress' => $errors->first('progress'),
                    'is_tampil' => $errors->first('is_tampil'),
                    'file_proposal' => $errors->first('file_proposal'),
                ]
            ]);
        }

        $tahun = date('Y');
        $bulantanggal = date('md');
        $max = M_pendaftar_mtu::whereRaw('DATE_FORMAT(created_at,\'%m%y\')=?', [date('my')])->max('no_register_urut');
        $no_urut_reg = $max + 1;
        $tahun=date('Y');
        $tahun_short = substr($tahun,2,2);
        $no_reg_ok = sprintf("%04s", $no_urut_reg);
        $tipe_pelatihan = 'SWA';

        $noreg = 'REG-'.$tahun_short.'-BLKPAS-'.$bulantanggal.'-'.$tipe_pelatihan.'-'.$no_reg_ok.'';


        DB::beginTransaction();
        $object = new M_pendaftar_mtu;
        $object->id_m_pendaftar_mtu = M_pendaftar_mtu::MaxId();
        $object->nm_pemohon = $request->nm_pemohon;
        $object->jabatan_pemohon = $request->jabatan_pemohon;
        $object->wa_pemohon = $request->wa_pemohon;
        $object->email_pemohon = $request->email_pemohon;
        $object->id_m_jenis_lokasi_mtu = $request->id_m_jenis_lokasi_mtu;
        $object->nm_lokasi_mtu = $request->nm_lokasi_mtu;
        $object->telp_lokasi_mtu = $request->telp_lokasi_mtu;
        // $object->potensi_wilayah = $request->potensi_wilayah;
        $object->email_mtu = $request->email_mtu;
        $object->kategori_m_pendaftar = $request->cat;
        $object->alamat_lokasi_mtu = $request->alamat_lokasi_mtu;
        $object->id_m_provinsi = $request->id_m_provinsi;
        $object->id_m_kota = $request->id_m_kota;
        $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_kelurahan = $request->id_m_kelurahan;
        $object->id_m_jadwal = $request->id_m_jadwal;
        $object->progress = $request->progress;
        $object->is_tampil = $request->is_tampil;
        $object->no_register = $noreg;
        $object->no_register_urut = $no_urut_reg;

        try{

            if($request->file('file_proposal')){
                $filename1 = time() . '_' . $request->file('file_proposal')->getClientOriginalName();
                $folder = 'upload/proposal';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('file_proposal'), $filename1);
                $object->file_proposal = $path1;
            }
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Pendaftaran Pihak Ketiga berhasil disimpan',
                'redirect' => route('admin.m_pendaftar_swadaya.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_pendaftar_mtu') or !is_numeric(request('id_m_pendaftar_mtu')), 404);

        $old = M_pendaftar_mtu::where([
            'id_m_pendaftar_mtu' => request('id_m_pendaftar_mtu')
        ])->firstOrFail();

        $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            ->whereYear('created_at', date('Y'))
                            ->with('gelombang')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 3);
                            })
                            ->orderBy('created_at')->get();
        $jenis_lokasi = M_jenis_lokasi_mtu::where('aktif', '1')->where('is_swadaya', '1')->get();

        $list_kel = M_kelurahan::where('id_m_kecamatan', $old->id_m_kecamatan)->orderBy('nm_m_kelurahan')->get();
        $list_kec = M_kecamatan::where('id_m_kota', $old->id_m_kota)->orderBy('nm_m_kecamatan')->get();
        $list_kab = M_kota::where('id_m_provinsi', $old->id_m_provinsi)->orderBy('nm_m_kota')->get();
        $list_prov = M_provinsi::orderBy('nm_m_provinsi')->get();
        

        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Swadaya',
            'child_menu_active'   => 'Data Pendaftaran',
            'old' => $old,
            'list_gelombang' => $list_gelombang,
            'id_m_jadwal' => null,
            'jenis_lokasi' => $jenis_lokasi,
            'id_m_provinsi' => $old->id_m_provinsi,
            'list_prov' => $list_prov,
            'list_kab' => $list_kab,
            'list_kec' => $list_kec,
            'list_kel' => $list_kel,
        ];

        return view('back.m_pendaftar_swadaya.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_pemohon.required' => 'harap diisi',
            'jabatan_pemohon.required' => 'harap diisi',
            'wa_pemohon.required' => 'harap diisi',
            'email_pemohon.email' => 'email tidak valid',
            'email_pemohon.required' => 'harap diisi',
            'id_m_jenis_lokasi_mtu.required' => 'harap diisi',
            'nm_lokasi_mtu.required' => 'harap diisi',
            'telp_lokasi_mtu.required' => 'harap diisi',
            'email_mtu.email' => 'email tidak valid',
            'email_mtu.required' => 'harap diisi',
            'alamat_lokasi_mtu.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
            'id_m_kelurahan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
            'file_proposal.required' => 'harap diisi',
            'progress.required' => 'harap diisi',
            'is_tampil.required' => 'harap diisi',
            'file_proposal.mimes' => 'format berkas harus PDF',
        ];

        $jadwal = ($request->progress == 'DISETUJUI') ? ['required'] : ['nullable'];

        $validator = Validator::make($request->all(), [
            'nm_pemohon' => ['required'],
            'jabatan_pemohon' => ['required'],
            'wa_pemohon' => ['required'],
            'email_pemohon' => ['required','email'],
            'id_m_jenis_lokasi_mtu' => ['required'],
            'nm_lokasi_mtu' => ['required'],
            'telp_lokasi_mtu' => ['required'],
            'email_mtu' => ['required','email'],
            'alamat_lokasi_mtu' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'id_m_kecamatan' => ['required'],
            'id_m_kelurahan' => ['required'],
            'id_m_jadwal' => $jadwal,
            'progress' => ['required'],
            'is_tampil' => ['required'],
            'file_proposal' => 'nullable|mimes:pdf',
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_pemohon' => $errors->first('nm_pemohon'),
                    'jabatan_pemohon' => $errors->first('jabatan_pemohon'),
                    'wa_pemohon' => $errors->first('wa_pemohon'),
                    'email_pemohon' => $errors->first('email_pemohon'),
                    'id_m_jenis_lokasi_mtu' => $errors->first('id_m_jenis_lokasi_mtu'),
                    'nm_lokasi_mtu' => $errors->first('nm_lokasi_mtu'),
                    'telp_lokasi_mtu' => $errors->first('telp_lokasi_mtu'),
                    'email_mtu' => $errors->first('email_mtu'),
                    'alamat_lokasi_mtu' => $errors->first('alamat_lokasi_mtu'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                    'id_m_kelurahan' => $errors->first('id_m_kelurahan'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'progress' => $errors->first('progress'),
                    'is_tampil' => $errors->first('is_tampil'),
                    'file_proposal' => $errors->first('file_proposal'),
                ]
            ]);
        }

        DB::beginTransaction();
        $object = M_pendaftar_mtu::where('id_m_pendaftar_mtu', $request->id_m_pendaftar_mtu)->first();
        $object->nm_pemohon = $request->nm_pemohon;
        $object->jabatan_pemohon = $request->jabatan_pemohon;
        $object->wa_pemohon = $request->wa_pemohon;
        $object->email_pemohon = $request->email_pemohon;
        $object->id_m_jenis_lokasi_mtu = $request->id_m_jenis_lokasi_mtu;
        $object->nm_lokasi_mtu = $request->nm_lokasi_mtu;
        $object->telp_lokasi_mtu = $request->telp_lokasi_mtu;
        // $object->potensi_wilayah = $request->potensi_wilayah;
        $object->email_mtu = $request->email_mtu;
        $object->kategori_m_pendaftar = $request->cat;
        $object->alamat_lokasi_mtu = $request->alamat_lokasi_mtu;
        $object->id_m_provinsi = $request->id_m_provinsi;
        $object->id_m_kota = $request->id_m_kota;
        $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_kelurahan = $request->id_m_kelurahan;
        $object->id_m_jadwal = $request->id_m_jadwal;
        $object->progress = $request->progress;
        $object->is_tampil = $request->is_tampil;
        try{
            if($request->file('file_proposal')){
                \Storage::delete('public/'.$find->file_proposal);
                $filename1 = time() . '_' . $request->file('file_proposal')->getClientOriginalName();
                $folder = 'upload/proposal';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('file_proposal'), $filename1);
                $object->file_proposal = $path1;
            }
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Pendaftaran Pihak Ketiga berhasil diupdate',
                'redirect' => route('admin.m_pendaftar_swadaya.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function delete(Request $request)
    {
        if(!$request->filled('id_m_pendaftar_mtu')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_pendaftar_mtu::where([
            'id_m_pendaftar_mtu' => $request->id_m_pendaftar_mtu
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->file_proposal);
            if($find->peserta and count($find->peserta) > 0){
                foreach($find->peserta as $peserta){
                    $peserta->peserta_noninst()->delete();
                }
            }
            $find->peserta()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pendaftar_swadaya.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan ?? null;
        $id_m_kejuruan = $request->id_m_kejuruan ?? null;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan ?? null;
        $id_m_jadwal = $request->id_m_jadwal ?? null;
        $type = 'SWA';
        $table = M_pendaftar_mtu::view_pendaftar($id_m_kategori_kejuruan, $id_m_kejuruan, $id_m_program_pelatihan, $id_m_jadwal, $type);
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->no_register;
            $datas[$key][] = Carbon::parse($value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = $value->nm_pemohon;
            $datas[$key][] = $value->wa_pemohon;
            $datas[$key][] = $value->jadwal->nm_m_jadwal;
            $datas[$key][] = number_format($value->peserta_count,0,'.',',');
            $datas[$key][] = $value->progress;
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_pendaftar_swadaya.edit',['id_m_pendaftar_mtu' => $value->id_m_pendaftar_mtu]).'">edit</a>
                                        <a data-no_register="'.$value->no_register.'" class="dropdown-item peserta" href="javascript:void(0)">tambah peserta</a>
                                        <a data-no_register="'.$value->no_register.'" class="dropdown-item lihatpeserta" href="javascript:void(0)">lihat peserta</a>
                                        <a class="dropdown-item delete" data-id_m_pendaftar_mtu="'.$value->id_m_pendaftar_mtu.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }



    public function datatable_siswa(Request $request)
    {
        $table = T_siswa_noninst::where('no_register',$request->no_register)->with('pendidikan')->orderByDesc('created_at')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nik_t_siswa;
            $datas[$key][] = $value->nm_t_siswa;
            $datas[$key][] = $value->email_t_siswa;
            $datas[$key][] = $value->wa_t_siswa;
            $datas[$key][] = $value->alamat_t_siswa;
            $datas[$key][] = $value->pendidikan->nm_m_pendidikan;
            $datas[$key][] = $value->tempat_lahir_t_siswa.', '.\Carbon\Carbon::parse($value->tgl_lahir_t_siswa)->format('d-m-Y');
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="javascript:void(0)" data-id_t_siswa_noninst="'.$value->id_t_siswa_noninst.'">edit</a>
                                        <a href="javascript:void(0)" data-id_t_siswa_noninst="'.$value->id_t_siswa_noninst.'" class="dropdown-item deletesiswa">hapus</a>
                                    </div>
                                </div>';
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }




    public function peserta(Request $request)
    {
        $list_disabilitas = M_disabilitas::where('aktif', '1')->get();
        $data = [
            'head_title' => 'Data Pendaftaran',
            'page_title' => 'Data Pendaftaran',
            'parent_menu_active' => 'Pelatihan Swadaya',
            'child_menu_active'   => 'Data Pendaftaran',
            'no_register' => $request->no_register,
            'list_disabilitas' => $list_disabilitas,

        ];

    	return view('back.m_pendaftar_swadaya.peserta')->with($data);
    }

    /** 
     * PESERTA POST
     */


    public function peserta_post(Request $request)
    {

        $messages = [
            'nm_t_siswa.required' => "Harap mengisi nama Anda",
            'nik_t_siswa.required' => "Harap mengisi nomor induk kependudukan Anda",
            'nik_t_siswa.numeric' => "Harap mengisikan 16 digit NIK berupa angka",
            'id_m_provinsi.required' => "Pilih provinsi",
            'id_m_kota.required' => "Pilih kota",
            'alamat_t_siswa.required' => "Harap mengisi alamat sesuai domisili",
            'jk_t_siswa.required' => "Pilih salah satu",
            'wa_t_siswa.required' => "Harap mengisi nomor whatsapp aktif",
            'tempat_lahir_t_siswa.required' => "Harap mengisi tempat lahir",
            'tgl_lahir_t_siswa.required' => "Harap mengisi tanggal lahir",
            'email_t_siswa.required' => "Harap mengisi email Anda",
            'email_t_siswa.email' => "Alamat email yang dimasukkan tidak valid",
            'quest_pernah_blk.required' => "Pilih salah satu",
            'quest_masih_bekerja.required' => "Pilih salah satu",
            'quest_minat_setelah_kursus.required' => "Pilih salah satu",
            'is_disabilitas.required' => "Pilih salah satu",
            'id_m_disabilitas.required' => "Pilih salah satu",
            'is_disabilitas.required' => "Pilih salah satu",
            'id_m_disabilitas.required' => "Pilih salah satu",
            
        ];

        $role_disabilitas = ($request->is_disabilitas == 'YA') ? ['required'] : ['nullable'];


        $validator = Validator::make($request->all(), [
            'nm_t_siswa' => ['required'],
            'nik_t_siswa' => ['required', 'numeric'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'alamat_t_siswa' => ['required'],
            'jk_t_siswa' => ['required'],
            'id_m_pendidikan' => ['required'],
            'wa_t_siswa' => ['required'],
            'tempat_lahir_t_siswa' => ['required'],
            'tgl_lahir_t_siswa' => ['required'],
            'email_t_siswa' => ['required','email'],
            'quest_pernah_blk' => ['required'],
            'quest_masih_bekerja' => ['required'],
            'quest_minat_setelah_kursus' => ['required'],
            'is_disabilitas' => ['required'],
            'id_m_disabilitas' => $role_disabilitas,
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'cbt_grup_id' => $errors->first('cbt_grup_id'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_t_siswa' => $errors->first('nm_t_siswa'),
                    'nik_t_siswa' => $errors->first('nik_t_siswa'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'alamat_t_siswa' => $errors->first('alamat_t_siswa'),
                    'jk_t_siswa' => $errors->first('jk_t_siswa'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'wa_t_siswa' => $errors->first('wa_t_siswa'),
                    'tempat_lahir_t_siswa' => $errors->first('tempat_lahir_t_siswa'),
                    'tgl_lahir_t_siswa' => $errors->first('tgl_lahir_t_siswa'),
                    'email_t_siswa' => $errors->first('email_t_siswa'),
                    'quest_pernah_blk' => $errors->first('quest_pernah_blk'),
                    'quest_masih_bekerja' => $errors->first('quest_masih_bekerja'),
                    'quest_minat_setelah_kursus' => $errors->first('quest_minat_setelah_kursus'),
                    'is_disabilitas' => $errors->first('is_disabilitas'),
                    'id_m_disabilitas' => $errors->first('id_m_disabilitas'),
                ]
            ]);
        }

        $ceknik = T_siswa_noninst::where('nik_t_siswa', $request->nik_t_siswa)->where('no_register', $request->no_register)->count();
        if($ceknik > 0){
            return response()->json([
                'status' => false,
                'message' => 'Data Peserta dengan NIK tersebut sudah ada di sistem !',
            ]);
        }

        $new = new T_siswa_noninst;
        $new->id_t_siswa_noninst = T_siswa_noninst::maxId();
        $new->no_register = $request->no_register;
        $new->nm_t_siswa = Str::upper($request->nm_t_siswa);
        $new->nik_t_siswa = $request->nik_t_siswa;
        $new->id_m_provinsi = $request->id_m_provinsi;
        $new->id_m_kota = $request->id_m_kota;
        $new->alamat_t_siswa =  $request->alamat_t_siswa;
        $new->jk_t_siswa =  $request->jk_t_siswa;
        $new->id_m_pendidikan =  $request->id_m_pendidikan;
        $new->wa_t_siswa =  $request->wa_t_siswa;
        $new->nm_jurusan =  $request->nm_jurusan;
        $new->tempat_lahir_t_siswa =  $request->tempat_lahir_t_siswa;
        $new->tgl_lahir_t_siswa = $request->tgl_lahir_t_siswa;
        $new->email_t_siswa =  $request->email_t_siswa;
        $new->quest_pernah_blk =  $request->quest_pernah_blk;
        $new->quest_masih_bekerja =  $request->quest_masih_bekerja;
        $new->quest_minat_setelah_kursus =  $request->quest_minat_setelah_kursus;

        try{
         
            $new->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Peserta Pelatihan berhasil disimpan !',
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }


    public function lihat_peserta(Request $request)
    {
        $get_nama_latihan = M_pendaftar_mtu::where('no_register', $request->no_register)->with('jadwal')->firstOrFail();
        $data = [
            'nama_latihan' => $get_nama_latihan->jadwal->nm_m_jadwal,
            'no_register' => $request->no_register,
        ];

    	return view('back.m_pendaftar_swadaya.lihat_peserta')->with($data);
    }


    public function delete_peserta(Request $request)
    {
        if(!$request->filled('id_t_siswa_noninst')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_siswa_noninst::where([
            'id_t_siswa_noninst' => $request->id_t_siswa_noninst
        ])
        ->with('peserta_noninst')
        ->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();
        try{
            if($find->peserta_noninst){
                $find->peserta_noninst()->delete();
            }
            $find->delete();

            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pendaftar_swadaya.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }


    public function load_kejuruan_available(Request $request)
    {
        $load = M_kejuruan::where('id_m_kategori_kejuruan', $request->id)
                ->where('aktif_m_kejuruan', '1')
                ->whereHas('program_pelatihan', function($q){
                    $q->where('aktif', '1')
                      ->where('id_m_tipe_pelatihan', 3)
                      ->whereHas('jadwal', function($q){
                            // $q->where('status', 'OPEN');
                      });
                })->get();
        $html = '';
        $html .= "<option value=''>Pilih Sub Kejuruan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_kejuruan."'>".$item->nm_m_kejuruan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    
    public function load_program_available(Request $request)
    {
        $load = M_program_pelatihan::select('id_m_program_pelatihan', 'nm_m_program_pelatihan')->where('id_m_kejuruan', $request->id)
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 3)
                ->whereHas('jadwal', function($q){
                    // $q->where('status', 'OPEN');
                })->get();

        $html = '';
        $html .= "<option value=''>Pilih Program Pelatihan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_program_pelatihan."'>".$item->nm_m_program_pelatihan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    public function load_jadwal_available(Request $request)
    {
        $load = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $request->id)
                // ->where('status', 'OPEN')
                ->get();

        $html = '';
        $html .= "<option value=''>Pilih Jadwal / Gelombang</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_jadwal."'>".$item->nm_m_jadwal."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }


    public function export_xls()
    {
        return Excel::download(new PendaftarSwadayaExport, 'export_pendaftar_swa.xlsx');
    }

}
