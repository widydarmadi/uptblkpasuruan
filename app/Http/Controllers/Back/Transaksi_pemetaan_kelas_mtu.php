<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_alumni;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_pendaftar;
use App\Models\M_pendaftar_mtu;
use App\Models\M_program_kegiatan;
use App\Models\M_program_pelatihan;
use App\Models\T_kelas_pelatihan;
use App\Models\T_kelas_pelatihan_det;
use App\Models\T_peserta;
use App\Models\T_seleksi;
use App\Models\T_siswa_noninst;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Transaksi_pemetaan_kelas_mtu extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
                                    ->whereHas('kejuruan', function($q){
                                        $q->where('aktif_m_kejuruan', '1');
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('aktif', '1')
                                              ->where('id_m_tipe_pelatihan', 2)
                                              ->whereHas('jadwal', function($q){
                                                // $q->where('status', 'OPEN');
                                              });
                                        });
                                    })->get();

        $data = [
            'head_title' => 'Pemetaan Kelas',
            'page_title' => 'Pemetaan Kelas',
            'parent_menu_active' => 'Pelatihan MTU',
            'child_menu_active'   => 'Pemetaan Kelas',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
        ];

        return view('back.t_pemetaan_kelas_mtu.index')->with($data);
    }

    public function load_kejuruan_available(Request $request)
    {
        $load = M_kejuruan::where('id_m_kategori_kejuruan', $request->id)
                ->where('aktif_m_kejuruan', '1')
                ->whereHas('program_pelatihan', function($q){
                    $q->where('aktif', '1')
                      ->where('id_m_tipe_pelatihan', 2)
                      ->whereHas('jadwal', function($q){
                      });
                })->get();
        $html = '';
        $html .= "<option value=''>Pilih Sub Kejuruan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_kejuruan."'>".$item->nm_m_kejuruan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    
    public function load_program_available(Request $request)
    {
        $load = M_program_pelatihan::select('id_m_program_pelatihan', 'nm_m_program_pelatihan')->where('id_m_kejuruan', $request->id)
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 2)
                ->whereHas('jadwal', function($q){
                })->get();

        $html = '';
        $html .= "<option value=''>Pilih Program Pelatihan</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_program_pelatihan."'>".$item->nm_m_program_pelatihan."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }
    
    public function load_jadwal_available(Request $request)
    {
        $load = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal')->where('id_m_program_pelatihan', $request->id)
                ->get();

        $html = '';
        $html .= "<option value=''>Pilih Jadwal / Gelombang</option>";
        foreach($load as $item){
            $html .= "<option value='".$item->id_m_jadwal."'>".$item->nm_m_jadwal."</option>";
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }

    public function add()
    {
        $prog = M_program_pelatihan::select('id_m_program_pelatihan','nm_m_program_pelatihan')
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 2)
                ->whereHas('jadwal', function($q){
                    $q->whereHas('pendaftar_mtu');
                    // $q->where('tahun', date('Y'));
                })
                ->orderByDesc('id_m_program_pelatihan')
                ->get();
        $data = [
            'head_title' => 'Pemetaan Kelas',
            'page_title' => 'Pemetaan Kelas',
            'parent_menu_active' => 'Pelatihan MTU',
            'child_menu_active'   => 'Pemetaan Kelas',
            'id_m_program_pelatihan' => $prog,
        ];

    	return view('back.t_pemetaan_kelas_mtu.add')->with($data);
    }

    public function save(Request $request)
    {

        $messages = [
            'nama_kelas.required' => 'harap diisi',
            'id_m_program_pelatihan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
            'nm_pemohon.required' => 'harap diisi',
            'status.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_kelas' => ['required'],
            'id_m_program_pelatihan' => ['required'],
            'id_m_jadwal' => ['required'],
            'nm_pemohon' => ['required'],
            'status' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nama_kelas' => $errors->first('nama_kelas'),
                    'id_m_program_pelatihan' => $errors->first('id_m_program_pelatihan'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_pemohon' => $errors->first('nm_pemohon'),
                    'status' => $errors->first('status'),
                ]
            ]);
        }

        if(!is_array($request->pilih_siswa)){
            return response()->json([
                'status' => false,
                'message' => 'silahkan pilih setidaknya 1 (satu) peserta untuk melanjutkan',
            ]);
        }


        DB::beginTransaction();
        $id_kelas = T_kelas_pelatihan::MaxId();

        $object = new T_kelas_pelatihan;
        $object->id_t_kelas_pelatihan = $id_kelas;
        $object->status = $request->status;
        $object->nama_kelas = $request->nama_kelas;
        $object->id_m_jadwal = $request->id_m_jadwal;
        $object->no_register = $request->nm_pemohon;

        if(count($request->pilih_siswa) > 16){
            return response()->json([
                'status' => false,
                'message' => 'Maksimal 16 (enam belas) peserta dalam satu kelas !',
            ]);
        }
        
        $redundan_nama = T_kelas_pelatihan::select('id_t_kelas_pelatihan')->where('nama_kelas', $request->nama_kelas)->count();
        if($redundan_nama > 0){
            return response()->json([
                'status' => false,
                'message' => 'Nama kelas sudah ada di dalam sistem ! silahkan cek input atau ubah penamaan kelas',
            ]);
        }

        try{

            for($i=0; $i < count($request->pilih_siswa); $i++){
                $det = new T_peserta;
                $det->id_t_peserta = T_peserta::maxId();
                $det->id_t_kelas_pelatihan = $id_kelas;
                $det->id_t_seleksi = ($request->pilih_siswa[$i]) ? $request->pilih_siswa[$i] : null;
                if($request->pilih_siswa[$i]){
                    $exist = T_siswa_noninst::select('nik_t_siswa','jk_t_siswa','id_m_pendidikan','nm_jurusan','alamat_t_siswa','tempat_lahir_t_siswa',
                    'tgl_lahir_t_siswa','nm_t_siswa','email_t_siswa','wa_t_siswa','nm_jurusan','id_m_provinsi','id_m_kota','quest_masih_bekerja')->where('id_t_siswa_noninst', $request->pilih_siswa[$i])->with('alumni')->first();
                    if($exist){
                        if($exist->alumni){
                            $new_account = $exist;
                        }else{
                            $new_account = new M_alumni;
                            $new_account->id_m_alumni = M_alumni::maxId();
                            $new_account->nik_m_alumni = $exist->nik_t_siswa;
                            $new_account->aktif = null;
                            $new_account->nm_m_alumni = Str::upper($exist->nm_t_siswa);
                            $new_account->email_m_alumni = $exist->email_t_siswa;
                            $new_account->hp_m_alumni = $exist->wa_t_siswa;
                            $new_account->password_m_alumni = Hash::make($exist->nik_t_siswa);
                            $new_account->token = Crypt::encrypt($exist->nik_t_siswa);
                            $new_account->id_m_pendidikan = $exist->id_m_pendidikan;
                            $new_account->kejuruan_m_alumni = $exist->nm_jurusan;
                            $new_account->jk_m_alumni = $exist->jk_t_siswa;
                            $new_account->alamat_m_alumni = $exist->alamat_t_siswa;
                            $new_account->tempat_lahir_m_alumni = $exist->tempat_lahir_t_siswa;
                            $new_account->tgl_lahir_m_alumni = $exist->tgl_lahir_t_siswa;
                            $new_account->id_m_provinsi = $exist->id_m_provinsi;
                            $new_account->id_m_kota = $exist->id_m_kota;
                            $new_account->is_bekerja_m_alumni = $exist->quest_masih_bekerja;
                            $new_account->save();
                        }
                    }
                    $det->save();
                }
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Kelas baru berhasil ditambahkan !',
                'redirect' => route('admin.t_pemetaan_kelas_mtu.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_t_kelas_pelatihan') or !is_numeric(request('id_t_kelas_pelatihan')), 404);

        $old = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => request('id_t_kelas_pelatihan')
        ])
        ->with('peserta_mtu','pendaftar_mtu:no_register,nm_pemohon')
        ->with('jadwal:id_m_jadwal,id_m_program_pelatihan,nm_m_jadwal')
        ->firstOrFail();

        $prog = M_program_pelatihan::select('id_m_program_pelatihan','nm_m_program_pelatihan')
                ->where('aktif', '1')
                ->where('id_m_tipe_pelatihan', 2)
                ->whereHas('jadwal', function($q){
                    // $q->where('tahun', date('Y'));
                })
                ->where('id_m_program_pelatihan', $old->jadwal->id_m_program_pelatihan)
                ->orderByDesc('id_m_program_pelatihan')
                ->get();


        $data = [
            'head_title' => 'Pemetaan Kelas',
            'page_title' => 'Pemetaan Kelas',
            'parent_menu_active' => 'Pelatihan MTU',
            'child_menu_active'   => 'Pemetaan Kelas',
            'old' => $old,
            'id_m_program_pelatihan' => $prog,
            // 'id_m_jadwal' => $jadwal,
            'selected_program' => $old->jadwal->id_m_program_pelatihan,
            'selected_jadwal' => $old->id_m_jadwal,
        ];

        return view('back.t_pemetaan_kelas_mtu.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nama_kelas.required' => 'harap diisi',
            'id_m_program_pelatihan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
            'nm_pemohon.required' => 'harap diisi',
            'status.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_kelas' => ['required'],
            'id_m_program_pelatihan' => ['required'],
            'id_m_jadwal' => ['required'],
            'nm_pemohon' => ['required'],
            'status' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nama_kelas' => $errors->first('nama_kelas'),
                    'id_m_program_pelatihan' => $errors->first('id_m_program_pelatihan'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_pemohon' => $errors->first('nm_pemohon'),
                    'status' => $errors->first('status'),
                ]
            ]);
        }

        if(!is_array($request->pilih_siswa)){
            return response()->json([
                'status' => false,
                'message' => 'silahkan pilih setidaknya 1 (satu) peserta untuk melanjutkan',
            ]);
        }

        $check_perubahan_nama = T_kelas_pelatihan::select('nama_kelas')->where('nama_kelas', $request->nama_kelas)->first();
        if($check_perubahan_nama->nama_kelas != $request->nama_kelas)
        {
            $redundan_nama = T_kelas_pelatihan::select('id_t_kelas_pelatihan')->where('nama_kelas', $request->nama_kelas)->count();
            if($redundan_nama > 0)
            {
                return response()->json([
                    'status' => false,
                    'message' => 'Nama kelas sudah ada di dalam sistem ! silahkan cek input atau ubah penamaan kelas',
                ]);
            }
        }

        
        DB::beginTransaction();
        $update = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => $request->id_t_kelas_pelatihan,
            ])->first();
        $id_kelas = $update->id_t_kelas_pelatihan;

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_t_kelas_pelatihan' => 'Data not found !',
                ]
            ]);
        }

        $update->id_t_kelas_pelatihan = $id_kelas;
        $update->status = $request->status;
        $update->nama_kelas = $request->nama_kelas;
        $update->id_m_jadwal = $request->id_m_jadwal;

        if(count($request->pilih_siswa) > 16){
            return response()->json([
                'status' => false,
                'message' => 'Maksimal 16 (enam belas) peserta dalam satu kelas !',
            ]);
        }
        try{
            //clear data peserta
            T_peserta::where('id_t_kelas_pelatihan', $id_kelas)->delete();

            //re-insert data peserta
            for($i=0; $i < count($request->pilih_siswa); $i++){
                $det = new T_peserta;
                $det->id_t_peserta = T_peserta::maxId();
                $det->id_t_kelas_pelatihan = $id_kelas;
                $det->id_t_seleksi = $request->pilih_siswa[$i];
                if($request->pilih_siswa[$i]){
                    $exist = T_siswa_noninst::select('nik_t_siswa','jk_t_siswa','id_m_pendidikan','nm_jurusan','alamat_t_siswa','tempat_lahir_t_siswa',
                    'tgl_lahir_t_siswa','nm_t_siswa','email_t_siswa','wa_t_siswa','nm_jurusan','id_m_provinsi','id_m_kota','quest_masih_bekerja')->where('id_t_siswa_noninst', $request->pilih_siswa[$i])->with('alumni')->first();
                    if($exist){
                        if($exist->alumni){
                            $new_account = $exist;
                        }else{
                            $new_account = new M_alumni;
                            $new_account->id_m_alumni = M_alumni::maxId();
                            $new_account->nik_m_alumni = $exist->nik_t_siswa;
                            $new_account->aktif = null;
                            $new_account->nm_m_alumni = Str::upper($exist->nm_t_siswa);
                            $new_account->email_m_alumni = $exist->email_t_siswa;
                            $new_account->hp_m_alumni = $exist->wa_t_siswa;
                            $new_account->password_m_alumni = Hash::make($exist->nik_t_siswa);
                            $new_account->token = Crypt::encrypt($exist->nik_t_siswa);
                            $new_account->id_m_pendidikan = $exist->id_m_pendidikan;
                            $new_account->kejuruan_m_alumni = $exist->nm_jurusan;
                            $new_account->jk_m_alumni = $exist->jk_t_siswa;
                            $new_account->alamat_m_alumni = $exist->alamat_t_siswa;
                            $new_account->tempat_lahir_m_alumni = $exist->tempat_lahir_t_siswa;
                            $new_account->tgl_lahir_m_alumni = $exist->tgl_lahir_t_siswa;
                            $new_account->id_m_provinsi = $exist->id_m_provinsi;
                            $new_account->id_m_kota = $exist->id_m_kota;
                            $new_account->is_bekerja_m_alumni = $exist->quest_masih_bekerja;
                            $new_account->save();
                        }
                    }
                    $det->save();
                }
            }

            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data kelas berhasil diperbaharui !',
                'redirect' => route('admin.t_pemetaan_kelas_mtu.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_t_kelas_pelatihan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => $request->id_t_kelas_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->peserta()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.t_pemetaan_kelas_mtu.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan;
        $id_m_kejuruan = $request->id_m_kejuruan;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan;
        $id_m_jadwal = $request->id_m_jadwal;
        // dd(123);
        $table = T_kelas_pelatihan::select('id_t_kelas_pelatihan','id_m_jadwal','nama_kelas', 'status','created_at')
                                    ->whereHas('jadwal', function($q){
                                        $q->whereHas('program_pelatihan', function($q){
                                            $q->where('id_m_tipe_pelatihan', 2);
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        !isset($id_m_kejuruan) and
                                        !isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_kategori_kejuruan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                                                $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                                    $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                                                });
                                            });
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        !isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_kejuruan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                                                $q->where('id_m_kejuruan', $id_m_kejuruan);
                                            });
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_program_pelatihan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                                            $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        isset($id_m_program_pelatihan) and
                                        isset($id_m_jadwal)
                                    , function($q) use ($id_m_jadwal){
                                        $q->where('id_m_jadwal', $id_m_jadwal);
                                    })
                                    ->with('jadwal:id_m_jadwal,nm_m_jadwal')
                                    ->withCount('peserta_mtu')
                                    ->orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nama_kelas;
            $datas[$key][] = $value->jadwal->nm_m_jadwal;
            $datas[$key][] = ($value->status=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Non-Aktif</span>';
            $datas[$key][] = number_format($value->peserta_mtu_count, 0, '.', '.');
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.t_pemetaan_kelas_mtu.edit',['id_t_kelas_pelatihan' => $value->id_t_kelas_pelatihan]).'">kelola kelas</a>
                                <a class="dropdown-item delete" data-id_t_kelas_pelatihan="'.$value->id_t_kelas_pelatihan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


    public function load_jadwal(Request $request)
    {
        if($request->id_m_program_pelatihan){
            $jadwal = M_jadwal::select('id_m_jadwal','nm_m_jadwal')
                        ->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)
                        // ->where('status', 'OPEN')
                        ->get();
        }else{
            $jadwal = null;
        }

        $html = ($jadwal and count($jadwal) > 0) ? 
                                    '<option value="">-- pilih jadwal pelatihan --</option>' :
                                    '<option value="">-- tidak ada jadwal --</option>';

        if($jadwal != null){
            foreach($jadwal as $jad){
                $html .= '<option value="'.$jad->id_m_jadwal.'">'.$jad->nm_m_jadwal.'</option>';
            }
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }

    public function nm_pemohon(Request $request)
    {
        if($request->id_m_jadwal){
            $mtu_person = M_pendaftar_mtu::select('nm_pemohon','no_register')
                        ->where('id_m_jadwal', $request->id_m_jadwal)
                        ->orderBy('created_at')
                        ->get();
        }else{
            $mtu_person = null;
        }

        $html = ($mtu_person and count($mtu_person) > 0) ? 
                '<option value="">-- pilih nama pemohon --</option>' :
                '<option value="">-- tidak ada data --</option>';

        if($mtu_person != null){
            foreach($mtu_person as $jad){
                $html .= '<option value="'.$jad->no_register.'">'.$jad->nm_pemohon.'</option>';
            }
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }

    public function list_siswa(Request $request)
    {
        $pagetype = ($request->pagetype == 'add') ? 'add' : null;
        if($request->no_register){
            DB::enableQuerylog();
            $list_siswa = T_siswa_noninst::select('id_t_siswa_noninst', 'id_t_siswa_noninst', 'nm_jurusan', 
                                'nm_t_siswa', 'nik_t_siswa', 'jk_t_siswa', 'alamat_t_siswa', 'id_m_pendidikan')
                                ->where('no_register', $request->no_register)
                                // ->whereDoesntHave('peserta_noninst')
                                ->when($pagetype == 'add', function($q){
                                    $q->whereDoesntHave('peserta_noninst');
                                })
                                ->with('pendidikan:id_m_pendidikan,nm_m_pendidikan')
                                ->with('peserta_noninst')
                                ->orderBy('nm_t_siswa')
                                ->get();
            dd(DB::getquerylog());
            $data = [
                'list_siswa' => $list_siswa,
            ];
            
            return view('back.t_pemetaan_kelas_mtu.list_siswa', $data);
        }
    }


    public function load_nama_jadwal(Request $request)
    {
        if($request->id_m_jadwal){
            $jadwal = M_jadwal::select('nm_m_jadwal')->where('id_m_jadwal', $request->id_m_jadwal)->first();
            echo $jadwal->nm_m_jadwal;
        }
    }


}
