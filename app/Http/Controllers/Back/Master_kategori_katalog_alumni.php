<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_katalog_alumni;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Master_kategori_katalog_alumni extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Kategori Katalog Alumni',
            'page_title' => 'Kategori Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Kategori Katalog Alumni',
        ];

        return view('back.m_kategori_katalog_alumni.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Kategori Katalog Alumni',
            'page_title' => 'Kategori Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Kategori Katalog Alumni',
        ];

    	return view('back.m_kategori_katalog_alumni.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_kategori_katalog_alumni.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_kategori_katalog_alumni' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kategori_katalog_alumni' => $errors->first('nm_m_kategori_katalog_alumni'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_kategori_katalog_alumni;
        $object->id_m_kategori_katalog_alumni = M_kategori_katalog_alumni::MaxId();
        $object->aktif_m_kategori_katalog_alumni = $request->aktif_m_kategori_katalog_alumni;
        $object->nm_m_kategori_katalog_alumni = $request->nm_m_kategori_katalog_alumni;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_kategori_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_kategori_katalog_alumni') or !is_numeric(request('id_m_kategori_katalog_alumni')), 404);

        $old = M_kategori_katalog_alumni::where([
            'id_m_kategori_katalog_alumni' => request('id_m_kategori_katalog_alumni')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Kategori Katalog Alumni',
            'page_title' => 'Kategori Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Kategori Katalog Alumni',
            'old' => $old,
        ];

        return view('back.m_kategori_katalog_alumni.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_kategori_katalog_alumni.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_kategori_katalog_alumni' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kategori_katalog_alumni' => $errors->first('nm_m_kategori_katalog_alumni'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_kategori_katalog_alumni::where([
            'id_m_kategori_katalog_alumni' => $request->id_m_kategori_katalog_alumni,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_kategori_katalog_alumni' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif_m_kategori_katalog_alumni = $request->aktif_m_kategori_katalog_alumni;
        $update->nm_m_kategori_katalog_alumni = $request->nm_m_kategori_katalog_alumni;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_kategori_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_kategori_katalog_alumni')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_kategori_katalog_alumni::where([
            'id_m_kategori_katalog_alumni' => $request->id_m_kategori_katalog_alumni
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_kategori_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_kategori_katalog_alumni::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_kategori_katalog_alumni;
            $datas[$key][] = ($value->aktif_m_kategori_katalog_alumni=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_kategori_katalog_alumni.edit',['id_m_kategori_katalog_alumni' => $value->id_m_kategori_katalog_alumni]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_kategori_katalog_alumni="'.$value->id_m_kategori_katalog_alumni.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
