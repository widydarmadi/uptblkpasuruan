<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_agenda;
use App\Models\M_link_cepat;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_link_cepat extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Link Cepat',
            'page_title' => 'Link Cepat',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Link Cepat',
        ];

        return view('back.m_link_cepat.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Link Cepat',
            'page_title' => 'Link Cepat',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Link Cepat',
        ];

    	return view('back.m_link_cepat.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_link_cepat.required' => 'harap diisi',
            'url_m_link_cepat.required' => 'harap diisi',
            'aktif.required' => 'pilih satu',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_link_cepat' => ['required'],
            'url_m_link_cepat' => ['required'],
            'aktif' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_m_link_cepat' => $errors->first('nm_m_link_cepat'),
                'url_m_link_cepat' => $errors->first('url_m_link_cepat'),
                'aktif' => $errors->first('aktif'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_link_cepat;
        $object->id_m_link_cepat = M_link_cepat::MaxId();
        $object->nm_m_link_cepat = $request->nm_m_link_cepat;
        $object->url_m_link_cepat = $request->url_m_link_cepat;
        $object->aktif = $request->aktif;

        try{
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_link_cepat.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_link_cepat') or !is_numeric(request('id_m_link_cepat')), 404);

        $old = M_link_cepat::where([
            'id_m_link_cepat' => request('id_m_link_cepat')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Link Cepat',
            'page_title' => 'Link Cepat',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Link Cepat',
            'old' => $old,
        ];

        return view('back.m_link_cepat.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_link_cepat.required' => 'harap diisi',
            'url_m_link_cepat.required' => 'harap diisi',
            'aktif.required' => 'pilih satu',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_link_cepat' => ['required'],
            'url_m_link_cepat' => ['required'],
            'aktif' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_link_cepat' => $errors->first('nm_m_link_cepat'),
                    'url_m_link_cepat' => $errors->first('url_m_link_cepat'),
                    'aktif' => $errors->first('aktif'),
                ]
            ]);
        }

        DB::beginTransaction();
        $object = M_link_cepat::where('id_m_link_cepat', $request->id_m_link_cepat)->first();
        $object->nm_m_link_cepat = $request->nm_m_link_cepat;
        $object->url_m_link_cepat = $request->url_m_link_cepat;
        $object->aktif = $request->aktif;
        try{
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_link_cepat.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_link_cepat')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_link_cepat::where([
            'id_m_link_cepat' => $request->id_m_link_cepat
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_link_cepat.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_link_cepat::orderByDesc('id_m_link_cepat')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_link_cepat;
            $datas[$key][] = $value->url_m_link_cepat;
            $datas[$key][] = ($value->aktif=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_link_cepat.edit',['id_m_link_cepat' => $value->id_m_link_cepat]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_link_cepat="'.$value->id_m_link_cepat.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
