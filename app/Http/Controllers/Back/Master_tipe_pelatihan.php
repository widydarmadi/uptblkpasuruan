<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_tipe_pelatihan;
use App\Models\M_sumber_dana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_tipe_pelatihan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Tipe Pelatihan',
            'page_title' => 'Tipe Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Tipe Pelatihan',
        ];

        return view('back.m_tipe_pelatihan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Tipe Pelatihan',
            'page_title' => 'Tipe Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Tipe Pelatihan',
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
            'kategori_kejuruan' => M_kategori_kejuruan::where('aktif', '1')->orderBy('nm_m_kategori_kejuruan')->get(),
            'tipe_pelatihan' => M_tipe_pelatihan::where('aktif_m_tipe_pelatihan', '1')->orderBy('nm_m_tipe_pelatihan')->get(),
        ];

    	return view('back.m_tipe_pelatihan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_tipe_pelatihan.required' => 'harap diisi',
            'deskripsi_m_tipe_pelatihan.required' => 'harap diisi',
            'aktif_m_tipe_pelatihan.required' => 'harap diisi',
            'gambar_m_tipe_pelatihan.required' => 'harap diisi',
            'gambar_m_tipe_pelatihan.image' => 'format file tidak diizinkan',
            'pdf_m_tipe_pelatihan.mimes' => 'format file tidak diizinkan',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_tipe_pelatihan' => ['required'],
            'deskripsi_m_tipe_pelatihan' => ['required'],
            'aktif_m_tipe_pelatihan' => ['required'],
            'gambar_m_tipe_pelatihan' => ['required', 'image'],
            'pdf_m_tipe_pelatihan' => ['nullable', 'mimes:pdf'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_tipe_pelatihan' => $errors->first('nm_m_tipe_pelatihan'),
                    'deskripsi_m_tipe_pelatihan' => $errors->first('deskripsi_m_tipe_pelatihan'),
                    'aktif_m_tipe_pelatihan' => $errors->first('aktif_m_tipe_pelatihan'),
                    'gambar_m_tipe_pelatihan' => $errors->first('gambar_m_tipe_pelatihan'),
                    'pdf_m_tipe_pelatihan' => $errors->first('pdf_m_tipe_pelatihan'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_tipe_pelatihan;
        $object->id_m_tipe_pelatihan = M_tipe_pelatihan::MaxId();
        $object->nm_m_tipe_pelatihan = $request->nm_m_tipe_pelatihan;
        $object->deskripsi_m_tipe_pelatihan = $request->deskripsi_m_tipe_pelatihan;
        $object->aktif_m_tipe_pelatihan = $request->aktif_m_tipe_pelatihan;
        try{

            if($request->file('gambar_m_tipe_pelatihan')){
                $filename1 = time() . '_FOTO_' . $request->file('gambar_m_tipe_pelatihan')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('gambar_m_tipe_pelatihan'), $filename1);
                $object->gambar_m_tipe_pelatihan = $path2;
            }

            if($request->file('pdf_m_tipe_pelatihan')){
                $filename1 = time() . '_PDF_' . $request->file('pdf_m_tipe_pelatihan')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('pdf_m_tipe_pelatihan'), $filename1);
                $object->pdf_m_tipe_pelatihan = $path2;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Tipe Pelatihan berhasil disimpan',
                'redirect' => route('admin.m_tipe_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_tipe_pelatihan') or !is_numeric(request('id_m_tipe_pelatihan')), 404);

        $old = M_tipe_pelatihan::where([
            'id_m_tipe_pelatihan' => request('id_m_tipe_pelatihan')
        ])->firstOrFail();

        $data = [
            'head_title' => 'Tipe Pelatihan',
            'page_title' => 'Tipe Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Tipe Pelatihan',
            'old' => $old,
        ];

        return view('back.m_tipe_pelatihan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_tipe_pelatihan.required' => 'harap diisi',
            'deskripsi_m_tipe_pelatihan.required' => 'harap diisi',
            'aktif_m_tipe_pelatihan.required' => 'harap diisi',
            'gambar_m_tipe_pelatihan.required' => 'harap diisi',
            'gambar_m_tipe_pelatihan.image' => 'format file tidak diizinkan',
            'pdf_m_tipe_pelatihan.mimes' => 'format file tidak diizinkan',
        ];

        $cek_foto = M_tipe_pelatihan::select('gambar_m_tipe_pelatihan')->where('id_m_tipe_pelatihan', $request->id_m_tipe_pelatihan)->first();
        if(isset($cek_foto->gambar_m_tipe_pelatihan)){
            $state_foto = ['nullable', 'image'];
        }else{
            $state_foto = ['required', 'image'];
        }

        $validator = Validator::make($request->all(), [
            'nm_m_tipe_pelatihan' => ['required'],
            'deskripsi_m_tipe_pelatihan' => ['required'],
            'aktif_m_tipe_pelatihan' => ['required'],
            'gambar_m_tipe_pelatihan' => $state_foto,
            'pdf_m_tipe_pelatihan' => ['nullable', 'mimes:pdf'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_tipe_pelatihan' => $errors->first('nm_m_tipe_pelatihan'),
                    'deskripsi_m_tipe_pelatihan' => $errors->first('deskripsi_m_tipe_pelatihan'),
                    'aktif_m_tipe_pelatihan' => $errors->first('aktif_m_tipe_pelatihan'),
                    'gambar_m_tipe_pelatihan' => $errors->first('gambar_m_tipe_pelatihan'),
                    'pdf_m_tipe_pelatihan' => $errors->first('pdf_m_tipe_pelatihan'),
                ]
            ]);
        }

        DB::beginTransaction();
        $update = m_tipe_pelatihan::where([
            'id_m_tipe_pelatihan' => $request->id_m_tipe_pelatihan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_tipe_pelatihan' => 'Data not found !',
                ]
            ]);
        }

        $update->nm_m_tipe_pelatihan = $request->nm_m_tipe_pelatihan;
        $update->deskripsi_m_tipe_pelatihan = $request->deskripsi_m_tipe_pelatihan;
        $update->aktif_m_tipe_pelatihan = $request->aktif_m_tipe_pelatihan;
        try{

            if($request->file('gambar_m_tipe_pelatihan')){
                \Storage::delete('public/'.$update->gambar_m_tipe_pelatihan);
                $filename1 = time() . '_FOTO_' . $request->file('gambar_m_tipe_pelatihan')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('gambar_m_tipe_pelatihan'), $filename1);
                $update->gambar_m_tipe_pelatihan = $path2;
            }

            if($request->file('pdf_m_tipe_pelatihan')){
                \Storage::delete('public/'.$update->pdf_m_tipe_pelatihan);
                $filename1 = time() . '_PDF_' . $request->file('pdf_m_tipe_pelatihan')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('pdf_m_tipe_pelatihan'), $filename1);
                $update->pdf_m_tipe_pelatihan = $path2;
            }


            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_tipe_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function reset(Request $request)
    {
        $find = M_tipe_pelatihan::where([
            'id_m_tipe_pelatihan' => $request->id_m_tipe_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        try{
            \Storage::delete('public/'.$find->gambar_m_tipe_pelatihan);
            $find->gambar_m_tipe_pelatihan = null;
            $find->save();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_tipe_pelatihan.edit', ['id_m_tipe_pelatihan' => $request->edit]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }
    
    
    public function reset_pdf(Request $request)
    {
        $find = M_tipe_pelatihan::where([
            'id_m_tipe_pelatihan' => $request->id_m_tipe_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        try{
            \Storage::delete('public/'.$find->pdf_m_tipe_pelatihan);
            $find->pdf_m_tipe_pelatihan = null;
            $find->save();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_tipe_pelatihan.edit', ['id_m_tipe_pelatihan' => $request->edit]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_tipe_pelatihan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_tipe_pelatihan::where([
            'id_m_tipe_pelatihan' => $request->id_m_tipe_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->gambar_m_tipe_pelatihan);
            \Storage::delete('public/'.$find->pdf_m_tipe_pelatihan);
            if($find->program_pelatihan and count($find->program_pelatihan) > 0){
                foreach($find->program_pelatihan as $program)
                {
                    if($program->jadwal and count($program->jadwal) > 0){
                        foreach($program->jadwal as $item){
                            if($item->kelas_pelatihan and count($item->kelas_pelatihan) > 0){
                                foreach($item->kelas_pelatihan as $kelas){
                                    $kelas->peserta()->delete();
                                }
                            }

                            if($item->pendaftar and count($item->pendaftar) > 0){
                                foreach($item->pendaftar as $pendaftar){
                                    $pendaftar->seleksi()->delete();
                                    $pendaftar->penilaian_wawancara()->delete();
                                }
                            }
                            
                            if($item->pendaftar_mtu and count($item->pendaftar_mtu) > 0){
                                foreach($item->pendaftar_mtu as $pendaftar_mtu){
                                    $pendaftar_mtu->peserta()->delete();
                                }
                            }

                            $item->kelas_pelatihan()->delete();
                            $item->cbt_user_grup()->delete();
                            $item->pendaftar()->delete();
                            $item->pendaftar_mtu()->delete();

                        }
                    }
                    $program->jadwal()->delete();
                }
            }
            $find->program_pelatihan()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_tipe_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_tipe_pelatihan::orderByDesc('created_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_tipe_pelatihan;
            $datas[$key][] = ($value->aktif_m_tipe_pelatihan=='1') ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">TIDAK AKTIF</span>';
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_tipe_pelatihan.edit',['id_m_tipe_pelatihan' => $value->id_m_tipe_pelatihan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_tipe_pelatihan="'.$value->id_m_tipe_pelatihan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
