<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_soal_wawancara;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_soal_wawancara extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Soal Wawancara',
            'page_title' => 'Soal Wawancara',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Soal Wawancara',
        ];

        return view('back.m_soal_wawancara.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Soal Wawancara',
            'page_title' => 'Soal Wawancara',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Soal Wawancara',
        ];

    	return view('back.m_soal_wawancara.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'soal_wawancara.required' => 'harap diisi',
            'status.required' => 'pilih satu',
        ];

        $validator = Validator::make($request->all(), [
            'soal_wawancara' => ['required'],
            'status' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'soal_wawancara' => $errors->first('soal_wawancara'),
                    'status' => $errors->first('status'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_soal_wawancara;
        $object->id_m_soal_wawancara = M_soal_wawancara::MaxId();
        $object->soal_wawancara = $request->soal_wawancara;
        $object->status = $request->status;

        try{
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_soal_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_soal_wawancara') or !is_numeric(request('id_m_soal_wawancara')), 404);

        $old = M_soal_wawancara::where([
            'id_m_soal_wawancara' => request('id_m_soal_wawancara')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Soal Wawancara',
            'page_title' => 'Soal Wawancara',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Soal Wawancara',
            'old' => $old,
        ];

        return view('back.m_soal_wawancara.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'soal_wawancara.required' => 'harap diisi',
            'status.required' => 'pilih satu',
        ];

        $validator = Validator::make($request->all(), [
            'soal_wawancara' => ['required'],
            'status' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'soal_wawancara' => $errors->first('soal_wawancara'),
                    'status' => $errors->first('status'),
                ]
            ]);
        }

        DB::beginTransaction();
        $object = M_soal_wawancara::where('id_m_soal_wawancara', $request->id_m_soal_wawancara)->first();
        $object->soal_wawancara = $request->soal_wawancara;
        $object->status = $request->status;

        try{
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_soal_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_soal_wawancara')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_soal_wawancara::where([
            'id_m_soal_wawancara' => $request->id_m_soal_wawancara
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_soal_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_soal_wawancara::orderByDesc('id_m_soal_wawancara')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->soal_wawancara;
            $datas[$key][] = ($value->status=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            // $datas[$key][] = ($value->updated_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_soal_wawancara.edit',['id_m_soal_wawancara' => $value->id_m_soal_wawancara]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_soal_wawancara="'.$value->id_m_soal_wawancara.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
