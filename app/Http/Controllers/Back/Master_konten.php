<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_konten;
use App\Models\M_folder;
use App\Models\M_file;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_konten extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Konten Dinamis',
            'page_title' => 'Konten Dinamis',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Konten Dinamis',
        ];

        return view('back.m_konten.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Konten Dinamis',
            'page_title' => 'Konten Dinamis',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Konten Dinamis',
        ];

    	return view('back.m_konten.add')->with($data);
    }
    
    public function add_media()
    {
        $data = [
            'head_title' => 'Konten Dinamis',
            'page_title' => 'Konten Dinamis',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Konten Dinamis',
        ];

    	return view('back.m_konten.add_media')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'judul_m_konten.required' => 'harap diisi',
            'narasi_m_konten.required' => 'harap diisi',
            'aktif_m_konten.required' => 'please choose one',
            'photo.image' => 'harus format .jpg / .png',
            'photo2.image' => 'harus format .jpg / .png',
            'bg_m_konten.image' => 'harus format .jpg / .png',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_konten' => ['required'],
            'narasi_m_konten' => ['required'],
            'aktif_m_konten' => ['required'],
            'photo' => ['image', 'nullable'],
            'photo2' => ['image', 'nullable'],
            'bg_m_konten' => ['image', 'nullable'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_konten' => $errors->first('judul_m_konten'),
                'narasi_m_konten' => $errors->first('narasi_m_konten'),
                'aktif_m_konten' => $errors->first('aktif_m_konten'),
                'photo' => $errors->first('photo'),
                'photo2' => $errors->first('photo2'),
                'bg_m_konten' => $errors->first('bg_m_konten'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_konten;
        $object->id_m_konten = M_konten::MaxId();
        $object->judul_m_konten = $request->judul_m_konten;
        $object->narasi_m_konten = $request->narasi_m_konten;
        $object->slug_m_konten = Str::slug($request->judul_m_konten);
        $object->aktif_m_konten = $request->aktif_m_konten;
        $object->author_m_konten = session()->get('logged_in.id_m_user_bo');
        

        try{
            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }
            
            if($request->file('photo2')){
                $filename1 = time() . '_' . $request->file('photo2')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('photo2'), $filename1);
                $object->photo2 = $path2;
            }
            
            if($request->file('bg_m_konten')){
                $filename1 = time() . '_' . $request->file('bg_m_konten')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('bg_m_konten'), $filename1);
                $object->bg_m_konten = $path2;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_konten.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }
    
    
    public function save_media(Request $request)
    {
        $messages = [
            'nm_m_folder.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_folder' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_m_folder' => $errors->first('nm_m_folder'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_folder;
        $object->id_m_folder = M_folder::MaxId();
        $object->nm_m_folder = $request->nm_m_folder;

        try{
            $id_m_folder = $object->id_m_folder;
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Folder Baru Berhasil Dibuat',
                'id_m_folder' => $id_m_folder
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_konten') or !is_numeric(request('id_m_konten')), 404);

        $old = M_konten::where([
            'id_m_konten' => request('id_m_konten')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Konten Dinamis',
            'page_title' => 'Konten Dinamis',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Konten Dinamis',
            'old' => $old,
        ];

        return view('back.m_konten.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'judul_m_konten.required' => 'harap diisi',
            'narasi_m_konten.required' => 'harap diisi',
            'aktif_m_konten.required' => 'please choose one',
            'photo.image' => 'harus format .jpg / .png',
            'photo2.image' => 'harus format .jpg / .png',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_konten' => ['required'],
            'narasi_m_konten' => ['required'],
            'aktif_m_konten' => ['required'],
            'photo' => ['image', 'nullable'],
            'photo2' => ['image', 'nullable'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_konten' => $errors->first('judul_m_konten'),
                'narasi_m_konten' => $errors->first('narasi_m_konten'),
                'aktif_m_konten' => $errors->first('aktif_m_konten'),
                'photo' => $errors->first('photo'),
                'photo2' => $errors->first('photo2'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_konten::where('id_m_konten', $request->id_m_konten)->first();
        $object->judul_m_konten = $request->judul_m_konten;
        $object->aktif_m_konten = $request->aktif_m_konten;
        $object->narasi_m_konten = $request->narasi_m_konten;
        $object->slug_m_konten = Str::slug($request->judul_m_konten);

        try{
            if($request->file('photo')){
                Storage::delete('public/'.$object->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/slider';
                $f = $folder;
                $path1 = Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            if($request->file('photo2')){
                Storage::delete('public/'.$object->photo2);
                $filename1 = time() . '_' . $request->file('photo2')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('photo2'), $filename1);
                $object->photo2 = $path2;
            }
            
            if($request->file('bg_m_konten')){
                Storage::delete('public/'.$object->bg_m_konten);
                $filename1 = time() . '_' . $request->file('bg_m_konten')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('bg_m_konten'), $filename1);
                $object->bg_m_konten = $path2;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_konten.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_konten')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_konten::where([
            'id_m_konten' => $request->id_m_konten
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->photo);
            Storage::delete('public/'.$find->photo2);
            Storage::delete('public/'.$find->bg_m_konten);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_konten.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Terjadi masalah pada server, silahkan refresh browser Anda',
            'status'  => false,
        ]);
    }
    
    
    public function delete_file(Request $request)
    {
        if(!$request->filled('id_m_file')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_file::where([
            'id_m_file' => $request->id_m_file
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->upload_m_file);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'id_m_folder' => $request->id_m_folder,
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Terjadi masalah pada server, silahkan refresh browser Anda',
            'status'  => false,
        ]);
    }
    
    
    public function delete_folder(Request $request)
    {
        if(!$request->filled('id_m_folder')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_folder::where([
            'id_m_folder' => $request->id_m_folder
        ])->with('file')->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            foreach($find->file as $file){
                Storage::delete('public/'.$file->upload_m_file);
            }
            $find->file()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_konten::orderByDesc('id_m_konten')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->judul_m_konten;
            $datas[$key][] = 'konten/'.$value->slug_m_konten;
            $datas[$key][] = ($value->aktif_m_konten=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = ($value->updated_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_konten.edit',['id_m_konten' => $value->id_m_konten]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_konten="'.$value->id_m_konten.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
    
    
    public function datatable_media(Request $request)
    {
        $table = M_folder::orderByDesc('id_m_folder')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_folder;
            $datas[$key][] = ($value->created_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item edit" href="javascript:void(0)" data-id_m_folder="'.$value->id_m_folder.'">kelola media</a>
                                        <a class="dropdown-item delete_folder" data-id_m_folder="'.$value->id_m_folder.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }



    public function load_folder_media(Request $request)
    {
        $id_m_folder = $request->id_m_folder;
        if(empty($id_m_folder)){
            return response()->json([
                'status' => false,
                'message' => 'invalid parameter',
            ]);
        }

        $folder = M_file::where('id_m_folder', $id_m_folder)->orderByDesc('created_at')->get();
        $data = [
            'status' => true,
            'folder' => $folder,
            'id_m_folder' => $id_m_folder,
        ];
        return view('back.m_konten.load_folder_media')->with($data);
        
    }


    public function edit_media(Request $request)
    {
        $old = M_folder::where('id_m_folder', $request->id_m_folder)->with('file')->first();
        $data = [
            'old' => $old,
        ];

    	return view('back.m_konten.edit_media')->with($data);
    }

    public function save_file(Request $request)
    {
        $messages = [
            'upload_m_file.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'upload_m_file' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'upload_m_file' => $errors->first('upload_m_file'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = new M_file;
        $object->id_m_file = M_file::MaxId();
        $object->id_m_folder = $request->id_m_folder;
        try{
            if($request->file('upload_m_file')){
                Storage::delete('public/'.$object->upload_m_file);
                $filename1 = time() . '_' . $request->file('upload_m_file')->getClientOriginalName();
                $folder = 'upload/media';
                $f = $folder;
                $path1 = Storage::disk('public')->putFileAs($f, $request->file('upload_m_file'), $filename1);
                $object->upload_m_file = $path1;
            }
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'id_m_folder' => $request->id_m_folder,
                'message' => 'Upload media berhasil',
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }
}
