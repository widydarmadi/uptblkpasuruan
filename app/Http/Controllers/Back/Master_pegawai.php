<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_pegawai;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_pegawai extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Pegawai',
            'page_title' => 'Pegawai',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Pegawai',
        ];

        return view('back.m_pegawai.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Pegawai',
            'page_title' => 'Pegawai',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Pegawai',
        ];

    	return view('back.m_pegawai.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_pegawai.required' => 'harap diisi',
            'jabatan_m_pegawai.required' => 'harap diisi',
            'order_tampil.required' => 'harap diisi',
            'pangkat_m_pegawai.required' => 'harap diisi',
            'golongan_m_pegawai.required' => 'harap diisi',
            'aktif_m_pegawai.required' => 'pilih satu',
            'unit_kerja_m_pegawai.required' => 'pilih satu',
            'kategori.required' => 'pilih satu',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_pegawai' => ['required'],
            'jabatan_m_pegawai' => ['required'],
            'order_tampil' => ['required'],
            'pangkat_m_pegawai' => ['required'],
            'golongan_m_pegawai' => ['required'],
            'aktif_m_pegawai' => ['required'],
            'unit_kerja_m_pegawai' => ['required'],
            'kategori' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_m_pegawai' => $errors->first('nm_m_pegawai'),
                'jabatan_m_pegawai' => $errors->first('jabatan_m_pegawai'),
                'order_tampil' => $errors->first('order_tampil'),
                'pangkat_m_pegawai' => $errors->first('pangkat_m_pegawai'),
                'golongan_m_pegawai' => $errors->first('golongan_m_pegawai'),
                'aktif_m_pegawai' => $errors->first('aktif_m_pegawai'),
                'unit_kerja_m_pegawai' => $errors->first('unit_kerja_m_pegawai'),
                'kategori' => $errors->first('kategori'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_pegawai;
        $object->id_m_pegawai = M_pegawai::MaxId();
        $object->nm_m_pegawai = $request->nm_m_pegawai;
        $object->jabatan_m_pegawai = $request->jabatan_m_pegawai;
        $object->order_tampil = $request->order_tampil;
        $object->pangkat_m_pegawai = $request->pangkat_m_pegawai;
        $object->golongan_m_pegawai = $request->golongan_m_pegawai;
        $object->detail_jabatan_m_pegawai = $request->detail_jabatan_m_pegawai;
        $object->kompetensi_m_pegawai = $request->kompetensi_m_pegawai;
        $object->aktif_m_pegawai = $request->aktif_m_pegawai;
        $object->unit_kerja_m_pegawai = $request->unit_kerja_m_pegawai;
        $object->kategori = $request->kategori;

        try{
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_pegawai.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_pegawai') or !is_numeric(request('id_m_pegawai')), 404);

        $old = M_pegawai::where([
            'id_m_pegawai' => request('id_m_pegawai')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Pegawai',
            'page_title' => 'Pegawai',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Pegawai',
            'old' => $old,
        ];

        return view('back.m_pegawai.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_pegawai.required' => 'harap diisi',
            'jabatan_m_pegawai.required' => 'harap diisi',
            'order_tampil.required' => 'harap diisi',
            'pangkat_m_pegawai.required' => 'harap diisi',
            'golongan_m_pegawai.required' => 'harap diisi',
            'aktif_m_pegawai.required' => 'pilih satu',
            'kategori.required' => 'pilih satu',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_pegawai' => ['required'],
            'jabatan_m_pegawai' => ['required'],
            'order_tampil' => ['required'],
            'pangkat_m_pegawai' => ['required'],
            'golongan_m_pegawai' => ['required'],
            'aktif_m_pegawai' => ['required'],
            'kategori' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_m_pegawai' => $errors->first('nm_m_pegawai'),
                'jabatan_m_pegawai' => $errors->first('jabatan_m_pegawai'),
                'order_tampil' => $errors->first('order_tampil'),
                'pangkat_m_pegawai' => $errors->first('pangkat_m_pegawai'),
                'golongan_m_pegawai' => $errors->first('golongan_m_pegawai'),
                'aktif_m_pegawai' => $errors->first('aktif_m_pegawai'),
                'kategori' => $errors->first('kategori'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_pegawai::where('id_m_pegawai', $request->id_m_pegawai)->first();
        $object->nm_m_pegawai = $request->nm_m_pegawai;
        $object->jabatan_m_pegawai = $request->jabatan_m_pegawai;
        $object->order_tampil = $request->order_tampil;
        $object->pangkat_m_pegawai = $request->pangkat_m_pegawai;
        $object->golongan_m_pegawai = $request->golongan_m_pegawai;
        $object->detail_jabatan_m_pegawai = $request->detail_jabatan_m_pegawai;
        $object->kompetensi_m_pegawai = $request->kompetensi_m_pegawai;
        $object->aktif_m_pegawai = $request->aktif_m_pegawai;
        $object->unit_kerja_m_pegawai = $request->unit_kerja_m_pegawai;
        $object->kategori = $request->kategori;
        try{
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_pegawai.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_pegawai')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_pegawai::where([
            'id_m_pegawai' => $request->id_m_pegawai
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_pegawai.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_pegawai::orderByDesc('id_m_pegawai')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_pegawai;
            $datas[$key][] = $value->kategori;
            $datas[$key][] = $value->jabatan_m_pegawai;
            $datas[$key][] = $value->pangkat_m_pegawai . ' ( ' .$value->golongan_m_pegawai . ' )';
            $datas[$key][] = $value->order_tampil;
            $datas[$key][] = ($value->aktif_m_pegawai=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_pegawai.edit',['id_m_pegawai' => $value->id_m_pegawai]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_pegawai="'.$value->id_m_pegawai.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
