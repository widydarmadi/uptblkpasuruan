<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_testimoni;
use App\Models\M_sumber_dana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_testimoni extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Testimoni',
            'page_title' => 'Testimoni',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Testimoni',
        ];

        return view('back.m_testimoni.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Testimoni',
            'page_title' => 'Testimoni',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Testimoni',
        ];

    	return view('back.m_testimoni.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_pengirim.required' => 'harap diisi',
            'isi_m_testimoni.required' => 'harap diisi',
            'jk.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_pengirim' => ['required'],
            'isi_m_testimoni' => ['required'],
            'jk' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_pengirim' => $errors->first('nm_pengirim'),
                    'isi_m_testimoni' => $errors->first('isi_m_testimoni'),
                    'jk' => $errors->first('jk'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_testimoni;
        $object->id_m_testimoni = M_testimoni::MaxId();
        $object->nm_pengirim = $request->nm_pengirim;
        $object->aktif_m_testimoni = $request->aktif_m_testimoni;
        $object->isi_m_testimoni = $request->isi_m_testimoni;
        $object->jk = $request->jk;

        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_testimoni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_testimoni') or !is_numeric(request('id_m_testimoni')), 404);

        $old = M_testimoni::where([
            'id_m_testimoni' => request('id_m_testimoni')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Testimoni',
            'page_title' => 'Testimoni',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Testimoni',
            'old' => $old,
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
        ];

        return view('back.m_testimoni.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_pengirim.required' => 'harap diisi',
            'isi_m_testimoni.required' => 'harap diisi',
            'jk.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_pengirim' => ['required'],
            'isi_m_testimoni' => ['required'],
            'jk' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_pengirim' => $errors->first('nm_pengirim'),
                    'isi_m_testimoni' => $errors->first('isi_m_testimoni'),
                    'jk' => $errors->first('jk'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_testimoni::where([
            'id_m_testimoni' => $request->id_m_testimoni,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_pengirim' => 'Data not found !',
                ]
            ]);
        }

        $update->nm_pengirim = $request->nm_pengirim;
        $update->aktif_m_testimoni = $request->aktif_m_testimoni;
        $update->isi_m_testimoni = $request->isi_m_testimoni;
        $update->jk = $request->jk;
        try{
          
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_testimoni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_testimoni')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_testimoni::where([
            'id_m_testimoni' => $request->id_m_testimoni
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->logo);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_testimoni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_testimoni::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_pengirim;
            $datas[$key][] = $value->jk;
            $datas[$key][] = $value->isi_m_testimoni;
            $datas[$key][] = ($value->aktif_m_testimoni=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_testimoni.edit',['id_m_testimoni' => $value->id_m_testimoni]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_testimoni="'.$value->id_m_testimoni.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
