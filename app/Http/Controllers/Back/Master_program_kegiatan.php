<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_program_kegiatan;
use App\Models\M_sumber_dana;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_program_kegiatan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Program Kegiatan',
            'page_title' => 'Program Kegiatan',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Program Kegiatan',
        ];

        return view('back.m_program_kegiatan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Program Kegiatan',
            'page_title' => 'Program Kegiatan',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Program Kegiatan',
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
        ];

    	return view('back.m_program_kegiatan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_program_kegiatan.required' => 'harap diisi',
            'id_m_sumber_dana.required' => 'pilih satu',
            'tgl_mulai.required' => 'harap diisi',
            'tgl_selesai.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_program_kegiatan' => ['required'],
            'id_m_sumber_dana' => ['required'],
            'tgl_mulai' => ['required'],
            'tgl_selesai' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_program_kegiatan' => $errors->first('nm_m_program_kegiatan'),
                    'id_m_sumber_dana' => $errors->first('id_m_sumber_dana'),
                    'tgl_mulai' => $errors->first('tgl_mulai'),
                    'tgl_selesai' => $errors->first('tgl_selesai'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_program_kegiatan;
        $object->id_m_program_kegiatan = M_program_kegiatan::MaxId();
        $object->nm_m_program_kegiatan = $request->nm_m_program_kegiatan;
        $object->id_m_sumber_dana = $request->id_m_sumber_dana;
        $object->tgl_mulai = \Carbon\Carbon::parse($request->tgl_mulai)->format('Y-m-d');
        $object->tgl_selesai = \Carbon\Carbon::parse($request->tgl_selesai)->format('Y-m-d');

        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_program_kegiatan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_program_kegiatan') or !is_numeric(request('id_m_program_kegiatan')), 404);

        $old = M_program_kegiatan::where([
            'id_m_program_kegiatan' => request('id_m_program_kegiatan')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Program Kegiatan',
            'page_title' => 'Program Kegiatan',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Program Kegiatan',
            'old' => $old,
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
        ];

        return view('back.m_program_kegiatan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_program_kegiatan.required' => 'harap diisi',
            'id_m_sumber_dana.required' => 'pilih satu',
            'tgl_mulai.required' => 'harap diisi',
            'tgl_selesai.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_program_kegiatan' => ['required'],
            'id_m_sumber_dana' => ['required'],
            'tgl_mulai' => ['required'],
            'tgl_selesai' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_program_kegiatan' => $errors->first('nm_m_program_kegiatan'),
                    'id_m_sumber_dana' => $errors->first('id_m_sumber_dana'),
                    'tgl_mulai' => $errors->first('tgl_mulai'),
                    'tgl_selesai' => $errors->first('tgl_selesai'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_program_kegiatan::where([
            'id_m_program_kegiatan' => $request->id_m_program_kegiatan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_program_kegiatan' => 'Data not found !',
                ]
            ]);
        }

        $update->nm_m_program_kegiatan = $request->nm_m_program_kegiatan;
        $update->id_m_sumber_dana = $request->id_m_sumber_dana;
        $update->tgl_mulai = \Carbon\Carbon::parse($request->tgl_mulai)->format('Y-m-d');
        $update->tgl_selesai = \Carbon\Carbon::parse($request->tgl_selesai)->format('Y-m-d');
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_program_kegiatan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_program_kegiatan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_program_kegiatan::where([
            'id_m_program_kegiatan' => $request->id_m_program_kegiatan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_program_kegiatan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_program_kegiatan::with('sumber_dana')->orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_program_kegiatan;
            $datas[$key][] = \Carbon\Carbon::parse($value->tgl_mulai)->format('d-m-Y');
            $datas[$key][] = \Carbon\Carbon::parse($value->tgl_selesai)->format('d-m-Y');
            $datas[$key][] = $value->sumber_dana->nm_m_sumber_dana;
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_program_kegiatan.edit',['id_m_program_kegiatan' => $value->id_m_program_kegiatan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_program_kegiatan="'.$value->id_m_program_kegiatan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
