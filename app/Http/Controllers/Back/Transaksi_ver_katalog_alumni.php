<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_bidang_usaha;
use App\Models\M_kecamatan;
use App\Models\M_kota;
use App\Models\M_provinsi;
use App\Models\T_usaha;
use App\Models\t_ver_katalog_alumni;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Transaksi_ver_katalog_alumni extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Verifikasi Katalog Alumni',
            'page_title' => 'Verifikasi Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Verifikasi Katalog Alumni',
        ];

        return view('back.t_ver_katalog_alumni.index')->with($data);
    }

   
    public function view()
    {
        abort_if(!request()->filled('id_t_usaha') or !is_numeric(request('id_t_usaha')), 404);
        $old = T_usaha::where([
            'id_t_usaha' => request()->get('id_t_usaha')
        ])->firstOrFail();

        $list_kec = M_kecamatan::where('id_m_kota', $old->id_m_kota)->orderBy('nm_m_kecamatan')->get();
        $list_kab = M_kota::where('id_m_provinsi', $old->id_m_provinsi)->orderBy('nm_m_kota')->get();
        $list_prov = M_provinsi::orderBy('nm_m_provinsi')->get();


        $data = [
            'head_title' => 'Verifikasi Katalog Alumni',
            'page_title' => 'Verifikasi Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Verifikasi Katalog Alumni',
            'old' => $old,
            'bidang_usaha' => M_bidang_usaha::where('aktif', '1')->get(),
            'id_m_provinsi' => $old->id_m_provinsi,
            'list_prov' => $list_prov,
            'list_kab' => $list_kab,
            'list_kec' => $list_kec,
            
        ];

        return view('back.t_ver_katalog_alumni.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'tipe_t_usaha.required' => 'harap diisi',
            'kategori_t_usaha.required' => 'harap diisi',
            'nm_t_usaha.required' => 'harap diisi',
            'is_tampil.required' => 'harap diisi',
            'id_m_bidang_usaha.required' => 'harap diisi',
            'alamat_t_usaha.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
            'email_t_usaha.email' => 'alamat email tidak valid',
            'foto_t_usaha.required' => 'pilih gambar (JPG atau PNG)',
            'foto_t_usaha.image' => 'format foto harus berupa gambar (JPG atau PNG)',
        ];

        $validator = Validator::make($request->all(), [
            'is_tampil' => ['required'],
            'tipe_t_usaha' => ['required'],
            'kategori_t_usaha' => ['required'],
            'nm_t_usaha' => ['required'],
            'id_m_bidang_usaha' => ['required'],
            'alamat_t_usaha' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'id_m_kecamatan' => ['required'],
            'email_t_usaha' => ['nullable', 'email'],
            'foto_t_usaha' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'is_tampil' => $errors->first('is_tampil'),
                    'tipe_t_usaha' => $errors->first('tipe_t_usaha'),
                    'kategori_t_usaha' => $errors->first('kategori_t_usaha'),
                    'nm_t_usaha' => $errors->first('nm_t_usaha'),
                    'id_m_bidang_usaha' => $errors->first('id_m_bidang_usaha'),
                    'alamat_t_usaha' => $errors->first('alamat_t_usaha'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                    'email_t_usaha' => $errors->first('email_t_usaha'),
                    'foto_t_usaha' => $errors->first('foto_t_usaha'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = T_usaha::where('id_t_usaha', $request->id_t_usaha)->first();
        $object->id_t_usaha = T_usaha::MaxId();
        $object->is_tampil = $request->is_tampil;
        $object->tipe_t_usaha = $request->tipe_t_usaha;
        $object->kategori_t_usaha = $request->kategori_t_usaha;
        $object->nm_t_usaha = $request->nm_t_usaha;
        $object->alamat_t_usaha = $request->alamat_t_usaha;
        $object->telp_t_usaha = $request->telp_t_usaha;
        $object->email_t_usaha = $request->email_t_usaha;
        $object->ig_t_usaha = $request->ig_t_usaha;
        $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_kota = $request->id_m_kota;
        $object->id_m_provinsi = $request->id_m_provinsi;
        $object->id_m_bidang_usaha = $request->id_m_bidang_usaha;

        try{
            if($request->file('foto_t_usaha')){
                Storage::delete('public/'.$object->foto_t_usaha);
                $filename1 = time() . '_' . $request->file('foto_t_usaha')->getClientOriginalName();
                $folder = 'upload/katalog';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('foto_t_usaha'), $filename1);
                $object->foto_t_usaha = $path1;
            }
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data usaha berhasil disimpan',
                'redirect' => route('admin.t_ver_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function reset_foto(Request $request)
    {
        $find = T_usaha::where([
            'id_t_usaha' => $request->id_t_usaha
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        try{
            \Storage::delete('public/'.$find->foto_t_usaha);
            $find->foto_t_usaha = null;
            $find->save();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.t_ver_katalog_alumni.edit', ['id_t_usaha' => $request->edit]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_t_usaha')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_usaha::where([
            'id_t_usaha' => $request->id_t_usaha
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->foto_t_usaha);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.t_ver_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable()
    {
        $table = T_usaha::with(['bidang','alumni'])->orderByDesc('created_at')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = ($value->alumni) ? $value->alumni->nm_m_alumni : null;
            $datas[$key][] = $value->nm_t_usaha;
            $datas[$key][] = $value->tipe_t_usaha;
            $datas[$key][] = $value->kategori_t_usaha;
            $datas[$key][] = $value->bidang->nm_m_bidang_usaha;
            $datas[$key][] = $value->ig_t_usaha;
            $datas[$key][] = ($value->is_tampil=='YA') ? '<span class="badge bg-success">YA</span>' : '<span class="badge bg-danger">Tidak</span>';
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.t_ver_katalog_alumni.view',['id_t_usaha' => $value->id_t_usaha]).'">tampilkan</a>
                                <a class="dropdown-item delete" data-id_t_usaha="'.$value->id_t_usaha.'" href="#">hapus</a>
                                </div>
                            </div>';
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
