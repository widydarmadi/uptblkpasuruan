<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_program_pelatihan;
use App\Models\M_sumber_dana;
use App\Models\M_tipe_pelatihan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_program_pelatihan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Program Pelatihan',
            'page_title' => 'Program Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Program Pelatihan',
        ];

        return view('back.m_program_pelatihan.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Program Pelatihan',
            'page_title' => 'Program Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Program Pelatihan',
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
            'kategori_kejuruan' => M_kategori_kejuruan::where('aktif', '1')->orderBy('nm_m_kategori_kejuruan')->get(),
            'tipe_pelatihan' => M_tipe_pelatihan::where('aktif_m_tipe_pelatihan', '1')->orderBy('nm_m_tipe_pelatihan')->get(),
        ];

    	return view('back.m_program_pelatihan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'id_m_kejuruan.required' => 'harap diisi',
            'nm_m_program_pelatihan.required' => 'harap diisi',
            'deskripsi.required' => 'harap diisi',
            'aktif.required' => 'harap diisi',
            'id_m_sumber_dana.required' => 'harap diisi',
            'id_m_tipe_pelatihan.required' => 'harap diisi',
            'foto.required' => 'harap diisi',
            'foto.image' => 'format file tidak diizinkan',
            'file_pdf.mimes' => 'format file tidak diizinkan',
        ];

        $validator = Validator::make($request->all(), [
            'id_m_kejuruan' => ['required'],
            'nm_m_program_pelatihan' => ['required'],
            'deskripsi' => ['required'],
            'aktif' => ['required'],
            'id_m_sumber_dana' => ['required'],
            'id_m_tipe_pelatihan' => ['required'],
            'foto' => ['required', 'image'],
            'file_pdf' => ['nullable', 'mimes:pdf'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_kejuruan' => $errors->first('id_m_kejuruan'),
                    'nm_m_program_pelatihan' => $errors->first('nm_m_program_pelatihan'),
                    'deskripsi' => $errors->first('deskripsi'),
                    'aktif' => $errors->first('aktif'),
                    'id_m_sumber_dana' => $errors->first('id_m_sumber_dana'),
                    'id_m_tipe_pelatihan' => $errors->first('id_m_tipe_pelatihan'),
                    'foto' => $errors->first('foto'),
                    'file_pdf' => $errors->first('file_pdf'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_program_pelatihan;
        $object->id_m_program_pelatihan = M_program_pelatihan::MaxId();
        $object->id_m_kejuruan = $request->id_m_kejuruan;
        $object->nm_m_program_pelatihan = $request->nm_m_program_pelatihan;
        $object->deskripsi = $request->deskripsi;
        $object->id_m_sumber_dana = $request->id_m_sumber_dana;
        $object->id_m_tipe_pelatihan = $request->id_m_tipe_pelatihan;
        $object->jampel = $request->jampel;
        $object->aktif = $request->aktif;
        try{

            if($request->file('foto')){
                $filename1 = time() . '_LAMP_DOC_' . $request->file('foto')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('foto'), $filename1);
                $object->foto = $path2;
            }

            if($request->file('file_pdf')){
                $filename1 = time() . '_LAMP_DOC_' . $request->file('file_pdf')->getClientOriginalName();
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('file_pdf'), $filename1);
                $object->file_pdf = $path2;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_program_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_program_pelatihan') or !is_numeric(request('id_m_program_pelatihan')), 404);

        $old = m_program_pelatihan::where([
            'id_m_program_pelatihan' => request('id_m_program_pelatihan')
        ])->firstOrFail();

        $selected_kategori_kejuruan = M_kejuruan::where('id_m_kejuruan', $old->id_m_kejuruan)->first()->id_m_kategori_kejuruan;
        $list_sub_kejuruan = M_kejuruan::select('id_m_kejuruan', 'nm_m_kejuruan')->where('id_m_kategori_kejuruan', $selected_kategori_kejuruan)->get();
        $data = [
            'head_title' => 'Program Pelatihan',
            'page_title' => 'Program Pelatihan',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Program Pelatihan',
            'old' => $old,
            'sumber_dana' => M_sumber_dana::where('aktif_m_sumber_dana', '1')->orderBy('nm_m_sumber_dana')->get(),
            'kategori_kejuruan' => M_kategori_kejuruan::where('aktif', '1')->orderBy('nm_m_kategori_kejuruan')->get(),
            'tipe_pelatihan' => M_tipe_pelatihan::where('aktif_m_tipe_pelatihan', '1')->orderBy('nm_m_tipe_pelatihan')->get(),
            'selected_kategori_kejuruan' => $selected_kategori_kejuruan,
            'list_sub_kejuruan' => $list_sub_kejuruan,
        ];

        return view('back.m_program_pelatihan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'id_m_kejuruan.required' => 'harap diisi',
            'nm_m_program_pelatihan.required' => 'harap diisi',
            'deskripsi.required' => 'harap diisi',
            'aktif.required' => 'harap diisi',
            'id_m_sumber_dana.required' => 'harap diisi',
            'id_m_tipe_kejuruan.required' => 'harap diisi',
            'foto.image' => 'format file tidak diizinkan',
            'foto.required' => 'harap diisi',
            'file_pdf.mimes' => 'format file tidak diizinkan',
        ];

        $cek_foto = M_program_pelatihan::select('foto')->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)->first();
        if(isset($cek_foto->foto)){
            $state_foto = ['nullable', 'image'];
        }else{
            $state_foto = ['required', 'image'];
        }

        $validator = Validator::make($request->all(), [
            'id_m_kejuruan' => ['required'],
            'nm_m_program_pelatihan' => ['required'],
            'deskripsi' => ['required'],
            'aktif' => ['required'],
            'id_m_sumber_dana' => ['required'],
            'id_m_tipe_pelatihan' => ['required'],
            'foto' => $state_foto,
            'file_pdf' => ['nullable', 'mimes:pdf'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_kejuruan' => $errors->first('id_m_kejuruan'),
                    'nm_m_program_pelatihan' => $errors->first('nm_m_program_pelatihan'),
                    'deskripsi' => $errors->first('deskripsi'),
                    'aktif' => $errors->first('aktif'),
                    'id_m_sumber_dana' => $errors->first('id_m_sumber_dana'),
                    'id_m_tipe_pelatihan' => $errors->first('id_m_tipe_pelatihan'),
                    'foto' => $errors->first('foto'),
                    'file_pdf' => $errors->first('file_pdf'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = m_program_pelatihan::where([
            'id_m_program_pelatihan' => $request->id_m_program_pelatihan,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_program_pelatihan' => 'Data not found !',
                ]
            ]);
        }

        $update->id_m_kejuruan = $request->id_m_kejuruan;
        $update->nm_m_program_pelatihan = $request->nm_m_program_pelatihan;
        $update->deskripsi = $request->deskripsi;
        $update->id_m_sumber_dana = $request->id_m_sumber_dana;
        $update->id_m_tipe_pelatihan = $request->id_m_tipe_pelatihan;
        $update->jampel = $request->jampel;
        $update->aktif = $request->aktif;
        try{

            if($request->file('foto')){
                $filename1 = time() . '_LAMP_DOC_' . $request->file('foto')->getClientOriginalName();
                \Storage::delete('public/'.$update->foto);
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('foto'), $filename1);
                $update->foto = $path2;
            }

            if($request->file('file_pdf')){
                $filename1 = time() . '_LAMP_DOC_' . $request->file('file_pdf')->getClientOriginalName();
                \Storage::delete('public/'.$update->file_pdf);
                $folder = 'upload/content';
                $f = $folder;
                $path2 = \Storage::disk('public')->putFileAs($f, $request->file('file_pdf'), $filename1);
                $update->file_pdf = $path2;
            }


            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_program_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function reset(Request $request)
    {
        $find = M_program_pelatihan::where([
            'id_m_program_pelatihan' => $request->id_m_program_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        try{
            \Storage::delete('public/'.$find->foto);
            $find->foto = null;
            $find->save();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_program_pelatihan.edit', ['id_m_program_pelatihan' => $request->edit]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }
    
    
    public function reset_pdf(Request $request)
    {
        $find = M_program_pelatihan::where([
            'id_m_program_pelatihan' => $request->id_m_program_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        try{
            \Storage::delete('public/'.$find->file_pdf);
            $find->file_pdf = null;
            $find->save();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_program_pelatihan.edit', ['id_m_program_pelatihan' => $request->edit]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_program_pelatihan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_program_pelatihan::where([
            'id_m_program_pelatihan' => $request->id_m_program_pelatihan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->foto);
            \Storage::delete('public/'.$find->file_pdf);
            if($find->jadwal and count($find->jadwal) > 0){
                foreach($find->jadwal as $item){
                    if($item->kelas_pelatihan and count($item->kelas_pelatihan) > 0){
                        foreach($item->kelas_pelatihan as $kelas){
                            $kelas->peserta()->delete();
                        }
                    }

                    if($item->pendaftar and count($item->pendaftar) > 0){
                        foreach($item->pendaftar as $pendaftar){
                            $pendaftar->seleksi()->delete();
                            $pendaftar->penilaian_wawancara()->delete();
                        }
                    }
                    
                    if($item->pendaftar_mtu and count($item->pendaftar_mtu) > 0){
                        foreach($item->pendaftar_mtu as $pendaftar_mtu){
                            $pendaftar_mtu->peserta()->delete();
                        }
                    }

                    $item->kelas_pelatihan()->delete();
                    $item->cbt_user_grup()->delete();
                    // $item->pendaftar()->delete();
                    $item->pendaftar_mtu()->delete();

                }
            }
            $find->jadwal()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_program_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_program_pelatihan::with('sumber_dana', 'kejuruan', 'tipe_pelatihan')->orderByDesc('created_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_program_pelatihan;
            $datas[$key][] = $value->id_m_kejuruan ? $value->kejuruan->nm_m_kejuruan : '';
            $datas[$key][] = $value->id_m_tipe_pelatihan ? $value->tipe_pelatihan->nm_m_tipe_pelatihan : '';
            $datas[$key][] = $value->jampel;
            $datas[$key][] = $value->id_m_sumber_dana ? $value->sumber_dana->nm_m_sumber_dana : '';
            $datas[$key][] = ($value->aktif=='1') ? '<span class="badge bg-success">AKTIF</span>' : '<span class="badge bg-danger">TIDAK AKTIF</span>';
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_program_pelatihan.edit',['id_m_program_pelatihan' => $value->id_m_program_pelatihan]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_program_pelatihan="'.$value->id_m_program_pelatihan.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
