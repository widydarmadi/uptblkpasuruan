<?php

namespace App\Http\Controllers\Back;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_reformasi_birokrasi;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Master_reformasi_birokrasi extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Reformasi Birokrasi',
            'page_title' => 'Reformasi Birokrasi',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Reformasi Birokrasi',
        ];

        return view('back.m_reformasi_birokrasi.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Reformasi Birokrasi',
            'page_title' => 'Reformasi Birokrasi',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Reformasi Birokrasi',
        ];

    	return view('back.m_reformasi_birokrasi.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'judul_m_reformasi_birokrasi.required' => 'harap diisi',
            'isi_m_reformasi_birokrasi.required' => 'harap diisi',
            'aktif_m_reformasi_birokrasi.required' => 'pilih satu',
            'photo.required' => 'pilih file gambar',
            'photo.image' => 'format file tidak diizinkan',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_reformasi_birokrasi' => ['required'],
            'isi_m_reformasi_birokrasi' => ['required'],
            'aktif_m_reformasi_birokrasi' => ['required'],
            'photo' => ['required', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_reformasi_birokrasi' => $errors->first('judul_m_reformasi_birokrasi'),
                'isi_m_reformasi_birokrasi' => $errors->first('isi_m_reformasi_birokrasi'),
                'aktif_m_reformasi_birokrasi' => $errors->first('aktif_m_reformasi_birokrasi'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_reformasi_birokrasi;
        $object->id_m_reformasi_birokrasi = M_reformasi_birokrasi::MaxId();
        $object->judul_m_reformasi_birokrasi = $request->judul_m_reformasi_birokrasi;
        $object->isi_m_reformasi_birokrasi = $request->isi_m_reformasi_birokrasi;
        $object->slug_m_reformasi_birokrasi = time().'-'.Str::slug($request->judul_m_reformasi_birokrasi);
        $object->aktif_m_reformasi_birokrasi = $request->aktif_m_reformasi_birokrasi;
        

        try{
            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/rb';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }
           
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_reformasi_birokrasi.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_reformasi_birokrasi') or !is_numeric(request('id_m_reformasi_birokrasi')), 404);

        $old = M_reformasi_birokrasi::where([
            'id_m_reformasi_birokrasi' => request('id_m_reformasi_birokrasi')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Reformasi Birokrasi',
            'page_title' => 'Reformasi Birokrasi',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Reformasi Birokrasi',
            'old' => $old,
        ];

        return view('back.m_reformasi_birokrasi.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'judul_m_reformasi_birokrasi.required' => 'harap diisi',
            'isi_m_reformasi_birokrasi.required' => 'harap diisi',
            'aktif_m_reformasi_birokrasi.required' => 'pilih satu',
            'photo.image' => 'format file tidak diizinkan',
        ];

        $validator = Validator::make($request->all(), [
            'judul_m_reformasi_birokrasi' => ['required'],
            'isi_m_reformasi_birokrasi' => ['required'],
            'aktif_m_reformasi_birokrasi' => ['required'],
            'photo' => ['nullable', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'judul_m_reformasi_birokrasi' => $errors->first('judul_m_reformasi_birokrasi'),
                'isi_m_reformasi_birokrasi' => $errors->first('isi_m_reformasi_birokrasi'),
                'aktif_m_reformasi_birokrasi' => $errors->first('aktif_m_reformasi_birokrasi'),
                'photo' => $errors->first('photo'),
            ]
            ]);
        }

        DB::beginTransaction();
        $object = M_reformasi_birokrasi::where('id_m_reformasi_birokrasi', $request->id_m_reformasi_birokrasi)->first();
        $object->judul_m_reformasi_birokrasi = $request->judul_m_reformasi_birokrasi;
        $object->isi_m_reformasi_birokrasi = $request->isi_m_reformasi_birokrasi;
        $object->slug_m_reformasi_birokrasi = time().'-'.Str::slug($request->judul_m_reformasi_birokrasi);
        $object->aktif_m_reformasi_birokrasi = $request->aktif_m_reformasi_birokrasi;

        try{
            if($request->file('photo')){
                Storage::delete('public/'.$object->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/rb';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }


            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_reformasi_birokrasi.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_reformasi_birokrasi')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_reformasi_birokrasi::where([
            'id_m_reformasi_birokrasi' => $request->id_m_reformasi_birokrasi
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->photo);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_reformasi_birokrasi.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => $e->getMessage(),
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = M_reformasi_birokrasi::orderByDesc('id_m_reformasi_birokrasi')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->judul_m_reformasi_birokrasi;
            $datas[$key][] = ($value->aktif_m_reformasi_birokrasi=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Tidak Aktif</span>';
            $datas[$key][] = \Carbon\Carbon::parse($value->updated_at)->format('d-m-Y H:i:s');
            // $datas[$key][] = ($value->updated_at) ? \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->updated_at)->format('d-m-Y H:i:s') : '-';
            $datas[$key][] = '<div class="btn-group">
                                    <button type="button" class="btn btn-primary btn-sm">Action</button>
                                    <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                    <span class="sr-only">Toggle Dropdown</span>
                                    </button>
                                    <div class="dropdown-menu" role="menu" style="">
                                        <a class="dropdown-item" href="'.route('admin.m_reformasi_birokrasi.edit',['id_m_reformasi_birokrasi' => $value->id_m_reformasi_birokrasi]).'">edit</a>
                                        <a class="dropdown-item delete" data-id_m_reformasi_birokrasi="'.$value->id_m_reformasi_birokrasi.'" href="#">hapus</a>
                                    </div>
                                </div>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
