<?php

namespace App\Http\Controllers\Back;
use Illuminate\Support\Facades\DB;
use App\Models\M_user_group;
use App\Models\M_menu_front;
use App\Models\M_hak_akses;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;

class Master_menu_front extends Controller
{

    public function index()
    {
        $data = [
            'head_title' => 'Menu Website',
            'page_title' => 'Menu Website',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Menu Website',
        ];

        return view('back.m_menu_front.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Menu Website',
            'page_title' => 'Menu Website',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Menu Website',
            'id_parent'   => M_menu_front::whereNull('id_parent')->where('aktif','1')->get(),
        ];

    	return view('back.m_menu_front.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_menu.required' => 'harap diisi',
            'order_m_menu.numeric' => 'please fill number only',
        ];

        $validator = Validator::make($request->all(), [
            'nm_menu' => ['required'],
            'order_m_menu' => ['numeric'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_menu' => $errors->first('nm_menu'),
                    'order_m_menu' => $errors->first('order_m_menu'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_menu_front;
        $object->id_m_menu = M_menu_front::MaxId();
        $object->aktif = $request->aktif;
        $object->nm_menu = $request->nm_menu;
        $object->order_m_menu = $request->order_m_menu;
        $object->id_parent = $request->id_parent;
        $object->url = $request->url;
        $object->order_m_menu = $request->order_m_menu;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_menu_front.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_menu') or !is_numeric(request('id_m_menu')), 404);

        $old = M_menu_front::where([
            'id_m_menu' => request('id_m_menu')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Menu Website',
            'page_title' => 'Menu Website',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'Menu Website',
            'old' => $old,
            'id_parent'   => M_menu_front::whereNull('id_parent')->where('aktif','1')->get(),
        ];

        return view('back.m_menu_front.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_menu.required' => 'harap diisi',
            'order_m_menu.numeric' => 'please fill number only',
        ];

        $validator = Validator::make($request->all(), [
            'nm_menu' => ['required'],
            'order_m_menu' => ['numeric'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_menu' => $errors->first('nm_menu'),
                    'order_m_menu' => $errors->first('order_m_menu'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_menu_front::where([
            'id_m_menu' => $request->id_m_menu,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_menu' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif = $request->aktif;
        $update->nm_menu = $request->nm_menu;
        $update->order_m_menu = $request->order_m_menu;
        $update->id_parent = $request->id_parent;
        $update->url = $request->url;
        $update->order_m_menu = $request->order_m_menu;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_menu_front.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_menu')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_menu_front::where([
            'id_m_menu' => $request->id_m_menu
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_menu_front.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_menu_front::orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_menu;
            $datas[$key][] = ($value->aktif=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = $value->url;
            $datas[$key][] = ($value->id_parent) ? M_menu_front::whereNotNull('aktif')->where('id_m_menu', $value->id_parent)->first()->nm_menu : null;
            // $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_menu_front.edit',['id_m_menu' => $value->id_m_menu]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_menu="'.$value->id_m_menu.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }



    public function hakakses()
    {
        abort_if(!request()->filled('id_m_user_group') or !is_numeric(request('id_m_user_group')), 404);

        $old = M_user_group::where([
            'id_m_user_group' => request('id_m_user_group')
        ])->firstOrFail();

        $menu = M_menu_front::whereNull('id_parent')->get();

        $data = [
            'head_title' => 'User Group',
            'page_title' => 'User Group',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'User Group',
            'old' => $old,
            'menu' => $menu,
        ];

        return view('admin.m_user_group.hakakses')->with($data);
    }


    public function hakakses_update(Request $request)
    {
        $old = M_menu_front::get();
        foreach ($old as $value) {
            if($request->input('cek_'.$value->id_m_menu) != ''){
                $aksesUpdate = M_hak_akses::where([
                    'id_m_menu' => $value->id_m_menu,
                    'id_m_user_group' => $request->id_m_user_group,
                ])->first();

                if($aksesUpdate == null)
                {
                    $new = new M_hak_akses;
                    $new->id_m_hak_akses = M_hak_akses::maxId();
                    $new->id_m_menu = $value->id_m_menu;
                    $new->id_m_user_group = $request->id_m_user_group;
                    $new->save();
                }

            }else{

                $aksesUpdate = M_hak_akses::where([
                    'id_m_menu' => $value->id_m_menu,
                    'id_m_user_group' => $request->id_m_user_group,
                ])->first();


                if($aksesUpdate)
                {
                    $aksesUpdate->delete();
                }

            }

        }

        return response()->json([
            'status' => true,
            'redirect' => route('admin.m_user_group.index'),
        ]);
    }


}
