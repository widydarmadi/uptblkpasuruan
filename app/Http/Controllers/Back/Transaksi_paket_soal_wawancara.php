<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_soal_wawancara;
use App\Models\T_paket_soal_wawancara;
use App\Models\T_paket_soal_wawancara_det;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Transaksi_paket_soal_wawancara extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Paket Soal Wawancara',
            'page_title' => 'Paket Soal Wawancara',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Paket Soal Wawancara',
        ];

        return view('back.t_paket_soal_wawancara.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Paket Soal Wawancara',
            'page_title' => 'Paket Soal Wawancara',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Paket Soal Wawancara',
        ];

    	return view('back.t_paket_soal_wawancara.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_paket_soal_wawancara.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_paket_soal_wawancara' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_paket_soal_wawancara' => $errors->first('nm_paket_soal_wawancara'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new T_paket_soal_wawancara;
        $object->id_t_paket_soal_wawancara = T_paket_soal_wawancara::MaxId();
        $object->aktif = $request->aktif;
        $object->nm_paket_soal_wawancara = $request->nm_paket_soal_wawancara;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.t_paket_soal_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_t_paket_soal_wawancara') or !is_numeric(request('id_t_paket_soal_wawancara')), 404);

        $old = T_paket_soal_wawancara::where([
            'id_t_paket_soal_wawancara' => request('id_t_paket_soal_wawancara')
        ])->firstOrFail(); 

        $det = T_paket_soal_wawancara_det::with('soal')->where('id_t_paket_soal_wawancara', request('id_t_paket_soal_wawancara'))->get();

        $data = [
            'head_title' => 'Paket Soal Wawancara',
            'page_title' => 'Paket Soal Wawancara',
            'parent_menu_active' => 'Master Pelatihan',
            'child_menu_active'   => 'Paket Soal Wawancara',
            'old' => $old,
            'm_soal' => M_soal_wawancara::get(),
            'paket_soal' => $det,
        ];

        return view('back.t_paket_soal_wawancara.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_paket_soal_wawancara.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_paket_soal_wawancara' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_paket_soal_wawancara' => $errors->first('nm_paket_soal_wawancara'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = T_paket_soal_wawancara::where([
            'id_t_paket_soal_wawancara' => $request->id_t_paket_soal_wawancara,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_paket_soal_wawancara' => 'Data not found !',
                ]
            ]);
        }

        $update->aktif = $request->aktif;
        $update->nm_paket_soal_wawancara = $request->nm_paket_soal_wawancara;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.t_paket_soal_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }
    
    
    public function addsoal(Request $request)
    {
        $messages = [
            'm_soal.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'm_soal' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'm_soal' => $errors->first('m_soal'),
                ]
            ]);
        }


        DB::beginTransaction();
        $check = T_paket_soal_wawancara_det::where([
            'id_t_paket_soal_wawancara' => $request->id_t_paket_soal_wawancara,
            'id_m_soal_wawancara' => $request->m_soal,
        ])->count();

        if($check > 0)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'm_soal' => 'Soal sudah ada di dalam modul !',
                ]
            ]);
        }


        $new = new T_paket_soal_wawancara_det;
        $new->id_t_paket_soal_wawancara_det = T_paket_soal_wawancara_det::maxId();
        $new->id_t_paket_soal_wawancara = $request->id_t_paket_soal_wawancara;
        $new->id_m_soal_wawancara = $request->m_soal;
        try{
            $new->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Soal telah ditambahkan ke dalam modul',
                'redirect' => route('admin.t_paket_soal_wawancara.edit', ['id_t_paket_soal_wawancara' => $request->id_t_paket_soal_wawancara]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_t_paket_soal_wawancara')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_paket_soal_wawancara::where([
                'id_t_paket_soal_wawancara' => $request->id_t_paket_soal_wawancara
            ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->soal()->delete();
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.t_paket_soal_wawancara.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'server error',
            'status'  => false,
        ]);
    }
    
    
    
    public function delete_soal(Request $request)
    {
        if(!$request->filled('id_t_paket_soal_wawancara_det')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_paket_soal_wawancara_det::where([
            'id_t_paket_soal_wawancara_det' => $request->id_t_paket_soal_wawancara_det
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.t_paket_soal_wawancara.edit', ['id_t_paket_soal_wawancara' => $request->id_t_paket_soal_wawancara]),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = T_paket_soal_wawancara::withCount('soal')->orderByDesc('created_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_paket_soal_wawancara;
            $datas[$key][] = $value->soal_count;
            $datas[$key][] = ($value->aktif=='1') ? '<span class="badge bg-success">Aktif</span>' : '<span class="badge bg-danger">Non-Aktif</span>';
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.t_paket_soal_wawancara.edit',['id_t_paket_soal_wawancara' => $value->id_t_paket_soal_wawancara]).'">edit</a>
                                <a class="dropdown-item delete" data-id_t_paket_soal_wawancara="'.$value->id_t_paket_soal_wawancara.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
