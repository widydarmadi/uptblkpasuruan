<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_pendaftar;
use App\Models\M_program_kegiatan;
use App\Models\M_program_pelatihan;
use App\Models\T_kelas_pelatihan;
use App\Models\T_kelas_pelatihan_det;
use App\Models\T_peserta;
use App\Models\T_seleksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Validator;

class Transaksi_hasil_pelatihan extends Controller
{
    public function index()
    {
        $list_kategori_kejuruan = M_kategori_kejuruan::where('aktif', '1')
        ->whereHas('kejuruan', function($q){
            $q->where('aktif_m_kejuruan', '1');
            $q->whereHas('program_pelatihan', function($q){
                $q->where('aktif', '1')
                  ->whereHas('jadwal', function($q){
                    // $q->where('status', 'OPEN');
                  });
            });
        })->get();

        $data = [
            'head_title' => 'Hasil Pelatihan',
            'page_title' => 'Hasil Pelatihan',
            'parent_menu_active' => 'Pelatihan Institusional',
            'child_menu_active'   => 'Hasil Pelatihan',
            'list_kategori_kejuruan' => $list_kategori_kejuruan,
        ];

        return view('back.t_hasil_pelatihan.index')->with($data);
    }

    public function edit()
    {
        abort_if(!request()->filled('id_t_kelas_pelatihan') or !is_numeric(request('id_t_kelas_pelatihan')), 404);

        $old = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => request('id_t_kelas_pelatihan')
        ])
        ->with('jadwal',function($q){
            $q->select('id_m_jadwal','id_m_gelombang','id_m_program_pelatihan','nm_m_jadwal','tahun');
            $q->with('gelombang:id_m_gelombang,nm_m_gelombang');
        })
        ->firstOrFail();

        $jadwal = $old->jadwal->select('id_m_jadwal','nm_m_jadwal')->get();

        $prog = M_program_pelatihan::select('id_m_program_pelatihan','nm_m_program_pelatihan')
                ->where('aktif', '1')
                ->whereHas('jadwal', function($q){
                    // $q->where('status', 'OPEN');
                    // $q->where('tahun', date('Y'));
                })
                ->where('id_m_program_pelatihan', $old->jadwal->id_m_program_pelatihan)
                ->orderByDesc('id_m_program_pelatihan')
                ->get();
                
        


        $data = [
            'head_title' => 'Hasil Pelatihan',
            'page_title' => 'Hasil Pelatihan',
            'parent_menu_active' => 'Pelatihan Institusional',
            'child_menu_active'   => 'Hasil Pelatihan',
            'old' => $old,
            'id_m_program_pelatihan' => $prog,
            'id_m_jadwal' => $jadwal,
            'selected_program' => $old->jadwal->id_m_program_pelatihan,
            'selected_jadwal' => $old->id_m_jadwal,
        ];

        return view('back.t_hasil_pelatihan.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nama_kelas.required' => 'harap diisi',
            'id_m_program_pelatihan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nama_kelas' => ['required'],
            'id_m_program_pelatihan' => ['required'],
            'id_m_jadwal' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nama_kelas' => $errors->first('nama_kelas'),
                    'id_m_program_pelatihan' => $errors->first('id_m_program_pelatihan'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                ]
            ]);
        }
        
        // dd($request->pilih_siswa);
        if(!is_array($request->pilih_siswa)){
            return response()->json([
                'status' => false,
                'message' => 'pilih setidaknya 1 (satu) peserta yang memenuhi standar kompetensi',
            ]);
        }


        DB::beginTransaction();
        $update = T_kelas_pelatihan::where([
            'id_t_kelas_pelatihan' => $request->id_t_kelas_pelatihan,
            ])->first();
        $id_kelas = $update->id_t_kelas_pelatihan;

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nama_kelas' => 'Data not found !',
                ]
            ]);
        }

        try{
            
            // dd(count($request->pilih_siswa));

            //reset kelulusan data peserta
            $clear_peserta = T_peserta::where([
                        'id_t_kelas_pelatihan' => $request->id_t_kelas_pelatihan,
                      ])->update([
                        'status' => null
                      ]);

            //re-update data peserta
            for($i=0; $i < count($request->pilih_siswa); $i++){
                $id_seleksi = T_seleksi::select('id_t_seleksi','id_m_pendaftar')
                                ->where('id_m_pendaftar', $request->pilih_siswa[$i])
                                ->with('peserta:id_t_seleksi,status')
                                ->first();
                // dd($id_seleksi);
                if($id_seleksi and $id_seleksi->peserta){
                    $id_seleksi->peserta()->update(
                        [
                            'status' => 'KOMPETEN',
                        ]
                    );
                    // $id_seleksi->peserta->save();
                }else if($id_seleksi and $id_seleksi->peserta == null){
                    $new_seleksi = new T_peserta;
                    $new_seleksi->id_t_peserta = T_peserta::maxId();
                    $new_seleksi->id_t_seleksi = $id_seleksi->id_t_seleksi;
                    $new_seleksi->id_t_kelas_pelatihan = $request->id_t_kelas_pelatihan;
                    $new_seleksi->status = 'KOMPETEN';
                    $new_seleksi->save();
                }
            }

            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data kompetensi telah diperbaharui !',
                'redirect' => route('admin.t_hasil_pelatihan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function datatable(Request $request)
    {
        $id_m_kategori_kejuruan = $request->id_m_kategori_kejuruan;
        $id_m_kejuruan = $request->id_m_kejuruan;
        $id_m_program_pelatihan = $request->id_m_program_pelatihan;
        $id_m_jadwal = $request->id_m_jadwal;
        $table = T_kelas_pelatihan::select('id_t_kelas_pelatihan','id_m_jadwal','nama_kelas', 'status','created_at')
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        !isset($id_m_kejuruan) and
                                        !isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_kategori_kejuruan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                                                $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                                    $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                                                });
                                            });
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        !isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_kejuruan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                                                $q->where('id_m_kejuruan', $id_m_kejuruan);
                                            });
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        isset($id_m_program_pelatihan) and
                                        !isset($id_m_jadwal)
                                    , function($q) use ($id_m_program_pelatihan){
                                        $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                                            $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                                        });
                                    })
                                    ->when(
                                        isset($id_m_kategori_kejuruan) and
                                        isset($id_m_kejuruan) and
                                        isset($id_m_program_pelatihan) and
                                        isset($id_m_jadwal)
                                    , function($q) use ($id_m_jadwal){
                                        $q->where('id_m_jadwal', $id_m_jadwal);
                                    })
                                    ->with('jadwal:id_m_jadwal,nm_m_jadwal')
                                    ->withCount('peserta')
                                    ->with('peserta:id_t_kelas_pelatihan,id_t_peserta,status')
                                    ->orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nama_kelas;
            $datas[$key][] = $value->jadwal->nm_m_jadwal;
            $datas[$key][] = number_format($value->peserta_count, 0, '.', '.');
            $datas[$key][] = ($value->peserta) ? $value->peserta->where('status', 'KOMPETEN')->count() : 0;
            $datas[$key][] = ($value->peserta) ? $value->peserta->where('status','<>' ,'KOMPETEN')->count() : 0;
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.t_hasil_pelatihan.edit',['id_t_kelas_pelatihan' => $value->id_t_kelas_pelatihan]).'">penilaian</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


    public function load_jadwal(Request $request)
    {
        if($request->id_m_program_pelatihan){
            $jadwal = M_jadwal::select('id_m_jadwal','nm_m_jadwal')->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)->where('status', 'OPEN')->get();
        }else{
            $jadwal = null;
        }

        $html = ($jadwal and count($jadwal) > 0) ? 
                '<option value="">-- pilih jadwal pelatihan --</option>' :
                '<option value="">-- tidak ada jadwal --</option>';

        if($jadwal != null){
            foreach($jadwal as $jad){
                $html .= '<option value="'.$jad->id_m_jadwal.'">'.$jad->nm_m_jadwal.'</option>';
            }
        }

        return response()->json([
            'status' => true,
            'html' => $html,
        ]);
    }

    public function list_siswa(Request $request)
    {
        $id_jadwal = $request->id_m_jadwal;
        $id_t_kelas_pelatihan = $request->id_t_kelas_pelatihan;
        if($request->id_m_jadwal){
            // $get_data_seleksi = M_pendaftar::select('id_m_pendaftar', 'nm_m_pendaftar', 'nik_m_pendaftar', 'no_register', 'kategori_m_pendaftar', 'jk_m_pendaftar', 'email_m_pendaftar')
            //                     ->where('id_m_jadwal', $request->id_m_jadwal)
            //                     ->whereHas('seleksi', function($q)  use ($id_t_kelas_pelatihan){
            //                         $q->where('status', 'LULUS');
            //                         $q->whereHas('peserta', function($q) use ($id_t_kelas_pelatihan){
            //                             $q->where('id_t_kelas_pelatihan', $id_t_kelas_pelatihan);
            //                         });
            //                     })
            //                     ->with('seleksi', function($q){
            //                         $q->select('id_m_pendaftar','id_t_seleksi','total_nilai_keseluruhan')
            //                           ->with('peserta:id_t_seleksi,id_t_peserta,status');
            //                     })
            //                     ->orderBy('id_m_pendaftar')
            //                     ->get();
            
            
                                
            $get_data_seleksi = T_peserta::select('id_t_seleksi','id_t_peserta','status')
                                ->whereHas('seleksi', function($q){
                                    $q->where('status', 'LULUS');
                                })
                                ->where('id_t_kelas_pelatihan', $id_t_kelas_pelatihan)
                                ->with('seleksi', function($q) use ($id_jadwal){
                                    $q->select('id_m_pendaftar','id_t_seleksi','total_nilai_keseluruhan')
                                      ->with('pendaftar', function($q) use ($id_jadwal){
                                          $q->where('id_m_jadwal', $id_jadwal);
                                      });
                                })
                                ->get();
                
            // dd(count($get_data_seleksi));
            $data = [
                'list_siswa' => $get_data_seleksi,
            ];
            
            return view('back.t_hasil_pelatihan.list_siswa', $data);
        }
    }
    


}
