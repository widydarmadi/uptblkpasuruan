<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_katalog_alumni;
use App\Models\M_kategori_katalog_alumni;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\DB;

class Master_katalog_alumni extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Katalog Alumni',
            'page_title' => 'Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Katalog Alumni',
        ];

        return view('back.m_katalog_alumni.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'Katalog Alumni',
            'page_title' => 'Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Katalog Alumni',
            'kategori' => M_kategori_katalog_alumni::where('aktif_m_kategori_katalog_alumni', '1')->get(),
        ];

    	return view('back.m_katalog_alumni.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_m_katalog_alumni.required' => 'harap diisi',
            'id_m_kategori_katalog_alumni.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_katalog_alumni' => ['required'],
            'id_m_kategori_katalog_alumni' => ['required'],
            'photo' => ['image', 'nullable'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_katalog_alumni' => $errors->first('nm_m_katalog_alumni'),
                    'id_m_kategori_katalog_alumni' => $errors->first('id_m_kategori_katalog_alumni'),
                    'photo' => $errors->first('photo'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_katalog_alumni;
        $object->id_m_katalog_alumni = M_katalog_alumni::MaxId();
        $object->aktif_m_katalog_alumni = $request->aktif_m_katalog_alumni;
        $object->id_m_kategori_katalog_alumni = $request->id_m_kategori_katalog_alumni;
        $object->nm_m_katalog_alumni = $request->nm_m_katalog_alumni;
        $object->id_m_kategori_katalog_alumni = $request->id_m_kategori_katalog_alumni;
        $object->instagram = $request->instagram;
        try{

            if($request->file('photo')){
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/katalog';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $object->photo = $path1;
            }

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_katalog_alumni') or !is_numeric(request('id_m_katalog_alumni')), 404);

        $old = M_katalog_alumni::where([
            'id_m_katalog_alumni' => request('id_m_katalog_alumni')
        ])->firstOrFail();


        $data = [
            'head_title' => 'Katalog Alumni',
            'page_title' => 'Katalog Alumni',
            'parent_menu_active' => 'Katalog Alumni',
            'child_menu_active'   => 'Katalog Alumni',
            'old' => $old,
            'kategori' => M_kategori_katalog_alumni::where('aktif_m_kategori_katalog_alumni', '1')->get(),
        ];

        return view('back.m_katalog_alumni.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'nm_m_katalog_alumni.required' => 'harap diisi',
            'id_m_kategori_katalog_alumni.required' => 'harap diisi',
            'photo.image' => 'harus berupa gambar',
        ];

        $validator = Validator::make($request->all(), [
            'nm_m_katalog_alumni' => ['required'],
            'id_m_kategori_katalog_alumni' => ['required'],
            'photo' => ['image', 'nullable'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_katalog_alumni' => $errors->first('nm_m_katalog_alumni'),
                    'id_m_kategori_katalog_alumni' => $errors->first('id_m_kategori_katalog_alumni'),
                    'photo' => $errors->first('photo'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_katalog_alumni::where([
            'id_m_katalog_alumni' => $request->id_m_katalog_alumni,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_m_katalog_alumni' => 'Data not found !',
                ]
            ]);
        }

        $update->id_m_kategori_katalog_alumni = $request->id_m_kategori_katalog_alumni;
        $update->aktif_m_katalog_alumni = $request->aktif_m_katalog_alumni;
        $update->nm_m_katalog_alumni = $request->nm_m_katalog_alumni;
        $update->instagram = $request->instagram;
        try{
            if($request->file('photo')){
                \Storage::delete('public/'.$update->photo);
                $filename1 = time() . '_' . $request->file('photo')->getClientOriginalName();
                $folder = 'upload/katalog';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('photo'), $filename1);
                $update->photo = $path1;
            }

            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_katalog_alumni')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_katalog_alumni::where([
            'id_m_katalog_alumni' => $request->id_m_katalog_alumni
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            \Storage::delete('public/'.$find->photo);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_katalog_alumni.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function datatable(Request $request)
    {

        $table = M_katalog_alumni::with('m_kategori_katalog_alumni')->orderByDesc('updated_at')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_m_katalog_alumni;
            $datas[$key][] = $value->m_kategori_katalog_alumni->nm_m_kategori_katalog_alumni;
            $datas[$key][] = ($value->aktif_m_katalog_alumni=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_katalog_alumni.edit',['id_m_katalog_alumni' => $value->id_m_katalog_alumni]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_katalog_alumni="'.$value->id_m_katalog_alumni.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
