<?php

namespace App\Http\Controllers\Back;

use App\Http\Controllers\Controller;
use App\Models\M_faq;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Master_faq extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'F A Q',
            'page_title' => 'F A Q',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'F A Q',
        ];

        return view('back.m_faq.index')->with($data);
    }

    public function add()
    {
        $data = [
            'head_title' => 'F A Q',
            'page_title' => 'F A Q',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'F A Q',
        ];

    	return view('back.m_faq.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'tanya_m_faq.required' => 'harap diisi',
            'jawab_m_faq.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'tanya_m_faq' => ['required'],
            'jawab_m_faq' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'tanya_m_faq' => $errors->first('tanya_m_faq'),
                    'jawab_m_faq' => $errors->first('jawab_m_faq'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new M_faq;
        $object->id_m_faq = M_faq::MaxId();
        $object->tanya_m_faq = $request->tanya_m_faq;
        $object->jawab_m_faq = $request->jawab_m_faq;
        $object->aktif_m_faq = $request->aktif_m_faq;
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_faq.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }


    public function edit()
    {
        abort_if(!request()->filled('id_m_faq') or !is_numeric(request('id_m_faq')), 404);

        $old = M_faq::where([
            'id_m_faq' => request('id_m_faq')
        ])->firstOrFail();


        $data = [
            'head_title' => 'F A Q',
            'page_title' => 'F A Q',
            'parent_menu_active' => 'Konten Website',
            'child_menu_active'   => 'F A Q',
            'old' => $old,
        ];

        return view('back.m_faq.edit')->with($data);
    }

    public function update(Request $request)
    {
        $messages = [
            'tanya_m_faq.required' => 'harap diisi',
            'jawab_m_faq.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'tanya_m_faq' => ['required'],
            'jawab_m_faq' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'tanya_m_faq' => $errors->first('tanya_m_faq'),
                    'jawab_m_faq' => $errors->first('jawab_m_faq'),
                ]
            ]);
        }


        DB::beginTransaction();
        $update = M_faq::where([
            'id_m_faq' => $request->id_m_faq,
        ])->first();

        if($update == null)
        {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'id_m_faq' => 'Data not found !',
                ]
            ]);
        }

        $update->tanya_m_faq = $request->tanya_m_faq;
        $update->jawab_m_faq = $request->jawab_m_faq;
        $update->aktif_m_faq = $request->aktif_m_faq;
        try{
            $update->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data Saved',
                'redirect' => route('admin.m_faq.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }

    public function delete(Request $request)
    {
        if(!$request->filled('id_m_faq')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = M_faq::where([
            'id_m_faq' => $request->id_m_faq
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('admin.m_faq.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {

        $table = M_faq::orderByDesc('id_m_faq')->get();

    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->tanya_m_faq;
            $datas[$key][] = ($value->aktif_m_faq=='1') ? '<span class="badge bg-success">Active</span>' : '<span class="badge bg-danger">Inactive</span>';
            $datas[$key][] = \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $value->created_at)->format('d-m-Y H:i:s');
            $datas[$key][] = '<div class="btn-group">
                                <button type="button" class="btn btn-primary btn-sm">Action</button>
                                <button type="button" class="btn btn-primary btn-sm dropdown-toggle dropdown-icon" data-toggle="dropdown" aria-expanded="false">
                                <span class="sr-only">Toggle Dropdown</span>
                                </button>
                                <div class="dropdown-menu" role="menu" style="">
                                <a class="dropdown-item" href="'.route('admin.m_faq.edit',['id_m_faq' => $value->id_m_faq]).'">edit</a>
                                <a class="dropdown-item delete" data-id_m_faq="'.$value->id_m_faq.'" href="#">hapus</a>
                                </div>
                            </div>';

                                
    	}

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }


}
