<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_faq;
use Illuminate\Http\Request;

class Email extends Controller
{
    public function send_email_pendaftaran()
    {
        $details = [
            'title' => 'eRecruitment Information - Invitation Test',
            'body' => "Testing email saja",
        ];

        \Mail::to('widyreddevils@gmail.com')->send(new \App\Mail\MyTestMail($details));
    }
}
