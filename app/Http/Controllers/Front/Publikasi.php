<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_album;
use App\Models\M_foto_album;
use Illuminate\Http\Request;

class Publikasi extends Controller
{
    public function galeri()
    {
        $list_galeri = M_album::where('aktif_m_album', '1')
                        ->whereHas('cover')
                        ->with('cover')->orderByDesc('updated_at')->paginate(15);

        $data = [
            'list_galeri' => $list_galeri,
        ];

        return view('front.galeri.index', compact('list_galeri'))->with($data);
    }

    public function galeri_detail($slug)
    {
        abort_if(!$slug, 404);
        $list_foto = M_foto_album::where('aktif_m_foto_album', '1')
                        ->with('m_album')->orderByDesc('updated_at');

        $data = [
            'list_foto' => $list_foto,
        ];

        return view('front.galeri.galeri_detail')->with($data);
    }
}
