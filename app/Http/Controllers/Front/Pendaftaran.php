<?php

namespace App\Http\Controllers\Front;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Cbt_user;
use App\Models\Cbt_user_grup;
use App\Models\M_jadwal;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_pendaftar;
use App\Models\M_program_pelatihan;
use App\Models\T_peserta;
use App\Models\T_seleksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class Pendaftaran extends Controller
{
    public function index()
    {
        if(
            (request()->filled('grup_id') and !request()->filled('id_m_jadwal')) or
            (!request()->filled('grup_id') and request()->filled('id_m_jadwal'))
        ){
            abort(404);
        }
        $cek_cbt_grup = request()->has('grup_id') ? Cbt_user_grup::select('grup_id')->where('grup_id', request()->get('grup_id'))->firstOrFail() : null;
        $get_jadwal = request()->has('id_m_jadwal') ? M_jadwal::select('id_m_program_pelatihan', 'id_m_gelombang')->where('id_m_jadwal', request()->get('id_m_jadwal'))->firstOrFail() : null;
        $get_program = $get_jadwal ? M_program_pelatihan::select('id_m_kejuruan')->where('id_m_program_pelatihan', $get_jadwal->id_m_program_pelatihan)->firstOrFail() : null;
        if($get_program){
            $get_kategori = M_kejuruan::select('id_m_kategori_kejuruan')->where('id_m_kejuruan',$get_program->id_m_kejuruan)->first();
            $list_kejuruan = M_kejuruan::where('id_m_kategori_kejuruan', $get_kategori->id_m_kategori_kejuruan)->orderBy('nm_m_kejuruan')->get();
            $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            //   ->where('id_m_program_pelatihan', $get_jadwal->id_m_program_pelatihan)
                            //   ->whereYear('created_at', date('Y'))
                            //   ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                              ->where('status', 'OPEN')
                              ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 1);
                              })
                              ->with('gelombang')
                              ->orderBy('created_at')->get();

            $list_program_pelatihan = M_program_pelatihan::where('aktif', '1')
                                        // where('id_m_kejuruan',$get_program->id_m_kejuruan)
                                        ->whereHas('jadwal', function($q){
                                            $q->where('status', 'OPEN');
                                            // $q->where('tahun', date('Y'));
                                        })
                                        ->orderBy('id_m_program_pelatihan')
                                        ->get();

            $id_m_program_pelatihan = $get_jadwal->id_m_program_pelatihan;
            $id_m_jadwal = request()->get('id_m_jadwal');
        }else{
            $list_kejuruan = null;
            $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            //   ->where('id_m_program_pelatihan', $get_jadwal->id_m_program_pelatihan)
                            // ->whereYear('created_at', date('Y'))
                            // ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            ->where('status', 'OPEN')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 1);
                            })
                            ->with('gelombang')
                            ->orderBy('created_at')->get();
            $list_program_pelatihan = M_program_pelatihan::where('aktif', '1')
                                        ->whereHas('jadwal', function($q){
                                            $q->where('status', 'OPEN');
                                            // $q->where('tahun', date('Y'));
                                        })
                                        ->where('id_m_tipe_pelatihan', 1)
                                        ->orderBy('id_m_program_pelatihan')
                                        ->get();
            $id_m_program_pelatihan = null;
            $id_m_jadwal = null;
        }



        $data = [
            'list_kejuruan' => $list_kejuruan,
            'list_gelombang' => $list_gelombang,
            'id_m_kategori_kejuruan' => ($get_program) ? $get_kategori->id_m_kategori_kejuruan : null,
            'id_m_kejuruan' => ($get_program) ? $get_program->id_m_kejuruan : null,
            'id_m_gelombang' => ($get_jadwal) ? $get_jadwal->id_m_gelombang : null,
            'list_program_pelatihan' => ($list_program_pelatihan)  ? $list_program_pelatihan : null,
            'id_m_program_pelatihan' => ($get_program)  ? $id_m_program_pelatihan : null,
            'id_m_jadwal' => (request()->has('id_m_jadwal'))  ? $id_m_jadwal : null,
        ];


        return view('front.pendaftaran.index')->with($data);
    }

    public function index_post(Request $request)
    {
        $messages = [
            'cbt_grup_id.required' => "CBT ID masih kosong",
            'id_m_jadwal.required' => "pilih salah satu",
            'nm_m_pendaftar.required' => "Harap mengisi nama Anda",
            'nik_m_pendaftar.required' => "Harap mengisi nomor induk kependudukan Anda",
            // 'nik_m_pendaftar.size' => "Harap mengisikan 16 digit NIK",
            'nik_m_pendaftar.numeric' => "Harap mengisikan 16 digit NIK berupa angka",
            'id_m_provinsi.required' => "Pilih provinsi",
            'id_m_kota.required' => "Pilih kota",
            'alamat_m_pendaftar.required' => "Harap mengisi alamat sesuai domisili",
            'jk_m_pendaftar.required' => "Pilih salah satu",
            'wa_m_pendaftar.required' => "Harap mengisi nomor whatsapp aktif",
            'tempat_lahir_m_pendaftar.required' => "Harap mengisi tempat lahir",
            'tgl_lahir_m_pendaftar.required' => "Harap mengisi tanggal lahir",
            'email_m_pendaftar.required' => "Harap mengisi email Anda",
            'email_m_pendaftar.email' => "Alamat email yang dimasukkan tidak valid",
            'quest_pernah_blk.required' => "Pilih salah satu",
            'quest_masih_bekerja.required' => "Pilih salah satu",
            'id_m_kejuruan.required' => "Pilih salah satu",
            'quest_minat_setelah_kursus.required' => "Pilih salah satu",
            'captcha.required' => "Harap memasukkan kode captcha",
            'captcha.captcha' => "Kode tidak sesuai dengan gambar",
            'is_disabilitas.required' => "Pilih salah satu",
            'id_m_disabilitas.required' => "Pilih salah satu",
        ];

        $role_disabilitas = ($request->is_disabilitas == 'YA') ? ['required'] : ['nullable'];

        $validator = Validator::make($request->all(), [
            'cbt_grup_id' => ['required'],
            'id_m_jadwal' => ['required'],
            'nm_m_pendaftar' => ['required'],
            'nik_m_pendaftar' => ['required', 'numeric'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'alamat_m_pendaftar' => ['required'],
            'jk_m_pendaftar' => ['required'],
            'id_m_pendidikan' => ['required'],
            'wa_m_pendaftar' => ['required'],
            'tempat_lahir_m_pendaftar' => ['required'],
            'tgl_lahir_m_pendaftar' => ['required'],
            'email_m_pendaftar' => ['required','email'],
            'quest_pernah_blk' => ['required'],
            'quest_masih_bekerja' => ['required'],
            // 'id_m_kejuruan' => ['required'],
            'quest_minat_setelah_kursus' => ['required'],
            'captcha' => 'required|captcha',
            'is_disabilitas' => ['required'],
            'id_m_disabilitas' => $role_disabilitas,
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'cbt_grup_id' => $errors->first('cbt_grup_id'),
                    'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'nm_m_pendaftar' => $errors->first('nm_m_pendaftar'),
                    'nik_m_pendaftar' => $errors->first('nik_m_pendaftar'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'alamat_m_pendaftar' => $errors->first('alamat_m_pendaftar'),
                    'jk_m_pendaftar' => $errors->first('jk_m_pendaftar'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'wa_m_pendaftar' => $errors->first('wa_m_pendaftar'),
                    'tempat_lahir_m_pendaftar' => $errors->first('tempat_lahir_m_pendaftar'),
                    'tgl_lahir_m_pendaftar' => $errors->first('tgl_lahir_m_pendaftar'),
                    'email_m_pendaftar' => $errors->first('email_m_pendaftar'),
                    'quest_pernah_blk' => $errors->first('quest_pernah_blk'),
                    'quest_masih_bekerja' => $errors->first('quest_masih_bekerja'),
                    // 'id_m_kejuruan' => $errors->first('id_m_kejuruan'),
                    'quest_minat_setelah_kursus' => $errors->first('quest_minat_setelah_kursus'),
                    'captcha' => $errors->first('captcha'),
                    'is_disabilitas' => $errors->first('is_disabilitas'),
                    'id_m_disabilitas' => $errors->first('id_m_disabilitas'),
                ]
            ]);
        }


        $find = M_pendaftar::select('id_m_pendaftar')
                ->where('nik_m_pendaftar', $request->nik_m_pendaftar)
                ->whereYear('created_at', date('Y'))
                ->with('seleksi', function($q){
                    $q->where('status', 'LULUS');
                    $q->with('peserta:id_t_seleksi,id_t_peserta');
                });
                // ->get()->dd();

        if($find->count() > 0){
            $data_pendaftar = $find->first();
            // $lookup_data_peserta = T_peserta::select('no_register')->where('no_register', $data_pendaftar->no_register)->count();
            if($data_pendaftar->seleksi and $data_pendaftar->seleksi->peserta){
                return response()->json([
                    'message' => 'Anda tidak dapat mendaftar dengan identitas kependudukan (NIK) yang sama lebih dari 1 (satu) kali dalam setahun',
                    'status'  => false,
                ]);
            }
        }
        
        $tahun = date('Y');
        $bulantanggal = date('md');

        $max = M_pendaftar::whereRaw('DATE_FORMAT(created_at,\'%m%y\')=?', [date('my')])->max('no_register_urut');
        $no_urut_reg = $max + 1;
        $tahun=date('Y');
        $tahun_short = substr($tahun,2,2);
        $no_reg_ok = sprintf("%04s", $no_urut_reg);
        $tipe_pelatihan = 'INS';

        $noreg = 'REG-'.$tahun_short.'-BLKPAS-'.$bulantanggal.'-'.$tipe_pelatihan.'-'.$no_reg_ok.'';
        $noreg_cbt = $tipe_pelatihan.$tahun_short.$bulantanggal.$no_reg_ok;

        $new = new M_pendaftar;
        $new->id_m_pendaftar = M_pendaftar::maxId();
        $new->no_register = $noreg;
        $new->no_register_urut = $no_urut_reg;
        $new->id_m_jadwal = $request->id_m_jadwal;
        $new->nm_m_pendaftar = Str::upper($request->nm_m_pendaftar);
        $new->nik_m_pendaftar = $request->nik_m_pendaftar;
        $new->id_m_provinsi = $request->id_m_provinsi;
        $new->id_m_kota = $request->id_m_kota;
        $new->alamat_m_pendaftar =  $request->alamat_m_pendaftar;
        $new->jk_m_pendaftar =  $request->jk_m_pendaftar;
        $new->id_m_pendidikan =  $request->id_m_pendidikan;
        $new->wa_m_pendaftar =  $request->wa_m_pendaftar;
        $new->nm_jurusan =  $request->nm_jurusan;
        $new->tempat_lahir_m_pendaftar =  $request->tempat_lahir_m_pendaftar;
        $new->tgl_lahir_m_pendaftar = $request->tgl_lahir_m_pendaftar;
        $new->email_m_pendaftar =  $request->email_m_pendaftar;
        $new->quest_pernah_blk =  $request->quest_pernah_blk;
        $new->quest_masih_bekerja =  $request->quest_masih_bekerja;
        $new->quest_minat_setelah_kursus =  $request->quest_minat_setelah_kursus;
        // $new->id_m_kejuruan =  $request->id_m_kejuruan;
        $new->kategori_m_pendaftar =  ($request->cat) ? $request->cat : 'PBK';
        $new->cbt_grup_id =  $request->cbt_grup_id;
        $new->is_disabilitas =  $request->is_disabilitas;
        $new->id_m_disabilitas =  ($request->is_disabilitas == 'YA') ? $request->id_m_disabilitas : null;

        /** 
         * create or update cbt_user
         */

        // $cbt_check = Cbt_user::select('user_id')->where('user_email', $request->email_m_pendaftar)->whereYear('user_regdate', date('Y'));
        // if($cbt_check->count() > 0){
        //     $data_cbt = $cbt_check->first();
        //     $new_cbt = Cbt_user::where('user_id', $data_cbt->user_id)->firstOrFail();
        // }else{
        //     $new_cbt = new Cbt_user;
        //     $new_cbt->user_id = Cbt_user::MaxId();
        // }
        
        $new_cbt = new Cbt_user;
        $new_cbt->user_id = Cbt_user::MaxId();
        $new_cbt->user_grup_id = $request->cbt_grup_id;
        $new_cbt->user_name = $noreg_cbt;
        $new_cbt->user_password = '123456';
        $new_cbt->user_email = $request->email_m_pendaftar;
        $new_cbt->user_regdate = now();
        $new_cbt->user_firstname = $request->nm_m_pendaftar;
        $new_cbt->user_level = 1;
        try{

            $pelatihan = M_jadwal::select('nm_m_jadwal')->where('id_m_jadwal', $request->id_m_jadwal)->first();
            app()->setLocale('id');
            $details = [
                'nama' => Str::upper($request->nm_m_pendaftar),
                'username' => $noreg_cbt,
                'pelatihan' => (($pelatihan) ? $pelatihan->nm_m_jadwal : ''),
                'tgl_lahir' => Carbon::parse(date('d-m-Y', strtotime($request->tgl_lahir_m_pendaftar)))->isoFormat('D MMMM YYYY'),
            ];

            //Mail::to($request->email_m_pendaftar)->send(new \App\Mail\MyTestMail($details));

            $new->save();
            $new_cbt->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Selamat, data pendaftaran Anda berhasil disimpan. Silahkan cek pengumuman secara berkala di website UPT BLK Pasuruan atau melalui media sosial untuk mengetahui informasi pelatihan selengkapnya',
                'redirect' => route('pendaftaran'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }
}
