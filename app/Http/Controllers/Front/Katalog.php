<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_katalog_alumni;
use Illuminate\Http\Request;

class Katalog extends Controller
{
    public function index()
    {
        $search = request()->get('search');
        $kategories = request()->get('kategori');
        $kategori = M_kategori_katalog_alumni::where('aktif_m_kategori_katalog_alumni', '1')->get();
        $data = [
            'kategori' => $kategori,
        ];

        return view('front.katalog.index')->with($data);
    }
}
