<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_jadwal;
use App\Models\M_program_kegiatan;
use App\Models\M_program_pelatihan;
use App\Models\T_seleksi;
use Illuminate\Http\Request;

class Hasil_seleksi extends Controller
{
    public function index()
    {
        $program_pelatihan = request()->get('program_pelatihan');
        $jadwal = request()->get('jadwal');
        $data = M_jadwal::whereHas('pendaftar', function($q){
                    $q->whereHas('seleksi', function($q){
                        $q->where('status', 'LULUS');
                    });
                })
                ->when(isset($program_pelatihan), function($q) use ($program_pelatihan){
                    $q->where('id_m_program_pelatihan', $program_pelatihan);
                })
                ->when(isset($jadwal), function($q) use ($jadwal){
                    $q->where('id_m_jadwal', $jadwal);
                })
                ->where('is_published', '1')
                ->orderByDesc('id_m_jadwal')->paginate(10);
        $prog = M_program_pelatihan::whereHas('jadwal', function($q){
                    $q->where('is_published', '1');
                    $q->whereHas('pendaftar', function($q){
                        $q->whereHas('seleksi', function($q){
                            $q->where('status', 'LULUS');
                        });
                    });
                })->orderBy('nm_m_program_pelatihan')->get();
        $data = [
            'data' => $data,
            'prog' => $prog,
        ];

        return view('front.hasil_seleksi.index')->with($data);
    }


    public function detail($id)
    {
        abort_if($id == null, 404);

        $seleksi = T_seleksi::select('id_m_pendaftar','id_t_seleksi','total_nilai_cbt', 'total_nilai_wawancara', 'total_nilai_keseluruhan', 'status')
                    ->whereHas('pendaftar', function($q) use ($id){
                        $q->where('id_m_jadwal', $id);
                    })
                    ->with('pendaftar', function($q){
                        $q->select('id_m_pendaftar', 'alamat_m_pendaftar', 'nm_m_pendaftar', 'id_m_kota', 'no_register', 'jk_m_pendaftar');
                        $q->with('kota:id_m_kota,nm_m_kota');
                    })
                    ->orderByDesc('total_nilai_keseluruhan')
                    ->where('status', 'LULUS')
                    ->get();

        $data = M_jadwal::select('id_m_jadwal', 'created_at', 'nm_m_jadwal')
                ->where('id_m_jadwal', $id)
                ->where('is_published', '1')
                ->orderByDesc('id_m_jadwal')->firstOrFail();

        $data = [
            'item' => $data,
            'seleksi' => $seleksi,
        ];

        return view('front.hasil_seleksi.detail')->with($data);

    }
}
