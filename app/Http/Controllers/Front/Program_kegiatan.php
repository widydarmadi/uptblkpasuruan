<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_program_kegiatan;
use Illuminate\Http\Request;

class Program_kegiatan extends Controller
{
    public function program_kegiatan()
    {
        $data = [
            'program_kegiatan' => M_program_kegiatan::with('sumber_dana')->orderByDesc('id_m_program_kegiatan')->paginate(10),
        ];

        return view('front.program.index')->with($data);
    }
}
