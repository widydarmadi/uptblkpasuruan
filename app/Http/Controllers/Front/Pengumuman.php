<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_konten;
use App\Models\M_pendaftar_mtu;
use App\Models\M_program_pelatihan;
use Illuminate\Http\Request;

class Pengumuman extends Controller
{
    public function index()
    {
        $mtu = M_pendaftar_mtu::where('is_tampil', 'YA')->with('jadwal')->orderByDesc('created_at')->paginate(12);
        $prog = M_program_pelatihan::whereHas('jadwal', function($q){
            $q->whereHas('pendaftar_mtu', function($q){
                $q->where('is_tampil', 'YA');
            });
        })->orderBy('nm_m_program_pelatihan')->get();

        $data = [
            'list' => $mtu,
            'prog' => $prog,
        ];

        return view('front.pengumuman.index')->with($data);
    }

    public function pengumuman_detail($id)
    {
        
    }
}
