<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_pendaftar;
use App\Models\M_welcome;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Home extends Controller
{
    public function index()
    {
        $data = [
            'm_welcome' => M_welcome::where('aktif_m_welcome', '1')->orderByDesc('updated_at')->get(),
        ];

        return view('front.home.index')->with($data);
    }

    
}
