<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_alumni;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Api extends Controller
{
    public function login_as_alumni(Request $request)
    {
        $get = M_alumni::where('nik_m_alumni', $request->username)->first();
        if($get){
            $success = TRUE;
        }else{
            $success = FALSE;
        }
        return response()->json([
            'success' => $success,
            'data' => $get
        ]);
    }
}