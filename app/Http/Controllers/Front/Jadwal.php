<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_jadwal;
use App\Models\M_program_pelatihan;
use App\Models\M_sumber_dana;
use App\Models\M_tipe_pelatihan;
use Illuminate\Http\Request;

class Jadwal extends Controller
{
    public function index()
    {
        $tahun_opt = M_jadwal::select('tahun')->groupBy('tahun')->get();
        $anggaran_opt = M_sumber_dana::where('aktif_m_sumber_dana', '1')->get();
        $tipe_opt = M_tipe_pelatihan::where('aktif_m_tipe_pelatihan', '1')->get();
        $prog_opt = M_program_pelatihan::whereHas('jadwal')->orderBy('nm_m_program_pelatihan')->get();

        $tahun = request()->get('tahun');
        $anggaran = request()->get('anggaran');
        $tipe = request()->get('tipe');
        $program_pelatihan = request()->get('program_pelatihan');

        // dd($anggaran);

        $jadwal = M_jadwal::select('id_m_jadwal', 'status', 'grup_id', 'nm_m_jadwal','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'hari', 'jml_paket', 'kuota', 'tahun', 'id_m_program_pelatihan')
                    // ->whereYear('created_at', date('Y')) //kuncian tahun berjalan di loss
                    // ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                    // ->where('status', 'OPEN')
                    ->with('program_pelatihan', function($q){
                        $q->select('id_m_program_pelatihan', 'id_m_tipe_pelatihan');
                        $q->with('tipe_pelatihan:id_m_tipe_pelatihan,nm_m_tipe_pelatihan');
                    })
                    ->with('gelombang')
                    ->when($tahun == null or $tahun == '' or empty($tahun), function($q) {
                        // $q->where('tahun', date('Y'));
                    })
                    ->when($tahun, function($q) use ($tahun) {
                        $q->where('tahun', $tahun);
                    })
                    ->when($anggaran, function($q) use ($anggaran) {
                        $q->whereHas('program_pelatihan', function($q) use ($anggaran){
                            $q->where('id_m_sumber_dana', $anggaran);
                        });
                    })
                    ->when($tipe, function($q) use ($tipe, $anggaran) {
                        $q->whereHas('program_pelatihan', function($q) use ($tipe,$anggaran){
                            $q->where('id_m_tipe_pelatihan', $tipe);
                        });
                    })
                    ->when($program_pelatihan, function($q) use ($program_pelatihan) {
                        $q->where('id_m_program_pelatihan', $program_pelatihan);
                    })
                    ->orderByDesc('tgl_mulai')->get();

                


        $data = [
            'jadwal' => $jadwal,
            // 'prog' => $prog,
            'tahun' => $tahun_opt,
            'tipe' => $tipe_opt,
            'anggaran' => $anggaran_opt,
        ];

        return view('front.jadwal.index')->with($data);
    }
}
