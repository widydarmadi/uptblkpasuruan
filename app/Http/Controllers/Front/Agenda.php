<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_agenda;
use Illuminate\Http\Request;

class Agenda extends Controller
{
    public function index()
    {
        $data = [
            'agenda' => M_agenda::orderByDesc('id_m_agenda')->where('aktif_m_agenda', '1')->paginate(9),
        ];

        return view('front.agenda.index')->with($data);
    }
    
    
    public function detail($slug)
    {
        abort_if($slug == null, 404);
        $data = [
            'agenda' => M_agenda::where('slug_m_agenda', $slug)->where('aktif_m_agenda', '1')->firstOrFail(),
        ];

        return view('front.agenda.detail')->with($data);
    }
}
