<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_bidang_usaha;
use App\Models\M_link_cepat;
use App\Models\M_provinsi;
use App\Models\T_pekerjaan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class Alumni_pekerjaan extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Data Pekerjaan Alumni',
            'page_title' => 'Data Pekerjaan Alumni',
            'parent_menu_active' => 'Alumni',
            'child_menu_active'   => 'Data Pekerjaan Alumni',
        ];

        return view('front.alumni.pekerjaan.index')->with($data);
    }

    public function add()
    {
        $provinsi = M_provinsi::select('id_m_provinsi','nm_m_provinsi')->orderBy('nm_m_provinsi')->get();
        $data = [
            'head_title' => 'Data Pekerjaan Alumni',
            'page_title' => 'Data Pekerjaan Alumni',
            'parent_menu_active' => 'Alumni',
            'child_menu_active'   => 'Data Pekerjaan Alumni',
            'bidang_usaha' => M_bidang_usaha::where('aktif', '1')->get(),
            'id_m_provinsi' => $provinsi,
        ];

    	return view('front.alumni.pekerjaan.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'nm_t_pekerjaan.required' => 'harap diisi',
            'id_m_bidang_usaha.required' => 'harap diisi',
            'jabatan_t_pekerjaan.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
            // 'captcha.captcha' => 'kode salah',
            // 'captcha.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_t_pekerjaan' => ['required'],
            'id_m_bidang_usaha' => ['required'],
            'jabatan_t_pekerjaan' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'id_m_kecamatan' => ['required'],
            // 'captcha' => 'required|captcha',
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_t_pekerjaan' => $errors->first('nm_t_pekerjaan'),
                    'id_m_bidang_usaha' => $errors->first('id_m_bidang_usaha'),
                    'jabatan_t_pekerjaan' => $errors->first('jabatan_t_pekerjaan'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                    // 'captcha' => $errors->first('captcha'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new T_pekerjaan;
        $object->id_t_pekerjaan = T_pekerjaan::MaxId();
        $object->nm_t_pekerjaan = $request->nm_t_pekerjaan;
        $object->jabatan_t_pekerjaan = $request->jabatan_t_pekerjaan;
        $object->telp_t_pekerjaan = $request->telp_t_pekerjaan;
        $object->tgl_mulai_bekerja = ($request->tgl_mulai_bekerja) ? Carbon::parse($request->tgl_mulai_bekerja)->format('Y-m-d') : null;
        $object->alamat_t_pekerjaan = $request->alamat_t_pekerjaan;
        $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_alumni = $request->id_m_alumni;
        $object->id_m_bidang_usaha = $request->id_m_bidang_usaha;

        try{
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data pekerjaan berhasil disimpan',
                'redirect' => route('alumni_page.pekerjaan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function delete(Request $request)
    {
        if(!$request->filled('id_t_pekerjaan')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_pekerjaan::where([
            'id_t_pekerjaan' => $request->id_t_pekerjaan
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('alumni_page.pekerjaan.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = T_pekerjaan::where('id_m_alumni', session()->get('alumni_logged_in.id_m_alumni'))
                ->with('bidang')
                ->orderByDesc('id_t_pekerjaan')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_t_pekerjaan;
            $datas[$key][] = $value->bidang->nm_m_bidang_usaha;
            $datas[$key][] = $value->tgl_mulai_bekerja;
            $datas[$key][] = $value->jabatan_t_pekerjaan;
            $datas[$key][] = $value->telp_t_pekerjaan;
            $datas[$key][] = '<a class="btn-sm btn-danger delete" data-id_t_pekerjaan="'.$value->id_t_pekerjaan.'" href="#">hapus</a>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
