<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Carbon\Carbon;
use App\Http\Controllers\Controller;
use App\Models\M_bidang_usaha;
use App\Models\M_link_cepat;
use App\Models\M_provinsi;
use App\Models\T_usaha;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;

class Alumni_usaha extends Controller
{
    public function index()
    {
        $data = [
            'head_title' => 'Data Usaha',
            'page_title' => 'Data Usaha',
            'parent_menu_active' => 'Alumni',
            'child_menu_active'   => 'Data Usaha',
        ];

        return view('front.alumni.usaha.index')->with($data);
    }

    public function add()
    {
        $provinsi = M_provinsi::select('id_m_provinsi','nm_m_provinsi')->orderBy('nm_m_provinsi')->get();
        $data = [
            'head_title' => 'Data Usaha',
            'page_title' => 'Data Usaha',
            'parent_menu_active' => 'Alumni',
            'child_menu_active'   => 'Data Usaha',
            'bidang_usaha' => M_bidang_usaha::where('aktif', '1')->get(),
            'id_m_provinsi' => $provinsi,
        ];

    	return view('front.alumni.usaha.add')->with($data);
    }

    public function save(Request $request)
    {
        $messages = [
            'tipe_t_usaha.required' => 'harap diisi',
            'kategori_t_usaha.required' => 'harap diisi',
            'nm_t_usaha.required' => 'harap diisi',
            'id_m_bidang_usaha.required' => 'harap diisi',
            'alamat_t_usaha.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
            'email_t_usaha.email' => 'alamat email tidak valid',
            'foto_t_usaha.required' => 'pilih gambar (JPG atau PNG)',
            'foto_t_usaha.image' => 'format foto harus berupa gambar (JPG atau PNG)',
        ];

        $validator = Validator::make($request->all(), [
            'tipe_t_usaha' => ['required'],
            'kategori_t_usaha' => ['required'],
            'nm_t_usaha' => ['required'],
            'id_m_bidang_usaha' => ['required'],
            'alamat_t_usaha' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            // 'id_m_kecamatan' => ['required'],
            'email_t_usaha' => ['nullable', 'email'],
            'foto_t_usaha' => ['required', 'image'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'tipe_t_usaha' => $errors->first('tipe_t_usaha'),
                    'kategori_t_usaha' => $errors->first('kategori_t_usaha'),
                    'nm_t_usaha' => $errors->first('nm_t_usaha'),
                    'id_m_bidang_usaha' => $errors->first('id_m_bidang_usaha'),
                    'alamat_t_usaha' => $errors->first('alamat_t_usaha'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                    'email_t_usaha' => $errors->first('email_t_usaha'),
                    'foto_t_usaha' => $errors->first('foto_t_usaha'),
                ]
            ]);
        }


        DB::beginTransaction();
        $object = new T_usaha;
        $object->id_t_usaha = T_usaha::MaxId();
        $object->tipe_t_usaha = $request->tipe_t_usaha;
        $object->kategori_t_usaha = $request->kategori_t_usaha;
        $object->nm_t_usaha = $request->nm_t_usaha;
        $object->alamat_t_usaha = $request->alamat_t_usaha;
        $object->telp_t_usaha = $request->telp_t_usaha;
        $object->email_t_usaha = $request->email_t_usaha;
        $object->ig_t_usaha = $request->ig_t_usaha;
        // $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_kota = $request->id_m_kota;
        $object->id_m_provinsi = $request->id_m_provinsi;
        $object->id_m_alumni = $request->id_m_alumni;
        $object->id_m_bidang_usaha = $request->id_m_bidang_usaha;

        try{
            if($request->file('foto_t_usaha')){
                $filename1 = time() . '_' . $request->file('foto_t_usaha')->getClientOriginalName();
                $folder = 'upload/katalog';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('foto_t_usaha'), $filename1);
                $object->foto_t_usaha = $path1;
            }
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Data usaha berhasil disimpan',
                'redirect' => route('alumni_page.usaha.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

    }



    public function delete(Request $request)
    {
        if(!$request->filled('id_t_usaha')){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        $find = T_usaha::where([
            'id_t_usaha' => $request->id_t_usaha
        ])->first();

        if($find==null){
            return response()->json([
                'message' => 'parameter invalid !',
                'status'  => false,
            ]);
        }

        DB::beginTransaction();

        try{
            Storage::delete('public/'.$find->foto_t_usaha);
            $find->delete();
            DB::commit();
            return response()->json([
                'status' => true,
                'redirect' => route('alumni_page.usaha.index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }

        return response()->json([
            'message' => 'Server error',
            'status'  => false,
        ]);
    }



    public function datatable(Request $request)
    {
        $table = T_usaha::where('id_m_alumni', session()->get('alumni_logged_in.id_m_alumni'))
                ->with('bidang')
                ->orderByDesc('id_t_usaha')->get();
    	$datas = [];
    	$i = 1;
    	foreach ($table as $key => $value) {

    		$datas[$key][] = $i++;
            $datas[$key][] = $value->nm_t_usaha;
            $datas[$key][] = $value->tipe_t_usaha;
            $datas[$key][] = $value->kategori_t_usaha;
            $datas[$key][] = $value->bidang->nm_m_bidang_usaha;
            $datas[$key][] = $value->ig_t_usaha;
            $datas[$key][] = $value->email_t_usaha;
            $datas[$key][] = '<a class="btn-sm btn-danger delete" data-id_t_usaha="'.$value->id_t_usaha.'" href="#">hapus</a>';
    	}
        // die();

    	$data = [
    		'data' => $datas
    	];

    	return response()->json($data);
    }
}
