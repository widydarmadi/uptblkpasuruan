<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_info_lowongan;
use Illuminate\Http\Request;

class Info_lowongan extends Controller
{
    public function info_lowongan()
    {
        $data = [
            'lowongan' => M_info_lowongan::with('perusahaan')->orderByDesc('id_m_lowongan')->paginate(10),
        ];

        return view('front.info_lowongan.index')->with($data);
    }


    public function detail($id = null)
    {
        abort_if($id == null, 404);

        $data = [
            'lowongan' => M_info_lowongan::where('id_m_lowongan', $id)->with('perusahaan')->firstOrFail(),
        ];

        return view('front.info_lowongan.detail')->with($data);
    }
}
