<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_konten;
use Illuminate\Http\Request;

class Konten extends Controller
{
    public function konten($slug)
    {
        abort_if(!$slug, 404);
        $find = M_konten::where('aktif_m_konten', '1')->where('slug_m_konten', $slug)->firstOrFail();
        $data = [
            'konten' => $find,
        ];

        return view('front.konten.index')->with($data);
    }
}
