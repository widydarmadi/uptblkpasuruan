<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_tipe_pelatihan;
use Illuminate\Http\Request;

class Tipe_pelatihan extends Controller
{
    public function index()
    {
        $data = [
            'tipe_pelatihan' => M_tipe_pelatihan::where('aktif_m_tipe_pelatihan', '1')->orderBy('nm_m_tipe_pelatihan')->get(),
        ];

        return view('front.tipe_pelatihan.index')->with($data);
    }
}
