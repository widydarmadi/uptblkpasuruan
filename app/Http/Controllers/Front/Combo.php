<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\Cbt_user_grup;
use App\Models\M_disabilitas;
use App\Models\M_gelombang;
use App\Models\M_jadwal;
use App\Models\M_kecamatan;
use App\Models\M_kejuruan;
use App\Models\M_kelurahan;
use App\Models\M_kota;
use App\Models\M_program_pelatihan;
use Carbon\Carbon;
use Illuminate\Http\Request;

class Combo extends Controller
{
    public function get_jenis_disabilitas(Request $request)
    {
        $get = M_disabilitas::where('aktif', '1')->get();
        $html = '';
        if($get){
            $html .= '<option value="">-- pilih jenis disabilitas --</option>';

            foreach ($get as $key => $value) {
                $html .= '<option value="'.$value->id_disabilitas.'">'.$value->nama_disabilitas.'</option>';
            }
        }else{
            $html .= '<option value="">tidak ada data</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }
    
    public function get_combo_kota(Request $request)
    {
        $get = M_kota::where('id_m_provinsi', $request->id_m_provinsi)->orderBy('nm_m_kota')->get();
        $html = '';
        if($get){
            $html .= '<option value="">- pilih kabupaten / kota -</option>';

            foreach ($get as $key => $value) {
                $html .= '<option value="'.$value->id_m_kota.'">'.$value->nm_m_kota.'</option>';
            }
        }else{
            $html .= '<option value="">kabupaten tidak ditemukan</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }
    
    
    public function get_combo_kecamatan(Request $request)
    {
        $get = M_kecamatan::where('id_m_kota', $request->id_m_kota)->orderBy('nm_m_kecamatan')->get();
        $html = '';
        if($get){
            $html .= '<option value="">- pilih kecamatan- </option>';
            foreach ($get as $key => $value) {
                $html .= '<option value="'.$value->id_m_kecamatan.'">'.$value->nm_m_kecamatan.'</option>';
            }
        }else{
            $html .= '<option value="">kecamatan tidak ditemukan</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }
    
    
    public function get_combo_kelurahan(Request $request)
    {
        $get = M_kelurahan::where('id_m_kecamatan', $request->id_m_kecamatan)->orderBy('nm_m_kelurahan')->get();
        $html = '';
        if($get){
            $html .= '<option value="">- pilih kelurahan- </option>';
            foreach ($get as $key => $value) {
                $html .= '<option value="'.$value->id_m_kelurahan.'">'.$value->nm_m_kelurahan.'</option>';
            }
        }else{
            $html .= '<option value="">kelurahan tidak ditemukan</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }
    
    
    public function get_combo_kejuruan(Request $request)
    {
        $get = M_kejuruan::where('id_m_kategori_kejuruan', $request->id_m_kategori_kejuruan)->where('aktif_m_kejuruan', '1')->orderBy('nm_m_kejuruan')->get();
        $html = '';
        if($get){
            $html .= '<option value="">Pilih Sub Kejuruan</option>';
            foreach ($get as $key => $value) {
                $html .= '<option value="'.$value->id_m_kejuruan.'">'.$value->nm_m_kejuruan.'</option>';
            }
        }else{
            $html .= '<option value="">kabupaten tidak ditemukan</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }


    public function get_pelatihan_dibuka(Request $request)
    {
        $get_pelatihan = M_program_pelatihan::select('id_m_program_pelatihan', 'nm_m_program_pelatihan')
                        ->where('id_m_kejuruan', $request->id_m_kejuruan)
                        ->where('aktif', '1')
                        ->orderBy('created_at')->get();

        $html = '';
        app()->setLocale('id');
        if($get_pelatihan and count($get_pelatihan) > 0){
            $html .= '<option value="">-- pilih program pelatihan --</option>';
            foreach ($get_pelatihan as $value) {
                $html .= '<option value="'.$value->id_m_program_pelatihan.'">'.$value->nm_m_program_pelatihan.'</option>';
            }
        }else{
            $html .= '<option value="">BELUM ADA PROGRAM PELATIHAN YANG DIBUKA</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }
    
    public function get_gelombang_dibuka(Request $request)
    {
        $get_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'tgl_mulai', 'tgl_selesai')
                        ->where('id_m_program_pelatihan', $request->id_m_program_pelatihan)
                        ->where('status', 'OPEN')
                        ->where('tahun', Carbon::parse(now())->format('Y'))
                        ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                        ->orderBy('created_at')
                        ->get();

        $html = '';
        app()->setLocale('id');
        if($get_gelombang and count($get_gelombang) > 0){
            $html .= '<option value="">-- pilih gelombang pelatihan --</option>';
            foreach ($get_gelombang as $value) {
                $mulai = Carbon::parse(date('d-m-Y', strtotime($value->tgl_mulai)))->isoFormat('D MMMM YYYY');
                $selesai = Carbon::parse(date('d-m-Y', strtotime($value->tgl_selesai)))->isoFormat('D MMMM YYYY');
                $html .= '<option value="'.$value->id_m_jadwal.'">'.$value->nm_m_jadwal.' | '.$mulai.' s/d '.$selesai.'</option>';
            }
        }else{
            $html .= '<option value="">BELUM ADA GELOMBANG YANG TERSEDIA</option>';
        }
        
        return response()->json([
            'html' => $html
        ]);
    }


    public function get_cbt_grup_id(Request $request)
    {
        $cbt_grup_id = M_jadwal::select('grup_id')->where('id_m_jadwal', $request->id_m_jadwal)->first();
        return response()->json([
            'valuez' => ($cbt_grup_id) ? $cbt_grup_id->grup_id : null
        ]);
    }
}
