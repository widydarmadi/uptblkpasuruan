<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_faq;
use Illuminate\Http\Request;

class Faq extends Controller
{
    public function faq()
    {
        $data = [
            'faq' => M_faq::where('aktif_m_faq', '1')->get(),
        ];

        return view('front.faq.index')->with($data);
    }
}
