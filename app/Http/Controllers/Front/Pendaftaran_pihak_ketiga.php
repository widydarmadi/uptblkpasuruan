<?php

namespace App\Http\Controllers\Front;

use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Models\Cbt_user;
use App\Models\Cbt_user_grup;
use App\Models\M_jadwal;
use App\Models\M_jenis_lokasi_mtu;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use App\Models\M_pendaftar;
use App\Models\M_pendaftar_mtu;
use App\Models\M_program_pelatihan;
use App\Models\T_peserta;
use App\Models\T_seleksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class Pendaftaran_pihak_ketiga extends Controller
{
    public function index()
    {
        $get_jadwal = request()->has('id_m_jadwal') ? M_jadwal::select('id_m_program_pelatihan', 'id_m_gelombang')->where('id_m_jadwal', request()->get('id_m_jadwal'))->whereYear('created_at', date('Y'))->firstOrFail() : null;
        $get_program = $get_jadwal ? M_program_pelatihan::select('id_m_kejuruan')->where('id_m_program_pelatihan', $get_jadwal->id_m_program_pelatihan)->firstOrFail() : null;
        if($get_program){
            $get_kategori = M_kejuruan::select('id_m_kategori_kejuruan')->where('id_m_kejuruan',$get_program->id_m_kejuruan)->first();
            $list_kejuruan = M_kejuruan::where('id_m_kategori_kejuruan', $get_kategori->id_m_kategori_kejuruan)->orderBy('nm_m_kejuruan')->get();
            $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            //   ->where('id_m_program_pelatihan', $get_jadwal->id_m_program_pelatihan)
                              ->whereYear('created_at', date('Y'))
                            //   ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            //   ->where('status', 'OPEN')
                              ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 3);
                              })
                              ->with('gelombang')
                              ->orderBy('created_at')->get();

            $list_program_pelatihan = M_program_pelatihan::where('aktif', '1')
                                        // where('id_m_kejuruan',$get_program->id_m_kejuruan)
                                        ->whereHas('jadwal', function($q){
                                            // $q->where('status', 'OPEN');
                                            $q->where('tahun', date('Y'));
                                        })
                                        ->orderBy('id_m_program_pelatihan')
                                        ->get();

            $id_m_program_pelatihan = $get_jadwal->id_m_program_pelatihan;
            $id_m_jadwal = request()->get('id_m_jadwal');
        }else{
            $list_kejuruan = null;
            $list_gelombang = M_jadwal::select('id_m_jadwal', 'nm_m_jadwal', 'grup_id','id_m_gelombang', 'tgl_mulai', 'tgl_selesai', 'kuota', 'tahun')
                            ->whereYear('created_at', date('Y'))
                            ->whereDate('tgl_mulai', '>', date('Y-m-d'))
                            // ->where('status', 'OPEN')
                            ->whereHas('program_pelatihan', function($q){
                                $q->where('id_m_tipe_pelatihan', 3);
                            })
                            ->with('gelombang')
                            ->orderBy('created_at')->get();
            $list_program_pelatihan = M_program_pelatihan::where('aktif', '1')
                                        ->whereHas('jadwal', function($q){
                                            // $q->where('status', 'OPEN');
                                            $q->where('tahun', date('Y'));
                                        })
                                        ->where('id_m_tipe_pelatihan', 3)
                                        ->orderBy('id_m_program_pelatihan')
                                        ->get();
            $id_m_program_pelatihan = null;
            $id_m_jadwal = null;
        }

        $jenis_lokasi_mtu = M_jenis_lokasi_mtu::where('aktif', '1')->where('is_swadaya', '1')->get();



        $data = [
            'list_kejuruan' => $list_kejuruan,
            'list_gelombang' => $list_gelombang,
            'id_m_kategori_kejuruan' => ($get_program) ? $get_kategori->id_m_kategori_kejuruan : null,
            'id_m_kejuruan' => ($get_program) ? $get_program->id_m_kejuruan : null,
            'id_m_gelombang' => ($get_jadwal) ? $get_jadwal->id_m_gelombang : null,
            'list_program_pelatihan' => ($list_program_pelatihan)  ? $list_program_pelatihan : null,
            'id_m_program_pelatihan' => ($get_program)  ? $id_m_program_pelatihan : null,
            'id_m_jadwal' => (request()->has('id_m_jadwal'))  ? $id_m_jadwal : null,
            'jenis_lokasi_mtu' => $jenis_lokasi_mtu,
        ];


        return view('front.pendaftaran_pihak_ketiga.index')->with($data);
    }

    public function index_post(Request $request)
    {
        $messages = [
            'nm_pemohon.required' => 'harap diisi',
            'jabatan_pemohon.required' => 'harap diisi',
            'wa_pemohon.required' => 'harap diisi',
            'email_pemohon.email' => 'email tidak valid',
            'email_pemohon.required' => 'harap diisi',
            'id_m_jenis_lokasi_mtu.required' => 'harap diisi',
            'nm_lokasi_mtu.required' => 'harap diisi',
            'telp_lokasi_mtu.required' => 'harap diisi',
            'email_mtu.email' => 'email tidak valid',
            'email_mtu.required' => 'harap diisi',
            'alamat_lokasi_mtu.required' => 'harap diisi',
            'id_m_provinsi.required' => 'harap diisi',
            'id_m_kota.required' => 'harap diisi',
            'id_m_kecamatan.required' => 'harap diisi',
            'id_m_kelurahan.required' => 'harap diisi',
            'id_m_jadwal.required' => 'harap diisi',
            'file_proposal.required' => 'harap diisi',
            'file_proposal.mimes' => 'format berkas harus PDF',
            'captcha.required' => "Harap memasukkan kode captcha",
            'captcha.captcha' => "Kode tidak sesuai dengan gambar",
        ];

        $validator = Validator::make($request->all(), [
            'nm_pemohon' => ['required'],
            'jabatan_pemohon' => ['required'],
            'wa_pemohon' => ['required'],
            'email_pemohon' => ['required','email'],
            'id_m_jenis_lokasi_mtu' => ['required'],
            'nm_lokasi_mtu' => ['required'],
            'telp_lokasi_mtu' => ['required'],
            'email_mtu' => ['required','email'],
            'alamat_lokasi_mtu' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'id_m_kecamatan' => ['required'],
            'id_m_kelurahan' => ['required'],
            // 'id_m_jadwal' => ['required'],
            'file_proposal' => 'required|mimes:pdf',
            'captcha' => 'required|captcha',
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nm_pemohon' => $errors->first('nm_pemohon'),
                    'jabatan_pemohon' => $errors->first('jabatan_pemohon'),
                    'wa_pemohon' => $errors->first('wa_pemohon'),
                    'email_pemohon' => $errors->first('email_pemohon'),
                    'id_m_jenis_lokasi_mtu' => $errors->first('id_m_jenis_lokasi_mtu'),
                    'nm_lokasi_mtu' => $errors->first('nm_lokasi_mtu'),
                    'telp_lokasi_mtu' => $errors->first('telp_lokasi_mtu'),
                    'email_mtu' => $errors->first('email_mtu'),
                    'alamat_lokasi_mtu' => $errors->first('alamat_lokasi_mtu'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'id_m_kecamatan' => $errors->first('id_m_kecamatan'),
                    'id_m_kelurahan' => $errors->first('id_m_kelurahan'),
                    // 'id_m_jadwal' => $errors->first('id_m_jadwal'),
                    'file_proposal' => $errors->first('file_proposal'),
                    'captcha' => $errors->first('captcha'),
                ]
            ]);
        }

        $tahun = date('Y');
        $bulantanggal = date('md');
        $max = M_pendaftar_mtu::whereRaw('DATE_FORMAT(created_at,\'%m%y\')=?', [date('my')])->max('no_register_urut');
        $no_urut_reg = $max + 1;
        $tahun=date('Y');
        $tahun_short = substr($tahun,2,2);
        $no_reg_ok = sprintf("%04s", $no_urut_reg);
        $tipe_pelatihan = 'SWA';

        $noreg = 'REG-'.$tahun_short.'-BLKPAS-'.$bulantanggal.'-'.$tipe_pelatihan.'-'.$no_reg_ok.'';

        DB::beginTransaction();
        $object = new M_pendaftar_mtu;
        $object->id_m_pendaftar_mtu = M_pendaftar_mtu::MaxId();
        $object->nm_pemohon = $request->nm_pemohon;
        $object->jabatan_pemohon = $request->jabatan_pemohon;
        $object->wa_pemohon = $request->wa_pemohon;
        $object->email_pemohon = $request->email_pemohon;
        $object->id_m_jenis_lokasi_mtu = $request->id_m_jenis_lokasi_mtu;
        $object->nm_lokasi_mtu = $request->nm_lokasi_mtu;
        $object->telp_lokasi_mtu = $request->telp_lokasi_mtu;
        // $object->potensi_wilayah = $request->potensi_wilayah;
        $object->email_mtu = $request->email_mtu;
        $object->kategori_m_pendaftar = 'SWA';
        $object->alamat_lokasi_mtu = $request->alamat_lokasi_mtu;
        $object->id_m_provinsi = $request->id_m_provinsi;
        $object->id_m_kota = $request->id_m_kota;
        $object->id_m_kecamatan = $request->id_m_kecamatan;
        $object->id_m_kelurahan = $request->id_m_kelurahan;
        // $object->id_m_jadwal = $request->id_m_jadwal;
        $object->no_register = $noreg;
        $object->no_register_urut = $no_urut_reg;
        $object->progress = 'DITINDAKLANJUTI';

        try{

            if($request->file('file_proposal')){
                $filename1 = time() . '_' . $request->file('file_proposal')->getClientOriginalName();
                $folder = 'upload/proposal';
                $f = $folder;
                $path1 = \Storage::disk('public')->putFileAs($f, $request->file('file_proposal'), $filename1);
                $object->file_proposal = $path1;
            }
       
            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Selamat, data pendaftaran Anda berhasil disimpan. Silahkan cek pengumuman secara berkala di website UPT BLK Pasuruan atau melalui media sosial untuk mengetahui informasi pelatihan selengkapnya',
                'redirect' => route('pendaftaran_pihak_ketiga'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }
}
