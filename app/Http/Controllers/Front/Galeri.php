<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_album;
use App\Models\T_album;
use Illuminate\Http\Request;

class Galeri extends Controller
{
    public function index()
    {
        $data = [
            'galeri' => M_album::whereHas('cover')->where('aktif_m_album', '1')->withCount('cover')->with('cover')->orderByDesc('id_m_album')->paginate(10),
        ];
        
        return view('front.galeri.index')->with($data);
    }


    public function detail($id = null)
    {
        abort_if($id == null, 404);

        $data = [
            'album' => M_album::where('id_m_album', $id)->firstOrFail(),
            'galeri' => T_album::where('id_m_album', $id)->with('album')->get(),
        ];

        return view('front.galeri.detail')->with($data);
    }
}
