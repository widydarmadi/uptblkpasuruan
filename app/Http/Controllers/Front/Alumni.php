<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_alumni;
use App\Models\M_faq;
use App\Models\M_pendaftar;
use App\Models\M_pendidikan;
use App\Models\M_provinsi;
use App\Models\M_kota;
use App\Models\M_kecamatan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class Alumni extends Controller
{

    public function alumni_daftar()
    {
        if(session()->get('alumni_logged_in') == true){
            return redirect()->route('alumni_page.dashboard');
        }
        
        $data = [
        ];

        return view('front.alumni.alumni_daftar')->with($data);
    }
    
    public function alumni_login()
    {
        if(session()->get('alumni_logged_in') == true){
            return redirect()->route('alumni_page.dashboard');
        }

        $data = [
            'aktif' => false
        ];

        return view('front.alumni.alumni_login')->with($data);
    }


    public function alumni_login_post(Request $request)
    {
        $messages = [
            'email_m_alumni.email' => '* Email tidak valid',
            'email_m_alumni.required' => '* Masukkan email',
            'password.required' => '* Masukkan password',
        ];

        $validator = Validator::make($request->all(), [
            'email_m_alumni' => ['required', 'email'],
            'password' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'email_m_alumni' => $errors->first('email_m_alumni'),
                    'password' => $errors->first('password'),
                ]
            ]);
        }

		$cek = M_alumni::where([
			'email_m_alumni' => $request->email_m_alumni,
// 			'aktif' => '1',
		])->first();
		
// 		dd($cek);


		if($cek){
			$cek_hash = Hash::check($request->password, $cek->password_m_alumni);
            // dd(Hash::make(3514116505040003));
            // dd($cek_hash);
			if ($cek_hash == false) {
                return response()->json([
                    'status' => false,
                    'message' => 'Email atau password salah !',
                ]);
            } else if ($cek_hash === true) {
                $request->session()->put('alumni_logged_in', 'true');
                $request->session()->put('alumni_logged_in.id_m_alumni', $cek->id_m_alumni);
                $request->session()->put('alumni_logged_in.nm_m_alumni', $cek->nm_m_alumni);
                $request->session()->put('alumni_logged_in.nik_m_alumni', $cek->nik_m_alumni);
                $request->session()->put('alumni_logged_in.email_m_alumni', $cek->email_m_alumni);
                $request->session()->put('alumni_logged_in.hp_m_alumni', $cek->hp_m_alumni);
                $cek->last_login = now();
                $cek->save();
            }
            
            return response()->json([
                'redirect' => route('alumni_page.dashboard'),
                'status'  => true,
            ]);
		}else{
            return response()->json([
                'status' => false,
                'message' => 'Email atau password salah !',
            ]);
        }
    }



    public function alumni_daftar_post(Request $request)
    {

        if($request->password != $request->repassword){
            return response()->json([
                'error' => [
                    'password' => 'konfirmasi password tidak cocok',
                ]
            ]);
        }

        $messages = [
            'nik_m_alumni.required' => "harap diisi",
            'nm_m_alumni.required' => "harap diisi",
            'email_m_alumni.required' => "harap diisi",
            'hp_m_alumni.required' => "harap diisi",
            'password.required' => "harap diisi",
            'password.min' => "minimal 6 karakter huruf atau angka",
            'password.alphanum' => "karakter harus berupa huruf atau angka",
            'repassword.required' => "harap diisi",
            'captcha.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nik_m_alumni' => ['required'],
            'nm_m_alumni' => ['required'],
            'email_m_alumni' => ['required'],
            'hp_m_alumni' => ['required'],
            'password' => ['required','min:6','alphanum'],
            'repassword' => ['required'],
            'captcha' => 'required|captcha'
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nik_m_alumni' => $errors->first('nik_m_alumni'),
                    'nm_m_alumni' => $errors->first('nm_m_alumni'),
                    'email_m_alumni' => $errors->first('email_m_alumni'),
                    'hp_m_alumni' => $errors->first('hp_m_alumni'),
                    'password' => $errors->first('password'),
                    'repassword' => $errors->first('repassword'),
                    'captcha' => $errors->first('captcha'),
                ]
            ]);
        }

        /**
         * PENGECEKAN 1
         * bila data tidak ditemukan, maka melakukan lookup ke dalam tabel m_pendaftar dengan syarat NIK yg sama
         */

        $cari_alumni = M_alumni::where('nik_m_alumni', $request->nik_m_alumni)->whereNull('aktif')->orWhere('aktif','0');
        if($cari_alumni->count() == 0){

            /** 
             * PENGECEKAN 2
             * pengecekan melalui jalur institusional / PBK dilakukan dengan lookup ke tabel peserta
             */
            
            $find = M_pendaftar::select('id_m_pendaftar')
                ->where('nik_m_pendaftar', $request->nik_m_alumni)
                ->whereYear('created_at', date('Y'))
                ->whereHas('seleksi', function($q){
                    // $q->where('status', 'LULUS');
                    $q->whereHas('peserta', function($q){
                        // $q->where('status', 'KOMPETEN');
                    });
                });

            if($find->count() == 0){

                return response()->json([
                    'message' => 'Anda tidak dapat mendaftarkan diri sebagai alumni apabila Anda belum pernah menyelesaikan pelatihan apapun di UPT BLK Pasuruan. 
                    Harap mengunjungi media informasi UPT BLK Pasuruan melalui media sosial instagram atau dari website ini.',
                    'status'  => false,
                ]);

            }else{
                $new_account = new M_alumni;
                $new_account->id_m_alumni = M_alumni::maxId();
            }
            
        }else{
            $new_account = $cari_alumni->first();
        }

        
        $new_account->nik_m_alumni = $request->nik_m_alumni;
        $new_account->nm_m_alumni = Str::upper($request->nm_m_alumni);
        $new_account->email_m_alumni = $request->email_m_alumni;
        $new_account->aktif = '1';
        $new_account->hp_m_alumni = $request->hp_m_alumni;
        $new_account->password_m_alumni = Hash::make($request->password);
        $new_account->token = Crypt::encrypt($new_account->nik_m_alumni);

        try{
            $new_account->save();
            $details = [
                'email' => $new_account->email_m_alumni,
                'nama' => Str::upper($new_account->nm_m_alumni),
                'nik' => $new_account->nik_m_alumni,
                'password' => $request->password,
            ];
            Mail::to($new_account->email_m_alumni)->send(new \App\Mail\AlumniMail($details));

            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Selamat, Pendaftaran alumni berhasil dilakukan. Silahkan login untuk melanjutkan.',
                'redirect' => route('alumni_daftar'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }


    }


    public function alumni_get_nik(Request $request)
    {
        if($request->filled('nik_m_alumni')){
            $pendaftar = M_pendaftar::select('no_register','nm_m_pendaftar')
                                      ->where('nik_m_pendaftar', $request->nik_m_alumni)
                                      ->whereNotNull('no_register')
                                      ->whereNotNull('id_m_jadwal')
                                      ->first();
            if($pendaftar){
                return response()->json([
                    'status' => true,
                    'text' => $pendaftar->nm_m_pendaftar,
                ]);
            }else{
                $find_alumni = M_alumni::where('nik_m_alumni', $request->nik_m_alumni)->whereNull('aktif')->orWhere('aktif','0');
                if($find_alumni->count() == 0){
                    return response()->json([
                        'status' => false,
                        'message' => 'Data NIK tidak ditemukan !',
                    ]);
                }
                
                return response()->json([
                    'status' => true,
                    'text' => $find_alumni->first()->nm_m_alumni,
                ]);
            }                     
        }else{
            return response()->json([
                'status' => false,
                'message' => 'NIK belum terisi',
            ]);
        }
    }


    public function aktivasi_alumni()
    {
        $token = request()->get('token');
        abort_if(!isset($token), 404);

        try{
            $decrypt = Crypt::decrypt($token);
            // dd($decrypt);
            $alumni = M_alumni::where('nik_m_alumni', $decrypt)->firstOrFail();
            $alumni->aktif = '1';
            $alumni->save();

            $data = [
                'aktif' => true
            ];
    
            return view('front.alumni.alumni_login')->with($data);
        }catch(\Exception $e){
            abort(404);
        }
    }


    public function alumni_dashboard()
    {
        $data = [
        ];

        return view('front.alumni.alumni_dashboard')->with($data);
    }
    
    public function edit_profil()
    {
        $sess = M_alumni::where('id_m_alumni', session()->get('alumni_logged_in.id_m_alumni'))->with('kota')->firstOrFail();
        $pendidikan = M_pendidikan::get();
        $provinsi = M_provinsi::select('id_m_provinsi','nm_m_provinsi')->orderBy('nm_m_provinsi')->get();
        $list_kab = M_kota::where('id_m_provinsi', $sess->id_m_provinsi)->orderBy('nm_m_kota')->get();

        $data = [
            'sess' => $sess,
            'pendidikan' => $pendidikan,
            'id_m_provinsi' => $provinsi,
            'list_kab' => $list_kab,
            
        ];

        return view('front.alumni.edit_profil')->with($data);
    }

    public function edit_profil_post(Request $request)
    {

        if(
            ($request->password != $request->repassword)
        ){
            return response()->json([
                'error' => [
                    'password' => 'konfirmasi password tidak cocok',
                ]
            ]);
        }

        $messages = [
            'nik_m_alumni.required' => "harap diisi",
            'nm_m_alumni.required' => "harap diisi",
            'email_m_alumni.required' => "harap diisi",
            'hp_m_alumni.required' => "harap diisi",
            
            'is_bekerja_m_alumni.required' => "harap diisi",
            'jk_m_alumni.required' => "harap diisi",
            'id_m_pendidikan.required' => "harap diisi",
            'kejuruan_m_alumni.required' => "harap diisi",
            'asal_sekolah_m_alumni.required' => "harap diisi",
            'alamat_m_alumni.required' => "harap diisi",
            'id_m_provinsi.required' => "harap diisi",
            'id_m_kota.required' => "harap diisi",
            'tempat_lahir_m_alumni.required' => "harap diisi",
            'tgl_lahir_m_alumni.required' => "harap diisi",

            'password.required' => "harap diisi",
            'password.min' => "minimal 6 karakter huruf atau angka",
            'password.alphanum' => "karakter harus berupa huruf atau angka",
            'repassword.required' => "harap diisi",
        ];

        $validator = Validator::make($request->all(), [
            'nik_m_alumni' => ['required'],
            'nm_m_alumni' => ['required'],
            'email_m_alumni' => ['required'],
            'hp_m_alumni' => ['required'],
            'password' => ['nullable','min:6','alphanum'],
            'repassword' => ['nullable'],

            'is_bekerja_m_alumni' => ['required'],
            'jk_m_alumni' => ['required'],
            'id_m_pendidikan' => ['required'],
            'kejuruan_m_alumni' => ['nullable'],
            'asal_sekolah_m_alumni' => ['nullable'],
            'alamat_m_alumni' => ['required'],
            'id_m_provinsi' => ['required'],
            'id_m_kota' => ['required'],
            'tempat_lahir_m_alumni' => ['required'],
            'tgl_lahir_m_alumni' => ['required'],

        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'nik_m_alumni' => $errors->first('nik_m_alumni'),
                    'nm_m_alumni' => $errors->first('nm_m_alumni'),
                    'email_m_alumni' => $errors->first('email_m_alumni'),
                    'hp_m_alumni' => $errors->first('hp_m_alumni'),
                    'password' => $errors->first('password'),
                    'repassword' => $errors->first('repassword'),
                    'captcha' => $errors->first('captcha'),
                    'is_bekerja_m_alumni' => $errors->first('is_bekerja_m_alumni'),
                    'jk_m_alumni' => $errors->first('jk_m_alumni'),
                    'id_m_pendidikan' => $errors->first('id_m_pendidikan'),
                    'kejuruan_m_alumni' => $errors->first('kejuruan_m_alumni'),
                    'asal_sekolah_m_alumni' => $errors->first('asal_sekolah_m_alumni'),
                    'alamat_m_alumni' => $errors->first('alamat_m_alumni'),
                    'id_m_provinsi' => $errors->first('id_m_provinsi'),
                    'id_m_kota' => $errors->first('id_m_kota'),
                    'tempat_lahir_m_alumni' => $errors->first('tempat_lahir_m_alumni'),
                    'tgl_lahir_m_alumni' => $errors->first('tgl_lahir_m_alumni'),
                ]
            ]);
        }

        $new_account = M_alumni::where('id_m_alumni', $request->id_m_alumni)->first();
        $new_account->nik_m_alumni = $request->nik_m_alumni;
        $new_account->nm_m_alumni = Str::upper($request->nm_m_alumni);
        $new_account->email_m_alumni = $request->email_m_alumni;
        $new_account->hp_m_alumni = $request->hp_m_alumni;

        if($request->password){
            $new_account->password_m_alumni = Hash::make($request->password);
        }

        $new_account->is_bekerja_m_alumni = $request->is_bekerja_m_alumni;
        $new_account->jk_m_alumni = $request->jk_m_alumni;
        $new_account->id_m_pendidikan = $request->id_m_pendidikan;
        $new_account->kejuruan_m_alumni = $request->kejuruan_m_alumni;
        $new_account->asal_sekolah_m_alumni = $request->asal_sekolah_m_alumni;
        $new_account->alamat_m_alumni = $request->alamat_m_alumni;
        $new_account->id_m_kota = $request->id_m_kota;
        $new_account->tempat_lahir_m_alumni = $request->tempat_lahir_m_alumni;
        $new_account->tgl_lahir_m_alumni = $request->tgl_lahir_m_alumni;

        try{
            $new_account->save();
          
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Profil alumni berhasil diperbarui',
                'redirect' => route('alumni_page.dashboard'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }

    public function logout()
    {
		session()->forget('alumni_logged_in');
	    return redirect()->route('alumni_login');
    }
    
    
    
    
    
    public function reset_password()
    {
        $data = [
            'head_title' => 'Edit Profile',
            'page_title' => 'Edit Profile',
            'parent_menu_active' => null,
            'child_menu_active'   => 'Edit Profile',
        ];
    	return view('front.alumni.edit_profile',$data);
    }

    public function reset_password_post(Request $request)
    {
        $messages = [
            'nama.required' => 'harap diisi',
            'username.required' => 'harap diisi',
        ];

        $validator = Validator::make($request->all(), [
            'nm_user' => ['required'],
            'username' => ['required'],
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
            'error' => [
                'nm_user' => $errors->first('nm_user'),
                'username' => $errors->first('username'),
            ]
            ]);
        }


        DB::beginTransaction();
        $object = M_user_bo::where('id_m_user_bo', session()->get('logged_in.id_m_user_bo'))->first();


        $object->nm_user = $request->nm_user;
        $object->username = $request->username;
        if($request->filled('password')){
            $object->password = bcrypt($request->password);
        }
        try{

            $object->save();
            DB::commit();
            return response()->json([
                'status' => true,
                'message' => 'Profile has been changed',
                'redirect' => route('admin.edit_profile'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }
}
