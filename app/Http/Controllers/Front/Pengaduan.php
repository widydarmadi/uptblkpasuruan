<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_pengaduan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class Pengaduan extends Controller
{
    public function pengaduan_post(Request $request)
    {
        $messages = [
            'email_m_pengaduan.required' => "Harap mengisi email Anda",
            'email_m_pengaduan.email' => "Alamat email yang Anda masukkan tidak valid",
            'is_bersedia.required' => "Harap mengisi bidang ini",
            'status_pelapor.required' => "Harap mengisi bidang ini",
            'is_terlapor.required' => "Harap mengisi bidang ini",
            'email_korespondensi_m_pengaduan.email' => "Email korespondensi tidak valid",
            'id_m_permasalahan.required' => "Harap mengisi bidang ini",
            'jml_kerugian_m_pengaduan.required' => "Harap mengisi bidang ini",
            'terlibat_m_pengaduan.required' => "Harap mengisi bidang ini",
            'lokasi_m_pengaduan.required' => "Harap mengisi bidang ini",
            'waktu_m_pengaduan.required' => "Harap mengisi bidang ini",
            'isi_m_pengaduan.required' => "Harap mengisi isi pengaduan",
            'captcha.required' => "Harap memasukkan kode captcha",
            'captcha.captcha' => "Kode tidak sesuai dengan gambar",
        ];

        $validator = Validator::make($request->all(), [
            'email_m_pengaduan' => ['required','email'],
            'is_bersedia' => ['required'],
            'status_pelapor' => ['required'],
            'is_terlapor' => ['required'],
            'email_korespondensi_m_pengaduan' => ['nullable', 'email'],
            'id_m_permasalahan' => ['required'],
            'jml_kerugian_m_pengaduan' => ['required'],
            'terlibat_m_pengaduan' => ['required'],
            'lokasi_m_pengaduan' => ['required'],
            'waktu_m_pengaduan' => ['required'],
            'isi_m_pengaduan' => ['required'],
            'captcha' => 'required|captcha'
        ], $messages);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response()->json([
                'error' => [
                    'email_m_pengaduan' => $errors->first('email_m_pengaduan'),
                    'is_bersedia' => $errors->first('is_bersedia'),
                    'status_pelapor' => $errors->first('status_pelapor'),
                    'is_terlapor' => $errors->first('is_terlapor'),
                    'email_korespondensi_m_pengaduan' => $errors->first('email_korespondensi_m_pengaduan'),
                    'id_m_permasalahan' => $errors->first('id_m_permasalahan'),
                    'jml_kerugian_m_pengaduan' => $errors->first('jml_kerugian_m_pengaduan'),
                    'terlibat_m_pengaduan' => $errors->first('terlibat_m_pengaduan'),
                    'lokasi_m_pengaduan' => $errors->first('lokasi_m_pengaduan'),
                    'waktu_m_pengaduan' => $errors->first('waktu_m_pengaduan'),
                    'isi_m_pengaduan' => $errors->first('isi_m_pengaduan'),
                    'captcha' => $errors->first('captcha'),
                ]
            ]);
        }

        $new = new M_pengaduan;
        $new->id_m_pengaduan = M_pengaduan::maxId();
        $new->email_m_pengaduan = $request->email_m_pengaduan;
        $new->is_bersedia = $request->is_bersedia;
        $new->status_pelapor = $request->status_pelapor;
        $new->is_terlapor = $request->is_terlapor;
        $new->email_korespondensi_m_pengaduan = $request->email_korespondensi_m_pengaduan;
        $new->id_m_permasalahan = $request->id_m_permasalahan;
        $new->jml_kerugian = $request->jml_kerugian_m_pengaduan;
        $new->terlibat_m_pengaduan = $request->terlibat_m_pengaduan;
        $new->lokasi_m_pengaduan = $request->lokasi_m_pengaduan;
        $new->waktu_m_pengaduan = \Carbon\Carbon::parse($request->waktu_m_pengaduan)->format('Y-m-d');
        $new->isi_m_pengaduan = $request->isi_m_pengaduan;
        $new->ket_m_pengaduan = $request->ket_m_pengaduan;
        $new->saran_m_pengaduan = $request->saran_m_pengaduan;

        try{
            
            $new->save();
            DB::commit();

            return response()->json([
                'status' => true,
                'message' => 'Pesan pengaduan Anda berhasil terkirim, terima kasih atas partisipasi Anda dalam memberikan masukan positif terhadap layanan kami.',
                'redirect' => route('index'),
            ]);
        }catch(\Exception $e){
            DB::rollback();
            return response()->json([
                'message' => $e->getMessage(),
                'status'  => false,
            ]);
        }
    }

    
}
