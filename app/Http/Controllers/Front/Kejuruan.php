<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use Illuminate\Http\Request;

class Kejuruan extends Controller
{
    public function index()
    {
        $data = [
            'kategori_kejuruan' => M_kategori_kejuruan::where('aktif', '1')->withCount('kejuruan')->orderBy('urut_tampil')->get(),
            // 'kejuruan' => M_kejuruan::with('kategori_kejuruan', function($q){
            //                     $q->    where('aktif', '1')->
            //                 })
        ];

        return view('front.kejuruan.index')->with($data);
    }
}
