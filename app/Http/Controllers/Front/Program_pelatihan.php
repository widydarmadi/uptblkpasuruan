<?php

namespace App\Http\Controllers\Front;

use App\Http\Controllers\Controller;
use App\Models\M_jadwal;
use App\Models\M_kejuruan;
use App\Models\M_program_pelatihan;
use App\Models\M_tipe_pelatihan;
use Illuminate\Http\Request;

class Program_pelatihan extends Controller
{
    // public function index()
    // {
    //     $data = [
    //         'program_pelatihan' => M_program_pelatihan::where('aktif', '1')->orderBy('nm_m_program_pelatihan')->get(),
    //     ];

    //     return view('front.program_pelatihan.index')->with($data);
    // }
    
    
    public function home($id=null)
    {
        // abort_if(!$id, 404);

        $tipe = request()->get('tipe') ? request()->get('tipe') : null;
        $kejuruan = request()->get('kejuruan') ? request()->get('kejuruan') : null;
        $tipe_opt = M_tipe_pelatihan::where('aktif_m_tipe_pelatihan', '1')->get();
        $kejuruan_opt = M_kejuruan::whereHas('program_pelatihan', function($q){
            $q->whereHas('jadwal', function($q){
                // $q->where('status', 'OPEN');     /** berdasarkan jadwal dibuka tahun ini */
                // $q->where('tahun', date('Y'));
            });
        })->orderBy('nm_m_kejuruan')->get();

        $program_pelatihan = M_program_pelatihan::whereHas('kejuruan', function($q) use($id){
                    $q->where('aktif_m_kejuruan', '1');
                    $q->when(isset($id), function($q) use ($id){
                        $q->where('id_m_kategori_kejuruan', $id);
                    });
                })
                ->whereHas('jadwal', function($q){
                    // $q->where('status', 'OPEN');     /** berdasarkan jadwal dibuka tahun ini */
                    // $q->where('tahun', date('Y'));
                })
                ->when(isset($tipe), function($q) use ($tipe){
                    $q->where('id_m_tipe_pelatihan', $tipe); /** berdasarkan tipe pelatihan */
                })
                ->when(isset($kejuruan), function($q) use ($kejuruan){
                    $q->where('id_m_kejuruan', $kejuruan); /** berdasarkan tipe pelatihan */
                })
                ->where('aktif', '1')
                ->with('kejuruan', 'tipe_pelatihan')
                ->orderBy('nm_m_program_pelatihan')
                ->paginate(9);

        $data = [
            'program_pelatihan' => $program_pelatihan,
            'tipe' => $tipe_opt,
            'kejuruan' => $kejuruan_opt,
        ];

        return view('front.program_pelatihan.index')->with($data);

    }
    
    
    public function index($id)
    {
        // abort_if(!$id, 404);

        $tipe = request()->get('tipe_pelatihan') ? request()->get('tipe_pelatihan') : null;
        $data = [
            'program_pelatihan' => M_program_pelatihan::whereHas('kejuruan', function($q) use($id){
                                        $q->where('aktif_m_kejuruan', '1');
                                        $q->where('id_m_kategori_kejuruan', $id);
                                    })
                                    ->whereHas('jadwal', function($q){
                                        $q->where('status', 'OPEN');     /** berdasarkan jadwal dibuka tahun ini */
                                        $q->where('tahun', date('Y'));
                                    })
                                    ->when(isset($tipe), function($q) use ($tipe){
                                        $q->where('id_m_tipe_pelatihan', $tipe); /** berdasarkan tipe pelatihan */
                                    })
                                    ->where('aktif', '1')
                                    ->with('kejuruan')
                                    ->orderBy('nm_m_program_pelatihan')
                                    ->get(),
            
        ];

        return view('front.program_pelatihan.index')->with($data);

    }


    public function detail($id)
    {
        abort_if(!$id, 404);
        $old = M_program_pelatihan::where('id_m_program_pelatihan', $id)
                ->with('jadwal:id_m_program_pelatihan,status,id_m_jadwal,nm_m_jadwal,grup_id,tgl_mulai,tgl_selesai')
                ->firstOrFail();
        $data = [
            'old' => $old,
        ];

        return view('front.program_pelatihan.detail')->with($data);
        
    }
}
