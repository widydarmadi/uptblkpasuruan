<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class Ceklogin_alumni
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if(session('alumni_logged_in')==null){
            return redirect()->route('alumni_login');
        }
        return $next($request);
    }
}
