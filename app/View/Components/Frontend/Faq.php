<?php

namespace App\View\Components\Frontend;

use App\Models\M_faq;
use Illuminate\View\Component;

class Faq extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $data = [
            'faq' => M_faq::where('aktif_m_faq', '1')->get(),
        ];

        return view('components.frontend.faq')->with($data);
    }
}
