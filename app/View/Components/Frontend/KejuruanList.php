<?php

namespace App\View\Components\Frontend;

use App\Models\M_kategori_kejuruan;
use App\Models\M_kejuruan;
use Illuminate\View\Component;

class KejuruanList extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $list = M_kategori_kejuruan::where('aktif', '1')->where('id_m_kategori_kejuruan', '<>', 14)->get();
        return view('components.frontend.kejuruan-list', [
            'list' => $list,
            'kategori_kejuruan' => M_kategori_kejuruan::where('aktif', '1')->withCount('kejuruan')->orderBy('urut_tampil')->get(),
        ]);
    }
}
