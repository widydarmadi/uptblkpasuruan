<?php

namespace App\View\Components\Frontend;

use App\Models\M_slider;
use Illuminate\View\Component;

class Slider extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $list = M_slider::where('aktif_m_slider', '1')->orderByDesc('id_m_slider')->get();

        return view('components.frontend.slider', [
            'list' => $list,
        ]);
    }
}
