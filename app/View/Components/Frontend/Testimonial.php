<?php

namespace App\View\Components\Frontend;

use App\Models\M_testimoni;
use Illuminate\View\Component;

class Testimonial extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $list = M_testimoni::where('aktif_m_testimoni', '1')->take(6)->orderByDesc('id_m_testimoni')->get();
        return view('components.frontend.testimonial',[
            'list' => $list,
        ]);
    }
}
