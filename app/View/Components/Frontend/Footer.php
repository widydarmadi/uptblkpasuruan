<?php

namespace App\View\Components\Frontend;

use App\Models\M_teks_footer;
use Illuminate\View\Component;

class Footer extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $footer = M_teks_footer::where('id_m_teks_footer', 1)->firstOrFail();
        $data = [
            'footer' => $footer,
        ];
        return view('components.frontend.footer', $data);
    }
}
