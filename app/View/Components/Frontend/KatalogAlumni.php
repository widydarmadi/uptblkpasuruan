<?php

namespace App\View\Components\Frontend;

use App\Models\M_kategori_katalog_alumni;
use Illuminate\View\Component;

class KatalogAlumni extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $kategori = M_kategori_katalog_alumni::where('aktif_m_kategori_katalog_alumni', '1')->get();
        $data = [
            'kategori' => $kategori,
        ];
        return view('components.frontend.katalog-alumni', $data);
    }
}
