<?php

namespace App\View\Components\Frontend;

use App\Models\M_agenda;
use App\Models\M_reformasi_birokrasi;
use Illuminate\View\Component;

class ReformasiBirokrasi extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|\Closure|string
     */
    public function render()
    {
        $list = M_reformasi_birokrasi::where('aktif_m_reformasi_birokrasi', '1')->take(3)->get();
        return view('components.frontend.reformasi-birokrasi', [
            'rb' => $list,
        ]);
    }
}
