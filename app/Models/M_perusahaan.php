<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_perusahaan extends Model
{
    protected $table = "m_perusahaan";
    protected $primaryKey = "id_m_perusahaan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_perusahaan')+1;
    }
}
