<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_nilai_cbt extends Model
{
    protected $table = "t_nilai_cbt";
    protected $primaryKey = "id_t_nilai_cbt";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_nilai_cbt')+1;
    }

    // public function m_hak_akses()
    // {
    //     return $this->belongsTo(\App\Models\M_hak_akses::class,'id_m_menu','id_m_menu');
    // }


}
