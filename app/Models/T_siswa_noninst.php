<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_siswa_noninst extends Model
{
    protected $table = "t_siswa_noninst";
    protected $primaryKey = "id_t_siswa_noninst";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_siswa_noninst')+1;
    }

    public function pendidikan()
    {
        return $this->belongsTo(\App\Models\M_pendidikan::class,'id_m_pendidikan','id_m_pendidikan');
    }
    
    
    public function peserta_noninst()
    {
        return $this->hasOne(\App\Models\T_peserta::class,'id_t_seleksi','id_t_siswa_noninst');
    }
    
    public function pendaftar_mtu()
    {
        return $this->belongsTo(\App\Models\M_pendaftar_mtu::class,'no_register','no_register');
    }
    public function alumni()
    {
        return $this->hasOne(\App\Models\M_alumni::class,'nik_m_alumni','nik_t_siswa');
    }
    
}