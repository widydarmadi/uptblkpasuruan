<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_kejuruan extends Model
{
    protected $table = "m_kejuruan";
    protected $primaryKey = "id_m_kejuruan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_kejuruan')+1;
    }

    public function kategori_kejuruan() {
        return $this->belongsTo(M_kategori_kejuruan::class, 'id_m_kategori_kejuruan', 'id_m_kategori_kejuruan')->withDefault();
    }
    
    
    public function program_pelatihan() {
        return $this->hasMany(M_program_pelatihan::class, 'id_m_kejuruan', 'id_m_kejuruan');
    }

}
