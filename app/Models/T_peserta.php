<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_peserta extends Model
{
    protected $table = "t_peserta";
    protected $primaryKey = "id_t_peserta";

    protected $fillable = [
        'id_t_peserta',
        'id_t_kelas_pelatihan',
        'id_t_seleksi',
        'status'
    ];
    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_peserta')+1;
    }

    public function seleksi()
    {
        return $this->hasOne(\App\Models\T_seleksi::class,'id_t_seleksi','id_t_seleksi');
    }


}
