<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class M_jadwal extends Model
{
    protected $table = "m_jadwal";
    protected $primaryKey = "id_m_jadwal";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_jadwal')+1;
    }

    public function program_pelatihan(): BelongsTo
    {
        return $this->belongsTo(M_program_pelatihan::class, 'id_m_program_pelatihan', 'id_m_program_pelatihan')->withDefault();
    }

    public function gelombang() {
        return $this->belongsTo(M_gelombang::class, 'id_m_gelombang', 'id_m_gelombang');
    }
    
    public function kelas_pelatihan() {
        return $this->hasMany(T_kelas_pelatihan::class, 'id_m_jadwal', 'id_m_jadwal');
    }
    
    public function pendaftar() {
        return $this->hasMany(M_pendaftar::class, 'id_m_jadwal', 'id_m_jadwal');
    }
    
    public function pendaftar_mtu() {
        return $this->hasMany(M_pendaftar_mtu::class, 'id_m_jadwal', 'id_m_jadwal');
    }
    
    public function cbt_user_grup() {
        return $this->belongsTo(Cbt_user_grup::class, 'grup_id', 'grup_id');
    }


}
