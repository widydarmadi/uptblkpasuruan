<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_program_pelatihan extends Model
{
    protected $table = "m_program_pelatihan";
    protected $primaryKey = "id_m_program_pelatihan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_program_pelatihan')+1;
    }

    public function kejuruan() {
        return $this->belongsTo(M_kejuruan::class, 'id_m_kejuruan', 'id_m_kejuruan')->withDefault();
    }
    
    public function tipe_pelatihan() {
        return $this->belongsTo(M_tipe_pelatihan::class, 'id_m_tipe_pelatihan', 'id_m_tipe_pelatihan');
    }
    
    public function sumber_dana() {
        return $this->belongsTo(M_sumber_dana::class, 'id_m_sumber_dana', 'id_m_sumber_dana');
    }
    
    public function jadwal() {
        return $this->hasMany(M_jadwal::class, 'id_m_program_pelatihan', 'id_m_program_pelatihan');
    }
}
