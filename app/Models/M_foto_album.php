<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_foto_album extends Model
{
    protected $table = "m_foto_album";
    protected $primaryKey = "id_m_foto_album";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_foto_album')+1;
    }

    public function m_album() {
        return $this->belongsTo(M_album::class, 'id_m_album', 'id_m_album');
    }
}
