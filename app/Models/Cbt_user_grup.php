<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cbt_user_grup extends Model
{
    protected $table = "cbt_user_grup";
    protected $primaryKey = "grup_id";
    protected $connection = 'cbt';
    public $timestamps = false;


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('grup_id')+1;
    }

    public function cbt_user() {
        return $this->hasMany(Cbt_user::class, 'user_grup_id', 'grup_id');
    }
    
}
