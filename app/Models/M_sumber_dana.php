<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_sumber_dana extends Model
{
    protected $table = "m_sumber_dana";
    protected $primaryKey = "id_m_sumber_dana";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_sumber_dana')+1;
    }
}
