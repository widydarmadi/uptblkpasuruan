<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_album extends Model
{
    protected $table = "t_album";
    protected $primaryKey = "id_t_album";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_album')+1;
    }

    public function album() {
        return $this->belongsTo(M_album::class, 'id_m_album', 'id_m_album');
    }
}
