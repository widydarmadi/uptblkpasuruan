<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_alumni extends Model
{
    protected $table = "m_alumni";
    protected $primaryKey = "id_m_alumni";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_alumni')+1;
    }

    
    public function pendaftar() {
        return $this->hasMany(M_pendaftar::class, 'nik_m_pendaftar', 'nik_m_alumni');
    }
    
    public function usaha() {
        return $this->hasMany(T_usaha::class, 'id_m_alumni', 'id_m_alumni');
    }
    
    public function kota() {
        return $this->belongsTo(M_kota::class, 'id_m_kota', 'id_m_kota');
    }


    public static function view_alumni($id_m_kategori_kejuruan = null, $id_m_kejuruan = null, $id_m_program_pelatihan = null, $id_m_jadwal = null)
    {

        $data = (new static)::when(
                    $id_m_kategori_kejuruan and
                    ($id_m_kejuruan == null or $id_m_kejuruan == 'null' or empty($id_m_kejuruan)) and
                    ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                    ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                , function($q) use ($id_m_kategori_kejuruan){
                    $q->whereHas('pendaftar', function($q) use ($id_m_kategori_kejuruan){
                        $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                                $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                    $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                                });
                            });
                        });
                    });
                })
                ->when(
                    $id_m_kategori_kejuruan and
                    $id_m_kejuruan and
                    ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                    ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                , function($q) use ($id_m_kejuruan){
                    $q->whereHas('pendaftar', function($q) use ($id_m_kejuruan){
                        $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                                $q->where('id_m_kejuruan', $id_m_kejuruan);
                            });
                        });
                    });
                })
                ->when(
                    $id_m_kategori_kejuruan and
                    $id_m_kejuruan and
                    $id_m_program_pelatihan and 
                    ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                , function($q) use ($id_m_program_pelatihan){
                    $q->whereHas('pendaftar', function($q) use ($id_m_program_pelatihan){
                        $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                            $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                        });
                    });
                })
                ->when(
                    $id_m_kategori_kejuruan and
                    $id_m_kejuruan and
                    $id_m_program_pelatihan and 
                    $id_m_jadwal
                , function($q) use ($id_m_jadwal){
                    $q->whereHas('pendaftar', function($q) use ($id_m_jadwal){
                        $q->where('id_m_jadwal', $id_m_jadwal);
                    });
                })
                ->with('pendaftar', function($q){
                    $q->with('seleksi', 'kota', 'pendidikan', 'disabilitas')
                      ->with('jadwal:id_m_jadwal,nm_m_jadwal');
                })
                ->orderByDesc('id_m_alumni')
                ->get();

        return $data;
    }
}
