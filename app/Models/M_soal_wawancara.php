<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_soal_wawancara extends Model
{
    protected $table = "m_soal_wawancara";
    protected $primaryKey = "id_m_soal_wawancara";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_soal_wawancara')+1;
    }

    public function paket_soal_wawancara()
    {
        return $this->belongsTo(\App\Models\T_paket_soal_wawancara::class,'id_t_paket_soal_wawancara','id_t_paket_soal_wawancara');
    }


}
