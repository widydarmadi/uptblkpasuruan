<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_paket_soal_wawancara extends Model
{
    protected $table = "t_paket_soal_wawancara";
    protected $primaryKey = "id_t_paket_soal_wawancara";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_paket_soal_wawancara')+1;
    }

    public function soal()
    {
        return $this->hasMany(\App\Models\T_paket_soal_wawancara_det::class,'id_t_paket_soal_wawancara','id_t_paket_soal_wawancara');
    }


}
