<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Join_katalog extends Model
{
    protected $table = "join_katalog";
    protected $primaryKey = "id";
}
