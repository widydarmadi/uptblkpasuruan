<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_logo extends Model
{
    protected $table = "m_logo";
    protected $primaryKey = "id_m_logo";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_logo')+1;
    }
    
    public function scopeMaxOnly($query)
    {
        return $query->max('id_m_logo');
    }


}
