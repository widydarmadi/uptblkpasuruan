<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_reformasi_birokrasi extends Model
{
    protected $table = "m_reformasi_birokrasi";
    protected $primaryKey = "id_m_reformasi_birokrasi";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_reformasi_birokrasi')+1;
    }
}
