<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_album extends Model
{
    protected $table = "m_album";
    protected $primaryKey = "id_m_album";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_album')+1;
    }

    public function cover() {
        return $this->hasOne(T_album::class, 'id_m_album', 'id_m_album');
    }
    
    
    public function foto() {
        return $this->hasMany(T_album::class, 'id_m_album', 'id_m_album');
    }
}
