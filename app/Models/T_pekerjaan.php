<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_pekerjaan extends Model
{
    protected $table = "t_pekerjaan";
    protected $primaryKey = "id_t_pekerjaan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_pekerjaan')+1;
    }

    public function bidang()
    {
        return $this->belongsTo(\App\Models\M_bidang_usaha::class,'id_m_bidang_usaha','id_m_bidang_usaha');
    }
}