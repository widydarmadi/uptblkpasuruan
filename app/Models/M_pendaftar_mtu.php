<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_pendaftar_mtu extends Model
{
    protected $table = "m_pendaftar_mtu";
    protected $primaryKey = "id_m_pendaftar_mtu";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_pendaftar_mtu')+1;
    }

    public function jadwal()
    {
        return $this->belongsTo(\App\Models\M_jadwal::class,'id_m_jadwal','id_m_jadwal')->withDefault();
    }
    
    public function pendidikan()
    {
        return $this->belongsTo(\App\Models\M_pendidikan::class,'id_m_pendidikan','id_m_pendidikan');
    }
    
    public function peserta()
    {
        return $this->hasMany(\App\Models\T_siswa_noninst::class,'no_register','no_register');
    }

    public function kelurahan()
    {
        return $this->belongsTo(\App\Models\M_kelurahan::class,'id_m_kelurahan','id_m_kelurahan');
    }

    public function kecamatan()
    {
        return $this->belongsTo(\App\Models\M_kecamatan::class,'id_m_kecamatan','id_m_kecamatan');
    }

    public function kota()
    {
        return $this->belongsTo(\App\Models\M_kota::class,'id_m_kota','id_m_kota');
    }

    public function provinsi()
    {
        return $this->belongsTo(\App\Models\M_provinsi::class,'id_m_provinsi','id_m_provinsi');
    }


    public static function view_pendaftar($id_m_kategori_kejuruan = null, $id_m_kejuruan = null, $id_m_program_pelatihan = null, $id_m_jadwal = null, $type)
    {
        
        $data = (new static)::with('jadwal:id_m_jadwal,nm_m_jadwal')
                            ->with('kelurahan','kecamatan','kota', 'provinsi')
                            ->where('kategori_m_pendaftar', $type)
                            ->when(
                                $id_m_kategori_kejuruan and
                                ($id_m_kejuruan == null or $id_m_kejuruan == 'null' or empty($id_m_kejuruan)) and
                                ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                                ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                            , function($q) use ($id_m_kategori_kejuruan){
                                $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                                    $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                                        $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                            $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                                        });
                                    });
                                });
                            })
                            ->when(
                                $id_m_kategori_kejuruan and
                                $id_m_kejuruan and
                                ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                                ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                            , function($q) use ($id_m_kejuruan){
                                $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                                    $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                                        $q->where('id_m_kejuruan', $id_m_kejuruan);
                                    });
                                });
                            })
                            ->when(
                                $id_m_kategori_kejuruan and
                                $id_m_kejuruan and
                                $id_m_program_pelatihan and 
                                ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                            , function($q) use ($id_m_program_pelatihan){
                                $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                                    $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                                });
                            })
                            ->when(
                                $id_m_kategori_kejuruan and
                                $id_m_kejuruan and
                                $id_m_program_pelatihan and 
                                $id_m_jadwal
                            , function($q) use ($id_m_jadwal){
                                $q->where('id_m_jadwal', $id_m_jadwal);
                            })
                    ->withCount('peserta')->orderByDesc('id_m_pendaftar_mtu')->get();

        return $data;
    }


}