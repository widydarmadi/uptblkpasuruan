<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_testimoni extends Model
{
    protected $table = "m_testimoni";
    protected $primaryKey = "id_m_testimoni";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_testimoni')+1;
    }
}
