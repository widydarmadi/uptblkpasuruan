<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_pengumuman extends Model
{
    protected $table = "m_pengumuman";
    protected $primaryKey = "id_m_pengumuman";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_pengumuman')+1;
    }
}
