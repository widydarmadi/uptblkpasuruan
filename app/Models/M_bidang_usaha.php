<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_bidang_usaha extends Model
{
    protected $table = "m_bidang_usaha";
    protected $primaryKey = "id_m_bidang_usaha";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_bidang_usaha')+1;
    }

}
