<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_link_cepat extends Model
{
    protected $table = "m_link_cepat";
    protected $primaryKey = "id_m_link_cepat";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_link_cepat')+1;
    }
}
