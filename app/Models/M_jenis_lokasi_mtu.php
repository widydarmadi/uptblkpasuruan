<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_jenis_lokasi_mtu extends Model
{
    protected $table = "m_jenis_lokasi_mtu";
    protected $primaryKey = "id_m_jenis_lokasi_mtu";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_jenis_lokasi_mtu')+1;
    }


}
