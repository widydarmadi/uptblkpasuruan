<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_tipe_pelatihan extends Model
{
    protected $table = "m_tipe_pelatihan";
    protected $primaryKey = "id_m_tipe_pelatihan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_tipe_pelatihan')+1;
    }

    public function program_pelatihan() {
        return $this->hasMany(M_program_pelatihan::class, 'id_m_tipe_pelatihan', 'id_m_tipe_pelatihan');
    }
}
