<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_kelas_pelatihan extends Model
{
    protected $table = "t_kelas_pelatihan";
    protected $primaryKey = "id_t_kelas_pelatihan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_kelas_pelatihan')+1;
    }

    public function jadwal()
    {
        return $this->belongsTo(\App\Models\M_jadwal::class,'id_m_jadwal','id_m_jadwal');
    }
    
    public function peserta()
    {
        return $this->hasMany(\App\Models\T_peserta::class,'id_t_kelas_pelatihan','id_t_kelas_pelatihan');
    }
    
    public function peserta_mtu()
    {
        return $this->hasMany(\App\Models\T_peserta::class,'id_t_kelas_pelatihan','id_t_kelas_pelatihan');
    }
    
    public function pendaftar_mtu()
    {
        return $this->belongsTo(\App\Models\M_pendaftar_mtu::class,'no_register','no_register');
    }


}
