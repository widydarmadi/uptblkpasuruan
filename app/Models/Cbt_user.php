<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cbt_user extends Model
{
    protected $table = "cbt_user";
    protected $primaryKey = "user_id";
    protected $connection = 'cbt';
    public $timestamps = false;


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('user_id')+1;
    }

    // public function cover() {
    //     return $this->hasOne(T_album::class, 'id_m_album', 'id_m_album');
    // }
    
    
    public function cbt_tes_soal() {
        return $this->hasMany(Cbt_tes_soal::class, 'tessoal_tesuser_id', 'user_id');
    }
    
    
    public function cbt_tes_user() {
        return $this->belongsTo(Cbt_tes_user::class, 'user_id','tesuser_user_id');
    }
    
    
}
