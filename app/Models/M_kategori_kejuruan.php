<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_kategori_kejuruan extends Model
{
    protected $table = "m_kategori_kejuruan";
    protected $primaryKey = "id_m_kategori_kejuruan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_kategori_kejuruan')+1;
    }

    public function kejuruan() {
        return $this->hasMany(M_kejuruan::class, 'id_m_kategori_kejuruan', 'id_m_kategori_kejuruan');
    }

}
