<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cbt_tes_soal extends Model
{
    protected $table = "cbt_tes_soal";
    protected $primaryKey = "tessoal_id";
    protected $connection = 'cbt';
    public $timestamps = false;


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('tessoal_id')+1;
    }

    public function cbt_tes_soal_jawaban() {
        return $this->hasMany(Cbt_tes_soal_jawaban::class, 'soaljawaban_tessoal_id', 'tessoal_id');
    }
    public function cbt_soal() {
        return $this->belongsTo(Cbt_soal::class, 'tessoal_soal_id', 'soal_id');
    }

}
