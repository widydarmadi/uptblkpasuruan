<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_pegawai extends Model
{
    protected $table = "m_pegawai";
    protected $primaryKey = "id_m_pegawai";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_pegawai')+1;
    }


}
