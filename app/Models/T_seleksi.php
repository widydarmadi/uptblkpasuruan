<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_seleksi extends Model
{
    protected $table = "t_seleksi";
    protected $primaryKey = "id_t_seleksi";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_seleksi')+1;
    }

    public function pendaftar()
    {
        return $this->belongsTo(\App\Models\M_pendaftar::class,'id_m_pendaftar','id_m_pendaftar');
    }
    
    
    public function penilaian_wawancara()
    {
        return $this->hasMany(\App\Models\T_penilaian_wawancara::class,'id_t_seleksi','id_t_seleksi');
    }

    public function peserta()
    {
        return $this->hasOne(\App\Models\T_peserta::class,'id_t_seleksi','id_t_seleksi');
    }

}
