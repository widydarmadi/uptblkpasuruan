<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_katalog_alumni extends Model
{
    protected $table = "m_katalog_alumni";
    protected $primaryKey = "id_m_katalog_alumni";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_katalog_alumni')+1;
    }

    public function m_kategori_katalog_alumni()
    {
        return $this->belongsTo(\App\Models\M_kategori_katalog_alumni::class,'id_m_kategori_katalog_alumni','id_m_kategori_katalog_alumni');
    }

}
