<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_permasalahan extends Model
{
    protected $table = "m_permasalahan";
    protected $primaryKey = "id_m_permasalahan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_permasalahan')+1;
    }
}
