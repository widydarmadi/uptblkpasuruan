<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cbt_tes_soal_jawaban extends Model
{
    protected $table = "cbt_tes_soal_jawaban";
    protected $primaryKey = "soaljawaban_jawaban_id";
    protected $connection = 'cbt';
    public $timestamps = false;


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('soaljawaban_jawaban_id')+1;
    }

}
