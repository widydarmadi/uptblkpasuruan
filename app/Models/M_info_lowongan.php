<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_info_lowongan extends Model
{
    protected $table = "m_lowongan";
    protected $primaryKey = "id_m_lowongan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_lowongan')+1;
    }

    public function perusahaan() {
        return $this->belongsTo(M_perusahaan::class, 'id_m_perusahaan', 'id_m_perusahaan');
    }

  
}
