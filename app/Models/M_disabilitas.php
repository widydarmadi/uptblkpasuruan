<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_disabilitas extends Model
{
    protected $table = "m_disabilitas";
    protected $primaryKey = "id_disabilitas";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_disabilitas')+1;
    }

}
