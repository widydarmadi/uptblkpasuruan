<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_penilaian_wawancara extends Model
{
    protected $table = "t_penilaian_wawancara";
    protected $primaryKey = "id_t_penilaian_wawancara";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_penilaian_wawancara')+1;
    }

    public function soal_wawancara()
    {
        return $this->belongsTo(\App\Models\M_soal_wawancara::class,'id_m_soal','id_m_soal_wawancara');
    }
    
    public function paket_soal_wawancara_det()
    {
        return $this->belongsTo(\App\Models\T_paket_soal_wawancara_det::class,'id_m_soal','id_m_soal_wawancara');
    }


}
