<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cbt_tes_user extends Model
{
    protected $table = "cbt_tes_user";
    protected $primaryKey = "tesuser_id";
    protected $connection = 'cbt';
    public $timestamps = false;


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('tesuser_id')+1;
    }

    public function cbt_tes_soal() {
        return $this->hasMany(Cbt_tes_soal::class, 'tessoal_tesuser_id', 'tesuser_id');
    }

}
