<?php

namespace App\Models;
use App\Models\M_user_group;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_welcome extends Model
{
    protected $table = "m_welcome";
    protected $primaryKey = "id_m_welcome";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_welcome')+1;
    }

}