<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_pengaduan extends Model
{
    protected $table = "m_pengaduan";
    protected $primaryKey = "id_m_pengaduan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_pengaduan')+1;
    }

    public function permasalahan()
    {
        return $this->belongsTo(\App\Models\M_permasalahan::class,'id_m_permasalahan','id_m_permasalahan');
    }
}
