<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_usaha extends Model
{
    protected $table = "t_usaha";
    protected $primaryKey = "id_t_usaha";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_usaha')+1;
    }

    public function bidang()
    {
        return $this->belongsTo(\App\Models\M_bidang_usaha::class,'id_m_bidang_usaha','id_m_bidang_usaha');
    }

    public function alumni()
    {
        return $this->belongsTo(\App\Models\M_alumni::class,'id_m_alumni','id_m_alumni');
    }
    
}