<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_pendaftar extends Model
{
    protected $table = "m_pendaftar";
    protected $primaryKey = "id_m_pendaftar";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_pendaftar')+1;
    }

    public function jadwal()
    {
        return $this->belongsTo(\App\Models\M_jadwal::class,'id_m_jadwal','id_m_jadwal')->withDefault();
    }
    
    public function pendidikan()
    {
        return $this->belongsTo(\App\Models\M_pendidikan::class,'id_m_pendidikan','id_m_pendidikan');
    }
    
    public function seleksi()
    {
        return $this->belongsTo(\App\Models\T_seleksi::class,'id_m_pendaftar','id_m_pendaftar');
    }
    
    public function alumni()
    {
        return $this->hasOne(\App\Models\M_alumni::class,'nik_m_alumni','nik_m_pendaftar');
    }
    
    public function cbt()
    {
        return $this->hasOne(\App\Models\Cbt_user::class,'no_register','no_register');
    }
    
    public function penilaian_wawancara()
    {
        return $this->hasMany(\App\Models\T_penilaian_wawancara::class,'id_m_pendaftar','id_m_pendaftar');
    }
    
    public function paket_soal_wawancara()
    {
        return $this->belongsTo(\App\Models\T_paket_soal_wawancara::class,'id_t_paket_soal_wawancara','id_t_paket_soal_wawancara');
    }
    
    
    public function kota()
    {
        return $this->belongsTo(\App\Models\M_kota::class,'id_m_kota','id_m_kota');
    }
    
    public function disabilitas()
    {
        return $this->belongsTo(\App\Models\M_disabilitas::class,'id_m_disabilitas','id_disabilitas');
    }
    
    
    public static function view_pendaftar($id_m_kategori_kejuruan = null, $id_m_kejuruan = null, $id_m_program_pelatihan = null, $id_m_jadwal = null)
    {
        $registered = Cbt_user::select('user_name')->whereHas('cbt_tes_user', function($q){
            $q->whereHas('cbt_tes_soal');
        })->get();

        $__converted_noreg = array();

        foreach($registered as $item){
            $tipe_pendaftar = substr($item->user_name,0,3);
            $tahun = substr($item->user_name,3,2);
            $bulan_tanggal = substr($item->user_name,5,4);
            $urut = substr($item->user_name,9,4);
            $__converted_noreg[] = 'REG-'.$tahun.'-BLKPAS-'.$bulan_tanggal.'-'.$tipe_pendaftar.'-'.$urut;
        }

      
        $data = (new static)::whereNotNull('no_register')
                ->whereIn('no_register', $__converted_noreg)
                ->when(
                    $id_m_kategori_kejuruan and
                    ($id_m_kejuruan == null or $id_m_kejuruan == 'null' or empty($id_m_kejuruan)) and
                    ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                    ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                , function($q) use ($id_m_kategori_kejuruan){
                    $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                        $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                            $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                            });
                        });
                    });
                })
                ->when(
                    $id_m_kategori_kejuruan and
                    $id_m_kejuruan and
                    ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                    ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                , function($q) use ($id_m_kejuruan){
                    $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                        $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                            $q->where('id_m_kejuruan', $id_m_kejuruan);
                        });
                    });
                })
                ->when(
                    $id_m_kategori_kejuruan and
                    $id_m_kejuruan and
                    $id_m_program_pelatihan and 
                    ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                , function($q) use ($id_m_program_pelatihan){
                    $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                        $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                    });
                })
                ->when(
                    $id_m_kategori_kejuruan and
                    $id_m_kejuruan and
                    $id_m_program_pelatihan and 
                    $id_m_jadwal
                , function($q) use ($id_m_jadwal){
                    $q->where('id_m_jadwal', $id_m_jadwal);
                })
                ->with('seleksi', 'kota', 'pendidikan', 'disabilitas')
                ->with('jadwal:id_m_jadwal,nm_m_jadwal')
                ->orderByDesc('id_m_pendaftar')->get();

        return $data;
    }
    
    
    
    public static function view_pendaftar_belum_test($id_m_kategori_kejuruan = null, $id_m_kejuruan = null, $id_m_program_pelatihan = null, $id_m_jadwal = null, $tahun = null)
    {
        
        $data = (new static)::select('id_m_pendaftar', 'nik_m_pendaftar', 'nm_jurusan', 
                                    'id_m_pendidikan', 'nm_m_pendaftar', 'no_register',
                                    'id_m_jadwal','email_m_pendaftar','created_at', 
                                    'id_m_kota', 'alamat_m_pendaftar', 'jk_m_pendaftar', 
                                    'wa_m_pendaftar', 'tempat_lahir_m_pendaftar','tgl_lahir_m_pendaftar',
                                    'quest_pernah_blk', 'quest_masih_bekerja',
                                    'quest_minat_setelah_kursus', 'kategori_m_pendaftar',
                                    'is_disabilitas', 'id_m_disabilitas'
                                    )->whereNotNull('id_m_jadwal')
        // ->whereDoesntHave('seleksi')
        ->whereNotNull('no_register')
        ->with('kota:id_m_kota,nm_m_kota')
        ->with('jadwal:id_m_jadwal,nm_m_jadwal')
        ->with('pendidikan:id_m_pendidikan,nm_m_pendidikan')
        ->with('disabilitas')
        ->when(($tahun), function($q) use($tahun){
            $q->whereRaw('year(created_at) = '.$tahun.'');
        })
        ->when(
            $id_m_kategori_kejuruan and
            ($id_m_kejuruan == null or $id_m_kejuruan == 'null' or empty($id_m_kejuruan)) and
            ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
            ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
        , function($q) use ($id_m_kategori_kejuruan){
            $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                    $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                        $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                    });
                });
            });
        })
        ->when(
            $id_m_kategori_kejuruan and
            $id_m_kejuruan and
            ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
            ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
        , function($q) use ($id_m_kejuruan){
            $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                    $q->where('id_m_kejuruan', $id_m_kejuruan);
                });
            });
        })
        ->when(
            $id_m_kategori_kejuruan and
            $id_m_kejuruan and
            $id_m_program_pelatihan and 
            ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
        , function($q) use ($id_m_program_pelatihan){
            $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
            });
        })
        ->when(
            $id_m_kategori_kejuruan and
            $id_m_kejuruan and
            $id_m_program_pelatihan and 
            $id_m_jadwal
        , function($q) use ($id_m_jadwal){
            $q->where('id_m_jadwal', $id_m_jadwal);
        })
        ->orderByDesc('id_m_pendaftar')->get();

        return $data;
    }
    
    
    public static function view_pendaftar_v2($id_m_kategori_kejuruan = null, $id_m_kejuruan = null, $id_m_program_pelatihan = null, $id_m_jadwal = null)
    {
        $registered = Cbt_user::select('user_name')->whereHas('cbt_tes_user', function($q){
            $q->whereHas('cbt_tes_soal');
        })->get();

        $__converted_noreg = array();

        foreach($registered as $item){
            $tipe_pendaftar = substr($item->user_name,0,3);
            $tahun = substr($item->user_name,3,2);
            $bulan_tanggal = substr($item->user_name,5,4);
            $urut = substr($item->user_name,9,4);
            $__converted_noreg[] = 'REG-'.$tahun.'-BLKPAS-'.$bulan_tanggal.'-'.$tipe_pendaftar.'-'.$urut;
        }

        if(
            ($id_m_kategori_kejuruan == null or $id_m_kategori_kejuruan == 'null' or empty($id_m_kategori_kejuruan))
        ){
            $data = [];
        }else{
            $data = (new static)::whereNotNull('no_register')
                    ->whereIn('no_register', $__converted_noreg)
                    ->when(
                        $id_m_kategori_kejuruan and
                        ($id_m_kejuruan == null or $id_m_kejuruan == 'null' or empty($id_m_kejuruan)) and
                        ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                        ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                    , function($q) use ($id_m_kategori_kejuruan){
                        $q->whereHas('jadwal', function($q) use ($id_m_kategori_kejuruan){
                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kategori_kejuruan){
                                $q->whereHas('kejuruan', function($q) use ($id_m_kategori_kejuruan){
                                    $q->where('id_m_kategori_kejuruan', $id_m_kategori_kejuruan);
                                });
                            });
                        });
                    })
                    ->when(
                        $id_m_kategori_kejuruan and
                        $id_m_kejuruan and
                        ($id_m_program_pelatihan == null or $id_m_program_pelatihan == 'null' or empty($id_m_program_pelatihan)) and
                        ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                    , function($q) use ($id_m_kejuruan){
                        $q->whereHas('jadwal', function($q) use ($id_m_kejuruan){
                            $q->whereHas('program_pelatihan', function($q) use ($id_m_kejuruan){
                                $q->where('id_m_kejuruan', $id_m_kejuruan);
                            });
                        });
                    })
                    ->when(
                        $id_m_kategori_kejuruan and
                        $id_m_kejuruan and
                        $id_m_program_pelatihan and 
                        ($id_m_jadwal == null or $id_m_jadwal == 'null' or empty($id_m_jadwal))
                    , function($q) use ($id_m_program_pelatihan){
                        $q->whereHas('jadwal', function($q) use ($id_m_program_pelatihan){
                            $q->where('id_m_program_pelatihan', $id_m_program_pelatihan);
                        });
                    })
                    ->when(
                        $id_m_kategori_kejuruan and
                        $id_m_kejuruan and
                        $id_m_program_pelatihan and 
                        $id_m_jadwal
                    , function($q) use ($id_m_jadwal){
                        $q->where('id_m_jadwal', $id_m_jadwal);
                    })
                    ->with('seleksi', 'kota', 'pendidikan', 'disabilitas')
                    ->with('jadwal:id_m_jadwal,nm_m_jadwal')
                    ->orderByDesc('id_m_pendaftar')->get();
        }

        return $data;
    }

}
