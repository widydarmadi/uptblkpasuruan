<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_gelombang extends Model
{
    protected $table = "m_gelombang";
    protected $primaryKey = "id_m_gelombang";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_gelombang')+1;
    }
}
