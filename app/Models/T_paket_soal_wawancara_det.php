<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class T_paket_soal_wawancara_det extends Model
{
    protected $table = "t_paket_soal_wawancara_det";
    protected $primaryKey = "id_t_paket_soal_wawancara_det";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_t_paket_soal_wawancara_det')+1;
    }

    public function parent()
    {
        return $this->belongsTo(\App\Models\T_paket_soal_wawancara::class,'id_t_paket_soal_wawancara','id_t_paket_soal_wawancara');
    }
    
    public function soal()
    {
        return $this->belongsTo(\App\Models\M_soal_wawancara::class,'id_m_soal_wawancara','id_m_soal_wawancara');
    }
 
}
