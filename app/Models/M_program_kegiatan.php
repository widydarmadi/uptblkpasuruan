<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_program_kegiatan extends Model
{
    protected $table = "m_program_kegiatan";
    protected $primaryKey = "id_m_program_kegiatan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_program_kegiatan')+1;
    }

    public function sumber_dana()
    {
        return $this->belongsTo(\App\Models\M_sumber_dana::class,'id_m_sumber_dana','id_m_sumber_dana');
    }
}
