<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_pendidikan extends Model
{
    protected $table = "m_pendidikan";
    protected $primaryKey = "id_m_pendidikan";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_pendidikan')+1;
    }

    // public function m_hak_akses()
    // {
    //     return $this->belongsTo(\App\Models\M_hak_akses::class,'id_m_menu','id_m_menu');
    // }


}
