<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_agenda extends Model
{
    protected $table = "m_agenda";
    protected $primaryKey = "id_m_agenda";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_agenda')+1;
    }
}
