<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class M_teks_footer extends Model
{
    protected $table = "m_teks_footer";
    protected $primaryKey = "id_m_teks_footer";


    /* fungsi untuk mendapatkan nilai ID maksimal dari tabel */
    public function scopeMaxId($query)
    {
        return $query->max('id_m_teks_footer')+1;
    }

}
