<?php

namespace App;

use App\Models\Mainmn;
use App\Models\News;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;

class GlobalHelper
{
    public static function someNews()
    {
        return News::select('post_date', 'slug', 'post_title', 'post_description', 'photo_news')->take(9)->orderByDesc('id')->get();
    }

    public static function tanggal($date, $date_format)
    {
        return ($date == '' || $date == "''" || empty($date) || $date == null) ? null : htmlspecialchars(\Carbon\Carbon::createFromFormat('Y-m-d', $date)->format($date_format));
    }

    public static function rupiah($angka){
        $hasil = "IDR " . number_format($angka,0,',','.');
        return $hasil;
    }

    public static function romawi($n){
        $hasil = "";
        $iromawi = array("","I","II","III","IV","V","VI","VII","VIII","IX","X",20=>"XX",30=>"XXX",40=>"XL",50=>"L",
        60=>"LX",70=>"LXX",80=>"LXXX",90=>"XC",100=>"C",200=>"CC",300=>"CCC",400=>"CD",500=>"D",600=>"DC",700=>"DCC",
        800=>"DCCC",900=>"CM",1000=>"M",2000=>"MM",3000=>"MMM");
        if(array_key_exists($n,$iromawi)){
            $hasil = $iromawi[$n];
            }elseif($n >= 11 && $n <= 99){
            $i = $n % 10;
            $hasil = $iromawi[$n-$i] . Romawi($n % 10);
            }elseif($n >= 101 && $n <= 999){
            $i = $n % 100;
            $hasil = $iromawi[$n-$i] . Romawi($n % 100);
            }else{
            $i = $n % 1000;
            $hasil = $iromawi[$n-$i] . Romawi($n % 1000);
        }
        return $hasil;
    }

    public static function random_string($length)
    {
        $str_result = '23456789ABCDEFGHJKLMNPRSTUVWXYZ';
        return substr(str_shuffle($str_result),0, $length);
    }

    public static function penyebut($nilai)
    {
        $nilai = abs($nilai);
        $huruf = array("", "Satu", "Dua", "Tiga", "Empat", "Lima", "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas");
        $temp = "";
        if ($nilai < 12) {
            $temp = " " . $huruf[$nilai];
        } else if ($nilai < 20) {
            $temp = penyebut($nilai - 10) . " Belas";
        } else if ($nilai < 100) {
            $temp = penyebut($nilai / 10) . " Puluh" . penyebut($nilai % 10);
        } else if ($nilai < 200) {
            $temp = " Seratus" . penyebut($nilai - 100);
        } else if ($nilai < 1000) {
            $temp = penyebut($nilai / 100) . " Ratus" . penyebut($nilai % 100);
        } else if ($nilai < 2000) {
            $temp = " Seribu" . penyebut($nilai - 1000);
        } else if ($nilai < 1000000) {
            $temp = penyebut($nilai / 1000) . " Ribu" . penyebut($nilai % 1000);
        } else if ($nilai < 1000000000) {
            $temp = penyebut($nilai / 1000000) . " Juta" . penyebut($nilai % 1000000);
        } else if ($nilai < 1000000000000) {
            $temp = penyebut($nilai / 1000000000) . " Milyar" . penyebut(fmod($nilai, 1000000000));
        } else if ($nilai < 1000000000000000) {
            $temp = penyebut($nilai / 1000000000000) . " Trilyun" . penyebut(fmod($nilai, 1000000000000));
        }
        return $temp;
    }

    public static function terbilang($nilai)
    {
        if ($nilai < 0) {
            $hasil = "minus " . trim(penyebut($nilai));
        } else {
            $hasil = trim(penyebut($nilai));
        }
        return $hasil;
    }

    public static function is_angka_pecahan($val)
    {
        return is_numeric($val) && floor($val) != $val;
    }

    public static function terbilang_dan_decimal($nilai = null)
    {
        if ($nilai == null) {
            return 'function terbilang error : variabel nilai angka belum diisi !';
        } else {
            if (!is_numeric($nilai)) {
                return 'function terbilang error : harus berupa angka numerik !';
            } else {
                if ($nilai < 0) {
                    $hasil = "minus " . trim(penyebut($nilai));
                } else {
                    if (is_angka_pecahan($nilai)) {
                        // pecah by dot
                        $arr_pecah = explode('.', $nilai);
                        $hasil = trim(penyebut($nilai)) . ' Koma ' . penyebut($arr_pecah[1]);
                    } else {
                        $hasil = trim(penyebut($nilai));
                    }
                }
                return $hasil;
            }
        }
    }

    public static function rute_asal($val=null)
    {
        $_init = explode('.', Route::currentRouteName());
        $_count = count($_init);
        $route = array_slice($_init, 0, $_count - 1, true);
        $rute = implode('.', $route) . '.';
        return $rute.($val ?? null);
    }

    public static function rute_sekarang($val=null)
    {
        $_init = explode('.', Route::currentRouteName());
        $_count = count($_init);
        $route = array_slice($_init, 0, $_count, true);
        $rute = implode('.', $route);
        return $rute.($val ?? null);
    }

    public static function last_route($val=null)
    {
        $_init = explode('.', Route::currentRouteName());
        return last($_init);
    }

    /**
     * Convert empty string to null (input text)
     *
     * @param [type] $string
     * @return void
     */
    public static function contul($string)
    {
        if (filter_var($string, FILTER_VALIDATE_EMAIL)) {
                return ($string == '' || $string == "''" || empty($string) || $string==null) ? null : $string;
        }else{
            if(is_array($string)){
                return ($string == '' || $string == "''" || empty($string) || $string==null) ? null : \Str::upper($string[0]);
            }else{
                return ($string == '' || $string == "''" || empty($string) || $string==null) ? null : \Str::upper($string);
            }
        }

        // return ($string == '' || $string == "''" || empty($string) || $string==null) ? null : htmlspecialchars(\Str::upper($string));
    }

    /**
     * fungsi merubah YYYY-MM-DD ke DD String Bulan Indo YYYY
     *
     * @param [type] $string
     * @return void
     */
    public static function tanggal_indo($date)
    {
        $arr_bulan = [
            1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'
        ];
        $retval = date('d', strtotime($date)) . ' ' . $arr_bulan[(int) date('m', strtotime($date))] . ' ' . date('Y', strtotime($date));
        return $retval;
    }

    /**
     * fungsi merubah YYYY-MM-DD ke DD String Bulan Indo YYYY beserta hari nya
     *
     * @param [type] $string
     * @return void
     */
    public static function tanggal_hari($format,$nilai="now"){
        $en=array("Sun","Mon","Tue","Wed","Thu","Fri","Sat","Jan","Feb",
        "Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");
        $id=array("Minggu","Senin","Selasa","Rabu","Kamis","Jumat","Sabtu",
        "Januari","Februari","Maret","April","Mei","Juni","Juli","Agustus","September",
        "Oktober","November","Desember");
        return str_replace($en,$id,date($format,strtotime($nilai)));
    }

    /**
     * fungsi mencari selisih dob (YYYY-MM-DD) ke format yg ditentukan
     *
     * @param [type] $string
     * @return void
     */
    public static function ambil_umur($format, $tgl)
    {
        $retval = \Carbon\Carbon::parse($tgl)->diff(\Carbon\Carbon::now())->format('%'.$format.'%');
        return $retval;
    }
}
