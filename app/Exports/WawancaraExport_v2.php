<?php

namespace App\Exports;

use App\Invoice;
use App\Models\Cbt_user;
use App\Models\M_pendaftar;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class WawancaraExport_v2 implements FromView
{

    public function view(): View
    {
        $id_m_kategori_kejuruan = request()->filled('id_m_kategori_kejuruan') ? request()->get('id_m_kategori_kejuruan') : '';
        $id_m_kejuruan = request()->filled('id_m_kejuruan') ? request()->get('id_m_kejuruan') : '';
        $id_m_program_pelatihan = request()->filled('id_m_program_pelatihan') ? request()->get('id_m_program_pelatihan') : '';
        $id_m_jadwal = request()->filled('id_m_jadwal') ? request()->get('id_m_jadwal') : '';
        
        $data = M_pendaftar::view_pendaftar_v2($id_m_kategori_kejuruan, $id_m_kejuruan, $id_m_program_pelatihan, $id_m_jadwal);
        if($data){

            $sorted = $data->sortByDesc(function($q){
                if($q->seleksi){
                    return $q->seleksi->total_nilai_keseluruhan;
                }
            })->all();

        }else{
            $sorted = [];
        }
        return view('exports.wawancara', [
            'wawancara' => $sorted,
        ]);
    }
}