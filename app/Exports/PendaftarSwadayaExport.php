<?php

namespace App\Exports;

use App\Invoice;
use App\Models\Cbt_user;
use App\Models\M_pendaftar_mtu;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\Exportable;

class PendaftarSwadayaExport implements FromView
{

    public function view(): View
    {
        $id_m_kategori_kejuruan = request()->filled('id_m_kategori_kejuruan') ? request()->get('id_m_kategori_kejuruan') : '';
        $id_m_kejuruan = request()->filled('id_m_kejuruan') ? request()->get('id_m_kejuruan') : '';
        $id_m_program_pelatihan = request()->filled('id_m_program_pelatihan') ? request()->get('id_m_program_pelatihan') : '';
        $id_m_jadwal = request()->filled('id_m_jadwal') ? request()->get('id_m_jadwal') : '';
        $type = 'SWA';
        
        $data = M_pendaftar_mtu::view_pendaftar($id_m_kategori_kejuruan, $id_m_kejuruan, $id_m_program_pelatihan, $id_m_jadwal, $type);
        
        return view('exports.pendaftar_swa', [
            'data' => $data,
        ]);
    }
}